// ******************************************************
// tots.h   totalitzadors
// ******************************************************


#ifndef __INC_TOTS
#define __INC_TOTS


// totalitzadors
#define TOT_NSER     0
#define TOT_IMPC     1
#define TOT_IMPS     2
#define TOT_IMPT     3
#define TOT_KMT      4
#define TOT_KMO      5
#define TOT_KML      6
#define TOT_KMLP     7
#define TOT_TOC      8
#define TOT_TON      9
#define TOT_NDESC   10
#define TOT_TDESC   11
#define TOT_BORR    12
#define TOT_NSER_P  13
#define TOT_IMPC_P  14
#define TOT_IMPS_P  15
#define TOT_IMPT_P  16
#define TOT_KMT_P   17
#define TOT_KMO_P   18
#define TOT_KML_P   19
#define TOT_KMLP_P  20
#define TOT_TOC_P   21
#define TOT_TON_P   22
#define TOT_SALTS   23
#define TOT_TONHHMM 24
#define TOT_CCAB    25
#define TOT_E3      26
#define TOT_E6      27
#define TOT_NTICK   28
#define TOT_NTORN   29
#define TOT_BB      30
#define TOT_BB_P    31
#define TOT_KMOFF	32
#define TOT_KMOFF_P 33
#define TOT_FIN     34

// per dimensionar tarcom.disp_tot[] per si en el futur n'hi hagues mes
// fer els equips vells compatibles amb les tarifes noves
#define TOT_FIN_MAX  38   


struct s_total
{
  unsigned long value;
  unsigned long value_compl;
};

enum eConvertTot{CONVERT_NO = 0, CONVERT_MINUTS};

struct s_tot
{
 unsigned char parcial;  /* =1 tot. parcial */
 unsigned char modif;    /* =1 modificable  */
 unsigned char form;
 unsigned char led;
 enum eConvertTot convert;
};

extern struct s_total total[TOT_FIN];
extern const struct s_tot tot[];

extern long totalitzador_read(char ntot,int * st);
extern void totalitzador_write(char ntot, long value);
extern void totalitzador_reset(char ntot);
extern void totalitzador_add(char ntot, long value);
extern void totalitzador_incr(char ntot);
extern void totalitzador_disable(char ntot);
extern void totalitzador_compost_suma(int tsum, int t1, int t2);
//extern void totalitzador_compost_resta(int tsum, int t1, int t2);
extern long totalitzador_read_for_display(char ntot,int * st);
extern void borrar_tots_parcials(void);
#endif


