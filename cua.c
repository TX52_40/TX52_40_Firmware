//***********************************************************
// cua.c
//***********************************************************

#include "cua.h"

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

CODE_FAST void cua_init(t_cua* pCua,char* buf, int maxLen)
{
  pCua->p_in = 0;
  pCua->p_out = 0;
  pCua->maxLen = maxLen;
  pCua->buf = buf;  
}


CODE_FAST int cua_bytes_lliures(t_cua* pCua )
{
    if(pCua->p_in < pCua->p_out)
        return(pCua->p_out - pCua->p_in - 1);
    return(pCua->maxLen - 1 + pCua->p_out - pCua->p_in);
}

CODE_FAST int cua_extreure(t_cua* pCua, char* c)
{
	if(pCua->p_in == pCua->p_out)
	    return(1);
	*c = pCua->buf[pCua->p_out++];
	if(pCua->p_out >= pCua->maxLen)
	  pCua->p_out = 0;
	return(0);    
}

CODE_FAST int cua_guardar(t_cua* pCua,char* buf, int nc)
{
int i;
    if(cua_bytes_lliures(pCua) < nc )
      return(1);
    for(i=0;i<nc;i++)
    {
      pCua->buf[pCua->p_in] = buf[i];
      pCua->p_in++;
      if(pCua->p_in >= pCua->maxLen)
         pCua->p_in = 0;
    }          
    return(0);  
}

CODE_FAST int cua_guardar_c(t_cua* pCua,char c)
{
    if(cua_bytes_lliures(pCua) == 0 )
      return(1);
    pCua->buf[pCua->p_in] = c;
    pCua->p_in++;
    if(pCua->p_in >= pCua->maxLen)
       pCua->p_in = 0;
    return(0);  
}

