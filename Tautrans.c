/***************** tautrans.c ***************/



#include "tautrans.h"
#include "tx30.h"


/* array per call indirecte a semantiques */

const SUB0 executar_semantica_TX30[]={
               PASO_LIBRE        ,
               ON_OFF_CARG_CERRT   ,
               INICIO_CODS         ,
               INI_TOTS            ,
               LIBRE_OCUP          ,
               LIBRE_UP            ,
               LIBRE_ROT           ,
               LIB_ROT_UP          ,
               LIB_ROT_DWN         ,
               VISU_MOD_RELOJ      ,
               LLUM                ,
               ESP_OFF             ,
               LIBREOFF_OCAUT      ,
               BLOQ_TEC            ,
               reloj_en_libre      ,
               test_impre          ,
               OCUPADO_0           ,
               OCUP_OCUP           ,
               OCUP_UP             ,
               OCUP_DWN            ,
               OCUP_UPROT          ,
               OCUP_DWNROT         ,
               OCUP_PAGROT         ,
               SUPL_TECLA          ,
               OCUP_PAGAR          ,
               flipflop_vd         ,
               ERR_GENIMP          ,
               FIN_ERR_GENIMP      ,
               ROT_VIAJ            ,
               TARARI              ,
               SUMA_OCUP           ,
               PAGAR_0             ,
               PAGAR_OCUP          ,
               SUMA                ,
               PAGAR_UP            ,
               PAGAR_DWN           ,
               pag_lib_tecla       ,
               pag_lib_temps       ,
               pag_lib_dist        ,
               ABORT_SUPL          ,
               PAGAR_EURO          ,
               PAGAR_OCUP_MF       ,
               fin_reltots         ,
               RELOJ_NEXT          ,
               RELOJ_ACTUAL        ,
               RELOJ_FIN           ,
               AVANPAP_INCREREL    ,
               MODIF_DECREREL      ,
               OFF_0               ,
               OFF_LIBRE           ,
               DESBLOQ_TEC         ,
               ESP_ON              ,
               fin_cods            ,
               TEST                ,
               TESTIMPR            ,
               TOTS                ,
               TOTS_TR             ,
               CODS                ,
               CODS_PARMS_SIN_PASSW,
               MT_TOTS             ,
               MT_SKIP             ,
               MT_INCR             ,
               MT_CLR              ,
               MR_REL              ,
               MR_SKIP             ,
               MR_INCR             ,
               MR_FP               ,
               PARMS_0             ,
               PARMS               ,
               PARMS_K_ABORT       ,
               PARMS_TR            ,
               FS_0                ,
               FS_FI_IMPRE         ,
               FS_LIBRE            ,
               FS_PLUS             ,
               UP_UP               ,
               UP_DWN              ,
               UP_OCUPADO          ,
               OFF_TREURE_TENSIO   ,
               FS_DIST             ,
               CERR_0              ,
               CERR_TLIBRE         ,
               ABRIR_TURNO_COND    ,
               CERR_TREL           ,
               CERR_TIMER          ,
               TORN_REPE_TICKET    ,
               CERR_TCODS          ,
               CERR_TREURE_TENSIO  ,
               MOD_TAR_TNUM        ,
               APAGAR_LLUMINOS     ,
               TANCAR_TORN         ,
               PREPROG_TNUM        ,
               OFF_TCODS           ,
               FI_ITV              ,
               FS_LIB_TEMPS        ,
               PETICIO_PIN         ,
               TORN_ACK_PASSWORD   ,
               TORN_NACK_PASSWORD  ,
			   SUMA_SOLTAR         ,
			   ACUM_PAUSA		   ,
			   FI_ACUM_PAUSA	   ,
			   TOTS_PASW		   ,
			   PREC_ACORDADO
};  

// DEFINICION TABLA TRANSICIONES 


const struct s_taula_trans taula_trans_TX30[]={
/*  E_MEM_MAL */
    V_START        , E_MEM_MAL      , T_PASO_LIBRE        , 0x00           ,
    V_TLIBRE       , E_MEM_MAL      , T_ON_OFF_CARG_CERRT , 0x00           ,
/*  E_LIBRE */
    V_START        , E_LIBRE        , T_PASO_LIBRE     , 0x00           ,
    V_TNUM         , E_LIBRE        , T_LIBRE_OCUP     , MSK_FP|MSK_BLQ | MSK_PAUSA,
    V_TUP          , E_LIBRE        , T_LIBRE_UP       , MSK_FP|MSK_BLQ | MSK_PAUSA,
    V_TROT         , E_LIBRE        , T_LIBRE_ROT      , MSK_FP|MSK_BLQ | MSK_PAUSA,
    V_TROTUP       , E_LIBRE        , T_LIB_ROT_UP     , MSK_FP|MSK_BLQ | MSK_PAUSA,
    V_TROTDWN      , E_LIBRE        , T_LIB_ROT_DWN    , MSK_FP|MSK_BLQ | MSK_PAUSA,
    V_TLIBRE       , E_LIBRE        , T_ON_OFF_CARG_CERRT, MSK_BLQ,
    V_TREL         , E_LIBRE        , T_VISU_MOD_RELOJ , MSK_FP|MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_TTOT         , E_LIBRE        , T_INI_TOTS       , MSK_BLQ | MSK_PAUSA,
    V_TCODS        , E_LIBRE        , T_INICIO_CODS    , MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_TLLUM        , E_LIBRE        , T_LLUM           , MSK_FP|MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_GENIMP       , E_LIBRE        , T_PASO_LIBRE     , MSK_FP|MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_F_GENIMP     , E_LIBRE        , T_PASO_LIBRE     , MSK_FP|MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_TEMPOR       , E_LIBRE        , T_ESP_OFF        , MSK_BLQ,
    V_MF           , E_LIBRE        , T_PASO_LIBRE     , MSK_BLQ | MSK_PIN_ENTRAR,
    V_PASAUT       , E_LIBRE        , T_LIBREOFF_OCAUT , MSK_FP|MSK_BLQ | MSK_PIN_ENTRAR,
    V_BLOQ         , E_LIBRE        , T_BLOQ_TEC       , 0x00 | MSK_PIN_ENTRAR            | MSK_PAUSA,
    V_TEMPOR_1     , E_LIBRE        , T_reloj_en_libre , 0x00           ,
    V_TEMPOR_2     , E_LIBRE        , T_test_impre     , 0x00           ,
    V_TAPAGAR_LLUM , E_LIBRE        , T_APAGAR_LLUMINOS, 0x00 | MSK_PIN_ENTRAR            | MSK_PAUSA,
    V_TPLUSPLUS    , E_LIBRE        , T_TANCAR_TORN    , MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_FI_ITV       , E_LIBRE        , T_FI_ITV         , MSK_BLQ | MSK_PIN_ENTRAR | MSK_PAUSA,
    V_PETICIO_PIN  , E_LIBRE        , T_PETICIO_PIN    , MSK_BLQ | MSK_PAUSA,
    V_TSUMA        , E_LIBRE        , T_ACUM_PAUSA     , 0x00,
    V_TSOLTAR      , E_LIBRE        , T_FI_ACUM_PAUSA  , 0x00,
    V_PREC_ACORD   , E_LIBRE        , T_PREC_ACORDADO   , MSK_FP|MSK_BLQ | MSK_PAUSA,
/*  E_OCUPADO */
    V_START        , E_OCUPADO      , T_OCUPADO_0      , 0x00           ,
    V_TNUM         , E_OCUPADO      , T_OCUP_OCUP      , MSK_BLQ        ,
    V_TUP          , E_OCUPADO      , T_OCUP_UP        , MSK_BLQ        ,
    V_TDWN         , E_OCUPADO      , T_OCUP_DWN       , MSK_BLQ        ,
    V_TROTUP       , E_OCUPADO      , T_OCUP_UPROT     , MSK_BLQ        ,
    V_TROTDWN      , E_OCUPADO      , T_OCUP_DWNROT    , MSK_BLQ        ,
    V_TROT         , E_OCUPADO      , T_OCUP_PAGROT    , MSK_BLQ        ,
    V_TSUPL        , E_OCUPADO      , T_SUPL_TECLA     , MSK_BLQ        ,
    V_TPAGAR       , E_OCUPADO      , T_OCUP_PAGAR     , MSK_BLQ        ,
    V_TLIBRE       , E_OCUPADO      , T_flipflop_vd    , MSK_BLQ        ,
    V_GENIMP       , E_OCUPADO      , T_ERR_GENIMP     , 0x00           ,
    V_F_GENIMP     , E_OCUPADO      , T_FIN_ERR_GENIMP , 0x00           ,
    V_TEMPOR_1     , E_OCUPADO      , T_ROT_VIAJ       , 0x00           ,
    V_TR_TARI      , E_OCUPADO      , T_TARARI         , 0x00           ,
    V_TSUMA        , E_OCUPADO      , T_SUMA_OCUP      , MSK_BLQ        ,
/*  E_PAGAR */
    V_START        , E_PAGAR        , T_PAGAR_0        , 0x00           ,
    V_TNUM         , E_PAGAR        , T_PAGAR_OCUP     , 0x00           ,
    V_TSUPL        , E_PAGAR        , T_SUPL_TECLA     , 0x00 | MSK_TIPUS_IVA ,
    V_TSUMA        , E_PAGAR        , T_SUMA           , 0x00 | MSK_TIPUS_IVA ,
    V_TUP          , E_PAGAR        , T_PAGAR_UP       , 0x00 | MSK_TIPUS_IVA ,
    V_TDWN         , E_PAGAR        , T_PAGAR_DWN      , 0x00 | MSK_TIPUS_IVA ,
    V_TROTUP       , E_PAGAR        , T_PAGAR_DWN      , 0x00 | MSK_TIPUS_IVA ,
    V_TROTDWN      , E_PAGAR        , T_PAGAR_DWN      , 0x00 | MSK_TIPUS_IVA ,
    V_TROT         , E_PAGAR        , T_PAGAR_UP       , 0x00 | MSK_TIPUS_IVA ,
    V_TLIBRE       , E_PAGAR        , T_pag_lib_tecla  , 0x00 | MSK_TIPUS_IVA ,
    V_TEMPOR       , E_PAGAR        , T_SUMA           , 0x00  				  ,
    V_MF           , E_PAGAR        , T_SUMA           , 0x00 | MSK_TIPUS_IVA ,
    V_TSOLTAR      , E_PAGAR        , T_SUMA_SOLTAR    , 0x00                 ,
    V_GENIMP       , E_PAGAR        , T_ERR_GENIMP     , 0x00 | MSK_TIPUS_IVA ,
    V_F_GENIMP     , E_PAGAR        , T_FIN_ERR_GENIMP , 0x00 | MSK_TIPUS_IVA ,
    V_TEMPOR_1     , E_PAGAR        , T_pag_lib_temps  , 0x00                 ,
    V_PASAUT       , E_PAGAR        , T_pag_lib_dist   , 0x00 | MSK_TIPUS_IVA ,
    V_TCANCELSP    , E_PAGAR        , T_ABORT_SUPL     , 0x00 | MSK_TIPUS_IVA ,
    V_TEURO        , E_PAGAR        , T_PAGAR_EURO     , 0x00 | MSK_TIPUS_IVA ,
    V_APOCMF       , E_PAGAR        , T_PAGAR_OCUP_MF  , 0x00 | MSK_TIPUS_IVA ,
    V_TR_TARI      , E_PAGAR        , T_TARARI         , 0x00 | MSK_TIPUS_IVA ,
/*  E_RELOJ */
    V_START        , E_RELOJ        , T_fin_reltots    , 0x00           ,
    V_TREL         , E_RELOJ        , T_RELOJ_NEXT     , MSK_BLQ        ,
    V_TEMPOR       , E_RELOJ        , T_RELOJ_ACTUAL   , MSK_BLQ        ,
    V_TEMPOR_1     , E_RELOJ        , T_RELOJ_FIN      , MSK_BLQ        ,
    V_MF           , E_RELOJ        , T_fin_reltots    , MSK_BLQ|MSK_REL,
    V_TABORT       , E_RELOJ        , T_fin_reltots    , MSK_BLQ        ,
    V_TSUMA        , E_RELOJ        , T_AVANPAP_INCREREL, MSK_BLQ       ,
    V_TTOT         , E_RELOJ        , T_MODIF_DECREREL , MSK_BLQ        ,
/*  E_OFF */
    V_START        , E_OFF          , T_OFF_0          , 0x00           ,
    V_TLIBRE       , E_OFF          , T_OFF_LIBRE      , MSK_BLQ,
    V_PASAUT       , E_OFF          , T_LIBREOFF_OCAUT , MSK_BLQ,
    V_TREL         , E_OFF          , T_VISU_MOD_RELOJ , MSK_BLQ,
    V_TTOT         , E_OFF          , T_INI_TOTS       , MSK_BLQ,
    V_BLOQ         , E_OFF          , T_DESBLOQ_TEC    , 0x00   ,
    V_SEN2         , E_OFF          , T_OFF_LIBRE      , 0x00   ,
    V_TEMPOR       , E_OFF          , T_ESP_ON         , 0x00           ,
    V_TEMPOR_1     , E_OFF          , T_OFF_TREURE_TENSIO, 0x00         ,
//  afegit per poder entrar password ITV fran�a    
    V_TCODS        , E_OFF         , T_OFF_TCODS    , MSK_BLQ   ,
/*  E_TEST */
    V_START        , E_TEST         , T_fin_cods       , 0x00           ,
    V_TEMPOR       , E_TEST         , T_TEST           , MSK_BLQ        ,
    V_MF           , E_TEST         , T_fin_cods       , MSK_BLQ        ,
    V_TABORT       , E_TEST         , T_fin_cods       , MSK_BLQ        ,
    V_TSUMA        , E_TEST         , T_TESTIMPR       , MSK_BLQ        ,
/*  E_TOTS */
    V_START        , E_TOTS         , T_fin_reltots    , 0x00           ,
    V_TEMPOR       , E_TOTS         , T_TOTS           , MSK_BLQ        ,
    V_TTOT         , E_TOTS         , T_TOTS           , MSK_BLQ        ,
    V_TSUMA        , E_TOTS         , T_TOTS_TR        , MSK_BLQ        ,
    V_MF           , E_TOTS         , T_fin_reltots    , MSK_BLQ        ,
    V_TABORT       , E_TOTS         , T_fin_reltots    , MSK_BLQ        ,
    V_TNUM         , E_TOTS         , T_TOTS_PASW      , MSK_BLQ        ,
/*  E_CODS */
    V_START        , E_CODS         , T_fin_cods       , 0x00           ,
    V_TABORT       , E_CODS         , T_fin_cods       , MSK_BLQ        ,
    V_MF           , E_CODS         , T_fin_cods       , MSK_BLQ        ,
    V_TEMPOR_1     , E_CODS         , T_fin_cods       , MSK_BLQ        ,
    V_TNUM         , E_CODS         , T_CODS           , MSK_BLQ        ,
    V_TCODS        , E_CODS         , T_CODS_PARMS_SIN_PASSW, MSK_BLQ   ,
/*  E_MODTOT */
    V_START        , E_MODTOT       , T_fin_cods       , 0x00           ,
    V_TEMPOR       , E_MODTOT       , T_MT_TOTS        , MSK_BLQ        ,
    V_TTOT         , E_MODTOT       , T_MT_TOTS        , MSK_BLQ        ,
    V_TSKIP        , E_MODTOT       , T_MT_SKIP        , MSK_BLQ        ,
    V_TINCR        , E_MODTOT       , T_MT_INCR        , MSK_BLQ        ,
    V_TCLR         , E_MODTOT       , T_MT_CLR         , MSK_BLQ        ,
    V_MF           , E_MODTOT       , T_fin_cods       , MSK_BLQ        ,
    V_TABORT       , E_MODTOT       , T_fin_cods       , MSK_BLQ        ,
/*  E_MODREL */
    V_START        , E_MODREL       , T_fin_cods       , 0x00           ,
    V_TEMPOR       , E_MODREL       , T_MR_REL         , MSK_BLQ        ,
    V_TREL         , E_MODREL       , T_MR_REL         , MSK_BLQ        ,
    V_TSKIP        , E_MODREL       , T_MR_SKIP        , MSK_BLQ        ,
    V_TINCR        , E_MODREL       , T_MR_INCR        , MSK_BLQ        ,
    V_MF           , E_MODREL       , T_fin_cods       , MSK_BLQ        ,
    V_TABORT       , E_MODREL       , T_fin_cods       , MSK_BLQ        ,
/*  E_MODFP */
    V_START        , E_MODFP        , T_fin_cods       , 0x00           ,
    V_TEMPOR       , E_MODFP        , T_MR_FP          , MSK_BLQ        ,
    V_TREL         , E_MODFP        , T_MR_FP          , MSK_BLQ        ,
    V_TSKIP        , E_MODFP        , T_MR_SKIP        , MSK_BLQ        ,
    V_TINCR        , E_MODFP        , T_MR_INCR        , MSK_BLQ        ,
    V_MF           , E_MODFP        , T_fin_cods       , MSK_BLQ        ,
    V_TABORT       , E_MODFP        , T_fin_cods       , MSK_BLQ        ,
/*  E_PARMS */
    V_START        , E_PARMS        , T_PARMS_0        , 0x00           ,
    V_TEMPOR       , E_PARMS        , T_PARMS          , MSK_BLQ,
    V_TREL         , E_PARMS        , T_PARMS          , MSK_BLQ,
    V_MF           , E_PARMS        , T_PARMS_K_ABORT  , MSK_BLQ,
    V_TABORT       , E_PARMS        , T_fin_cods       , MSK_BLQ,
    V_TINCR        , E_PARMS        , T_PARMS_TR       , MSK_BLQ,
/*  E_FISERV */
    V_START        , E_FISERV       , T_FS_0           , 0x00           ,
    V_TR_IN        , E_FISERV       , T_FS_FI_IMPRE    , MSK_BLQ        ,
    V_TLIBRE       , E_FISERV       , T_FS_LIBRE       , MSK_BLQ|MSK_IMPRE|MSK_FSLIB,
    V_TUP          , E_FISERV       , T_FS_LIBRE       , MSK_BLQ|MSK_IMPRE|MSK_FSLIB,
    V_TSUMA        , E_FISERV       , T_FS_PLUS        , MSK_BLQ|MSK_IMPRE,
    V_PASAUT       , E_FISERV       , T_FS_DIST        , 0x00             , 
    V_TEMPOR_1     , E_FISERV       , T_FS_LIB_TEMPS   , 0x00           ,
/*  E_UP */
    V_START        , E_UP           , T_PASO_LIBRE     , 0x00           ,
    V_TUP          , E_UP           , T_UP_UP          , MSK_BLQ|MSK_REL_LIB,
    V_TDWN         , E_UP           , T_UP_DWN         , MSK_BLQ|MSK_REL_LIB,
    V_TEMPOR       , E_UP           , T_UP_OCUPADO     , MSK_BLQ        ,  
/*  E_CERRADO */
    V_START        , E_CERRADO      , T_CERR_0         , 0x00           ,
    V_TLIBRE       , E_CERRADO      , T_CERR_TLIBRE    , MSK_BLQ        ,
    V_TNUM         , E_CERRADO      , T_ABRIR_TURNO_COND , MSK_BLQ      ,
    V_TREL         , E_CERRADO      , T_CERR_TREL      , MSK_BLQ        ,
//    V_TR_IN        , E_CERRADO      , T_CT_TR          , MSK_BLQ      ,
    V_TCODS        , E_CERRADO      , T_CERR_TCODS     , MSK_BLQ        ,
    V_TTOT         , E_CERRADO      , T_INI_TOTS       , MSK_BLQ        ,
    V_TEMPOR       , E_CERRADO      , T_CERR_TIMER     , MSK_BLQ        ,
    V_TSUMA        , E_CERRADO      , T_TORN_REPE_TICKET,MSK_BLQ        ,
    V_TEMPOR_1     , E_CERRADO      , T_CERR_TREURE_TENSIO, 0x00         ,
    V_ACK_PASSWORD , E_CERRADO      , T_TORN_ACK_PASSWORD , 0x00         ,
    V_NACK_PASSWORD, E_CERRADO      , T_TORN_NACK_PASSWORD, 0x00         ,
/*  E_MOD_TAR */
    V_START        , E_MOD_TAR      , T_fin_cods          , 0x00           ,
    V_TABORT       , E_MOD_TAR      , T_fin_cods          , MSK_BLQ        ,
    V_TNUM         , E_MOD_TAR      , T_MOD_TAR_TNUM      , MSK_BLQ        ,
/*  E_PREPROG */
    V_START        , E_PREPROG      , T_fin_cods          , 0x00           ,
    V_TABORT       , E_PREPROG      , T_fin_cods          , MSK_BLQ        ,
    V_TNUM         , E_PREPROG      , T_PREPROG_TNUM      , MSK_BLQ        ,
};

/* DEFINICION TABLA NUM.TRANS. INICIAL PARA CADA ESTADO */

const unsigned char trans_ini_TX30[19]={
      0, /* E_MEM_MAL */
      2, /* E_LIBRE */
     28, /* E_OCUPADO */
     43, /* E_PAGAR */
     64, /* E_RELOJ */
     72, /* E_OFF */
     82, /* E_TEST */
     87, /* E_TOTS */
     94, /* E_CODS */
    100, /* E_MODTOT */
    108, /* E_MODREL */
    115, /* E_MODFP */
    122, /* E_PARMS */
    128, /* E_FISERV */
    135, /* E_UP */
    139, // E_CERRADO
    150, // E_MOD_TAR
    153, // E_PREPROG
    156,
  };

/* DEFINICION ESTRUCTURA ESTADO(.grup_luz) */

const int menu_estado_TX30[18]={
    GL_MAL         ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_OFF         ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_LIBRE       ,
    GL_FISERV      ,
    GL_LIBRE       ,
    GL_OFF         ,
    GL_LIBRE       ,
    GL_LIBRE
};

// definicio llum frontal segons estat

const int luz_frontal[18]={
      0, /* E_MEM_MAL */
      1, /* E_LIBRE */
      1, /* E_OCUPADO */
      1, /* E_PAGAR */
      1, /* E_RELOJ */
      0, /* E_OFF */
      1, /* E_TEST */
      1, /* E_TOTS */
      1, /* E_CODS */
      1, /* E_MODTOT */
      1, /* E_MODREL */
      1, /* E_MODFP */
      1, /* E_PARMS */
      1, /* E_FISERV */
      1, /* E_UP */
      0, // E_CERRADO
      1, // E_MOD_TAR
      1, // E_PREPROG
};