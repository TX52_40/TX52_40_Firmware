//*************************************************************
// serial.h
//*************************************************************

#ifndef __SERIAL_H
#define __SERIAL_H

#include "cua.h"
#include "rtos.h"

typedef struct{
  int flag_tx_activa;
  // cua tx i rx
  t_cua cua_rx;
  t_cua cua_tx;
}t_usart;

#define MODO_232 0
#define MODO_485 1

#define USART_LUMIN     0   // Usart lluminos
#define USART_TD30      1   // Usart per comunicar amb TD30
#define USART_CCAB      1   // Usart per ComputerCab
#define USART_IMPRE     1   // usart per impressora externa
#define USART_BLUETOOTH 2   // usart per bluetooth
#define USART_BT40      1   // usart per BT40


extern t_usart usart[];

extern void usart_install(int nUsart, OS_ISR_HANDLER* pISRHandler);
extern void usart_init(int nUsart, int bauds, int mode, int odd );
extern void usart_hard(int nUsart, int modo, int invert_tx, int invert_rx, int carga);
extern void usart_enviar(int nUsart,char* buf, int nBytes);
extern int usart_extreure(int nUsart,char* buf,int max_nc);
extern int usart_extreure_char(int nUsart,char* c);


extern void UART1_IRQHandler(void);
extern void UART1_IRQHandler_Ccab(void);

#endif
