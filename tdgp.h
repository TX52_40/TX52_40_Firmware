#ifndef _INC_TDGP
#define _INC_TDGP
// Fitxer de constants generat pel TDGP 


/////// Mailboxes
#define MBX_NULL                        0
#define MBX_NUM_MAX       4

/////// Tipos de Formato
#define FORM_LZ                          0
#define FORM_MON                         1
#define FORM_LZ_EBCDIC                   2
#define FORM_DDMMAA                      3
#define FORM_N_BCD                       4
#define FORM_AMD                         5
#define FORM_HHMM                        6
#define FORM_HM                          7
#define FORM_KM                          8
#define FORM_DEC                         9
#define FORM_1DEC                        10
#define FORM_0DEC                        11
#define FORM_HMSMD                       12
#define FORM_LLVAR                       13
#define FORM_0DEC_BLK                    14
#define FORM_DM                          15
#define FORM_ES0                         16
#define FORM_4BITS                       17
#define FORM_SECRET                      18
#define FORM_DMA                         19
#define FORM_BIN                         20
#define FORM_RZ                          21
#define FORM_RZ_VAR                      22
#define FORM_LZ_VAR                      23
#define FORM_CONTRAVALOR                 24
#define FORM_2DEC                        25
#define FORM_ACH                         26
#define FORM_MON_AUX                     27
#define FORM_DEC_AUX                     28
#define FORM_RZ_ACH                      29
#define FORM_RZ_ALFANUM                  30
#define FORM_MON_AST                     31
#define FORM_MON_AUX_AST                 32
#define FORM_SECRET_CC                   33
#define FORM_RZ_NUM                      34
#define FORM_ANAG                        35
#define FORM_FONT_SIZE                   36
#define FORM_G_ES0                       37
#define FORM_FIN_G_ES0                   38
#define FORM_TOT_MINUTS                  39
#define FORM_TOT_HORES                   40
#define FORM_MON_LZ                      41
#define FORM_MON_AUX_LZ                  42
#define FORM_4DEC                        43
#define FORM_3DEC                        44
#define FORM_5DEC                        45
#define FORM_6DEC                        46
#define FORM_LOGO                        47

/////// Tipos de Pantalla
#define TIPPANT_VISU                     0

/////// Tipos de Registro
#define TREG_MONOCAMP                    0
#define TREG_MULTICAMP                   1

/////// Funciones de Menu
typedef void (*SUB)(int);

/*
SUB subrutines_menu[]={
(SUB)NULL,
};
*/

/////// Opciones de Menu
#define OPCM_NULA                        0
#define MAX_NUM_OPCMENU  1

/////// Opciones de Ticket (y Pantalla !!)
#define OPCT_CERO                        0
#define OPCT_COPIA_SER2                  1
#define OPCT_HAY_NIF                     2
#define OPCT_HAY_LICENCIA                3
#define OPCT_HAY_MATRICULA               4
#define OPCT_HAY_NOMBRE                  5
#define OPCT_LINEAS_CON_TOTAL            6
#define OPCT_LINEAS_SIN_TOTAL            7
#define OPCT_PRECIO_FIJO                 8
#define OPCT_NOT_INSIKA                  9
#define OPCT_GENERICO1                   10
#define OPCT_GENERICO2                   11
#define OPCT_GENERICO3                   12
#define OPCT_GENERICO4                   13
#define OPCT_COPIA_SER1                  14
#define OPCT_COPIA_SER3                  15
#define OPCT_IMPR_32_24                  16
#define OPCT_G_ES0                       252
#define OPCT_G_NO_ES0                    253
#define OPCT_NO_ES0                      254
#define OPCT_ES0                         255
#define OPCT_nmax                        21

/////// Tipos de Campo
#define VAR_FIN                          0
#define VAR_UCHAR                        1
#define VAR_UINT                         2
#define VAR_ULONG                        3
#define VAR_FEHOR                        4
#define VAR_ASC                          5
#define VAR_BCD                          6
#define VAR_AMDHM                        7
#define VAR_AMD                          8
#define VAR_HM                           9
#define VAR_STR                          10
#define VAR_SUMA                         11
#define VAR_FLOAT                        12

/////// Tipos de Cnv
#define CNV_FIN                          0

/////// Parámetros de Configuración
#define PAR_hay_impresora            prmt_i[0]
#define PAR_hay_varios               prmt_i[1]
#define PAR_hay_peajes               prmt_i[2]
#define PAR_diames_mesdia            prmt_i[3]
#define PAR_num_extras               prmt_i[4]
#define PAR_hay_supl_aut             prmt_i[5]

#define PARAM_nmax                          6

/////// 
#define PARAM_hay_impresora                 0
#define PARAM_hay_varios                    1
#define PARAM_hay_peajes                    2
#define PARAM_diames_mesdia                 3
#define PARAM_num_extras                    4
#define PARAM_hay_supl_aut                  5

/////// Tickets
#define TICKET_SERV1                     0
#define TICKET_SERV2                     1
#define TICKET_AVANCE_PAPEL              2
#define TICKET_TCK_TOTS                  3
#define TICKET_TCK_PAR_TX30              4
#define TICKET_PAR_TX30_UPD              5
#define TICKET_TCK_PAR_TX30_CONT         6
#define TICKET_TCK_PAR_TX30_FIN          7
#define TICKET_PRIMA_TURNO               8
#define TICKET_PRIMA_SERVEI_N            9
#define TICKET_PRIMA_TURNO_PEU           10
#define TICKET_SERV3                     11
#define TICKET_PER_TARIFA                12
#define TICKET_EVENTS_TORN_INI           13
#define TICKET_EVENTS_TORN_DESC          14
#define TICKET_EVENTS_TORN_FIN           15
#define TICKET_EVENTS_TORN_TORN          16
#define TICKET_CABE_NO_LEGAL             17
#define TICKET_PEU_NO_LEGAL              18
#define TICKET_PAR_TX30_UPD_TAR          19
#define TICKET_TCK_TOTS_HEADER           20
#define TICKET_TCK_TOTS_PERIODICO        21
#define TICKET_PAR_TX80_UPD_K            22
#define MAX_NUM_TICKETS  23

/////// Definiciones de Registros
#define DREG_MONOTXT                     1
#define DREG_SERVEI                      2
#define DREG_MONEDA                      3
#define DREG_MONOCHAR                    4
#define DREG_TCKS_TX30                   5
#define DREG_CONTINUAR_TICKET            6
#define DREG_PRIMA_TORN                  7
#define DREG_PRIMA_SERVEI                8
#define MAX_NUM_DREGS  9

/////// Tablas
#define TABLA_CONTINUAR_TICKET           2
#define TABLA_PRIMA_TORNS                3
#define TABLA_PRIMA_SERVEIS              4
#define MAX_NUM_TABLAS  5

/////// Registros
#define REG_NULO                         0
#define REG_TXTSYS                       1
#define REG_TXTGEN                       2
#define REG_OPCTS                        3
#define REG_SERVEI                       4
#define REG_MONEDA                       6
#define REG_TCKS_TX30                    7
#define REG_CONTINUAR_TICKET             8
#define REG_PRIMA_TORN                   9
#define REG_PRIMA_TORN_AUX               10
#define REG_PRIMA_SERVEI                 11
#define REG_PRIMA_SERVEI_AUX             12
#define MAX_NUM_REGS  13

/////// Campos

/////// Campos de Definición de Registro: DREG_CONTINUAR_TICKET
#define CAM_num_ticket                   0
#define CAM_reg_index                    1
#define CAM_camp_fin_index               2
#define CAM_taula_repeticio              3
#define CAM_reg_repeticio                4
#define CAM_camp_ini_index               5

/////// Campos de la Definición de Registro: DREG_MONEDA
#define CAM_ISO_A                        0
#define CAM_num_dec_A                    1
#define CAM_punto_coma_A                 2
#define CAM_abrev_A                      3
#define CAM_ISO_B                        4
#define CAM_num_dec_B                    5
#define CAM_punto_coma_B                 6
#define CAM_abrev_B                      7
#define CAM_contravalor                  8
#define CAM_decimales_contravalor        9
#define CAM_mostrar_contravalor          10
#define CAM_moneda_tecleo                11
#define CAM_trim_ab_A                    12
#define CAM_trim_ab_B                    13

/////// Campos de la Definición de Registro: DREG_MONOCHAR
#define CAM_ch                           0

/////// Campos de la Definición de Registro: DREG_MONOTXT
#define CAM_text                         0

/////// Campos de la Definición de Registro: DREG_PRIMA_SERVEI
#define CAM_numero_servei                0
#define CAM_servei_num_tick              1
#define CAM_servei_ini                   2
#define CAM_servei_fi                    3
#define CAM_servei_import                4
#define CAM_servei_total                 5
#define CAM_servei_supl                  6
#define CAM_servei_kmlib                 7
#define CAM_servei_kmoc                  8
#define CAM_servei_min                   9
#define CAM_servei_min_oc                10
#define CAM_servei_velmax                11
#define CAM_servei_velmaxlib             12
#define CAM_servei_hm_libre_pasj         13
#define CAM_servei_buf_tartrab           14
#define CAM_servei_fet_ticket            15
#define CAM_servei_ini_lliure            16

/////// Campos de la Definición de Registro: DREG_PRIMA_TORN
#define CAM_num_torn                     0
#define CAM_fecha_ini                    1
#define CAM_fecha_fi                     2
#define CAM_num_serveis                  3
#define CAM_nreg_servei_ini              4
#define CAM_num_conductor                5
#define CAM_eur_km                       6
#define CAM_import_torn                  7
#define CAM_supl_torn                    8
#define CAM_total_torn                   9
#define CAM_km_oc_torn                   10
#define CAM_km_torn                      11
#define CAM_km_per_cent                  12
#define CAM_min_oc_torn                  13
#define CAM_min_torn                     14
#define CAM_min_per_cent                 15
#define CAM_fecha_fi_torn_ant            16
#define CAM_dum1_torn                    17
#define CAM_hm_libre_pasj_torn_tanca     18
#define CAM_km_torn_tancat_prev          19
#define CAM_velmax_torn_tancat_prev      20
#define CAM_hm_libre_pasj_fins_fi_to     21
#define CAM_km_fins_fi_torn              22
#define CAM_velmax_fins_fi_torn          23
#define CAM_hhmm_ini_tram_fi_torn        24
#define CAM_neto_torn                    25
#define CAM_iva_torn                     26
#define CAM_percent_iva0                 27
#define CAM_percent_iva1                 28
#define CAM_percent_iva2                 29
#define CAM_iva_0                        30
#define CAM_iva_1                        31
#define CAM_iva_2                        32

/////// Campos de la Definición de Registro: DREG_SERVEI
#define CAM_import                       0
#define CAM_supl_desgl_0                 1
#define CAM_supl_desgl_1                 2
#define CAM_supl_desgl_2                 3
#define CAM_supl_desgl_3                 4
#define CAM_supl_autom                   5
#define CAM_total                        6
#define CAM_servei_bb                    7
#define CAM_import_dist                  8
#define CAM_import_temps                 9
#define CAM_servei_tartrab               10
#define CAM_finicio                      11
#define CAM_ffin                         12
#define CAM_hm_oc                        13
#define CAM_minuts_ocup                  14
#define CAM_num_servei                   15
#define CAM_d_t_sep                      16
#define CAM_buf_tartrab                  17
#define CAM_dum_percent_iva              18
#define CAM_import_net                   19
#define CAM_import_iva                   20
#define CAM_num_tick                     21
#define CAM_per_tar_nom                  22
#define CAM_per_tar_hm_oc                23
#define CAM_per_tar_import               24
#define CAM_percent_iva                  25
#define CAM_import_tarifat               26
#define CAM_corsa_min                    27
#define CAM_total_abans_corsa_min        28
#define CAM_aplicat_corsa_min            29
#define CAM_dum1_servei                  30
#define CAM_import_abans_corsa_min       31
#define CAM_salt_distancia               32
#define CAM_salt_temps                   33
#define CAM_distancia_tarifada           34
#define CAM_temps_tarifat                35
#define CAM_acum_dist_control            36
#define CAM_acum_temps_control           37
#define CAM_es_control                   38
#define CAM_dum2                         39
#define CAM_longitud_inicio              40
#define CAM_longitud_final               41
#define CAM_latitud_inicio               42
#define CAM_latitud_final                43
#define CAM_norte_sur_inicio             44
#define CAM_norte_sur_final              45
#define CAM_este_oeste_inicio            46
#define CAM_este_oeste_final             47
#define CAM_per_tar_temps                48
#define CAM_numTick8dig                  49
#define CAM_num_salts                    50
#define CAM_propinas                     51
#define CAM_peajes                       52
#define CAM_supl_desgl_3r                53
#define CAM_supl_desgl_4                 54
#define CAM_supl_desgl_5                 55
#define CAM_supl_desgl_6                 56
#define CAM_supl_desgl_7                 57
#define CAM_numServ8Dig                  58

/////// Campos de la Definición de Registro: DREG_TCKS_TX30
#define CAM_cTOT_NSER                    0
#define CAM_cTOT_IMPC                    1
#define CAM_cTOT_IMPS                    2
#define CAM_cTOT_IMPT                    3
#define CAM_cTOT_KMT                     4
#define CAM_cTOT_KMO                     5
#define CAM_cTOT_KML                     6
#define CAM_cTOT_KMLP                    7
#define CAM_cTOT_TOC                     8
#define CAM_cTOT_TON                     9
#define CAM_cTOT_NDESC                   10
#define CAM_cTOT_TDESC                   11
#define CAM_cTOT_BORR                    12
#define CAM_cTOT_SALTS                   13
#define CAM_cTOT_TONHHMM                 14
#define CAM_cTOT_E3                      15
#define CAM_cTOT_E6                      16
#define CAM_cTOT_NSER_P                  17
#define CAM_cTOT_IMPC_P                  18
#define CAM_cTOT_IMPS_P                  19
#define CAM_cTOT_IMPT_P                  20
#define CAM_cTOT_KMT_P                   21
#define CAM_cTOT_KMO_P                   22
#define CAM_cTOT_KML_P                   23
#define CAM_cTOT_KMLP_P                  24
#define CAM_cTOT_TOC_P                   25
#define CAM_cTOT_TON_P                   26
#define CAM_k                            27
#define CAM_nt                           28
#define CAM_f_grab                       29
#define CAM_CCC                          30
#define CAM_visu_chk                     31
#define CAM_visu_f_precambio             32
#define CAM_visu_chk_prec                33
#define CAM_check_programa               34
#define CAM_check_tiquet                 35
#define CAM_nmodifs_k                    36
#define CAM_nmodifs_tar                  37
#define CAM_n_lic                        38
#define CAM_n_serie                      39
#define CAM_f_exp                        40
#define CAM_f_inst                       41
#define CAM_CCC_inst                     42
#define CAM_VERSION_TX                   43
#define CAM_ntar                         44
#define CAM_ledocup                      45
#define CAM_sduuummm                     46
#define CAM_bb                           47
#define CAM_m_ps                         48
#define CAM_seg_ps                       49
#define CAM_impkm_ss                     50
#define CAM_impho_ss                     51
#define CAM_ledapagar                    52
#define CAM_supl                         53
#define CAM_texte_nom                    54
#define CAM_texte_dni                    55
#define CAM_texte_llicencia              56
#define CAM_texte_matricula              57
#define CAM_fecha_actual                 58
#define CAM_mp_version                   59
#define CAM_mp_check                     60
#define CAM_mp_fecha                     61
#define CAM_texte_H1                     62
#define CAM_texte_H2                     63
#define CAM_texte_H3                     64
#define CAM_texte_H4                     65
#define CAM_texte_H5                     66
#define CAM_texte_H6                     67
#define CAM_texte_F1                     68
#define CAM_texte_F2                     69
#define CAM_texte_F3                     70
#define CAM_texte_F4                     71
#define CAM_texte_F5                     72
#define CAM_texte_F6                     73
#define CAM_cTOT_NTICK                   74
#define CAM_fecha_borr_tots              75
#define CAM_texte1                       76
#define CAM_texte2                       77
#define CAM_texte3                       78
#define CAM_texte4                       79
#define CAM_texte5                       80
#define CAM_texte6                       81
#define CAM_texte7                       82
#define CAM_texte8                       83
#define CAM_texte9                       84
#define CAM_texte10                      85
#define CAM_num_dia_torn                 86
#define CAM_diames_torn                  87
#define CAM_hora_inici_torn              88
#define CAM_hora_final_torn              89
#define CAM_fi_torn_guions               90
#define CAM_hora_inici_descans           91
#define CAM_inici_descans_guions         92
#define CAM_hora_final_descans           93
#define CAM_final_descans_guions         94
#define CAM_buffer_insika                95
#define CAM_dum_2b                       96
#define CAM_cTOT_BB                      97
#define CAM_cTOT_BB_P                    98
#define CAM_Texte_Document               99
#define CAM_cTOT_KMOFF                   100
#define CAM_cTOT_KMOFF_P                 101
#define CAM_operari                      102
#define CAM_texte_H1_32                  103
#define CAM_texte_H2_32                  104
#define CAM_texte_H3_32                  105
#define CAM_texte_H4_32                  106
#define CAM_texte_H5_32                  107
#define CAM_texte_H6_32                  108
#define CAM_texte_F1_32                  109
#define CAM_texte_F2_32                  110
#define CAM_texte_F3_32                  111
#define CAM_texte_F4_32                  112
#define CAM_texte_F5_32                  113
#define CAM_texte_F6_32                  114
#define CAM_texte_nom_32                 115

/////// STRUCTS
typedef struct S_DREG_MONOTXT {
unsigned char*           text;
} T_DREG_MONOTXT;

typedef struct S_DREG_SERVEI {
unsigned long            import;
unsigned long            supl_desgl_0;
unsigned long            supl_desgl_1;
unsigned long            supl_desgl_2;
unsigned long            supl_desgl_3;
unsigned long            supl_autom;
unsigned long            total;
unsigned long            servei_bb;
unsigned long            import_dist;
unsigned long            import_temps;
unsigned long            servei_tartrab;
T_FECHHOR                finicio;
T_FECHHOR                ffin;
unsigned long            hm_oc;
unsigned long            minuts_ocup;
unsigned short           num_servei;
unsigned char            d_t_sep;
unsigned char            buf_tartrab[12];
unsigned char            dum_percent_iva;
unsigned long            import_net;
unsigned long            import_iva;
unsigned short           num_tick;
unsigned char            per_tar_nom[2];
unsigned long            per_tar_hm_oc;
unsigned long            per_tar_import;
float                    percent_iva;
unsigned long            import_tarifat;
unsigned long            corsa_min;
unsigned long            total_abans_corsa_min;
unsigned short           aplicat_corsa_min;
unsigned char            dum1_servei[2];
unsigned long            import_abans_corsa_min;
unsigned long            salt_distancia;
unsigned long            salt_temps;
unsigned long            distancia_tarifada;
unsigned long            temps_tarifat;
unsigned long            acum_dist_control;
unsigned long            acum_temps_control;
unsigned char            es_control;
unsigned char            dum2[3];
unsigned long            longitud_inicio;
unsigned long            longitud_final;
unsigned long            latitud_inicio;
unsigned long            latitud_final;
unsigned char            norte_sur_inicio;
unsigned char            norte_sur_final;
unsigned char            este_oeste_inicio;
unsigned char            este_oeste_final;
unsigned long            per_tar_temps;
unsigned long            numTick8dig;
unsigned long            num_salts;
unsigned long            propinas;
unsigned long            peajes;
unsigned long            supl_desgl_3r;
unsigned long            supl_desgl_4;
unsigned long            supl_desgl_5;
unsigned long            supl_desgl_6;
unsigned long            supl_desgl_7;
unsigned long            numServ8Dig;
} T_DREG_SERVEI;

typedef struct S_DREG_MONEDA {
unsigned char            ISO_A[4];
unsigned short           num_dec_A;
unsigned char            punto_coma_A[1];
unsigned char            abrev_A[5];
unsigned char            ISO_B[4];
unsigned short           num_dec_B;
unsigned char            punto_coma_B[1];
unsigned char            abrev_B[5];
unsigned long            contravalor;
unsigned short           decimales_contravalor;
unsigned short           mostrar_contravalor;
unsigned short           moneda_tecleo;
unsigned char            trim_ab_A[4];
unsigned char            trim_ab_B[4];
} T_DREG_MONEDA;

typedef struct S_DREG_MONOCHAR {
unsigned char            ch;
} T_DREG_MONOCHAR;

typedef struct S_DREG_TCKS_TX30 {
unsigned long            cTOT_NSER;
unsigned long            cTOT_IMPC;
unsigned long            cTOT_IMPS;
unsigned long            cTOT_IMPT;
unsigned long            cTOT_KMT;
unsigned long            cTOT_KMO;
unsigned long            cTOT_KML;
unsigned long            cTOT_KMLP;
unsigned long            cTOT_TOC;
unsigned long            cTOT_TON;
unsigned long            cTOT_NDESC;
unsigned long            cTOT_TDESC;
unsigned long            cTOT_BORR;
unsigned long            cTOT_SALTS;
unsigned long            cTOT_TONHHMM;
unsigned long            cTOT_E3;
unsigned long            cTOT_E6;
unsigned long            cTOT_NSER_P;
unsigned long            cTOT_IMPC_P;
unsigned long            cTOT_IMPS_P;
unsigned long            cTOT_IMPT_P;
unsigned long            cTOT_KMT_P;
unsigned long            cTOT_KMO_P;
unsigned long            cTOT_KML_P;
unsigned long            cTOT_KMLP_P;
unsigned long            cTOT_TOC_P;
unsigned long            cTOT_TON_P;
unsigned long            k;
unsigned char            nt[2];
T_FECHHOR                f_grab;
unsigned char            CCC[2];
unsigned short           visu_chk;
T_FECHHOR                visu_f_precambio;
unsigned short           visu_chk_prec;
unsigned short           check_programa;
unsigned short           check_tiquet;
unsigned long            nmodifs_k;
unsigned long            nmodifs_tar;
unsigned char            n_lic[4];
unsigned char            n_serie[4];
T_FECHHOR                f_exp;
T_FECHHOR                f_inst;
unsigned char            CCC_inst[2];
unsigned char            VERSION_TX[2];
unsigned short           ntar;
unsigned char            ledocup[4];
unsigned char            sduuummm[2];
unsigned long            bb;
unsigned long            m_ps;
unsigned long            seg_ps;
unsigned long            impkm_ss;
unsigned long            impho_ss;
unsigned char            ledapagar[4];
unsigned long            supl;
unsigned char            texte_nom[24];
unsigned char            texte_dni[16];
unsigned char            texte_llicencia[12];
unsigned char            texte_matricula[12];
T_FECHHOR                fecha_actual;
unsigned short           mp_version;
unsigned short           mp_check;
T_FECHHOR                mp_fecha;
unsigned char            texte_H1[24];
unsigned char            texte_H2[24];
unsigned char            texte_H3[24];
unsigned char            texte_H4[24];
unsigned char            texte_H5[24];
unsigned char            texte_H6[24];
unsigned char            texte_F1[24];
unsigned char            texte_F2[24];
unsigned char            texte_F3[24];
unsigned char            texte_F4[24];
unsigned char            texte_F5[24];
unsigned char            texte_F6[24];
unsigned long            cTOT_NTICK;
T_FECHHOR                fecha_borr_tots;
unsigned char            texte1[24];
unsigned char            texte2[24];
unsigned char            texte3[24];
unsigned char            texte4[24];
unsigned char            texte5[24];
unsigned char            texte6[24];
unsigned char            texte7[24];
unsigned char            texte8[24];
unsigned char            texte9[24];
unsigned char            texte10[24];
unsigned short           num_dia_torn;
unsigned short           diames_torn;
unsigned char            hora_inici_torn[2];
unsigned char            hora_final_torn[2];
unsigned char            fi_torn_guions;
unsigned char            hora_inici_descans[2];
unsigned char            inici_descans_guions;
unsigned char            hora_final_descans[2];
unsigned char            final_descans_guions;
unsigned char            buffer_insika[65];
unsigned char            dum_2b[2];
unsigned long            cTOT_BB;
unsigned long            cTOT_BB_P;
unsigned char            Texte_Document[24];
unsigned long            cTOT_KMOFF;
unsigned long            cTOT_KMOFF_P;
unsigned char            operari[8];
unsigned char            texte_H1_32[32];
unsigned char            texte_H2_32[32];
unsigned char            texte_H3_32[33];
unsigned char            texte_H4_32[32];
unsigned char            texte_H5_32[32];
unsigned char            texte_H6_32[32];
unsigned char            texte_F1_32[32];
unsigned char            texte_F2_32[32];
unsigned char            texte_F3_32[32];
unsigned char            texte_F4_32[32];
unsigned char            texte_F5_32[32];
unsigned char            texte_F6_32[32];
unsigned char            texte_nom_32[32];
} T_DREG_TCKS_TX30;

typedef struct S_DREG_CONTINUAR_TICKET {
unsigned short           num_ticket;
unsigned short           reg_index;
unsigned short           camp_fin_index;
unsigned short           taula_repeticio;
unsigned short           reg_repeticio;
unsigned short           camp_ini_index;
} T_DREG_CONTINUAR_TICKET;

typedef struct S_DREG_PRIMA_TORN {
unsigned long            num_torn;
T_FECHHOR                fecha_ini;
T_FECHHOR                fecha_fi;
unsigned long            num_serveis;
unsigned long            nreg_servei_ini;
unsigned long            num_conductor;
unsigned long            eur_km;
unsigned long            import_torn;
unsigned long            supl_torn;
unsigned long            total_torn;
unsigned long            km_oc_torn;
unsigned long            km_torn;
unsigned long            km_per_cent;
unsigned long            min_oc_torn;
unsigned long            min_torn;
unsigned long            min_per_cent;
T_FECHHOR                fecha_fi_torn_ant;
unsigned char            dum1_torn[2];
unsigned long            hm_libre_pasj_torn_tancat_prev;
unsigned long            km_torn_tancat_prev;
unsigned long            velmax_torn_tancat_prev;
unsigned long            hm_libre_pasj_fins_fi_torn;
unsigned long            km_fins_fi_torn;
unsigned long            velmax_fins_fi_torn;
unsigned long            hhmm_ini_tram_fi_torn;
unsigned long            neto_torn;
unsigned long            iva_torn;
float                    percent_iva0;
float                    percent_iva1;
float                    percent_iva2;
unsigned long            iva_0;
unsigned long            iva_1;
unsigned long            iva_2;
} T_DREG_PRIMA_TORN;

typedef struct S_DREG_PRIMA_SERVEI {
unsigned long            numero_servei;
unsigned long            servei_num_tick;
T_FECHHOR                servei_ini;
T_FECHHOR                servei_fi;
unsigned long            servei_import;
unsigned long            servei_total;
unsigned long            servei_supl;
unsigned long            servei_kmlib;
unsigned long            servei_kmoc;
unsigned long            servei_min;
unsigned long            servei_min_oc;
unsigned long            servei_velmax;
unsigned long            servei_velmaxlib;
unsigned long            servei_hm_libre_pasj;
unsigned char            servei_buf_tartrab[12];
unsigned long            servei_fet_ticket;
T_FECHHOR                servei_ini_lliure;
} T_DREG_PRIMA_SERVEI;

/////// Menus
#define MENU_0                           0
#define MENU_DUM                         2
#define MAX_NUM_MENUS                    2

/////// Pantallas
#define PAN_DUM                          1
#define MAX_NUM_PANTALLAS  2

/////// Textos de Sistema
#define T_KM                             0

/////// Conversiones
#define CONV_DUM                         1
#define MAX_NUM_CONVS  2
#endif
