// ***********************************************************************
// teclat.c
// ***********************************************************************

#include "rtos.h"
#include "iostr710.h"



#include "teclat.h"
#include "i2cifac.h"
#include "hard.h"
#include "rut30.h"
#include "debug.h"
#include "tx30.h"

#define TIMETEC0  3           // 3*10 ms PER MIRAR TECLAT
#define TIMETEC1  5           // 5*10 ms PER REBOT TECLAT
#define TIMESHIFT0  200       // 2"  per tecla shift
#define TIMETEC_LONG 200      // 2" per tecla '+ ' amb llarga pulsacio

#define BTEC_CODS 3     // teclat per cas desde  CARGADOR

const uchar def_teclat[][ETX30_FIN]={
//
// M  L  O  P  R  O  T  T  C  M  M  M  P  F  U  C  M  P
// A  I  C  A  E  F  S  O  O  T  R  F  A  I  P  E  T  R
// L  B  U  G  L  F  T  T  D  O  E  P  R  S     R  A  E
//
   0, 0, 1,11, 2, 0, 2, 2, 3, 4, 4, 4, 4, 1, 0, 0, 3, 3, // tecl paralel
   0, 6, 7,12, 2, 0, 2, 2, 3, 4, 4, 4, 4, 8, 8, 0, 3, 3, // tecl secuencial
   0,10, 5, 9, 2, 0, 2, 2, 3, 4, 4, 4, 4, 8, 8, 0, 3, 3, // tecl rotatori
};

const unsigned short   codif_teclat[13][48]={
//
// 0
// T1         T2            T3            T4            T5
// T6         T7            T8            T1+T2         T1+T3
// T1+T4      T1+T5         T1+T6         T1+T7         T1+T8
// T2+T3      T2+T4         T2+T5         T2+T6         T2+T7
// T2+T8      T3+T4         T3+T5         T3+T6         T3+T7
// T3+T8      T4+T5         T4+T6         T4+T7         T4+T8
// T5+T6      T5+T7         T5+T8         T6+T7         T6+T8
// T7+T8      soltar        T9(shift)     SHFT+T1       SHFT+T2
// SHFT+T3    SHFT+T4       SHFT+T5       SHFT+T6       SHFT+T7
// SHFT+T8    TEC8_LONG_50
  
/*  0  */
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TREL,       V_TTOT,       V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             V_TLLUM,
V_TAPAGAR_LLUM,V_TSOLTAR,   V_TCODS,      0,             0,
0,            0,            0,            0,             0,
0,            V_TPLUSPLUS,
/*  1  */
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TPAGAR,     V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,            V_TSOLTAR,    0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/*  2  */
/* ABORT RELOJ TEST TOTS */
0,
V_TABORT,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TREL,       V_TTOT,       V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/*  3  BTEC_CODS */
/* CODS y CARGADOR para codif codisecret */
0,
V_TABORT,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TNUM+32*4,  V_TNUM+32*5,  V_TNUM+32*6,  0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            V_TCODS,      0,             0,
0,            0,            0,            0,             0,
0,            0,
/*  4  */
/* MOD_TOT  MOD_REL */
0,
V_TABORT,     0,            0,            V_TCLR,        V_TSKIP,
V_TREL+32*4,  V_TTOT,       V_TINCR,      0,             0,   
// !!!!el 32*4 deu sobrar 
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/* 5  ocupat  rotatori supl paral com el 11 pero amb fin_vd2*/
0,
V_TLIBRE,     V_TROT+32*0,  V_TROTUP+32*1,V_TROTDWN+32*2,V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/*  6  */
0,
V_TLIBRE,     V_TUP,        0,            0,             0,
V_TREL,       V_TTOT,       0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TLLUM,
V_TAPAGAR_LLUM,0,           V_TCODS,      0,             0,
0,            0,            0,            0,             0,
0,            V_TPLUSPLUS,
/* 7   */
0,
V_TLIBRE,     V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TPAGAR,     V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/*  8  */
0,
0,            V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            0,            V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            V_TSOLTAR,    0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/* 9   pagar rotatori doble tecla para euros / cancel supl */
0,
0,            V_TROT+32*0,  V_TROTUP+32*1,V_TROTDWN+32*2,V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             0,
V_TNUM+32*4,  0,            0,            0,             0,
V_TNUM+32*5,  0,            0,            0,             V_TNUM+32*6,
0,            0,            V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
/* 10   lliure rotatori */
0,
V_TLIBRE,     V_TROT,       V_TROTUP,     V_TROTDWN,     0,
V_TREL,       V_TTOT,       0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TLLUM,
V_TAPAGAR_LLUM,0,           V_TCODS,      0,             0,
0,            0,            0,            0,             0,
0,            V_TPLUSPLUS,
//  11    
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
//  12    
0,
0,            V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,            0,             0,
0,            0,            0,            0,             0,
0,            0,
};

// cas TX40 no te tecla 9 (SHIFT)
//
// V_TAPAGAR_LLUM   passa de (T7+T8) a (T1+T8)
// V_TCODS          passa de (T9)    a (T7+T8)
const unsigned short codif_teclat_tx40[13][39]={
//
// 0
// T1         T2            T3            T4            T5
// T6         T7            T8            T1+T2         T1+T3
// T1+T4      T1+T5         T1+T6         T1+T7         T1+T8
// T2+T3      T2+T4         T2+T5         T2+T6         T2+T7
// T2+T8      T3+T4         T3+T5         T3+T6         T3+T7
// T3+T8      T4+T5         T4+T6         T4+T7         T4+T8
// T5+T6      T5+T7         T5+T8         T6+T7         T6+T8
// T7+T8      soltar        TEC8_LONG_40
  
/*  0  */
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TREL,       V_TTOT,       V_TSUMA,      0,             0,
0,            0,            0,            0,             V_TAPAGAR_LLUM,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             V_TLLUM,
V_TCODS,      V_TSOLTAR,    V_TPLUSPLUS,  
/*  1  */
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TPAGAR,     V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,  		  V_TSOLTAR,    0,
/*  2  */
/* ABORT RELOJ TEST TOTS */
0,
V_TABORT,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TREL,       V_TTOT,       V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,
/*  3  BTEC_CODS */
/* CODS y CARGADOR para codif codisecret */
0,
V_TABORT,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TNUM+32*4,  V_TNUM+32*5,  0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
V_TCODS,      0,            V_TNUM+32*6,
/*  4  */
/* MOD_TOT  MOD_REL */
0,
V_TABORT,     0,            0,            V_TCLR,        V_TSKIP,
V_TREL+32*4,  V_TTOT,       V_TINCR,      0,             0,   
// !!!!el 32*4 deu sobrar 
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,
/* 5  ocupat  rotatori supl paral com el 11 pero amb fin_vd2*/
0,
V_TLIBRE,     V_TROT+32*0,  V_TROTUP+32*1,V_TROTDWN+32*2,V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,            0,            0,
/*  6  */
0,
V_TLIBRE,     V_TUP,        0,            0,             0,
V_TREL,       V_TTOT,       0,            0,             0,
0,            0,            0,            0,             V_TAPAGAR_LLUM,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TLLUM,
V_TCODS,      0,            V_TPLUSPLUS,
/* 7   */
0,
V_TLIBRE,     V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
V_TPAGAR,     V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             0,
0,            0,            0,
/*  8  */
0,
0,            V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            0,            V_TSUMA,      0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            V_TSOLTAR,    0,
/* 9   pagar rotatori doble tecla para euros / cancel supl */
0,
0,            V_TROT+32*0,  V_TROTUP+32*1,V_TROTDWN+32*2,V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             0,
V_TNUM+32*4,  0,            0,            0,             0,
V_TNUM+32*5,  0,            0,            0,             V_TNUM+32*6,
0,            0,            V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,
/* 10   lliure rotatori */
0,
V_TLIBRE,     V_TROT,       V_TROTUP,     V_TROTDWN,     0,
V_TREL,       V_TTOT,       0,            0,             0,
0,            0,            0,            0,             V_TAPAGAR_LLUM,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             0,
0,            0,            0,            0,             V_TLLUM,
V_TCODS,      0,            V_TPLUSPLUS,
//  11    
0,
V_TLIBRE,     V_TNUM+32*0,  V_TNUM+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            V_TNUM+32*10,  V_TNUM+32*6,
0,            V_TNUM+32*11, V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,
//  12    
0,
0,            V_TUP+32*0,   V_TDWN+32*1,  V_TNUM+32*2,   V_TNUM+32*3,
0,            V_TSUPL,      V_TSUMA,      0,             0,
0,            0,            V_TCANCELSP,  0,             0,
0,            0,            0,            0,             V_TNUM+32*8,
V_TNUM+32*4,  0,            0,            0,             V_TNUM+32*9,
V_TNUM+32*5,  0,            0,            0,             V_TNUM+32*6,
0,            0,            V_TNUM+32*7,  0,             V_TEURO,
0,            V_TSOLTAR,    0,
};

void teclat_init(void)
{
   teclat.tpuls=0;
   teclat.timetec = TIMETEC0;
   teclat.timeshift = 0;      // no hi ha tecla shift
   teclat.time_longtec = 0;
}


void teclat_tic10ms(void)
{
  // s'ha de cridar cada 10ms
  if(teclat.timetec)
    --teclat.timetec;
  if(teclat.timeshift)
    --teclat.timeshift;
  if(teclat.time_longtec)
    teclat.time_longtec++;
}


const int ini_doble_tecla[]={0,9,16,22,27,31,34,36};


extern void ledDriver_dataRead(int numIc, unsigned char *buf, int num_c);

int scan_teclat(void)
{
unsigned char buftec[2],mask,i;  
int scan_code = 0;
    if(tipoTx==TX50)
    {
      ledDriver_dataRead(1,buftec,2);        
      // inclou teclas 7 i 8 a buftec[0]  
      buftec[0] = (buftec[0]& 0x3f) | ((buftec[1]<<6) & 0xc0);                
      if(buftec[0] & 0x01)
      {
          scan_code = 38;     // cas tecla 9
          return(scan_code);
      }
      if(IOPORT0_PD_bit.no15 == 0)
        buftec[0] |= 0x01;        // inclou tecla 0
    }
    else
    {
      // cas TX40
      buftec[0] = 0;
      if(IOPORT0_PD_bit.no15 == 1 )
        buftec[0] = 1;
      IOPORT0_PD |= 0x0038; //bits 3,4,5 = 1
      IOPORT0_PD &= 0xfff7; //bit 3 = 0 Row1
      delay_us(5); // per estabilitzar entrada
      if(!IOPORT0_PD_bit.no6) buftec[0] |= 0x04;
      if(!IOPORT0_PD_bit.no7) buftec[0] |= 0x02;
      IOPORT0_PD |= 0x0038; //bits 3,4,5 = 1
      IOPORT0_PD &= 0xffef; //bit 4 = 0 Row2
      delay_us(5); // per estabilitzar entrada
      if(!IOPORT0_PD_bit.no6) buftec[0] |= 0x08;
      if(!IOPORT0_PD_bit.no7) buftec[0] |= 0x10;
      if(!IOPORT0_PD_bit.no14) buftec[0] |= 0x20;
      IOPORT0_PD |= 0x0038; //bits 3,4,5 = 1
      IOPORT0_PD &= 0xffdf; //bit 5 = 0 Row3
      delay_us(5); // per estabilitzar entrada
      if(!IOPORT0_PD_bit.no7) buftec[0] |= 0x40;
      if(!IOPORT0_PD_bit.no14) buftec[0] |= 0x80;
    }
    for(mask = 0x01,i=1;mask;mask<<=1,i++)
      if(buftec[0] & mask)
      {
        scan_code = i;
        break;
      }
    if(teclat.timeshift)          
      scan_code += 38; // esta dins de temps tecla shift
    else
    {
     // no tecla shift.Mira doble tecla     
      for(mask <<= 1,i=0;mask;mask<<=1,i++)
        if(buftec[0] & mask)
        {
           scan_code = ini_doble_tecla[scan_code] + i;
           break;
        }
    }
  if(scan_code==0)
    scan_code=37;
  return scan_code;
}

int teclat_llegir(void){
	int tecla;
	static int tecla_ant=0x37;
	int ret = 0;
	static int shift = 0;
	
	tecla=scan_teclat();
	if(tecla != tecla_ant) {
		if(tecla == 37) {		//No hay tecla pulsada
			ret = ((tecla_ant == 7 || tecla_ant == 8) && shift == 0) ? tecla_ant : 0;
			teclat.time_longtec = 0;
			shift = 0;
		} else if(tecla > 8) {	//Doble tecla
			ret = tecla;
			shift=1;
		} else if(tecla > 6){	//Tecla shift o doble shift
			ret = 0;
		} else {
			ret = shift == 0 ? tecla : 0;
		}
		tecla_ant = tecla;
	} else {
		if(tecla == 8 && shift == 0) {
			if(teclat.time_longtec == 0)
			  teclat.time_longtec = 1; // per iniciar temps es mante tecla + pulsada
			else if(teclat.time_longtec >= TIMETEC_LONG) {
				ret = tipoTx==TX50 ? TEC8_LONG_50 : TEC8_LONG_40;
				teclat.time_longtec = 0;
				shift = 1;
			}
		}
	}
	if(ret)
        buzzer_set(BUZZER_TIME_TECLA);
	return ret;
}

// *********************************************************************************
//   event per teclat
// *********************************************************************************

unsigned char get_event_teclat(int mode_carregador,unsigned char*  tecla_num)
{ 
unsigned char ev = 0;
int aux;
int num_teclat;
 if (teclat.timetec==0)
    {
     aux=teclat_llegir();
     if (aux !=0)
        {
         if(mode_carregador)
           num_teclat = BTEC_CODS;   // teclat per passwords
         else
           num_teclat = def_teclat[tarcom.tipus_teclat][tx30.estat];
         if(tipoTx==TX50)
            aux = codif_teclat[num_teclat][aux];
         else
            aux = codif_teclat_tx40[num_teclat][aux];
         *tecla_num = aux >>5;
         ev = aux & 0x1f;
        }
    }
 return ev;
}

char buzzerHalf;
OS_TIMER buzzerTimer ;


void fin_timer_buzzer_tecla(void)
{
  buzzer_set(0);
}

void buzzer_init(void)
{
    TIM1_CR1_bit.OCAE = 0;
    OS_CreateTimer (&buzzerTimer, fin_timer_buzzer_tecla,1);
}

void buzzer_set(int ms)
{
  if(ms)
  {
    TIM1_OCAR = TIM1_CNTR + 0x100;  // per comen�ar inmediatament
//
    TIM1_CR1_bit.OLVLA = 1;
    TIM1_CR1_bit.OCAE = 1;
    buzzerHalf = 0;

    TIM1_CR1_bit.EN = 1;
    
    TIM1_CR2_bit.OCAIE = 1;   // activa interrupcio
    
    OS_SetTimerPeriod(&buzzerTimer, ms);
	OS_RetriggerTimer(&buzzerTimer);
    OS_StartTimer(&buzzerTimer);
  }
  else
  {
    OS_StopTimer(&buzzerTimer);
    
    TIM1_CR2_bit.OCAIE = 0; // desactiva interrupcio
    // deixa sortida a 0
    TIM1_CR1_bit.OLVLA = 0;
    TIM1_CR1_bit.FOLVA = 1;
    TIM1_CR1_bit.FOLVA = 0;
  }
}

