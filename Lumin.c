//******************************************************
// lumin.c    gestion luminoso TXD30
// compatible con luminosos nuevos version 7   01/07/2003
//******************************************************

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "intrinsics.h"

#include "lumin.h"
#include "tx30.h"
#include "serial.h"
#include "i2c.h"
#include "display.h"
#include "hard.h"
#include "multiplex.h"

#define TIMER_500MS   11    // cada 500ms. (10*50ms)
#define TIMEOUT_RESP_300MS  7    // 300ms.      (6*50ms)
#define CNT_REFR_0      20    // refresca cada 20 envios
#define CNT_REFR_FAST    1   // per refrescar rapit (1") despres de canvi

#define DECIMSEG_ERROR_LUMIN 	40   // decimes segon per treure tensio a lluminos

#define NUM_MAX_ERROR_SERIE	2	//numero de errores serie consecutivo para considerar error

#define LUMINOSO_OFF		0
#define LUMINOSO_ON		1


const unsigned char TAU_CODE[]={
0x04,0x32,0x89 ,0x52,0x8c,0xc6 ,0x1d,0x85,0xd9 ,0x9d,0x16,0x48 ,
0x74,0x1e,0x91 ,0xbb,0x5f,0xc2 ,0x3b,0x96,0xec ,0x22,0xb0,0x7c ,
0x34,0x71,0xd1 ,0x5d,0xb9,0x46 ,0xe3,0x79,0xcc ,0x0b,0x24,0xa8 ,
0x0f,0x93,0xf9 ,0x30,0x63,0x19 ,0x58,0xa4,0x7f ,0x55,0xe0,0x2d ,

0x06,0x9c,0xe7 ,0xb1,0x41,0x3e ,0xc8,0x7a,0x11 ,0x95,0x26,0xac ,
0xe5,0x56,0xfc ,0xb2,0x53,0xca ,0x35,0x80,0x9a ,0x38,0x4d,0xae ,
0x6e,0x29,0xea ,0xf7,0x9e,0x69 ,0x8d,0x23,0xd0 ,0x6c,0xdc,0x97 ,
0x72,0xc1,0x8e ,0x5b,0x75,0xba ,0x07,0x47,0x9f ,0x55,0xe0,0x2d ,
			   
0xd2,0x84,0xad ,0x3A,0x27,0x12 ,0xd2,0x84,0xad ,0xd2,0x84,0xad ,
0x88,0x67,0xaa ,0xd2,0x84,0xad ,0xd2,0x84,0xad ,0xd2,0x84,0xad ,
0xd2,0x84,0xad ,0x44,0x25,0x76 ,0x87,0xFE,0x2A ,0x37,0xF3,0xC5 ,
0x77,0x4F,0xF2 ,0x99,0x37,0x2C ,0xC5,0x5C,0x66 ,0x55,0xe0,0x2d ,
			    
0x88,0x67,0xAA ,0x13,0x13,0xA5 ,0x88,0x67,0xAA ,0x88,0x67,0xAA ,
0x88,0x67,0xAA ,0x88,0x67,0xAA ,0x88,0x67,0xAA ,0x88,0x67,0xAA ,
0x88,0x67,0xAA ,0x50,0x45,0xa9 ,0x13,0x31,0xd5 ,0x27,0x83,0xcd ,
0xce,0x1b,0x70 ,0xf4,0x94,0x6b ,0x76,0x15,0x90 ,0x55,0xe0,0x2d ,

};
			   
#define NUM_CODIGS (sizeof(TAU_CODE)/3)

const unsigned char TAU_RESP[]={
0x57,0x33,0xc5,0x83,0x64,0x28,0x5c,0xad,0x4e,0xc2,0x88,0x72,0xeb,0x95,0xe3,0x4a,
0xb4,0x26,0xa6,0x92,0x1c,0x9a,0xb8,0x44,0x8d,0xcf,0x86,0x52,0xa0,0xca,0x3d,0x4a,
0x73,0x55,0x73,0x73,0x07,0x73,0x73,0x73,0x73,0xe5,0xcd,0x31,0x68,0xe9,0x9f,0x4a,
0x07,0xf9,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x40,0x2f,0x6d,0x8c,0x35,0xd9,0x4a,

//0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,

0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};		

const unsigned char sv = 0x41;

__no_init t_luminoso luminoso;

unsigned char hay_error;
unsigned char cnt_errors;
unsigned char mask_luzex[2];
unsigned char hayTensionL6;	// se�al de potencia L6 activada si o no. Da alimentacion luminoso serie



void init_llums_IO_EXPANDER(void)
{
  write_i2c(IO_EXPANDER_LUCES,0x01,1,0);// sortida a 0    
  write_i2c(IO_EXPANDER_LUCES,0x03,1,(tipoTx==TX40)? "\x00" : "\x80");  // configure bit 7 as input    
}

unsigned char index_resp;


void treure_llums(char mask)
{
char imask=0;
  if(tipoTx==TX40)
  {
    if(luminoso.tipo_lumin & LLUMINOS_MONODRIVER)
    {
      // cas nomes te instalat un driver (Argelia)
      if(mask&0x01) imask|=0x10;  // L1 
      if(mask&0x02) imask|=0x40;  // L2
      if(mask&0x04) imask|=0x80;  // L3
    }
    else
    {
        if(mask&0x02) imask|=0x80;  // L2
        if(mask&0x04) imask|=0x01;  // L3
        if(mask&0x08) imask|=0x02;  // L4
        if(mask&0x10) imask|=0x04;  // L5
		if(mask&0x01) imask|=0x50;  // L1 + Piruli
		if(mask&0x20) imask|=0x28;  // L6 + Lluminos Intel.
    }
  }
  else
  {
    if(mask&0x20)imask|=0x01;  // L6
    if(mask&0x10)imask|=0x02;  // L5
    if(mask&0x08)imask|=0x04;  // L4
    if(mask&0x04)imask|=0x08;  // L3
    if(mask&0x02)imask|=0x10;  // L2
    if(mask&0x01)imask|=0x20;  // L1
  }
    write_i2c(IO_EXPANDER_LUCES,0x01,1,&imask);  // output
}


//
// retorn  
//        0 o.k.
//        1 error
//
char preguntar_llums(void)
{
char ret;
if(tipoTx==TX40)
  {
    wread_i2c(IO_EXPANDER_TX40,0x12,1,&ret);
    return ((ret & 1) ? 0x0 : 1);
  }
  else
  {
    wread_i2c(IO_EXPANDER_LUCES,0x00,1,&ret);
    return ((ret & 0x80) ? 0x0 : 1);
  }
}

void out_luzex_verif(char mask)
{
// treu llums paralel per verificacio
char imask=0;
  if(tipoTx==TX40)
  {
    if(luminoso.tipo_lumin & LLUMINOS_MONODRIVER)
    {
      // cas nomes te instalat un driver (Argelia)
      if(mask&0x01) imask|=0x10;  // L1 
      if(mask&0x02) imask|=0x40;  // L2
      if(mask&0x04) imask|=0x80;  // L3
    }
    else
    {
        if(mask&0x02) imask|=0x80;  // L2
        if(mask&0x04) imask|=0x01;  // L3
        if(mask&0x08) imask|=0x02;  // L4
        if(mask&0x10) imask|=0x04;  // L5
        if(mask&0x01) imask|=0x40;  // L1 
        if(mask&0x20) imask|=0x08;  // L6 
    }
  }
  else
  {
    if(mask&0x20)imask|=0x01;  // L6
    if(mask&0x10)imask|=0x02;  // L5
    if(mask&0x08)imask|=0x04;  // L4
    if(mask&0x04)imask|=0x08;  // L3
    if(mask&0x02)imask|=0x10;  // L2
    if(mask&0x01)imask|=0x20;  // L1
  }
    write_i2c(IO_EXPANDER_LUCES,0x01,1,&imask);  // output
}


void out_luzex_no_save(char mask, char index_env)
{
	////////////////////////////////////////
	// Actualizamos luminoso por TENSION
	////////////////////////////////////////

        treure_llums(mask);  // output
 
	////////////////////////////////////////
	// Actualizamos luminoso SERIE
	////////////////////////////////////////
	if((luminoso.tipo_lumin & LLUMINOS_SERIE)!=0)
	{
          // cas canal serie
          if (mask&0x20)
             hayTensionL6 = LUMINOSO_ON;
          else
             hayTensionL6 = LUMINOSO_OFF;

          luminoso.llums_a_enviar = index_env;
          luminoso.cnt_refr = CNT_REFR_FAST;  // per que tregui de seguida
	}
}

void out_luzex(char mask, char index_env)
{ 
   
 if((!tarcom.hay_bloq_luz_lib)||(luminoso.timer_apagat_per_error==0)||((luminoso.tipo_lumin & LLUMINOS_NO_VERIFICABLE)==1))
 {
      //  v 2.12  luminoso.timer_envio_serie = TIMER_500MS*10;
	out_luzex_no_save(mask, index_env);
 }
  

 mask_luzex[0] = mask;
 mask_luzex[1] = index_env;
 luminoso.tempor_test=0;
}

void out_luzex_restore(void)
{    
 out_luzex_no_save(mask_luzex[0], mask_luzex[1]);
}


extern void display_1_en_libre(int err);


//
// retorna estat lluminos
//
// retorn:
//        0           llums 0k.
//        ERR_LLUMS   error llums

unsigned char luminoso_get_error(void)
{
    if((tarcom.luces_ver == 0) || ((luminoso.tipo_lumin & LLUMINOS_NO_VERIFICABLE)==1)) // no verifica
      return(0);
    
    if(luminoso.serie_pendent_test_desde_pas_a_lliure)
    {
      luminoso.hay_error_luzex = 0;
      luminoso.habia_error_luzex = 0;
      return(0);
    }

    if(luminoso.cnt_dec_segon_desde_pas_a_lliure)
    {
    //!!!   luminoso.habia_error_luzex = 0;
    //!!!   return(0);
     return(luminoso.hay_error_luzex);
    }
    if(luminoso.timer_apagat_per_error)
        return(ERR_LLUMS);             // cas te el lluminos apagat per haver detectat error
    
    test_luzex(0);  // actualitza error
    
    return(luminoso.hay_error_luzex);
    
}

void set_error(char raw)
{
	if(raw==1) 
	  hay_error++;
	else if (++cnt_errors>=2)
		{
		cnt_errors=0;
		hay_error++;
		}
	if (hay_error>NUM_MAX_ERROR_SERIE )
	{
		hay_error=NUM_MAX_ERROR_SERIE;
	}
}

void reset_error(void)
{
	hay_error=0;
	cnt_errors=0;
}

unsigned char hay_error_serie(void)
{
  return(hay_error >= NUM_MAX_ERROR_SERIE);
}


/********************************************************************/
/* detecta llum exterior fos                                        */
/********************************************************************/
//
// accio:
//        actualitza variable luminoso.hay_error_luzex:
//            0           llums 0k.
//            ERR_LLUMS   error llums
//
void test_luzex(int treure_display)
{
unsigned char mask,i,es_serie;
int j;
unsigned char retorn = 0;
  
    if(tx30.estat != E_LIBRE)
      return;
    es_serie = (luminoso.tipo_lumin & LLUMINOS_SERIE)!= 0;
    if(!es_serie)
      {
            for(mask=0,i=1;i<0x40;i+=i)
              {
                  if(tarcom.luces_ver & i)
                  {
                      if(luminoso.tipo_lumin & LLUMINOS_TEST_AMB_ALIMENTACIO)
                           out_luzex_verif(i | 0x20);  // cas incorpora alimentacio (france)
                      else
                           out_luzex_verif(i);  
//                      for(j=0;j<500;j++)
//                        __no_operation();
                      if(preguntar_llums() != 0 )
                       {
                             mask=1;
                             break;
                       }
                      out_luzex_no_save(mask_luzex[0], 0);  // el segon parametre no s'utilitza ja que no es serie
                      for(j=0;j<9000;j++)
                        __no_operation();
                  }
              }
      }
    else
            mask = hay_error_serie();  // cas serie
    if(mask)
    {
        retorn = ERR_LLUMS;
        if(tarcom.hay_bloq_luz_lib)
        {
            luminoso.timer_apagat_per_error=DECIMSEG_ERROR_LUMIN;
            if (!es_serie)
                 out_luzex_no_save(0x00, 0);      // treu tensio lluminos ( el segon parametre no s'utilitza ja que no es serie)
        }
        else if(!es_serie)
        {
        out_luzex_restore();
        }
    }
    else if(!es_serie)
      out_luzex_restore();
    
    luminoso.hay_error_luzex = retorn;
    if(tx30.estat == E_LIBRE)
    {
      if((luminoso.hay_error_luzex == 0) && (luminoso.hay_error_luzex !=luminoso.habia_error_luzex))
      {
        luminoso.habia_error_luzex = 0;
        if(treure_display )
          display_1_en_libre(0); // treu rellotge/texte/visu_esp        
      }
      else if(luminoso.hay_error_luzex  && (luminoso.hay_error_luzex != luminoso.habia_error_luzex))
      {
        luminoso.habia_error_luzex = 1;
        if(treure_display )
          display(1,NF_TEXT6,&tarcom.text_error[ERR_LLUMS][0]);   // E-2
      }
    }
    else
      luminoso.habia_error_luzex = 0xff;  // per actualitzar quan passi a E_LIBRE    
 return;
}



char buf_env[4];


char es_codig_programat(void)
{
	char ret;
	int i,j;
	ret=0;
	for(i=j=0;i<NUM_CODIGS;i++,j+=3)
		{
		if(memcmp(&buf_env[1],&TAU_CODE[j],3)==0)
			{
			ret=1;
			break;
			}
		}
	return(ret);
}


//
// genera 3 bytes aleatoris.
// si algun byte es igual a 0x01 genera nou valor del byte
// si els tres bytes coincideixen amb algun valor de la taula de codigs
// genera nous valors per els tres bytes
//

unsigned long     G_AL=0x47b35aL;	 // INICIALITZA GENERADOR ALEATORI

void gen_alea(void)
{
char i_env;
unsigned char nibb;
unsigned long xor_b0_b23;
unsigned char mask_nibb;

  do{
	 for(i_env=1;i_env<4;i_env++)
		{
		do
		  {
		  for(mask_nibb=1,nibb=0;mask_nibb;mask_nibb<<=1)
			  {
			  // genera 8 bits random
			  xor_b0_b23=0;
			  if(G_AL & 0x1L)
				  {
				  xor_b0_b23=0x800000L;	// per fer xor amb bit 23
				  nibb|=mask_nibb;  // inclou bit  a byte generat
				  }
			  xor_b0_b23^=(G_AL & 0x800000L); // bit 0 XOR bit 23
			  G_AL=(G_AL>>1) | xor_b0_b23;   // actualitza shift register
			  }
		  }while(nibb==1);	 // si valor generat==1 repeteix byte
		buf_env[i_env]=nibb;
		}
	}while(es_codig_programat());  // repeteix generacio dels tres bytes
}


unsigned short const  taula_tipo_lumin[]=
{
  0x0000,                                                       //  paral�lel (saludes)
  LLUMINOS_NO_VERIFICABLE,                                      //  paral�lel no verificable
  LLUMINOS_SERIE,                                               //  serie (saludes) 
  LLUMINOS_DELAY_VERIFICACIO  | LLUMINOS_TEST_AMB_ALIMENTACIO,  //  paral�lel (fran�a)
  LLUMINOS_SERIE | LLUMINOS_MULTIPLEXAT,                        //  Taxitronic TL70
  LLUMINOS_MONODRIVER,                                          //  Lluminos Argelia                         
};


//
// ****************************************************************
// ****************************************************************
// funcions per recollir caracter arribat de lluminos
//
// cas  per canal serie 0
int lumin_get_char_mono(char *c)
{
    return(usart_extreure_char(USART_LUMIN, c));
}
// Lumin TL70
int lumin_get_char_multiplex(char *c)
{
    if(multiplex.msg_lum.proces_en_curs == 0)
      return(1);
    *c = multiplex.msg_lum.buf[multiplex.msg_lum.ichar_a_processar++];
    if(multiplex.msg_lum.ichar_a_processar >= multiplex.msg_lum.nc_rebuts)
    {
      multiplex.msg_lum.proces_en_curs = 0;
    }
    return(0);
}

//
// funcio virtual per agafar caracter de lluminos
// se li assigna una funcio segons el tipus de Lluminos
//
int (*lumin_get_char)(char* c) = lumin_get_char_mono;

//
// ****************************************************************
// ****************************************************************

extern void get_tarifa_lumin(char* luminosoTipo, char* llums_combi);

void luminoso_init(void)
{
char luminosoTipo, llums_combi;

    get_tarifa_lumin(&luminosoTipo, &llums_combi);
    luminoso.tipo_lumin = taula_tipo_lumin[luminosoTipo];
    luminoso.llums_combi = llums_combi;
    luminoso.timer_envio_serie = TIMER_500MS;
    luminoso.timeout_resp_serie = 0;
    luminoso.tempor_test = 0;
    luminoso.timer_apagat_per_error = 0;
    luminoso.hay_error_luzex = 0;
    luminoso.habia_error_luzex = 0xff;  // per actualitzar quan passi a E_LIBRE    
    luminoso.serie_pendent_test_desde_pas_a_lliure = 0;
    luminoso.cnt_refr = 0;
    cnt_errors=0;
    reset_error();
    
    init_llums_IO_EXPANDER();
    
    // inicialitza valor temps per fer test a partir de pas a lliure
    if(tarcom.luces_ver  &&
       (luminoso.tipo_lumin & LLUMINOS_DELAY_VERIFICACIO) &&  // hi ha d'haver delay
       ((luminoso.tipo_lumin & LLUMINOS_SERIE) == 0)&&        // ha de ser paralel
        ((luminoso.tipo_lumin & LLUMINOS_NO_VERIFICABLE)==0)) // i verificable 
         luminoso.valor_dec_segon_desde_pas_a_lliure = 30;
    else
         luminoso.valor_dec_segon_desde_pas_a_lliure = 0;
    luminoso.cnt_dec_segon_desde_pas_a_lliure = luminoso.valor_dec_segon_desde_pas_a_lliure;
    
    if((luminoso.tipo_lumin & LLUMINOS_SERIE)!= 0)
    {
      if(luminoso.tipo_lumin & LLUMINOS_MULTIPLEXAT)
      {
        //inicializo canal serie para luminoso serie TL70
        usart_install(USART_LUMIN,NULL);
        usart_hard(USART_LUMIN, MODO_232, 0, 1, 0);
        usart_init(USART_LUMIN, 38400, 7,1); // 38400 bauds, 8bit  parity, parity odd 
        lumin_get_char = lumin_get_char_multiplex;
      }
      else
      {
        //inicializo canal serie para luminoso serie
        usart_install(USART_LUMIN, NULL);
        usart_hard(USART_LUMIN, MODO_232, 0, 1, 0);
        usart_init(USART_LUMIN, 600, 7, 1); // 600 bauds, 8bit  parity, parity odd
        lumin_get_char = lumin_get_char_mono;
      }
    }
        
}

//
// envia a usart lumin segons sigui multiplexat o no
//
void lumin_enviar(char *buf_env, int nb)
{
  if(luminoso.tipo_lumin & LLUMINOS_MULTIPLEXAT)
  {
    multiplex_enviar(DEVICE_TX5240, PER_LUM, buf_env, nb);
  }
  else
    usart_enviar(USART_LUMIN,buf_env,nb);  
}


void luminoso_envio_serie(void)
{
	char envio_aleatori;
	static unsigned char llums_ant = 0xff;	 // PER QUE PRIMERA VEGADA NO CUADRIN
	
	luminoso.timer_envio_serie = TIMER_500MS;
	envio_aleatori=1;
	if(luminoso.llums_a_enviar!=llums_ant)
	{
		llums_ant = luminoso.llums_a_enviar;
		luminoso.cnt_refr = CNT_REFR_FAST;
	}
	if(luminoso.cnt_refr==0)
	{
		envio_aleatori=0;
		luminoso.cnt_refr = CNT_REFR_0;
	}
	if(envio_aleatori)			   
	{
		luminoso.cnt_refr--;
		index_resp=0x80;    // per indicar no ha de verificar resposta
		gen_alea();
	}
	else
	{
		index_resp = llums_ant;  // guarda index resposta
		memcpy(buf_env+1,&TAU_CODE[index_resp*3],3);
		
	}
	buf_env[0] = 0x01;    // caracter d'inici
	lumin_enviar(buf_env, 4);
	luminoso.timeout_resp_serie = TIMEOUT_RESP_300MS; //300 ms
}


void luminoso_test_resp_serie(void)
{
char ch;
unsigned char i;

  luminoso.timeout_resp_serie=0;

  if((lumin_get_char(&ch)!=0) && (hayTensionL6 == LUMINOSO_ON))
    {
    // cas time out
    set_error(0);    // error la segona vegada
    if(luminoso.serie_pendent_test_desde_pas_a_lliure)
      luminoso.serie_pendent_test_desde_pas_a_lliure--;
    return;
    }
  if((index_resp!=0x80) && (hayTensionL6 == LUMINOSO_ON))
   {
     // cas ha de verificar codig
     if(TAU_RESP[index_resp]!=ch)
         {
         // cas no cuadra
         set_error(1);
         }
     else
     {
        reset_error();
     }
    luminoso.serie_pendent_test_desde_pas_a_lliure = 0; 
   }
//  if (hayTensionL6 == LUMINOSO_OFF)
//  {
//        reset_error();
//  }
  // buida cua entrada (per evitar que si entres un caracter de mes, 
  // quedessin desfassat envio i resposta
  for(i=0;i<10;i++)  // per posar-hi limit
        if(lumin_get_char(&ch)!=0)
           break;
}

void luminoso_serie_main_loop(void)
{
  if(luminoso.tipo_lumin & LLUMINOS_SERIE)
  {
     // cas es lluminos serie
      if(luminoso.timer_envio_serie==1) 
         {
           luminoso_envio_serie();
         }
      if(luminoso.timeout_resp_serie==1) 
         {
           luminoso_test_resp_serie();
         }
  }
}


void lluminos_45ms_loop(void)
{
   if(tarcom.luces_ver && 
      ((luminoso.tipo_lumin & LLUMINOS_NO_VERIFICABLE)==0)) 
     {
      if(luminoso.timer_apagat_per_error==1)
             {
             // cas torna a encendre lluminos despres de error
             luminoso.timer_apagat_per_error=0;

             luminoso.cnt_dec_segon_desde_pas_a_lliure = luminoso.valor_dec_segon_desde_pas_a_lliure;

             out_luzex_restore();
             }
      else if((luminoso.timer_apagat_per_error==0)&&
              ( ((luminoso.tempor_test>=SEGONS_TEST_LUMIN)&&
                   (luminoso.cnt_dec_segon_desde_pas_a_lliure==0))||
                (luminoso.cnt_dec_segon_desde_pas_a_lliure == 1)))
               
              {
               luminoso.tempor_test=0;
               luminoso.cnt_dec_segon_desde_pas_a_lliure = 0;
               test_luzex(1);
              }
     }
}
