// *********************************************************************
// td30.c
// *********************************************************************

#include <stdlib.h>

#include "tx30.h"
#include "td30.h"
#include "serial.h"
#include "rut30.h"
#include "rutines.h"
#include "multiplex.h"
#include "hard.h"
#include "car30.h"
#include "pausa.h"
#include "sha256.h"
#include "Lumin.h"

struct s_item_transm
{
 unsigned char tipo;
 unsigned char length;
 void * adr;
};
 
struct s_transm
{
 unsigned char op;           /* primer caracter a enviar */
 unsigned char num_items;
 struct s_item_transm const * item;
};

struct s_sendTar  // per enviar a td30 dades tarifes
{
 short  ledocup;
 unsigned long bb;
 unsigned long m_ps;
 unsigned long seg_ps;
 unsigned long impkm_ss;
 unsigned long impho_ss;
 short  ledpag;
 unsigned long supl;
};

struct s_sendTar  sendTar;

// tipus ITem Transmisio

#define ITT_ASC    0
#define ITT_LONG   1
#define ITT_BCD    2
#define ITT_BCD2B  3
#define ITT_MINS   4   // unsigned int (minuts) a mmhh
#define ITT_LMINS  5   // ulong a mmhhh....h
#define ITT_1CHAR  6
#define ITT_ASC7   7  // ASCII posa bit 7 sempre a 1
#define ITT_INT    8
#define ITT_ESTAT  9
#define ITT_BIN2B  10
#define ITT_TOTALIZ_LONG  11
#define ITT_TOTALIZ_LMINS 12
#define ITT_TOTALIZ_METRES 13
#define ITT_PERCENT        14
#define ITT_PAUSA          15

unsigned long  data_dum=0L;
unsigned char  num_preg;


const struct s_item_transm   items_servicio[]=
{
 ITT_BCD ,1,&data_dum,                       //&tarcom.transm,
 ITT_LONG,6,&serv[0].import,
 ITT_LONG,4,&serv[0].supl_desgl[0],
 ITT_LONG,4,&serv[0].supl_desgl[1],
 ITT_LONG,4,&serv[0].supl_desgl[2],
 ITT_LONG,4,&serv[0].supl_desgl[3],
 ITT_LONG,6,&serv[0].supl_aut,
 ITT_BCD ,1,&serv[0].ninot,
 ITT_BIN2B,10,&serv[0].finicio.minute,
 ITT_BIN2B,10,&serv[0].ffin.minute,
 ITT_BCD ,1,&tarcom.NDEC,
 ITT_LONG,6,&serv[0].hm_oc,
 ITT_MINS ,4,&servei.minuts_lib,    //  tiempo entre serv. mmhh 
 ITT_LONG,4,&servei.hm_libre,
 ITT_LONG,4,&servei.velmaxlib,
 ITT_MINS,4,&servei.minuts_lib_pasj, //  tiempo entre serv. pasj mmhh 
 ITT_LONG,4,&servei.hm_libre_pasj,
 ITT_LONG,2,&servei.nsent,
 ITT_LONG,4,&servei.velmax,
 ITT_MINS ,4,&serv[0].minuts_oc,    //  tiempo ocup(sin a pagar) 
 ITT_BCD2B,2,(unsigned char *)&serv[0].tartrab+1,
 ITT_BCD2B,2,(unsigned char *)&serv[0].tartrab,
 ITT_BCD ,4,&data_dum,                      // ITT_MINS,4,&servei.minuts_lib_marcha, //  tiempo libre en marcha 
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab+3  ,    // tarifas 17-32
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab+2  ,    // tarifas 17-32
 ITT_LONG,6,&serv[0].bb,
 ITT_BCD, 1,&serv[0].d_t_sep,
 ITT_LONG,6,&serv[0].import_dist_temps[0],
 ITT_LONG,6,&serv[0].import_dist_temps[1],
 ITT_TOTALIZ_LONG,8,(void*)TOT_NSER,
 ITT_BCD ,1,&serv[0].envio_copia,
 ITT_BCD ,4,&data_dum,                      // ITT_MINS,4,&serv[0].minuts_ocup_marcha, //  tiempo ocupado en marcha 
 ITT_BCD ,1,&servei.es_precio_fijo,
 ITT_BCD ,1,&data_dum,
 ITT_LONG,8,&data_dum,
 ITT_LONG,6,&servei.total_euros, 
 ITT_PERCENT,4,&servei.percent_iva,
 ITT_LONG,6,&servei.import_iva,
 ITT_LONG,4,&serv[0].supl_desgl[3],
 ITT_LONG,4,&serv[0].supl_desgl[4],
 ITT_LONG,4,&serv[0].supl_desgl[5],
 ITT_LONG,4,&serv[0].supl_desgl[6],
 ITT_LONG,4,&serv[0].supl_desgl[7],
 ITT_LONG,4,&serv[0].supl_max,
};
                  
const struct s_item_transm items_lastserv[]=
{
 ITT_BCD ,1,"\x01",
 ITT_LONG,6,&serv[0].import,
 ITT_LONG,4,&serv[0].supl_desgl[0],
 ITT_LONG,4,&serv[0].supl_desgl[1],
 ITT_LONG,4,&serv[0].supl_desgl[2],
 ITT_LONG,4,&serv[0].supl_desgl[3],
 ITT_LONG,6,&serv[0].supl_aut,
 ITT_BCD ,1,&serv[0].ninot,
 ITT_BIN2B,10,&serv[0].finicio.minute,
 ITT_BIN2B,10,&serv[0].ffin.minute,
 ITT_BCD ,1,&tarcom.NDEC,
 ITT_LONG,6,&serv[0].hm_oc,
 ITT_MINS ,4,&servei.sav_minuts_lib       ,    //  tiempo entre serv. mmhh 
 ITT_LONG, 4,&servei.sav_hm_libre         ,
 ITT_LONG, 4,&servei.sav_velmaxlib        ,
 ITT_MINS, 4,&servei.sav_minuts_lib_pasj  , //  tiempo entre serv. pasj mmhh 
 ITT_LONG, 4,&servei.sav_hm_libre_pasj    ,
 ITT_LONG, 2,&servei.sav_nsent            ,
 ITT_LONG, 4,&servei.sav_velmax           ,
 ITT_MINS ,4,&serv[0].minuts_oc            ,    //  tiempo ocup(sin a pagar) 
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab+1  , // tarifas 1-16
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab  , // tarifas 1-16
 ITT_BCD ,4,&data_dum,                            // ITT_MINS, 4,&servei.sav_minuts_lib_marcha, //  tiempo libre en marcha 
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab+3  ,    // tarifas 17-32
 ITT_BCD2B,2,(unsigned char*)&serv[0].tartrab+2  ,    // tarifas 17-32
 ITT_LONG,6,&serv[0].bb,
 ITT_BCD, 1,&serv[0].d_t_sep,
 ITT_LONG,6,&serv[0].import_dist_temps[0],
 ITT_LONG,6,&serv[0].import_dist_temps[1],
 ITT_TOTALIZ_LONG,8,(void*)TOT_NSER,
 ITT_BCD ,1,&serv[0].envio_copia,
 ITT_BCD ,4,&data_dum,                            // ITT_MINS,4,&serv[0].minuts_ocup_marcha, //  tiempo ocupado en marcha 
 ITT_BCD ,1,&servei.es_precio_fijo,
 ITT_BCD ,1,&data_dum,
 ITT_LONG,8,&data_dum,
 ITT_LONG,6,&servei.total_euros, 
 ITT_PERCENT,4,&servei.percent_iva,
 ITT_LONG,6,&servei.import_iva, 
 ITT_LONG,4,&serv[0].supl_desgl[3],
 ITT_LONG,4,&serv[0].supl_desgl[4],
 ITT_LONG,4,&serv[0].supl_desgl[5],
 ITT_LONG,4,&serv[0].supl_desgl[6],
 ITT_LONG,4,&serv[0].supl_desgl[7],
 ITT_LONG,4,&serv[0].supl_max,
};

const struct s_item_transm items_par_ini[]=
{
  ITT_BCD , 1,"\x01",
  ITT_LONG, 5,&blqs.k,
  ITT_BCD2B,4,&blqs.nt,
  ITT_BCD2B,5,&blqs.f_grab,
  ITT_BCD2B,4,&blqs.CCC,
  ITT_BCD2B,4,&visu_chk,
  ITT_BCD2B,5,&blqs.n_lic,
  ITT_BCD2B,5,&blqs.n_serie,
  ITT_BCD2B,5,&blqs.f_exp,
  ITT_BCD2B,5,&blqs.f_inst,
  ITT_BCD2B,4,&blqs.CCC_inst,
  ITT_BCD2B,4,(void*)&VERSION_TX,
  ITT_ASC7,1,&tarcom.haybloc,
  ITT_BCD2B,6,&tarcom.serie,
  ITT_INT,  4,&tarcom.Id_clau_preu_ac,

};

const struct s_item_transm items_par_cont[]=
{
 ITT_ASC7,2,&sendTar.ledocup,
 ITT_LONG,5,&sendTar.bb,
 ITT_LONG,5,&sendTar.m_ps,
 ITT_LONG,5,&sendTar.seg_ps,
 ITT_LONG,5,&sendTar.impkm_ss,
 ITT_LONG,5,&sendTar.impho_ss,
 ITT_ASC7,2,&sendTar.ledpag,
 ITT_LONG,4,&sendTar.supl,
};

const struct s_item_transm items_equip_id[]=
{
  ITT_BCD , 1,&tipoTx,
  ITT_BCD2B,4,(void*)&VERSION_TX,
  ITT_BCD2B,5,&blqs.n_serie,
};

 
const struct s_item_transm items_pin[]=
{
  ITT_ASC , 1,&tx30.pin_status_resp,
  ITT_ASC,  4,tx30.pin_value,
};

const struct s_item_transm items_password[]=
{
 ITT_LONG,4,&tx30.num_conductor,
 ITT_LONG,4,&tx30.password_conductor,
};

const struct s_item_transm items_confi_prima[] =
{
ITT_LONG ,2,&data_dum,
};

const struct s_item_transm items_resp_tarifa[]=
{
  ITT_1CHAR,  2,&tecla_num   ,
};

const struct s_item_transm items_resp_bloq[]=
{
  ITT_ASC7,  1,&MASK_1     ,
};

const struct s_item_transm items_preg_ini[]=
{
  ITT_1CHAR,  2,&num_preg   ,
};



const struct s_item_transm items_preg_estado[]=
{
 ITT_ESTAT,3,&data_dum,
 ITT_BCD, 1, &sensors,
 ITT_TOTALIZ_METRES,10,(void*)TOT_KMT,
 ITT_PAUSA,1,&data_dum,
};

const struct s_item_transm items_preg_vmax[]=
{
 ITT_LONG,4,&servei.velmax,
};

const struct s_item_transm items_preg_haypas[]=
{
 ITT_1CHAR,2,&pasj,
};

const struct s_item_transm items_preg_totnserv[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_NSER,
};

const struct s_item_transm items_preg_totimport[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_IMPC,
};

const struct s_item_transm  items_preg_totsupl[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_IMPS,
};

const struct s_item_transm items_preg_totkmoc[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_KMO,
};

const struct s_item_transm items_preg_totkmtot[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_KMT,
};

const struct s_item_transm items_preg_totkmlpasj[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_KMLP,
};

const struct s_item_transm items_preg_tottoc[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_TOC,
};

const struct s_item_transm items_preg_totton[]=
{
 ITT_TOTALIZ_LONG,8,(void*)TOT_TON,
};

const struct s_item_transm items_preg_import[]=
{
 ITT_LONG,6,&serv[0].import,
 ITT_LONG,4,&serv[0].supl,
};

const struct s_item_transm items_preg_hini[]=
{
 ITT_BIN2B,10,&serv[0].finicio.minute,
};

const struct s_item_transm items_preg_hfin[]=
{
 ITT_BIN2B,10,&serv[0].ffin.minute,
};

const struct s_item_transm items_preg_kmoc[]=
{
 ITT_LONG,6,&serv[0].hm_oc,
};

const struct s_item_transm items_preg_toc[]=
{
 ITT_MINS,4,&serv[0].minuts_oc,
};

const struct s_item_transm items_preg_ntar[]=
{
 ITT_1CHAR,2,&serv[0].tarifa,
};

const struct s_item_transm items_preg_velo[]=
{
 ITT_LONG,4,&tx30.vel,
};

char rPreuAc;
extern int precio_acordado;

const struct s_item_transm items_resp_preu_a[] = {
	ITT_1CHAR,	2,	&rPreuAc,
	ITT_LONG,	6,	&precio_acordado,
};

const struct s_item_transm items_config_lumin[] = {
	ITT_ASC,	1, 	(void*)&sv,
};

const struct s_item_transm items_preg_hora[]=
{
  ITT_BCD2B,10,&gt.time.minute,
  ITT_BCD2B,2,&gt.time.sec,
};

const struct s_item_transm items_preg_turno[]=
{
	ITT_MINS ,4,&servei.sav_minuts_lib,					// tiempo entre serv. mmhh
	ITT_LONG, 4,&servei.sav_hm_libre,
	ITT_LONG, 4,&servei.sav_velmax,
	ITT_MINS, 4,&servei.sav_minuts_lib_pasj,			// tiempo entre serv. pasj mmhh
	ITT_LONG, 4,&servei.sav_hm_libre_pasj,
	ITT_LONG, 2,&servei.sav_nsent,
	ITT_LONG, 4,&data_dum,

//	ITT_MINS,	4,	&servei.minuts_lib,    //  tiempo entre serv. mmhh
//	ITT_LONG,	4,	&servei.hm_libre,
//	ITT_LONG,	4,	&servei.velmaxlib,
//	ITT_MINS,	4,	&servei.minuts_lib_pasj, //  tiempo entre serv. pasj mmhh
//	ITT_LONG,	4,	&servei.hm_libre_pasj,
//	ITT_LONG,	2,	&servei.nsent,
//	ITT_BCD,	4,	&data_dum,                // ITT_MINS,4,&servei.minuts_lib_marcha, //  tiempo libre en marcha
	
	ITT_TOTALIZ_LONG,8,(void*)TOT_KMT,
	ITT_TOTALIZ_LONG,8,(void*)TOT_KMO,
	ITT_TOTALIZ_LONG,8,(void*)TOT_KMLP,
	ITT_TOTALIZ_LONG,8,(void*)TOT_TOC,
	ITT_TOTALIZ_LONG,8,(void*)TOT_TON,
	ITT_TOTALIZ_LONG,8,(void*)TOT_NDESC ,
	ITT_TOTALIZ_LMINS,8,(void*)TOT_TDESC ,
	ITT_TOTALIZ_LONG,8,(void*)TOT_SALTS,
	ITT_TOTALIZ_LONG,8,(void*)TOT_TONHHMM,

	ITT_TOTALIZ_LONG,8,(void*)TOT_NSER   ,
	ITT_TOTALIZ_LONG,8,(void*)TOT_IMPC   ,
	ITT_TOTALIZ_LONG,8,(void*)TOT_IMPS   ,
	ITT_BCD2B,10,&gt.time.minute,
	ITT_LONG,4,&tx30.num_conductor,
};

const struct s_item_transm items_preg_params[]=
{
  ITT_BCD ,1,&data_dum,                       //&tarcom.transm,
  ITT_BCD ,1,&tarcom.NDEC,
  ITT_ASC,64,&tarcom.ledtar[0],   //!!!! 64 ????????
  ITT_BCD ,1,&tarcom.formddmm,
  ITT_BCD ,1,&tarcom.digtot,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_NSER   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPC   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPS   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPT   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMT    ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMO    ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KML    ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMLP   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_TOC    ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_TON    ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_NDESC  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_TDESC  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_BORR   ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_NSER_P ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPC_P ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPS_P ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_IMPT_P ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMT_P  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMO_P  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KML_P  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_KMLP_P ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_TOC_P  ].nt,
  ITT_BCD2B,2,&tarcom.disp_tot[TOT_TON_P  ].nt,
  ITT_BCD2B,8,&tarcom.tars_blq3,
  ITT_ASC7,1,&tarcom.haybloc,
  ITT_BCD2B,4,(void*)&VERSION_TX,
};

const struct s_item_transm items_preg_tots[]=
{
  ITT_TOTALIZ_LONG,8,(void*)TOT_NSER   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPC   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPS   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPT   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMT    ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMO    ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KML    ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMLP   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_TOC    ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_TON    ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_NDESC  ,
  ITT_TOTALIZ_LMINS,8,(void*)TOT_TDESC  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_BORR   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_NSER_P ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPC_P ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPS_P ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_IMPT_P ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMT_P  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMO_P  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KML_P  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMLP_P ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_TOC_P  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_TON_P  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_SALTS  ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_TONHHMM,
  ITT_TOTALIZ_LONG,8,(void*)TOT_CCAB   ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMOFF	 ,
  ITT_TOTALIZ_LONG,8,(void*)TOT_KMOFF_P,
};


//*********************************************
// opcions enviar_serie
//*********************************************
#define OPTR_ENVIAR    0x40
#define OPTR_ESP_RESP  0x80

// Num. transmisio per ir100 (eliminades)
/*
#define NTR_SERV       1
#define NTR_OPEN       2
#define NTR_CLOS       3
#define NTR_STAT       4
#define NTR_TOTS       5
#define NTR_CLOP       6
#define NTR_TOTS_VD    7
#define NTR_TSTIMPR    8
#define NTR_AVANPAP    9
#define NTR_PAR_INI   10
#define NTR_PAR_CONT  11
#define NTR_LASTSERV  12
#define NTR_PARAMS    13
#define NTR_PAR2_INI  14
*/
// transmisions per terminal
#define NTR_PREU_AC	  14
#define NTR_TARIF     15
#define NTR_BLOQ      16
#define NTR_RESET     17
#define NTR_PREG_INI  18
#define NTR_PREG_CONT 19
#define NTR_PIN       46
#define NTR_PASSWORD  47
#define NTR_CONFI_TORN 48
#define NTR_BOR_TOTSP  49
#define NTR_CONFIG_BTH	50


// num. pregunta cas preguntes terminal
#define NPREG_ESTAT   0
//      .....
//      .....
#define NPREG_PARAMS  19
#define NPREG_TOTS    20
#define NPREG_PARAMS_INI   24
#define NPREG_PARAMS_CONT  25
#define NPREG_EQUIP_ID     26
#define NPREG_LAST    26    //ultima  per control no passi de llarg






const  struct s_transm transm[]=
 {
    0, 0,NULL,
// transmissions per ir100 (eliminades)    
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
    0, 0,NULL,
	0, 0,NULL,
  // transmissions per terminal
  'A', 2,items_resp_preu_a	,	// NTR_PREU_AC (resposta)
  'M', 1,items_resp_tarifa       ,   // NTR_TARIF  (resposta)
  'N', 1,items_resp_bloq         ,   // NTR_BLOQ   (resposta)
  'O', 0,NULL                    ,   // NTR_RESET  (resposta)
  'K', 1,items_preg_ini          ,   // NTR_PREG_INI
  ' ', 4,items_preg_estado       ,   // NTR_PREG_CONT + NPREG_ESTAT    
  ' ', 1,items_preg_vmax         ,
  ' ', 1,items_preg_haypas       ,
  ' ', 1,items_preg_totnserv     ,
  ' ', 1,items_preg_totimport    ,
  ' ', 1,items_preg_totsupl      ,
  ' ', 1,items_preg_totkmoc      ,
  ' ', 1,items_preg_totkmtot     ,
  ' ', 1,items_preg_totkmlpasj   ,
  ' ', 1,items_preg_tottoc       ,
  ' ', 1,items_preg_totton       ,
  ' ', 2,items_preg_import       ,
  ' ', 1,items_preg_hini         ,
  ' ', 1,items_preg_hfin         ,
  ' ', 1,items_preg_kmoc         ,
  ' ', 1,items_preg_toc          ,
  ' ', 1,items_preg_ntar         ,
  ' ',44,items_servicio          ,
  ' ',21,items_preg_turno        ,
  ' ',30,items_preg_params       , // NTR_PREG_CONT + NPREG_PARAMS
  ' ',28,items_preg_tots         , // NTR_PREG_CONT + NPREG_TOTS
  ' ',44,items_lastserv          ,
  ' ',1 ,items_preg_velo         ,
  ' ',2 ,items_preg_hora         ,
  ' ',15,items_par_ini           ,    // NTR_PREG_CONT + NPREG_PARAMS_INI
  ' ', 8,items_par_cont          ,    // NTR_PREG_CONT + NPREG_PARAMS_CONT 
  ' ', 3,items_equip_id          ,
  'P', 2,items_pin               ,    // NTR_PIN
  'D', 2,items_password          ,    // NTR_PASSWORD
  'C', 1,items_confi_prima       ,    // NTR_CONFI_TORN
  'T', 0,NULL                    ,    // NTR_BOR_TOTSP (resposta)
  'J', 1,items_config_lumin	     ,    // NTR_CONFIG_BTH
 
 };



void transf_par(char i)
{
struct s_tarifa *  pt;
struct s_bloc_tar *  pbt;
 pt=&tarifa[i];
 pbt=&bloc_tar[i];
 sendTar.ledocup = pt->ledocup[0];
 sendTar.bb=pt->bb;
 sendTar.m_ps = (unsigned long)(pbt->m_ps*10.+.5);
 sendTar.seg_ps = (unsigned long)(pbt->seg_ps*10.+.5);
 sendTar.impkm_ss = (unsigned long)(pbt->impkm_ss*FKDEC[tarcom.NDEC]+.5);
 sendTar.impho_ss = (unsigned long)(pbt->impho_ss*FKDEC[tarcom.NDEC]+.5);
 sendTar.ledpag = tarifa[pt->b_t_pagar].ledocup[0];
 sendTar.supl=pt->supl;
}


struct s_missatge_td30 missatge_td30;



// ********************************************************
// inicialitza entrades i sortides amb td30
// ********************************************************
void td30_reset(void)
{
  missatge_td30.stat_mess = 0;
  missatge_td30.nc_rep = 0;
  missatge_td30.len_env = 0;
}

//
// ****************************************************************
// ****************************************************************
// funcions per recollir caracter arribat de td30
//
// cas no hi ha td30
int td30_get_char_null(char *c)
{
  return(1);
}

// cas td30 per canal serie 1
int td30_get_char_td30(char *c)
{
    return(usart_extreure_char(USART_TD30, c));
}
//  TD30tip_tl70
// TD30tip_blue
int td30_get_char_multiplex(char *c)
{
    if(multiplex.msg_txm.proces_en_curs == 0)
      return(1);
    *c = multiplex.msg_txm.buf[multiplex.msg_txm.ichar_a_processar++];
    if(multiplex.msg_txm.ichar_a_processar >= multiplex.msg_txm.nc_rebuts)
    {
      multiplex.msg_txm.proces_en_curs = 0;
    }
    return(0);
}

//
// funcio virtual per agafar caracter de td30
// se l'hi assigna una funcio segons el tipus de TD30
//
int (*td30_get_char)(char* c) = td30_get_char_null;

//
// ****************************************************************
// ****************************************************************
// funcions per enviar a td30
//

// cas no hi ha td30
void td30_enviar_null(char *buf_env, int nb)
{
  return;
}

// cas td30 per canal serie 1
void td30_enviar_td30(char *buf_env, int nb)
{
    usart_enviar(USART_TD30,buf_env,nb);
    return;
}

//  TD30tip_tl70
// TD30tip_blue
void td30_enviar_multiplex(char *buf_env, int nb)
{
    multiplex_enviar(multiplex.msg_txm.device, PER_TXM | 0x80, buf_env, nb);
    return;
}

//
// funcio virtual per enviar a td30
// se l'hi assigna una funcio segons el tipus de TD30
//
void (*td30_enviar)(char *buf_env, int nb) = td30_enviar_null;


// ********************************************************
// inicialitza  td30
// ********************************************************
void td30_init(char hi_ha_td30)
{
  switch(hi_ha_td30)
  {
      case 0:   // TD30tip_no
        multiplex_init(MULTIPLEX_NO,NULL,0);
        td30_get_char = td30_get_char_null;
        td30_enviar = td30_enviar_null;
        break;
      case 1:   // TD30tip_td30
        usart_install(USART_TD30,UART1_IRQHandler);
        usart_hard(USART_TD30, MODO_232, 0, 0, 0);
        usart_init(USART_TD30, 2400, 1,0); 
        td30_reset();
        multiplex_init(MULTIPLEX_NO, NULL,0);
        td30_get_char = td30_get_char_td30;
        td30_enviar = td30_enviar_td30;
        break;
      case 2:   // TD30tip_tl70
        multiplex_init(MULTIPLEX_TL70, tarcom.taula_claus, tarcom.id_claus);
        td30_get_char = td30_get_char_multiplex;
        td30_enviar = td30_enviar_multiplex;
        break;
      case 3:   // TD30tip_blue
        multiplex_init(MULTIPLEX_BLUETOOTH, tarcom.taula_claus, tarcom.id_claus);
        td30_get_char = td30_get_char_multiplex;
        td30_enviar = td30_enviar_multiplex;
        break;
      case 4:   // TD30tip_bt40
        multiplex_init(MULTIPLEX_BT40, tarcom.taula_claus, tarcom.id_claus);
        td30_get_char = td30_get_char_multiplex;
        td30_enviar = td30_enviar_multiplex;
        break;
      case 5:   // TD30tip_bt40_L
        multiplex_init(MULTIPLEX_BT40_L, tarcom.taula_claus, tarcom.id_claus);
        td30_get_char = td30_get_char_multiplex;
        td30_enviar = td30_enviar_multiplex;
        break;
  }
  
}


// *********************************************************
//  extreu missatge de cua recepcio canal serie terminal   *
// *********************************************************
 
char td30_get_missatge(void)
{
 int st;
 char c;
 
 st = td30_get_char(&c);  
 if(st)
   return(0);
 switch(missatge_td30.stat_mess)
      {
       case 0 :
         if(c==':')
            {
             missatge_td30.stat_mess=1;
             missatge_td30.nc_rep=0;
            }
         return(0);
       case 1:
         if(c==':')
            {
             missatge_td30.stat_mess=1;
             missatge_td30.nc_rep=0;
             return(0);
            }
         if(c=='\x0a')
           {
            missatge_td30.stat_mess=0;
            if(missatge_td30.nc_rep!=0)
               return(1);
            else
               return(0);
           }
         missatge_td30.buf_rep[missatge_td30.nc_rep++]=c;
         if(missatge_td30.nc_rep >= sizeof missatge_td30.buf_rep)
           {
            missatge_td30.stat_mess = 0;
           }
         return(0);
      }
    return(0);
}


// converteix estats de TX50 a estat TX30
// per que sigui compatible amb TD30
const struct s_convert_estat
{
unsigned char estat_real;
unsigned char estat_segons_retorn;
 char* var_retorn;
}taula_convert_estat[] = 
{
//			                                        estat	     TX50  TD30
E_MEM_MAL , E_MEM_MAL , NULL                             ,//   E_MEM_MAL      0    0
E_LIBRE   , E_LIBRE   , NULL                             ,//   E_LIBRE        1    1
E_OCUPADO , E_OCUPADO , NULL                             ,//   E_OCUPADO      2    2
E_PAGAR   , E_PAGAR   , NULL                             ,//   E_PAGAR        3    3
E_RELOJ   , 0         , (char*)&tx30.estat_retorn_reltots,//   E_RELOJ        4    4
E_OFF     , E_OFF     , NULL                             ,//   E_OFF          5    5
E_TEST    , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_TEST         6    6
E_TOTS    , 0         , (char*)&tx30.estat_retorn_reltots,//   E_TOTS         7    7
E_CODS    , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_CODS         8    8
E_MODTOT  , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_MODTOT       9    9
E_MODREL  , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_MODREL      10   10
E_MODFP   , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_MODFP       11   11
E_PARMS   , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_PARMS       12   12
E_FISERV+1, E_FISERV+1, NULL                             ,//   E_FISERV      13   14
E_UP+1    , E_LIBRE   , NULL                             ,//   E_UP          14   15
E_CERRADO-2,E_CERRADO-2,NULL                             ,//   E_CERRADO     15   13
E_MOD_TAR , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_MOD_TAR     16   --     Canviat a v2.16F (abans o assimilava a E_LIBRE)
E_PREPROG , 0         , (char*)&tx30.estat_retorn_cods   ,//   E_PREPROG     17   --     Afegit a v2.16F 
};

void posa_estat_a_buffer(char* buf)
{
char estat;
char baux;
    // modificacio a 2.16F per que no envii estat E_CERRADO fins que
   // s'hagi tancat realment el torn
   estat = tx30.estat;
   if((estat == E_CERRADO) && (tx30.subestat_torn == TORN_ESPERA_TANCAR))
      estat = E_LIBRE;
   buf[2] = taula_convert_estat[estat].estat_real | 0x80;
   // per reconvertir estat cas hagi de tornar estat retorn
   if(taula_convert_estat[estat].var_retorn != NULL)
     estat = *taula_convert_estat[estat].var_retorn;
   estat = taula_convert_estat[estat].estat_segons_retorn;  // per fer-ho compatible amb td30
   baux=bin_bcd(estat);
   bcd2btoasc(buf,&baux,2);
   // guarda estat real
 
}


// *******************************************************************
//   posa a buffer dades a enviar segons estructura transm[n_env]    *
// *******************************************************************
//
// A partir versio v 2.00 s'ha afegit checksum de la transmissio.
// Abans dels : s'inclou caracter DLE ( 0x10) per indicar que la transmissio te check
// Despres del caracter 0x0a de fi de trensmissio s'inclouen dos caracters ASCII
// corresponents al check-sum.
// primer caracter '0' a 'F' corresponent als 4 bits baixos del check-sum
// segon caracter '0' a 'F' corresponent als 4 bits alts.
// El check-sum es calcula com a CRC16 dels bytes transmessos desde DLE fins a 0x0a
// ambdos inclosos.
//
//
// Amb aquesta modificacio de la transmissio es mante compatibilitat entre versions
// antigues i noves tant del taximetre com del terminal.
//

void td30_respondre(unsigned char op)
{
unsigned short  iaux;
unsigned long  laux;
int nb,i;
unsigned char  j;
char  baux[10];
struct s_item_transm const *  pitem;
unsigned char * p;
struct s_transm const*  p_transm;
int st,nt;
unsigned short check_env;

 if(op&0x3f)
   {
    // cas preparar envio

    p_transm = &transm[op &0x3f];

    if(p_transm->op != ' ')
      {
       // cas bloc inicial
       missatge_td30.buf_env[0] = 0x10;  // DLE
       missatge_td30.buf_env[1] = ':';
       missatge_td30.buf_env[2] = p_transm->op;
       // canal_serie.len_rep=p_transm->len_rep;
       nb = 3;
      }
    else
      {
       nb = missatge_td30.len_env;
      }
    for(i=0;i<p_transm->num_items;i++)
      {
       pitem = &p_transm->item[i];
       switch (pitem->tipo)
          {
           case ITT_ASC:
           case ITT_ASC7:
             if(pitem->tipo==ITT_ASC7)
                baux[0]=0x80;
             else
                baux[0]=0;
             for(j=0,p=(unsigned char *)pitem->adr;j<pitem->length;j++)
                  missatge_td30.buf_env[nb+j]=(*p++)|baux[0];
             break;
           case ITT_LONG:
             ltobcd(*(unsigned long *)pitem->adr,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;
           case ITT_BCD:
             bcdtoasc(missatge_td30.buf_env+nb,(char *)pitem->adr,pitem->length);
             break;
           case ITT_BCD2B:
             bcd2btoasc(missatge_td30.buf_env+nb,(char *)pitem->adr,pitem->length);
             break;
           case ITT_MINS:
             iaux=*(unsigned short *)pitem->adr;
             if(iaux>5999)
                iaux=5999;  /* maximo 99horas 59 mins.  */
             baux[0]=bin_bcd((unsigned char)(iaux % 60));
             baux[1]=bin_bcd((unsigned char)(iaux / 60));
             bcd2btoasc(missatge_td30.buf_env+nb,baux,4);
             break;
           case ITT_1CHAR:
             baux[0]=bin_bcd(*(unsigned char *)pitem->adr);
             bcd2btoasc(missatge_td30.buf_env+nb,baux,2);
             break;
           case ITT_INT:
             laux=(unsigned long)(*(unsigned short *)pitem->adr);
             ltobcd(laux,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;
           case ITT_ESTAT:  
             posa_estat_a_buffer(missatge_td30.buf_env+nb);
             break;
           case ITT_BIN2B:    // passa  n chars ( de 0 a 99) a 2n caracters ascii
             bin2btoasc(missatge_td30.buf_env+nb,pitem->adr,pitem->length);
             break;
           case ITT_TOTALIZ_LONG:
             nt = (int)pitem->adr;
             laux = totalitzador_read(nt, &st);
             ltobcd(laux,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;
           case ITT_TOTALIZ_LMINS:
             nt = (int)pitem->adr;
             laux = totalitzador_read(nt, &st);
             laux=(laux/60)*100L+(laux%60);
             ltobcd(laux,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;
          case ITT_TOTALIZ_METRES:
             nt = (int)pitem->adr;
             laux = totalitzador_read(nt, &st)*100;
             if(tarcom.pmpk0>0)
                    laux+=((100*((unsigned long)tarcom.pmpk0-(unsigned long)pk_tot_tot))/(unsigned long)tarcom.pmpk0);
             ltobcd(laux,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;
          case ITT_PERCENT:
             laux = (unsigned long)((*(float *)pitem->adr) * 100.);
             ltobcd(laux,baux);
             bcdtoasc(missatge_td30.buf_env+nb,baux,pitem->length);
             break;                        
           case ITT_PAUSA:  
             missatge_td30.buf_env[nb] = pausa.estat +'0';
             break;
          }
       nb+=pitem->length;
      }
    missatge_td30.len_env=nb;  // per si ha de continuar amb una altre crida
   }

if(op & OPTR_ENVIAR)
   {
    // cas iniciar envio
    missatge_td30.buf_env[nb++]='\x0a';  /* line feed final */
    check_env = calcula_crc((unsigned char *)missatge_td30.buf_env, nb);
    bcd2btoasc(missatge_td30.buf_env+nb,(char*)&check_env,4);
    nb += 4;
        
    td30_enviar(missatge_td30.buf_env,nb);
   }
}

unsigned char  tipo_inf[2];

unsigned char tipo_inf_to_char(void)
{
unsigned char  aux;
 if((tipo_inf[0]>='0')&&(tipo_inf[0]<='9'))
    aux=(tipo_inf[0]& 0x0f)*10;
 else
    return(255);
 if((tipo_inf[1]>='0')&&(tipo_inf[1]<='9'))
   {
    aux+=(tipo_inf[1]&0x0f);
    return(aux);
   }
 else
    return(255);
}

extern void set_precio_acordado(int precio);

unsigned char td30_get_transm(void)
{
unsigned char event;
unsigned char  aux;
int len;
   
 event = 0;
 if(td30_get_missatge())
   {
    tipo_inf[0]=missatge_td30.buf_rep[2];
    tipo_inf[1]=missatge_td30.buf_rep[1];
    num_preg=tipo_inf_to_char();
    switch(missatge_td30.buf_rep[0])
      {
       case 'L':
         if((tx30.estat == E_LIBRE)&& (tarcom.pot_apagar_lluminos))
         {
           if(missatge_td30.buf_rep[1] == '0')
             tx30.lluminos_apagat = 0;
           else
             tx30.lluminos_apagat = 1;
           APAGAR_LLUMINOS();
         }
         break;
       case 'K':
         if(num_preg <= NPREG_LAST)
           {
            switch(num_preg)
              {
               case NPREG_PARAMS_CONT:
                  tipo_inf[0]=missatge_td30.buf_rep[4];
                  tipo_inf[1]=missatge_td30.buf_rep[3];
                  aux=tipo_inf_to_char();
                  transf_par(aux);
                  break;
			   case NPREG_TOTS:
				  
               default:
                  break;
              }            
            td30_respondre(NTR_PREG_INI);
            td30_respondre(NTR_PREG_CONT+num_preg | OPTR_ENVIAR);
#if DBG_BTH
			if(num_preg == NPREG_EQUIP_ID)
			{
			debug_buf_UARTC_BTH("***",3);
			debug_buf_UARTC_BTH(missatge_td30.buf_rep,5);
			}
#endif

           }
         break;
       case 'M':
         tecla_num=num_preg;
         td30_respondre(NTR_TARIF | OPTR_ENVIAR);
         event=V_TR_TARI;
         break;
       case 'N':  // caso bloqueo/desbl. transiciones
         MASK_1=missatge_td30.buf_rep[1];
           td30_respondre(NTR_BLOQ | OPTR_ENVIAR);
         break;
       case 'O': // reset interservei
           reset_interserv();
           td30_respondre(NTR_RESET | OPTR_ENVIAR);
           break;
       case 'P': // peticio pin 
         if((tx30.estat == E_LIBRE) && (tx30.pin_subestat == 0)&& hay_cargador())
           {
            event=V_PETICIO_PIN;
            tx30.pin_status_resp = '1';
           }
         else
            tx30.pin_status_resp = '0';           
         break;
      case 'C': // configuracions
        switch(num_preg){
          case 0:
            // torn prima i password
            configs_desde_bg40.hi_ha_torn_prima_bg40 = (missatge_td30.buf_rep[3] == '1')? 1 : 0;
            configs_desde_bg40.hi_ha_password_cond = (missatge_td30.buf_rep[4] == '1')? 1 : 0;
            configs_desde_bg40.hi_ha_pausa_activa = (missatge_td30.buf_rep[5] == '1')? 1 : 0;
            configs_desde_bg40.select_iva_teclat = (missatge_td30.buf_rep[6] == '1')? 1 : 0;
			missatge_td30.buf_rep[9] = 0;
            configs_desde_bg40.segons_pausa_passiva = atoi(&missatge_td30.buf_rep[7]) * 60;  // ho passa de minuts a segons
            td30_respondre(NTR_CONFI_TORN | OPTR_ENVIAR);
            break;
        }
        break;
      case 'D': // resposta a password conductor
        if(missatge_td30.nc_rep <2)
          break;
        if(missatge_td30.buf_rep[1] == '1')
        {
            event = V_ACK_PASSWORD;
            memset(tx30.nom_conductor, ' ', sizeof tx30.nom_conductor);
            len = min(sizeof tx30.nom_conductor, missatge_td30.nc_rep - 2);
            memcpy(tx30.nom_conductor, missatge_td30.buf_rep+2, len);
        }
        else
            event = V_NACK_PASSWORD;
        break;  
       case 'T': // borrat totalitzadors parcials
           borrar_tots_parcials();
           td30_respondre(NTR_BOR_TOTSP | OPTR_ENVIAR);
           break;
	   case 'J':
		   td30_respondre(NTR_CONFIG_BTH | OPTR_ENVIAR);
		   break;
	   case 'A':
			{
				char simport[7];
				char ts[9];
				int import, time;
				char sha[48];
				char baux[48];
				int sec;
				int nc = getNumCampos(missatge_td30.buf_rep, missatge_td30.nc_rep);
				if (nc < 4){
					set_precio_acordado(0);
					rPreuAc = 0;
				} else {
					get_string_sep_comas(missatge_td30.buf_rep, simport, 1, 6, missatge_td30.nc_rep);
					get_string_sep_comas(missatge_td30.buf_rep, ts, 2, 8, missatge_td30.nc_rep);
					import = atoi(simport);
					decodB64((unsigned char *)&time, ts, strlen(ts));
					T_FECHHOR data_aux;
					time_to_fechhor(&data_aux,&gt.time);
					sec = getSegundosFromFecha(&data_aux);
					if(diff((sec), time) > 3600){
						rPreuAc = 1;
					} else {
						memset(baux, 0 ,sizeof(baux));
						strcat(baux, simport);
						strcat(baux, ts);
						memset(sha,0,sizeof(sha));
						hmac_sha256((unsigned char *)&sha, (unsigned char *)&baux, strlen(baux), (unsigned char *)tarcom.clau_preu_ac, tarcom.Id_clau_preu_ac ? 64 : 13);
						memset(baux, 0 ,sizeof(baux));
						int nb=base64((unsigned char *)sha, baux, 32);
						memset(sha,0,sizeof(sha));
						get_string_sep_comas(missatge_td30.buf_rep, sha, 3, 48, missatge_td30.nc_rep);
						if(memcmp(sha, baux, nb) != 0){
							rPreuAc = 2;
						} else {
							set_precio_acordado(import);
							rPreuAc = 3;
						}
					}
				}
				td30_respondre(NTR_PREU_AC | OPTR_ENVIAR);
			}
			break;
       default:
         break;
      }
   }
 return(event);
}

void solicitaGps(void){
	multiplex_enviar(multiplex.msg_txm.device, PER_GPS, "4", 1);
}
// cridat desde sem30.c al acabar d'entrar pin

void enviar_resposta_pin(void)
{
int i;
  for(i=0;i<4;i++)
    tx30.pin_value[i] |= 0x30;
  td30_respondre(NTR_PIN | OPTR_ENVIAR);
}

// cridat desde sem30.c

void enviar_pregunta_password(void)
{
      td30_respondre(NTR_PASSWORD | OPTR_ENVIAR);
}