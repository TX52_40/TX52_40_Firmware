// xmlassoc.c


#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>


//#include "stream.h"
#include "rutines.h"

#include "xmlassoc.h"

#define __min(a,b)  (((a) < (b)) ? (a) : (b))


typedef struct
{
	int element_inici;
	int estat;
	char* addr_dades;
	int index_array_dades;
	char last_nom_array[XML_MAX_LEN_NOM+1];
}XML_Cadena;

typedef struct
{
	XML_Parser m_parser;
	XML_DefElement const* m_taula_def;
	char *m_desti;
	int m_index_cadena;
	XML_Cadena m_cadena[XML_MAX_LEVELS];
	int m_skip_nivell;
	int m_agafar_dades;
	int m_index_dades;
	char m_sumar_dades;
}Assoc;

Assoc assoc;

#define parser  (assoc.m_parser)
#define taula_def (assoc.m_taula_def)
#define desti (assoc.m_desti)
#define index_cadena (assoc.m_index_cadena)
#define cadena  (assoc.m_cadena)
#define skip_nivell  (assoc.m_skip_nivell)
#define agafar_dades  (assoc.m_agafar_dades)
#define index_dades  (assoc.m_index_dades)
#define sumar_dades  (assoc.m_sumar_dades)



int Depth;
int tarifaCurta;
int check_dades; 

void get_token(const char* buf, char* token)
{
	int ini,fin,len0,len;
	len0 = len = strlen(buf);
	for(ini=0;ini<len0;ini++,len--)
		if(buf[ini] != ' ')
			break;
	for(fin=len0-1;len;fin--,len--)
		if(buf[fin] != ' ')
			break;
	if(len)
		memcpy(token,buf+ini,len);
	token[len]=0;
}
void get_data_token(const char* buf,int len, char* token)
{
	int ini,fin,len0;
	char c;
	len0= len;
	for(ini=0;ini<len0;ini++,len--)
	{
		c = buf[ini];
		if(( c!= ' ')&& (c != '\x09')&&(c != '\x0a')&&(c != '\x0d'))
			break;
	}
	for(fin=len0-1;len;fin--,len--)
	{
		c = buf[fin];
		if(( c!= ' ')&& (c != '\x09')&&(c != '\x0a')&&(c != '\x0d'))
			break;
	}
	if(len)
		memcpy(token,buf+ini,len);
	token[len]=0;
}

void freeattr(const char **attr){
	int i;
	for (i = 0; attr[i]; i += 2)
	{
		free((void*)attr[i]);
	}
}

void start(const char *el, int chk)
{
	char token[51];
	int index,trobat;
	
	get_token(el,token);
	
	// mira si s'ha d'incloure a check-sum
	// independenment que s�hagi trobat a la llista de camps o no. (cas s'hagi afegit a la tarifa posteriorment)
	// es suposa que els camps nomes tenen valors si son assignables
	sumar_dades = chk;
	
	// busca nom dins taula def
	if(skip_nivell)
	{
		skip_nivell++;
		return;
	}
	index = cadena[index_cadena].element_inici;
	while(1)
	{
		if(taula_def[index].nivell < 0)
		{trobat =0;break;}// final taula
		if(taula_def[index].nivell < index_cadena)
		{trobat =0;break;}// final taula
		if(taula_def[index].nivell > index_cadena)
		{
			index++;
			continue;
		}
		if(strcmp(token, (tarifaCurta) ? taula_def[index].nomCurt : taula_def[index].nom) == 0)
		{
			trobat = 1;
			break;
		}
		index++;
	}
	if(trobat ==0)
	{
		skip_nivell= 1;
		return;
	}
	// cas trobat
	if(taula_def[index].assignable)
	{
		agafar_dades = 1;
		index_dades = index;
	}
	else
	{
		cadena[index_cadena+1].element_inici = index+1;
		cadena[index_cadena+1].estat = 0;
		cadena[index_cadena+1].last_nom_array[0] = 0;
		if(taula_def[index].tipus == TXML_VECTOR)
		{
			if(strcmp(cadena[index_cadena].last_nom_array,taula_def[index].nom)!= 0)
			{
				// cas primer element de l'array
				strcpy(cadena[index_cadena].last_nom_array,taula_def[index].nom);
				cadena[index_cadena+1].addr_dades = cadena[index_cadena].addr_dades + taula_def[index].element_offset;
			}
			else
			{
				// cas seguent element
				cadena[index_cadena+1].addr_dades += taula_def[index].element_size;
			}
		}
		else
		{
			cadena[index_cadena].last_nom_array[0] = 0;  // reset nom array
			cadena[index_cadena+1].addr_dades = cadena[index_cadena].addr_dades + taula_def[index].element_offset;
		}
		index_cadena++;
		
	}
	return;
}

void end(const char *el)
{
	char token[51];
	
	get_token(el,token);
	
	if(skip_nivell){
		skip_nivell--;
		return;
	}
	
	if(agafar_dades)
		agafar_dades = 0;
	else
	{
		cadena[index_cadena].last_nom_array[0] = 0;  // reset nom array
		index_cadena--;
	}
	
	sumar_dades = 0;
}


void saveValue(char * value){
	char caux;
	short saux;
	long laux;
	float faux;
	char * next_token;
	char* p_token;
	int i,bytes_despl;
	int pot_repetir;
	int len_string,len_copy;
	
	char token[200];
	get_data_token(value,strlen(value),token);
	
	if(sumar_dades)
	{
		for(i=0;i<strlen(token);i++)
			check_dades += (unsigned short)token[i];
		
	}
	if(skip_nivell) return;
	pot_repetir = 1;
	for(i=0,p_token = value,bytes_despl = 0;pot_repetir && (i<taula_def[index_dades].repe);i++,p_token = next_token)
	{
		switch(taula_def[index_dades].tipus)
		{
		case TXML_CHAR:
			caux = (char)strtoul(p_token, &next_token, 0);
			*(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset+bytes_despl) = caux;
			bytes_despl += sizeof(char);
			break;
		case TXML_SHORT:
			saux = (short)strtoul(p_token, &next_token, 0);
			*(short*)(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset+bytes_despl) = saux;
			bytes_despl += sizeof(short);
			break;
		case TXML_LONG:
			laux = (long)strtoul(p_token, &next_token, 0);
			*(long*)(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset+bytes_despl) = laux;
			bytes_despl += sizeof(long);
			break;
		case TXML_FLOAT:
			faux = (float)strtod(p_token, &next_token);
			*(float*)(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset+bytes_despl) = faux;
			bytes_despl += sizeof(float);
			break;
		case TXML_STRING:
			// elimina " d'inici i final si hi son
			if(p_token[0] == '"')
				p_token++;
			len_string = strlen(p_token);
			if(len_string)
				if(p_token[len_string-1] == '"')
					p_token[len_string-1] = 0;
			memset(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset,0,taula_def[index_dades].element_size);
			len_copy = __min(len_string,taula_def[index_dades].element_size);
			memcpy(cadena[index_cadena].addr_dades+taula_def[index_dades].element_offset,p_token,len_copy);
			pot_repetir = 0;
			break;
			
		}
	}
}

// defineix taula d'assignacions i adre�a inicial
void  XML_SetDefAssoc(XML_DefElement const * p_taula_def,char *p_desti)
{
	taula_def = p_taula_def;
	desti = p_desti;
	skip_nivell = 0;
	agafar_dades = 0;
	index_cadena = 0;
	sumar_dades = 0;
	check_dades = 0;
	cadena[0].element_inici = 0;
	cadena[0].last_nom_array[0] = 0;
	cadena[0].addr_dades = p_desti;
}

int XML_Par(const char *s, int len){
	int state=0;
	char ch;
	char tag[64];
	char value[256];
	char attName[16];
	int ptag;
	int pvalue;
	int pattr;
	int checksum;
	
	while(len){
		ch=*s++;
		if(ch==0xff)
			break;
		switch (state){
		case 0:		//espera caracter inicio tag
			if(ch=='<'){
				ptag=0;
				pvalue=0;
				state=1;
				checksum=0;
			}
			break;
		case 1:		//lee TAG
			if(ch=='/'){
				state=(ptag==0)? 7 : 8;
				break;
			} else if(ch==' '){
				state=2;
				tag[ptag++]=0;
				pattr=0;
			} else if(ch=='>'){
				tag[ptag++]=0;
				start(tag, checksum);
				state=4;
			} else
				tag[ptag++]=ch;
			break;
		case 2:		//busca atributos
			if(ch=='>') {
				start(tag, checksum);
				state=4;
			} else if(ch=='=') {
				if(memcmp(attName,"ck",pattr)==0){
					state=3;
					checksum=1;
				}
			} else if(ch==' ') {
				pattr=0;
			} else if(ch=='/') {
				state=8;
				break;
			} else {
				attName[pattr++]=ch;
			}
			break;
		case 3:		//busca el caracter de final del TAG
			if(ch=='>'){
				start(tag, checksum);
				state=4;
			}
			break;
		case 4:		//Obtiene el valor
			if(ch=='<'){
				value[pvalue++]=0;
				state=5;
			}
			else
				if(ch!=0x0d && ch!=0x0a)
					value[pvalue++]=ch;
			break;
		case 5:		//Busca final del tag
			if(ch=='/')
				state=6;
			else{
				tag[0]=ch;
				ptag=1;
				pvalue=0;
				state=1;
				checksum=0;
			}
			break;
		case 6:
			if(ch=='>'){
				saveValue(value);
				end(tag);
				ptag=0;
				pvalue=0;
				state=0;
				checksum=0;
			}
			break;
		case 7:
			if(ch=='>'){
				tag[ptag++]=0;
				end(tag);
				ptag=0;
				pvalue=0;
				state=0;
				checksum=0;
			}
			else
				tag[ptag++]=ch;
			break;
		case 8:
			if(ch=='>'){
				ptag=0;
				pvalue=0;
				state=0;
				checksum=0;
			}
			break;
		}
		len--;
	}
	return 1;
}

int XML_Associar(char* buff_arxiu,int len_arxiu,XML_DefElement const* taula,char* pdesti)
{
	int st;
	
	XML_SetDefAssoc(taula, pdesti);
	
	tarifaCurta = (buff_arxiu[1]=='C') ? 1 : 0;
	st = XML_Par(buff_arxiu, len_arxiu);
	st = check_dades; 
	return(st);
}
