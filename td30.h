// *********************************************************************
// td30.h
// *********************************************************************

#ifndef _INC_TD30
#define _INC_TD30



struct s_missatge_td30
{
 // recepcio 
 char stat_mess;
 int nc_rep;
 char buf_rep[128];  // la resposta mes llarga correspon a :D
 // envio
 int len_env;
 char buf_env[512];
};


extern struct s_missatge_td30 missatge_td30;


extern void td30_init(char hi_ha_td30);
extern unsigned char td30_get_transm(void);
extern void (*td30_enviar)(char *buf_env, int nb);
extern void solicitaGps(void);

#endif