//************************************************************
// aplic.h
//************************************************************

extern T_DREG_SERVEI*               reg_servei;
extern T_DREG_MONEDA*               reg_moneda;
extern T_DREG_CONTINUAR_TICKET*     reg_continuar_ticket;
extern T_DREG_TCKS_TX30*	    	reg_tcks_tx30;
extern T_DREG_PRIMA_TORN*           reg_prima_torn;
extern T_DREG_PRIMA_TORN*           reg_prima_torn_aux;
extern T_DREG_PRIMA_SERVEI*         reg_prima_servei;
extern T_DREG_PRIMA_SERVEI*         reg_prima_servei_aux;
