// *********************************************************************
// multiplex.h
// *********************************************************************

#ifndef __MULTIPLEX_INC
#define __MULTIPLEX_INC

#define MULTIPLEX_NO        0
#define MULTIPLEX_TL70      1
#define MULTIPLEX_BLUETOOTH 2
#define MULTIPLEX_BT40      3
#define MULTIPLEX_BT40_L    4


#define MPX_LEN_KEY  16

#define MPX_LEN_MIN     3 // 1 device + 1 perif  + 1 check
#define MPX_LEN_ENCRIP  5 // 2 id_taula + 3 clau
#define MPX_LEN_MIN_ENCRIP (MPX_LEN_MIN + MPX_LEN_ENCRIP) 

#define DEVICE_TX5240  '0'

#define PER_CPU           '0'
#define PER_LUM           '1'
#define PER_GPS           '2'
#define PER_GSM           '3'
#define PER_BOT           '4'
#define PER_PRN_TX        '5' // per comunicar amb impre taximetre
#define PER_TXM           '6'
#define PER_PRN_PREG_STS  '7' // per pregunta status impre de BT40
#define PER_TMG           '8'  // tarja magnetica
#define PER_TXM_ESTAT     '9' // per preguntar estat TX en obert
#define PER_PRN_RESP_STS  'A' // per respostes status impre de BT40
#define PER_TRANSM        'B' // transmissions amb TC60 
#define PER_TRANSFER      'C' // per carregar tarifa / enviar signatura tiquet
#define PER_STATUS        'D' // status cobertura

#define PROG_SKY		'2'
#define PROG_TX80		'3'
#define ARXIU_TARIFA 	'4'
#define ARXIU_SONS		'5'
#define PROG_TX40		'6'

typedef struct s_msg_mpxd
{
  unsigned char device;
  char buf[1024+MPX_LEN_MIN];
  int nc_rebuts;
  int ichar_a_processar;
  char proces_en_curs;
}t_msg_mpxd;

typedef struct s_multiplex{
  char tipus;
  char num_usart;
  char estat_rep;
  int nc_rep;
  char buf_rep[1024+MPX_LEN_MIN+20];
  char len_encrip;
  t_msg_mpxd* missatge_pendent_transferir;
  char* p_msg;
  t_msg_mpxd msg_lum;
  t_msg_mpxd msg_prn;
  t_msg_mpxd msg_txm;
// gestio claus  
  char* taula_encrip;
  short id_taula;
  unsigned short last_data_no_ac;
  int dies_sense_ac;
  unsigned char pos_clau[3];
  unsigned char Key[MPX_LEN_KEY];
  unsigned char Key_alt[MPX_LEN_KEY];
  unsigned char* Key_encriptar;
}t_multiplex;

extern t_multiplex multiplex;

extern void multiplex_init_0(void);
extern void multiplex_init(char tipus, char* taula, short id_taula);
extern void RC4_init(void);

extern void multiplex_enviar(int device, int perif, char * buf, int nb);
extern void multiplex_retrieve_missatge(void);


#endif
