// ******************************************************
// tots.c   totalitzadors
// ******************************************************

#include "hard.h"
#include "tx30.h"
#include "tots.h"
#include "totsEq.h"

#define  __TOTALIZERS _Pragma("location=\"__TOTALIZERS\"")
#define  __TOTALIZERS_EQ_INIT _Pragma("location=\"__TOTALIZERS_EQ_INIT\"")
#define  __TOTALIZERS_EQ _Pragma("location=\"__TOTALIZERS_EQ\"")

__TOTALIZERS 			__no_init struct s_total total[TOT_FIN];
__TOTALIZERS_EQ_INIT  	__no_init int tots_eq_init;
__TOTALIZERS_EQ			__no_init struct tot_equador tots_eq[TOT_PER_FIN];


const struct s_tot tot[]=
{
 0,1,NF_K_T,1,CONVERT_NO,        //  TOT_NSER  
 0,1,NF_I_T,2,CONVERT_NO,        //  TOT_IMPC  
 0,1,NF_I_T,3,CONVERT_NO,        //  TOT_IMPS  
 0,0,NF_I_T,0,CONVERT_NO,        //  TOT_IMPT  
 0,1,NF_D_T,5,CONVERT_NO,        //  TOT_KMT   
 0,1,NF_D_T,6,CONVERT_NO,        //  TOT_KMO   
 0,0,NF_D_T,0,CONVERT_NO,        //  TOT_KML   
 0,1,NF_D_T,8,CONVERT_NO,        //  TOT_KMLP  
 0,1,NF_K2D,9,CONVERT_MINUTS,     //  TOT_TOC   
 0,1,NF_K2D,0x0a,CONVERT_MINUTS,  //  TOT_TON   
 0,1,NF_K_T,0x0b,CONVERT_NO,     //  TOT_NDESC 
 0,1,NF_K2D,0x0c,CONVERT_MINUTS, //  TOT_TDESC 
 0,1,NF_K_T,0x0d,CONVERT_NO,     //  TOT_BORR  

 1,0,NF_K_T,0,CONVERT_NO,        //  TOT_NSER_P  
 1,0,NF_I_T,0,CONVERT_NO,        //  TOT_IMPC_P  
 1,0,NF_I_T,0,CONVERT_NO,        //  TOT_IMPS_P  
 1,0,NF_I_T,0,CONVERT_NO,        //  TOT_IMPT_P  
 1,0,NF_D_T,0,CONVERT_NO,        //  TOT_KMT_P   
 1,0,NF_D_T,0,CONVERT_NO,        //  TOT_KMO_P   
 1,0,NF_D_T,0,CONVERT_NO,        //  TOT_KML_P   
 1,0,NF_D_T,0,CONVERT_NO,        //  TOT_KMLP_P  
 1,0,NF_K2D,0,CONVERT_MINUTS,    //  TOT_TOC_P   
 1,0,NF_K2D,0,CONVERT_MINUTS,    //  TOT_TON_P   
 0,0,NF_K_T,0,CONVERT_NO,        //  TOT_NSALTS
 0,0,NF_K2D,0,CONVERT_NO,        //  TOT_TONHHMM
 0,0,NF_K_T,0,CONVERT_NO,        //  TOT_CCAB
 0,0,NF_K_T,0,CONVERT_NO,        //  TOT_E3
 0,0,NF_K_T,0,CONVERT_NO,        //  TOT_E6
 0,1,NF_K_T,0x0e,CONVERT_NO,     //  TOT_NTICK   afegit a v 1.03E
 0,1,NF_K_T,0x0f,CONVERT_NO,     //  TOT_NTORN   afegit a v 1.03F
 0,1,NF_I_T,4,CONVERT_NO,        //  TOT_BB
 1,0,NF_I_T,0,CONVERT_NO,        //  TOT_BB_P
 0,0,NF_D_T,0,CONVERT_NO,		 //  TOT_KMOFF
 1,0,NF_D_T,0,CONVERT_NO		 //  TOT_KMOFF_P
};


void totalitzador_disable(char ntot)
{
  if(ntot >= TOT_FIN)
    return;
  total[ntot].value_compl = total[ntot].value ^ 0x55555555;
}

void totalitzador_reset(char ntot)
{
  if(ntot >= TOT_FIN)
    return;
  
  while(fallo_tension());
  
  total[ntot].value = 0;
  total[ntot].value_compl = 0xffffffffL;
}

long totalitzador_read(char ntot,int * st)
{
  if((total[ntot].value ^ total[ntot].value_compl) == 0xffffffffL)
  {
    *st = 0;
    return(total[ntot].value);
  }
  else
  {
    *st = 1;
    return(99999999L);
  }
              
}
            
            
void totalitzador_write(char ntot, long value)
{
  while(fallo_tension());
  
  if((ntot >= TOT_FIN) || ((total[ntot].value ^ total[ntot].value_compl) != 0xffffffffL))
    return;
  total[ntot].value = value;
  total[ntot].value_compl = value ^0xffffffffL;
}

void totalitzador_add(char ntot, long value)
{
  while(fallo_tension());
  
  if((ntot >= TOT_FIN) || ((total[ntot].value ^ total[ntot].value_compl) != 0xffffffffL))
    return;
  total[ntot].value += value;
  total[ntot].value_compl = total[ntot].value ^0xffffffffL;
}
void totalitzador_incr(char ntot)
{
  while(fallo_tension());
  
  if((ntot >= TOT_FIN) || ((total[ntot].value ^ total[ntot].value_compl) != 0xffffffffL))
    return;
  total[ntot].value ++;
  total[ntot].value_compl = total[ntot].value ^ 0xffffffffL;
}
            
void totalitzador_compost_suma(int tsum, int t1, int t2)
{  
unsigned long value1,value2;
int st1,st2;
 value1 = totalitzador_read(t1, &st1);
 value2 = totalitzador_read(t2, &st2);
 if((st1 | st2))
 {
   totalitzador_disable(tsum);
   return;
 }
 value1 += value2;
 totalitzador_write(tsum, value1);
}

//void totalitzador_compost_resta(int tsum, int t1, int t2)
//{  
//unsigned long value1,value2;
//int st1,st2;
// value1 = totalitzador_read(t1, &st1);
// value2 = totalitzador_read(t2, &st2);
// if((st1 | st2))
// {
//   totalitzador_disable(tsum);
//   return;
// }
// value1 = (value1+100000000L) - value2;  // es suma 1000... per si ha donat la volta
// totalitzador_write(tsum, value1);
//}
//
// llegeig valor totalitzador i aplica conversio per sortida a display
long totalitzador_read_for_display(char ntot,int * st)
{
long value;

  value = totalitzador_read(ntot, st);
  if(*st)
    return(value);  // no aplica conversio
  switch(tot[ntot].convert)
  {
    case CONVERT_NO:
      break;
    case CONVERT_MINUTS:
      value = (value/60)*100 + (value%60);
      break;
  }
  return(value);

}
            
void borrar_tots_parcials(void){
	int i;
	for(i=0;i<TOT_FIN;i++)
		if(tot[i].parcial)
			totalitzador_reset(i);
	totalitzador_incr(TOT_BORR);
}