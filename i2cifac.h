//***************************************************************
// i2cifac.h
//***************************************************************

#ifndef __i2cifac_H
#define __i2cifac_H

//extern int time_out_eprom;

extern void delay_us(int us);
extern void reset_i2c_ifac(void);
extern unsigned char status_i2c_ifac(char nClock, char nSlave);
//extern int private_status_i2c_ifac(unsigned char sl);

extern void output_i2c_ifac(char nClock, char nSlave, unsigned char dato);
extern unsigned char input_i2c_ifac(char nClock, char nSlave);


extern void write_i2c_ifac(char nClock, unsigned char sl, unsigned char dir, unsigned char nb, char *buf);
extern void wread_i2c_ifac(char nClock, unsigned char sl, unsigned char dir, unsigned char nb, char *buf);

extern void write_i2c_extnd_ifac(char nClock, unsigned char sl, unsigned int dir, unsigned int nb, char *buf);
extern void wread_i2c_extnd_ifac(char nClock, unsigned char sl,unsigned int dir,unsigned int nb, char *buf);

extern void wread_i2c_adr3_ifac(char nClock, unsigned char sl,unsigned int dir,unsigned int nb, char *buf);
extern void write_i2c_adr3_ifac(char nClock, unsigned char sl, unsigned int dir, unsigned int nb, char *buf);

#endif
