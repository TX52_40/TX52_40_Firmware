// *********************************************************************
// insika.c
// *********************************************************************

#include <stdlib.h>
#include <string.h>

#include "regs.h"
#include "aplic.h"
#include "tx30.h"

extern unsigned int x_impre(T_TICKET *tck, unsigned int maxlen, unsigned char num_pqt);

// *************************************************************
// preparacio e impressio tiquet en ram
// *************************************************************

T_CAMP_TICKET camps_tiquet_insika[] =
{
 0,             0,                 FORM_FONT_SIZE,   0, 0,      // num. font
 REG_TCKS_TX30, CAM_buffer_insika, FORM_LZ | 0x80, 0, 0,      // buffer
};

T_TICKET tiquet_insika =
{
  100, 2, 0, camps_tiquet_insika,
};

const char canvi_tipus_lletra[4] =
{
  3,2,1,0,
};

void imprimir_insika(void)
{
short i;
char font;

  for(i=0; i < tx30.insika_n_lin; i++) 
  {
    font = tx30.insika_line_to_print[i][0];
    if(font > 3)
      font = 0;
    else
      font = canvi_tipus_lletra[font];
    camps_tiquet_insika[0].nc_out =  font;  // font size
    camps_tiquet_insika[1].nc_out = strlen(&tx30.insika_line_to_print[i][1]);
    memcpy(reg_tcks_tx30->buffer_insika, &tx30.insika_line_to_print[i][1],camps_tiquet_insika[1].nc_out);
    x_impre(&tiquet_insika,64000,0);
  }
  // avanc 3linies
    camps_tiquet_insika[0].nc_out =  2;  // default font size
    camps_tiquet_insika[1].nc_out = 3;
    memcpy(reg_tcks_tx30->buffer_insika, "\x0a\x0a\x0a",3);
    x_impre(&tiquet_insika,64000,0);
  
}


char processar_transmissio_insika(char* buf_rep, int nc_rep)
{
int i, index, nc;

  switch(buf_rep[0])
    {
    case 'i':
      tx30.insika_needed = 1;
      return 'i';
    case 't':
      if(nc_rep < 4)
        return 'N';
      tx30.insika_n_lin = buf_rep[1];
      for(i=0,index=2;i<tx30.insika_n_lin;i++)
      {
        nc = buf_rep[index+1];
        if( (nc > 64) || (nc_rep < (index+1 + nc)))           
           return 'N';
        tx30.insika_line_to_print[i][0] = buf_rep[index];  // tamany lletra
        memcpy(&tx30.insika_line_to_print[i][1], &buf_rep[index+2], nc);
        tx30.insika_line_to_print[i][nc+1] = 0;
        index += (nc+2);
      }
      tx30.insika_received = 1;
      return 't';
    default:
      return 'N';
    }
    
}
