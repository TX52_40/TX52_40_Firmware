/*********************************************************************/
/*  car30.c      CARREGADOR TX30                                     */
/*********************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "tots.h"
#include "totseq.h"
#include "tautrans.h"
#include "Tx30.h"
#include "rtos.h"
#include "metros.h"
#include "format.h"
#include "rut30.h"
#include "i2c.h"
#include "reloj.h"
#include "rutines.h"
#include "display.h"
#include "eprom.h"
#include "stream.h"
#include "regs.h"
#include "aplic.h"
#include "teclat.h"
#include "lumin.h"
#include "impre.h"
#include "bloqueig_torn.h"
#include "prima.h"
#include "zapper.h"
#include "xmlassoc.h"
#include "hard.h"
#include "multiplex.h"
#include "car30.h"
#include "td30.h"

#define DIM(array) (sizeof(array)/sizeof(array[0]))

const unsigned char password_paris_default[LEN_CODS] = {0x03,0x01,0x04,0x02,0,0,0,0,0};

// **********************************************************************
//                                                                      
  
__no_init t_tarifa_tx50 xram,xram0;

__no_init char bufferStreamCargador[80*1024];

__no_init char  precambio_pend;
__no_init char  precambio_pend_bt40;

XML_DefElement const taula_DefTarifaTx50[]=
{
	"C",	"TarifaTx50"					,0  ,0,TXML_NONE    ,1	,0                                     					,0,
	"A",	"tarcom"						,1  ,0,TXML_STRUCT  ,1	,offsetof(t_tarifa_tx50, tarcom)						,sizeof(t_tarcom_eprom),
								
	"a",	"serie"							,2  ,1,TXML_CHAR    ,3  ,offsetof(t_tarcom_eprom,serie)							,0,     
	"b",	"CCC"							,2  ,1,TXML_CHAR    ,2  ,offsetof(t_tarcom_eprom,CCC)							,0,     
	"c",	"DDD"     						,2  ,1,TXML_CHAR    ,2  ,offsetof(t_tarcom_eprom,DDD)							,0,     
	"d",	"SS"							,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,SS)							,0,     
	"e",	"TTT"     						,2  ,1,TXML_CHAR    ,2  ,offsetof(t_tarcom_eprom,TTT)							,0,     
	"f",	"n_progt"    					,2  ,1,TXML_CHAR    ,2  ,offsetof(t_tarcom_eprom,n_progt)						,0,     
	"g",	"robo"							,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,robo)							,0,     
	"h",	"versions_valides"				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,versions_valides)				,0,     
	"i",	"tiup"							,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tiup)							,0,     
	"m",	"OP_CANVIVEL"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_CANVIVEL)					,0,     
	"n",	"veltop_canvivel"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,veltop_canvivel)				,0,     
	"o",	"segssortir_canvivel" 			,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,segssortir_canvivel)			,0,     
	"p",	"segsentrar_canvivel" 			,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,segsentrar_canvivel)			,0,     
	"q",	"OP_MULTIT"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_MULTIT)						,0,     
	"r",	"tiviaj"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tiviaj)						,0,
	"s",	"veltop"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,veltop)						,0,     
	"t",	"veltop_fin_err" 				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,veltop_fin_err)				,0,     
	"u",	"yardas"    					,2  ,1,TXML_FLOAT   ,1  ,offsetof(t_tarcom_eprom,yardas)						,0,     
	"v",	"OP_VISUTOTSOFF"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_VISUTOTSOFF)				,0,
	"w",	"OP_PERCENT_DESC"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_PERCENT_DESC)				,0,
	"v",	"OP_ANUL10SEG"   				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_ANUL10SEG)					,0,
	"y",	"OP_ANUL_100M"   				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_ANUL_100M)					,0,
	"z",	"OP_RELOJ_VISU"   				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_RELOJ_VISU)					,0,
	"aa",	"OP_RELOJ_PERM_METRES"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,OP_RELOJ_PERM_METRES)			,0,  
	"ab",	"OP_RELOJ_LIBRE"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_RELOJ_LIBRE)				,0,
	"ac",	"tiup_reloj_libre"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tiup_reloj_libre)				,0,
	"ad",	"OP_VISURELOFF" 				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_VISURELOFF)					,0,
	"ae",	"OP_LAP"  						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_LAP)						,0,
	"af",	"OP_LIB_E2" 					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_LIB_E2)						,0,
	"ag",	"sec_treure_tensio"				,2 	,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,sec_treure_tensio)				,0,
	"ah",	"OP_E5"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_E5)							,0,
	"ai",	"hay_impresora"  				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_impresora)					,0,
	"aj",	"ticket_obligatori"				,2 	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ticket_obligatori)				,0,
	"ak",	"ticket_voluntari"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ticket_voluntari)				,0,
	"am",	"OP_APOCMF"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_APOCMF)						,0,
	"an",	"OP_ETT"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_ETT)						,0,
	"ao",	"OP_TST"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_TST)						,0,
	"ap",	"OP_LOCKT"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_LOCKT)						,0,
	"aq",	"OP_BLOQ_ERR"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_BLOQ_ERR)					,0,
	"ar",	"OP_LLAVEOFF"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_LLAVEOFF)					,0,
	"as",	"visvel_imp_extr" 				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,visvel_imp_extr)				,0,
	"at",	"haypassw"    					,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,haypassw)						,0,
	"au",	"haybloc"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,haybloc)						,0,
	"av",	"tars_blq3"   					,2  ,1,TXML_LONG    ,1  ,offsetof(t_tarcom_eprom,tars_blq3)						,0,
	"aw",	"totbormodif"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,totbormodif)					,0,
	"ax",	"disp_errrel" 					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,disp_errrel)					,0,
	"ay",	"hay_zapper"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_zapper)					,0,
	"be",	"luces_ver"						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,luces_ver)						,0,
	"bf",	"hay_bloq_luz_lib"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_bloq_luz_lib)				,0,
	"bg",	"luz_bloq_lib"					,2  ,1,TXML_CHAR    ,2  ,offsetof(t_tarcom_eprom,luz_bloq_lib)					,0,
	"bh",	"hay_liocaut"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_liocaut)					,0,
	"bi",	"tar_liocaut"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tar_liocaut)					,0,
	"bj",	"m_liocaut"    					,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,m_liocaut)						,0,
	"bk",	"tipus_teclat" 					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tipus_teclat)					,0,
	"bl",	"formddmm"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,formddmm)						,0,
	"bm",	"NDEC"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,NDEC)							,0,
	"bn",	"ntars"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ntars)							,0,
	"bo",	"nblocs_tar"   					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,nblocs_tar)					,0,
	"bp",	"canautpag"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,canautpag)						,0,
	"bq",	"t_rel"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,t_rel)							,0,
	"br",	"titot"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,titot)							,0,
	"bs",	"tipsum"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tipsum)						,0,
	"bt",	"tisum"    						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tisum)							,0,
	"bu",	"sumlib"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,sumlib)						,0,
	"bv",	"tip_paglib"   					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tip_paglib)					,0,
	"bw",	"tipaglib"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tipaglib)						,0,
	"bx",	"hay_paglibaut"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_paglibaut)					,0,
	"by",	"m_paglibaut"  					,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,m_paglibaut)					,0,
	"bz",	"t_cods"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,t_cods)						,0,
	"ca",	"fallot"    					,2  ,1,TXML_LONG    ,1  ,offsetof(t_tarcom_eprom,fallot)						,0,
	"cb",	"tioff"    						,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,tioff)							,0,
	"cc",	"sup_p_s"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,sup_p_s)						,0,
	"cd",	"abortsupl"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,abortsupl)						,0,
	"ce",	"tiposupl"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tiposupl)						,0,
	"cf",	"maxsup"    					,2  ,1,TXML_LONG    ,1  ,offsetof(t_tarcom_eprom,maxsup)						,0,     
	"cg",	"maxsup_excluye_aut"			,2  ,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,maxsup_excluye_aut)			,0,
	"ch",	"pcimpsup"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,pcimpsup)						,0,
	"ci",	"supl_solo_fin_serv"			,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,supl_solo_fin_serv)			,0,
	"cj",	"digtot"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,digtot)						,0,
	"ck",	"tot_un_paso"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tot_un_paso)					,0,
	"cl",	"segs_interm"  					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,segs_interm)					,0,
	"A",	"disp_tot"						,2  ,0,TXML_VECTOR  ,TOT_FIN_MAX ,offsetof(t_tarcom_eprom,disp_tot) 			,sizeof(t_disp_tot),
	"a",	"nt"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_disp_tot,nt)								,0,
	"b",	"sst"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_disp_tot,sst)								,0,
	"c",	"lf" 	        				,3  ,1,TXML_SHORT   ,1  ,offsetof(t_disp_tot,lf)								,0,
	"B",	"luzest"	        			,2  ,0,TXML_VECTOR  ,GL_FIN_MAX ,offsetof(t_tarcom_eprom,luzest) 				,sizeof(t_luz),
	"a",	"luz"    						,3  ,1,TXML_CHAR    ,8  ,offsetof(t_luz,luz)									,0,
	"co",	"lede_rel"    					,2  ,1,TXML_SHORT   ,3  ,offsetof(t_tarcom_eprom,lede_rel)						,0,
	"cp",	"text_lib"    					,2  ,1,TXML_CHAR    ,10 ,offsetof(t_tarcom_eprom,text_lib)						,0,
	"C",	"text_disem"					,2  ,0,TXML_VECTOR  ,7  ,offsetof(t_tarcom_eprom,text_disem) 					,sizeof(t_text_disem),
	"a",	"text"    						,3  ,1,TXML_CHAR    ,4  ,offsetof(t_text_disem,text)							,0,
	"D",	"ajuste_hor"					,2  ,0,TXML_VECTOR  ,NUM_AJUSTS_HORA_OLD,offsetof(t_tarcom_eprom,ajuste_hor) 	,sizeof(t_ajuste_hor),
	"a",	"tope"    						,3  ,1,TXML_CHAR    ,5  ,offsetof(t_ajuste_hor,tope)							,0,
	"b",	"tipo"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_ajuste_hor,tipo)							,0, 
	
	// al seguent array hi ha la resta de elements que s'han ampliat de 	NUM_AJUSTS_HORA_OLD a NUM_AJUSTS_HORA
	"E",	"ajuste_hor_added"				,2  ,0,TXML_VECTOR  ,NUM_AJUSTS_HORA - NUM_AJUSTS_HORA_OLD,offsetof(t_tarcom_eprom,ajuste_hor)+ (sizeof(t_ajuste_hor) * NUM_AJUSTS_HORA_OLD) ,sizeof(t_ajuste_hor),
	"a",	"tope"    						,3  ,1,TXML_CHAR    ,5  ,offsetof(t_ajuste_hor,tope),0,     
	"b",	"tipo"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_ajuste_hor,tipo),0,     
	
	"hs",	"test_disp_auto"				,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,test_disp_auto)				,0,
	"cu",	"con_precambio"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,con_precambio)					,0,     
	"cv",	"anydia_precambio"				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,anydia_precambio)				,0,     
	"cw",	"hora_precambio"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hora_precambio)				,0,     
	"cx",	"newtransm"						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,newtransm)						,0,     
	"cy",	"newtransm_esp"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,newtransm_esp)					,0,     
	"cz",	"OP_CANZH_INI"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_CANZH_INI)					,0,     
	"da",	"genimp_ocup"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,genimp_ocup)					,0,     
	"db",	"OP_error_impre"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_error_impre)				,0,     
	"dc",	"OP_test_old_new"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_test_old_new)				,0,     
	"dd",	"OP_bloc5Km"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,OP_bloc5Km)					,0,     
	"de",	"VISU_LAST_SERV" 				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,VISU_LAST_SERV)				,0,     
	"df",	"flash_visu_last_serv"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,flash_visu_last_serv )			,0,     
	"dg",	"segs_visu_last_serv" 			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,segs_visu_last_serv  )			,0,     
	"dh",	"confi_vls"						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,confi_vls)						,0,
	"dl",	"visu_euro_pagar"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,visu_euro_pagar)				,0,     
	"dm",	"TEXT_EURO"						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,TEXT_EURO)						,0,     
	"do",	"NDEC_ALT"						,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,NDEC_ALT)						,0,     
	"dp",	"factor_alt"					,2  ,1,TXML_FLOAT   ,1  ,offsetof(t_tarcom_eprom,factor_alt)					,0,     
	"dq",	"suma_autom"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,suma_autom)					,0,     
	"dr",	"paglib_autom"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,paglib_autom)					,0,     
	"ds",	"hay_precio_fijo"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_precio_fijo)				,0,     
	"dt",	"tar_precio_fijo"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tar_precio_fijo)				,0,     
	"du",	"hm_precio_fijo"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hm_precio_fijo)				,0,     
	"dv",	"texto_precio_fijo"				,2 	,1,TXML_CHAR    ,4  ,offsetof(t_tarcom_eprom,texto_precio_fijo)				,0,     
	"dx",	"ajuste_rel"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ajuste_rel)					,0,     
	"dy",	"exces_vel_no_horaria"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,exces_vel_no_horaria)			,0,     
	"dz",	"errgenimp_no_horaria"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,errgenimp_no_horaria)			,0,     
	"ea",	"bloq_libre_impre"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloq_libre_impre)				,0,     
	"eb",	"bloq_ocup_impre"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloq_ocup_impre)				,0,     
	"ed",	"llums_combi"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,llums_combi)					,0,     
	"ec",	"luminTipoFixe"   				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,luminTipoFixe)					,0,     
	"ee",	"corsa_min_supl"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,corsa_min_supl)				,0,     
	"ef",	"salto_pita"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,salto_pita)					,0,     
	"eg",	"ledtar_interm"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ledtar_interm)					,0,     
	"eh",	"lf_HORARIA"      				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_HORARIA)					,0,  
	"ei",	"new_leds_ho_km"  				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,new_leds_ho_km)				,0,        
	"ej",	"lf_FECHA_PARO"   				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_FECHA_PARO)					,0,     
	"ek",	"lf_FLASH_LAPSO_SUPL"			,2	,1,TXML_SHORT  	,1  ,offsetof(t_tarcom_eprom,lf_FLASH_LAPSO_SUPL)			,0,     
	"el",	"lf_euro1"        				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_euro1)						,0,     
	"em",	"lf_euro2"        				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_euro2)						,0,     
	"en",	"lf_euro3"        				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_euro3)						,0,     
	"eo",	"lf_vls0"         				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_vls0)						,0,     
	"ep",	"lf_vls0_alt"     				,2 	,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_vls0_alt)					,0,     
	"eq",	"lf_vls1"         				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_vls1)						,0,     
	"er",	"lf_vls2"         				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_vls2)						,0,     
	"es",	"lf_vls3"         				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,lf_vls3)						,0,     
	"F",	"vars_estat"					,2  ,0,TXML_VECTOR  ,E_MAX,offsetof(t_tarcom_eprom,vars_estat)					,sizeof(t_vars_estat),
	"a",	"ledest"    					,3  ,1,TXML_SHORT   ,1  ,offsetof(t_vars_estat,ledest)							,0,     
	"b",	"lf"            				,3  ,1,TXML_SHORT   ,1  ,offsetof(t_vars_estat,lf)								,0,     
	"eu",	"tipf_disem"    				,2  ,1,TXML_CHAR    ,7  ,offsetof(t_tarcom_eprom,tipf_disem)					,0,     
	"G",	"diaesp"	        			,2  ,0,TXML_VECTOR  ,NUM_DIAESP,offsetof(t_tarcom_eprom,diaesp)					,sizeof(t_diaesp),
	"a",	"anydia"   						,3  ,1,TXML_SHORT   ,1  ,offsetof(t_diaesp,anydia)								,0,     
	"b",	"tip"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_diaesp,tip)									,0,     
	"H",	"diaamb"	        			,2  ,0,TXML_VECTOR  ,17 ,offsetof(t_tarcom_eprom,diaamb)						,sizeof(t_diaamb),
	"a",	"anydia_ini" 					,3  ,1,TXML_SHORT   ,1  ,offsetof(t_diaamb,anydia_ini)							,0,     
	"b",	"anydia_fin" 					,3  ,1,TXML_SHORT   ,1  ,offsetof(t_diaamb,anydia_fin)							,0,     
	"c",	"tip"    						,3  ,1,TXML_CHAR    ,1  ,offsetof(t_diaamb,tip)									,0,         
	"ey",	"tck_no_legal"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tck_no_legal)					,0,     
	"ez",	"control_turno"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,control_turno)					,0,     
	"fe",	"bloqueo_disem"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloqueo_disem)					,0,     
	"ff",	"minut_inici_dia"				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,minut_inici_dia)				,0,     
	"fg",	"dias_librar"    				,2  ,1,TXML_CHAR    ,10 ,offsetof(t_tarcom_eprom,dias_librar)					,0,     
	"fh",	"sabdom"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,sabdom)						,0,     
	"fd",	"bloqueo_torn"    				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloqueo_torn)					,0,     
	"fp",	"minut_inici_torn"				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,minut_inici_torn)				,0,     
	"fq",	"minut_final_torn"				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,minut_final_torn)				,0,     
	"fi",	"minuts_max_torn" 				,2  ,1,TXML_LONG    ,1  ,offsetof(t_tarcom_eprom,minuts_max_torn)				,0,     
	"ex",	"minuts_penal_torn"				,2  ,1,TXML_SHORT  	,1  ,offsetof(t_tarcom_eprom,minuts_pas_off_a_lliure)		,0,     
	"fm",	"minuts_descans"  				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,minuts_descans)				,0,     
	"fo",	"hm_off_torn"   				,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,hm_off_torn)					,0,     
	"fj",	"minuts_max_torn_sabdom"		,2	,1,TXML_LONG	,1  ,offsetof(t_tarcom_eprom,minuts_max_torn_sabdom)		,0,     
	"fb",	"bloq_ocup_turno"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloq_ocup_turno)				,0,     
	"fa",	"tipo_descanso"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tipo_descanso)					,0,     
	"fc",	"bloq_luz_libre"				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,bloq_luz_libre)				,0,     
	"gl",	"nivell_llum_minim"				,2  ,1,TXML_CHAR   	,1  ,offsetof(t_tarcom_eprom,nivell_llum_minim)				,0,     
	"gm",	"tr_ccab"	        			,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tr_ccab)						,0,     
	"gn",	"miles_km_ccab"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,miles_km_ccab)					,0,     
	"go",	"hay_fslibdist"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,hay_fslibdist)					,0,     
	"gp",	"m_fslibdist"  					,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,m_fslibdist)					,0,     
	"gq",	"texte_COND"      				,2  ,1,TXML_CHAR    ,4  ,offsetof(t_tarcom_eprom,texte_COND)					,0,     
	"gr",	"texte_PASSW"      				,2  ,1,TXML_CHAR    ,4  ,offsetof(t_tarcom_eprom,texte_PASSW)					,0,     
	"gs",	"texte_TANCAR_TORN"				,2 	,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,texte_TANCAR_TORN)				,0,     
	"gt",	"texte_TORN"      				,2  ,1,TXML_CHAR    ,4  ,offsetof(t_tarcom_eprom,texte_TORN)					,0,     
	"gu",	"ticket_tancar_torn"			,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,ticket_tancar_torn)			,0,     
	"gv",	"valors_iva"    				,2  ,1,TXML_FLOAT   ,3  ,offsetof(t_tarcom_eprom,valors_iva)					,0,     
	"gw",	"hi_ha_conf_llums_test_display"	,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,hi_ha_conf_llums_test_display)	,0,     
	"gx",	"llums_test_display"			,2  ,1,TXML_CHAR  	,2  ,offsetof(t_tarcom_eprom,llums_test_display)			,0,     
	"gy",	"NUM_supl_max"					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,NUM_supl_max)					,0,     
	"gz",	"pot_apagar_lluminos"			,2	,1,TXML_CHAR   	,1  ,offsetof(t_tarcom_eprom,pot_apagar_lluminos)			,0,     
	"fr",	"sanciona_descans_curt"			,2	,1,TXML_CHAR 	,1  ,offsetof(t_tarcom_eprom,sanciona_descans_curt)			,0,     
	"fs",	"sanciona_fallo_tensio"			,2	,1,TXML_CHAR 	,1  ,offsetof(t_tarcom_eprom,sanciona_fallo_tensio)			,0,     
	"ft",	"control_horari_cap_setmana"	,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,control_horari_cap_setmana)	,0,     
	"fk",	"minuts_max_torn_2" 			,2  ,1,TXML_LONG    ,1  ,offsetof(t_tarcom_eprom,minuts_max_torn_2)				,0,     
	"fl",	"minuts_max_torn_sabdom_2"		,2	,1,TXML_LONG	,1  ,offsetof(t_tarcom_eprom,minuts_max_torn_sabdom_2)		,0,     
	"fu",	"sanciona_moviment"				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,sanciona_moviment)				,0,     
	"az",	"max_acc1"         				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,max_acc1)						,0,     
	"ba",	"max_acc2"         				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,max_acc2)						,0,     
	"bb",	"vel_zap_change"   				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,vel_zap_change)				,0,     
	"bc",	"num_errors1"      				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,num_errors1)					,0,     
	"bd",	"zapper_segons_error_definitiu"	,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,zapper_segons_error_definitiu)	,0,     
	"hk",	"corsa_min_deixa_de_comptar"	,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,corsa_min_deixa_de_comptar)	,0,     
	"hl",	"corsa_min_aplica_pas_fiserv"	,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,corsa_min_aplica_pas_fiserv)	,0,     
	"hm",	"display_suma_en_fi_servei"		,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,display_suma_en_fi_servei)		,0,     
	"if",	"datos_tr1"       				,2 	,1,TXML_STRING  ,1 	,offsetof(t_tarcom_eprom,taula_claus)					,64,
	"ig",	"datos_tr2"       				,2 	,1,TXML_STRING  ,1 	,offsetof(t_tarcom_eprom,taula_claus) + 64				,64,
	"ih",	"datos_tr3"       				,2 	,1,TXML_STRING  ,1 	,offsetof(t_tarcom_eprom,taula_claus) +128				,64,
	"ii",	"datos_tr4"       				,2 	,1,TXML_STRING  ,1 	,offsetof(t_tarcom_eprom,taula_claus) +192				,64,
	"ij",	"id_tr"  	        			,2 	,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,id_claus)						,0,     
	"fv",	"visu_torn_en_lliure"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,visu_torn_en_lliure)			,0,     
	"fw",	"visu_dia_torn"   				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,visu_dia_torn)					,0,     
	"gb",	"torn_continu"    				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,torn_continu)					,0,     
	"gc",	"segs_anular_pas_off"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,segs_anular_pas_off)			,0,     
	"gd",	"interval_entre_torns"			,2	,1,TXML_SHORT	,1  ,offsetof(t_tarcom_eprom,interval_entre_torns)			,0,     
	"ge",	"preavis_fi_torn" 				,2	,1,TXML_SHORT	,1  ,offsetof(t_tarcom_eprom,preavis_fi_torn)				,0,     
	"fn",	"minuts_maxim_descans"			,2	,1,TXML_SHORT	,1  ,offsetof(t_tarcom_eprom,minuts_maxim_descans)			,0,     
	"gf",	"veure_events_torn"				,2	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,veure_events_torn)				,0,     
	"gg",	"preprog_events_torn"			,2	,1,TXML_CHAR  	,1  ,offsetof(t_tarcom_eprom,preprog_events_torn)			,0,     
	"gh",	"hora_minim_ini_torn"			,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,hora_minim_ini_torn)			,0,     
	"gi",	"hora_maxim_ini_torn"			,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,hora_maxim_ini_torn)			,0,     
	"fx",	"num_descans_torn"				,2 	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,num_descans_torn)				,0,     
	"fy",	"max_descans_fets"				,2 	,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,max_descans_fets)				,0,     
	"fz",	"texte_max_descans_fets"		,2 	,1,TXML_CHAR	,6	,offsetof(t_tarcom_eprom,texte_max_descans_fets)		,0,     
	"gk",	"password_paris"  				,2 	,1,TXML_STRING  ,9  ,offsetof(t_tarcom_eprom,password_paris)				,9,     
	"gj",	"dates_desactivacio_torn"		,2	,1,TXML_CHAR	,4	,offsetof(t_tarcom_eprom,dates_desactivacio_torn)		,0,     
	"dw",	"control_chile"   				,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,control_chile)					,0,        
	"ga",	"no_rearmar_automaticament"   	,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,no_rearmar_automaticament)		,0,        
	"hn",	"tots_historics"				,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,tots_historics)				,0,
	"hv",	"err_2"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[1])					,0,
	"hw",	"err_3"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[2])					,0,
	"hx",	"err_4"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[3])					,0,
	"hy",	"err_5"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[4])					,0,
	"hz",	"err_6"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[5])					,0,
	"ia",	"err_7"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[6])					,0,
	"ib",	"err_8"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[7])					,0,
	"ic",	"err_10"    					,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[9])					,0,
	"id",	"avis_7"    					,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[10])				,0,
	"ie",	"err_z"    						,2  ,1,TXML_CHAR    ,6  ,offsetof(t_tarcom_eprom,text_error[11])				,0,
	"l",	"no_canvis_primer_salt"			,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,no_canvis_primer_salt)			,0,
	"hu",	"Tiq_obl_imp"					,2	,1,TXML_LONG	,1	,offsetof(t_tarcom_eprom,Tiq_obl_imp)					,0,
	"ho",	"tarifa_remota"					,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,tarifa_remota)					,0,
	"hr",	"progr_TX_remoto"				,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,progr_TX_remoto)				,0,
	"ik",	"pcsup_bb"						,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,pcsup_bb)						,0,
	"il",	"pcsup_pt"						,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,pcsup_pt)						,0,
	"im",	"acumula_franq"					,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,acumula_franq)					,0,
	"in",	"hora_ini_fr2"					,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,hora_ini_fr2)					,0,     
	"io",	"hora_fin_fr2"					,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,hora_fin_fr2)					,0,     
	"ip",	"min_max_fr2"					,2	,1,TXML_SHORT 	,1  ,offsetof(t_tarcom_eprom,min_max_fr2)					,0,     
	"iq",	"tar_pre_ac"					,2	,1,TXML_CHAR	,1	,offsetof(t_tarcom_eprom,tec_preu_acordat)				,0,
	"it",	"clau_preu_ac"					,2	,1,TXML_STRING	,1	,offsetof(t_tarcom_eprom,clau_preu_ac)			,64,
    "iw",   "Id_clau_preu_ac"               ,2  ,1,TXML_SHORT   ,1  ,offsetof(t_tarcom_eprom,Id_clau_preu_ac)               ,0,
	"ix",	"tisum_s"    					,2  ,1,TXML_CHAR    ,1  ,offsetof(t_tarcom_eprom,tisum_s)						,0,
	
	"C",	"bloc_tar"						,1  ,0,TXML_VECTOR  ,32,offsetof(t_tarifa_tx50, bloc_tar)						,sizeof(t_bloc_tar_eprom),
	
	"a",	"tipt_vf"     					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_bloc_tar_eprom,tipt_vf)						,0,     //  0/1/2  rent/vvf/isr 
	"b",	"tipt_acs"    					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_bloc_tar_eprom,tipt_acs)						,0,     //  0/1  ACFS_H ACFS_K  junts/separats  
	"c",	"vsal"    						,2  ,1,TXML_LONG    ,2 ,offsetof(t_bloc_tar_eprom,vsal)							,0,     // valor salto 
	"d",	"vf_fija"     					,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_bloc_tar_eprom,vf_fija)						,0,     // velo frontera caso isr (km/hora)
	"e",	"bits_cuenta" 					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_bloc_tar_eprom,bits_cuenta)					,0,    	//    76543210      0/1  no/si cuenta 
	"f",	"m_ps"							,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_bloc_tar_eprom,m_ps)							,0,     // metros salto 
	"g",	"seg_ps"						,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_bloc_tar_eprom,seg_ps)						,0,     // segundos salto 
	"h",	"impkm_ss"						,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_bloc_tar_eprom,impkm_ss)						,0,     // importe km. 
	"i",	"impho_ss"						,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_bloc_tar_eprom,impho_ss)						,0,     // importe hora 
	
	"B",	"tarifa"						,1  ,0,TXML_VECTOR  ,32,offsetof(t_tarifa_tx50, tarifa)							,sizeof(t_tarifa),
	
	"a",	"bb"		    				,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,bb)									,0,
	"e",	"corsa_min"       				,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,corsa_min)							,0,
	"f",	"lioc"	    					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,lioc)									,0,
	"g",	"ococ"	    					,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,ococ)									,0,
	"h",	"ocap"	    					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,ocap)									,0,
	"i",	"apoc"	    					,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,apoc)									,0,
	"j",	"b_t_ocup"        				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,b_t_ocup)								,0,
	"k",	"b_t_pagar"       				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,b_t_pagar)							,0,
	"l",	"ledocup"         				,2  ,1,TXML_SHORT   ,2 ,offsetof(t_tarifa,ledocup)								,0,
	"m",	"ledapagar"       				,2  ,1,TXML_SHORT   ,2 ,offsetof(t_tarifa,ledapagar)							,0,
	"n",	"luzocup"         				,2  ,1,TXML_CHAR    ,8 ,offsetof(t_tarifa,luzocup)								,0,
	"o",	"luzpagar"        				,2  ,1,TXML_CHAR    ,8 ,offsetof(t_tarifa,luzpagar)								,0,
	"p",	"supl"	    					,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,supl)									,0,
	"q",	"nmaxs"	    					,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,nmaxs)								,0,
	"r",	"pcsup"           				,2  ,1,TXML_FLOAT   ,1 ,offsetof(t_tarifa,pcsup)								,0,
	"s",	"pctip"           				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,pctip)								,0,
	"t",	"supl_aut"        				,2  ,1,TXML_LONG    ,1 ,offsetof(t_tarifa,supl_aut)								,0,
	"u",	"tip_s_aut"       				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,tip_s_aut)							,0,
	"v",	"op_vd"	    					,2  ,1,TXML_CHAR    ,4 ,offsetof(t_tarifa,op_vd)								,0,
	"w",	"op_tec0"         				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,op_tec0)								,0,
	"x",	"visu_globtar"    				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,visu_globtar)							,0,
	"y",	"tiptop"          				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,tiptop)								,0,
	"z",	"tec_supl"        				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,tec_supl)								,0,
	"aa",	"lf_oc"           				,2  ,1,TXML_SHORT   ,1 ,offsetof(t_tarifa,lf_oc)								,0,
	"ab",	"lf_pag"          				,2  ,1,TXML_SHORT   ,1 ,offsetof(t_tarifa,lf_pag)								,0,
	"ac",	"impre_tartrab"   				,2  ,1,TXML_CHAR    ,2 ,offsetof(t_tarifa,impre_tartrab)						,0,
	"ad",	"tipus_iva"       				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_tarifa,tipus_iva)							,0,
	"b",	"bb_manual"						,2	,1,TXML_CHAR	,1 	,offsetof(t_tarifa,bb_manual)							,0,
	"c",	"prim_salt_man"					,2	,1,TXML_CHAR	,1 	,offsetof(t_tarifa,prim_salt_man)						,0,
	"d",	"branca"						,2	,1,TXML_CHAR	,1 	,offsetof(t_tarifa,branca)								,0,
	
	"D",	"zonh"  						,1  ,0,TXML_VECTOR  ,17,offsetof(t_tarifa_tx50, zonh)							,sizeof(t_zonh),
	
	"a",	"zon"         					,2  ,1,TXML_SHORT   ,1 ,0                               						,0,     
	
	"E",	"tope_imp"  					,1  ,0,TXML_VECTOR  ,17,offsetof(t_tarifa_tx50, tope_imp)						,sizeof(t_tope_imp),
	
	"a",	"timp"         					,2  ,1,TXML_LONG    ,1 ,0                               						,0,     
	
	"F",	"tope_dist"  					,1  ,0,TXML_VECTOR  ,17,offsetof(t_tarifa_tx50, tope_dist)						,sizeof(t_tope_dist),
	
	"a",	"tdist"       					,2  ,1,TXML_LONG    ,1  ,0                               						,0,     
	
	"G",	"tope_temps"  					,1  ,0,TXML_VECTOR  ,17,offsetof(t_tarifa_tx50, tope_temps)						,sizeof(t_tope_temps),
	
	"a",	"ttemps"       					,2  ,1,TXML_SHORT  ,1  ,0                               						,0,     
	
	"H",	"canvi_autom"					,1  ,0,TXML_VECTOR  ,32*8,offsetof(t_tarifa_tx50, canvi_autom)					,sizeof(t_canvi_autom),
	
	"a",	"tarifa"						,2  ,1,TXML_CHAR    ,1 ,offsetof(t_canvi_autom,tarifa)							,0,
	"b",	"bits"							,2  ,1,TXML_CHAR    ,1 ,offsetof(t_canvi_autom,bits)							,0,
	"c",	"teclat"						,2  ,1,TXML_LONG    ,1 ,offsetof(t_canvi_autom,teclat)							,0,
	"e",	"amb" 							,2  ,1,TXML_CHAR    ,1 ,offsetof(t_canvi_autom,amb)								,0,
	"l",	"velo"							,2  ,1,TXML_CHAR    ,1 ,offsetof(t_canvi_autom,velo)							,0,
	"f",	"tdia"							,2  ,1,TXML_SHORT   ,1 ,offsetof(t_canvi_autom,tdia)							,0,
	"g",	"zonh"							,2  ,1,TXML_SHORT   ,1 ,offsetof(t_canvi_autom,zonh)							,0,
	"h",	"import"						,2  ,1,TXML_SHORT   ,1 ,offsetof(t_canvi_autom,import)							,0,
	"i",	"dist"							,2  ,1,TXML_SHORT   ,1 ,offsetof(t_canvi_autom,dist)							,0,
	"j",	"temps"							,2  ,1,TXML_SHORT   ,1 ,offsetof(t_canvi_autom,temps)							,0,
	"k",	"sext" 							,2  ,1,TXML_CHAR    ,1 ,offsetof(t_canvi_autom,sext)							,0,
	"d",	"branca"						,2	,1,TXML_CHAR	,1 ,offsetof(t_canvi_autom,branca)							,0,
	
	"I",	"textes_tiquet"					,1  ,0,TXML_STRUCT  ,1	,offsetof(t_tarifa_tx50, textes_tiquet)					,sizeof(t_textes_tiquet),
	
	"a",	"hi_ha_textes"  				,2  ,1,TXML_CHAR    ,1 ,offsetof(t_textes_tiquet,hi_ha_textes ) 				,0,
	"b",	"texte_nom"     				,2  ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_nom)     				,LEN_TEXTE_NOM,
	"c",	"texte_dni"     				,2  ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_dni)     				,LEN_TEXTE_DNI,
	"d",	"texte_llicencia"				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_llicencia)				,LEN_TEXTE_LLICENCIA,
	"e",	"texte_matricula"				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_matricula)				,LEN_TEXTE_MATRICULA,
	
	"f",	"texte_H1"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H1)      					,LEN_TEXTE_H,
	"g",	"texte_H2"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H2)      					,LEN_TEXTE_H,
	"h",	"texte_H3"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H3)      					,LEN_TEXTE_H,
	"i",	"texte_H4"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H4)      					,LEN_TEXTE_H,
	"j",	"texte_H5"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H5)      					,LEN_TEXTE_H,
	"k",	"texte_H6"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_H6)      					,LEN_TEXTE_H,
	"l",	"texte_F1"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F1)      					,LEN_TEXTE_F,
	"m",	"texte_F2"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F2)      					,LEN_TEXTE_F,
	"n",	"texte_F3"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F3)      					,LEN_TEXTE_F,
	"o",	"texte_F4"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F4)      					,LEN_TEXTE_F,
	"p",	"texte_F5"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F5)      					,LEN_TEXTE_F,
	"q",	"texte_F6"       				,2 ,1,TXML_STRING  ,1 ,offsetof(t_textes_tiquet,texte_F6)      					,LEN_TEXTE_F,
	
	"r",	"texte_generic"  				,2  ,0,TXML_VECTOR ,MAXNUM_TEXTE_GENERIC,offsetof(t_textes_tiquet,texte_generic),sizeof(t_texte_generic),
	"a",	"nom"   	   					,3  ,1,TXML_SHORT  ,1  ,offsetof(t_texte_generic,nom)							,0,
	"b",	"texte"        					,3  ,1,TXML_STRING ,1  ,offsetof(t_texte_generic,texte)							,LEN_TEXTE_GENERIC,
	
	"",	""								,-1 ,0,TXML_NONE   ,0  ,0														,0,	 // indicador final
};

//                                                                      *
// **********************************************************************



const uchar  TEXT_LEDEST_K[2]={0x39,0x3f};
const uchar  TEXT_LEDEST_KI[2]={0x39,0x30};
const uchar  TEXT_LEDEST_DI[2]={0x5e,0x30};
const uchar  TEXT_LEDEST_TRIG[2]={0x78,0x50};
const uchar  TEXT_LEDEST_TOT[2]={0x78,0x5c};
const uchar  TEXT_LEDEST_LIC1[2]={0xb8,0x06};
const uchar  TEXT_LEDEST_LIC2[2]={0xb8,0x5b};
const uchar  TEXT_LEDEST_LIC3[2]={0xb8,0x4f};
const uchar  TEXT_LEDEST_LIC4[2]={0xb8,0x66};
const uchar  TEXT_LEDEST_N_S[2]={0xb8,0x5b};
const uchar  TEXT_LEDEST_NT[2]={0x78,0x40};
const uchar  TEXT_LEDEST_REP[2]={0xd0,0x00};
const uchar  TEXT_LEDEST_NG[2]={0x37,0x3e};
const uchar  TEXT_LEDEST_r5[2]={0xd0,0x6d};
const uchar  TEXT_LEDEST_r6[2]={0xd0,0x7d};
const uchar  TEXT_LEDEST_NGQ[2]={0x40,0x37};
const uchar  TEXT_LEDEST_C6[2]={0xb9,0x7d};  // cambios K
const uchar  TEXT_LEDEST_C7[2]={0xb9,0x07};  // cambios tarifa
const uchar  TEXT_LEDEST_CP[2]={0xb9,0x73};  // cambios programa
const uchar  TEXT_LEDEST_NUMTICKET[2]={0x54,0x78};  // numero ticket

const uchar  TEXT_LEDEST_dia_descans[2]={0x5e,0x5e};  // dia librar
const uchar  TEXT_LEDEST_semana_descans[2]={0x6d,0x5e};  // dia librar

const uchar  TEXT_K_MAL[6]={0x40,0x40,0x40,0x40,0x40,0x40};
const uchar  TEXT_IMPRE[6]={0x73,0x50,0x04,0x54,0x78,0x00};
const uchar  TEXT_TR[6]={0x78,0x78,0x78,0x78,0x78,0x78};
const uchar  TEXT_LOAD[6]={0x49,0x49,0x49,0x49,0x49,0x49};
const uchar  TEXT_PRE_LOAD[6]={0x36,0x36,0x36,0x36,0x36,0x36};

const uchar  TEXT_LEDEST_PASW[2]={0x39,0x6d,};    //  CS


typedef struct s_def_taula_opcions{
 const  char (*textes)[6] ;
  char* enabled;
  const char* ledest;
  char n_max;
}t_def_taula_opcions;


#define ini_taula_opcions(nom,led1,led2) \
  const char ledest##nom[2]={led1,led2}; \
  const char textes##nom[][6]={

#define fin_taula_opcions(nom) \
      };\
  char enabled##nom[DIM(textes##nom)];\
  t_def_taula_opcions nom = {\
    textes##nom,enabled##nom,ledest##nom,DIM(textes##nom) };

#define GItip_Ana  0
#define GItip_Enc  1
#define GItip_Can  2

ini_taula_opcions(GItip,0x3d,0x10)    // Gi
  {0x77,0x54,0x77,0,0,0}, // Ana  0
  {0x79,0x54,0x58,0,0,0}, // Enc  1
//  {0x39,0x77,0x54,0,0,0}, // Can  2 (CIA447)
fin_taula_opcions(GItip)


ini_taula_opcions(GInivell,0x37,0x10)  // Ni
  {0x40,0x40,0x40,0,0,0}, // None
  {0x01,0x01,0x01,0,0,0}, // Pull up
  {0x08,0x08,0x08,0,0,0}, // Pull down
  {0x09,0x09,0x09,0,0,0}, // Both
fin_taula_opcions(GInivell)


// si s'afegeix un tipus nou, s'ha d'afegir un item nou
// a taula taula_tipo_lumin[] (a lumin.c)

#define LUMINtip_saludes        0
#define LUMINtip_otros          1
#define LUMINtip_saludesSerial  2
#define LUMINtip_france         3
#define LUMINtip_tl70           4
#define LUMINtip_argel          5

ini_taula_opcions(LUMINtip,0x38,0x1c)   // Lu
  {0x38,0x1c,0x00,0x3f,0,0},      // Lu 0     Saludes
  {0x38,0x1c,0x00,0x06,0,0},      // Lu 1     Otros
  {0x38,0x1c,0x00,0x5b,0,0},      // Lu 2     Saludes Serial
  {0x38,0x1c,0x00,0x4f,0,0},      // Lu 3     Paral�lel amb delay verificacio  (fran�a)
  {0x38,0x00,0x78,0x38,0x07,0x3f},// L tL70   Taxitronic TL70
  {0x38,0x00,0x77,0x50,0x00,0x00},// L Ar     Taxitronic Argelia
fin_taula_opcions(LUMINtip)


ini_taula_opcions(CONTRHORnumconds,0x37,0x58)
  {0x06,0x39,0x5c,0x54,0x5e,0},  // 1Cond
  {0x5b,0x39,0x5c,0x54,0x5e,0},  // 2Cond
fin_taula_opcions(CONTRHORnumconds)

#define IMPREtip_no   0
#define IMPREtip_int  1
#define IMPREtip_ext  2
#define IMPREtip_Opt  3
#define IMPREtip_Kas  4

ini_taula_opcions(IMPREtip,0x73,0x50)  // Pr
  {0x54,0x5c,0   ,0   ,0,0},  // no      sense impre
  {0x30,0x54,0x78,0   ,0,0},  // Int     interna
  {0x10,0x50,0x4f,0x5b,0,0},  // ir32    externa
  {0x3f,0x73,0x78,0x50,0,0},  // OPtr	 optronic
  {0x38,0x77,0x6d,0x00,0,0},  // CAS	 Kashino
fin_taula_opcions(IMPREtip)

#define TD30tip_no      0
#define TD30tip_td30    1
#define TD30tip_tl70    2
#define TD30tip_blue    3
#define TD30tip_bt40    4
#define TD30tip_bt40_L  5

ini_taula_opcions(TD30tip,0x78,0x5e)    // td
  {0x54,0x5c,0   ,0   ,0,0},        // no      sense TD30
  {0x78,0x5e,0x4f,0x3f,0,0},        // td30    amb td30
  {0x78,0x38,0x07,0x3f,0,0},        // tL70    a traves lluminos
  {0x7c,0x06,0x1c,0x79,0x78,0x5c},  // bluEto  a traves bluetooth
  {0x7c,0x78,0x66,0x3f,0,0},        // bt40    a traves bt40
  {0x7c,0x78,0x66,0x3f,0x08,0x38},  // bt40_L  a traves bt40 per canal lluminos serie 
fin_taula_opcions(TD30tip)


int tipus_impressora_fora_limits(char value)
{
  return(value >= DIM(textesIMPREtip));
}




__no_init uchar  modif_reloj;



#define TC_NUM   1
#define TC_CLEAR 2
#define TC_ENTER 3
#define TC_OFF   4


struct s_grab  aux_bufgrab;
struct s_grab  new_bufgrab;
struct s_grab  sav_bufgrab;

// *********************************************************************

void desencrip_i_newgrab(void)
{
      memcpy((char*)&new_bufgrab,(char*)&aux_bufgrab,sizeof(struct s_grab));
      if(new_bufgrab.n_grabs == 0)
          return; // no es pot descontar i no caldra regrabar
      new_bufgrab.n_grabs--;
  
}

// ********************************************************************

struct s_time  auxtime;

unsigned char digit_hora_auxtime_ok(uchar nd,uchar tnum)
{
 switch(nd)
    {
     case 0:
       if(tnum>2)
         return(0);
       if(tnum==2)
         auxtime.hour &=0xf3; /* no passi de 23h */
       return(1);
     case 1:
       if(auxtime.hour<0x20)
         return(1);
       else
         return(tnum<4);
     case 2:
       return(tnum<6);
     case 3:
       return(1);
    default:
      return(0);
    }
}


unsigned char digit_dia_auxtime_ok(uchar nd,uchar tnum)
{
 switch(nd)
    {
     case 0:
       if((auxtime.day & 0xf0)==0)
          return(tnum>0);
       if((auxtime.day & 0xf0)==0x10)
          return(1);
       if(((auxtime.day & 0xf0)==0x20) && (auxtime.month!=0x02))
          return(1);
       if(auxtime.month==0x02)
          return((tnum<9 )||!(bcd_bin(auxtime.year) & 0x03));
       return(tnum<=(dias_mes[auxtime.month]-30));
     case 1:
       if(auxtime.month==0x02)
          return(tnum<3);
       return(tnum<4);
     case 2:
       if(auxtime.month& 0x10)
         return(tnum<3);
       return(tnum>0);
     case 3:
       return(tnum<2);
    default:
      return(0);
    }
}


uchar  valnum;

int hay_cargador(void)
{
int ret;
OS_Delay(3);
  if(!status_i2c(EPROM_ARXIUS_CARG))
          ret = 1;
  else
    ret = 0;
  return ret;
}

unsigned char tabla_puls[12]={0xde,0xb7,0xd7,0xe7,0xbb,0xdb,0xeb,0xbd,0xdd,0xed,0xbe,0xee};

unsigned char tecant ;
unsigned char timetec ; //!!!! tx50  (timtec)

void teclat_carg_init(void)
{
tecant = 15;
timetec = 0 ;
}

const unsigned char CODITEC_1[]=  // PER CODIFICAR TECLAT  1 
{
 0,10,7,7,4,4,4,4,1,1,1,1,1,1,1,1,
 0,11,8,8,5,5,5,5,2,2,2,2,2,2,2,2,
 37,12,9,9,6,6,6,6,3,3,3,3,3,3,3,3,
};

unsigned char llegir_teclat_carg(void)
{
unsigned char i,tecla;
signed char mask;
unsigned char in_i2c;
    for(i=0,mask=0xbf;i<48;i+=16,mask >>=1)
      {
       output_i2c(TECLAT_CARG, mask);  // SELECCIONAR P6,P5,P4 
       in_i2c = input_i2c(TECLAT_CARG);
       tecla=CODITEC_1[i+((in_i2c  & 0x0F) ^ 0x0F)];
       if(tecla)
         break;
      }
   if (tecant!=tecla)
      {tecant=tecla; return tecla;}
   else return 15;

}


uchar get_tecla_cargador(void)
{
uchar  tnum;
 if(!hay_cargador())
    return(TC_OFF);
 if (timetec==0)
    {
     tnum=llegir_teclat_carg();
     if(tnum)
       {
        if(tnum==11) tnum=0;
        if(tnum<10)
             {
              //*tn=tnum;
              valnum=tnum;
              return(TC_NUM);
             }
        if(tnum==10)
           return(TC_CLEAR);
        if(tnum==12)
           return(TC_ENTER);
       }
    }
 return(0);
}

const uchar  tau_pos[4]={4,3,1,0}; /* posicio a disp digit interm. */

uchar entrar_hora(void)
{
//uchar tnum;
uchar  nd,tecla;
unsigned char fin;
 fin=0;
 nd=0;
 if(!modif_reloj)
    DISP_DIG_FLASH(1,2);
 DISP_DIG_FLASH(1,4);
 display(1,NF_HORA,(uchar *)&auxtime.minute);
 do
    {
     do
       {
        if(!modif_reloj)
          {
           get_reloj(RELLOTGE_CARG,&auxtime);
           display(1,NF_HORA,(uchar *)&auxtime.minute);
          }
        tecla=get_tecla_cargador(/*&tnum*/);
        if(tecla)
          break;
        OS_Delay(8L);
       }while(1);
     switch(tecla)
       {
         case TC_NUM:
           if(digit_hora_auxtime_ok(nd,valnum))
              {
               DISP_DIG_FIXE(1,2);
               modif_reloj=1;
               set_nibble(valnum,3-nd,(uchar *)&auxtime.minute);
               DISP_DIG_FIXE(1,tau_pos[nd]);
               display(1,NF_HORA,(uchar *)&auxtime.minute);
               if(++nd>3)
                  nd=0;
               DISP_DIG_FLASH(1,tau_pos[nd]);
              }
           break;
         case TC_CLEAR:
           break;
         case TC_ENTER:
         case TC_OFF:
           DISP_DIG_FIXE(1,tau_pos[nd]);
           DISP_DIG_FIXE(1,2);
           fin=1;
           break;
       }
    }while(!fin);
 return(tecla);
}
     
     

uchar entrar_dia_auxtime(void)
{
uchar  nd,tecla,auxtnum;
unsigned char fin;
 fin=0;
 confi_format_diames(xram.tarcom.formddmm);
 nd=3;
 if(!modif_reloj)
     DISP_DIG_FLASH(1,2);
 DISP_DIG_FLASH(1,(ndigdisp_ddmm[nd]));
 display(1,NF_DIA,(uchar *)&auxtime.day);
 do
    {
     do
       {
        if(!modif_reloj)
          {
           get_reloj(RELLOTGE_CARG,&auxtime);
           display(1,NF_DIA,(uchar *)&auxtime.day);
           display(2,NF_TEXT4,&xram.tarcom.text_disem[dia_de_la_semana_from_time(&auxtime)].text[0]);
          }
        tecla=get_tecla_cargador();
        if(tecla)
          break;
        OS_Delay(8L);
       }while(1);
     switch(tecla)
       {
         case TC_NUM:
           if(digit_dia_auxtime_ok(nd,valnum))
              {
               if(valnum!=get_nibble(nd,(uchar *)&auxtime.day))
                 {
                  DISP_DIG_FIXE(1,2);
                  modif_reloj=1;
                  set_nibble(valnum,nd,(uchar *)&auxtime.day);
                  switch(nd)
                     {
                      case 0:
                        break;
                      case 1:
                        if(valnum==0)
                          auxtnum=1;
                        else
                          auxtnum=0;
                        set_nibble(auxtnum,0,(uchar *)&auxtime.day);
                        break;
                      case 2:
                        auxtime.day=1;
                        break;
                      case 3:
                        auxtime.day=1;
                        if(valnum==0)
                          auxtnum=1;
                        else
                          auxtnum=0;
                        set_nibble(auxtnum,2,(uchar *)&auxtime.day);
                        break;
                     }
                  display(1,NF_DIA,(uchar *)&auxtime.day);
                  display(2,NF_TEXT4,&xram.tarcom.text_disem[dia_de_la_semana_from_time(&auxtime)].text[0]);
                 }
               DISP_DIG_FIXE(1,(ndigdisp_ddmm[nd]));
               if(nd==0)
                  nd=3;
               else
                  nd--;
               DISP_DIG_FLASH(1,(ndigdisp_ddmm[nd]));
              }
           break;
         case TC_CLEAR:
           break;
         case TC_ENTER:
         case TC_OFF:
           DISP_DIG_FIXE(1,(ndigdisp_ddmm[nd]));
           DISP_DIG_FIXE(1,2);
           fin=1;
           break;
       }
    }while(!fin);
 return(tecla);
} 



uchar entrar_any_auxtime(void)
{
uchar  nd,tecla;
unsigned char fin;
 fin=0;
 nd=1;
 if(!modif_reloj)
     DISP_DIG_FLASH(1,4);
 DISP_DIG_FLASH(1,1);
 display(1,NF_ANY,(uchar *)&auxtime.year);
 do
    {
     do
       {
        if(!modif_reloj)
          {
           get_reloj(RELLOTGE_CARG,&auxtime);
           display(1,NF_ANY,(uchar *)&auxtime.year);
          }
        tecla=get_tecla_cargador();
        if(tecla)
          break;
        OS_Delay(8L);
       }while(1);
     switch(tecla)
       {
         case TC_NUM:
               DISP_DIG_FIXE(1,4);
               if(valnum!=get_nibble(nd,(uchar *)&auxtime.year))
                 {
                  modif_reloj=1;
                  set_nibble(valnum,nd,(uchar *)&auxtime.year);
                  auxtime.day=1;
                  auxtime.month=1;
                 }
               DISP_DIG_FIXE(1,nd);
               display(1,NF_ANY,(uchar *)&auxtime.year);
               if(nd)
                  nd=0;
               else
                  nd=1;
               DISP_DIG_FLASH(1,nd);
           break;
         case TC_CLEAR:
           break;
         case TC_ENTER:
         case TC_OFF:
           DISP_DIG_FIXE(1,nd);
           DISP_DIG_FIXE(1,4);
           fin=1;
           break;
       }
    }while(!fin);
 return(tecla);
}


void shl_nib(uchar n,uchar *p)
{
//uchar  i,aux,mask;
uchar  i,aux,mask;
 mask=0;
 aux=*p >> 4;
 for(i=0;i<(n+1)/2;i++)
   {
    p[i]=p[i]<<4 | mask;
    mask=aux;
    aux=p[i+1] >> 4;
   }
 if(n & 0x1)
    p[i-1]&=0x0f;
}


//
// rutines auxiliars entrada opcio per carregador
//

void opcio_enable_all(t_def_taula_opcions* taula)
{
  memset(taula->enabled, 1, taula->n_max);
}

void opcio_disable(t_def_taula_opcions* taula, int n_opcio)
{
  taula->enabled[n_opcio] = 0;
}

char opcio_next_enabled(t_def_taula_opcions* taula,char n)
{
int i;
  for(i=0;i<taula->n_max;i++)// per evitar loop infinit cas cap enabled
  {
     n++;
     if(n >= taula->n_max)
       n = 0;
     if(taula->enabled[n])
       return(n);
  }
  return(0);
}

char opcio_first_enabled(t_def_taula_opcions* taula,char n)
{
  if(n >= taula->n_max)
    n=0;
  if(taula->enabled[n] == 0)    // si opcio desactivada
    n = opcio_next_enabled(taula, n);  // busca seguent activada
  return(n);
}


// ***********************************************************************
// ***********************************************************************
//  seleccio opcio per teclat carregador                        
//  crida:                                                      
//        punter a no. opcio ( opcio inicial i retornar-hi valor triat)                       
//        punter a la taula definicio de l'opcio a entrar       
//  retorn:                                                     
//        =0 entrada abortada per desconexio carregador         
//        =1 variable llegida i colocada al seu lloc            
//        a n_opcio retorn valor seleccionat                    
// ***********************************************************************
// ***********************************************************************

char opcio_triar(char *n_opcio, t_def_taula_opcions* taula)
{
char n;
int fin = 0;
unsigned char tecla;

  display(0,NF_LEDEST,(void*)taula->ledest);
  n = opcio_first_enabled(taula,*n_opcio);
  do{
    display(1,NF_TEXT6,(void*)taula->textes[n]);
     do {
         tecla=get_tecla_cargador();
        if(tecla)
          break;
        OS_Delay(8L);
       }while(1);
     switch(tecla)
       {
         case TC_CLEAR:
           n = opcio_next_enabled(taula, n);  // busca seguent activada
           break;
         case TC_ENTER:
         case TC_OFF:
           fin=1;
           break;
         default:
           break;
       }
    }while(!fin);
  *n_opcio = n;
  if(tecla==TC_OFF)
    return(0);
  return(1);         
}

/***************************************************************/
/* entrada de un valor per teclat carregador                   */
/* crida:                                                      */
/*       no. format d'entrada                                  */
/*       adressa de la variable a entrar                       */
/*       display per taxim/ carreg.  0/1   (afegit a v 1.07)   */
/* retorn:                                                     */
/*       =0 entrada abortada per desconexio carregador         */
/*       =1 variable llegida i colocada al seu lloc            */
/*                                                             */
/***************************************************************/
unsigned char entrar_item(uchar nf,void *pvar, char disp_carg)
{
uchar  nd,tecla,tipo;
unsigned char fin;
uchar  ndec,ndig;
unsigned long  laux;float  faux;uchar  caux[10];
void *  paux;
 tipo=formatos[nf].tipo;
 ndig=formatos[nf].blk_flsh_ndig & 0x07;
 switch(tipo)
   {
    case F_LONG:
        laux=*(long*)pvar;
        paux=&laux;
        break;
    case F_FLOAT:
        faux=*(float*)pvar;
        paux=&faux;
        ndec=formatos[nf].dec;
        if(ndec)  // cas decimals 
          {
            if(ndec==1)
              ndec=  tarcom.NDEC ;
            else ndec--;
          }
        break;
    case F_BCD:
        memcpy(caux,(uchar*)pvar,(ndig+1)/2);
        paux=caux;
        break;
   }
 fin=0;
 nd=0;
 do  
    {
     if(disp_carg) 
       display(3,nf,paux);  // per display carregador
     else
       display(1,nf,paux);  // per display taximetre
     do {
         tecla=get_tecla_cargador();
        if(tecla)
          break;
        OS_Delay(8L);
       }while(1);
     switch(tecla)
       {
         case TC_NUM:
           switch(tipo)
             {
              case F_LONG:
              case F_FLOAT:
                  nd++;
                  if(nd==1 || nd>6 || laux==0){nd=1;laux=0;}
                  laux=laux*10+valnum;
                  if(tipo==F_FLOAT)
                    faux=(float)laux/FKDEC[ndec];
                  break;
              case F_BCD:
                  nd++;
                  if(nd==1 || nd>ndig){nd=1;memset(caux,0,3);}
                  shl_nib(ndig,caux);
                  caux[0] |=valnum;
                  if(valnum==0 && nd==1)
                    nd=0;
                  break;
             }
           break;
         case TC_CLEAR:
           nd=0;
           switch(tipo)
             {
              case F_LONG:
                  laux=0;
                  break;
              case F_FLOAT:
                  faux=0;
                  break;
              case F_BCD:
                  memset(caux,0,3);
                  break;
             }
           break;
         case TC_ENTER:
         case TC_OFF:
           fin=1;
           break;
       }
    }while(!fin);
 if(tecla==TC_OFF)
    return(0);
 switch(tipo)
   {
    case F_LONG:
        *(long*)pvar=laux;
        break;
    case F_FLOAT:
        *(float*)pvar=faux;
        break;
    case F_BCD:
        memcpy((uchar*)pvar,caux,(ndig+1)/2);
        break;
   }
 return(1);
}

// **************************************************************
//  entrada de un valor alfanumeric per teclat carregador                   
//  crida:                                                      
//        num caracters                                 
//        adressa de la variable a entrar                       
//  retorn:                                                     
//        =0 entrada abortada per desconexio carregador         
//        =1 variable llegida i colocada al seu lloc            
// *************************************************************

const uchar ARRAY_ALFANUM_MAXTEC[10]=   // numero de tecles per cada fila
{
 1,9,8,7,7,7,9,9,7,9,
};

const uchar ARRAY_ALFANUM[10][9]=
{
 '0',' ',' ',' ',' ',' ',' ',' ',' ',
 '1',' ','+','-','*',',','=',':','.',
 '2','A','B','C','a','b','c','@',' ',
 '3','D','E','F','d','e','f',' ',' ',
 '4','G','H','I','g','h','i',' ',' ',
 '5','J','K','L','j','k','l',' ',' ',
 '6','M','N','�','O','m','n','�','o',
 '7','P','Q','R','S','p','q','r','s',
 '8','T','U','V','t','u','v',' ',' ',
 '9','W','X','Y','Z','w','x','y','z',
};

#define DEC_SEC_ALFANUM		12     // decimes  segon per tecla alfanumerica
#define DEC_SEC_ALFANUM_FLASH   7      // decimes  segon per intermitent


#define NDIG_DISP_CARR  8   // digits display carregador

void display_carregador(unsigned char dir, unsigned char nb, char *buf)
{
int i;
char buf_aux[NDIG_DISP_CARR];

  if(nb > NDIG_DISP_CARR)
    nb = NDIG_DISP_CARR;
  for(i=0;i<nb;i++)
  {
    if(buf[i] == '�')
      buf_aux[i] = 0xa5;    // codif. � a display carregador
    else if(buf[i] == '�')
        buf_aux[i] = 0xa5;  // codif. � a display carregador
    else 
        buf_aux[i] = buf[i];
  }
  write_i2c(DISPLAY_CARG,dir,nb,buf_aux);
}

int treure_blancs(char *buf)
{
  int len,ini,fin,i,j;
  len = strlen(buf);
  do
  {
      if(len == 0)
        break;
      for(ini=0;ini<len;ini++)
        if(buf[ini] != ' ')
          break;
      if(ini == len)
      {
        buf[0] = 0;
        len = 0;
        break;
      }
      for(fin = (len-1);fin > ini;fin--)
        if(buf[fin] != ' ')
          break;
      for(i=ini,j=0;i<=fin;i++,j++)  
        buf[j] = buf[i];
      buf[j] = 0;
      len = j;
  }while(0);
  return(len);
}

enum estats_alfanum { EALFA_WAIT_CHAR, EALFA_ENTER_CHAR, EALFA_WAIT_CLR};
struct s_alfn
{
int input_pos;
int nc_ini_disp;
int nc;
int nc_disp;
int nc_max; 
int half;
enum estats_alfanum estat;
char bufdisp[NDIG_DISP_CARR];
char* buf;
} alfn;

void next_char(int hi_ha_tecla)
{
       if(alfn.nc < alfn.nc_max)
       {
         // es pot entrar un nou caracter
//        alfn.nc++; 
//        alfn.input_pos = (alfn.nc -1)- alfn.nc_ini_disp;
        alfn.input_pos = alfn.nc - alfn.nc_ini_disp;
        if(alfn.input_pos >= NDIG_DISP_CARR)
        {
         alfn.input_pos--; 
         alfn.nc_ini_disp++; 
         memcpy(alfn.bufdisp,alfn.buf+alfn.nc_ini_disp,NDIG_DISP_CARR);
        }
        alfn.bufdisp[alfn.input_pos] = 'l';
        display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);
        if(hi_ha_tecla)
        {
           tx30.timer_alfanum = DEC_SEC_ALFANUM;
           alfn.estat = EALFA_ENTER_CHAR;
        }
        else
        {
          alfn.estat = EALFA_WAIT_CHAR;
          tx30.timer_alfanum = DEC_SEC_ALFANUM_FLASH;
          alfn.half = 0;
        }
       }
       else
       {
         tx30.timer_alfanum = 0;
         alfn.estat = EALFA_WAIT_CLR;
       }
}

void prev_char(void)
{
           if(alfn.nc )
           {
             // es pot eliminar un caracter
            alfn.nc--; 
            if(alfn.nc_ini_disp)
              if(alfn.estat != EALFA_WAIT_CLR)
                alfn.nc_ini_disp--;
            alfn.nc_disp = min(alfn.nc,NDIG_DISP_CARR);
            memset(alfn.bufdisp,' ',NDIG_DISP_CARR);
            memcpy(alfn.bufdisp,alfn.buf+alfn.nc_ini_disp,alfn.nc_disp);
            alfn.input_pos = alfn.nc  - alfn.nc_ini_disp;
            alfn.bufdisp[alfn.input_pos] = 'l';
            display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);
            alfn.estat = EALFA_WAIT_CHAR;
            tx30.timer_alfanum = DEC_SEC_ALFANUM_FLASH;
            alfn.half = 0;
           }
}

int entrar_item_alfanum(char ncMax, char* buf)
{
char  tecla;
char fila_alfanum,col_alfanum;
char fin=0;

    alfn.nc_max = ncMax;
    alfn.buf = buf;
    alfn.buf[alfn.nc_max] = 0;
    alfn.nc = treure_blancs(alfn.buf);
 
    // display valor inicial
    alfn.nc_ini_disp = 0;
    alfn.nc_disp = min(alfn.nc,NDIG_DISP_CARR);
    memset(alfn.bufdisp,' ',NDIG_DISP_CARR);
    memcpy(alfn.bufdisp,alfn.buf,alfn.nc_disp);
    display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);  
    while((alfn.nc-alfn.nc_ini_disp)>NDIG_DISP_CARR)
    {
      OS_Delay(150);
      alfn.nc_ini_disp++;
      memcpy(alfn.bufdisp,alfn.buf+alfn.nc_ini_disp,NDIG_DISP_CARR);
      display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);  
    }
    OS_Delay(150);
    
    next_char(0);

    do  
    {
      OS_Delay(50L);
      if(tx30.timer_alfanum == 1)
      {
       switch(alfn.estat)
       {
         case EALFA_WAIT_CHAR:
             if(alfn.half == 0)
             {
              alfn.bufdisp[alfn.input_pos] = ' ';
             }
             else
             {
              alfn.bufdisp[alfn.input_pos] = 'l';
             }
            tx30.timer_alfanum = DEC_SEC_ALFANUM_FLASH;
            alfn.half = !alfn.half;
//            display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);  // indicador a display
            display_carregador(alfn.input_pos,1,alfn.bufdisp+alfn.input_pos); // nomes refresca cursor intermitent 
           break;
         case EALFA_ENTER_CHAR:
              alfn.nc++;
              next_char(0);
           break;
       }
      }          
      tecla=get_tecla_cargador();
      if(tecla)
           switch(tecla)
             {
               case TC_CLEAR:
                  tx30.timer_alfanum = 1;
                  prev_char();
                  break;
               case TC_ENTER:
               case TC_OFF:
                 fin=1;
                 break;
               case TC_NUM:
                   if(alfn.estat == EALFA_WAIT_CHAR)
                          {
                           fila_alfanum=valnum;
                           col_alfanum=0;
                          }
                   else if(alfn.estat == EALFA_ENTER_CHAR)
                   {
                     if(fila_alfanum != valnum )
                     {
                        alfn.nc++;
                        next_char(1);
                        fila_alfanum=valnum;
                        col_alfanum=0;
                     }
                  }
                   if(alfn.estat == EALFA_WAIT_CLR)
                     break;
                   
                   *(alfn.buf +alfn.nc_ini_disp + alfn.input_pos)= alfn.bufdisp[alfn.input_pos] = ARRAY_ALFANUM[fila_alfanum][col_alfanum];
                   if(++col_alfanum>=ARRAY_ALFANUM_MAXTEC[fila_alfanum])
                          col_alfanum=0;
                   display_carregador(0,NDIG_DISP_CARR,alfn.bufdisp);  
                   if(ARRAY_ALFANUM_MAXTEC[valnum]==1)
                   {
                     tx30.timer_alfanum = 1;   // per que salti inmediatament
                   }
                   else
                   {
                   tx30.timer_alfanum = DEC_SEC_ALFANUM;
                   }
                   alfn.estat = EALFA_ENTER_CHAR;
                 break;
             }
    }while(!fin);
 alfn.buf[alfn.nc] = 0;
 if(tecla == TC_OFF)
    return(0);
 return(1);
}


uchar  tecla_car;
unsigned long  impulsos,distancia;
__no_init unsigned long nueva_k;

uint metros;


uchar  nbt;
float  vf_t,vf_real,vf_real_ps,raux;
uchar  ctm1;
unsigned int  intaux;



unsigned char medicion_k(void)
{
unsigned char fin=0;
t_gi gi;
char luminosoTipo;
char tipoImpre;
char hi_ha_td30;
char num_torns_control_horari;

 gi = tarcom.gi; 
 do
    {
     opcio_enable_all(&GItip); 
     if(tipoTx == TX40)
       opcio_disable(&GItip, GItip_Enc);     //  TX40 no te opcio gi encriptat
     if(!opcio_triar(&gi.giTipo,&GItip))  // Gi 
           {
            fin=1; break;  // cas TC_OFF  
           }
     
     if(gi.giTipo == GItip_Ana)
     {
         opcio_enable_all(&GInivell); 
         if(!opcio_triar(&gi.giNivell,&GInivell)) 
           {
            fin=1; break;  // cas TC_OFF  
           }
         display(0,NF_LEDEST,(void*)TEXT_LEDEST_TRIG);
         do{
             if(!entrar_item(NF_K,&gi.giTrigger,0))
               {
                fin=1; break;  // cas TC_OFF  
               }
             if((gi.giTrigger <giTrigger_MIN)||(gi.giTrigger >giTrigger_MAX))
             {
               gi.giTrigger = giTrigger_DEFAULT;
               buzzer_set(BUZZER_TIME_TECLA*4);
               continue;
             }
             break;
            }while(1);  // fins entrar valor correcte
         if(fin==1)
           break;  // cas TC_OFF 
     }
     tarcom.gi = gi;    // per que interrupcio ho tingui
     
     if(xram0.tarcom.luminTipoFixe)
       tarcom.luminosoTipo = xram0.tarcom.luminTipoFixe - 1;
     else
     {
       luminosoTipo = tarcom.luminosoTipo;
       opcio_enable_all(&LUMINtip);
       if(!opcio_triar(&luminosoTipo,&LUMINtip)) 
             {
              fin=1; break;  // cas TC_OFF  
             }
       tarcom.luminosoTipo = luminosoTipo;
     }
     
     tipoImpre = tarcom.tipus_impressora;
     opcio_enable_all(&IMPREtip);
     if(!opcio_triar(&tipoImpre,&IMPREtip)) 
           {
            fin=1; break;  // cas TC_OFF  
           }
     tarcom.tipus_impressora = tipoImpre;
     
     // determina opcions valides per td30
     if(xram.tarcom.tr_ccab)
       hi_ha_td30 = 0;    // si hi ha computercab no hi pot haver TD30
     else
     {
         opcio_enable_all(&TD30tip);
         
         if((tarcom.tipus_impressora == IMPREtip_ext || tarcom.tipus_impressora == IMPREtip_Opt)
            || (xram.tarcom.newtransm == 0))     // versio 2.12
           opcio_disable(&TD30tip,TD30tip_td30);

         if(tarcom.luminosoTipo != LUMINtip_tl70)
           opcio_disable(&TD30tip,TD30tip_tl70);
         
         if(taula_tipo_lumin[tarcom.luminosoTipo] & LLUMINOS_SERIE)
           opcio_disable(&TD30tip, TD30tip_bt40_L);
         
         hi_ha_td30 = tarcom.hi_ha_td30;
           
         if(!opcio_triar(&hi_ha_td30,&TD30tip)) 
           {
            fin=1; break;  // cas TC_OFF  
           }
     }
     tarcom.hi_ha_td30 = hi_ha_td30;
     
     
       
       
// A versio 2.14 recuperem 1/2 torns 
       
         if(xram.tarcom.minuts_max_torn)
         {
           // cas hi ha control horari.Mira si hi ha 1 o 2 torns (conductors)
           if(xram.tarcom.minuts_max_torn_2 == 0)
           {
             num_torns_control_horari = 0;
           }
           else
           {
             // triar si es treballa amb 1 o 2 conductors
             if(tarcom.num_torns_control_horari > 0)
               num_torns_control_horari = tarcom.num_torns_control_horari - 1;
             else
               num_torns_control_horari = 0;
             opcio_enable_all(&CONTRHORnumconds);
             if(!opcio_triar(&num_torns_control_horari,&CONTRHORnumconds)) 
                   {
                    fin=1; break;  // cas TC_OFF  
                   }
           }
           tarcom.num_torns_control_horari = num_torns_control_horari + 1;
         }
// final  1 / 2 torns      
     
     
     instala_k(1000,&gi);
     display(0,NF_LEDEST,(void*)TEXT_LEDEST_KI);
     impulsos=0;
     display(1,NF_K,(void*)&impulsos);
     do{
        metros=get_clear_COLA_MF();
        if(metros)
          {
           impulsos+=metros;
           display(1,NF_K,(void *)&impulsos);
           OS_Delay(50L);
          }
        tecla_car=get_tecla_cargador();
        if((tecla_car == TC_ENTER)||(tecla_car== TC_OFF))
          break;
        OS_Delay(8L);
       }while(1);     /* espera enter */
     if(tecla_car==TC_OFF)
       {
        fin=1;  break;
       }
     if(impulsos)
        {
         display(0,NF_LEDEST,(void*)TEXT_LEDEST_DI);
         distancia=0;
         if(!entrar_item(NF_K,&distancia,0))
           {
            fin=1; break;  // cas TC_OFF  
           }
         if(distancia)
            impulsos=((long)impulsos*((long)xram.tarcom.yardas))/distancia;
         else
            impulsos=0;  /* per que doni error */
        }
     else
        {  // entrar K directament 
         display(0,NF_LEDEST_FLASH,(void*)TEXT_LEDEST_K);
         impulsos=0;
         if(!entrar_item(NF_K,&impulsos,0))
           {
            fin=1; break;  // cas TC_OFF  
           }
         if(impulsos == 0)   // afegit a versio 1.04
           impulsos = nueva_k;   // si no s'ha entrat res, mante valor actual
        }
     if(k_fora_limits(impulsos))
         nueva_k=0;
     else
         nueva_k=impulsos;
     break;
    }while(1);
 
 if(nueva_k)
 {
   instala_k(nueva_k,&gi);
   tarcom.gi = gi;
 }
 return(fin);
}

   
unsigned char tratamiento_reloj(void)
{
unsigned char fin;
 fin=0;
 do
    {
           if(!reloj_ok(RELLOTGE_CARG))
             {
              confi_reloj(RELLOTGE_CARG,0);
              memcpy((uchar *)&auxtime,"\x0\x0\x0\x0\x01\x01\x90\x00",8); /* 1/1/91 Martes */
              set_reloj(RELLOTGE_CARG,&auxtime);
             }
           modif_reloj=0;
           display(0,NF_LEDEST,(void *)&xram.tarcom.lede_rel[2]);
           //tecla_car=entrar_any(&auxtime);
           tecla_car=entrar_any_auxtime();
           if(tecla_car==TC_CLEAR)
              break;
           if(tecla_car==TC_OFF)
              {
               fin=1;
               break;
              }
           display(0,NF_LEDEST,(void *)&xram.tarcom.lede_rel[1]);
           //tecla_car=entrar_dia(&auxtime);
           tecla_car=entrar_dia_auxtime();
           APAGAR_DISPLAY(2);
           if(tecla_car==TC_CLEAR)
              break;
           if(tecla_car==TC_OFF)
              {
               fin=1;
               break;
              }

           display(0,NF_LEDEST,(void *)&xram.tarcom.lede_rel[0]);
           tecla_car=entrar_hora();
           if(tecla_car==TC_CLEAR)
              break;
           if(tecla_car==TC_OFF)
              {
               fin=1;
               break;
              }
           if(modif_reloj)
             {
              auxtime.s100=0;
              auxtime.sec=0;
               //  auxtime.wday=dia_de_la_semana_from_time(&auxtime);
              confi_reloj(RELLOTGE_CARG,0);
              set_reloj(RELLOTGE_CARG,&auxtime);
             }
           get_reloj(RELLOTGE_CARG,&auxtime);
           confi_reloj(RELLOTGE_PLACA,0);
           set_reloj(RELLOTGE_PLACA,&auxtime);
           tx30.ajuste_rel_last_anydia=0;   // v2.20b
           tx30.ajuste_rel_minuts=0;
           break;
    }while(1);
 return(fin);
}
                          

unsigned char fecha_fab_ok(void)
{
uchar  diaux;
if(!es_bcd(blqs.f_fab[2]))
   return(0);
if(!es_bcd(blqs.f_fab[1]) || (blqs.f_fab[1]>0x12)|| (blqs.f_fab[1]==0))
   return(0);
diaux=bin_bcd(dias_mes[blqs.f_fab[1]]);
if((diaux==0x28) && ((bcd_bin(blqs.f_fab[2]) & 0x03)==0 ))
  diaux=0x29;
if(!es_bcd(blqs.f_fab[0]) || (blqs.f_fab[0]==0) || (blqs.f_fab[0]>diaux))
   return(0);
 return(1);
}


uchar  op_regrab,op_fabrica,op_repar,op_modif_c;

void calcul_op_regrab(void)
{
 op_regrab=0;
    if(xram0.tarcom.CCC[0]==0x5b &&
       xram0.tarcom.CCC[1]==0xdf &&
       xram0.tarcom.DDD[0]==0x37 &&
       xram0.tarcom.DDD[1]==0x81 &&
       xram0.tarcom.SS==0x76 &&
       xram0.tarcom.TTT[0]==0x3d &&
       xram0.tarcom.TTT[1]==0xaa)
     op_regrab=5;

 else if(blqs.DDD[0]==0 && blqs.DDD[1]==0 && blqs.SS==0)
   op_regrab=1;
 else
    {
     if(memcmp(xram0.tarcom.DDD,blqs.DDD,2)==0)
       {
        if(xram0.tarcom.SS==0)
           op_regrab=2;
        else
          {
           if(xram0.tarcom.SS==blqs.SS)
             op_regrab=3;
           else
             if(memcmp(xram0.tarcom.TTT,blqs.DDD,2)==0)
                op_regrab=4;
          }
       }
     else
        if(memcmp(xram0.tarcom.TTT,blqs.DDD,2)==0)
           op_regrab=4;
    }
}

unsigned char hay_fecha_fab(void)
{
uchar  chk;
 chk=blqs.f_fab[2]+blqs.f_fab[1]+blqs.f_fab[0];
 return(fecha_fab_ok() && (chk==blqs.chk_ff));
}

uchar  item_act[3];

//
// nomes es fa servir per carregador reparacio(NO per visual. parametres)
//              
#define PARMS_C_FIN   11    // es salta els tres ultims
#define PARMS_L_FIN   20 
#define PARMS_r_FIN   28 

const uchar  item_ini[3]={0,PARMS_L_INI,PARMS_r_INI};
const uchar  item_fin[3]={PARMS_C_FIN,PARMS_L_FIN,PARMS_r_FIN};

void visu_next_item(uchar nb)
{
 uchar  nv;
 if(item_act[nb]>item_fin[nb])
    item_act[nb]=item_ini[nb];
 nv=item_act[nb];
 display(0,NF_LEDEST,(void*)visu_parms[nv].led);//!!!!tx50
 display(1,visu_parms[nv].nform,visu_parms[nv].p);
 ++item_act[nb];
 if(nb==0 && nv==0)
    ++item_act[nb];  /* per saltarse verif k */
}


void get_tc_until_tec(void)
{
 do{
    tecla_car=get_tecla_cargador();
    if(tecla_car)
      break;
    OS_Delay(8L);
   }while(1); /* espera tecla */
}
                                    

const struct s_clear_x  taula_clears_repar[]=
{ 
 sizeof  blqs.f_inst         ,blqs.f_inst         ,
 sizeof  blqs.n_descs        ,( char*)&blqs.n_descs,
 sizeof  blqs.n_lic          ,blqs.n_lic          ,
 sizeof  blqs.CCC            ,&blqs.CCC[0]        ,
 sizeof  blqs.DDD            ,&blqs.DDD[0]        ,
 sizeof  blqs.CCC_inst       ,&blqs.CCC_inst[0]   ,
 sizeof  blqs.SS             ,&blqs.SS            ,
 sizeof  contar_salts_inst   ,&contar_salts_inst  ,
 sizeof  blqs.texte_nom      ,blqs.texte_nom      ,
 sizeof  blqs.texte_dni      ,blqs.texte_dni      ,
 sizeof  blqs.texte_llicencia,blqs.texte_llicencia,
 sizeof  blqs.texte_matricula,blqs.texte_matricula,
 sizeof  blqs_ext.texte_H1,blqs_ext.texte_H1,
 sizeof  blqs_ext.texte_H2,blqs_ext.texte_H2,
 sizeof  blqs_ext.texte_H3,blqs_ext.texte_H3,
 sizeof  blqs_ext.texte_H4,blqs_ext.texte_H4,
 sizeof  blqs_ext.texte_H5,blqs_ext.texte_H5,
 sizeof  blqs_ext.texte_H6,blqs_ext.texte_H6,
 sizeof  blqs_ext.texte_F1,blqs_ext.texte_F1,
 sizeof  blqs_ext.texte_F2,blqs_ext.texte_F2,
 sizeof  blqs_ext.texte_F3,blqs_ext.texte_F3,
 sizeof  blqs_ext.texte_F4,blqs_ext.texte_F4,
 sizeof  blqs_ext.texte_F5,blqs_ext.texte_F5,
 sizeof  blqs_ext.texte_F6,blqs_ext.texte_F6,
 
 sizeof  blqs_ext.array_text,(char*)blqs_ext.array_text,  // versio 1.11
 sizeof	 blqs_ext_32, (char*)&blqs_ext_32,
 0,
};

unsigned char reparacion(void)
{

 memcpy(item_act,item_ini,3);

 display(0,NF_LEDEST,(void*)TEXT_LEDEST_REP);
 APAGAR_DISPLAY(1);

 while(1)
   {
    get_tc_until_tec();

    if(tecla_car== TC_OFF)
        return(1); /* desconectat carregador */
    if(tecla_car==TC_ENTER)
       return(0);

    if(tecla_car!=TC_NUM)
       continue;
    switch(valnum)
       {
        case 1:
        case 2:
        case 3:
           visu_next_item(valnum-1);
           break;
        case 9:
           display(0,NF_LEDEST,(void*)TEXT_LEDEST_REP);
           display(1,NF_AMD,(uchar*)&auxtime.day);
           get_tc_until_tec();   /* espera tecla */
           if(tecla_car== TC_OFF)
               return(1); /* desconectat carregador */
           if(tecla_car== TC_ENTER)
              {
               memcpy(blqs.f_exp,(uchar*)&auxtime.day,3);
               ++blqs.n_reps;
               blqs.nt[0]=0xab;
               blqs.nt[1]=0xcd;
               clear_xdata(taula_clears_repar);

               blqs.chk_blqs = calcul_chk_blqs();
               grabar_eprom_tabla(GE_BLQS);//EPROM_TAX,EEPT_BLQS_POS,EEPT_BLQS_LEN,(uchar *)&blqs,
			   
               blqs_ext.chk_blqs_ext = calcul_chk_blqs_ext();
               grabar_eprom_tabla(GE_BLQS_EXT);//EPROM_TAX,EEPT_BLQS_EXT_POS,EEPT_BLQS_EXT_LEN,(uchar *)&blqs,
			   
			   blqs_ext_32.chk_blqs_ext = calcul_chk_blqs_ext_32();
			   grabar_eprom_tabla(GE_BLQS_EXT_32);
			   
               memset(fecha_paro,0x55,3);
               grabar_fp();
               leer_verif_fp();
               memset(cods_tax.cods,0,sizeof cods_tax.cods);
               grabar_cods();
               memset(cods_tax_ext.cods,0,sizeof cods_tax_ext.cods);
               grabar_cods_ext();
              }
              APAGAR_DISPLAY(1);
            break;
        default:
          break;
       }  /* switch */
   }   /* do */
}

unsigned char entrada_modif_c(void)
{
unsigned long sav_nmodifs_prog;
int i,j;

 display(0,NF_LEDEST,(void*)TEXT_LEDEST_C6);
 if(!entrar_item(NF_K_T,(uchar*)&blqs.nmodifs_k,0))
     return(1);   /* cas TC_OFF  */
 display(0,NF_LEDEST,(void*)TEXT_LEDEST_C7);
 if(!entrar_item(NF_K_T,(uchar*)&blqs.nmodifs_tar,0))
     return(1);   /* cas TC_OFF  */
 sav_nmodifs_prog = blqs.nmodifs_prog;
 if(sav_nmodifs_prog > MAX_MODIFS_PROG)
 {
   sav_nmodifs_prog = 0;
   blqs.nmodifs_prog = 0;
 }
 display(0,NF_LEDEST,(void*)TEXT_LEDEST_CP);
 if(!entrar_item(NF_K_T,(uchar*)&sav_nmodifs_prog,0))
     return(1);   // cas TC_OFF 
 if(sav_nmodifs_prog <= blqs.nmodifs_prog)
 {
   // guarda las ultimes programacions  (puja les ultimes)
   for(i=0,j=blqs.nmodifs_prog - sav_nmodifs_prog;i<sav_nmodifs_prog;i++,j++)
     blqs.modif_prog[i] = blqs.modif_prog[j];
   blqs.nmodifs_prog = sav_nmodifs_prog;
 }
 
 blqs.chk_blqs=calcul_chk_blqs();
 //grabar_eprom(EPROM_TAX,EEPT_BLQS_POS,
 //                       EEPT_BLQS_LEN,
 //                       (uchar *)&blqs);
 grabar_eprom_tabla(GE_BLQS);
 return(0);
}

const struct s_clear_x  taula_clears_alta_tax[]=
{
 sizeof nueva_k           ,   (char*)&nueva_k           ,
 sizeof contar_salts_inst ,   (char*)&contar_salts_inst ,
 sizeof blqs.nmodifs_k    ,   (char*)&blqs.nmodifs_k    ,
 sizeof blqs.nmodifs_tar  ,   (char*)&blqs.nmodifs_tar  ,
 sizeof blqs.nmodifs_prog ,   (char*)&blqs.nmodifs_prog ,
 0,
};


void guarda_versio_prog_a_blqs(void)
{
			   blqs.modif_prog[blqs.nmodifs_prog].version = bcd_short(VERSION_TX);
			   blqs.modif_prog[blqs.nmodifs_prog].check = reverse_int(check_programa);
			   time_to_fechhor(&blqs.modif_prog[blqs.nmodifs_prog].fecha , &gt.time);
			   blqs.nmodifs_prog++;
			   blqs.chk_blqs=calcul_chk_blqs();  
			   grabar_eprom_tabla(GE_BLQS);      //grabar_eprom(EPROM_TAX,EEPT_BLQS_POS+offsetof(struct s_blqs,n_descs),sizeof blqs.n_descs,(unsigned char *)&blqs.n_descs);
	
}


unsigned char alta_taximetro(void)
{
uchar  sav_serie[3],sav_ffab[3];
 display(0,NF_LEDEST,(void*)TEXT_LEDEST_N_S);
 if(!entrar_item(NF_BCD5,(uchar*)&blqs.n_serie,0))
    {
     return(1);   /* cas TC_OFF  */
    }

 /* mira si es primera vez para grabar fecha fabricacion */

 memcpy(sav_serie,blqs.n_serie,3);
 if(hay_fecha_fab())
    memcpy(sav_ffab,blqs.f_fab,3);
 else
     memcpy(sav_ffab,(uchar *)&auxtime.day,3);
 memset((uchar*)&blqs,0,sizeof blqs);
 memcpy(blqs.n_serie,sav_serie,3);
 memcpy(blqs.f_fab,sav_ffab,3);
 memcpy(blqs.f_exp,(uchar *)&auxtime.day,3);
 blqs.chk_ff=blqs.f_fab[0]+blqs.f_fab[1]+blqs.f_fab[2];

 //nueva_k=0;
 //contar_salts_inst=0;
 //blqs.nmodifs_k=0;
 //blqs.nmodifs_tar=0;
 //blqs.nmodifs_prog = 0;
 clear_xdata(taula_clears_alta_tax);
  
 // guarda modificacio programa i escriu blqs
 guarda_versio_prog_a_blqs();
 blqs.chk_blqs=calcul_chk_blqs();

 grabar_eprom_tabla(GE_BLQS); //EPROM_TAX,EEPT_BLQS_POS,EEPT_BLQS_LEN,(uchar *)&blqs,
 memset(fecha_paro,0x55,3);
 grabar_fp();
 leer_verif_fp();
         memset(cods_tax.cods,0,sizeof cods_tax.cods);
         grabar_cods();
         memset(cods_tax_ext.cods,0,sizeof cods_tax_ext.cods);
         grabar_cods_ext();

 display(0,NF_LEDEST,(void*)TEXT_LEDEST_r5);
 if(!entrar_item(NF_BCD4,(uchar*)&blqs.r5,0))
    {
     return(1);   /* cas TC_OFF  */
    }
 display(0,NF_LEDEST,(void*)TEXT_LEDEST_r6);
 if(!entrar_item(NF_BCD5,(uchar*)&blqs.r6,0))
    {
     return(1);   /* cas TC_OFF  */
    }

 return(0);
}
        
        
        
unsigned char hay_que_descontar(void)
{
 return(!(op_regrab==5  || op_regrab==1 || xram0.tarcom.serie[1]>0x99 ||
        memcmp(blqs.nt,xram0.tarcom.serie,sizeof blqs.nt)==0));
}



/*
unsigned short calcul_check_visu(void)
{
unsigned short  chk = 0;

#ifdef prov
unsigned int  adr_bt;
uchar  k,msk;
uchar bmsk;
unsigned short iaux;
uchar  * adr_msk;
 for(adr_bt=0,chk=0,adr_msk=&xram_8031.mem[LEN_TARIFA_8031-1032];adr_bt<(LEN_TARIFA_8031);)
    {
     for(bmsk=0x01,msk=*adr_msk++;bmsk;bmsk<<=1,adr_bt++)
       if(bmsk&msk)
         {
          k=adr_bt%7;                         
          iaux=(unsigned short)xram_8031.mem[adr_bt];
          chk+=( (iaux>>k)|(iaux<<(16-k)));      // td-tx  _iror_(iaux,k);
         }
    }
// if(chk==0xa808)
//	 chk=0x8b32;
#endif
 return(chk);
}
*/



void recalcul_params_tar(void)
{
float  fs;
float  tics_d,tics_t,factor;
unsigned long  tics,i_factor;
unsigned long     KK;
unsigned long     KH;
char  modo; 
struct s_bloc_tar blocaux;

/***********************************************************************/
/*****************   calcul parametres tarifacio    ********************/
/***********************************************************************/
  tarcom.k=nueva_k;
  tarcom.DIVK=nueva_k/DIVISOR_MAX_K + 1;
  raux=(100.*((float)tarcom.k))/(1000.*(float)tarcom.DIVK) +0.5;
  tarcom.pmpk0=(int)raux;
  raux=(100.*((float)tarcom.k))/(1000.*(float)tarcom.DIVK)/1.609 +0.5;
  tarcom.pmpk0_ccab=(int)raux;
  //tarcom.fvelo=3240./(((float)tarcom.k)/(1000.*(float)tarcom.DIVK));   // 3600/(k*1.111131) 
  velocimetro_confi(tarcom.k,tarcom.DIVK,1);


  pk_tot_tot=tarcom.pmpk0;
  pk_tot_tot_ccab=tarcom.pmpk0_ccab;
  pk_lib_tot=tarcom.pmpk0;
  pk_oc_tot=tarcom.pmpk0;
  pk_off_tot=tarcom.pmpk0;
  pk_lpasj_tot=tarcom.pmpk0;
  servcom.pk_lib_serv=tarcom.pmpk0;
  servcom.pk_lib_pasj=tarcom.pmpk0;
  servcom.pk_oc_serv=tarcom.pmpk0;
  tx30.tarifa_rot=0;
  tx30.acum_off=0;
  if(tarcom.hay_liocaut)
     {
      raux=(tarcom.m_liocaut*((float)tarcom.k))/(1000.*(float)tarcom.DIVK) +0.5;
      tarcom.m_liocaut_fk=(unsigned short)raux;
     }
  if(tarcom.hay_paglibaut)
     {
      raux=(tarcom.m_paglibaut*((float)tarcom.k))/(1000.*(float)tarcom.DIVK) +0.5;
      tarcom.m_paglibaut_fk=(unsigned short)raux;
     }
  if(tarcom.hay_fslibdist)
     {
      raux=(tarcom.m_fslibdist*((float)tarcom.k))/(1000.*(float)tarcom.DIVK) +0.5;
      tarcom.m_fslibdist_fk=(unsigned short)raux;
     }

  tarcom.max_acc1_fk =(unsigned short)((float)tarcom.max_acc1 * (float)tarcom.k /(1000.*(float)tarcom.DIVK)); 
  tarcom.max_acc2_fk =(unsigned short)((float)tarcom.max_acc2 * (float)tarcom.k /(1000.*(float)tarcom.DIVK));
  tarcom.min_pulses_per_sec =(unsigned short)((float) tarcom.vel_zap_change * (float)tarcom.k /(3600.*(float)tarcom.DIVK)+.5);


 KK = tarcom.k/tarcom.DIVK;                  
 KH = 3600L*FACTOR_HZ;            
 
 for(nbt=0;nbt<tarcom.nblocs_tar;nbt++)
   {
    memcpy(&blocaux,&bloc_tar[nbt],sizeof blocaux);
    
    
    // versio 1.04G (per arreglar tarifa feta amb vf i contadors separats, sense canviar tarifa)
    if(blocaux.tipt_vf == 1)
    {
      blocaux.tipt_acs = 0;
      blocaux.vf_fija = 0;
    }
    
    // INI: ANTONIO XAPU BCN CARRETERA
	blocaux.tarifarDistancia_ps=1;
	blocaux.tarifarDistancia_ss=1;
	if((blocaux.tipt_vf==2) && (blocaux.tipt_acs==1))
	{
		if(	(blocaux.bits_cuenta & 0x44) && 
			(blocaux.impkm_ss==0.0) && 
			(blocaux.vf_fija!=0.0) && 
			(blocaux.impho_ss>0.0) )
		{
			blocaux.impkm_ss=(blocaux.impho_ss/blocaux.vf_fija);
			blocaux.tarifarDistancia_ss=0;//false
			blocaux.tipt_vf=1;
			blocaux.tipt_acs=0;
		}
		if(	(blocaux.bits_cuenta & 0x11) &&
			(blocaux.m_ps==0.0) && 
			(blocaux.vf_fija!=0.0) &&
			(blocaux.seg_ps>0.0) )
		{
			blocaux.m_ps=(blocaux.vf_fija/3.6)*blocaux.seg_ps;
			blocaux.tarifarDistancia_ps=0;//false
			blocaux.tipt_vf=1;
			blocaux.tipt_acs=0;
		} 
	} 
	// FIN: ANTONIO XAPU BCN CARRETERA      
    // ***** fraccions salt succesius salts ******************

    modo=0;
    if(blocaux.bits_cuenta & 0x44)  // k_ss  oc pag  
      {
       modo=1;
       tics_d=(KK*(float)blocaux.vsal[0])/(blocaux.impkm_ss*KDEC[tarcom.NDEC]);
      } 
    if(blocaux.bits_cuenta & 0x88)  // h_ss  oc pag  
      {
       modo+=2;  
       tics_t=(3600.*FACTOR_HZ*(float)blocaux.vsal[1])/(blocaux.impho_ss*KDEC[tarcom.NDEC]);
      }
    switch(modo)
      {
       case 0:
         // no conta ni temps ni distancia
         // valors nomes serveixen per cas canvi tarifa
         // per convertir fraccio de salt que ja hi hagues feta
         blocaux.FACTOR_N_ss=10;
         blocaux.FS_K_ss=1000;
         blocaux.KK_ss=1000;
         blocaux.FS_H_ss=1000;
         blocaux.KH_ss=1000;
         break;
       case 1:  // no conta temps
         tics=(unsigned long)tics_d;
         blocaux.FS_H_ss=1000;
         blocaux.KH_ss=1000;
         break;
       case 2:  // no conta distancia
         tics=(unsigned long)tics_t;
         blocaux.FS_K_ss=1000;
         blocaux.KK_ss=1000;
         break;  
       case 3:
         if(tics_d<tics_t)
           tics=(unsigned long)tics_d;
         else  
           tics=(unsigned long)tics_t;
         break;      
      }
    if(modo)
      {  
       if(tics<100)
         blocaux.FACTOR_N_ss=i_sqrt(tics);
       else
         blocaux.FACTOR_N_ss=tics/10;
       if(modo & 0x01)
         {      
          fs=(blocaux.impkm_ss*blocaux.FACTOR_N_ss*KDEC[tarcom.NDEC])/blocaux.vsal[0];
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_K_ss = (unsigned long)(fs*(float)i_factor);
             blocaux.KK_ss = (unsigned long)(KK*(float)i_factor);
            }            
          else
            {
             blocaux.FS_K_ss = (unsigned long)fs;
             blocaux.KK_ss = KK;
            }
         }
       if(modo & 0x02)
         {      
          fs=(blocaux.impho_ss*blocaux.FACTOR_N_ss*KDEC[tarcom.NDEC])/blocaux.vsal[1];
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_H_ss = (unsigned long)(fs*(float)i_factor);
             blocaux.KH_ss = (unsigned long)(KH*(float)i_factor);
            }            
          else
            {
             blocaux.FS_H_ss = (unsigned long)fs;
             blocaux.KH_ss=KH;
            }
         }   
      }
    //****** fraccions salt primer salt *********************

    modo=0;
    if(blocaux.bits_cuenta & 0x11)  // k_ps  oc pag  
      {
       modo=1;
       //tics_d=(KK*(float)blocaux.m_ps)/(xram.tarcom.yardas*KDEC[tarcom.NDEC]);
       tics_d=(KK*(float)blocaux.m_ps)/tarcom.yardas;
      } 
    if(blocaux.bits_cuenta & 0x22)  // h_ps  oc pag  
      {
       modo+=2;  
       //tics_t=(FACTOR_HZ*(float)blocaux.seg_ps)/KDEC[tarcom.NDEC];
       tics_t=FACTOR_HZ*(float)blocaux.seg_ps;
      }
    switch(modo)
      {
       case 0:
         // no conta ni temps ni distancia
         // valors nomes serveixen per cas canvi tarifa
         // per convertir fraccio de salt que ja hi hagues feta
         blocaux.FACTOR_N_ps=10;
         blocaux.FS_K_ps=1000;
         blocaux.KK_ps=1000;
         blocaux.FS_H_ps=1000;
         blocaux.KH_ps=1000;
         break;
       case 1:  // no conta temps
         tics=(unsigned long)tics_d;
         blocaux.FS_H_ps=1000;
         blocaux.KH_ps=1000;
         break;
       case 2:  // no conta distancia
         tics=(unsigned long)tics_t;
         blocaux.FS_K_ps=1000;
         blocaux.KK_ps=1000;
         break;  
       case 3:
         if(tics_d<tics_t)
           tics=(unsigned long)tics_d;
         else  
           tics=(unsigned long)tics_t;
         break;      
      }
    if(modo)
      {  
       if(tics<100)
         blocaux.FACTOR_N_ps=i_sqrt(tics);
       else
         blocaux.FACTOR_N_ps=tics/10;
       if(modo & 0x01)
         {      
          //fs=(1000.*blocaux.FACTOR_N_ps*KDEC[tarcom.NDEC])/blocaux.m_ps;
          // fs=(1000.*blocaux.FACTOR_N_ps)/blocaux.m_ps;
          fs=(tarcom.yardas*blocaux.FACTOR_N_ps)/blocaux.m_ps;    // canviat a 2.12D 
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_K_ps = (unsigned long)(fs*(float)i_factor);
             blocaux.KK_ps = (unsigned long)(KK*(float)i_factor);
            }            
          else
            {
             blocaux.FS_K_ps = (unsigned long)fs;
             blocaux.KK_ps = KK;
            }
         }
       if(modo & 0x02)
         {      
          //fs=(3600.*blocaux.FACTOR_N_ps*KDEC[tarcom.NDEC])/blocaux.seg_ps;
          fs=(3600.*blocaux.FACTOR_N_ps)/blocaux.seg_ps;
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_H_ps = (unsigned long)(fs*(float)i_factor);
             blocaux.KH_ps = (unsigned long)(KH*(float)i_factor);
            }            
          else
            {
             blocaux.FS_H_ps = (unsigned long)fs;
             blocaux.KH_ps=KH;
            }
         }   
      }
    memcpy(&bloc_tar[nbt],&blocaux,sizeof blocaux);
	

}  // final loop tarifas 
}






void copia_textes_a_reg(void)
{
 memcpy(reg_tcks_tx30->texte_nom , blqs_ext_32.texte_nom ,sizeof(reg_tcks_tx30->texte_nom));
 memcpy(reg_tcks_tx30->texte_nom_32, blqs_ext_32.texte_nom, sizeof(reg_tcks_tx30->texte_nom_32));
 memcpy(reg_tcks_tx30->texte_dni , blqs.texte_dni ,sizeof(reg_tcks_tx30->texte_dni));		    
 memcpy(reg_tcks_tx30->texte_llicencia , blqs.texte_llicencia ,sizeof(reg_tcks_tx30->texte_llicencia));		    
 memcpy(reg_tcks_tx30->texte_matricula , blqs.texte_matricula ,sizeof(reg_tcks_tx30->texte_matricula));

 memcpy(reg_tcks_tx30->texte_H1_32 , blqs_ext_32.texte_H1 ,sizeof(reg_tcks_tx30->texte_H1_32));
 memcpy(reg_tcks_tx30->texte_H2_32 , blqs_ext_32.texte_H2 ,sizeof(reg_tcks_tx30->texte_H2_32));
 memcpy(reg_tcks_tx30->texte_H3_32 , blqs_ext_32.texte_H3 ,sizeof(reg_tcks_tx30->texte_H3_32));
 memcpy(reg_tcks_tx30->texte_H4_32 , blqs_ext_32.texte_H4 ,sizeof(reg_tcks_tx30->texte_H4_32));
 memcpy(reg_tcks_tx30->texte_H5_32 , blqs_ext_32.texte_H5 ,sizeof(reg_tcks_tx30->texte_H5_32));
 memcpy(reg_tcks_tx30->texte_H6_32 , blqs_ext_32.texte_H6 ,sizeof(reg_tcks_tx30->texte_H6_32));
 memcpy(reg_tcks_tx30->texte_F1_32 , blqs_ext_32.texte_F1 ,sizeof(reg_tcks_tx30->texte_F1_32));
 memcpy(reg_tcks_tx30->texte_F2_32 , blqs_ext_32.texte_F2 ,sizeof(reg_tcks_tx30->texte_F2_32));
 memcpy(reg_tcks_tx30->texte_F3_32 , blqs_ext_32.texte_F3 ,sizeof(reg_tcks_tx30->texte_F3_32));
 memcpy(reg_tcks_tx30->texte_F4_32 , blqs_ext_32.texte_F4 ,sizeof(reg_tcks_tx30->texte_F4_32));
 memcpy(reg_tcks_tx30->texte_F5_32 , blqs_ext_32.texte_F5 ,sizeof(reg_tcks_tx30->texte_F5_32));
 memcpy(reg_tcks_tx30->texte_F6_32 , blqs_ext_32.texte_F6 ,sizeof(reg_tcks_tx30->texte_F6_32));

 memcpy(reg_tcks_tx30->texte_H1 , blqs_ext_32.texte_H1 ,sizeof(reg_tcks_tx30->texte_H1));		    
 memcpy(reg_tcks_tx30->texte_H2 , blqs_ext_32.texte_H2 ,sizeof(reg_tcks_tx30->texte_H2));		    
 memcpy(reg_tcks_tx30->texte_H3 , blqs_ext_32.texte_H3 ,sizeof(reg_tcks_tx30->texte_H3));		    
 memcpy(reg_tcks_tx30->texte_H4 , blqs_ext_32.texte_H4 ,sizeof(reg_tcks_tx30->texte_H4));		    
 memcpy(reg_tcks_tx30->texte_H5 , blqs_ext_32.texte_H5 ,sizeof(reg_tcks_tx30->texte_H5));		    
 memcpy(reg_tcks_tx30->texte_H6 , blqs_ext_32.texte_H6 ,sizeof(reg_tcks_tx30->texte_H6));		    
 memcpy(reg_tcks_tx30->texte_F1 , blqs_ext_32.texte_F1 ,sizeof(reg_tcks_tx30->texte_F1));		    
 memcpy(reg_tcks_tx30->texte_F2 , blqs_ext_32.texte_F2 ,sizeof(reg_tcks_tx30->texte_F2));		    
 memcpy(reg_tcks_tx30->texte_F3 , blqs_ext_32.texte_F3 ,sizeof(reg_tcks_tx30->texte_F3));		    
 memcpy(reg_tcks_tx30->texte_F4 , blqs_ext_32.texte_F4 ,sizeof(reg_tcks_tx30->texte_F4));		    
 memcpy(reg_tcks_tx30->texte_F5 , blqs_ext_32.texte_F5 ,sizeof(reg_tcks_tx30->texte_F5));		    
 memcpy(reg_tcks_tx30->texte_F6 , blqs_ext_32.texte_F6 ,sizeof(reg_tcks_tx30->texte_F6));		    
 
  memcpy(reg_tcks_tx30->texte1 , blqs_ext.array_text[0].texte ,sizeof(reg_tcks_tx30->texte1));		    
  memcpy(reg_tcks_tx30->texte2 , blqs_ext.array_text[1].texte ,sizeof(reg_tcks_tx30->texte2));		    
  memcpy(reg_tcks_tx30->texte3 , blqs_ext.array_text[2].texte ,sizeof(reg_tcks_tx30->texte3));		    
  memcpy(reg_tcks_tx30->texte4 , blqs_ext.array_text[3].texte ,sizeof(reg_tcks_tx30->texte4));		    
  memcpy(reg_tcks_tx30->texte5 , blqs_ext.array_text[4].texte ,sizeof(reg_tcks_tx30->texte5));		    
  memcpy(reg_tcks_tx30->texte6 , blqs_ext.array_text[5].texte ,sizeof(reg_tcks_tx30->texte6));		    
  memcpy(reg_tcks_tx30->texte7 , blqs_ext.array_text[6].texte ,sizeof(reg_tcks_tx30->texte7));		    
  memcpy(reg_tcks_tx30->texte8 , blqs_ext.array_text[7].texte ,sizeof(reg_tcks_tx30->texte8));		    
  memcpy(reg_tcks_tx30->texte9 , blqs_ext.array_text[8].texte ,sizeof(reg_tcks_tx30->texte9));		    
  memcpy(reg_tcks_tx30->texte10, blqs_ext.array_text[9].texte ,sizeof(reg_tcks_tx30->texte10));		    

}


int cargar_configuracion(int tipus_device,char *buf, unsigned short* p_check)
{
int st=0;

    stream_close();
    st=stream_open(tipus_device,buf);
    if(st)
      return(st);
    
    *p_check = 0;    
    init_alloc_ifac();
    do{
      
    st=leer_ticket_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    st=leer_defregs_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    st=leer_regs_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
        
    // per tots els registres amb estructura
    reg_continuar_ticket=(T_DREG_CONTINUAR_TICKET*)reg[REG_CONTINUAR_TICKET].buf;
    reg_moneda=(T_DREG_MONEDA*)reg[REG_MONEDA].buf; 
    reg_servei = (T_DREG_SERVEI*)reg[REG_SERVEI].buf;
    reg_prima_torn = (T_DREG_PRIMA_TORN*)reg[REG_PRIMA_TORN].buf;
    reg_prima_torn_aux = (T_DREG_PRIMA_TORN*)reg[REG_PRIMA_TORN_AUX].buf;
    reg_prima_servei = (T_DREG_PRIMA_SERVEI*)reg[REG_PRIMA_SERVEI].buf;
    reg_prima_servei_aux = (T_DREG_PRIMA_SERVEI*)reg[REG_PRIMA_SERVEI_AUX].buf;

    reg_tcks_tx30=(T_DREG_TCKS_TX30*)reg[REG_TCKS_TX30].buf;
    copia_textes_a_reg();
    
    st = leer_tablas_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    prima_init();   // inicialitza torns i serveis
    
    st=leer_textgen_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    st = leer_opcts_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    st=leer_textsys_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}

    st=leer_bmp_paqram(p_check);
    if(st) 
        {buzzer_set(BUZZER_TIME_ERROR);break;}
    
    }while(0);
    
    stream_close();
    
return(st);  
}


typedef struct{
  char* dest;
  int len_dest;
  char* orig;
  int len_orig;
}t_taula_copia;

#define exp_item_blqs(item)   blqs.item, sizeof blqs.item, xram.textes_tiquet.item, sizeof xram.textes_tiquet.item
#define exp_item_blqs_ext(item)   blqs_ext.item, sizeof blqs_ext.item, xram.textes_tiquet.item, sizeof xram.textes_tiquet.item

const t_taula_copia taula_copia[] =
{
  //blqs.texte_nom, sizeof blqs.texte_nom, xram.textes_tiquet.texte_nom, sizeof xram.textes_tiquet.texte_nom,
  exp_item_blqs(texte_nom),
  exp_item_blqs(texte_dni),
  exp_item_blqs(texte_llicencia),
  exp_item_blqs(texte_matricula),
  exp_item_blqs_ext(texte_H1),  
  exp_item_blqs_ext(texte_H2),  
  exp_item_blqs_ext(texte_H3),  
  exp_item_blqs_ext(texte_H4),  
  exp_item_blqs_ext(texte_H5),  
  exp_item_blqs_ext(texte_H6),  
  exp_item_blqs_ext(texte_F1),  
  exp_item_blqs_ext(texte_F2),  
  exp_item_blqs_ext(texte_F3),  
  exp_item_blqs_ext(texte_F4),  
  exp_item_blqs_ext(texte_F5),  
  exp_item_blqs_ext(texte_F6),  
};

void copia_textes_tarifa_a_blqs(void)
{
  int i;
  for(i=0;i<DIM(taula_copia);i++)
  {
    memset(taula_copia[i].dest,0,taula_copia[i].len_dest);
    memcpy(taula_copia[i].dest,taula_copia[i].orig,taula_copia[i].len_orig);
  }
  for(i=0;i<DIM(blqs_ext.array_text);i++)
  {
    memcpy(&blqs_ext.array_text[i].nom, &xram.textes_tiquet.texte_generic[i].nom,2);
    memset(blqs_ext.array_text[i].texte,0,sizeof blqs_ext.array_text[i].texte);
    memcpy(blqs_ext.array_text[i].texte,xram.textes_tiquet.texte_generic[i].texte,LEN_TEXTE_GENERIC);
  }
   blqs.chk_blqs = calcul_chk_blqs();
   grabar_eprom_tabla(GE_BLQS);//EPROM_TAX,EEPT_BLQS_POS,EEPT_BLQS_LEN,(uchar *)&blqs,
   blqs_ext.chk_blqs_ext = calcul_chk_blqs_ext();
   grabar_eprom_tabla(GE_BLQS_EXT);//EPROM_TAX,EEPT_BLQS_EXT_POS,EEPT_BLQS_EXT_LEN,(uchar *)&blqs,
   copia_blqs_ext_a_blqs_ext_32();
}


unsigned long  sav_blqs_k;


typedef enum{MODO_CARGADOR =0, MODO_PRECARGA}eModoCargador;

extern int escri_sector(char *buf, short nsect, int encriptat);

void get_tarifa_lumin(char* luminosoTipo, char* llums_combi)
{
    // cas lluminos tl70 si hi ha bt40 el passa a tipus saludes serie
    if((tarcom.luminosoTipo == LUMINtip_tl70) && (tarcom.hi_ha_td30 != TD30tip_tl70))
      *luminosoTipo = LUMINtip_saludesSerial;
    else
      *luminosoTipo = tarcom.luminosoTipo;
    
    *llums_combi = tarcom.llums_combi;
}

int cargar_tarifa(eModoCargador modo)
{
//unsigned int  aux_n_grabs;
uchar  i,j,st;
ifacFILE *fp;
char* bufaux;
int len_arxiu;

 APAGAR_DISPLAY(0);
 if(modo == MODO_PRECARGA)
   {
    get_reloj(RELLOTGE_PLACA,&auxtime);
    display(1,NF_TEXT_TR,(void*)TEXT_PRE_LOAD);       // cas precambio || || || || || a display 
   }
 else
   {
    get_reloj(RELLOTGE_CARG,&auxtime);
    confi_reloj(RELLOTGE_PLACA,0);
    set_reloj(RELLOTGE_PLACA,&auxtime);
    tx30.ajuste_rel_last_anydia=0;   // v2.20b
    tx30.ajuste_rel_minuts=0;
    
    
    if(hay_que_descontar())
      {
        // mira que encara hi hagi carregador
     st=stream_open(STREAM_E2PROM,NULL);
     if(st)
        return(1);  
     
     // gestio numero gravacions
     fp=IFACfopen("ngrabs.bin",'r',0);
     if(fp==(ifacFILE*)NULL)
     {
        stream_close();
        return(1);  
     }
      len_arxiu = fp->entry.fsize;
      if(len_arxiu == 0 )
      {
        IFACfclose(fp);
        stream_close();
        return(1);  
      }
      IFACfread((char*)&aux_bufgrab, len_arxiu, fp);
 
        if(memcmp((uchar*)&aux_bufgrab,(uchar*)&sav_bufgrab,sizeof sav_bufgrab)) 
        {
            IFACfclose(fp);
            stream_close();
            return(1);
        }
        // reescriu sector directament (!!!! una mica xapu)
        bufaux = malloc(256);
        memset(bufaux,0,256);  // no caldria
        memcpy(bufaux,(char*)&new_bufgrab,sizeof new_bufgrab);
        escri_sector(bufaux,fp->transf_sect,0);
        free(bufaux);
        IFACfclose(fp);
        stream_close();
        
      }
     // actualitza num. gravacions a eprom carregador   (recuperat a versio v 1.07)    
     param_carg.n_grabs_carg++;
     grabar_eprom_tabla(GE_n_grabs_carg);	 //grabar_eprom(EPROM_CARGADOR,offsetof(struct s_param_carg,n_grabs_carg),sizeof param_carg.n_grabs_carg, (uchar *)&param_carg.n_grabs_carg);


     display(1,NF_TEXT_TR,(void*)TEXT_LOAD);         // ===== a display 

   }

  if (!precambio_pend || modo == MODO_PRECARGA) visu_chk = xram0.visu_chk;   
  memcpy((uchar*)&tarcom,(uchar*)&xram.tarcom,sizeof(struct s_tarcom_eprom));
  memcpy(tarifa,xram.tarifa,32*sizeof( struct s_tarifa));
  for(nbt=0;nbt<32;nbt++)
    memcpy(&bloc_tar[nbt],&xram.bloc_tar[nbt],sizeof(struct s_bloc_tar_eprom));
  memcpy(zonh,xram.zonh,sizeof zonh);
  memcpy(tope_imp,xram.tope_imp,sizeof tope_imp);
  memcpy(tope_dist,xram.tope_dist,sizeof tope_dist);
  memcpy(tope_temps,xram.tope_temps,sizeof tope_temps);
  memcpy(canvi_autom,xram.canvi_autom,sizeof canvi_autom);
  
  // per apagar led horaria
  tarcom.lf_HORARIA_OFF = ~tarcom.lf_HORARIA;
 

// prepara leds estat tarifes per envio ir100 
 memset((uchar *)&tarcom.ledtar[0],0x80,sizeof(tarcom.ledtar));
 for(j=0;j<tarcom.ntars;j++)
    tarcom.ledtar[j]=tarifa[j].ledocup[0] | 0x8080;

// recalcula valors que depenen de K

 recalcul_params_tar();
 
      zapper_init();
    
   
// Omplim ajuste_hor per +/- 1 hora
  for (i=0; i<NUM_AJUSTS_HORA; i++)
          {
          ajuste_hor[i].tipo = tarcom.ajuste_hor[i].tipo;
          for (j=0; j<5; j++)
                  ajuste_hor[i].tope[j] = tarcom.ajuste_hor[i].tope[j];
          }
  if(xram.textes_tiquet.hi_ha_textes)
  {
    // copia textes tarifa a blqs i blqs_ext i guarda a e2prom
    copia_textes_tarifa_a_blqs();
  }
  precambio_pend_bt40 = 0;

  // carrega conf. tickets
  if((modo == MODO_PRECARGA) || ((modo == MODO_CARGADOR) && !precambio_pend))
  {
     st = cargar_configuracion(STREAM_RAM,bufferStreamCargador,&check_tiquet);
     if(st)
        return(1);  
  }
  else
  {
    copia_textes_a_reg();
  }
  tx30.estat=E_LIBRE;
  gt.min_ant=0xff;
  gt.dia_ant=0xff;

  luminoso_init();
  format_configurar(tarcom.formddmm, tarcom.tipus_impressora);
  confi_format_diames(tarcom.formddmm);
  
  tx30.nivell_llum_minim = tarcom.nivell_llum_minim;
  
   /* graba a eprom_tax */

   if(op_regrab!=0x99)  // afegit per cas hi ha hagut recarga
	   {
	   blqs.k=tarcom.k;
	   if(blqs.k!=sav_blqs_k)
		 {
		  blqs.nmodifs_k++;
		  if(blqs.nmodifs_k>=10000)
			blqs.nmodifs_k=9999;
		 }
	   if(memcmp(blqs.nt,"\xab\xcd",2)==0)
		  {
		   if(memcmp(tarcom.serie,"\xab\xcd",2))
			  {
			   blqs.nsalts=1;
			   contar_salts_inst=1;
			  }
		  }
	   if(memcmp(blqs.nt,tarcom.serie,sizeof blqs.nt))
		 {
		  blqs.nmodifs_tar++;
		  if(blqs.nmodifs_tar>=10000)
			blqs.nmodifs_tar=9999;
		 }   
	   memcpy(blqs.nt,tarcom.serie,sizeof blqs.nt);
	   memcpy(blqs.n_progt,tarcom.n_progt,sizeof blqs.n_progt);

           // guarda definicio generador impulsos
           blqs.gi = tarcom.gi;
           
           // guarda tipus impressora
           blqs.tipus_impressora = tarcom.tipus_impressora;
           // guarda hi ha td30
           blqs.hi_ha_td30 = tarcom.hi_ha_td30;
	   	   blqs.luminosoTipo = tarcom.luminosoTipo;
           
	   switch(op_regrab)
		  {
		   case 1:
		   case 3:
		   case 4:
			  blqs.SS=tarcom.SS;
                   case 0:
		   case 2:
			  memcpy(blqs.CCC,tarcom.CCC,2);
			  memcpy(blqs.DDD,tarcom.DDD,2);
			  break;
		   default:
			  break;
		  }
	   if (!precambio_pend || modo == MODO_PRECARGA) memcpy(blqs.f_grab,&auxtime.day,3);
	   if(op_regrab != 5)
		  blqs.n_grabs++;

	   blqs.chk_blqs=calcul_chk_blqs();
	   //grabar_eprom(EPROM_TAX,EEPT_BLQS_POS,
	   //                       EEPT_BLQS_LEN,
	   //                       (uchar *)&blqs);
	   grabar_eprom_tabla(GE_BLQS);
	   }

//   definir_tipo_display(1);

 MASK_GEN = 0;
 return(0);
}
 
 
const struct s_parms_tar  input_parms_tar[]={
 0,0xfc,(unsigned long*)&xram.tarifa[0].bb,        NF_IMPORT,
 1,0xbe,(unsigned long*)&xram.bloc_tar[0].vsal[0] ,NF_IMPORT,
 1,0x9c,(unsigned long*)&xram.bloc_tar[0].vsal[1] ,NF_IMPORT,
 1,0xb7,(unsigned long*)&xram.bloc_tar[0].m_ps,    NF_FL_1,
 1,0xed,(unsigned long*)&xram.bloc_tar[0].seg_ps,  NF_FL_1,
 1,0xde,(unsigned long*)&xram.bloc_tar[0].impkm_ss,NF_FL_D,
 1,0xf6,(unsigned long*)&xram.bloc_tar[0].impho_ss,NF_FL_D,
 0,0xf9,(unsigned long*)&xram.tarifa[0].supl,      NF_IMPORT
};




const struct s_clear_x  taula_clears_visu_prec[3]=
{ 
 sizeof visu_f_precambio        ,  visu_f_precambio   ,
 sizeof visu_chk_prec           ,  (char*)&visu_chk_prec      ,
 0,
};


extern void reset_configs_desde_bg40(void);

void configs_finales(eModoCargador modo)
{
uchar i;
unsigned long laux;
int st;
 if(precambio_pend)
    {
    anydia_a_dma(xram0.tarcom.anydia_precambio,visu_f_precambio);
    visu_chk_prec = xram0.visu_chk;   
    }
 else
    {
     clear_xdata(taula_clears_visu_prec);     
    } 
     
 for(i=0;i<TOT_FIN;i++)
 {
    laux = totalitzador_read(i, &st) % 100000000L;  // retalla totalitzadors a 8 digits 
    totalitzador_write(i, laux);
 }

     if((blqs.nmodifs_k==9999) ||(blqs.nmodifs_tar==9999))
          tx30.estat=E_MEM_MAL;
     else if(cods_programat(CODS_CERRAR_TURNO))
     {
          tx30.estat = E_CERRADO;
          tx30.subestat_torn = TORN_TANCAT;
          tx30.timer_tick_torn = 0;
          time_to_fechhor(&reg_prima_torn->fecha_fi,&gt.time); // per que ho agafi com data fi torn anterior
          reset_interserv();
          tarcom.hi_ha_torn_prima_intern = 1;
          prima.torn_obert = 0;
     }
     else
     {
          tx30.estat=E_LIBRE;
          tarcom.hi_ha_torn_prima_intern = 0;
     }
     
     
     // BLOQUEIG TORN     
     bloqueig_torn_configura();
     rearmar_bloqueo_torn(REARMAR_GLOBAL, 0);
     if(tarcom.torn_continu)
     {
      // cas Fran�a comen�a amb torn tancat   2.14
      tx30.estat = E_OFF;
     }

     // verifica cas td30 estigui permes per tarifa (v 2.12)
     if((tarcom.hi_ha_td30 == TD30tip_td30) && (tarcom.newtransm ==0))
     {
        tarcom.hi_ha_td30 = TD30tip_no;
        blqs.hi_ha_td30 = TD30tip_no;
        blqs.chk_blqs=calcul_chk_blqs();
        grabar_eprom_tabla(GE_BLQS); //EPROM_TAX,EEPT_BLQS_POS,EEPT_BLQS_LEN,(uchar *)&blqs,
     }

     // reset bloqueo taxim per sancio cas Fran�a
     if(tarcom.torn_continu)
     {
      memset(fecha_paro,0x55,3);
      grabar_fp();
      leer_verif_fp();
     }
     // calcula check tarifa
     check_sum=calcul_check();
     
     // passa a memoria dades canvis tarifa a e2prom 
     leer_verif_blqs_modifs_tar();

     // guarda dades modificacio tarifa si es diferent de l'anterior ( o es la primera)
     if((blqs_modifs_tar.nmodifs_tar == 0)
         ||(memcmp(blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar-1].num_serie, tarcom.serie,2) != 0)
         ||(blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar-1].check != reverse_int(visu_chk)) )
     {
       if(blqs_modifs_tar.nmodifs_tar >= MAX_MODIFS_TAR)
       {
         // cas ha de perdre el mes antic
         for(i=1;i<MAX_MODIFS_TAR;i++)
           memcpy((char*)&blqs_modifs_tar.modif_tar[i-1],(char*)&blqs_modifs_tar.modif_tar[i], sizeof (struct s_modif_tar));
         blqs_modifs_tar.nmodifs_tar--;
       }
       blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar].num_serie[0] = tarcom.serie[1];
       blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar].num_serie[1] = tarcom.serie[0];
       blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar].check = reverse_int(visu_chk);
       time_to_fechhor(&blqs_modifs_tar.modif_tar[blqs_modifs_tar.nmodifs_tar].fecha , &gt.time);
       blqs_modifs_tar.nmodifs_tar++;
       blqs_modifs_tar.chk_blqs_modifs_tar = calcul_chk_blqs_modifs_tar();  //grabar_eprom(EPROM_TAX,EEPT_BLQS_POS+offsetof(struct s_blqs,n_descs),sizeof blqs.n_descs,(unsigned char *)&blqs.n_descs);
       grabar_eprom_tabla(GE_BLQS_MODIFS_TAR);
     }
     
     MASK_1=0;  // treu bloqueig
     zapper_reset_bloqueig();     // treu bloqueig zapper
     
     multiplex_init_0();    // per reiniciar contador dies sense clau correcte

	// reset configuracions que venen desde bg40
	 reset_configs_desde_bg40();
     
}


uchar precambio_vencido(void)
{
 if(gt.anydia>xram0.tarcom.anydia_precambio) return(1);
 if(gt.anydia==xram0.tarcom.anydia_precambio && (bcd_bin(gt.time.hour)>=xram0.tarcom.hora_precambio))
   return(1);
 return(0);
}

uchar  guardar_en_precambio(void)
{
 if(tx30.estat==E_MEM_MAL) return 0;
 if((xram0.tarcom.con_precambio==0)||precambio_vencido())
    return(0);
 return(1);
}



char toca_precambio(void)
{
   if(precambio_pend_bt40)
     return(PRECAMBIO_BT40);
   if(!precambio_pend || !precambio_vencido())
     return(PRECAMBIO_NO);
   return(PRECAMBIO_CARREGADOR);
}




void leer_eprom_tabla(unsigned char n)
{
 leer_eprom(tabla_crida_eprom[n].nChip,tabla_crida_eprom[n].adr,tabla_crida_eprom[n].nb,tabla_crida_eprom[n].buf);
}


void grabar_eprom_tabla(unsigned char n)
{
//  if(tabla_crida_eprom[n].nChip==EPROM_BASE) 
// eliminat l' if a versio 1.07 per poguer gravar EPROM_CONF_CARG  
   grabar_eprom(tabla_crida_eprom[n].nChip,tabla_crida_eprom[n].adr,tabla_crida_eprom[n].nb,tabla_crida_eprom[n].buf);
}


// pasa a n_lic[3] els 6 digits mes a la dreta de texte_llicencia
// si n'hi ha menys o no son 0-9 acaba

void texte_llicencia_a_n_lic(void)
{
char buf_aux[LEN_TEXTE_LLICENCIA+1];
int i,len;
char c;
  memset(blqs.n_lic,0,3);
  memcpy(buf_aux,blqs.texte_llicencia,sizeof buf_aux);
  treure_blancs(buf_aux);
  len = strlen(buf_aux);
  
  for(i=0;(i<5) && (i<len);i++)
  {
    c = buf_aux[(len-1)-i];
    if( (c<'0') || (c > '9'))
      break;
    c&= 0x0f;
    if(i&0x01)
      c <<= 4;
    blqs.n_lic[i/2] |= c;
  }
}

const struct s_clear_x  taula_clear_restrin[]=
{ 
 sizeof param_carg.restrin,(char*)&param_carg.restrin[0],
 sizeof param_carg.restrin_2,(char*)&param_carg.restrin_2,
 0,
};

// entrada linies texte Header i Foot per tiquets
char * const taula_H[6] =
{
  blqs_ext_32.texte_H1,
  blqs_ext_32.texte_H2,
  blqs_ext_32.texte_H3,
  blqs_ext_32.texte_H4,
  blqs_ext_32.texte_H5,
  blqs_ext_32.texte_H6,
};
char * const taula_F[6] =
{
  blqs_ext_32.texte_F1,
  blqs_ext_32.texte_F2,
  blqs_ext_32.texte_F3,
  blqs_ext_32.texte_F4,
  blqs_ext_32.texte_F5,
  blqs_ext_32.texte_F6,
};

char entrar_textes_tiquet(void)
{
unsigned long l_aux;  
char fin = 0;
int i;
char buf_led[2];

  do{ 
     display(0,NF_LEDEST,(void*)TEXT_LEDEST_LIC1);
     if(!entrar_item_alfanum(LEN_TEXTE_NOM_32,blqs_ext_32.texte_nom))
        {fin = 1; break;}   // cas TC_OFF
     display(0,NF_LEDEST,(void*)TEXT_LEDEST_LIC2);
     if(!entrar_item_alfanum(LEN_TEXTE_DNI,blqs.texte_dni))
        {fin = 1; break;}   // cas TC_OFF
     display(0,NF_LEDEST,(void*)TEXT_LEDEST_LIC3);
     if(!entrar_item_alfanum(LEN_TEXTE_LLICENCIA,blqs.texte_llicencia))
        {fin = 1; break;}   // cas TC_OFF
     texte_llicencia_a_n_lic();    // el guarda en format antic  (BCD)
     display(0,NF_LEDEST,(void*)TEXT_LEDEST_LIC4);
     if(!entrar_item_alfanum(LEN_TEXTE_MATRICULA,blqs.texte_matricula))
        {fin = 1; break;}   // cas TC_OFF
  }while (0);

  if(!fin)
  {
  	  buf_led[0] = 0x76;  // 'H'
	  for(i=0; i<6; i++)
	  {
		 buf_led[1] = SS[i+1];
		 display(0,NF_LEDEST,(void*)buf_led);
		 if(!entrar_item_alfanum(LEN_TEXTE_H_32,taula_H[i]))
			{fin = 1; break;}   // cas TC_OFF
	  }
  }
  if(!fin)
  {
      buf_led[0] = 0x71;  // 'F'
      for(i=0; i<6; i++)
      {
         buf_led[1] = SS[i+1];
         display(0,NF_LEDEST,(void*)buf_led);
         if(!entrar_item_alfanum(LEN_TEXTE_F_32,taula_F[i]))
            {fin = 1; break;}   // cas TC_OFF
      }
  }
  if(!fin)    
  {
      buf_led[0] = 0x78;  // 't'
	  for(i=0;i<10;i++)
	  {
		   int len=LEN_TEXTE_GENERIC;
		   if(i==0) len=10;		//SRI (Ecuador)
		   else if(i==1) len=13;	//RUC (Ecuador)

      	   buf_led[1] = SS[i+1];
		   display(0,NF_LEDEST,(void*)(void*)buf_led);
		   if(!entrar_item_alfanum(len,blqs_ext.array_text[i].texte))
			  {fin = 1; break;}   // cas TC_OFF
	  }
  }
  
  if(!fin)
  {
   display(0,NF_LEDEST,(void*)TEXT_LEDEST_NUMTICKET);  // numero ticket
   l_aux=total[TOT_NTICK].value;
   if(! entrar_item(NF_K,&l_aux, 1))
     fin = 1;
   else
   {
     totalitzador_reset(TOT_NTICK);
     totalitzador_write(TOT_NTICK, l_aux);
   }
  }
  
  DISP_FILL(3,' ');  // apaga display carregador
  return(fin);
}


// ************************************************************************
//  llegeig arxiu tarifa i l'assigna a xram0
// ************************************************************************
const char TEXT_ERROR_DEFAULT[12][6]=
{
 0,0,0xf9,0x40,0x06,0,    //  E.-1   (0)  fallo segmento fos 
 0,0,0xf9,0x40,0x5b,0,    //  E.-2   (1)  fallo luces        
 0,0,0xf9,0x40,0x4f,0,    //  E.-3   (2)  fallo genimp       
 0,0,0xf9,0x40,0x66,0,    //  E.-4   (3)  fallo reloj        
 0,0,0xf9,0x40,0x6D,0,    //  E.-5   (4)  fallo fecha paro   
 0,0,0xf9,0x40,0x7d,0,    //  E.-6   (5)  fallo exceso velo. 
 0,0,0xf9,0x40,0x07,0,    //  E.-7   (6)  bloqueo dia librar o turno excedido. 
 0,0,0xf9,0x40,0x7f,0,    //  E.-8   (7)  bloqueo check_sum tarifa 
 0,0,0xf9,0x40,0x6f,0,    //  E.-9   (8)  bloqueo check_sum programa 
 0,0,0xf9,0x40,0x06,0x3f, //  E.-10  (9)  fallo impresora 
 0,0,0xf7,0x40,0x07,0,    //  A.-7  (10)  aviso 10' para turno excedido. 
 0,0,0xf9,0x40,0x77,0,    //  E.-A  (11)  error zapper 
};


int lectura_arxiu_tarifa(char telecarga)
{
int st;
ifacFILE *farx1;
char *buff_arxiu;
int len_arxiu,llegits;

     st=stream_open(STREAM_RAM,bufferStreamCargador);
     if(st)
        goto retorn;  // no entra
     
     if(!telecarga)
     {
           // gestio numero gravacions
           farx1=IFACfopen("ngrabs.bin",'r',0);
           if(farx1==(ifacFILE*)NULL)
           {
              stream_close();
              goto retorn;  // no entra
           }
            len_arxiu = farx1->entry.fsize;
            if(len_arxiu == 0 )
            {
              IFACfclose(farx1);
              stream_close();
              goto retorn;  // no entra
            }
            llegits=IFACfread((char*)&aux_bufgrab, len_arxiu, farx1);
            IFACfclose(farx1);
            // guarda aux_bufgrab per verificar ( mes endevant) si s'ha tret carregador
            memcpy((char*)&sav_bufgrab,(char*)&aux_bufgrab,sizeof(struct s_grab));
            // prepara descomptar numero gravacions
            desencrip_i_newgrab();
     }
                
     // llegir arxiu tarifa   
     farx1=IFACfopen("ttx_xml.bin",'r',1);
     if(farx1==(ifacFILE*)NULL)
     {
        stream_close();
        goto retorn;  // no entra
     }
      len_arxiu = farx1->entry.fsize;
      if(len_arxiu == 0 )
      {
        IFACfclose(farx1);
        stream_close();
        goto retorn;  // no entra
      }
      buff_arxiu = malloc(len_arxiu+1);
      if(buff_arxiu == NULL)
      {
        stream_close();
        goto retorn;  // no entra
      }
      
//      OS_StopTimer(&tx30Timer1ms);
//      OS_StopTimer(&Impre_Timer);

      llegits=IFACfread(buff_arxiu, len_arxiu, farx1);
      
//      OS_StartTimer(&tx30Timer1ms);
//      OS_StartTimer(&Impre_Timer);
      
      IFACfclose(farx1);
      stream_close();
      if(llegits!= len_arxiu)						
      {
        free(buff_arxiu);
        goto retorn;  // no entra
      }  
            
    // transfereix xml a estructura.
      
    // 
    // inicialitza camps afegits per cas tarifes anteriors no els tinguin  
    //
	  
	memset((void*)&xram0, 0, sizeof xram0);
	

    xram0.tarcom.NUM_supl_max = 3;
    xram0.tarcom.sanciona_descans_curt = 1;
    memcpy(xram0.tarcom.password_paris,password_paris_default,sizeof(xram0.tarcom.password_paris));
    xram0.tarcom.segs_anular_pas_off = 0xff;
    memcpy(xram0.tarcom.texte_PASSW, "\x73\x77\x6d\x6d", 4);  // PASS 
	memset(xram0.tarcom.ajuste_hor,0xff,sizeof(tarcom.ajuste_hor));
	memcpy(xram0.tarcom.text_error, TEXT_ERROR_DEFAULT, sizeof tarcom.text_error);
	xram0.tarcom.pcsup_bb = 1;
	xram0.tarcom.acumula_franq = 1;
	xram0.tarcom.hora_fin_fr2 = 0;
	xram0.tarcom.hora_ini_fr2 = 0;
	xram0.tarcom.min_max_fr2 = 0;
//	xram0.tarcom.tec_preu_acordat = 13;
	strcpy(xram0.tarcom.clau_preu_ac, "perfilsherpan");
        xram0.tarcom.Id_clau_preu_ac = 0;
        xram.tarcom.tisum_s = 0;
	
    st = XML_Associar(buff_arxiu,llegits, taula_DefTarifaTx50, (char*) &xram0);
    
    free(buff_arxiu);
    
// accions per camps afegits posteriorment i que alguna tarifa pot no tenir

    if(xram0.tarcom.veltop_fin_err == 0)
      xram0.tarcom.veltop_fin_err = xram0.tarcom.veltop;  // fins versio 1.04X no existia
    
    
    if(st < 0)
        goto retorn;  // no entra
       
    // check visu 
    xram0.visu_chk = (unsigned short)st; 

    // verifica versio de tarifa valida
    if((xram0.tarcom.versions_valides & VERSION_TARIFA)==0)
       goto retorn;  // eprom tarifa no soporta versio tarifa del taximetre 
    return(0);
// cas produit error    
retorn:
  if(telecarga)
    return(1);
  else
    salta_a_reset();   // salta a 0x60000000
  return 0;
}

void EJECUTAR_PRECAMBIO(char tipus)
{
int st; 
 if(tipus == PRECAMBIO_BT40)
 {
   // cas tarifa rebuda a traves Bt40
   st = lectura_arxiu_tarifa(1);  // des de bt40
   if(st)
   {
    // cas tarifa incorrecta
    precambio_pend_bt40 = 0;
    return;
   }
 }
 RESET_LAPSUS();   // Per que es pugui escriure a Display
 xram = xram0;

 cargar_tarifa(MODO_PRECARGA);

 get_reloj(RELLOTGE_PLACA,&gt.time);
 recalculs_gt();
 gt.canvi_fecha=1;  // per actualitzar fecha paro 

 inici_ajustar_1h();   /* prepara gestio +-1h */

 gt.segs_interm=tarcom.segs_interm;
 gt.mitad_vd=0;

 // anula precarga
 precambio_pend=0;   
 precambio_pend_bt40 = 0;
 
 configs_finales(MODO_PRECARGA);   

 salta_a_reset();   // salta a 0x60000000
}

void gravaTextesTiquets(void){
	blqs.chk_blqs=calcul_chk_blqs();               
	grabar_eprom_tabla(GE_BLQS);
	blqs_ext.chk_blqs_ext = calcul_chk_blqs_ext();
	grabar_eprom_tabla(GE_BLQS_EXT);
	blqs_ext_32.chk_blqs_ext = calcul_chk_blqs_ext_32();
	grabar_eprom_tabla(GE_BLQS_EXT_32);
	copia_textes_a_reg();  // textes de blqs a registre ticket
}

void receiveTextesTiquets(char * buf, int nb){
	int p = 1;
	get_string_sep_comas(buf, blqs_ext_32.texte_nom, p++, LEN_TEXTE_NOM_32, nb);
	get_string_sep_comas(buf, blqs.texte_dni, p++, LEN_TEXTE_DNI, nb);
	get_string_sep_comas(buf, blqs.texte_llicencia, p++, LEN_TEXTE_LLICENCIA, nb);
	get_string_sep_comas(buf, blqs.texte_matricula, p++, LEN_TEXTE_MATRICULA, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H1, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H2, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H3, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H4, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H5, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_H6, p++, LEN_TEXTE_H_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F1, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F2, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F3, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F4, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F5, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext_32.texte_F6, p++, LEN_TEXTE_F_32, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[0].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[1].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[2].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[3].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[4].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[5].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[6].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[7].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[8].texte, p++, LEN_TEXTE_GENERIC, nb);
	get_string_sep_comas(buf, blqs_ext.array_text[9].texte, p++, LEN_TEXTE_GENERIC, nb);
	gravaTextesTiquets();
}

int sendTextesTiquets(int remit, char * buf){
	int nb = sprintf(buf,"q,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,",
				   blqs_ext_32.texte_nom, blqs.texte_dni, blqs.texte_llicencia, blqs.texte_matricula, 
				   blqs_ext_32.texte_H1, blqs_ext_32.texte_H2, blqs_ext_32.texte_H3, blqs_ext_32.texte_H4, blqs_ext_32.texte_H5, blqs_ext_32.texte_H6, 
				   blqs_ext_32.texte_F1, blqs_ext_32.texte_F2, blqs_ext_32.texte_F3, blqs_ext_32.texte_F4, blqs_ext_32.texte_F5, blqs_ext_32.texte_F6,
				   blqs_ext.array_text[0].texte, blqs_ext.array_text[1].texte, blqs_ext.array_text[2].texte, blqs_ext.array_text[3].texte, blqs_ext.array_text[4].texte,
				   blqs_ext.array_text[5].texte, blqs_ext.array_text[6].texte, blqs_ext.array_text[7].texte, blqs_ext.array_text[8].texte, blqs_ext.array_text[9].texte);
	return nb;
}

extern OS_TIMER tx30Timer1ms;
extern OS_TIMER Impre_Timer;

unsigned char  TEXT_PASW[NUM_CODS_MAX];   // per displays import i extres
unsigned char  bufsecre[NUM_CODS_MAX][LEN_CODS];

extern unsigned char get_event_teclat(int mode_carregador,unsigned char*  tecla_num);
// *************************************************************
// *************************************************************
void CARGADOR(void)
{
uchar  un_salto,i,j,nt;
uchar  buf_nt[2];
unsigned char finfunc;
unsigned char fin;
uchar  bld[2];
unsigned int  despl;
unsigned long  laux_sem;
unsigned long  laux;  //  v 1.07
uchar  bor_fp;
uchar* p_var;
int primera_vegada_dia_descans = 1;
int primera_vegada_sem = 1;
unsigned char tnum;
unsigned short mask_passw;
int st, st1;

 teclat_carg_init();

 APAGAR_DISPLAY(0);
 APAGAR_DISPLAY(2);
 RESET_LAPSUS();
 display(1,NF_TEXT_TR,(void*)TEXT_TR);
 
// llegeix tarifa

     // passa a ram e2prom carregador  64K
     stream_close();
     st = stream_open(STREAM_E2PROM,NULL);
     if(st)
        goto retorn;  // no entra
     
     
     st=read_ext_mem(0,stream_num_bytes_used(),bufferStreamCargador);
     stream_close();
     if(st==0)
        goto retorn;  // no entra
 
     
     lectura_arxiu_tarifa(0);  // des de carregador
     // si dona error ja no retorna (salta a 0x60000000)
     
// ********* Afegit a versio 1.03G  *****************  
  // carrega conf. tickets
  if(tx30.estat == E_MEM_MAL)
  {
     st = cargar_configuracion(STREAM_RAM,bufferStreamCargador,&check_tiquet);
     if(st)
        goto retorn;  
  }
// final de l'afegit a versio 1.03G
  
   leer_eprom_tabla(GE_BLQS);
  
  st1 = leer_verif_blqs_ext();
  st = leer_verif_blqs_ext_32();
  if(st == 0 && st1 == 1)
	 copia_blqs_ext_a_blqs_ext_32();
  
   copia_textes_a_reg();  // textes de blqs i blqs_ext a registre ticket
   
   sav_blqs_k=blqs.k;
   leer_verif_cods(); // llegir i verificar codigs secrets 

 
    op_fabrica=0;
    op_repar=0;
    op_modif_c=0;

    //param_carg.restrin[0]=0;
    //param_carg.restrin[1]=0;
    //param_carg.restrin_2 =0;
    clear_xdata(taula_clear_restrin);

    //leer_eprom(EPROM_CARGADOR,0,sizeof(struct s_param_carg),(uchar *)&param_carg);
    leer_eprom_tabla(GE_param_carg);
    
     /* verifica es pot conectar carregador  */

    if(param_carg.restrin[0] == 0x97ad0300) // 241047L amb els bytes canviats
      op_fabrica=1;
    if(param_carg.restrin[1] == 0x17560700) // 480791L amb els bytes canviats
      op_repar=1;
    if(param_carg.restrin_2 == 0xe7450701)  // 17253863L amb els bytes canviats
      op_modif_c=1;

    
 calcul_op_regrab();								  

 if(!op_regrab
      ||(hay_que_descontar() && (aux_bufgrab.n_grabs==0)))  
    goto retorn;        /* no entra carregador  */

    DISP_FIXE(1);
    if((tx30.estat==E_MEM_MAL) || k_fora_limits(tarcom.k))
    {
       tarcom.k = blqs.k;     // agafa k de eeprom_tax
       tarcom.gi = blqs.gi;   // agafa definicio generador impulsos de eeprom_tax
       tarcom.tipus_impressora = blqs.tipus_impressora;
       tarcom.hi_ha_td30 = blqs.hi_ha_td30;
	   tarcom.luminosoTipo = blqs.luminosoTipo;
    }
    if(k_fora_limits(tarcom.k))  // mira que blqs.k fos correcta
       nueva_k=0;
    else
       nueva_k=tarcom.k;
    
    if(giTrigger_fora_limits(tarcom.gi.giTrigger))
    {
      tarcom.gi.giTrigger = giTrigger_DEFAULT;
    }
    if(tipus_impressora_fora_limits(tarcom.tipus_impressora))
       tarcom.tipus_impressora = 0;
       
    if(guardar_en_precambio())
      {
       if((tx30.estat==E_MEM_MAL) || (op_regrab==1))
         goto retorn;
       precambio_pend=1;
      }
    else
      {
       precambio_pend=0;
       xram = xram0;
      } 

     fin=0;
     get_reloj(RELLOTGE_CARG,&auxtime);
      display(0,NF_LEDEST,(void*)TEXT_LEDEST_K);
      if(nueva_k)
        display(1,NF_K,(void *)&nueva_k);
      else
        display(1,NF_TEXT6,(void*)TEXT_K_MAL);

     do
       {
        if(!reloj_ok(RELLOTGE_CARG) || auxtime.year==0x90)
           {
            valnum=2;   // per tractament rellotge
           }
        else
           {
            tecla_car=get_tecla_cargador();
            if(tecla_car==TC_OFF)
               break;            /* cas retirat carregador */
            if(tecla_car!=TC_NUM)
              {
               OS_Delay(50L);
               continue;
              }
           }
        switch(valnum)
           {
            case 0:        /* cargar tarifa */
               if(nueva_k ==0 || !hay_fecha_fab())
                  break;
               st = cargar_tarifa(MODO_CARGADOR);
               if(st)
               {
                  tx30.estat = E_MEM_MAL;  // s'ha tret carregador abans d'hora
                  goto retorn;
               }
               fin=1;
               break;

            case 1:      // inicio medicion k 
               fin = medicion_k();
               break;

            case 2:     /* modificacio any,mesdia,horaminut */
               fin=tratamiento_reloj();
               break;

            case 3:     /* borrado totalizadores */
               display(0,NF_LEDEST,(void*)TEXT_LEDEST_TOT);
               APAGAR_DISPLAY(1);
               get_tc_until_tec();

               if(tecla_car== TC_OFF)
                     {
                      fin=1;
                      break;
                     }
               if(tecla_car==TC_ENTER)
                     {
                       
                        //memset(&total[0],0,sizeof total);
                        for(i=0;i<TOT_FIN;i++)
                          totalitzador_reset(i);
						borra_tots_eq();	

                        pk_tot_tot=tarcom.pmpk0;
                        pk_tot_tot_ccab=tarcom.pmpk0_ccab;
                        pk_oc_tot=tarcom.pmpk0;
                        pk_lpasj_tot=tarcom.pmpk0;
                        servcom.pk_lib_serv=tarcom.pmpk0;
                        servcom.pk_lib_pasj=tarcom.pmpk0;
                        servcom.pk_oc_serv=tarcom.pmpk0;
/* v 2.02                        
                        phora_on=60;
                        phora_oc=60;
*/                        
                        // inicialitza fecha borrat totalitzadors (v 1.04)
                        time_to_fechhor(&blqs.fecha_borr_tots , &gt.time);
                        // i ho grava a e2prom
                         blqs.chk_blqs=calcul_chk_blqs();
                         grabar_eprom_tabla(GE_BLQS);
                     }
#if 0
//!!!!!!!!               
               //leer_ticket_paqram(STREAM_E2PROM);
               leer_prog_pic();
#endif
               
               break;

            case 4:     /* entrada codis secrets */
               mask_passw = xram.tarcom.haypassw;
               if(xram.tarcom.torn_continu)
                 mask_passw &= 0xfff7; // elimina entrada pasword fp cas Fran�a
               if(!mask_passw)
                  break;
               memcpy(bufsecre,cods_tax.cods,sizeof cods_tax.cods);
               memcpy(bufsecre[6],cods_tax_ext.cods,sizeof cods_tax_ext.cods);
               finfunc=0;
               display(0,NF_LEDEST,(void*)TEXT_LEDEST_PASW);                  
               for(j=0;j<NUM_CODS;j++)
                 {
                  if(!((mask_passw>>j)& 0x01))
                     continue;
                  i=0;
                  memset(bufsecre[j],0,sizeof(bufsecre[0]));
                  memset(TEXT_PASW,0,sizeof TEXT_PASW);
                  TEXT_PASW[j]=0x80;
                  display(1,NF_TEXT6,TEXT_PASW);
                  display(2,NF_TEXT4,TEXT_PASW+6);
                  do
                    {
                     if (i<sizeof(bufsecre[0])) 
                        {
                          
                         if(get_event_teclat(1,&tnum) == V_TNUM)
                           bufsecre[j][i++] = tnum +1;
                        }
                     tecla_car=get_tecla_cargador();
                    }while((tecla_car != TC_ENTER)&&(tecla_car!= TC_OFF)&&(tecla_car !=TC_CLEAR));     /* espera enter */
                  if(tecla_car== TC_OFF)
                     {
                      fin=1;
                      break;
                     }
                  if(tecla_car==TC_CLEAR)
                     {
                      finfunc=1;
                      break;
                     }
                 }
               APAGAR_DISPLAY(2);
               if(fin || finfunc)
                  break;
               memcpy(cods_tax.cods,bufsecre,sizeof cods_tax.cods);
               grabar_cods();
               memcpy(cods_tax_ext.cods,bufsecre[6],sizeof cods_tax_ext.cods);
               grabar_cods_ext();
               bor_fp=1;
               for(i=0;i<sizeof cods_tax.cods[0];i++)
                  if(cods_tax.cods[3][i])
                    {
                     bor_fp=0;
                     break;   
                    }
               if(bor_fp)
                 {
                  memset(fecha_paro,0x55,3);
                  grabar_fp();
                  leer_verif_fp();
                 }
               break;
            case 5:       // entrada textes per tiquets
                              
               APAGAR_DISPLAY(1);
			   imprimir_buffer("\x1b" "A", 2 , 0);
               imprimir_buffer("L 1\x0d",4,1);
               if(blqs_ext_32.texte_nom[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_nom,LEN_TEXTE_NOM_32,1);
               imprimir_buffer("L 2: ",5,0);
               if(blqs.texte_dni[0] != 0)
                  imprimir_buffer(blqs.texte_dni,LEN_TEXTE_DNI,1);
               else
                   imprimir_buffer("\x0d",1,1);
               imprimir_buffer("L 3: ",5,0);
               if(blqs.texte_llicencia[0] != 0)
                  imprimir_buffer(blqs.texte_llicencia,LEN_TEXTE_LLICENCIA,1);
               else
                   imprimir_buffer("\x0d",1,1);
               imprimir_buffer("L 4: ",5,0);
               if(blqs.texte_matricula[0] != 0)
                  imprimir_buffer(blqs.texte_matricula,LEN_TEXTE_MATRICULA,1);
               else
                   imprimir_buffer("\x0d",1,1);

               if(blqs_ext_32.texte_H1[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H1,32,1);
               if(blqs_ext_32.texte_H2[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H2,32,1);
               if(blqs_ext_32.texte_H3[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H3,32,1);
               if(blqs_ext_32.texte_H4[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H4,32,1);
               if(blqs_ext_32.texte_H5[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H5,32,1);
               if(blqs_ext_32.texte_H6[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_H6,32,1);
               
               imprimir_buffer("\x0d",1,1);
               
               if(blqs_ext_32.texte_F1[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F1,32,1);
               if(blqs_ext_32.texte_F2[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F2,32,1);
               if(blqs_ext_32.texte_F3[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F3,32,1);
               if(blqs_ext_32.texte_F4[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F4,32,1);
               if(blqs_ext_32.texte_F5[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F5,32,1);
               if(blqs_ext_32.texte_F6[0] != 0)
                  imprimir_buffer(blqs_ext_32.texte_F6,32,1);               
                
               // v 1.11
               for(i=0;i<10;i++)
               {
                 if(blqs_ext.array_text[i].nom != 0)  
                 {
                   // cas texte definit
                  imprimir_buffer(blqs_ext.array_text[i].texte,24,1);                                  
                 }
                 else
                   continue;
               }
               
               imprimir_buffer("\x0d\x0d\x0d\x0d\x0d",5,0);
               
               
               fin = entrar_textes_tiquet();      
               
               //!!!! si s'en ha modificat algun abans del TC_OFF
               // no quedara guardat a blqs ????
               if(fin)
                 break;
               gravaTextesTiquets();
                                                            
               break;
               
            case 6:   /* entrada parametres tarifa per teclat */
                if(!xram.tarcom.OP_ETT)
                   break;
                finfunc=0;
                tarcom.NDEC=xram.tarcom.NDEC;
              do
                {
                 display(0,NF_LEDEST,(void*)TEXT_LEDEST_NT);
                 buf_nt[0]=0;
                 if(!entrar_item(NF_BCD2,&buf_nt,0))
                     {
                      fin=1;   /* cas TC_OFF  */
                      break;
                     }
                 nt=bcd_bin(buf_nt[0]);
                 if(nt ==0 || nt > xram.tarcom.ntars)
                    break;  /* final funcio 6 */
                 bld[1]=SS[(nt)%16];  //!!!! 32?????
                 --nt;
                 
                 long l1,l2;
                 for(i=0;i<8;i++)
                   {
                    if(i==1)
                    {
                      l1= xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[0];
                      l2 = xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[1];
                     // if( xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[0]
                     //    == xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[1])
                      if(l1==l2)
                        un_salto=1;
                      else
                        un_salto=0;
                    }
                    if(i==2 && un_salto) /* si vsal[0]==vsal[1] salta item */
                         {
                          xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[1]
                          =xram.bloc_tar[xram.tarifa[nt].b_t_ocup].vsal[0];
                          continue;
                         }
                    bld[0]=input_parms_tar[i].led;
                    display(0,NF_LEDEST,(void*)bld);    
                    switch(input_parms_tar[i].tip)
                       {
                        case 0:  /* depen de no. tarifa */
                            despl=nt*sizeof(struct s_tarifa);
                            break;
                        case 1:
                            despl=xram.tarifa[nt].b_t_ocup*sizeof(struct s_bloc_tar_eprom);
                            break;
                        case 2:
                            despl=xram.tarifa[nt].b_t_pagar*sizeof(struct s_bloc_tar_eprom);
                            break;
                        case 3:  /* fixe  */
                            despl=0;
                            break;
                       }
                    p_var=((unsigned char*)input_parms_tar[i].p)+despl; 
                    if(!entrar_item(input_parms_tar[i].nform,p_var,0))
                        {
                         fin=1;   /* cas TC_OFF  */
                         break;
                        }
                   }
                 if(fin) break;

                }while(1); /* final entrada de tarifas */
               break;  /* final funcio 6  */
            case 7:  /* entrada num. cambios / visu num cargaciones */
               if(!op_modif_c)
                 {
                  display(0,NF_LEDEST,(void*)TEXT_LEDEST_NG);
                  display(1,NF_K_T,(void *)&param_carg.n_grabs_carg);
                  get_tc_until_tec();
                  if(tecla_car== TC_OFF)
                    fin=1;
                  break;
                 } 
               else
                 {
                  fin = entrada_modif_c();
                 }
               break;
            case 8:  /* entrada num. serie fabricacion /visu grabs que quedan*/
               if(!op_fabrica)
                   {
                    //  recuperat a versio v 1.07                     
                    display(0,NF_LEDEST,(void*)TEXT_LEDEST_NGQ);
                    laux=(unsigned long)sav_bufgrab.n_grabs;		  
                    display(1,NF_K_T,(void *)&laux);
                    get_tc_until_tec();
                    if(tecla_car== TC_OFF)
                           fin=1;
                     break;

                    }
               else
                  fin=alta_taximetro();
               break;
            case 9:   // entrada dia descanso /reparacion
               if(op_repar)      
                 fin=reparacion();
               else
                 {
                  // BLOQUEIG TORN
                  if(xram.tarcom.bloqueo_disem 
                     && ((xram.tarcom.tipo_descanso & TIPO_DESCANSO_DIASEM)==0))
                    {
                     display(0,NF_LEDEST,(void*)TEXT_LEDEST_dia_descans);
                     APAGAR_DISPLAY(1);
                     if(primera_vegada_dia_descans)
                       {
                         primera_vegada_dia_descans = 0;
                         bloqueo_torn.dia_descans=0;
                       }
                     do
                       {
                        display(1,NF_TEXT4,&xram.tarcom.text_disem[bloqueo_torn.dia_descans].text[0]);
                        tecla_car=get_tecla_cargador();
                        if(tecla_car==TC_OFF)
                          {
                           /* cas retirat carregador */
                           fin=1;
                           break;
                          }             
                        if(tecla_car==TC_ENTER)
                          {                        
                           break;
                          } 
                        if(tecla_car==TC_CLEAR)
                          {
                           bloqueo_torn.dia_descans=(bloqueo_torn.dia_descans+1)%5;
                           continue;
                          }
                       } while(1);    
                    }
                  if(xram.tarcom.bloqueo_disem 
                     && ((xram.tarcom.tipo_descanso & TIPO_DESCANSO_FINSEM)==0))
                    {
                     // cas descans semanal per carregador
                     display(0,NF_LEDEST,(void*)TEXT_LEDEST_semana_descans); 
                     if(primera_vegada_sem)
                       {
                         primera_vegada_sem = 0;
                         laux_sem=0L;
                       }
                     do
                       {
                        display(1,NF_K,&laux_sem);
                        tecla_car=get_tecla_cargador();
                        if(tecla_car==TC_OFF)
                          {
                           /* cas retirat carregador */
                           fin=1;
                           break;
                          }             
                        if(tecla_car==TC_ENTER)
                          {                        
                           break;
                          } 
                        if(tecla_car==TC_CLEAR)
                          {
                           laux_sem=(laux_sem+1)%2;
                           continue;
                          }
                       } while(1);
                     if(!fin)
                       {
                        bloqueo_torn.semana_descans=(numero_semana(&gt.time)+(char)laux_sem)%2;
                       }      
                    }
                 }
               break;
           }
        display(0,NF_LEDEST,(void*)TEXT_LEDEST_K);
        if(nueva_k)
          display(1,NF_K,(void *)&nueva_k);   
        else
          display(1,NF_TEXT6,(void*)TEXT_K_MAL);
       }while(!fin);   /* final carregador  */
    


 get_reloj(RELLOTGE_PLACA,&gt.time);
 recalculs_gt();
 gt.canvi_fecha=1;  /* per actualitzar fecha paro */

 inici_ajustar_1h();   /* prepara gestio +-1h */

 gt.segs_interm=tarcom.segs_interm;
 gt.mitad_vd=0;

 configs_finales(MODO_CARGADOR);   

retorn:

 salta_a_reset();   // salta a 0x60000000

}

                                
// ************************************************************************************
// recepcio tarifa a traves bt40
// ************************************************************************************
long index_recepcio = 0xffffffff;

void processar_transmissio_tarifa(char* buf_rep, int nc_rep, char* buf_resp)
{
char buf_long[7];
long l_aux;
 
  if(nc_rep < 7 || xram0.tarcom.tarifa_remota != 1)
  {
    // retorna error
    *buf_resp = 'N';
    return;
  }
  memcpy(buf_long,buf_rep+1,6);
  buf_long[6] = 0;
  l_aux = atol(buf_long);
  switch(buf_rep[0])
  {
  case 'P':
      if(l_aux == 0)
      {
        // inici transmissio
        precambio_pend = 0;
        index_recepcio = 0;
      }
      else if(l_aux != index_recepcio)
      {
        // retorna error
        *buf_resp = 'N';
        return;
      }
      // copia a buffer 
      memcpy(bufferStreamCargador + index_recepcio, buf_rep + 7, nc_rep - 7);
      index_recepcio += (nc_rep - 7);
      *buf_resp = 'P';
      return;
  case 'E':
      if(l_aux != index_recepcio)
         *buf_resp = 'N';// retorna error
      else
      {
         *buf_resp = 'E'; 
         precambio_pend_bt40 = 1;
      }
      break;
  default:
      *buf_resp = 'N';    
      break;
  }
}
