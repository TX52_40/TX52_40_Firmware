// display.h

#ifndef __INC_DISPLAY
#define __INC_DISPLAY


#define MAX_DIGITS_DRIVER 6   // 6 digits * 12 segments
#define MAX_RAWS_DRIVER   2   // teclat amb nomes 2 files

struct s_ledDriver_Ram
{
  unsigned char buf[MAX_DIGITS_DRIVER*2];
};

// copia a memoria ARM ram del chip PT6961
extern struct s_ledDriver_Ram ledDriver_Ram[2];

#define DISP_CHIP_RAM  0    // displays Taxim
#define DISP_I2C       1    // display Carregador

struct s_disp
{
  unsigned char NDIG;         /*  Numero digits del display              */
  unsigned char * POS_RAM;    /*  posicio a ram del digit mes a la dreta */
  unsigned char * POS_RAM_FLASH;  /* idem per indicador flash */
  unsigned char * POS_RAMPUNTS;    /*  posicio a ram del digit mes a la dreta */
  unsigned char * POS_RAMPUNTS_FLASH;  /* idem per indicador flash */
  int tipus_disp;                  // display Tx / Carregador 
  int gestiona_flash;             // 0/1  no/si es fa gestio flash automatica
  int numIc;                      // chip de display
  int posRamIc;                   // posicio dins chip ram (digit mes a la dreta
                                  // i en ordre descendent
};


struct s_formato
{
 unsigned char  tipo ;    /*  F_BCD  2 digitos bcd por byte  */
                          /*  FORM_LONG  VARIABLE LONG          */
                          /*  FORM_TEXTO 1 caracter(8segments) por byte    */
                          /*   2  HORA  HH-MM                   */
                          /*   3  FECHA DD-MM                   */
 unsigned char  dec  ;    /*   0  sin decimales                 */
                  /*   1  con tarcom.NDEC decimales            */
                  /*   2  con 1 decimal                 */
                  /*   3  con 2 decimales               */
                  /*   4  con 3 decimales               */
                  // ....
                  //   7  con 6 decimales
                  //   8  con tarcom.NDEC_ALT decimales (per conversio euros)
                  //      - nomes per formats F_FLOAT,F_LONG,F_LONG_NC
                  //      - i en rutina display (no implementat per entrar_item)
                  /*   nomes per cas FORM_BCD           */
                  /*   0x80+
                       bits a 1 dels digits que portin punt  */
 unsigned char  blk_flsh_ndig;
                  //   bit 7   0/1   sin/con   blanking
                  //   bit 6   =1  elimina flashing (fixe)
                  //   bit 5   =1  posa flashing
                  //   bit 4   =1  posa fixe/flashing segons MASK i display FIXE/FLASH
                  //
                  //   bits 0-2    numero digitos
 unsigned char * maskflsh; /*   punter a la mascara per flash    */
};                   



#define LED_HORARIA_ON      tx30.led_horaria = 1
#define NEW_LED_HORARIA_ON  tx30.new_led_ho = 1
#define NEW_LED_KM_ON       tx30.new_led_km = 1

#define LED_HORARIA_OFF     tx30.led_horaria = 0
#define NEW_LED_HORARIA_OFF tx30.new_led_ho = 0
#define NEW_LED_KM_OFF      tx30.new_led_km = 0

#define LED_HHMM 0x0040   // per indicacio hh.mm cas visu temps torn en lliure
extern void LEDS_FRONTAL_FIXE(void);

extern void LEDS_FRONTAL_FLASH(unsigned short val);



#define NUM_DISPLAYS    4

#define DISP_LEDEST   0
#define DISP_IMPORT   1
#define DISP_EXTRES   2
#define DISP_CARREGADOR 3

#define SS_0   0x3f  // CODIFICACIO 0 PER 7 SEGMENTS (UTILITZAT PER BLANKING
#define SS_DP  0x80  // CODIFICACIO DECIMAL POINT  PER 7 SEGMENTS
#define SS_GUION 0x40 // CODIFICACIO guion

extern const unsigned char SS[16] ;
extern const struct s_disp DISP[NUM_DISPLAYS];




extern void display_init(void);
extern void display(int nDisp,int nFormat, void *var);


extern void barra_progres_init(int max_value);
extern void barra_progres();


extern void APAGAR_DISPLAY(unsigned char nDisp);
extern void DISP_FILL(unsigned char nDisp,unsigned char value);
extern void DISP_DIG_SET(unsigned char ndisp,unsigned char ndig, unsigned char value);
extern void DISP_FIXE(unsigned char nDisp);
extern void DISP_FLASH(unsigned char nDisp);

extern void DISPLAY_SAVE(unsigned char ndisp,char * buf);
extern void DISPLAY_RESTORE(unsigned char ndisp,char * buf);
extern void DISPLAY_SAVE_FLASH(unsigned char ndisp,char * buf);
extern void DISPLAY_RESTORE_FLASH(unsigned char ndisp,char * buf);

extern void LEDS_FRONTAL(unsigned short val);

extern void refresh_luzfront(void);
extern void out_luzfront_flash(void);

extern void REFRESH_ON(void);
extern void REFRESH_OFF(void);

extern void refresh_backlight(void);

extern void display_to_chip(int nDisp);

extern unsigned char lap_on;
extern unsigned char lap_nf;        // guarda crida a disp cas lap_on
extern char lap_var[10];

extern void RESET_LAPSUS(void);
extern void INI_LAPSUS(void);
extern void FIN_LAPSUS(void);
extern void DISPLAY_ERROR_PLUS_LAP(int error);

#endif

