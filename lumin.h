//******************************************************
// lumin.h   gestion luminoso 
//******************************************************


#define LLUMS_COMBI_NORMAL  0
#define LLUMS_COMBI_ALT     1

#define LLUMINOS_NO_VERIFICABLE       0x0001
#define LLUMINOS_DELAY_VERIFICACIO    0x0002
#define LLUMINOS_SERIE                0x0004
#define LLUMINOS_MULTIPLEXAT          0x0008
#define LLUMINOS_TEST_AMB_ALIMENTACIO 0x0010
#define LLUMINOS_MONODRIVER           0x0020

extern unsigned short const  taula_tipo_lumin[];


#define SEGONS_TEST_LUMIN  20   // segons per test lluminos

typedef struct s_luminoso
{
unsigned short  tipo_lumin;
char llums_combi;
unsigned char llums_a_enviar;
unsigned char timer_envio_serie;
unsigned char timeout_resp_serie;
int cnt_dec_segon_desde_pas_a_lliure;
int valor_dec_segon_desde_pas_a_lliure;
unsigned char tempor_test;
unsigned char timer_apagat_per_error;
char habia_error_luzex;   // v 1.11
char hay_error_luzex;
unsigned char cnt_refr;

char serie_pendent_test_desde_pas_a_lliure;
}t_luminoso;

extern t_luminoso luminoso;
extern const unsigned char sv;

extern unsigned char luminoso_get_error(void);
extern void luminoso_init();
extern void out_luzex(char mask, char index_env);
extern void out_luzex_restore(void);
extern void ini_luces_IOEXPAND_I2C(void);
extern void test_luzex(int display);
extern void out_luzex_no_save(char mask, char index_env);

extern void luminoso_serie_main_loop(void);
extern void lluminos_45ms_loop(void);
