#include "rc4.h"

int skey[256];
char Key[]="andromeda";

void RC4(char * Buf, int dwBufLen)
{
  int i=0, j = 0;
  int dw;
  char tmp;
  for(i = 0; i < 256; i++)
  {
          skey[i] = i;
  }
  for(i = 0; i < 256; i++)
  {
          j = (j + skey[i] + Key[i % 9]) % 256;
          tmp = skey[i];
          skey[i] = skey[j];
          skey[j] = tmp;
  }
  i=0; j=0;
  for(dw = 0; dw < dwBufLen; dw++)
  {
          i = (i + 1) % 256;
          j = (j + skey[i]) % 256;
          tmp = skey[i];
          skey[i] = skey[j];
          skey[j] = tmp;
          *Buf++ ^= skey[(skey[i] + skey[j]) % 256];
  }
}
