//********************************************************
// format.h
//********************************************************
#ifndef _INC_FORMAT
#define _INC_FORMAT

#include "regs.h"

extern int GLOBAL_ES0;

#define FINLIN   0x80  // per afegir LF als formats


#define _MONEDA_DESCONOCIDA	0
#define _MONEDA_TECLEO		1
#define _MONEDA_AUX		2

// sustituit per GR   #define car_imp_ANAGRAMA	0x08

#define FS 9    // FontSize
#define GR 11   // Bitmap

#define car_imp_CONTROL		0x10


extern const unsigned long KDEC[7];

// *****************************************************
// prototips rutines
// *****************************************************
extern void format_configurar(int diames, int tipoImpresora);

extern void itoa_dp(unsigned int ival,char* buf,int nc,int ndec,char sep);
extern void ltoa_dp(unsigned long lval,char* buf,int nc,int ndec,char sep);
extern void ltoa_dp_blk0(unsigned long lval,char* buf,int nc,int ndec,char sep);
void itoa_dp_blk0(unsigned int ival,char* buf,int nc,int ndec,char sep);
extern int formatear(int format,int ndig,T_VAR* var,char *buf, int *local_es0, int *global_es0);
extern void miss_err_pqr(int nm,char *buf);
extern void miss_err_cc(int nm,char *buf);
extern char get_uso_sle(char moneda_tarj);
extern void itoa_dp_blk0(unsigned int ival,char* buf,int nc,int ndec,char sep);
#endif
