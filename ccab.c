// *************************************************
// ccab.c
// *************************************************

#include "rtos.h"
#include "iostr710.h"

#include "71x_map.h"
#include "hard.h"

#include "cua.h"
#include "serial.h"
#include "tx30.h"
#include "ccab.h"


__no_init struct s_ccab ccab;


// ********************************************************
// inicialitza entrades i sortides amb ccab
// ********************************************************
void reset_ccab(void)
{
    ccab.logon=0;
    ccab.control=0;                
    ccab.resp_en_curs=0;
}

// ********************************************************
// inicialitza transmissio amb ccab
// ********************************************************
void ccab_init(char hi_ha_ccab)
{
  if(hi_ha_ccab)
  {
   // cas transmissio amb ComputerCab
   usart_install(USART_CCAB,UART1_IRQHandler_Ccab);
   usart_hard(USART_CCAB, MODO_232, 0, 0, 0);
   usart_init(USART_CCAB, 9600, 7,0); // 8bits + paritat,  even
   reset_ccab();
  }   
}

void UART1_IRQHandler_Ccab(void) {
volatile unsigned short _Dummy;
volatile char ch;
  do {
      if (UART1_SR_bit.RxBufNotEmpty)                // Data received?
      {
        if (UART1_SR_bit.ParityError || UART1_SR_bit.FrameError ||UART1_SR_bit.OverrunError)      // Any error ?
        {  
          _Dummy = UART1_RXBUFR_bit.RX;                  // => Discard data
        }
        else
        {
          ch = UART1_RXBUFR;                  // Process actual byte
        }
       if(ccab.control || ccab.resp_en_curs)  // cas encare esta enviant
         {
          ccab.control=0;      // cancela resp.
          ccab.resp_en_curs=0;
         }
       else
         {
          switch(ch)
            {
             case 1:   // meter logon request
                if(ccab.logon==0 && tx30.estat!=E_OFF)
                  {
                   UART1_TXBUFR = 0x02;  // meter logon Ack
                   ccab.logon=1;
                  }
                break;
             case 3:   // meter logoff Request
                if(ccab.logon==1)
                  {
                   UART1_TXBUFR = 0x04; // meter logoff Ack
                   ccab.logon=0;
                  }
                break;
             default:
                if(ccab.logon)
                   ccab.control = ch; // ja contestara el background
                break;
            }
         }
      }  // final cas recepcio  Ccab
    
      if (UART1_SR_bit.TxEmpty)                 // Check Tx status => Send next character
      {
        // !!! no es pot escriure  UART1_SR_bit.TxHalfEmpty = 0;                 // Clear Tx Int       
       if(ccab.resp_en_curs)
         {
          if(++ccab.nc_env < ccab.len_env)
            {
              UART1_TXBUFR = ccab.buf_env[ccab.nc_env]; // seguent caracter
              UART1_IER_bit.TxEmptyIE = 1;
            }
          else
            {
              UART1_IER_bit.TxEmptyIE = 0; // Disable further tx interrupts
              ccab.resp_en_curs=0;
            }
         }
              
      }// final cas envio  Ccab
     } while ((UART1_IER_bit.TxEmptyIE && UART1_SR_bit.TxEmpty) || (UART1_IER_bit.RxBufNotEmptyIE && UART1_SR_bit.RxBufNotEmpty));
}


char lbaux[11];  

void gestio_ccab(void)
{
unsigned char  i,aux;
int st;
 if(ccab.control)
    {
     switch(ccab.control)
      {
       case 5:  // status req.
         ccab.buf_env[0]=0x06; //meter status
         switch(tx30.estat)
           {
            case E_OCUPADO:
              ccab.buf_env[1]=0x02;
              break;
            case E_PAGAR:
              ccab.buf_env[1]=0x03;
              break;
            default:
              ccab.buf_env[1]=0x01;
              break;
           }
         ccab.len_env=2;
         break;
       case 7:   // Reading req
         ccab.buf_env[0]=0x08;   // meter reading
         ltobcd(serv[0].import,lbaux);
         for(i=1;i<6;i++)
           ccab.buf_env[i]=lbaux[5-i];
         ltobcd(serv[0].supl,lbaux);
         for(i=6;i<10;i++)
           ccab.buf_env[i]=lbaux[9-i];
         ccab.len_env=10;
         break;
       case 9:  // Odometer req.
         ccab.buf_env[0]=0x0a; // odometer reading
         if(tarcom.miles_km_ccab)
           ltobcd(totalitzador_read(TOT_CCAB, &st),lbaux);
         else
           ltobcd(totalitzador_read(TOT_KMT, &st),lbaux);
         for(i=1;i<6;i++)
           ccab.buf_env[i]=lbaux[5-i];
         ccab.len_env=6;
         break;
      }
     aux=0;
     for(i=0;i<ccab.len_env;i++)
       aux^=ccab.buf_env[i];
     ccab.buf_env[i]=aux;
     ccab.len_env++;
     ccab.nc_env=0;
     ccab.resp_en_curs=1;
     ccab.control=0;
     UART1_TXBUFR = ccab.buf_env[0]; // inicia transmissio
     UART1_IER_bit.TxEmptyIE = 1;
    }
 }
