// adc.c


#include "iostr710.h"
#include "rtos.h"

#include "adc.h"


void adc_init(void)
{
  
  // reset data available
  ADC_CSR_bit.DA0 = 0;
  ADC_CSR_bit.DA1 = 0;
  ADC_CSR_bit.DA2 = 0;
  ADC_CSR_bit.DA3 = 0;
  // set prescaler
  ADC_CPR_bit.PRESC = 20;   // divideix per 40
  // enable
  PCU_BOOTCR_bit.ADCEN = 1;   // enable ADC
  //!!!!APB2_CKDIS_bit.bn6 = 0;     // enable clock (reset ja ho fa.Nomes per retorn power down)
}

int adc_read(int nADC, int* status)
{
  int ret,st;
  switch(nADC)
  {
  case 0:
    st = ADC_CSR_bit.DA0;   // data available
    ADC_CSR_bit.DA0 = 0;
    ret = ADC_DATA0_bit.DATA;   // data
    break;
  case 1:
    st = ADC_CSR_bit.DA1;   // data available
    ADC_CSR_bit.DA1 = 0;
    ret = ADC_DATA1_bit.DATA;   // data
    break;
  case 2:
    st = ADC_CSR_bit.DA2;   // data available
    ADC_CSR_bit.DA2 = 0;
    ret = ADC_DATA2_bit.DATA;   // data
    break;
  case 3:
    st = ADC_CSR_bit.DA3;   // data available
    ADC_CSR_bit.DA3 = 0;
    ret = ADC_DATA3_bit.DATA;   // data
    break;
  }
  *status = st;
  ret = (ret + 0x800)& 0xfff;
  return(ret);
}


void adc_powerDown(void)
{
  PCU_BOOTCR_bit.ADCEN = 0;   // disable ADC
  //!!!!APB2_CKDIS_bit.bn6 = 0;     // disable clock
  
}
