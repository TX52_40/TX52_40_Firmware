// ***********************************************************************
//  pausa.h                                       
// ***********************************************************************


#ifndef _INC_PAUSA
#define _INC_PAUSA

#define PAUSA_SEGONS_PREPAUSA  15*60
#define PAUSA_SEGONS_PREPAUSA_PASIVA  30

enum ePausaEstat{
	EPAUSA_temporitza_inici_pausa_passiva=0,
	EPAUSA_temporitza_pre_pausa_activa,
	EPAUSA_pausa_activa,
	EPAUSA_temporitza_pre_pausa_passiva,
	EPAUSA_pausa_passiva,
};

struct s_pausa
{
  enum ePausaEstat estat;
  long segons;
  char tecla_polsada;
  long segons_acumulat_pausa;
  char visu_acumulat;
};

extern struct s_pausa pausa;

#endif
