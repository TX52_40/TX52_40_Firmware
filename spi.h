//*******************************************************************
// spi.h
//*******************************************************************

#ifndef __INC_SPI
#define __INC_SPI

extern void spi_init(void);
void spi_send(int num_dev, unsigned char *buf, int num_c);
void spi_recv(int num_dev, unsigned char *buf, int num_c);

#endif

