#ifndef __INC_TOTS_EQ
#define __INC_TOTS_EQ

#include "tots.h"

#define TOT_EQ_NSER		0
#define TOT_EQ_IMPT		1
#define TOT_EQ_KMT		2
#define TOT_EQ_FIN		3

#define TOT_DIA			0
#define TOT_MES			1
#define TOT_SEM			2
#define TOT_ANY			3
#define TOT_SEM1		4
#define TOT_SEM2		5
#define TOT_DIA_ANT		6
#define TOT_MES_ANT		7
#define TOT_SEM1_ANT	8
#define TOT_SEM2_ANT	9
#define TOT_ANY_ANT		10
#define TOT_PER_FIN		11

struct tot_equador{
	unsigned long timeIni;
	unsigned long timeFi;
	struct s_total total_eq[3];
};

extern struct tot_equador tots_eq[TOT_PER_FIN];

extern char semestre_actiu;

extern unsigned long tot_eq_read(char ntot, char periode); 
extern void copia_tot_sem(void);
extern void comprova_totalitzadors_eq(void);
extern void borra_tots_eq(void);

#endif