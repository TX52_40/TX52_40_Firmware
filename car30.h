// **************************************************************
// car30.h
// **************************************************************


#ifndef __CAR30_H
#define __CAR30_H

#define PRECAMBIO_NO          0
#define PRECAMBIO_CARREGADOR  1
#define PRECAMBIO_BT40        2

extern int hay_cargador(void);
extern char  toca_precambio(void);
extern void EJECUTAR_PRECAMBIO(char tipus);
extern void CARGADOR(void);
extern void guarda_versio_prog_a_blqs(void);
void receiveTextesTiquets(char * buf, int nb);
extern int sendTextesTiquets(int remit, char * buf);
extern void copia_textes_a_reg(void);

#endif

