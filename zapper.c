// ********************************************************
// zapper.c
// ********************************************************

#include <string.h>
#include <stdlib.h>

#include "tx30.h"
#include "display.h"
#include "zapper.h"
#include "debug.h"


#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")



#define NUM_MAX_ERRORS_ZAPPER 50    



typedef struct s_error_zapper    //fb_zapper
{
 T_FECHHOR fecha_hora;
 unsigned short error_code;
 unsigned short speed;
 unsigned short accel;
 unsigned short excees_time;
 unsigned char dum[2];       //!!!!!! per debug (16 bytes per ratlla al fer dump)
}T_ERROR_ZAPPER;


// variables

__no_init unsigned char segons_last_minut[10];   
__no_init unsigned char num_errors1;
__no_init unsigned char num_errors_zapper;
__no_init unsigned char index_errors_zapper;    // per posar error a cua circular      
__no_init unsigned char index_errors_zapper_ant;    // per incrementar durada
__no_init unsigned char zapper_timer_error2;   
__no_init unsigned short accel_zapper;
__no_init unsigned short accel_zapper1;
__no_init unsigned short speed_zapper;
__no_init unsigned char tar_zapper_ant;
__no_init float ft0,ft1,ft;
__no_init int zapper_segons_amb_error;
__no_init char MASK_BLOQUEIG_ZAPPER;

__no_init T_ERROR_ZAPPER error_zapper[NUM_MAX_ERRORS_ZAPPER];                  

__no_init unsigned char ZAPPER_ACTIVO;

int zapper_err_from_interrupt = 0;

unsigned char METRO_ZAPPER = 0; 

unsigned int time_from_last_pulse;          
unsigned int last_time_between_pulses;      
unsigned int pulses_per_second;            
unsigned int pulses_per_last_second;       
unsigned int max_diff_time_between_pulses;
unsigned int PPS,LAST_PPS;



#if DEBUG_ZAPPER
char var_canvi_visu_zapper;
#endif

//
// treu bloqueig zapper
//
void zapper_reset_bloqueig(void)
{
     MASK_BLOQUEIG_ZAPPER = 0;    
}

void zapper_activar(void)
{
  if(tarcom.hay_zapper)
    ZAPPER_ACTIVO = 1;
}

void zapper_desactivar(void)
{
  ZAPPER_ACTIVO = 0;
}

int zapper_bloqueja_equip(void)
{
  if(tarcom.hay_zapper && MASK_BLOQUEIG_ZAPPER)
    return(1);
  return(0);
}

int zapper_error_en_curs(void)
{
  if(tarcom.hay_zapper && zapper_segons_amb_error)
    return(1);
  return(0);
}

int zapper_hi_ha_zapper(void)
{
   return(tarcom.hay_zapper);
}

int zapper_esta_activat(void)
{
  if(ZAPPER_ACTIVO)
    return(1);
  return(0);
}


//
// inicialitzacions  zapper
//                 
void zapper_init(void)    
{ 
 ZAPPER_ACTIVO=0;
 num_errors_zapper=0;
 index_errors_zapper=0;
 num_errors1=0;
 memset(error_zapper,0,sizeof(error_zapper));
 memset(segons_last_minut,0,sizeof(segons_last_minut));
 zapper_timer_error2=0;
 zapper_segons_amb_error = 0;
 zapper_err_from_interrupt = 0;
}

// 
// tractament metro_zapper.Es crida desde interrupcio temps (cada 1ms)
// nomes es crida si zapper esta activat
//



unsigned short cnt_ms;           
unsigned long laux;            

CODE_FAST void zapper_interrupt_1ms(void)
{

  if(time_from_last_pulse < 2000)  // per cas molt lent
   time_from_last_pulse++;
 if(METRO_ZAPPER)
   {                            
    METRO_ZAPPER=0;
    // per calcular acceleracions mes grans de 5m/seg2
    pulses_per_second++;
    PPS++;
    // per calcular acceleracions >4m/seg2 
    if(ZAPPER_ACTIVO 
       && (time_from_last_pulse<last_time_between_pulses)
       && (zapper_err_from_interrupt == 0)  
       //&& (velocidad <=tarcom_ext.vel_zap_change )
         )
      {
       if(LAST_PPS==0)
         LAST_PPS=1;
       max_diff_time_between_pulses=
          ( 1000/LAST_PPS - 1000/(LAST_PPS+tarcom.max_acc1_fk))/LAST_PPS +10;
       
       if(( (last_time_between_pulses-time_from_last_pulse)>max_diff_time_between_pulses)
           && (LAST_PPS>=tarcom.min_pulses_per_sec))
         {
          // cas detectada acceleracio >= 4m/seg2
           
          zapper_err_from_interrupt = 1; 
          
          if(num_errors1>=tarcom.num_errors1)
            { 
             // s'ha superat tarcom.num_errors1 (4) vegades en 1 minut.             
             // registra error
             accel_zapper=last_time_between_pulses;
             accel_zapper1=time_from_last_pulse;
             speed_zapper=LAST_PPS;
             
/* passat a background
             // reinicialitza array errors dins minut
             num_errors1 = tarcom.num_errors1; // per si era > (no hauria de passar )
             for(i=1;i< num_errors1;i++)
               segons_last_minut[i-1] = segons_last_minut[i];
             segons_last_minut[num_errors1-1]=0;
*/             
            }
          else
            {
/* passat a background              
            // inicia temps del nou error dins de minut
            segons_last_minut[num_errors1++]=0;
*/              
            }
         }
      }  
    last_time_between_pulses=time_from_last_pulse;
    time_from_last_pulse = 0;
   }
  if(++cnt_ms>=1000)  // contador per fer 1 seg.
     cnt_ms=0;
  if((cnt_ms==0) ||(cnt_ms==500))  // 1/2 seg.
    {
     if(ZAPPER_ACTIVO          
        && (zapper_err_from_interrupt == 0)  
        && (pulses_per_second>pulses_per_last_second))
       {
        accel_zapper=pulses_per_second-pulses_per_last_second;
        if(accel_zapper>tarcom.max_acc2_fk)    // accel > 5m/s2
           {
             // registre error
             speed_zapper=LAST_PPS; 
             zapper_err_from_interrupt = 2;
           }
       }
     pulses_per_last_second=pulses_per_second;
     pulses_per_second=0;
     if(cnt_ms==0)
       {
        // cas 1 segon
        LAST_PPS=PPS;
        PPS=0;
       }
    }
  return;
}

//
// acumula impulsos distancia durant 1 segon.
// mante dos acumuladors decalats mig segon per detectar acceleracions
// superiors al limit durant al menys mig segon
//
void zapper_interrupt_1ms_chile(void)
{
/*
 if(ZAPPER_ACTIVO && METRO_ZAPPER)
   {                            
    METRO_ZAPPER=0;
    // per calcular acceleracions 
    zapper_cnt_metres1++;
    zapper_cnt_metres2++;
  if(++cnt_ms>=1000)  // contador per fer 1 seg.
     cnt_ms=0;
  if((cnt_ms==0) ||(cnt_ms==500))  // 1/2 seg.
    {
     if(ZAPPER_ACTIVO          
        && (zapper_err_from_interrupt == 0)  
        && (pulses_per_second>pulses_per_last_second))
       {
        accel_zapper=pulses_per_second-pulses_per_last_second;
        if(accel_zapper>tarcom.max_acc2_fk)    // accel > 5m/s2
           {
             // registre error
             speed_zapper=LAST_PPS; 
             zapper_err_from_interrupt = 2;
           }
       }
     pulses_per_last_second = pulses_per_second;
     pulses_per_second=0;
     if(cnt_ms==0)
       {
        // cas 1 segon
        LAST_PPS=PPS;
        PPS=0;
       }
    }
  return;
 
}
  */
}

void zapper_NULL(void){};


ZAPPER_INT zapper_1ms[3] =
{
  zapper_NULL,                // no hi ha zapper  tarcom.hay_zapper = 0
  zapper_interrupt_1ms,       // versio classica  tarcom.hay_zapper = 1
  zapper_interrupt_1ms_chile  // versio chile     tarcom.hay_zapper = 2
};


//
//   gestio desde background dels errors 1 i 2  generats a l'interrupcio  
//
void zapper_loop(void)
{
int i;
int ii;
  
    if(!zapper_esta_activat())
    {
      zapper_err_from_interrupt = 0;
      return;
    }
    if(zapper_err_from_interrupt == 0)
      return;
    
    // registra error
    time_to_fechhor(&error_zapper[index_errors_zapper].fecha_hora,&gt.time);
    error_zapper[index_errors_zapper].error_code=zapper_err_from_interrupt;
    error_zapper[index_errors_zapper].speed= velocidad; //!!!!
    //!!!!!(float) speed_zapper *( (float)tarcom_ext.k) /(3600.*(float)tarcom_ext.K0);
    index_errors_zapper_ant=index_errors_zapper;
    if(num_errors_zapper<NUM_MAX_ERRORS_ZAPPER)
      num_errors_zapper++;
    if(++index_errors_zapper>=NUM_MAX_ERRORS_ZAPPER)
      index_errors_zapper=0;   //dona volta a la cua errors  
    if(zapper_err_from_interrupt == 1)
    {      
          // calcula acc.
          if(accel_zapper)
            ft0=(float)accel_zapper;
          else
            ft0=1;
          if(accel_zapper1)   
           ft1=(float)accel_zapper1;
          else
            ft1=1;
          ft=(ft0-ft1)*1000000./(ft0*ft1*ft1)*(1000.*(float)tarcom.DIVK)/ ( (float)tarcom.k) ;
          if(ft>999)
             error_zapper[index_errors_zapper_ant].accel=999;
          else
            {
             if(ft<4)
               ft=4;   
             error_zapper[index_errors_zapper_ant].accel=(short)ft;
            } 
          error_zapper[index_errors_zapper_ant].excees_time=0;
          
          if(num_errors1>=tarcom.num_errors1)
            { 
             // s'ha superat tarcom.num_errors1 (4) vegades en 1 minut.             
             // registra error
             // reinicialitza array errors dins minut
             num_errors1 = tarcom.num_errors1; // per si era > (no hauria de passar )
             for(i=1;i< num_errors1;i++)
               segons_last_minut[i-1] = segons_last_minut[i];
             segons_last_minut[num_errors1-1]=0;            
            }
          else
            {
            // inicia temps del nou error dins de minut
            segons_last_minut[num_errors1++]=0;              
            }
    }
    else // zapper_err_from_interrupt == 2
    {
          error_zapper[index_errors_zapper_ant].accel=
           (unsigned short)(((float)accel_zapper)*((float)4.)* (1000.*(float)tarcom.DIVK) /  ( (float)tarcom.k)) ;
          error_zapper[index_errors_zapper_ant].excees_time=0;
          
          zapper_timer_error2=4;            // 3"
    }
    zapper_err_from_interrupt = 0;
    // engega timer 1 minut (si no esta en marxa)
    if(zapper_segons_amb_error == 0)
    {
      DISPLAY_ERROR_PLUS_LAP(ERR_ZAPPER);
      zapper_segons_amb_error = 1;
      // desactiva distancia / temps
    for(ii=0; ii<MAX_PSGRS; ii++)
      {
       if(servcom.viaj_activo[ii])
         {
          serv[ii].CUENTA_K=0;
          if(tarcom.exces_vel_no_horaria)
                 serv[ii].CUENTA_H=0;                  
         }
      }
    }
     
}


//
// gestio zapper cada segon         
//
void zapper_canvi_segon(void)
{
unsigned char i,j,k;
int ii;
  
     // mira si porta 1 minut amb error
     if(zapper_segons_amb_error)
     {
       if(++zapper_segons_amb_error > tarcom.zapper_segons_error_definitiu)
       {
         MASK_BLOQUEIG_ZAPPER = 1;   // bloqueja pas a ocupat definitiu
       }
     }
// incrementa contador segons per veure si ha passat 1 minut                
// si ja ha passat (i encara no hi han 5 errors)fa fora els errors que ja   
// porten 1 minut i reorganitza els restants                                
     for(i=0,k=0;i<num_errors1;i++)
       if((++segons_last_minut[i])>=60)
         k++;
     if(k)
     {
       num_errors1-=k;    
       for(i=k,j=0;j<num_errors1;)
         segons_last_minut[j++]=segons_last_minut[i++];   
     }
     
     if(zapper_timer_error2)
       --zapper_timer_error2;
     
     // mira si toca parar temps error zapper
     if(zapper_segons_amb_error)
     {
       if((num_errors1 < tarcom.num_errors1) && (zapper_timer_error2 == 0))
         {
           zapper_segons_amb_error = 0;   // para contador segons amb error
           FIN_LAPSUS();
           // restaura comptar temps i distancia
          for(ii=0; ii<MAX_PSGRS; ii++)
            {
             if(servcom.viaj_activo[ii])
               {
               serv[ii].CUENTA_K=serv[ii].NEW_CUENTA_K;
               serv[ii].CUENTA_H=serv[ii].NEW_CUENTA_H;
               }
            }
         }
     }
}



#if DEBUG_ZAPPER
// per cas zapper_err_from_interrupt == 1
       if(var_canvi_visu_zapper & 0x01)        
           {
           *(DISP[2].POS_RAM )= SS[max_diff_time_between_pulses%10];
           *(DISP[2].POS_RAM -1)= SS[(max_diff_time_between_pulses/10)%10];
           *(DISP[2].POS_RAM -2)= SS[(last_time_between_pulses-time_from_last_pulse)%10];
           *(DISP[2].POS_RAM -3)= SS[((last_time_between_pulses-time_from_last_pulse)/10)%10];
           display_to_chip(2);
           } 
#endif

#if DEBUG_ZAPPER
// per cas zapper_err_from_interrupt == 2
        if(!(var_canvi_visu_zapper & 0x01))        
         {
          *(DISP[2].POS_RAM -2)=SS[accel_zapper%10];
          *(DISP[2].POS_RAM -3)=SS[(accel_zapper/10)%10]; 
          if((cnt_ms==0) && (PPS>LAST_PPS))
            {
             *(DISP[2].POS_RAM )=SS[(PPS-LAST_PPS)%10];
             *(DISP[2].POS_RAM -1)=SS[((PPS-LAST_PPS)/10)%10];
            } 
          display_to_chip(2);
         } 
#endif        

