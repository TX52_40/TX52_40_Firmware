// *****************************************************
// teclat.h
// *****************************************************

#ifndef __INC_TECLAT
#define __INC_TECLAT

#define BUZZER_TIME_ERROR 500    
#define BUZZER_TIME_TECLA  25
#define BUZZER_TIME_SALT   10


#define TEC8_LONG_50  47   // valor tecla + pulsada 2" (a nivell scan teclat) TX52
#define TEC8_LONG_40  38   // valor tecla + pulsada 2" (a nivell scan teclat) TX40

struct s_teclat          // per teclat carg
{
 unsigned char tpuls;
 unsigned char timetec;
 char timeshift;
 char time_longtec;
 
};

extern void buzzer_init(void);
extern void buzzer_set(int ms);

extern struct s_teclat teclat;

extern void teclat_init(void);
extern int teclat_llegir(void);
extern void teclat_tic10ms(void);

extern unsigned char llegir_teclat(unsigned char ntec);
extern unsigned char get_event_teclat(int mode_carregador,unsigned char*  tecla_num);

#endif

