//************************************************************
// metros.c    generador impulsos
//************************************************************

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "iostr710.h"

#include "rtos.h"
#include "i2c.h"         
#include "metros.h"
#include "hard.h"

extern void testConector_change_mode(int forced, char k_ja_instalada);

__no_init unsigned int COLA_MF;  

char flanc_pujada;


unsigned int get_clear_COLA_MF(void)
{
unsigned int m;
 m=COLA_MF;
 OS_DI();
 COLA_MF-=m;
 OS_RestoreI();
 return(m);
}


void decr_COLA_MF(void)
{
 OS_DI();
 COLA_MF--;
 OS_RestoreI();
}


typedef struct 
{
  unsigned char tipo_impulsos;
  unsigned char divisor_k;
  unsigned char t0du;
  unsigned char ANLG_MIN;
  unsigned char ANLG_MAX;
  unsigned char impulsos[2];
}Tpic_impulsos_read;

typedef struct 
{
  unsigned char tipo_impulsos;
  unsigned char divisor_k;
  unsigned char t0du;
  unsigned char ANLG_MIN;
  unsigned char ANLG_MAX;
}Tpic_impulsos_write;

Tpic_impulsos_read pic_impulsos_read;
Tpic_impulsos_write pic_impulsos_write;

char funcTriggerMinMax(float voltage)
{
char valor;
      voltage-=1.25;
      if(voltage < 0)
	  voltage=0;
      if(voltage >= (2.5)) // 5-1.25-1.25
	  voltage=2.5;

      valor = (char)(0.5+((voltage*15)/2.5));
      if (valor>15)
	  valor=15;
      return(valor);
}

char funcTriggerMin(int mV)
{
float value;
  value = 2.5 - (float)mV/1000.;
  return(funcTriggerMinMax(value));
}

char funcTriggerMax(int mV)
{
float value;
  value = 2.5 + (float)mV/1000.;
  return(funcTriggerMinMax(value));
}


const char def_pull_up[4] ={
0, // None
1, // Pull up
0, // Pull down
1, // Both
};

const char def_pull_down[4] ={
0, // None
0, // Pull up
1, // Pull down
1, // Both
};

unsigned char CNT_DIVK = 1;

void instala_k(unsigned long K,t_gi* gi)
{
int i;
unsigned char divk;
char pot_value;
int trig;

 divk=K/DIVISOR_MAX_K + 1;
 
 if(tipoTx == TX50)
 {
      for(i=0;i<3;i++)
      {
          pic_impulsos_write.tipo_impulsos = 0x31 + gi->giTipo;
          pic_impulsos_write.divisor_k = divk | 0x80;
          pic_impulsos_write.t0du = 0x30 + gi->giNivell; 
          pic_impulsos_write.ANLG_MIN = 0x80 + funcTriggerMin(gi->giTrigger);
          pic_impulsos_write.ANLG_MAX = 0x80 + funcTriggerMax(gi->giTrigger);
          write_i2c(PIC_FIRMWARE,ADR_INFORM, sizeof(pic_impulsos_write), (char*)&pic_impulsos_write);
          
          OS_Delay(2);
          wread_i2c(PIC_FIRMWARE,ADR_INFORM, sizeof(pic_impulsos_read), (char*)&pic_impulsos_read);
          pic_impulsos_read.divisor_k |= 0x80;   // !!!!! per arreglar error de PIC que no ho posa
          if(memcmp((char*)&pic_impulsos_read,(char*)&pic_impulsos_write, sizeof(pic_impulsos_write)) == 0)
            break;    // s'ha escrit correctament el pic
      }
 }
 else
 {
   // TX40
   
   CNT_DIVK = divk;
   
   XTI_TRL |=0x20;
   switch(gi->giTipo)
   {
   case 0:    // analogic
        write_i2c(POT_DIGITAL,0x40,1,"\xff");
        trig = gi->giTrigger;
        if(trig >2000)
          trig = 2000;
        pot_value = (char)((128*(2500-trig))/5000);
        write_i2c(POT_DIGITAL,0x00,1,&pot_value);
        pot_value = (char)((128*(2500+trig))/5000);
        write_i2c(POT_DIGITAL,0x10,1,&pot_value);
        // selecciona entrada analogica
        PORT6 &= 0xfb;
        write_i2c(IO_EXPANDER_TX40,0x12,1,(char*)&PORT6);      
        // pull up/down
        IOPORT2_PD_bit.no2 = def_pull_up[gi->giNivell]; 
        IOPORT2_PD_bit.no3 = def_pull_down[gi->giNivell]; 
     break;
   case 1:    // 2 Pulses
        write_i2c(POT_DIGITAL,0x00,1,"\x00");
        write_i2c(POT_DIGITAL,0x10,1,"\x00");
        // selecciona entrada digital
        PORT6 |= 0x04;
        write_i2c(IO_EXPANDER_TX40,0x12,1,(char*)&PORT6);
     break;
   }
    testConector_change_mode(1, 1);  // (forced i k_ja_instalada)
//        XTI_MRL |= 0x10;   // P2.10     int dist1
//        XTI_MRL |= 0x20;   // P2.11     int dist2
 }
 
  COLA_MF=0;
  flanc_pujada = 0;
               
}


// ********************************************************************************
//   VELOCIMETRO
// ********************************************************************************
static float fvelo;
static unsigned char CERO_VEL;
static unsigned char TRASPAS_VEL;
static unsigned int DATOS_TRASPAS_VEL;
static unsigned int cms,cms_last,cd;
static unsigned char METRO_VEL;    // per comunicar amb interrup. distancia per velocimetro


// es crida desde interrupcio distancia  INTD_C
void  velocimetro_INTD(void)
{
  METRO_VEL = 1;
}

// es crida desde tx30_interrupt_timer1ms()
void  velocimetro_int_1ms(void)
{
 if(METRO_VEL)
    {
     METRO_VEL=0;
     if(CERO_VEL)
       {
        cms=0;
        cd=0;
        CERO_VEL=0;
       }
     else
       {
        ++cd;
        cms_last=++cms;
        if(cd==10 || (cms>1000))   // >1000
          {
           DATOS_TRASPAS_VEL=cms + (cd<< 12);
           TRASPAS_VEL=1;
           cd=0;
           cms=0;
          }
       }
    }
 else
    {
     // cas no metro
     if(!CERO_VEL)
       {
        ++cms;
        if(cms>1000)   // Ja porta 1"
          {
           if(cd)
              {
               DATOS_TRASPAS_VEL=cms_last + (cd<< 12);
               TRASPAS_VEL=1;
               cd=0;
               cms-=cms_last;
              }
           else
              {
               if(cms>2000)   // ( 2")
                 {
                  DATOS_TRASPAS_VEL=0;  // indicador vel 0
                  TRASPAS_VEL=1;
                  CERO_VEL=1;
                 }
              }
          }
       }
    }

}

void velocimetro_confi(unsigned long k, unsigned int divk, float delta)
{
 fvelo = 3600./delta/(((float)k)/(1000.*(float)divk));   

 // inicialitza contadors
 CERO_VEL=1;
 DATOS_TRASPAS_VEL=0;  // indicador vel 0
 TRASPAS_VEL=1;
}

unsigned int velocimetro_get(void)
{
static unsigned int velocidad=0;
unsigned int datos;
unsigned int cd;
 if(TRASPAS_VEL)
   {
    TRASPAS_VEL = 0;
    datos = DATOS_TRASPAS_VEL; // una sola instrucció
    cd = datos>>12;
    if(datos)
       velocidad = (int) (fvelo*cd/(float)(datos& 0x0fff)+.5);
    else
       velocidad = 0;
   }
 return(velocidad); 
}

int k_fora_limits(long k)
{
 return((k<500L) || (k>=80001L));
}

int giTrigger_fora_limits(int value)
{
  return((value < giTrigger_MIN) || (value > giTrigger_MAX));
}

 



