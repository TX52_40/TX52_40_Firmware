//*************************************************************
// serial.c
//*************************************************************


#include "rtos.h"
#include "iostr710.h"

#include "71x_map.h"
#include "hard.h"

#include "cua.h"
#include "serial.h"
#include "i2c.h"

#define UART0_PRIO   0x01  // lowest priority
#define UART1_PRIO   0x01  // lowest priority
#define UART2_PRIO   0x05  
#define NUM_UARTS   3

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")


 char usart0_buf_tx[600];
 char usart0_buf_rx[2000];
 
 char usart1_buf_tx[800];
 char usart1_buf_rx[800];
 
 char usart2_buf_tx[200];
 char usart2_buf_rx[300];
 
 
typedef struct 
{
  char* buf;
  int len;
}t_defBuf;
 
t_defBuf const usartN_defBuf_tx[NUM_UARTS]=
{
  usart0_buf_tx,sizeof usart0_buf_tx,
  usart1_buf_tx,sizeof usart1_buf_tx,
  usart2_buf_tx,sizeof usart2_buf_tx,
};

t_defBuf const usartN_defBuf_rx[NUM_UARTS]=
{
  usart0_buf_rx,sizeof usart0_buf_rx,
  usart1_buf_rx,sizeof usart1_buf_rx,
  usart2_buf_rx,sizeof usart2_buf_rx,
};



t_usart usart[NUM_UARTS];



/*********************************************************************
*
*       OS_OnTx_UART0

  This routine is normally called from the transmitter buffer empty
  interrupt service handler.

  It will send out the next byte from the buffer (or string buffer).

Return value:
    0 : There are more bytes to be send.
  !=0 : Buffer empty, no more bytes to be send
*/

CODE_FAST int OS_OnTx_UART0(void)
{
int st;
char ch;
    st = cua_extreure(&usart[0].cua_tx,&ch);
    if(st == 0)
    {
    // envia caracter a usart

      UART0_TXBUFR = ch;
      UART0_IER_bit.TxEmptyIE = 1;
    }
    return(st);
}

CODE_FAST int OS_OnTx_UART1(void)
{
int st;
char ch;
    st = cua_extreure(&usart[1].cua_tx,&ch);
    if(st == 0)
    {
    // envia caracter a usart
      UART1_TXBUFR = ch;
      UART1_IER_bit.TxEmptyIE = 1;
    }
    return(st);
}

CODE_FAST int OS_OnTx_UART2(void)
{
int st;
char ch;
    st = cua_extreure(&usart[2].cua_tx,&ch);
    if(st == 0)
    {
    // envia caracter a usart

      UART2_TXBUFR = ch;
      UART2_IER_bit.TxEmptyIE = 1;
    }
    return(st);
}

/*********************************************************************
*
*       OS_OnRx
*  This routine is called from rx interrupt service handler.
*/

CODE_FAST void OS_OnRx_UART0(char ch)
{
  cua_guardar_c(&usart[0].cua_rx,ch);
}

CODE_FAST void OS_OnRx_UART1(char ch)
{
  cua_guardar_c(&usart[1].cua_rx,ch);
}

CODE_FAST void OS_OnRx_UART2(char ch)
{
  cua_guardar_c(&usart[2].cua_rx,ch);
}



/*********************************************************************
*
*       OS_COM_ISR_Usart() OS USART interrupt handler
*       handles both, Rx and Tx interrupt
*/

CODE_FAST static void UART0_IRQHandler(void) {
volatile unsigned short _Dummy;
  do {
      if (UART0_SR_bit.RxBufNotEmpty)                // Data received?
      {
        if (UART0_SR_bit.ParityError || UART0_SR_bit.FrameError ||UART0_SR_bit.OverrunError)      // Any error ?
        {  
          _Dummy = UART0_RXBUFR_bit.RX;                  // => Discard data
        }
        else
        {
          OS_OnRx_UART0(UART0_RXBUFR);                  // Process actual byte
        }
      }
    
      if (UART0_SR_bit.TxEmpty)                 // Check Tx status => Send next character
      {
        // !!! no es pot escriure  UART0_SR_bit.TxHalfEmpty = 0;                 // Clear Tx Int
        if (OS_OnTx_UART0())            // No more characters to send ?
        {
          UART0_IER_bit.TxEmptyIE = 0; // Disable further tx interrupts
          usart[0].flag_tx_activa = 0;  // desactiva transmissio

        }
      }
     } while ((UART0_IER_bit.TxEmptyIE && UART0_SR_bit.TxEmpty) || (UART0_IER_bit.RxBufNotEmptyIE && UART0_SR_bit.RxBufNotEmpty));
}

CODE_FAST void UART1_IRQHandler(void) {
volatile unsigned short _Dummy;
  do {
      if (UART1_SR_bit.RxBufNotEmpty)                // Data received?
      {
        if (UART1_SR_bit.ParityError || UART1_SR_bit.FrameError ||UART1_SR_bit.OverrunError)      // Any error ?
        {  
          _Dummy = UART1_RXBUFR_bit.RX;                  // => Discard data
        }
        else
        {
          OS_OnRx_UART1(UART1_RXBUFR);                  // Process actual byte
        }
      }
    
      if (UART1_SR_bit.TxEmpty&& (UART1_IER_bit.TxEmptyIE == 1))                 // Check Tx status => Send next character
      {
        // !!! no es pot escriure  UART1_SR_bit.TxHalfEmpty = 0;                 // Clear Tx Int
        if (OS_OnTx_UART1())            // No more characters to send ?
        {
          UART1_IER_bit.TxEmptyIE = 0; // Disable further tx interrupts
          usart[1].flag_tx_activa = 0;  // desactiva transmissio

        }
      }
     } while ((UART1_IER_bit.TxEmptyIE && UART1_SR_bit.TxEmpty) || (UART1_IER_bit.RxBufNotEmptyIE && UART1_SR_bit.RxBufNotEmpty));
}




CODE_FAST static void UART2_IRQHandler(void) {
volatile unsigned short _Dummy;
  do {
      if (UART2_SR_bit.RxBufNotEmpty)                // Data received?
      {
        if (UART2_SR_bit.ParityError || UART2_SR_bit.FrameError ||UART2_SR_bit.OverrunError)      // Any error ?
        {  
          _Dummy = UART2_RXBUFR_bit.RX;                  // => Discard data
        }
        else
        {
          OS_OnRx_UART2(UART2_RXBUFR);                  // Process actual byte
        }
      }
    
      if (UART2_SR_bit.TxEmpty)                 // Check Tx status => Send next character
      {
        // !!! no es pot escriure  UART2_SR_bit.TxHalfEmpty = 0;                 // Clear Tx Int
        if (OS_OnTx_UART2())            // No more characters to send ?
        {
          UART2_IER_bit.TxEmptyIE = 0; // Disable further tx interrupts
          usart[2].flag_tx_activa = 0;  // desactiva transmissio

        }
      }
     } while ((UART2_IER_bit.TxEmptyIE && UART2_SR_bit.TxEmpty) || (UART2_IER_bit.RxBufNotEmptyIE && UART2_SR_bit.RxBufNotEmpty));
}




int usart_tx_activa(int nUsart)
{
  return(usart[nUsart].flag_tx_activa);
}


void usart_install(int nUsart,OS_ISR_HANDLER* pISRHandler )
{

  OS_DI();
  switch( nUsart)
    {
    case 0:
      if(pISRHandler == NULL)
        OS_ARM_InstallISRHandler(UART0_INT, &UART0_IRQHandler); // UART interrupt vector.
      else
        OS_ARM_InstallISRHandler(UART0_INT, pISRHandler); // UART interrupt vector.
      OS_ARM_ISRSetPrio(UART0_INT, UART0_PRIO);       // UART interrupt level.
      OS_ARM_EnableISR(UART0_INT);                   // Enable OS UART interrupt
      break;
    case 1:
      if(pISRHandler == NULL)
        OS_ARM_InstallISRHandler(UART1_INT, &UART1_IRQHandler); // UART interrupt vector.
      else
        OS_ARM_InstallISRHandler(UART1_INT, pISRHandler); // UART interrupt vector.
      OS_ARM_ISRSetPrio(UART1_INT, UART1_PRIO);       // UART interrupt level.
      OS_ARM_EnableISR(UART1_INT);                   // Enable OS UART interrupt
      break;
    case 2:
      if(pISRHandler == NULL)
          OS_ARM_InstallISRHandler(UART2_INT, &UART2_IRQHandler); // UART interrupt vector.
      else
          OS_ARM_InstallISRHandler(UART2_INT, pISRHandler); // UART interrupt vector.
      OS_ARM_ISRSetPrio(UART2_INT, UART2_PRIO);       // UART interrupt level.
      OS_ARM_EnableISR(UART2_INT);                   // Enable OS UART interrupt
      break;
    }
  // usart no activa
  usart[nUsart].flag_tx_activa = 0;
  // reset cues d'entrada / sortida
  cua_init(&usart[nUsart].cua_tx,usartN_defBuf_tx[nUsart].buf,usartN_defBuf_tx[nUsart].len);
  cua_init(&usart[nUsart].cua_rx,usartN_defBuf_rx[nUsart].buf,usartN_defBuf_rx[nUsart].len);
  OS_RestoreI();
  
}

unsigned int bitCarga[2]={0x0010,0x0020};
unsigned char bitInvertRx[2]={0x10,0x20};
unsigned char bitInvertTx[2]={0x40,0x80};
unsigned char bitModo40[2]={0x00,0x08};
unsigned char bitModo50[2]={0x04,0x08};

void usart_hard(int nUsart, int modo, int invert_tx, int invert_rx, int carga)
{
  IOPORT1_PD &= (~bitCarga[nUsart]);
  if(carga == 1)
    IOPORT1_PD |= bitCarga[nUsart];
  
  OS_IncDI();
  
  PORT6 &= ~bitInvertTx[nUsart];
  if(invert_tx)
    PORT6 |= bitInvertTx[nUsart];

  PORT6 &= ~bitInvertRx[nUsart];
  if(invert_rx)
    PORT6 |= bitInvertRx[nUsart];
  
  PORT6 &= (tipoTx==TX40) ? ~bitModo40[nUsart] : ~bitModo50[nUsart];
  if(modo == MODO_232)
    PORT6 |= (tipoTx==TX40) ? bitModo40[nUsart] : bitModo50[nUsart];
    
  OS_DecRI();

  if(tipoTx==TX50)
  {
     IOPORT_6 = PORT6;
  }
  else
  {
    IOPORT1_PD &= (~bitCarga[0]); //El TX40 no tiene bit modo usart 0
    write_i2c(IO_EXPANDER_TX40,0x12,1,(char*)&PORT6);  // output
  }
}

void usart_init(int nUsart, int bauds, int mode, int odd )
{
unsigned short baudDivide;

  baudDivide = (22118000+bauds*8) / (bauds*16);
  OS_DI();
  
  // Install OS UART interrupt handler
  switch( nUsart)
    {
    case 0:
      UART0_CR_bit.RUN = 0;
      UART0_CR_bit.LOOPBACK = 0; // per DEBUG     

      UART0_BR = baudDivide;      
      UART0_CR_bit.MODE = mode;        //  data bits / parity
      UART0_CR_bit.STOPBITS = 1;    // 1 Stop bit (bit 4:3 = 01)
      UART0_CR_bit.PARITYODD = odd;   // Parity 
      UART0_CR_bit.RXENABLE = 1;    // Rx Enable
      UART0_CR_bit.SCENABLE = 0;    // Smartcard mode disabled
      UART0_CR_bit.FIFOENABLE = 1;  // FIFO disabled
      UART0_RXRSTR = 1;             // FIFO Reset

      UART0_CR_bit.RUN = 1 ;        // Baudrate generator on
      UART0_IER_bit.RxBufNotEmptyIE = 1;             // Enable Rx interrupts
      break;
    case 1:

      UART1_CR_bit.RUN = 0;
      UART1_CR_bit.LOOPBACK = 0; // per DEBUG     

      UART1_BR = baudDivide;      
      UART1_CR_bit.MODE = mode;        //  data bits / parity
      UART1_CR_bit.STOPBITS = 1;    // 1 Stop bit (bit 4:3 = 01)
      UART1_CR_bit.PARITYODD = odd;   // Parity
      UART1_CR_bit.RXENABLE = 1;    // Rx Enable
      UART1_CR_bit.SCENABLE = 0;    // Smartcard mode disabled
      UART1_CR_bit.FIFOENABLE = 1;  // FIFO disabled
      UART1_RXRSTR = 1;             // FIFO Reset

      UART1_CR_bit.RUN = 1 ;        // Baudrate generator on
      UART1_IER_bit.RxBufNotEmptyIE = 1;             // Enable Rx interrupts
      break;
    case 2:

      UART2_CR_bit.RUN = 0;
      UART2_CR_bit.LOOPBACK = 0; // per DEBUG     

      UART2_BR = baudDivide;      
      UART2_CR_bit.MODE = mode;        //  data bits / parity
      UART2_CR_bit.STOPBITS = 1;    // 1 Stop bit (bit 4:3 = 01)
      UART2_CR_bit.PARITYODD = odd;   // Parity
      UART2_CR_bit.RXENABLE = 1;    // Rx Enable
      UART2_CR_bit.SCENABLE = 0;    // Smartcard mode disabled
      UART2_CR_bit.FIFOENABLE = 1;  // FIFO disabled
      UART2_RXRSTR = 1;             // FIFO Reset

      UART2_CR_bit.RUN = 1 ;        // Baudrate generator on
      UART2_IER_bit.RxBufNotEmptyIE = 1;             // Enable Rx interrupts
      break;
    }
  
  // usart no activa
  usart[nUsart].flag_tx_activa = 0;
  // reset cues d'entrada / sortida
  cua_init(&usart[nUsart].cua_tx,usartN_defBuf_tx[nUsart].buf,usartN_defBuf_tx[nUsart].len);
  cua_init(&usart[nUsart].cua_rx,usartN_defBuf_rx[nUsart].buf,usartN_defBuf_rx[nUsart].len);
  OS_RestoreI();
  
}


CODE_FAST void usart_enviar(int nUsart,char* buf, int nBytes)
{
  char ch;
  int st;
  cua_guardar(&usart[nUsart].cua_tx, buf, nBytes);
  if(!usart_tx_activa(nUsart))
  {
    st = cua_extreure(&usart[nUsart].cua_tx,&ch);
    if(st ==0)
    {
         // envia caracter a usart
        usart[nUsart].flag_tx_activa = 1;
        switch(nUsart)
        {
        case 0:
          UART0_TXBUFR = ch;
          UART0_IER_bit.TxEmptyIE = 1;
          break;
        case 1:          
          UART1_TXBUFR = ch;
          UART1_IER_bit.TxEmptyIE = 1;
          break;
        case 2:
          UART2_TXBUFR = ch;
          UART2_IER_bit.TxEmptyIE = 1;
          break;
        }
    }
  }
}

// retorna buffer llegit 
CODE_FAST int usart_extreure(int nUsart,char* buf,int max_nc)
{
int i,st;
  for(i=0;i<max_nc;i++)
  {
    st = cua_extreure(&usart[nUsart].cua_rx,&buf[i]);
    if(st != 0)
      break;
  }
  return i;
    
}


// retorna 1 caracter 
CODE_FAST int usart_extreure_char(int nUsart,char* c)
{
  int st;
  st = cua_extreure(&usart[nUsart].cua_rx,c);
  return st;
    
}



