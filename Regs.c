//********************************************************
// regs.c      gestio registres
//********************************************************

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "stream.h"
#include "regs.h"
#include "i2c.h"
#include "RTOS.h"
#include "rutines.h"



void trap_error(unsigned char err)
{
}




//char array_opcions[OPCT_nmax];

extern const char* VERSION_CONF;


//************************************************************
// TAULA REGISTRES
//
//************************************************************

__no_init T_ReG reg[MAX_NUM_REGS];
__no_init T_DEF_REG defreg[MAX_NUM_DREGS];
__no_init T_TABLA tabla[MAX_NUM_TABLAS];

                               
#define  NMAX_CAMPOS_SUMA  50
                              
__no_init short CAMPOS_SUMA[NMAX_CAMPOS_SUMA];

//************************************************************
// taula tickets
//************************************************************

__no_init T_TICKET* ticket[MAX_NUM_TICKETS];








//***************************************************
// obte un camp de registre i el deix a struct s_var
//***************************************************
void get_camp(T_ReG *reg,int i_camp,T_VAR* pvar)
{
int tip,nb,pos_ini,i_camp_suma,i_suma;
char *pbuf;
 switch(reg->def_reg->tipus_reg)
   {
    case TREG_MONOCAMP:
       tip=reg->def_reg->def_camp[0].tipus_camp;
       nb =reg->def_reg->def_camp[0].nb_camp;
       pos_ini=nb*i_camp;
       pbuf=reg->buf+pos_ini;
       pvar->nb=nb;
       break;
    case TREG_MULTICAMP:
       tip=reg->def_reg->def_camp[i_camp].tipus_camp;
       nb =reg->def_reg->def_camp[i_camp].nb_camp;
       pos_ini=reg->def_reg->def_camp[i_camp].pos_ini;
       pbuf=reg->buf+pos_ini;
       pvar->nb=nb;
       break;
   }

 pvar->tipus=tip;
 switch(tip)
   {
    case VAR_UCHAR:
      pvar->var.c=*pbuf;
      break;
    case VAR_UINT:
      pvar->var.i=*(short*)pbuf;
      break;
    case VAR_ULONG:
      pvar->var.l=*(long*)pbuf;
      break;
    case VAR_FLOAT:
      pvar->var.f=*(float*)pbuf;
      break;
    case VAR_FEHOR:
      pvar->var.fh=*(T_FECHHOR*)pbuf;
      break;
    case VAR_ASC:
    case VAR_BCD:
    case VAR_AMDHM:
    case VAR_AMD:
    case VAR_HM:
      memcpy(pvar->var.s,pbuf,nb);
      break;
    case VAR_STR:
      strcpy(pvar->var.s,*(char**)pbuf);
      break;
    case VAR_SUMA:
      pvar->tipus=VAR_ULONG;  // retorna tipus long per rutines format
      pvar->var.l=0L;
      i_suma=reg->def_reg->def_camp[i_camp].pos_ini;
      do
        {
         i_camp_suma=CAMPOS_SUMA[i_suma++];         
         if(i_camp_suma==-1)
           break;
         tip=reg->def_reg->def_camp[i_camp_suma].tipus_camp;
         nb =reg->def_reg->def_camp[i_camp_suma].nb_camp;
         pos_ini=reg->def_reg->def_camp[i_camp_suma].pos_ini;
         pbuf=reg->buf+pos_ini;
         switch(tip)
           {
            case VAR_UCHAR:
              pvar->var.l+=(long)(*pbuf);
              break;
            case VAR_UINT:
              pvar->var.l+=(long)(*(short*)pbuf);
              break;
            case VAR_ULONG:
              pvar->var.l+=*(long*)pbuf;
              break;
           }   
         
        }while(1);  
      break;  
   }
}

int getMaxFilas(int i_tabla)
{
int nfilas=0;
	if((i_tabla) && (i_tabla<MAX_NUM_TABLAS))
	{
		nfilas=tabla[i_tabla].n_filas;
	}
	return(nfilas);
}

void incrementa_taula(unsigned int *p, unsigned int taula)
{
    (*p)++;
	if((taula) && (*p>=getMaxFilas(taula)))
        *p=0;
}

void decrementa_taula(unsigned int *p, unsigned int taula)
{
	if(*p)
		(*p)--;
	else
		*p=getMaxFilas(taula);
}

void tabla_move_reg(int i_tabla,int i_fila,int i_reg,eTaulaOp operacio)
{
char *p;
	if((i_tabla) && (i_tabla<MAX_NUM_TABLAS))
	{
		if(i_fila<getMaxFilas(i_tabla))
		{
			if(tabla[i_tabla].buf!=NULL)
			{
				p=tabla[i_tabla].buf+ (tabla[i_tabla].def_reg->nb_reg)*i_fila;
				if(operacio == TAULA_LOAD)  // load
					memcpy(reg[i_reg].buf,p,tabla[i_tabla].def_reg->nb_reg);
				else            // store
					memcpy(p,reg[i_reg].buf,tabla[i_tabla].def_reg->nb_reg);
			}
		}
		else
		{
			trap_error(47);
			trap_error((unsigned char)(i_tabla+(operacio*128)));
			trap_error((unsigned char)i_fila);
		}
	}
}



//*************************************
// obte buffer amb texte systema
//*************************************
void get_texte_sys(int itext,char* buf)
{
T_VAR var;
	get_camp(&reg[REG_TXTSYS],itext,&var);
	strcpy(buf,var.var.s);
}

void set_camp_fromASC(T_ReG *reg,int i_camp,char* buf,int barra_cero)
{
int nb,pos_ini,tip;
	if(i_camp>=reg->def_reg->n_camps_reg)
		return;
	switch(reg->def_reg->tipus_reg)
	{
		case TREG_MONOCAMP:
			tip=reg->def_reg->def_camp[0].tipus_camp;
			nb =reg->def_reg->def_camp[0].nb_camp;
			pos_ini=nb*i_camp;
			break;
		case TREG_MULTICAMP:
			tip=reg->def_reg->def_camp[i_camp].tipus_camp;
			nb =reg->def_reg->def_camp[i_camp].nb_camp;
			pos_ini=reg->def_reg->def_camp[i_camp].pos_ini;
			break;
	}
 switch(tip)
   {
    //case VAR_SUMA:
    //  break;           // variable ficticia
    case VAR_UCHAR:
      *(char*)((char*)reg->buf+pos_ini)=(unsigned char)atoi(buf);//buf[0];
      break;
    case VAR_UINT:
      *(short*)((char*)reg->buf+pos_ini)=atoi(buf);
      break;
    case VAR_ULONG:
      *(long*)((char*)reg->buf+pos_ini)=atol(buf);
      break;
    case VAR_FLOAT:
      *(float*)((char*)reg->buf+pos_ini)=atof(buf);
      break;
    case VAR_ASC:
		if(barra_cero)
		  memncpy((char*)reg->buf+pos_ini,buf,nb-1);
		else
		  memcpy((char*)reg->buf+pos_ini,buf,nb);
      break;  
    //default:
    //  break;
   }
}

void memcpy_separador(unsigned int nregdest, const unsigned char* buf_org, unsigned int count, unsigned char sep) 
{
T_DEF_REG *p_defreg_dest;
T_DEF_CAMP *p_camp_dest;
char* buf_dest;
unsigned int n,i;
unsigned int pos_ini_org=0;
	p_defreg_dest=reg[nregdest].def_reg;
	buf_dest=reg[nregdest].buf;
	for(i=0; (i<p_defreg_dest->n_camps_reg); i++)
	{
		p_camp_dest=&p_defreg_dest->def_camp[i];
		for(n=0; ((n<p_camp_dest->nb_camp)&&(pos_ini_org<count)&&(buf_org[pos_ini_org]!=sep)); )
		{
			buf_dest[p_camp_dest->pos_ini+n]=buf_org[pos_ini_org++];
			n++;
			if(n<p_camp_dest->nb_camp)
				buf_dest[p_camp_dest->pos_ini+n]='\0';
		}
		if(pos_ini_org>=count)
			break;
		if((n==0) && (buf_org[pos_ini_org]==sep))
		{
			buf_dest[p_camp_dest->pos_ini]=' ';
			if(p_camp_dest->nb_camp>1)
				buf_dest[p_camp_dest->pos_ini+1]='\0';
		}
		if(buf_org[pos_ini_org]==sep)
			pos_ini_org++;
	}
}


int leer_ticket_paqram(unsigned short* p_check)
{
int i,k,st,n_camps_ticket,nb,num_tickets,n_asigs,n_reg_continuacio;
char* pb;
ifacFILE *farx1;
char buf[80];
 int n_reg;
 int n_item;
 int format;
 int nc_out;
 int opc;

 st = 0;
 farx1=IFACfopen("tickets.src",'r',1);
 if(farx1==(ifacFILE*)NULL)
   {st=ERR_UNIT_FNOTFOUND;return(st);}
 
 // inicialitza array ticket a NULL    v2.14
 for(k =0;k<MAX_NUM_TICKETS;k++)
   ticket[k]=(T_TICKET*)NULL;
 
 
 // lectura diferents tickets
 do{
    pb=IFACfgets(buf,80,farx1);
    if(pb==(char*)NULL)
      {st=ERR_UNIT_FNOTFOUND;break;}
    n_asigs=sscanf(buf,"%d",&num_tickets);
 if(n_asigs!=1)
  {trap_error(12);st=ERR_UNIT_FNOTFOUND;break;}


    for(k=0;k<num_tickets;k++)
      {
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL)
          {st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"%d %d %d",&nb,&n_camps_ticket,&n_reg_continuacio);
 if(n_asigs!=3)
  {trap_error(13);st=ERR_UNIT_FNOTFOUND;break;}
       ticket[k]=(T_TICKET*)malloc_ifac(sizeof(T_TICKET));
       if(ticket[k]==NULL)
          {st=ERR_UNIT_FNOTFOUND;break;}
       ticket[k]->nb=nb;
       ticket[k]->n_camps_ticket=n_camps_ticket;
       ticket[k]->n_reg_continuacio=n_reg_continuacio;
       ticket[k]->camp_ticket=(T_CAMP_TICKET*)calloc_ifac(n_camps_ticket,sizeof(T_CAMP_TICKET));
	   if(ticket[k]->camp_ticket==NULL)
   			{st=ERR_UNIT_FNOTFOUND;break;}
       for(i=0;i<n_camps_ticket;i++)
         {
          pb=IFACfgets(buf,80,farx1);
          if(pb==(char*)NULL)
             {st=ERR_UNIT_FNOTFOUND;break;}
          n_asigs=sscanf(buf," %d %d %d %d %d",
                      &n_reg,
                      &n_item,
                      &format,
                      &nc_out,
                      &opc);
 if(n_asigs!=5)
  {trap_error(14);st=ERR_UNIT_FNOTFOUND;break;}
          ticket[k]->camp_ticket[i].n_reg  =n_reg;
          ticket[k]->camp_ticket[i].n_item =n_item;
          ticket[k]->camp_ticket[i].format =format;
          ticket[k]->camp_ticket[i].nc_out =nc_out;
          ticket[k]->camp_ticket[i].opc    =opc;
         }
       if(st)
          break;
      }
    if(st)break;    // per si hi hagues mes instruccions
   }while(0);
 *p_check += farx1->entry.check;
 IFACfclose(farx1);
 return(st);
}


int leer_array_chars_paqram(ifacFILE *farx1,unsigned int nreg)
{
char* pb;
char buf[80];
int i,n_lins,n_asigs,opc;
	pb=IFACfgets(buf,80,farx1);   // opcions menu inicial
	if(pb==(char*)NULL)
		return(ERR_UNIT_FNOTFOUND);
	n_asigs=sscanf(buf,"%d",&n_lins);
	if(n_asigs!=1)
	{
		trap_error(6);
		return(ERR_UNIT_FNOTFOUND);
	}
    reg[nreg].buf=(char*)calloc_ifac(n_lins+1,sizeof(char));
	if(reg[nreg].buf==NULL)
		return(ERR_UNIT_FNOTFOUND);
	for(i=0;i<n_lins;i++)
	{
		pb=IFACfgets(buf,80,farx1);
		if(pb==(char*)NULL)
			return(ERR_UNIT_FNOTFOUND);
		n_asigs=sscanf(buf,"%d",&opc);
		if(n_asigs!=1)
		{
			trap_error(7);
			return(ERR_UNIT_FNOTFOUND);
		}
		reg[nreg].buf[i+1]=(char)opc;
	}
	return 0;
}

int leer_reg_monocampo_paqram( char* fitxer, unsigned int nreg,unsigned short* p_check)
{
int i,st,n_lins,n_asigs;
char* pb;
ifacFILE *farx1;
char buf[80];
char texte[80];
char** p_txt;

 st = 0;
 farx1=IFACfopen(fitxer,'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}
 do{
    pb=IFACfgets(buf,80,farx1);   // num. textes
    if(pb==(char*)NULL)
       {st=ERR_UNIT_FNOTFOUND;break;}

    n_asigs=sscanf(buf,"%d",&n_lins);
 if(n_asigs!=1)
  {trap_error(18);st=ERR_UNIT_FNOTFOUND;break;}

    reg[nreg].buf=(char*)calloc_ifac(n_lins,sizeof(char *));
	if(reg[nreg].buf==NULL)
	{trap_error(18);st=ERR_UNIT_FNOTFOUND;break;}

    p_txt=(char**)reg[nreg].buf;
    for(i=0;i<n_lins;i++)
      {
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL)
         {st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"\"%[^\"]\" ",texte);
 if(n_asigs!=1)
  {trap_error(19);st=ERR_UNIT_FNOTFOUND;break;}
       p_txt[i]=(char*)malloc_ifac(strlen(texte)+1);
	if(p_txt[i]==NULL)
	{trap_error(19);st=ERR_UNIT_FNOTFOUND;break;}
       strcpy(p_txt[i],texte);
      }
    if(st)break;
   }while(0);
 *p_check += farx1->entry.check;
 IFACfclose(farx1);
 return(st);
}

int leer_textsys_paqram(unsigned short* p_check)
{
	return(leer_reg_monocampo_paqram( "txtsys.src", REG_TXTSYS,p_check));
}

int leer_textgen_paqram(unsigned short* p_check)
{
	return(leer_reg_monocampo_paqram( "txtgen.src", REG_TXTGEN,p_check));
}

int leer_opcts_paqram(unsigned short* p_check)
{
int st;
ifacFILE *farx1;

 st = 0;
        farx1=IFACfopen("opcts.src",'r',1);
	if(farx1==(ifacFILE*)NULL)
		{st=ERR_UNIT_FNOTFOUND;return(st);}
	st=leer_array_chars_paqram(farx1,REG_OPCTS);
        *p_check += farx1->entry.check;
	IFACfclose(farx1);
	return(st);
}


int leer_defregs_paqram(unsigned short* p_check)
{
int i,k,st,n_camps_reg,tipus_reg,num_defs,nb_reg,i_reg,n_asigs;
char* pb;
ifacFILE *farx1;
char buf[80];
int tipus_camp;
int nb_camp   ;
int pos_ini   ; 
int nc_read,ac_nc_read;
int i_campos_suma;
 
 st = 0;
 i_campos_suma=0;  // index per omplir taula campos_suma                  
 farx1=IFACfopen("defregs.src",'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}

 // lectura diferents defs
 do{
    pb=IFACfgets(buf,80,farx1);
    if(pb==(char*)NULL)
      {st=ERR_UNIT_FNOTFOUND;break;}
    n_asigs=sscanf(buf,"%d",&num_defs);
    
    if(n_asigs!=1){trap_error(26);st=ERR_UNIT_FNOTFOUND;break;}

    for(k=0;k<num_defs;k++)
      {
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL)
          {st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"%d " ,&i_reg);
       
       if(n_asigs!=1){trap_error(27);st=ERR_UNIT_FNOTFOUND;break;}
        
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL){st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"%d %d %d",&tipus_reg,
                          &n_camps_reg,
                          &nb_reg);
       
       if(n_asigs!=3){trap_error(28);st=ERR_UNIT_FNOTFOUND;break;} 
         
       defreg[i_reg].tipus_reg=tipus_reg;
       defreg[i_reg].n_camps_reg=n_camps_reg;
       defreg[i_reg].nb_reg=nb_reg;
       defreg[i_reg].def_camp=(T_DEF_CAMP*)calloc_ifac(n_camps_reg,sizeof(T_DEF_CAMP));//V5.12 Traspasado desde _txt a _zon
       for(i=0;i<n_camps_reg;i++)
         {
          pb=IFACfgets(buf,80,farx1);
          if(pb==(char*)NULL){st=ERR_UNIT_FNOTFOUND;break;}
            
          n_asigs=sscanf(buf,"  %d %d %d%n",
                      &tipus_camp,
                      &nb_camp,
                      &pos_ini,&nc_read);
 
          if(n_asigs!=3){trap_error(29);st=ERR_UNIT_FNOTFOUND;break;}

          defreg[i_reg].def_camp[i].tipus_camp=tipus_camp;
          defreg[i_reg].def_camp[i].nb_camp   =nb_camp   ;
          if(tipus_camp!=VAR_SUMA)
             defreg[i_reg].def_camp[i].pos_ini   =pos_ini   ;
          else
            {
             defreg[i_reg].def_camp[i].pos_ini   =i_campos_suma;
             CAMPOS_SUMA[i_campos_suma++]=pos_ini;
             // llegir resta de camps que formen la suma
             ac_nc_read=0;
             do{         
                ac_nc_read+=nc_read;
                n_asigs=sscanf(buf+ac_nc_read," %d%n",
                            &pos_ini,&nc_read);
 
                if(n_asigs!=1){trap_error(30);st=ERR_UNIT_FNOTFOUND;break;}
                CAMPOS_SUMA[i_campos_suma++]=pos_ini;
                if(pos_ini==-1)
                  break;                
               }while(1);
             if(st) break;   
            }   
         }
       if(st)break;
      }
    if(st)break;
   }while(0);
 *p_check += farx1->entry.check;
 IFACfclose(farx1);
 return(st);
}

int leer_datos_reg(ifacFILE *farx1,int reglectura,int n_camps)
{
char* pb;
char* p_dest;
unsigned short iaux;
unsigned long laux;
unsigned char caux;
float faux;
int i,ii,nb,n_asigs,st;
static char buf[80];
static char texte[80];
 st=0;
 for(i=0;i<n_camps;i++)
   {                        
    pb=IFACfgets(buf,80,farx1);
    if(pb==(char*)NULL)
      {st=ERR_UNIT_FNOTFOUND;break;}
    p_dest=reg[reglectura].buf+reg[reglectura].def_reg->def_camp[i].pos_ini;
    nb=reg[reglectura].def_reg->def_camp[i].nb_camp;  
    switch(reg[reglectura].def_reg->def_camp[i].tipus_camp)
       {
        case VAR_UCHAR:
          n_asigs=sscanf(buf," %hd",&iaux);
          if(n_asigs!=1)
              {trap_error(31);st=ERR_UNIT_FNOTFOUND;break;}
          caux=iaux;
          memcpy(p_dest,&caux,nb);    
          break;
        case VAR_UINT:
          n_asigs=sscanf(buf," %hd",&iaux);
          if(n_asigs!=1)
              {trap_error(32);st=ERR_UNIT_FNOTFOUND;break;}
          memcpy(p_dest,(char*)&iaux,nb);    
          break;
        case VAR_ULONG:
          n_asigs=sscanf(buf," %ld",&laux);
          if(n_asigs!=1)
              {trap_error(33);st=ERR_UNIT_FNOTFOUND;break;}
          memcpy(p_dest,(char*)&laux,nb);    
          break;
        case VAR_FLOAT:
          n_asigs=sscanf(buf," %f",&faux);
          if(n_asigs!=1)
              {trap_error(33);st=ERR_UNIT_FNOTFOUND;break;}
          memcpy(p_dest,(char*)&faux,nb);    
          break;
	case VAR_FEHOR:
          n_asigs=sscanf(buf,"\"%[^\"]\" ",texte);
          if(n_asigs==1)
          {
            for(ii=0; ii<nb; ii++)
                p_dest[ii]=hexa_to_uchar((unsigned char *)&texte[ii*2]);
          }
          break;
        case VAR_ASC:
          n_asigs=sscanf(buf,"\"%[^\"]\" ",texte);
          if(n_asigs==0)
			p_dest[0]=texte[0]='\0';		  
		  else if(n_asigs!=1) 
		  {	
			trap_error(34);
			st=ERR_UNIT_FNOTFOUND;
			break;
		  }	
		  else
		  { 
			texte[nb]='\0';
			strncpy(p_dest,texte,nb);
		  }
          break;
        default:  
          trap_error(35);st=ERR_UNIT_FNOTFOUND;
          break;
       } 
    if(st)
      break;    
   }
 return(st);  
}

int leer_regs_paqram(unsigned short* p_check)
{
int st,i_reg,i_def,n_val,n_asigs;
char* pb;
ifacFILE *farx1;
char buf[80];

 st = 0;
 farx1=IFACfopen("regs.src",'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}

 // lectura diferents regs
 do{
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL)
          {st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"%d %d %d",&i_reg,
                          &i_def,
                          &n_val);
       if(n_asigs!=3)
        {trap_error(36);st=ERR_UNIT_FNOTFOUND;break;}
       if(i_reg==-1)
         break;
       reg[i_reg].def_reg=&defreg[i_def];
       reg[i_reg].buf=(char*)malloc_ifac(defreg[i_def].nb_reg);
       st=leer_datos_reg(farx1,i_reg,n_val);                        
       if(st)break;
   }while(1);
 *p_check += farx1->entry.check;
 IFACfclose(farx1);
 return(st);
}


int leer_tablas_paqram(unsigned short* p_check)
{
int i,st,i_tab,i_def,i_reglectura,n_asigs,n_maxfilas,n_filasllenas;
char* pb;
ifacFILE *farx1;
char buf[80];

 st = 0;
 farx1=IFACfopen("load_tab.src",'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}

 // lectura diferents taules
 do{
       pb=IFACfgets(buf,80,farx1);
       if(pb==(char*)NULL)
          {st=ERR_UNIT_FNOTFOUND;break;}
       n_asigs=sscanf(buf,"%d %d %d %d %d",&i_tab,
                          &i_def,          
                          &i_reglectura,
                          &n_maxfilas,
                          &n_filasllenas);
       if(n_asigs!=5)
        {trap_error(37);st=ERR_UNIT_FNOTFOUND;break;}
       if(i_tab==-1)
         break;
       tabla[i_tab].def_reg=&defreg[i_def];
       tabla[i_tab].n_filas=n_maxfilas;
        tabla[i_tab].buf=(char*)malloc_ifac(defreg[i_def].nb_reg*n_maxfilas);
       for(i=0;i<n_filasllenas;i++)
         {
          st=leer_datos_reg(farx1,i_reglectura,defreg[i_def].n_camps_reg);                        
          if(st)
            break;
          tabla_move_reg(i_tab,i,i_reglectura,TAULA_STORE);
          //tabla_store_reg(i_tab,i,i_reglectura);
         }
       if(st)break;
   }while(1);
 IFACfclose(farx1);
 *p_check += farx1->entry.check;
 return(st);
}





int leer_prog_pic(void)
{
  int st,len_arxiu,i,llegits;
ifacFILE *farx1;
char buf[16];

 st = 0;
 // llegeig versio firmware
 wread_i2c(PIC_FIRMWARE,READ_FIRMWARE, 16, buf);

 farx1=IFACfopen("progpic.bin",'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}
  len_arxiu =farx1->entry.fsize;
  if(len_arxiu == 0 || (len_arxiu %10))
    {
      IFACfclose(farx1);
      st=ERR_UNIT_FNOTFOUND;return(st);
    }

  
  // pasa pic a modo BOOTLOADER
  write_i2c(PIC_FIRMWARE,BACK_TO_BOOT, 1, buf);
 OS_Delay(20);
 wread_i2c(PIC_BOOTLOADER,READ_FIRMWARE, 16, buf);
 for(i=0;i<len_arxiu;i+=10)
 {
	llegits=IFACfread(buf, 10, farx1);		
	if(llegits!=10)						
          {st=ERR_UNIT_FNOTFOUND;break;}
        write_i2c(PIC_BOOTLOADER,ADR_STORE, 10, buf);
        OS_Delay(40);

 }
 OS_Delay(40);
 write_i2c(PIC_BOOTLOADER,RESET_PIC, 1, buf);
 OS_Delay(40);
 // llegeig versio firmware
 wread_i2c(PIC_FIRMWARE,READ_FIRMWARE, 16, buf);
 
 IFACfclose(farx1);
 
  return(st);
}

// lectura arxiu per imprimir ANAGRAMES

__no_init char* memory_bmp;
__no_init unsigned int mem_bmp_size;

int leer_bmp_paqram(unsigned short* p_check)
{
int st, llegits;
ifacFILE *farx1;
int len_arxiu;

 memory_bmp=NULL;
 mem_bmp_size=0;
 
 st = 0;
 farx1=IFACfopen("bmps.bin",'r',1);
 if(farx1==(ifacFILE*)NULL)
    {st=ERR_UNIT_FNOTFOUND;return(st);}
 len_arxiu=farx1->entry.fsize;
 if(len_arxiu)
	{
	memory_bmp=malloc_ifac(len_arxiu+1);
	if(memory_bmp!=NULL)
		{
		llegits=IFACfread(memory_bmp, len_arxiu, farx1);
		if(llegits!=len_arxiu)
			{st=ERR_UNIT_FNOTFOUND;}
		mem_bmp_size=(unsigned int)llegits;
		}
	else
		st=ERR_UNIT_FNOTFOUND;
	}
 *p_check += farx1->entry.check;
 IFACfclose(farx1);
 return(st);
}


void set_opc_tck(char n_op)
{
	if(reg[REG_OPCTS].buf!=NULL)
	{
		if((n_op>OPCT_CERO) && (n_op<OPCT_nmax))
	//		array_opcions[n_op]|=MSK_TD30;
			reg[REG_OPCTS].buf[n_op]|=MSK_TD30;
	}
}

void reset_opc_tck(char n_op)
{
	if(reg[REG_OPCTS].buf!=NULL)
	{
		if((n_op>OPCT_CERO) && (n_op<OPCT_nmax))
			reg[REG_OPCTS].buf[n_op]&=NOT_MSK_TD30;
	//		array_opcions[n_op]&=NOT_MSK_TD30;
	}
}

//void reset_all_opc_tck(void)
//{
//	memset(array_opcions,0,OPCT_nmax);
//}

char val_opc_tck(char n_op, unsigned long val)
{
	if(reg[REG_OPCTS].buf==NULL)
		return 1;
	if(val)
		set_opc_tck(n_op);
	else
		reset_opc_tck(n_op);
	return(reg[REG_OPCTS].buf[n_op] & MSK_TD30);
}

char get_opc_tck(char n_op)
{
	if(reg[REG_OPCTS].buf==NULL)
		return 1;
	return(reg[REG_OPCTS].buf[n_op] & MSK_TD30);
}


char get_tot_opc_tck(char n_op)
{
	if(reg[REG_OPCTS].buf==NULL)
		return 1;
	return(reg[REG_OPCTS].buf[n_op]);
}

/*
void save_opcs_tck(char store)// 1 salva les opcions, 0 les recupera
{
static char sav_opcions[OPCT_nmax];
	if(store)
		memcpy(sav_opcions, reg[REG_OPCTS].buf, OPCT_nmax*sizeof(char));
	else
		memcpy(reg[REG_OPCTS].buf, sav_opcions, OPCT_nmax*sizeof(char));
}
*/