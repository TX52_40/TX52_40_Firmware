// ********************************************************
// regs.h   definicions per gestio registres
// ********************************************************
#ifndef _INC_REGS
#define _INC_REGS


#include "reloj.h"     // per T_FECHHOR 
#include "tdgp.h"


typedef enum{TAULA_STORE = 0, TAULA_LOAD}eTaulaOp;

#define MAX_TRAPS		26*2 //26*3 //32	

#define MSK_TD30		0x01
#define NOT_MSK_TD30	        0xfe




typedef struct s_var
{
 short tipus;
 short nb;
 union u_var
  {
   char c;
   short i;
   long l;
   char s[220];
   //char s[51];
   T_FECHHOR fh;
   float f;
  }var;
}T_VAR;

//***********************************************************
// registres 
//***********************************************************
 
typedef struct s_def_camp
{
 short tipus_camp;
 short nb_camp;
 short pos_ini;
}T_DEF_CAMP;

typedef struct s_def_reg
{
 short tipus_reg;
 short n_camps_reg;
 short nb_reg;
 T_DEF_CAMP* def_camp;
}T_DEF_REG;    


typedef struct// s_reg
{
 T_DEF_REG* def_reg;
 char *buf;
}T_ReG;     // la e es per no xocar amb T_REG de HI8KERPN.h

extern T_ReG reg[MAX_NUM_REGS];

//*********************************************************
// TABLAS
//*********************************************************

typedef struct s_tabla
{         
 T_DEF_REG* def_reg;
 short n_filas;
 char* buf;
} T_TABLA; 

__no_init extern T_TABLA tabla[MAX_NUM_TABLAS];

typedef struct s_camp_ticket
{
// char *texte;
 short n_reg;
 short n_item;
 short format;
 short nc_out;
 short opc;
}T_CAMP_TICKET;

typedef struct s_ticket
{
 short nb;
 short n_camps_ticket;
 short n_reg_continuacio;
 T_CAMP_TICKET *camp_ticket;
}T_TICKET;

 extern T_DEF_REG defreg[MAX_NUM_DREGS];
 extern T_TICKET* ticket[MAX_NUM_TICKETS];



//*********************************************************
// prototips rutines
//*********************************************************

extern void trap_error(unsigned char err);
extern void reset_traps(void);

extern void memcpy_separador(unsigned int nregdest, const unsigned char* buf_org, unsigned int count, unsigned char sep);
extern void get_camp(T_ReG *reg,int i_camp,T_VAR* pvar);
extern void set_camp_fromASC(T_ReG *reg,int i_camp,char* buf,int barra_cero);
extern void set_camp_directe(T_ReG *reg,int i_camp,char* buf);
extern int getMaxFilas(int i_tabla);
extern void incrementa_taula(unsigned int *p, unsigned int taula);
extern void decrementa_taula(unsigned int *p, unsigned int taula);
extern void tabla_move_reg(int i_tabla,int i_fila,int i_reg,eTaulaOp operacio);
//extern void tabla_load_reg(int i_tabla,int i_fila,int i_reg);
//extern void tabla_store_reg(int i_tabla,int i_fila,int i_reg);
extern void get_texte_sys(int itext,char* buf);

extern int  leer_ticket_paqram(unsigned short* p_check);
extern int  leer_textgen_paqram(unsigned short* p_check);
extern int  leer_opcts_paqram(unsigned short* p_check);
extern int  leer_textsys_paqram(unsigned short* p_check);
extern int  leer_defregs_paqram(unsigned short* p_check); 
extern int  leer_regs_paqram(unsigned short* p_check);
extern int  leer_tablas_paqram(unsigned short* p_check);
extern int leer_bmp_paqram(unsigned short* p_check);

extern void set_opc_tck(char n_op);
extern void reset_opc_tck(char n_op);
//extern void reset_all_opc_tck(void);
extern char val_opc_tck(char n_op, unsigned long val);
extern char get_opc_tck(char n_op);
//extern void save_opcs_tck(char store);// 1 salva les opcions, 0 les recupera


extern int leer_prog_pic(void);

#endif
