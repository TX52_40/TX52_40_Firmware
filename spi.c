//*******************************************************************
// spi.c
//*******************************************************************

#include "iostr710.h"

#include "RTOS.H"

#include "hard.h"

OS_RSEMA SemaSPI;


#define CS_DISP1_ON {IOPORT2_PD &= 0xffbf;}
#define CS_DISP1_OFF {IOPORT2_PD |= 0x0040;}

#define CS_DISP2_ON {IOPORT2_PD &= 0xff7f;}
#define CS_DISP2_OFF {IOPORT2_PD |= 0x0080;}

extern void delay_us(int us);


unsigned short reverse_output_bits(unsigned char caracter)
{
  int i;
  unsigned char mask;
  unsigned short mask1;
  unsigned short ret=0;
  
  for (i=0, mask=1, mask1=0x8000 ;i<8; i++, mask<<=1, mask1>>=1)
  {
      if (caracter & mask) ret |= mask1;
  }
  return (ret);
}

unsigned char reverse_input_bits(unsigned short caracter)
{
  int i;
  unsigned char mask;
  unsigned short mask1;
  unsigned char ret=0;
  
  for (i=0, mask=1, mask1=0x8000 ;i<8; i++, mask<<=1, mask1>>=1)
  {
      if (caracter & mask1) ret |= mask;
  }
  return (ret);
}

void spi_init(void)
{
  if(tipoTx!=TX50) return;
  // configura SPI 1
  BSPI1_CLK = 40;
  BSPI1_CSR1 = 0x0301;
  BSPI1_CSR1 = 0x0303;
  BSPI1_CSR2 = 0x0001;
  OS_CREATERSEMA(&SemaSPI);
  CS_DISP1_OFF;
  CS_DISP2_OFF;
}




void spi_send(int num_dev, unsigned char *buf, int num_c)
{
  int i;
  int reverse;
  volatile unsigned short dum;
  if(tipoTx!=TX50) return;
  if (num_c==0)
    return;
  
  OS_Use(&SemaSPI);   // Semaforo per SPI
  switch(num_dev)
  {
  case 0:
    CS_DISP1_ON;
    reverse = 1;
    break;
  case 1:
    CS_DISP2_ON;
    reverse = 1;
    break;
  case 2:
    reverse = 0;
    break;
  default:
    OS_Unuse(&SemaSPI);   // Semaforo per SPI
    return;
  }
  
  while(BSPI1_CSR2_bit.RFNE) dum = BSPI1_RXR;
  for(i=0;i<num_c;i++)
  {
    if(reverse)
        BSPI1_TXR = reverse_output_bits(buf[i]);
    else
        BSPI1_TXR = buf[i]<<8;
    while(!BSPI1_CSR2_bit.RFNE);
    dum = BSPI1_RXR;
    delay_us(1);
  }

  switch(num_dev)
  {
  case 0:
    CS_DISP1_OFF;
    break;
  case 1:
    CS_DISP2_OFF;
    break;
  case 2:
    // latcha bytes enviats
    OS_IncDI();
    PORT3 &= 0xbf;
    IOPORT_3 = PORT3;
    OS_DecRI();
    
    delay_us(1);
    
    OS_IncDI();
    PORT3 |= 0x40;
    IOPORT_3 = PORT3;
    OS_DecRI();
    break;
  }
 OS_Unuse(&SemaSPI);   // Semaforo per SPI

}


void spi_recv(int num_dev, unsigned char *buf, int num_c)
{
  volatile unsigned short dum;
  int i;
  if(tipoTx!=TX50) return;
  if (num_c==0)
    return;
  
  OS_Use(&SemaSPI);   // Semaforo per SPI
  
  switch(num_dev)
  {
  case 0:
    CS_DISP1_ON;
    break;
  case 1:
    CS_DISP2_ON;
    break;
  default:
    OS_Unuse(&SemaSPI);   // Semaforo per SPI
    return;
  }

    while(BSPI1_CSR2_bit.RFNE) dum = BSPI1_RXR;
    BSPI1_TXR = reverse_output_bits(0x42); // Read Key Data
    while(!BSPI1_CSR2_bit.RFNE);
    dum = BSPI1_RXR;
    delay_us(2);
  for(i=0;i<num_c;i++)
  {
    BSPI1_TXR = 0;
    while(!BSPI1_CSR2_bit.RFNE);
    buf[i] = reverse_input_bits(BSPI1_RXR);
    delay_us(2);
  }

  switch(num_dev)
  {
  case 0:
    CS_DISP1_OFF;
    break;
  case 1:
    CS_DISP2_OFF;
    break;
  }
  OS_Unuse(&SemaSPI);   // Semaforo per SPI
  
}

