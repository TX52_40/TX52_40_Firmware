/*********************************************************************/
/*  sem30.c      Semantiques tx30                                    */
/*********************************************************************/

#include <stdlib.h>
#include <string.h>
#include "iostr710.h"
#include <intrinsics.h>

#include "tautrans.h"
#include "tx30.h"
#include "regs.h"
#include "aplic.h"
#include "rut30.h"
#include "display.h"
#include "i2c.h"
#include "impre.h"
#include "reloj.h"
#include "hard.h"
#include "lumin.h"
#include "teclat.h"
#include "bloqueig_torn.h"
#include "ccab.h"
#include "prima.h"
#include "format.h"
#include "RTOS.h"
#include "zapper.h"
#include "tots.h"
#include "totsEq.h"
#include "multiplex.h"
#include "car30.h"
#include "insika.h"
#include "pausa.h"
#include "td30.h"




#define METRES_SALT_XILE 200
#define MINUTS_SALT_XILE 1

#define COBERTURA_SEGONS_TIMEOUT 20

int hay_precio_acordado;
int precio_acordado;

int timCancPrecAc = 0;

void set_precio_acordado(int precio) { 
	
	precio_acordado = precio; 
	hay_precio_acordado = precio ? 1 : 0;
}

void setBthConnected(char connected){
	if(hay_precio_acordado){
		if(connected == '0'){
			if(timCancPrecAc== 0)
				timCancPrecAc = 600 * 100;	//0 minuts
		} else {
			timCancPrecAc=0;
		}
	}
}

void loop10ms(void){
	if(hay_precio_acordado && timCancPrecAc){
		set_precio_acordado(0);
	}
}

void copia_tots_a_reg(unsigned char ndig)
{
unsigned long mask;
unsigned char i;
int st;
	if(ndig>8)ndig=8;
	for(i=0,mask=1L;i<ndig;i++)
		mask*=10L;
	reg_tcks_tx30->cTOT_NSER    =totalitzador_read(TOT_NSER    , &st)%mask;
	reg_tcks_tx30->cTOT_IMPC    =totalitzador_read(TOT_IMPC    , &st)%mask;
	reg_tcks_tx30->cTOT_IMPS    =totalitzador_read(TOT_IMPS    , &st)%mask;
	reg_tcks_tx30->cTOT_IMPT    =totalitzador_read(TOT_IMPT    , &st)%mask;
	reg_tcks_tx30->cTOT_KMT     =totalitzador_read(TOT_KMT     , &st)%mask;
	reg_tcks_tx30->cTOT_KMO     =totalitzador_read(TOT_KMO     , &st)%mask;
	reg_tcks_tx30->cTOT_KML     =totalitzador_read(TOT_KML     , &st)%mask;
	reg_tcks_tx30->cTOT_KMLP    =totalitzador_read(TOT_KMLP    , &st)%mask;
	reg_tcks_tx30->cTOT_TOC     =totalitzador_read(TOT_TOC     , &st)%mask;
	reg_tcks_tx30->cTOT_TON     =totalitzador_read(TOT_TON     , &st)%mask;
	reg_tcks_tx30->cTOT_NDESC   =totalitzador_read(TOT_NDESC   , &st)%mask;
	reg_tcks_tx30->cTOT_TDESC   =totalitzador_read(TOT_TDESC   , &st)%mask;
	reg_tcks_tx30->cTOT_BORR    =totalitzador_read(TOT_BORR    , &st)%mask;
	reg_tcks_tx30->cTOT_NSER_P  =totalitzador_read(TOT_NSER_P  , &st)%mask;
	reg_tcks_tx30->cTOT_IMPC_P  =totalitzador_read(TOT_IMPC_P  , &st)%mask;
	reg_tcks_tx30->cTOT_IMPS_P  =totalitzador_read(TOT_IMPS_P  , &st)%mask;
	reg_tcks_tx30->cTOT_IMPT_P  =totalitzador_read(TOT_IMPT_P  , &st)%mask;
	reg_tcks_tx30->cTOT_KMT_P   =totalitzador_read(TOT_KMT_P   , &st)%mask;
	reg_tcks_tx30->cTOT_KMO_P   =totalitzador_read(TOT_KMO_P   , &st)%mask;
	reg_tcks_tx30->cTOT_KML_P   =totalitzador_read(TOT_KML_P   , &st)%mask;
	reg_tcks_tx30->cTOT_KMLP_P  =totalitzador_read(TOT_KMLP_P  , &st)%mask;
	reg_tcks_tx30->cTOT_TOC_P   =totalitzador_read(TOT_TOC_P   , &st)%mask;
	reg_tcks_tx30->cTOT_TON_P   =totalitzador_read(TOT_TON_P   , &st)%mask;
	reg_tcks_tx30->cTOT_SALTS   =totalitzador_read(TOT_SALTS   , &st)%mask;
	reg_tcks_tx30->cTOT_TONHHMM =totalitzador_read(TOT_TONHHMM , &st)%mask;
	reg_tcks_tx30->cTOT_E3      =totalitzador_read(TOT_E3      , &st)%mask;
	reg_tcks_tx30->cTOT_E6      =totalitzador_read(TOT_E6      , &st)%mask;
	reg_tcks_tx30->cTOT_NTICK   =totalitzador_read(TOT_NTICK   , &st)%mask;
	reg_tcks_tx30->cTOT_BB      =totalitzador_read(TOT_BB      , &st)%mask;  // v 3.05  
	reg_tcks_tx30->cTOT_BB_P    =totalitzador_read(TOT_BB_P    , &st)%mask;  // v 3.05
    reg_tcks_tx30->cTOT_KMOFF   =totalitzador_read(TOT_KMOFF   , &st)%mask;
    reg_tcks_tx30->cTOT_KMOFF_P =totalitzador_read(TOT_KMOFF_P , &st)%mask;

	// copia fecha borrat totalitzadors (v 1.04)
	reg_tcks_tx30->fecha_borr_tots = blqs.fecha_borr_tots;
        
} 

void copia_tots_eq_a_reg(int periode, unsigned char ndig, int parcial){
	unsigned long mask;
	unsigned char i;
	T_FECHHOR fecha_aux;
	unsigned long aux;
	
	if(ndig>8)ndig=8;
	for(i=0,mask=1L;i<ndig;i++)
		mask*=10L;
	
	reg_tcks_tx30->cTOT_NSER = tot_eq_read(TOT_EQ_NSER, periode)%mask;
	reg_tcks_tx30->cTOT_KMT  = tot_eq_read(TOT_EQ_KMT,  periode)%mask;
	aux = tot_eq_read(TOT_EQ_IMPT, periode)%mask;
	reg_tcks_tx30->cTOT_IMPT = aux;

	getFechaFromSegundos(tots_eq[periode].timeIni, &fecha_aux);
	reg_tcks_tx30->f_inst = fecha_aux;
	
	if(parcial){
		get_reloj_fechhor(RELLOTGE_PLACA, &fecha_aux);
	} else {
		// Restem un segon perqu� surti la data del dia anterior
		getFechaFromSegundos(tots_eq[periode].timeFi - 1, &fecha_aux);
	}
	
	reg_tcks_tx30->f_exp = fecha_aux;
}

	
	
void pchar_to_fechhor(T_FECHHOR* dest, char* org)
{
 dest->day=bcd_bin(org[0]);
 dest->month=bcd_bin(org[1]);
 dest->year=bcd_bin(org[2]);
}


void copia_serie_lic_a_reg(void)
{
 reg_tcks_tx30->n_lic[0]=blqs.n_lic[2];	   // numero de licencia
 reg_tcks_tx30->n_lic[1]=blqs.n_lic[1];
 reg_tcks_tx30->n_lic[2]=blqs.n_lic[0];
 
 reg_tcks_tx30->n_serie[0]=blqs.n_serie[2];	   // numero de serie del taximetro
 reg_tcks_tx30->n_serie[1]=blqs.n_serie[1];	   
 reg_tcks_tx30->n_serie[2]=blqs.n_serie[0];	   
}

void copia_parms_a_reg(void)
{
 reg_tcks_tx30->k=blqs.k;		    // constante K
 reg_tcks_tx30->nt[0]=blqs.nt[1];	   // numero de serie de la tarifa
 reg_tcks_tx30->nt[1]=blqs.nt[0];


 pchar_to_fechhor(&reg_tcks_tx30->f_grab,blqs.f_grab);	                // fecha grabacion tarifa
 pchar_to_fechhor(&reg_tcks_tx30->visu_f_precambio,visu_f_precambio);	 // fecha precambio tarifa
 pchar_to_fechhor(&reg_tcks_tx30->f_exp,blqs.f_exp);	 // fecha fabricacion
 pchar_to_fechhor(&reg_tcks_tx30->f_inst,blqs.f_inst);	 // fecha instalacion

 reg_tcks_tx30->CCC[0]=blqs.CCC[1];	   // numero de distribuidor de la tarifa
 reg_tcks_tx30->CCC[1]=blqs.CCC[0];
 
 reg_tcks_tx30->visu_chk=reverse_int(visu_chk);	          // check de la tarifa 
 reg_tcks_tx30->visu_chk_prec=reverse_int(visu_chk_prec); // check de la tarifa de precambio
 
 reg_tcks_tx30->check_tiquet = reverse_int(check_tiquet);   // check de tiquets
 reg_tcks_tx30->check_programa=reverse_int(check_programa); // check de programa
 
 reg_tcks_tx30->nmodifs_k  =blqs.nmodifs_k  ;	   // numero modificaciones k
 reg_tcks_tx30->nmodifs_tar=blqs.nmodifs_tar;	   // numero modificaciones de la tarifa


 reg_tcks_tx30->CCC_inst[0]=blqs.CCC_inst[1];	   // numero distrib. inst. taximetro
 reg_tcks_tx30->CCC_inst[1]=blqs.CCC_inst[0];	   

 reg_tcks_tx30->VERSION_TX[0]=VERSION_TX[0];	   // VERSION taximetro
 reg_tcks_tx30->VERSION_TX[1]=VERSION_TX[1];	   

}

struct ascii_ss{
char asc;
unsigned char ss;
} const ascii_ss[]={'1','\x06','2','\x5b','3','\x4f','4','\x66','5','\x6d',
              '6','\x7d','7','\x07','8','\x7f','9','\x6f','0','\x3f',
              ' ','\x00',
              'A',0x77,'b',0x7c,'C',0x39,'c',0x58,'d',0x5e,'E',0x79,'e',0x7b,
              'F',0x71,'H',0x76,'h',0x74,'I',0x30,'i',0x10,'i',0x04,'J',0x1e,
              'L',0x38,'N',0x37,'n',0x54,'O',0x3f,'o',0x5c,'P',0x73,'p',0x73,
              'R',0x77,'r',0x50,'S',0x6d,'t',0x78,'U',0x3e,'u',0x1c,'Y',0x6e,
              '\x00','\x7f'
              };

unsigned char ss_a_ascii(unsigned char ss)
{
int i=0;
int nmax;
 ss &=0x7f;   /* treu el punt */
 nmax=sizeof ascii_ss/sizeof(struct ascii_ss);
 while(i<nmax && ascii_ss[i].ss!=ss)i++;
 if(i==nmax)
    return('#');
 return(ascii_ss[i].asc);
}

void led_to_asc(unsigned char* dest,unsigned char* org)
{
unsigned char i,k,c;
 for(i=0,k=0;i<2;i++)
	 {
	 c=org[i];
	 dest[k++]=ss_a_ascii(c);
	 if(c&0x80)
		 dest[k++]='.';
	 else
		 dest[k++]=' ';
	 }
}

void copia_updates_prog_a_reg(char nu)
{
  reg_tcks_tx30->mp_version = blqs.modif_prog[nu].version;
  reg_tcks_tx30->mp_check = blqs.modif_prog[nu].check;
  reg_tcks_tx30->mp_fecha = blqs.modif_prog[nu].fecha;
}

void copia_updates_tar_a_reg(char nu)
{
  memcpy((char*)&reg_tcks_tx30->mp_version, blqs_modifs_tar.modif_tar[nu].num_serie, 2);
  reg_tcks_tx30->mp_check = blqs_modifs_tar.modif_tar[nu].check;
  reg_tcks_tx30->mp_fecha =blqs_modifs_tar.modif_tar[nu].fecha;
}


void copia_parms_tar_a_reg(char nt,char nt_visu)
{
 reg_tcks_tx30->ntar=(unsigned int)nt_visu+1;
 led_to_asc((unsigned char*)&reg_tcks_tx30->ledocup[0],(unsigned char*)&tarifa[nt].ledocup[0]);
 reg_tcks_tx30->bb=tarifa[nt].bb;
 reg_tcks_tx30->m_ps = (unsigned long)(bloc_tar[nt].m_ps*10.+.5);
 reg_tcks_tx30->seg_ps = (unsigned long)(bloc_tar[nt].seg_ps*10.+.5);
 reg_tcks_tx30->impkm_ss = (unsigned long)(bloc_tar[nt].impkm_ss*FKDEC[tarcom.NDEC]+.5);
 reg_tcks_tx30->impho_ss = (unsigned long)(bloc_tar[nt].impho_ss*FKDEC[tarcom.NDEC]+.5);

 led_to_asc((unsigned char*)&reg_tcks_tx30->ledapagar[0],(unsigned char*)&tarifa[tarifa[nt].b_t_pagar].ledocup[0]);
 reg_tcks_tx30->supl=tarifa[nt].supl;
 strcpy((char *)reg_tcks_tx30->texte1, (bloc_tar[tarifa[nt].b_t_ocup].tipt_vf==0) ? "T+D" : "T/D");
} 

extern unsigned char sensors_ant;

//
// retorn:
//        0               sense error
//        ERR_LLUMS       error llums
//        ERR_GEN_POLSOS  error generador polsos distancia

unsigned char fallos_hard(void)
{
unsigned char  retorn;
 retorn=0;
 if(tipoTx == TX50)
 {
   if(IOPORT2_PD_bit.no11 == 1)  
      {
       retorn = ERR_GEN_POLSOS;    // error genimp
      }
 }
 if(!retorn)
    retorn = luminoso_get_error();  // = ERR_LLUMS si hi ha error
 if(retorn)
   {
    //memset(RAM_DISP_FLASH,0xff,SIZE_RAM_DISP);
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
    timer50=0;
    display(1,NF_TEXT6,&tarcom.text_error[retorn][0]);
    APAGAR_DISPLAY(2);
   }
 sensors_ant=0xff;
 return(retorn);
}

unsigned char bloq_fallos_hard(void)
{
unsigned char nf;
 nf = fallos_hard();
 return((nf == ERR_GEN_POLSOS && !tarcom.genimp_ocup) ||(nf && tarcom.OP_BLOQ_ERR));
}

__no_init char error_impre_en_curs_a_display;
__no_init char error_impre_en_curs_a_lumin;

void select_luz(char estat, char* ret)
{
uchar temp;
 temp = menu_estado_TX30[estat];    //.grup_luz;
 if((temp == GL_LIBRE) && 
     (error_impre_en_curs_a_lumin   // !!! E10
      || bloqueo_luz_libre() 
      || tx30.lluminos_apagat  ))  
	 temp=GL_OFF;
 ret[0] = tarcom.luzest[temp].luz[sext_luz];
 ret[1] = tarcom.luzest[temp].luz[(sext_luz) + 4];
 return;
}


/************************************************************/
/* inicialitza struct servei                                */
/************************************************************/

extern   unsigned char  vel_canvi;
extern   char  iniciat_canvivel;

const struct s_clear_x  taula_clears_ini_servcom[]=
{
 sizeof servcom.viaj_activo           ,(char*)&servcom.viaj_activo[0]        ,
 sizeof servcom.n_viajs               ,(char*)&servcom.n_viajs               ,
 sizeof servcom.ja_fet_reset_interserv,(char*)&servcom.ja_fet_reset_interserv,
 sizeof servcom.visu_led_viaj         ,(char*)&servcom.visu_led_viaj         ,
 sizeof servcom.segs_ocupat           ,(char*)&servcom.segs_ocupat           ,
 sizeof servcom.viaj_per_llums        ,(char*)&servcom.viaj_per_llums        ,
 0,
};
 
const struct s_clear_x  taula_clears_ini_servei[]=
{ 
// sizeof servei.import               ,(char*)&servei.import[0]    ,
// sizeof servei.supl                 ,(char*)&servei.supl[0]      ,
 sizeof servei.total_euros          ,(char*)&servei.total_euros  ,
 sizeof servei.estat_euro           ,(char*)&servei.estat_euro   ,
// sizeof servei.supl_desgl           ,(char*)&servei.supl_desgl[0],
// sizeof servei.supl_aut             ,(char*)&servei.supl_aut[0]  ,
// sizeof servei.import_dist_temps    ,(char*)&servei.import_dist_temps[0] ,
// sizeof servei.fet_pcsup            ,&servei.fet_pcsup            ,
// sizeof servei.fet_pcaut            ,&servei.fet_pcaut            ,
// sizeof servei.fer_pcaut            ,&servei.fer_pcaut            ,
// sizeof servei.suma_en_curs         ,&servei.suma_en_curs         ,
// sizeof servei.num_salts            ,(char*)&servei.num_salts    ,
// sizeof servei.tartrab              ,(char*)&servei.tartrab      ,
// sizeof servei.no_es_primer_canvi   ,&servei.no_es_primer_canvi   ,
 sizeof servcom.pet_tick_opc         ,&servcom.pet_tick_opc         ,
// sizeof servcom.envio_copia          ,&servcom.envio_copia          ,
// sizeof servei.hm_oc                ,(char*)&servei.hm_oc                ,
// sizeof servei.hm_oc_tarifa         ,(char*)&servei.hm_oc_tarifa         ,
// sizeof servei.ninot                ,&servei.ninot                ,
 sizeof servei.velmax               ,(char*)&servei.velmax               ,
// sizeof servei.minuts_oc            ,(char*)&servei.minuts_oc            ,
// sizeof servei.minuts_ocup_marcha   ,(char*)&servei.minuts_ocup_marcha   ,
// sizeof servei.minuts_canviaut      ,(char*)&servei.minuts_canviaut      ,
 sizeof iniciat_canvivel            ,(char*)&iniciat_canvivel            ,
// sizeof gt.segs_ocupat              ,&gt.segs_ocupat              ,
// sizeof gt.canvi_minut_ocupat       ,&gt.canvi_minut_ocupat       ,
// sizeof parcial_dist                ,(char*)&parcial_dist[0]             ,
// sizeof parcial_temps               ,(char*)&parcial_temps[0]            ,
// sizeof ntope_dist                  ,(char*)ntope_dist                  ,
// sizeof ntope_temps                 ,(char*)ntope_temps                 ,
// sizeof ntope_imp                   ,(char*)ntope_imp                   ,
 sizeof servei.mask_vd              ,(char*)&servei.mask_vd              ,
 sizeof servei.vd                   ,(char*)&servei.vd[0]                ,
 sizeof gt.mitad_vd                 ,&gt.mitad_vd                 ,
 sizeof servei.es_precio_fijo       ,&servei.es_precio_fijo       ,
 sizeof servei.buf_precio_fijo      ,&servei.buf_precio_fijo[0]   ,
// sizeof servei.num_ticket_incrementat,&servei.num_ticket_incrementat	,
// sizeof servei.corsa_min            ,(char*)&servei.corsa_min	,
// sizeof servei.aplicat_corsa_min    ,&servei.aplicat_corsa_min	,
 sizeof servei.percent_iva          ,(char*)&servei.percent_iva	,
 sizeof servei.import_iva           ,(char*)&servei.import_iva	,
 sizeof servei.esPrecioAcordado		,(char*)&servei.esPrecioAcordado ,
 0,
};


void inicialitzar_servei(struct s_serv* p_serv)
{

 memset((void*)p_serv,0,sizeof(serv[0]));
 tarifador_stop_and_reset(p_serv);
 p_import=&p_serv->import;
 p_supl=&p_serv->supl;
 p_serv->d_t_sep=1;
 vel_canvi=0x01;   // es posa a zona normal
 time_to_fechhor(&p_serv->finicio,&gt.time);
 gt.segs_interm=tarcom.segs_interm;
 clear_xdata(taula_clears_ini_servei);
 servcom.n_viajs++;
}

void inicialitzar_servei_global(void)
{
 clear_xdata(taula_clears_ini_servcom);

 var_tarari=0;
 viaj=0;
 servcom.pk_oc_serv=tarcom.pmpk0;
 servei.velmaxlib=servei.velmax;
 canvi_top=0;
 tec_canvi = 0;

  tx30.insika_needed = 0;
  tx30.insika_received = 0;
  
// 2.14 MASK_GEN &= ~MSK_PAGFS;
 MASK_GEN &= ~MSK_FSLIB;

 
 
 // a mes inicialitza servei 0
 servcom.viaj_activo[0]=1; // la resta del array s'ha posat a 0 
 inicialitzar_servei(&serv[0]);
  
}


void fin_vd(void)
{
 if(servei.mask_vd==0)
   {
    DISPLAY_VD1(0);     /* display 1 */
    APAGAR_DISPLAY(2);
     if(servei.es_precio_fijo)
       {
        display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
        DISP_FIXE(2);
       }  
   }
 servei.mask_vd=2;
 servei.vd[0]=0;
 servei.vd[1]=0;
 return;
}


void flipflop_vd(void)
{
unsigned char  aux;
 switch(servei.mask_vd)
   {
    case 0:
        if((servei.op_tec0 & 0x01)==0)
           break;
        DISPLAY_VD1(0);     /* display 1 */
        DISPLAY_VD2(0);     /* display 2 */
        servei.mask_vd=1;  /* temporalment desactivat  */
        break;
    case 1:
        if((servei.op_tec0 & 0x02)==0)
           break;                  /* no es recuperable */
        aux=servei.vd[gt.mitad_vd];
        DISPLAY_VD1(aux);     /* display 1 */
        DISPLAY_VD2(aux &0x0f);  /* display 2 */
        servei.mask_vd=0;
        break;
    case 2:
        break;
   }
}

/****************************************************/
/* inicialitza interservei                          */
/****************************************************/

const struct s_clear_x  taula_clears_interservei[]=
{ 
 sizeof servei.pasj_ant         ,  (char*)&servei.pasj_ant         ,

 sizeof servei.m_libpas         ,  (char*)&servei.m_libpas         ,
 sizeof servei.minuts_lib       ,  (char*)&servei.minuts_lib       ,
// sizeof servei.minuts_lib_marcha,  (char*)&servei.minuts_lib_marcha,
 sizeof servei.minuts_lib_pasj  ,  (char*)&servei.minuts_lib_pasj  ,

 sizeof servei.hm_libre         ,  (char*)&servei.hm_libre         ,
 sizeof servei.hm_libre_pasj    ,  (char*)&servei.hm_libre_pasj    ,
 sizeof servei.velmax           ,  (char*)&servei.velmax           ,
 sizeof servei.velmaxlib        ,  (char*)&servei.velmaxlib        ,
 sizeof servei.nsent            ,  (char*)&servei.nsent            ,
 sizeof tx30.vel                ,  (char*)&tx30.vel                ,
 0,
};


void reset_interserv(void)
{
 servei.sav_minuts_lib         = servei.minuts_lib       ;
 servei.sav_hm_libre           = servei.hm_libre         ;
 servei.sav_velmaxlib          = servei.velmaxlib        ;
 servei.sav_minuts_lib_pasj    = servei.minuts_lib_pasj  ;
 servei.sav_hm_libre_pasj      = servei.hm_libre_pasj    ;
 servei.sav_nsent              = servei.nsent            ;
 servei.sav_velmax             = servei.velmax           ;
// servei.sav_minuts_lib_marcha  = servei.minuts_lib_marcha;

 clear_xdata(taula_clears_interservei);

 servcom.pk_lib_pasj=tarcom.pmpk0;
// CNT_MARCHA=tarcom.marcha0;
}


void calcul_tots_aux(void)   // calcula totalitzadors auxiliars 
{
   totalitzador_compost_suma(TOT_IMPT, TOT_IMPC, TOT_IMPS);
   totalitzador_compost_suma(TOT_IMPT_P,TOT_IMPC_P, TOT_IMPS_P);
//   totalitzador_compost_resta(TOT_KML, TOT_KMT, TOT_KMO);
//   totalitzador_compost_resta(TOT_KML_P, TOT_KMT_P, TOT_KMO_P);
}   

unsigned long calcul_pcsup(int ns)
{
float  fsupl,faux;
int  nt;
struct s_serv* p_serv;

  p_serv = &serv[viaj];
 fsupl=0;
 for(nt=0;nt<tarcom.ntars;nt++)
   {
    faux=p_serv->per_tarifa[nt].import;
	if(tarcom.pcsup_bb && p_serv->bb_num_tarifa == nt)
		faux += p_serv->bb;
    if(tarcom.pcimpsup == 2)
      {
       faux+=p_serv->per_tarifa[nt].supl;
       if(p_serv->per_tarifa[nt].fet_s_aut)
         faux+=tarifa[nt].supl_aut;
      }
    fsupl+=faux*tarifa[tarcom.pcsup_pt ? nt : ns].pcsup;
   }
 return ((unsigned long)(fsupl+.5));
}

#define TECLA_PARALELO    0
#define TECLA_SECUENCIAL  1


void MACR_SUPL(unsigned char ns,char tipus_tecla)
{
unsigned char  fet;
unsigned long val_compar;
char nmax;
struct s_serv* p_serv;

    if(servei.es_precio_fijo || servei.esPrecioAcordado)
       return;
    
    nmax = tarcom.NUM_supl_max;
    if(nmax >(NMAX_SUPL_DESGL -1))
      nmax = NMAX_SUPL_DESGL -1;  // per evitar es passi de llarg index de supl_desgl
    if((tarcom.tiposupl==SUP_ALL) ||
       (tarcom.tiposupl==SUP_OCUP  && tx30.estat==E_OCUPADO) ||
       (tarcom.tiposupl==SUP_APAG  && tx30.estat==E_PAGAR))
	{
          fet=0;
          p_serv = &serv[viaj];
          val_compar=p_serv->supl;
          if(tarcom.maxsup_excluye_aut)
                  if(val_compar>=p_serv->supl_aut)
                          val_compar-=p_serv->supl_aut;
                  else
                          val_compar=0;
          if( tarifa[ns].supl
            &&((tipus_tecla == TECLA_SECUENCIAL) || (tarifa[p_serv->tarifa].tec_supl & (0x01 << ns)))
            && ((val_compar+tarifa[ns].supl) <= tarcom.maxsup )
            && (p_serv->per_tarifa[ns].n_supl <tarifa[ns].nmaxs)
            && !p_serv->suma_en_curs)
          {
           fet=1;
           last_supl=tarifa[ns].supl;
           ++p_serv->per_tarifa[ns].n_supl;
           p_serv->per_tarifa[ns].supl+=last_supl;
           p_serv->supl+=last_supl;
           if(tipus_tecla == TECLA_SECUENCIAL)
              p_serv->supl_desgl[0]+=last_supl;  // cas secuencial ho acumula tot a primer suplement
           else
              p_serv->supl_desgl[min(nmax,ns)]+=last_supl;
          }

          if( tarifa[ns].pctip==1
            && !p_serv->fet_pcsup
            && !p_serv->suma_en_curs)
          {
           last_supl=calcul_pcsup(ns);
           if(last_supl!=0L)
             {
                  if(tarcom.OP_PERCENT_DESC)
                    {
                     fet|=2;  // cas descompte 
                     serv[viaj].import-=last_supl;
                     if(servei.mask_vd || (servei.vd[gt.mitad_vd]&0xf0)==VD1_IMP)
                            display(1,NF_IMPORT,p_import);
                    }
                  else
                    {
                     fet=1;
                     p_serv->supl+=last_supl;
                     if(tipus_tecla == TECLA_SECUENCIAL)
                        p_serv->supl_desgl[0]+=last_supl;  // cas secuencial ho acumula tot a primer suplement
                     else
                        p_serv->supl_desgl[min(nmax,ns)]+=last_supl;
                    }
                  p_serv->fet_pcsup=1;
             }
          }

          if(fet)
          {
              if(fet & 0x01)
                {
                 if((servei.mask_vd<2)&& (tarcom.supl_solo_fin_serv!=1))
                   fin_vd();
                 display_1_2();
                 //display(2,NF_SUPL,p_supl);
                }
              if((tarcom.tip_paglib==1) && (tx30.estat==E_PAGAR))  //  nomes en A Pagar  v 2.00
                  timer100=tarcom.tipaglib;
				if(p_serv->supl > p_serv->supl_max) 
					p_serv->supl_max = p_serv->supl;
          }
	}
}


long get_import_mes_extres(void)
{
unsigned long laux;
unsigned long val_compar;
struct s_serv* p_serv;

    p_serv = &serv[viaj];
    laux=p_serv->import+p_serv->supl;
    if((((tx30.new_estat == E_PAGAR) && tarcom.corsa_min_aplica_pas_fiserv) || (tx30.estat == E_FISERV)) && !p_serv->aplicat_corsa_min)
    {
      // gestiona cas corsa_minima
      val_compar = p_serv->import;
      if(tarcom.corsa_min_supl)
        val_compar += p_serv->supl;
      if(tarifa[p_serv->tarifa].corsa_min > val_compar)
         {
          laux += (tarifa[p_serv->tarifa].corsa_min - val_compar);
         }
    }
    
    return laux;
}

void display_1_2(void)
{
unsigned long laux;
if(serv[viaj].suma_en_curs)
   {
    laux = get_import_mes_extres();   // te en compta corsa_min 
    display(1,NF_IMPORT,(void *)&laux);
	if(MASK_GEN & MSK_TIPUS_IVA)
	{
		 laux = (long)tarcom.valors_iva[servei.tipus_iva];
		 display(2,NF_MODIF_IVA,(void*)&laux);
		 LEDS_FRONTAL(servcom.save_lf_pag & ~0x0100);  // apaga texte SUPL Euro
		 
	}
	else
      APAGAR_DISPLAY(2);
   }
 else
   {
    display(1,NF_IMPORT,p_import);
    if(servei.es_precio_fijo)
      display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]);
    else
     if((*p_supl)&& (tarcom.supl_solo_fin_serv!=1))
      display(2,NF_SUPL,(void *)p_supl);
    else
      if(servei.mask_vd || (!(servei.vd[gt.mitad_vd]&0x0f)))
         APAGAR_DISPLAY(2);  
   }
}              


void SUMA_OCUP(void)
{
	struct s_serv* p_serv=&serv[viaj];
	struct s_tarifa* p_tar=&tarifa[p_serv->tarifa];
	
	if(p_tar->bb && p_tar->bb_manual){
		if(p_serv->bb_ja_aplicada == 0){		//Encara no hem fet la baixada de bandera. La fem de forma manual
			p_serv->bb_ja_aplicada = 1;
			p_serv->bb = p_tar->bb;
			p_serv->bb_num_tarifa = p_serv->tarifa;
			p_serv->import += p_serv->bb;
			if(p_tar->prim_salt_man){
				tarifador_stop_and_reset(p_serv);
				tarifador_start(p_serv);
			}
		}
	}
	display_1_2();
}


void SUPL_AUT(unsigned char ns)
{
unsigned char  i,fet;
struct s_serv* p_serv;

 fet=0;
 p_serv = &serv[viaj];

     if(tarifa[ns].supl_aut && !p_serv->per_tarifa[ns].fet_s_aut)
       {
        fet=1;
        if(tarifa[ns].tip_s_aut==0)
           {          /* cas aditiu  */
            p_serv->per_tarifa[ns].fet_s_aut=1;
            last_supl=tarifa[ns].supl_aut;
            p_serv->supl_aut+=last_supl;
            p_serv->supl+=last_supl;
            //enviar_parlant(4);
           }
        else
           {           /* cas sustitucio  */
            if(p_serv->supl_aut<tarifa[ns].supl_aut)
               {
                for(i=0;i<tarcom.ntars;i++)
                    p_serv->per_tarifa[i].fet_s_aut=0;
                p_serv->per_tarifa[ns].fet_s_aut=1;
                p_serv->supl-=p_serv->supl_aut;
                last_supl=tarifa[ns].supl_aut;
                p_serv->supl_aut=last_supl;
                p_serv->supl+=last_supl;
                //enviar_parlant(4);
               }
           }
       }
     if(p_serv->fer_pcaut>1 && !p_serv->fet_pcaut)
        {
         last_supl=calcul_pcsup(-1);
         if(last_supl!=0L)
           {
            if(tarcom.OP_PERCENT_DESC)
              {
               fet|=2;  /* cas desconte */
               serv[viaj].import-=last_supl;
               if(servei.mask_vd || (servei.vd[gt.mitad_vd]&0xf0)==VD1_IMP)
                  display(1,NF_IMPORT,p_import);
               //enviar_parlant(3);
              }
            else
              {
               fet=1;
               p_serv->supl+=last_supl;
               p_serv->supl_aut+=last_supl;
               //enviar_parlant(4);
              }
            p_serv->fet_pcaut=1;
            p_serv->fer_pcaut=0;
           }
        }

     if(fet)
        {
         if(fet&0x01)
           {
            if((servei.mask_vd<2)&& (tarcom.supl_solo_fin_serv!=1))
               fin_vd();
            if(tarcom.supl_solo_fin_serv!=1)
               display(2,NF_SUPL,(void *)p_supl);
           }
         if(p_serv->suma_en_curs)
           {
            display(1,NF_IMPORT,p_import);
            p_serv->suma_en_curs=0;
            timer50=0;   /* per acabar tempor si n'hi havia */
            display(2,NF_SUPL,(void *)p_supl);
           }
         if((tarcom.tip_paglib==1) && (tx30.estat==E_PAGAR))    // nomes en A Pagar  v 2.00
             timer100=tarcom.tipaglib;
        }
}

     

__no_init unsigned char  num_cods;
char  test_vls;  // indicador test/visu_last_servei
unsigned char   timerseg; // per contar segons

/*********************************************************************/
/*                           SEMANTIQUES                             */
/*********************************************************************/

void opcions_ticket_servei(struct s_serv* p_serv)
{
int i;
int opc=0;

  if(servei.es_precio_fijo)
  {
      set_opc_tck(OPCT_PRECIO_FIJO);
      // elimina lineas de import (tant si hi ha total
      // com si no.
      // els extres ja no hi son en cas de precio_fijo
      reset_opc_tck(OPCT_LINEAS_CON_TOTAL);
      reset_opc_tck(OPCT_LINEAS_SIN_TOTAL);
  }
  else
  {
      
      for(i=0; i<NMAX_SUPL_DESGL ;i++)
          if(p_serv->supl_desgl[i])
          {
            opc = 1;
            break;
          }
        if(p_serv->supl_aut)
          opc = 1;
        if(opc)
        {
            set_opc_tck(OPCT_LINEAS_CON_TOTAL);
            reset_opc_tck(OPCT_LINEAS_SIN_TOTAL);
        }
        else
        {
            reset_opc_tck(OPCT_LINEAS_CON_TOTAL);
            set_opc_tck(OPCT_LINEAS_SIN_TOTAL);
        }
  }
    
}

void copia_a_reg_servei(struct s_serv* p_serv)
{
int st;
float raux;
  reg_servei->import = p_serv->import;
  
  reg_servei->supl_desgl_0 = p_serv->supl_desgl[0];
  reg_servei->supl_desgl_1 = p_serv->supl_desgl[1];
  reg_servei->supl_desgl_2 = p_serv->supl_desgl[2];
  reg_servei->supl_desgl_3 = p_serv->supl_desgl[3];
  
  reg_servei->supl_autom = p_serv->supl_aut;

  reg_servei->total = p_serv->import +
                      p_serv->supl_desgl[0] +
                      p_serv->supl_desgl[1] +
                      p_serv->supl_desgl[2] +
                      p_serv->supl_desgl[3] +
                      p_serv->supl_aut;

  reg_servei->servei_bb = p_serv->bb;
  reg_servei->d_t_sep = p_serv->d_t_sep;
  reg_servei->import_dist = p_serv->import_dist_temps[0];
  reg_servei->import_temps = p_serv->import_dist_temps[1];
  
  reg_servei->hm_oc = p_serv->hm_oc;
  reg_servei->minuts_ocup = p_serv->minuts_oc;
  
  reg_servei->num_servei = totalitzador_read(TOT_NSER, &st) % 10000;
  
  reg_servei->finicio = p_serv->finicio;
  reg_servei->ffin = p_serv->ffin;

  tartrab_a_buf_right(reg_servei->buf_tartrab, p_serv->tartrab);
  
  // iva
  reg_servei->percent_iva = servei.percent_iva;  
  reg_servei->import_iva = servei.import_iva;
  reg_servei->import_net = reg_servei->total - reg_servei->import_iva;
  
  reg_servei->aplicat_corsa_min = p_serv->aplicat_corsa_min;
  reg_servei->corsa_min = p_serv->corsa_min;
  reg_servei->import_abans_corsa_min = p_serv->import_abans_corsa_min;
  if(p_serv->aplicat_corsa_min)
  {
    reg_servei->import_tarifat = p_serv->import_tarifat;
    reg_servei->total_abans_corsa_min = 
                      p_serv->bb + p_serv->import_tarifat +
                      p_serv->supl_desgl[0] +
                      p_serv->supl_desgl[1] +
                      p_serv->supl_desgl[2] +
                      p_serv->supl_desgl[3] +
                      p_serv->supl_aut;
  }
  else
  {
    reg_servei->import_tarifat = p_serv->import - p_serv->bb;    // afegit a versio 1.04X
    reg_servei->total_abans_corsa_min = reg_servei->total;
  }
  // cas Xile
  reg_servei->salt_distancia = bloc_tar[0].vsal[0];
  reg_servei->salt_temps = bloc_tar[0].vsal[1];
  reg_servei->distancia_tarifada = p_serv->num_salts_dist_temps[0] * METRES_SALT_XILE;
  reg_servei->temps_tarifat = p_serv->num_salts_dist_temps[1] * MINUTS_SALT_XILE;
  raux=(float)tarcom.k/(1000.*(float)tarcom.DIVK);  
  reg_servei->acum_dist_control = (long)((float)p_serv->acum_dist_control/raux);
  reg_servei->acum_temps_control = (p_serv->acum_temps_control/600)*100 + (p_serv->acum_temps_control/10)%60;
  reg_servei->es_control = servcom.es_control;    
}

void copia_a_reg_servei_num_ticket(void)
{
	int st;
	reg_servei->num_tick = totalitzador_read(TOT_NTICK, &st) % 10000;
	if (reg[REG_SERVEI].def_reg->n_camps_reg > CAM_numTick8dig)
		reg_servei->numTick8dig = totalitzador_read(TOT_NTICK, &st) % 1000000;
	if(reg[REG_SERVEI].def_reg->n_camps_reg > CAM_numServ8Dig)
		reg_servei->numServ8Dig = totalitzador_read(TOT_NSER, &st) % 1000000;
}

int inicioFin=0;
int coordenadasViaj=0;

void askCoordenadas(int inicio){
	if(tarcom.hi_ha_td30 == 3 || tarcom.hi_ha_td30 == 4 || tarcom.hi_ha_td30 == 5){ //TD30tip_blue, TD30tip_bt40, TD30tip_bt40_L 
		inicioFin=inicio;
		coordenadasViaj=viaj;
		multiplex_enviar(multiplex.msg_txm.device, PER_TRANSFER, "m", 1);
	}
}

void recPosicion(char * buf, int nb){
	unsigned long l;
	if(nb<8)
		return;
	l=*(unsigned long*)buf;
	serv[coordenadasViaj].latitud[inicioFin]=(l & 0x3fffffff);
	serv[coordenadasViaj].ns[inicioFin]=l>0x7fffffff ? 'S' : 'N';
	buf+=4;
	l=*(unsigned long*)buf;
	serv[coordenadasViaj].longitud[inicioFin]=(l & 0x3fffffff);
	serv[coordenadasViaj].eo[inicioFin]=l>0x7fffffff ? 'O' : 'E';
}

void demanaPermisActualizar(char tipus){
	char buf[2];
	buf[0]='u';
	if(multiplex.buf_rep[3]==ARXIU_TARIFA)
		buf[1] = tarcom.tarifa_remota == 1 ? 'O' : 'E';
	else if(multiplex.buf_rep[3]==PROG_TX40)
		buf[1] = tarcom.progr_TX_remoto ==1 ?  'O' : 'E';
	multiplex_enviar(multiplex.msg_txm.device, PER_TRANSFER, buf, 2);	
}

void imprimir_ticket_servei(int ret_event)
{
  int fin,i,j;
    if(tx30.insika_needed)
       reset_opc_tck(OPCT_NOT_INSIKA);
    else
       set_opc_tck(OPCT_NOT_INSIKA);
    
    if(ret_event)
       MASK_GEN |= MSK_IMPRE;
    if(servcom.num_ticket_incrementat == 0)
    {
      // es primera vegada s'imprimeix ticket
      totalitzador_incr(TOT_NTICK);
      copia_a_reg_servei_num_ticket();
      servcom.num_ticket_incrementat = 1;
      if(tarcom.hi_ha_torn_prima_intern && (prima.reg_servei_en_curs >=0))
      {
        reg_prima_servei->servei_fet_ticket = 1;  // actualitza indicador ticket impres
        prima_actualitzar_servei(prima.reg_servei_en_curs);
      }
    }
	copia_textes_a_reg();
    inici_ticket(ret_event);
    imprimir(TICKET_SERV1);
    imprimir(TICKET_SERV2);
    if(ticket[TICKET_PER_TARIFA]->n_camps_ticket > 1)
    {
      for(i=0;i<32;i++)
      {
       for(j=0,fin = 0;j<i;j++)
         if(memcmp(tarifa[i].impre_tartrab,tarifa[j].impre_tartrab,2)==0)
         {
           fin = 1; // led tarifa coincideix amb alguna anterior
           break;
         }
       if(fin)
         continue;  // cas aquesta tarifa ja s'ha acumulat amb alguna anterior
       memcpy(reg_servei->per_tar_nom,tarifa[i].impre_tartrab,2);
       reg_servei->per_tar_import = serv[viaj].per_tarifa[i].import;
       reg_servei->per_tar_hm_oc = serv[viaj].per_tarifa[i].hm_oc;
	   reg_servei->per_tar_temps = serv[viaj].per_tarifa[i].parcial_temps;
	   reg_tcks_tx30->impkm_ss = (unsigned long)(bloc_tar[tarifa[i].b_t_ocup].impkm_ss*FKDEC[tarcom.NDEC]+.5);
	   reg_tcks_tx30->impho_ss = (unsigned long)(bloc_tar[tarifa[i].b_t_ocup].impho_ss*FKDEC[tarcom.NDEC]+.5);
       for(j=i+1;j<32;j++)// mira si alguna tarifa mes te el mateix led
       {
         if(memcmp(tarifa[i].impre_tartrab,tarifa[j].impre_tartrab,2)==0)
         {
           // acumula 
         reg_servei->per_tar_import += serv[viaj].per_tarifa[j].import;
         reg_servei->per_tar_hm_oc += serv[viaj].per_tarifa[j].hm_oc;
         }
       }
       if((reg_servei->per_tar_import!= 0) || (reg_servei->per_tar_hm_oc!= 0))
          imprimir(TICKET_PER_TARIFA);
      }
    }
	reg_servei->latitud_inicio=serv[viaj].latitud[0];
	reg_servei->latitud_final=serv[viaj].latitud[1];
	reg_servei->longitud_inicio=serv[viaj].longitud[0];
	reg_servei->longitud_final=serv[viaj].longitud[1];
	reg_servei->norte_sur_inicio=serv[viaj].ns[0];
	reg_servei->norte_sur_final=serv[viaj].ns[1];
	reg_servei->este_oeste_inicio=serv[viaj].eo[0];
	reg_servei->este_oeste_final=serv[viaj].eo[1];
    imprimir(TICKET_SERV3);
	reset_opc_tck(OPCT_COPIA_SER1);
	if(get_opc_tck(OPCT_COPIA_SER2))
		set_opc_tck(OPCT_COPIA_SER3);
	set_opc_tck(OPCT_COPIA_SER2);
    
    if(tx30.insika_received)
        imprimir_insika();

    final_ticket(); 
}


void imprimir_ticket_avance_papel(void)
{
    inici_ticket(0);
    imprimir(TICKET_AVANCE_PAPEL);
    final_ticket(); 
}

void imprimir_ticket_tots(void)
{
    inici_ticket(0);
    imprimir(TICKET_TCK_TOTS);
    final_ticket(); 
}

void imprimir_ticket_events_torn(void)
{
short laux;
int i,j,nt,fin;

  if(tarcom.torn_continu == 0)
  {
    imprimir_ticket_avance_papel();
  }
  else
  {
    // cas fran�a
    
    get_reloj_fechhor(RELLOTGE_PLACA,&reg_tcks_tx30->fecha_actual);
    inici_ticket(0);
    imprimir(TICKET_EVENTS_TORN_INI);

    for(j=0;j<NUM_MAX_TORNS;j++)
    {
       // torn a visualitzar.(Primer el mes antic)
       nt = (bloqueo_torn.index_registre_torn + 1 + j) % NUM_MAX_TORNS;
       if(tarcom.visu_dia_torn == 1)
         laux = (short)bloqueo_torn.registre_torn[nt].anydia_torn;// dia torn en dia de l'any
       else // tarcom.visu_dia_torn == 2
         laux = (short)bloqueo_torn.registre_torn[nt].diames_torn;   // dia torn en format mes/dia
       reg_tcks_tx30->num_dia_torn = laux;
       minuts200_to_hhmm(bloqueo_torn.registre_torn[nt].hora_inici_torn, reg_tcks_tx30->hora_inici_torn);
       if((nt == bloqueo_torn.index_registre_torn) && (bloqueo_torn.estat_torn == 2))
         reg_tcks_tx30->fi_torn_guions = 1;
       else
       {
         reg_tcks_tx30->fi_torn_guions = 0;
         minuts200_to_hhmm(bloqueo_torn.registre_torn[nt].hora_final_torn, reg_tcks_tx30->hora_final_torn);
       }
       imprimir(TICKET_EVENTS_TORN_TORN);
             
        
        for(i=0,fin = 0;!fin && (i<NUM_MAX_DESCANS_REGISTRAR)
                  && ((tarcom.num_descans_torn == 0) || (i < tarcom.num_descans_torn))
               ;i++)
        {
          if(bloqueo_torn.registre_torn[nt].registre_descans[i].hora_inici_descans == 0)
          {
            fin = 1;
            reg_tcks_tx30->inici_descans_guions = 1;
          }
          else
          {
            reg_tcks_tx30->inici_descans_guions = 0;
            minuts200_to_hhmm(bloqueo_torn.registre_torn[nt].registre_descans[i].hora_inici_descans, reg_tcks_tx30->hora_inici_descans);
          }
          if(bloqueo_torn.registre_torn[nt].registre_descans[i].hora_final_descans == 0)
          {
            fin = 1;
            reg_tcks_tx30->final_descans_guions = 1;
          }
          else
          {
            reg_tcks_tx30->final_descans_guions = 0;
            minuts200_to_hhmm(bloqueo_torn.registre_torn[nt].registre_descans[i].hora_final_descans, reg_tcks_tx30->hora_final_descans);
          }
          imprimir(TICKET_EVENTS_TORN_DESC);
        }
    }
    imprimir(TICKET_EVENTS_TORN_FIN);
    final_ticket(); 
  }
}

void imprimir_ticket_params(void)
{
char  i,j;
unsigned long msk;
    inici_ticket(0);
    imprimir(TICKET_TCK_PAR_TX30);

    for(i=0;i<blqs.nmodifs_prog;i++)
    {
     copia_updates_prog_a_reg(i); 
     imprimir(TICKET_PAR_TX30_UPD);
    }
    
     // passa a memoria dades canvis tarifa a e2prom 
     leer_verif_blqs_modifs_tar();
	if(ticket[TICKET_PAR_TX30_UPD_TAR]->n_camps_ticket > 1){
		for(i=0;i<blqs_modifs_tar.nmodifs_tar;i++)
		{
		 copia_updates_tar_a_reg(i); 
		 imprimir(TICKET_PAR_TX30_UPD_TAR);
		}
	}

    for(i=0,j=0,msk=0x01;i<tarcom.ntars;i++,msk<<=1)
           {
           if(tarcom.tars_blq3&msk)
                   {
                   copia_parms_tar_a_reg(i,j);
                   j++;
                   imprimir(TICKET_TCK_PAR_TX30_CONT);
                   }
           }
    imprimir(TICKET_TCK_PAR_TX30_FIN);
    final_ticket(); 
}

void imprimir_ticket_torn(void)
{
  int i,j;
T_FECHHOR sav_fin_prev;

 if(hay_impresora())
 {
    inici_ticket(0);
    imprimir(TICKET_PRIMA_TURNO);
    if(reg_prima_torn->num_serveis)
    {
      for(i=0,j=reg_prima_torn->nreg_servei_ini;
          (i<reg_prima_torn->num_serveis) &&(i<prima.max_regs_servei);
          i++,j=prima_get_index_next_serv(j))
      {
        prima_llegir_servei(j);
        if(i==0)
            reg_prima_servei->servei_ini_lliure = reg_prima_torn->fecha_ini;
        else
            reg_prima_servei->servei_ini_lliure = sav_fin_prev;
        sav_fin_prev = reg_prima_servei->servei_fi;
        imprimir(TICKET_PRIMA_SERVEI_N);
      }
    }
    // posa hora inici tram fins final torn
    if(reg_prima_torn->num_serveis)
      reg_prima_torn->hhmm_ini_tram_fi_torn = reg_prima_servei->servei_fi.hour*60L+reg_prima_servei->servei_fi.minute;
    else  
      reg_prima_torn->hhmm_ini_tram_fi_torn = reg_prima_torn->fecha_ini.hour*60L+reg_prima_torn->fecha_ini.minute;
    imprimir(TICKET_PRIMA_TURNO_PEU);
    final_ticket();
 }
}

void paso_a_libre(int desde_off);

void paso_a_libreXseguent_viaj(char fer_reset_interserv);

void aplicar_corsa_minima_si_toca(int es_pas_a_fi_serv, struct s_serv* p_serv)
{
unsigned long val_compar;

  if(p_serv->aplicat_corsa_min)
    return;
  if(tarcom.corsa_min_aplica_pas_fiserv &&(!es_pas_a_fi_serv))
    return;
  p_serv->import_abans_corsa_min = p_serv->import;
  p_serv->import_tarifat = p_serv->import - p_serv->bb;
  
  val_compar = p_serv->import;
  if(tarcom.corsa_min_supl)
    val_compar += p_serv->supl;
 if(tarifa[p_serv->tarifa].corsa_min > val_compar)
   {
    p_serv->aplicat_corsa_min = 1;
    p_serv->import += (tarifa[p_serv->tarifa].corsa_min - val_compar);
    p_serv->corsa_min = tarifa[p_serv->tarifa].corsa_min;
    display(1,NF_IMPORT,p_import);    
   }
}

//
// tanca servei i acumula a tots
//
char tanca_i_acumula_servei(struct s_serv* p_serv)
{
unsigned char  anul10seg;
unsigned long total;

  anul10seg = tarcom.OP_ANUL10SEG &&(p_serv->crono < 10);
  time_to_fechhor(&p_serv->ffin,&gt.time);        
  tarifador_stop(p_serv);        
  aplicar_corsa_minima_si_toca(1, p_serv);   // (1) es pas a fi servei
  
  if(!anul10seg) // cas anulacio 10seg. no fa res
    {
       if(tarcom.supl_solo_fin_serv==1) // si es !=1 ja es fa dins del servei
                 SUPL_AUT(p_serv->tarifa);
       if(tarcom.OP_ANUL_100M && p_serv->hm_oc==0L)
         {
          p_serv->import=0L;
          p_serv->supl=0L;
          memset(&p_serv->supl_desgl[0],0,sizeof p_serv->supl_desgl);
          p_serv->supl_aut = 0L;
         }
       totalitzador_add(TOT_BB, p_serv->bb);	// versio 3.05
       totalitzador_add(TOT_BB_P, p_serv->bb);  // versio 3.05
       totalitzador_incr(TOT_NSER);
       totalitzador_incr(TOT_NSER_P);
       totalitzador_add(TOT_IMPC, p_serv->import); 
       totalitzador_add(TOT_IMPC_P, p_serv->import);
       totalitzador_add(TOT_IMPS, p_serv->supl);
       totalitzador_add(TOT_IMPS_P, p_serv->supl);
       servei.total_euros=(ulong)((((float)p_serv->import)+(float)p_serv->supl)*tarcom.factor_alt+.5);

       servei.percent_iva = tarcom.valors_iva[servei.tipus_iva];
       total = p_serv->import +p_serv->supl_desgl[0] +p_serv->supl_desgl[1] +p_serv->supl_desgl[2] +p_serv->supl_desgl[3] +p_serv->supl_aut;
       servei.import_iva = (long)(((float)total * servei.percent_iva)/(100. + servei.percent_iva) + 0.5);

       copia_a_reg_servei(p_serv);
       opcions_ticket_servei(p_serv);
       if(tarcom.hi_ha_torn_prima_intern)
       {
           // prepara dades registre servei prima
           prima_get_dades_servei();           
       }
	   askCoordenadas(1);
    }
  return(anul10seg);
}

//
// finalitza servei
// retorn:
//         0  normal
//         1 s'ha anulat servei per menys de 10 segons

char fin_servicio(char abort) {
	
	char buf_luz[2];
	struct s_serv* p_serv;
	unsigned char  anul10seg;
	
	zapper_desactivar();
	servcom.num_ticket_incrementat = 0;
	set_opc_tck(OPCT_COPIA_SER1);
	reset_opc_tck(OPCT_COPIA_SER2); // reset opcio ticket es copia
	reset_opc_tck(OPCT_COPIA_SER3); // reset opcio ticket es copia
	
	p_serv = &serv[viaj];
	
	while (1) { // loop per fi servei de tots els viatgers cas abort i multiviatger  
	
		anul10seg = tanca_i_acumula_servei(p_serv);
		
		if (abort && (tarcom.OP_MULTIT && (servcom.n_viajs > 1))) {
			// busca seguent viatger actiu
			--servcom.n_viajs;        
			servcom.viaj_activo[viaj] = 0;
			viaj = 0;
			while (servcom.viaj_activo[viaj] == 0) 
				viaj++;
			p_serv = &serv[viaj];
			// i repeteix loop anulacio
		} else
			break;
	}   // final loop per fi servei de tots els viatgers cas abort i multiviatger 
	
	if (servei.mask_vd < 2)
		fin_vd();
	
	//
	// cas abort (per fallo tensio o error GENIMP) ja ha anulat tots els posibles serveis) 
	//
	if (abort) {
		reset_interserv();
		paso_a_libre(0);
		return 0;
	}
	if (anul10seg) {        
		paso_a_libreXseguent_viaj(0);   // no fa reset_interserv !!!!
		return 1;
	}
	if ((!hay_impresora() ||
		(!tarcom.ticket_obligatori &&
		 !(tarcom.ticket_voluntari == 0) &&       // afegit ticket automatic
			 !(tarcom.ticket_voluntari && servcom.pet_tick_opc))) &&
	   (!(tarcom.hi_ha_td30 && (MASK_1& MSK_PASOCFS))|| hay_cargador())   &&
		   !(MASK_GEN & MSK_FSLIB))
	{
		paso_a_libreXseguent_viaj(1);    //  i reset_interserv();
	} else {
		if (hay_impresora()) {
			if (tarcom.ticket_obligatori) {
				imprimir_ticket_servei(1);  // amb bloqueig i retorn event
				tx30.subestat = 0;
			} else if (tarcom.Tiq_obl_imp && reg_servei->total > tarcom.Tiq_obl_imp) {
				imprimir_ticket_servei(0);  // amb bloqueig i retorn event
				serv[viaj].envio_copia = 1;
				tx30.subestat = 2;
			} else {
				// cas bloqueig desde terminal td30/ticket opcional
				if (servcom.pet_tick_opc ||(tarcom.ticket_voluntari == 0)) { // afegit ticket automatic
					imprimir_ticket_servei(0);  // sense retorn
				}
				DISP_FIXE(DISP_IMPORT);
				DISP_FIXE(DISP_EXTRES);
				tx30.subestat = 2;
			}
		}
		tx30.new_estat = E_FISERV;
		
		if (tarcom.OP_MULTIT && (servcom.n_viajs > 1))
			;
		else {
			reset_interserv();
			servcom.ja_fet_reset_interserv = 1;
		}
		
		display(0, NF_LEDEST, (void *)&tarcom.vars_estat[E_FISERV].ledest);
		if (!(hay_impresora() && tarcom.ticket_obligatori))
			DISP_FLASH(0);   /* cas  bloqueig o ticket opcional */
		servcom.save_lf_pag = tarifa[p_serv->tarifa].lf_pag;
		LEDS_FRONTAL(servcom.save_lf_pag);
		LEDS_FRONTAL_FIXE();
		select_luz(E_FISERV,buf_luz);
		out_luzex(buf_luz[0], buf_luz[1]);
		servei.hm_fins_fi_torn = 0;
	}
	
	if (!fallo_tension()) {
		if (contar_salts_inst) {
			leer_eprom_tabla(GE_BLQS_nsalts);
			if (blqs.nsalts < 300) {
				blqs.nsalts += p_serv->num_salts;
				blqs.chk_blqs = calcul_chk_blqs();
				grabar_eprom_tabla(GE_BLQS_nsalts);
				if (blqs.nsalts >= 300)	{
					leer_eprom_tabla(GE_BLQS);
					memcpy(blqs.f_inst, (unsigned char *)&gt.time.day, 3);
					memcpy(blqs.CCC_inst, blqs.CCC, 2);
					contar_salts_inst = 0;
					
					blqs.chk_blqs = calcul_chk_blqs();
					grabar_eprom_tabla(GE_BLQS);
				}
			}
		}
	}
	return(0);
}

/*
void FS_LIB_TEMPS(void)
{
    if(tarcom.tip_paglib==2)
       {
        timer50=0;
        timer100=0;
        MASK_GEN &= ~MSK_FSLIB;
        if((!hay_impresora() ||
           (!tarcom.ticket_obligatori &&
            !(tarcom.ticket_voluntari == 0) &&       // afegit ticket automatic
            !(tarcom.ticket_voluntari && servcom.pet_tick_opc)&&
             (serv[viaj].envio_copia == 0)))&&
//        !(tarcom.hi_ha_td30 && (MASK_1& MSK_PASOCFS)))
        !tarcom.hi_ha_td30 )
        {
          paso_a_libreXseguent_viaj(1);   // i reset_interserv
        }

       }
}
*/
void FS_LIB_TEMPS(void)
{
char bloqueig_per_td30;

    if(tarcom.tip_paglib==2)
       {
        timer50=0;
        timer100=0;
        MASK_GEN &= ~MSK_FSLIB;
        bloqueig_per_td30 = 0;
        if(tarcom.hi_ha_td30 == 1)  // cas td30
          bloqueig_per_td30 = 1;
        else if(tarcom.hi_ha_td30 && (MASK_1& MSK_PASOCFS))
          bloqueig_per_td30 = 1;
        if((!hay_impresora() ||
           (!tarcom.ticket_obligatori &&
            !(tarcom.ticket_voluntari == 0) &&       // afegit ticket automatic
            !(tarcom.ticket_voluntari && servcom.pet_tick_opc)&&
             (serv[viaj].envio_copia == 0)))&&
//        !(tarcom.hi_ha_td30 && (MASK_1& MSK_PASOCFS)))
        !bloqueig_per_td30 &&
          !tx30.insika_needed)
        {
			paso_a_libreXseguent_viaj(1);   // i reset_interserv
        }
       }
}
 
void FS_FI_IMPRE(void)  // fi impressio en fi servei 
{
    MASK_GEN &= ~MSK_IMPRE;

  
 if(event_tr == TR_OK )
   {
    tx30.subestat=2;
    DISP_FLASH(0);
    DISP_FIXE(1);
    DISP_FIXE(2);
   }
 else
   {
    tx30.subestat=1;
    DISP_FIXE(0);
    DISP_FLASH(1);
    DISP_FLASH(2);
   }
}

void FS_LIBRE(void)
{
 switch(tx30.subestat)
   {
    case 0:
        paso_a_libreXseguent_viaj(1);   // i reset_interserv
        break;
    case 1:
//        tx30.subestat=0;
        if(hay_impresora())
          imprimir_ticket_servei(1);  // amb bloqueig i retorn event
        break;
    case 2:
//        tx30.estat_pet=EPET_LIBRE;
           if((!(tarcom.hi_ha_td30 && (MASK_1& MSK_PASOCFS))) || hay_cargador())  // v 3.05
             {
              paso_a_libreXseguent_viaj(1);   // i reset_interserv
             }
        break;
   }
} 
			  
void FS_0(void)
{
char buf_luz[2];
int ii;
unsigned char tar;
struct s_serv* p_serv;

     MASK_GEN &= ~MSK_FSLIB;
     if(exces_fallo_t)
       {
         // passa a fi servei resta de passatgers cas multitaximetre    
         if(tarcom.OP_MULTIT )
         {
           while(servcom.n_viajs>1)
           {
             // busca seguent viatger actiu
             --servcom.n_viajs;        
             servcom.viaj_activo[viaj]=0;
             viaj=0;
             while(servcom.viaj_activo[viaj]==0)viaj++;
             p_serv = &serv[viaj];
             // i repeteix loop anulacio
             tanca_i_acumula_servei(p_serv);
           }
         }
         
          reset_interserv();
          paso_a_libre(0);
          return;
       }
     // rectiva resta de viatgers 
     if(tarcom.OP_MULTIT &&(servcom.n_viajs>1))
        {
          for(ii=0; ii<MAX_PSGRS ; ii++)
          {
           if(servcom.viaj_activo[ii] && (ii != viaj))
           {
             tar=tarifa_canvi_autom(serv[ii].tarifa,AUT_ALL, &serv[ii]);
             cambio_tarifa(tar,E_OCUPADO, &serv[ii]);
             tarifador_start(&serv[ii]);   // td-tx  TARIFANDO=1;
           }
          }
        }
     
     display(0,NF_LEDEST,(void *)&tarcom.vars_estat[E_FISERV].ledest);
     select_luz(E_LIBRE,buf_luz);
     out_luzex(buf_luz[0],buf_luz[1]);
     switch(tx30.subestat)
       {
            case 0:
            case 1:
              if(!(hay_impresora() && tarcom.ticket_obligatori))
                     tx30.subestat=2;
              else
                     tx30.subestat=1;
              break;

            default:
              break;
       }
     switch(tx30.subestat)
       {
            case 0:
            case 1:
               DISP_FIXE(0);
               DISP_FLASH(1);
               DISP_FLASH(2);
               break;
            case 2:
               DISP_FLASH(0);
               DISP_FIXE(1);
               DISP_FIXE(2);
               break;
       }
}

void FS_PLUS(void)
{
 switch(tx30.subestat)
   {
    case 2:
      if(hay_impresora() && tarcom.ticket_voluntari)
       {
        imprimir_ticket_servei(0);  // sense retorn
        serv[viaj].envio_copia=1;
       }
        break;
    default:
        break;
   }
}

void FS_DIST(void)
{
  paso_a_libreXseguent_viaj(1);   // i reset_interserv
}




void display_1_reloj_en_libre(uchar op)
{
uchar buf[2];     
 if(op)
   {
    if(op==2)
      servei.mitad_reloj_en_libre=0;
    else
      servei.mitad_reloj_en_libre=!servei.mitad_reloj_en_libre; 
    if(servei.mitad_reloj_en_libre==0)
       timer100=40;  // intermitencia cada 4"
    else
       timer100=10;  // intermitencia cada 1"
   }
 if(servei.mitad_reloj_en_libre==0)
    display(1,NF_DIA,(unsigned char *)&gt.time.day);
 else
   {
    buf[0]=gt.time.year;
    if(gt.time.year>0x89)
      buf[1]=0x19;
    else
      buf[1]=0x20;
    display(1,NF_AAAA,buf);
   }
}


void display_torn_en_reloj(void)
{
	unsigned char minut_hora[2];
	long laux;
	
	LEDS_FRONTAL(LED_HHMM |tarcom.vars_estat[E_LIBRE].lf); // texte frontal TEMPS
	
	//Led de estat = CH
	display(0,NF_LEDEST,"\x39\x76");
	
	//Display de import
	if(shiftControlDeactivated()){
		display(1,NF_TEXT6,"\x00\x00\x40\x40\x40\x40");
	} else {
		if((tarcom.preavis_fi_torn && (bloqueo_torn.minuts_queden <= tarcom.preavis_fi_torn)))
			DISP_FLASH(1);
		else
			DISP_FIXE(1);
		if(tarcom.visu_torn_en_lliure == VISU_TORN_TEMPS_QUEDA)
			display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,(unsigned char *)&bloqueo_torn.minuts_queden);
		else {
			suma_minuts_a_hora_minut(bloqueo_torn.minuts_queden,&gt.time,minut_hora);
			display(1,NF_HORA,(unsigned char *)minut_hora);
		}
	}

	//Display de extras
	if(tarcom.visu_dia_torn == 1) {
		laux = (long)(bloqueo_torn.anydia_last_torn & 0x1ff);
		display(2,NF_TOT4,(void*)&laux);   // dia torn en format dia de l'any
	} else if(tarcom.visu_dia_torn == 2) {
		laux = (long)bloqueo_torn.diames_last_torn;
		display(2,NF_TOT4,(void*)&laux);   // dia torn en format mes/dia
	}
}

const char nivell_cobertura[3][4] =
{ 0x08,0x08,0x08,0x08,
  0x48,0x48,0x48,0x48,
  0x49,0x49,0x49,0x49,
};

void display_hora_en_reloj(void)
{
struct s_time  time;
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
    //display(0,NF_LEDEST,(void *)&tarcom.lede_rel[tx30.subestat]);
    display(0,NF_LEDEST,(void *)&tarcom.lede_rel[0]);   // v2.16B
    DISP_DIG_FLASH(1,2);
    get_reloj(RELLOTGE_PLACA,&time);
    display(1,NF_HORA,(unsigned char *)&time.minute);
    timer50=10;    /* inicia temporitzat 1/2"  */
    if(tx30.cobertura_rebuda)
    {
      // cas ha de treure cobertura per display extres
      display(2, NF_TEXT4, (unsigned char *)nivell_cobertura[0]);
      tx30.index_nivell = 1;
    }
    else if(tarcom.visvel_imp_extr!=2)
       APAGAR_DISPLAY(2);
}
 
// es crida desde transicio V_TEMPOR_1  , E_LIBRE  , T_reloj_en_libre 
void reloj_en_libre(void)
{
 display_1_reloj_en_libre(1);  // per canviar any/mes-dia
}

void reset_configs_desde_bg40(void)
{
    configs_desde_bg40.hi_ha_torn_prima_bg40 = 0;
	configs_desde_bg40.hi_ha_password_cond = 0;
    configs_desde_bg40.hi_ha_pausa_activa = 0;
    configs_desde_bg40.select_iva_teclat = 0;
    configs_desde_bg40.segons_pausa_passiva = 0;
	pausa.estat = EPAUSA_temporitza_inici_pausa_passiva;
	pausa.segons = 0;
}

const unsigned char TEXT_ERROR_8[6]=
{
 0,0,0xf9,0x40,0x7f,0,    //  E.-8   (7)  bloqueo check_sum tarifa 
};


void paso_MEM_MAL(void)
{
	viaj=0;
	memset((char*)&tarcom, 0, sizeof(tarcom));
    tarcom.llums_combi = LLUMS_COMBI_NORMAL;
    tarcom.haybloc=0x05;
    tarcom.DIVK = 1;
	reset_configs_desde_bg40();
    COLA_MF = 0;                 
    APAGAR_DISPLAY(0);
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
	memset((char*)&servei, 0, sizeof(servei));
	memset((char*)&serv, 0, sizeof(serv));
	memset((char*)&servcom, 0, sizeof(servcom));
	memset((char*)&tx30, 0, sizeof(tx30));
    display(1,NF_TEXT6,(void*)&TEXT_ERROR_8);
    APAGAR_DISPLAY(2);
    tx30.new_estat=E_MEM_MAL;
    tx30.estat=E_MEM_MAL;
}

void prima_tancar_torn(void);
void prima_obrir_torn(void);

const char TEXT_PRE_PAUSA_ACTIVA[6]={0x73,0x50,0x79,0x40,0x73,0x6d};  // PrE-PS
const char TEXT_PAUSA_ACTIVA[6]={0xf7,0x73,0x77,0x3e,0x6d,0x79};      // A.PAUSE
const char TEXT_PAUSA_PASIVA[6]={0xf3,0x73,0x77,0x3e,0x6d,0x79};	  // P.PAUSE
const char TEXT_PRE_PAUSA_PASIVA[6]={0x73,0xd0,0x7c,0x1c,0x78,0x78};  // Pr.butt


void display_1_en_libre(int err)
{
	long laux;
  if(tarcom.bloqueo_torn)
   {
      if(!bloqueo_torn.minuts_queden)
      {
        display(1,NF_TEXT6_FLASH_NOMOD,&tarcom.text_error[ERR_TORN][0]);
        return;
      }
      if(bloqueo_torn.minuts_queden<11)
      {
        display(1,NF_TEXT6_FLASH_NOMOD,&tarcom.text_error[ERR_AVIS_TORN][0]);
        return;
      }
   }
 
  if(err)
	  return;

// gestio cas pausa (activa o passiva)  
  switch(pausa.estat)
  {
  case EPAUSA_temporitza_inici_pausa_passiva:
	  // no hi ha pausa per display
	  break;
  case EPAUSA_temporitza_pre_pausa_activa:
	  display(1,NF_TEXT6,(void*)&TEXT_PRE_PAUSA_ACTIVA[0]);
	  APAGAR_DISPLAY(2);
	  return;
  case EPAUSA_pausa_activa:
	  display(1,NF_TEXT6,(void*)&TEXT_PAUSA_ACTIVA[0]);
	  laux = pausa.segons/60;
	  display(2,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
	  return;
  case EPAUSA_temporitza_pre_pausa_passiva:
	  display(1,NF_TEXT6,(void*)&TEXT_PRE_PAUSA_PASIVA[0]);
	  APAGAR_DISPLAY(2);
	  return;
  case EPAUSA_pausa_passiva:
	  display(1,NF_TEXT6,(void*)&TEXT_PAUSA_PASIVA[0]);
	  laux = pausa.segons/60;
	  display(2,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
	  return;
  }
  
    if(tarcom.visvel_imp_extr==1)
        DISPLAY_VD1(VD1_VEL); 
    else if(tarcom.OP_RELOJ_LIBRE )
        {
         display_1_reloj_en_libre(2);  // per iniciar secuencia
        }
    else if(!hay_precio_acordado)
        display(1,NF_TEXT6_FLASH_NOMOD,&tarcom.text_lib[0]);
}        
 

void PASO_LIBRE(void)
{
  paso_a_libre(0);
}

//
// crida desde fi servei
// cas multiviatger torna a ocupat si encara queden viatgers actius.
//
void paso_a_libreXseguent_viaj(char fer_reset_interserv)
{
struct s_serv* p_serv;
struct s_tarifa * pt;

 if(tarcom.OP_MULTIT &&(servcom.n_viajs>1))
    {
     --servcom.n_viajs;
     servcom.viaj_activo[viaj]=0;
     viaj=0;
     while(servcom.viaj_activo[viaj]==0)viaj++;

     p_serv = &serv[viaj];
     p_import= &p_serv->import;
     p_supl=&p_serv->supl;
     tx30.estat=E_OCUPADO;
     tx30.refresc_leds_ho_km = 1;    // per actualitzar
     
     // les tres linies seguents no calen ?
//     tar=tarifa_canvi_autom(serv[viaj].tarifa,AUT_ALL, p_serv);
//     cambio_tarifa(tar,E_OCUPADO, p_serv);
//     tarifador_start(p_serv);   // td-tx  TARIFANDO=1;
     
     zapper_activar();    

     pt = &tarifa[p_serv->tarifa];
     out_luzex(pt->luzocup[sext_luz],pt->luzocup[sext_luz+4]);
     LEDS_FRONTAL(pt->lf_oc);
     ledocpag[0] = (pt->ledocup[0] & 0x00ff)|((short)SS[viaj+1]<<8);
     
     display(0,NF_LEDEST,(void *)ledocpag);
     display(1,NF_IMPORT,p_import);
     if((p_serv->supl)&& (tarcom.supl_solo_fin_serv!=1))
        display(2,NF_SUPL,(void *)p_supl);
     else
        APAGAR_DISPLAY(2);
     tx30.new_estat=E_OCUPADO;
     if(servcom.n_viajs>1)
          timer100=tarcom.tiviaj;  /* per anar canviant de viatger */
     else
        timer100=0;
    }
 else
    {
      if(fer_reset_interserv && !servcom.ja_fet_reset_interserv)
          reset_interserv();
      paso_a_libre(0);
    }  
}


void paso_a_libre(int desde_off)
{
char buf_luz[2];
unsigned char  err;
T_FECHHOR f_ahora;

 RESET_LAPSUS();
 exces_vel=0;
 error_impre_en_curs_a_display=0;
 error_impre_en_curs_a_lumin=0;
// hay_precio_acordado = 0;
  servcom.es_control = 0;   // per NF_LEDTAR_INTERM


#if HOMOLOG
 SAVLUZ&=0xcf;
 timer_homolog=0;
#endif
 REFRESH_ON();
 if(tx30.estat>=ETX30_FIN || tx30.estat==E_MEM_MAL  || !tarifa_ok())
   {
    paso_MEM_MAL();
   }
 else
   {
    timer50=0;
    timer100=0;
    timer_2=12;   // per estat_impre cada 1.2"
    tx30.estat=E_LIBRE;
    tx30.new_estat=E_LIBRE;
    err=0;
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
    servei.mask_vd=2;
    servei.vd[0]=0;
    servei.vd[1]=0;

    // cancela possible entrada pin en Lliure
    tx30.pin_subestat = 0;
    MASK_GEN &= ~MSK_PIN_ENTRAR;
    
    luminoso.cnt_dec_segon_desde_pas_a_lliure = luminoso.valor_dec_segon_desde_pas_a_lliure;
    if(tarcom.luces_ver  &&
    ((luminoso.tipo_lumin & LLUMINOS_SERIE) )&&
    ((luminoso.tipo_lumin & LLUMINOS_NO_VERIFICABLE)==0))
        luminoso.serie_pendent_test_desde_pas_a_lliure = 2;
    else
        luminoso.serie_pendent_test_desde_pas_a_lliure = 0;

    err = fallos_hard();
    if((err == ERR_LLUMS)&&tarcom.hay_bloq_luz_lib)
    {
        out_luzex_no_save(tarcom.luz_bloq_lib[0],tarcom.luz_bloq_lib[1]);
    }
    else
    {
        select_luz(E_LIBRE,buf_luz);
        out_luzex(buf_luz[0],buf_luz[1]);
    }
    
    
	if(desde_off){
		reset_interserv();
        paso_a_libre_bloqueig_torn(); // nomes es crida si venim de OFF
	}

    tx30.acum_off=0;

    display_1_en_libre(err);
    
    
    if(!reloj_ok(RELLOTGE_PLACA)&& tarcom.disp_errrel)
       {
        display(1,NF_TEXT6,&tarcom.text_error[ERR_RELLOTGE][0]);
        APAGAR_DISPLAY(2);
        err=1;
       }
    LED_HORARIA_OFF;
    // apaga doble led cas chile
    NEW_LED_HORARIA_OFF;
    NEW_LED_KM_OFF;
    tx30.refresc_leds_ho_km = 0;
    
    if(tarcom.tipus_teclat==2)
       display(0,NF_LEDTAR_INTERM,(void*)tarifa[tx30.tarifa_rot].ledocup);
    else
       display(0,NF_LEDEST,(void*)&tarcom.vars_estat[E_LIBRE].ledest);

    if(!err && (pausa.estat == EPAUSA_temporitza_inici_pausa_passiva))
        {
         if(tarcom.visvel_imp_extr==2)
            DISPLAY_VD2(VD2_VEL); 
         else if(tarcom.OP_RELOJ_LIBRE)
            display(2,NF_HHMM,&gt.time.minute);
/*         
         else if(tarcom.visu_dia_torn == 1)
         {
           laux = (long)(bloqueo_torn.anydia_last_torn & 0x1ff);
           display(2,NF_TOT4,(void*)&laux);   // dia torn en format dia de l'any
         }
         else if(tarcom.visu_dia_torn == 2)
         {
           laux = (long)bloqueo_torn.diames_last_torn;
           display(2,NF_TOT4,(void*)&laux);   // dia torn en format mes/dia
         }
*/           
          else  
             display(2,NF_TEXT4,&tarcom.text_lib[6]);
        }
    gestio_fecha_paro();
    if(MASK_GEN & MSK_FP)
      {
       LEDS_FRONTAL(tarcom.lf_FECHA_PARO);
       if(tarcom.OP_E5)
           display(1,NF_TEXT6,&tarcom.text_error[ERR_FECHA_PARO][0]);
      }
    else
    {
         LEDS_FRONTAL(tarcom.vars_estat[E_LIBRE].lf);
    }
    
    if(tarcom.hi_ha_torn_prima_intern && prima.torn_obert)
      {
       time_to_fechhor(&f_ahora,&gt.time);
       if(fecha_hora_vencida(&f_ahora, &prima.data_tancar_torn))
       {
         prima_tancar_torn();
         prima_obrir_torn();
       }
      }
   }
 
 sensors_ant=0xff;
 impre_on();
       
}



typedef struct
{
 char desp_cpu_a;
 char desp_cpu_b;
 char nivel_a;
 char nivel_b;
 char temps_despertar;
 char desp_factor[2];
 char temps_watchdog;
 char temps_orden_off;
 char orden_off;
 char last_wakeup;
}pic_onoff_read;

typedef struct
{
 char desp_cpu_a;
 char desp_cpu_b;
 char nivel_a;
 char nivel_b;
 char temps_despertar;
 char desp_factor[2];
 char temps_watchdog;
 char temps_orden_off;
 char orden_off;
}pic_onoff_write;

pic_onoff_read buf_read_pic;
pic_onoff_write buf_write_pic;

 
void paso_off(void)
{
char buf_luz[2];

 reset_interserv();
 if(tx30.lluminos_apagat)   // v 1.11
   APAGAR_LLUMINOS();   // encen lluminos  i apaga punt (.)

 select_luz(E_OFF,buf_luz);
 out_luzex(buf_luz[0],buf_luz[1]);

 if(tx30.new_estat == E_OFF)
    display(0,NF_LEDEST,(void *)&tarcom.vars_estat[E_OFF].ledest);
 else
    display(0,NF_LEDEST,(void *)&tarcom.vars_estat[E_CERRADO].ledest);
 APAGAR_DISPLAY(1);
 APAGAR_DISPLAY(2);
 REFRESH_OFF();
 // v 1.04 out_luzfront(tarcom.luzest[menu_estado_TX30[E_OFF]].luz[sext_luz]);
 
 bloqueo_torn.stdesblq=0;

 tx30.cnt_sec_treure_tensio = tarcom.sec_treure_tensio;   // per treure tensio
 tx30.pendent_password_pas_a_on = 1;
 if(tarcom.test_disp_auto)
	 tx30.pendent_test_displ_pas_a_on = 1;
 impre_off();
 timer100=11; // inicia temporitzat 1"
}

void ESP_ON(void)
{
 if(tarcom.torn_continu ==  0)
 {
     if(--bloqueo_torn.cntdesblq)
       {
        timer50 = 20;
       }
     else
       {
        timer50=0;
        bloqueo_torn.stdesblq = 2;
        OFF_LIBRE();
       }
 }
 else
 {
      switch(bloqueo_torn.estat_torn)
      {
      case 0:   // torn tancat
      case 1:
      case 2:
         if(--bloqueo_torn.cntdesblq)
           {
            timer50 = 20;
           }
         else
           {
            timer50=0;
            bloqueo_torn.stdesblq = 2;
            OFF_LIBRE();
           }
         break;
      }
 }
}


void BLOQ_TEC(void)
{
 MASK_GEN |= MSK_BLQ;  // posa bloqueo teclat  
 tx30.new_estat=E_OFF;
 paso_off();
}

void DESBLOQ_TEC(void)
{
 MASK_GEN &= ~MSK_BLQ;  /* treu bloqueo teclat  */
}

__no_init unsigned char  switch_tarsup;   





void ON_OFF(int descansXtancar)
{
  // aqui pas a OFF
 if(tarcom.tr_ccab && ccab.logon)
   {
    paso_a_libre(0);
    return;
   }
    
 if(tarcom.hi_ha_td30 && (MASK_1 & MSK_PASLIOF))
  {
   paso_a_libre(0);
   return;
  }
 
// cas tornat a polsar Lliure mentres temporitzava
// pas a OFF o error no pot fer mes descansos
 if(tx30.acum_off) 
 {
   // cas Fran�a , si ja han passat tarcom.segs_anular_pas_off segons,
   // no permet cancelar pas a Off
   if(tx30.segons_pot_anular_pas_off)
      paso_a_libre(0);
   return;
 }
 
 
 if((descansXtancar == OP_DESCANS) && bloqueig_torn_prou_descansos())
 {
    // cas no pot passar a OFF (ja no pot fer mes descasos)
    display(1,NF_TEXT6,tarcom.texte_max_descans_fets);
    DISP_FLASH(DISP_LEDEST);
    DISP_FLASH(DISP_IMPORT);
    DISP_FLASH(DISP_EXTRES);
    timer50=20;
    tx30.acum_off = SEGONS_VISU_ERROR;
    tx30.segons_pot_anular_pas_off = SEGONS_VISU_ERROR + 1;  // per que es pugui anular sempre
    tx30.estat_a_final_tempor = E_LIBRE;
    return;
 }

 if(tarcom.tioff)
   {
       timer50=20;
       tx30.acum_off=tarcom.tioff;
       tx30.segons_pot_anular_pas_off = tarcom.segs_anular_pas_off;
       tx30.estat_a_final_tempor = E_OFF;
       tx30.descansXtancar = descansXtancar;
       //tx30.new_estat=E_LIBRE;
       //memset(RAM_DISP_FLASH,0,SIZE_RAM_DISP);
        DISP_FLASH(DISP_LEDEST);
        DISP_FLASH(DISP_IMPORT);
        DISP_FLASH(DISP_EXTRES);
   }
 else
    {
//     tx30.estat_pet=EPET_OFF;
     if(descansXtancar == OP_TANCAR)
       france_tancar_torn(minut_del_segle_from_time(&gt.time));
     else
       registrar_inici_descans();
     tx30.new_estat=E_OFF;  // 2.07
	 descans();
     paso_off();
    }
 
}
 

void ON_OFF_CARG_CERRT(void)
{
unsigned char cap_pram[6];
char st;

  if(hay_precio_acordado){
      hay_precio_acordado = 0;
      paso_a_libre(0);
	  return;
  }

  if(tx30.pin_subestat == 1)
  {
     // cas s'esta entrant pin
      paso_a_libre(0);
      return;
  }

	if(pausa.estat != EPAUSA_temporitza_inici_pausa_passiva)
	{
		// finalitza pausa
		pausa.estat = EPAUSA_temporitza_inici_pausa_passiva;
		pausa.segons = 0;
		MASK_GEN &= ~MSK_PAUSA;
		paso_a_libre(0);
		return;
	}

 if((tarcom.hi_ha_torn_prima_intern || configs_desde_bg40.hi_ha_torn_prima_bg40) && (tx30.estat != E_MEM_MAL))
  {
  // cas hi ha torn tria entre tancar torn o passar a off
    display(1,NF_TEXT6,&tarcom.texte_TANCAR_TORN[0]);
    tx30.new_estat = E_CERRADO;
    timer50 = 80;   // 4" per confirmar-ho   
    tx30.subestat_torn = TORN_ESPERA_TANCAR;  // per esperar confirmacio tancament
    return;
  }
 else
  {
  // nomes ho fa si no hi ha torn ( o si mem mal)
    if(hay_cargador())
      {
       // mira si es paquet de programa
       wread_i2c_adr3(EPROM_ARXIUS_CARG,0x0L,6,(char*)cap_pram);  // posicio bloc inici arxiu 1
       if(memcmp(cap_pram,"\x95\xaa\x55JRT",6) == 0)
       {
         buzzer_set(0);   // per parar pito tecla     
         salta_a_reset();   // treu interrupcions i salta a 0x60000000
       } 

       CARGADOR();  // nomes retorna si no entra.Si entra salta a 0x60000000
       return;
      }
    if(tx30.estat==E_MEM_MAL)
      {
       paso_off();
       return;
      }
    st = toca_precambio();
    if(st)
      {
       EJECUTAR_PRECAMBIO(st);  
       return;
      }
  }
  // aqui cas pas a off 

  ON_OFF(OP_DESCANS);
}


void ACUM_PAUSA(void)
{
long laux;
	if((MASK_GEN & MSK_PAUSA) &&
	   ((pausa.estat == EPAUSA_pausa_activa) || (pausa.estat == EPAUSA_pausa_passiva)))
	{
		laux = pausa.segons_acumulat_pausa/60;
		display(2,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
		pausa.visu_acumulat = 1;
	}
}

void FI_ACUM_PAUSA(void)
{
long laux;
	if((MASK_GEN & MSK_PAUSA) &&
	   ((pausa.estat == EPAUSA_pausa_activa) || (pausa.estat == EPAUSA_pausa_passiva)) &&
		   pausa.visu_acumulat)
	{
		laux = pausa.segons/60;
		display(2,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
		pausa.visu_acumulat = 0;
	}
}

// tanca torn (cas Fran�a)
void TANCAR_TORN(void)
{
  if(tarcom.torn_continu)
      ON_OFF(OP_TANCAR);
  else
  {
    paso_a_libre(0);    // nomes per restaurar timers i displays 
  }
}

void ESP_OFF(void)
{
  
 if(tarcom.tr_ccab && ccab.logon)
   {
    paso_a_libre(0);
    return;
   }
 if(tx30.segons_pot_anular_pas_off && (tarcom.segs_anular_pas_off != 0xff))
   --tx30.segons_pot_anular_pas_off;
 if(--tx30.acum_off)
   {
    timer50=20;
   }
 else
   {
//     tx30.estat_pet=EPET_OFF;
     if(tx30.estat_a_final_tempor == E_LIBRE)
        paso_a_libre(0);    // cas temporitzava error no pot fer descans
     else
     {
       // cas E_OFF
       if(tx30.descansXtancar == OP_TANCAR)
          france_tancar_torn(minut_del_segle_from_time(&gt.time));
       else
         registrar_inici_descans();

       paso_off();
       tx30.new_estat=E_OFF;
     }
   }                                             
}

void onoff_ports(int onoff)
{
  static unsigned short saveport0;
  static unsigned short saveport1;
  static unsigned short saveport2;
  static char saveport6;
  if (onoff==0)
  {
    saveport0=IOPORT0_PD;
    saveport1=IOPORT1_PD;
    saveport2=IOPORT2_PD;
    saveport6=PORT6;
    if(tipoTx==TX50)
    {
      IOPORT_3 = 0x00;
      IOPORT_4 = 0x80;
      IOPORT_5 = 0x00;
      IOPORT_6 = 0x00;
      
      IOPORT0_PC0 = 0x0000;
      IOPORT0_PC1 = 0xA504;
      IOPORT0_PC2 = 0x5AFB; 
     
      IOPORT0_PD&=0x1010;
      IOPORT1_PD&=0x7a4f;
      IOPORT2_PD&=0xff3f;
    }
    else
    {
      write_i2c(IO_EXPANDER_TX40,0x12,2,"\x00\x00");
      OS_Delay(10);
      IOPORT0_PC1 &= 0xf5ff;
      IOPORT0_PD &= 0xd5ff;
      IOPORT1_PD &= 0x0000;
      IOPORT2_PD &= 0xff3f;
    }
  }
  else
  {

    if(tipoTx==TX50)
    {
      IOPORT_3 = PORT3;
      IOPORT_4 = PORT4;
      IOPORT_5 = PORT5;
      IOPORT_6 = PORT6;
      IOPORT0_PC0 = 0x5aff;
      IOPORT0_PC1 = 0xef70;
      IOPORT0_PC2 = 0x5aff; 
    }
    else
    {
      IOPORT0_PC0 = 0x3a3f;
      IOPORT0_PC1 = 0xcfc0;
      IOPORT0_PC2 = 0x3a3f; 
      write_i2c(IO_EXPANDER_TX40,12,1,&saveport6);
    }
    IOPORT0_PD=saveport0;
    IOPORT1_PD=saveport1;
    IOPORT2_PD=saveport2;
  }
}

#define _OS_TIMER_ID     T2_GI_VECT_ID  // Assign to TC2 global interrupt  fbp canviat a timer 2
extern OS_INTERWORK void OS_InitPLL(void);

void stop(void)
{
    while(1)
    {
      
      // prepara Wake-up

      OS_ARM_DisableISR(_OS_TIMER_ID);    // Enable timer/counter 0 interrupt.
      OS_ARM_DisableISR(XTI_VECT_ID);     // Disable interrupt 

      XTI_CTRL_bit.ID1S = 0;
      XTI_PRH=0;                          // clear pending interrupts
      XTI_PRL=0;                          // clear pending interrupts
      if(tipoTx==TX50)
      {
      XTI_MRL |= 0x04;                    // P2.8    10Hz
      XTI_MRL &= ~0x08;                    // P2.9    Metres
      XTI_MRL &= ~0x10;                   // P2.10  inhibeix timer error impre
      }
      else
      {
        while(RTC_CRL_bit.RTOFF==0);          // Espera que el reloj este preparado para entrar en modo configuracion
        RTC_CRL_bit.CNF=1;                    // Entra en modo configuracion
        RTC_ALRH=RTC_CNTH;                    // Configura la alarma a 1 segundo
        RTC_ALRL=RTC_CNTL+1;
        if(RTC_ALRL==0) RTC_ALRH++;
        RTC_CRH_bit.AEN=1;                    // Activa la interrupcion para que se despirte por la alarma
        RTC_CRH_bit.GEN=1;                    // Activa las interrupciones del reloj
        RTC_CRL=0;                            // Sale del modo configuracion
        while(RTC_CRL_bit.RTOFF==0);          // Espera que el reloj salga del modo configuracion
        
        XTI_TRH |= 0x80;                      // despierta por nivel 1 al llegar la alarma del reloj
        XTI_MRH |= 0x80;                      // P1.15 Pulsador 1 | alarma reloj
      }
      XTI_CTRL_bit.WKUP_INT = 1;          // enable wake-up mode 
      RCCU_CFR_bit.STOP_I=0;
     
      // STOP
      XTI_CTRL_bit.STOP =0;
      XTI_CTRL_bit.STOP =0;
      XTI_CTRL_bit.STOP =1;
      XTI_CTRL_bit.STOP =0;
      XTI_CTRL_bit.STOP =1;
      
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      asm("NOP");
      
      if(XTI_PRL & 0x04)                    // P2.8 s'ha despertat per 10Hz
      {
        XTI_PRL &= ~0x04;
        break;
      }
      
      if(tipoTx==TX40)
      {
        if(IOPORT0_PD_bit.no15==1) break;    // pulsador T0
        if(IOPORT2_PD_bit.no12==0) break;    // Clau contacte
        //if(IOPORT2_PD_bit.no13==0) break;  // Llums posicio
      }
      
    }    // while(1)
    
    
    
    // desactiva Wake-up i restaura interrupts
      XTI_CTRL_bit.WKUP_INT = 0;          // Disable wake-up mode 

      XTI_MRH &= ~0x80;                   // P1.15 Pulsador 1 | alarma reloj
      XTI_PRH=0;                          // clear pending interrupts
      XTI_PRL=0;
      XTI_CTRL_bit.ID1S = 1;

      if(tipoTx==TX50)
      {
        XTI_MRL |= 0x04;   // P2.8    10Hz
        XTI_MRL |= 0x08;   // P2.9    Metres
        XTI_MRL |= 0x10;   // P2.10   timer error impre
      }
      else
      {
        while(RTC_CRL_bit.RTOFF==0);          // Espera que el reloj este preparado para entrar en modo configuracion
        RTC_CRL_bit.CNF=1;                    // Entra en modo configuracion
        RTC_CRH_bit.AEN=0;                    // Desactiva la interrupcion del segundo
        RTC_CRL=0;                            // Sale del modo configuracion
        while(RTC_CRL_bit.RTOFF==0);          // Espera que el reloj este preparado para entrar en modo configuracion
      }
      OS_ARM_EnableISR(XTI_VECT_ID);     // Enable interrupt 
      OS_ARM_EnableISR(_OS_TIMER_ID);    // Enable timer/counter 0 interrupt.
      OS_InitPLL();
}

void apagar_equip(void)
{
  
      // l'indicador ve_de_baix_consum s'actualitza abans de pasar a stop()
      // per tal que si es produis fallo tensio mentres esta en stop no ho vegi
      // com fallo de tensio.
      bloqueo_torn.ve_de_baix_consum = 1;   
      buzzer_set(0);
      REFRESH_OFF();    // apaga displays
      
      if(tipoTx==TX50)
      {
        buf_write_pic.desp_cpu_a = 0x85;  // desp. per clau contacte i impulsos
        buf_write_pic.desp_cpu_b = 0x82;  // desp. per tecla
        buf_write_pic.nivel_a = 0x84;
        buf_write_pic.nivel_b = 0x82;
        buf_write_pic.temps_despertar = 0x8c;
        buf_write_pic.desp_factor[0] = 0xb8;
        buf_write_pic.desp_factor[1] = 0xb8;
        buf_write_pic.temps_watchdog = 0xff;
        buf_write_pic.temps_orden_off = 0x80;
        buf_write_pic.orden_off = 0xC1;  // OFF alim  CPU i OTROS  Impulso tiempo
        write_i2c(PIC_FIRMWARE,ONOFF, sizeof(buf_write_pic), (char*)&buf_write_pic);
        OS_Delay(200);
      }
 
      onoff_ports(0);
      stop();
      onoff_ports(1);
      OS_Delay(200);
      REFRESH_ON();   // encen displays
      salta_a_reset();
}

void OFF_TREURE_TENSIO(void)
{
  
  // mira si ja porta prou temps en OFF
  if(tx30.cnt_sec_treure_tensio != 0)
  {
    tx30.cnt_sec_treure_tensio--;
    timer100=11; // re inicia temporitzat 1"
    return;
  }
  //  si  hi ha clau de contacte 
  //  o esta temporitzant per passar a ON
  // no apaga
  if((sensors & SENSORS_CLAU_CON) || bloqueo_torn.stdesblq )
  {
     timer100=11; // inicia temporitzat 1"
     return;
  }
  
  apagar_equip();
  
}


void CERR_TREURE_TENSIO(void)
{
  
  // mira si ja porta prou temps en CERR
  if(tx30.cnt_sec_treure_tensio != 0)
  {
    tx30.cnt_sec_treure_tensio--;
    timer100=11; // re inicia temporitzat 1"
    return;
  }
  //  si  hi ha clau de contacte 
  //  o esta temporitzant per passar a ON
  // no apaga
  if((sensors & SENSORS_CLAU_CON) || bloqueo_torn.stdesblq)
  {
     timer100=11; // inicia temporitzat 1"
     return;
  }
  
  apagar_equip();
    
}



// torna tensio desde OFF 
void OFF_0(void)
{
 // inicia timer per apagar equip
//!!!!!!! v 1.03F tx30.new_estat = E_OFF;  // 4.54
 paso_off();
}


void CERR_0(void)    
{

 switch(tx30.subestat_torn)
   {
    case TORN_TANCAT:
    case TORN_ESPERA_OBRIR:
    case TORN_TRIAR_OPCIO:
    case TORN_TRIAR_TORN:
      tx30.subestat_torn = TORN_TANCAT;
	  descans();
      OFF_0();
      break;
    case TORN_ESPERA_TANCAR:   
       tx30.subestat_torn = TORN_TANCAT;
	   descans();
       ON_OFF(OP_DESCANS);
       break;
   }
  
}


void CERR_TLIBRE(void)
{
unsigned char cap_pram[6];
char st;

 REFRESH_ON();
 switch(tx30.subestat_torn) 
    {
    case TORN_TANCAT:
      if(hay_cargador())
        {
         // mira si es paquet de programa
         wread_i2c_adr3(EPROM_ARXIUS_CARG,0x0L,6,(char*)cap_pram);  // posicio bloc inici arxiu 1
         if(memcmp(cap_pram,"\x95\xaa\x55JRT",6) == 0)
         {
           buzzer_set(0);   // per parar pito tecla
  
           // treu interrupcions i salta a 0x60000000
           salta_a_reset();   
         } 
  
         CARGADOR();  // nomes retorna si no entra.Si entra salta a 0x60000000
         REFRESH_OFF();
         return;
        }
        st = toca_precambio();
        if(st)
        {
         EJECUTAR_PRECAMBIO(st); // salta a 0x60000000 al acabar         
         return;
        }
      // mira si ja no hi ha torn (pot haver estat cancelat desde bg40)
      if(!tarcom.hi_ha_torn_prima_intern && !configs_desde_bg40.hi_ha_torn_prima_bg40)
      {
        // pasa directament a lliure
        timer50 = 0;
        // obre torn
        //prima_obrir_torn();
        tx30.new_estat=E_LIBRE;
        paso_a_libre(0);
        return;
      }
        
      // inici apertura torn ( si no esta prohibit per mask)
      if(!(tarcom.hi_ha_td30 && (MASK_1 & MSK_TORN_OBRIR)))
      {
          tx30.timer_tick_torn = 0;  // cancela imprimir torn anterior
          tx30.num_conductor = 0;
          display(2,NF_TEXT4,&tarcom.texte_COND[0]);
          display(1,NF_TOT4,&tx30.num_conductor);
          tx30.subestat_torn = TORN_ESPERA_OBRIR;
          tx30.subestat_obrir_torn = ENTRADA_CONDUCTOR;
          timer50 = 80;   // 4" d'espera 
          return;
      }
      // cas inhibit per mascara desde td30, torna a torn tancat
      // sense break;
    case TORN_ESPERA_OBRIR:
      // cancela entrada de conductor i torna a E_CERRADO
      timer50 = 0;
      tx30.subestat_torn = TORN_TANCAT;
	  descans();
      paso_off();
      return;
    case TORN_ESPERA_TANCAR:
      // cas esperaba tancar torn
      // passa a E_OFF
      timer50 = 0;
      ON_OFF(OP_DESCANS);
      return;
    case TORN_TRIAR_OPCIO:
    case TORN_TRIAR_TORN:
      timer50 = 0;
      tx30.subestat_torn = TORN_TANCAT;
	  descans();
      paso_off();
      return;
    }
}

void incre_var(int tecla, long* p_var)
{
char buf[4];
long var;
int i;
   if(tecla <4)
     {
      timer50 = 80;  // reinicia 4"
      for(i=0,var= *p_var;i<4;i++,var /=10)
        buf[i] = var%10;
      buf[3-tecla]++;
      if(buf[3-tecla]>9)
        buf[3-tecla]=0;
      for(i=0,var = 0L;i<4;i++)
        var = var*10L+buf[3-i];
      *p_var = var;
  
     }
}

void incre_hhmm(int tecla, short* p_var)
{
char hh,mm;

   if(tecla <4)
     {
       hh = (*p_var)/60;
       mm = (*p_var)% 60;
        switch(tecla)
        {
        case 0:
          if(hh >= 20) hh %= 10; else hh +=10;
          if(hh > 23) hh = 20;
          break;
        case 1:
          if(hh >= 23) hh = 20;
          else if((hh % 10) == 9) hh = (hh/10)*10; else hh++;
          break;
        case 2:
          if((mm / 10) >= 5)mm %= 10; else mm += 10;          
          break;
        case 3:
          if((mm % 10) == 9)mm = (mm/10)*10; else mm++;
          break;
        }
        *(p_var) = hh * 60 + mm;
     }
}

void ABRIR_TURNO_COND(void)
{
 if(tx30.subestat_torn == TORN_ESPERA_OBRIR)
 {
   switch(tx30.subestat_obrir_torn)
   {
   case ENTRADA_CONDUCTOR:
     incre_var(tecla_num,&tx30.num_conductor);
     display(1,NF_TOT4,&tx30.num_conductor);
     break;
   case ENTRADA_PASSWORD:
     incre_var(tecla_num,&tx30.password_conductor);
     display(1,NF_TOT4,&tx30.password_conductor);
     break;
   default:
     break;
   }
 }
}



void prima_obrir_torn(void)
{
T_FECHHOR save_fecha;
int st;

     while(fallo_tension()); 
  
     totalitzador_incr(TOT_NTORN);
     save_fecha = reg_prima_torn->fecha_fi;
     memset(reg_prima_torn,0,sizeof (struct S_DREG_PRIMA_TORN));
     reg_prima_torn->num_torn = totalitzador_read(TOT_NTORN, &st);
     reg_prima_torn->num_conductor = tx30.num_conductor;
     time_to_fechhor(&reg_prima_torn->fecha_ini,&gt.time);
     reg_prima_torn->fecha_fi_torn_ant = save_fecha;
     reg_prima_torn->km_torn_tancat_prev = servei.hm_libre;
     reg_prima_torn->velmax_torn_tancat_prev = servei.velmax;
     reg_prima_torn->hm_libre_pasj_torn_tancat_prev = servei.hm_libre_pasj; 
     // copia percentatges iva
     reg_prima_torn->percent_iva0 = tarcom.valors_iva[0];
     reg_prima_torn->percent_iva1 = tarcom.valors_iva[1];
     reg_prima_torn->percent_iva2 = tarcom.valors_iva[2];
     
     reset_interserv();     
     prima.reg_servei_en_curs = -1; // treu indicador hi ha servei actualitzable
     
     prima_inici_torn();
     // determina data tancament automatic torn
     prima.data_tancar_torn = reg_prima_torn->fecha_ini;
     sumar_dias_a_fecha(&prima.data_tancar_torn,1);  // + 1 dia
     prima.torn_obert = 1;
	 
	 pausa.segons_acumulat_pausa = 0;
}

void prima_tancar_torn(void)
{

    while(fallo_tension()); 
  
    time_to_fechhor(&reg_prima_torn->fecha_fi,&gt.time); 
    reg_prima_torn->km_torn += servei.hm_libre;
    reg_prima_torn->min_torn += servei.minuts_lib;
    reg_prima_torn->km_fins_fi_torn = servei.hm_fins_fi_torn;
	servei.hm_fins_fi_torn = 0;
     reg_prima_torn->velmax_fins_fi_torn = servei.velmax;
     reg_prima_torn->hm_libre_pasj_fins_fi_torn = servei.hm_libre_pasj; 
    reset_interserv();  // per reset km en torn tancat
    reg_prima_torn->eur_km = 0;
    if(reg_prima_torn->km_torn)
      reg_prima_torn->eur_km = (reg_prima_torn->total_torn*10 + reg_prima_torn->km_torn/2)/reg_prima_torn->km_torn;
    reg_prima_torn->km_per_cent = 0;
    if(reg_prima_torn->km_torn)
      reg_prima_torn->km_per_cent = (reg_prima_torn->km_oc_torn*100)/reg_prima_torn->km_torn;
    reg_prima_torn->min_per_cent = 0;
    if(reg_prima_torn->min_torn)
       reg_prima_torn->min_per_cent = (reg_prima_torn->min_oc_torn*100)/reg_prima_torn->min_torn;
    prima.reg_servei_en_curs = -1; // treu indicador hi ha servei actualitzable
    prima_final_torn();   // actualitza registre torn a taula 
    if(tarcom.ticket_tancar_torn)
        imprimir_ticket_torn();
    tx30.subestat_torn = TORN_TANCAT;
    prima.torn_obert = 0;

}

void TORN_ACK_PASSWORD(void)
{
  if((tx30.subestat_torn == TORN_ESPERA_OBRIR) &&
     (tx30.subestat_obrir_torn == ESPERA_RESPOSTA))
  {
  timer50 = 0;
    // obre torn
     prima_obrir_torn();
     tx30.new_estat=E_LIBRE;
     paso_a_libre(0);
  }
}

void TORN_NACK_PASSWORD(void)
{
  if((tx30.subestat_torn == TORN_ESPERA_OBRIR) &&
     (tx30.subestat_obrir_torn == ESPERA_RESPOSTA))
  {
  timer50 = 0;
  buzzer_set(300);
  tx30.subestat_torn = TORN_TANCAT;
  paso_off();
  }
}

extern void enviar_pregunta_password(void);

void CERR_TREL(void)
{
long laux,l_HHMM;
  REFRESH_ON();
  switch(tx30.subestat_torn)
  {
  case TORN_TANCAT:
    VISU_MOD_RELOJ();
    return;
  case TORN_ESPERA_OBRIR:
    switch(tx30.subestat_obrir_torn)
    {
    case ENTRADA_CONDUCTOR:
      if(configs_desde_bg40.hi_ha_password_cond)
      {
      tx30.password_conductor = 0;
      display(2,NF_TEXT4, &tarcom.texte_PASSW[0] );        
      display(1,NF_TOT4,&tx30.password_conductor);
      tx30.subestat_obrir_torn = ENTRADA_PASSWORD;
      timer50 = 80;   // 4" d'espera 
      }
      else
      {
        timer50 = 0;
        // obre torn
        prima_obrir_torn();
        tx30.new_estat=E_LIBRE;
        paso_a_libre(0);
      }
     break;
    case ENTRADA_PASSWORD:
      APAGAR_DISPLAY(2);      
      display(1,NF_TEXT6,"\x40\x40\x40\x40\x40\x40");
      tx30.subestat_obrir_torn = ESPERA_RESPOSTA;
      enviar_pregunta_password();
      timer50 = 80;   // 4" d'espera 
     break;
    default:
     break;
    } 
    break;
  case TORN_ESPERA_TANCAR:
      if((MASK_1 & MSK_TORN_TANCAR) && !hay_cargador()) // si hi ha carregador prescindeix de bloqueig tancar torn
      {
        // passa a lliure
        timer50 = 0;
        paso_a_libre(0);
        return;
      }
    // tanca torn
    if(tarcom.hi_ha_torn_prima_intern)
    {
      prima_tancar_torn();
      timer50 = 20;   // per 1"
      tx30.timer_tick_torn = 60; // segons per poder imprimir ticket
    }
    tx30.subestat_torn = TORN_TANCAT;
    paso_off();
    return;
  case TORN_TRIAR_OPCIO:
    // mira si hi ha torns per imprimir
    if(prima.num_regs_torn == 0)
    {
     // cas no hi ha cap torn per visualitzar
     tx30.subestat_torn = TORN_TANCAT; 
     timer50 = 0;
     paso_off();
    }
    else
    {
     prima.itorn_visu = prima_get_index_last_torn();
     prima.ntorns_visu = prima.num_regs_torn;      
     prima_llegir_torn(prima.itorn_visu);
     laux = reg_prima_torn->fecha_ini.day;
     display(0,NF_TOT2,&laux);
     l_HHMM = reg_prima_torn->fecha_ini.hour*100L + reg_prima_torn->fecha_ini.minute;
     display(1,NF_HH_MM,&l_HHMM);
     display(2,NF_VEL,&reg_prima_torn->num_conductor);
     tx30.subestat_torn = TORN_TRIAR_TORN;
     timer50 = 120;  // 6"
    }
    return;
    case TORN_TRIAR_TORN:
       // visualitza dades torn anterior
       if(--prima.ntorns_visu == 0)
       {
         prima.itorn_visu = prima_get_index_last_torn();
         prima.ntorns_visu = prima.num_regs_torn;
       }
       else
          prima.itorn_visu = prima_get_index_prev_torn(prima.itorn_visu);

       prima_llegir_torn(prima.itorn_visu);
       laux = reg_prima_torn->fecha_ini.day;
       display(0,NF_TOT2,&laux);
       l_HHMM = reg_prima_torn->fecha_ini.hour*100L + reg_prima_torn->fecha_ini.minute;
       display(1,NF_HH_MM,&l_HHMM);
       display(2,NF_VEL,&reg_prima_torn->num_conductor);
       timer50 = 120;  // 6"
       return;
  }
}

void CERR_TIMER(void)
{
  switch(tx30.subestat_torn)
  {
    case TORN_TANCAT:
      // entra cada segon
      if(tx30.timer_tick_torn)
        tx30.timer_tick_torn--;  // decrementa segons per poder imprimir ticket
      if(tx30.timer_tick_torn)
        timer50 = 20;   // per 1"
      else
        timer50 = 0;
      return;
    case TORN_ESPERA_OBRIR:
      // cancela apertura despres de 4"
      timer50 = 0;
      tx30.subestat_torn = TORN_TANCAT;
	  descans();
      paso_off();
      return;
    case TORN_ESPERA_TANCAR:
      // cas esperaba tancar torn
      // passa a lliure
      timer50 = 0;
      paso_a_libre(0);
      return;
    case TORN_TRIAR_OPCIO:
    case TORN_TRIAR_TORN:
      timer50 = 0;
      tx30.subestat_torn = TORN_TANCAT;
	  descans();
      paso_off();      
      return;
  }
}

void TORN_REPE_TICKET(void)
{
  switch(tx30.subestat_torn)
  {
    case TORN_TANCAT:
      if(tx30.timer_tick_torn)
      {
       imprimir_ticket_torn(); 
      }
      break;
    case TORN_TRIAR_TORN:
      // imprimeix torn triat
      timer50 = 0;
      imprimir_ticket_torn();
      tx30.subestat_torn = TORN_TANCAT;
      paso_off();
      break;
  }
  
}


void CERR_TCODS(void)
{
       switch(tx30.subestat_torn)
       {
        case TORN_TANCAT:
           // cancela es pugui treure ticket tancar torn
           timer50=0;
           tx30.timer_tick_torn=0;
           REFRESH_ON();
           INICIO_CODS();
           break;
        }
}

// ***********************************************************************
// ********* recalcula parametres tarifacio per distancia ****************
// ********* cas Wegstreckenzahler (alemania)             ****************
// ***********************************************************************
void recalcul_params_tar_distancia(void)
{                             
float  fs;
float  tics_d,tics_t,factor;
unsigned long  tics,i_factor;
unsigned long     KK;
unsigned long     KH;
char  modo; 
struct s_bloc_tar blocaux;
uchar  nbt;


 KK = tarcom.k/tarcom.DIVK;                  
 KH = 3600L*FACTOR_HZ;            
 
 for(nbt=0;nbt<tarcom.nblocs_tar;nbt++)
   {
    memcpy(&blocaux,&bloc_tar[nbt],sizeof blocaux);

    // INI: ANTONIO XAPU BCN CARRETERA
    // aqui a carregador, es feia una disquisici� per el cas de velo frontera tipus 2
    // com ara nomes es cas wegstreck..  no cal fer-ho
    // nomes es posa els valors = 1 
	blocaux.tarifarDistancia_ps=1;
	blocaux.tarifarDistancia_ss=1;       
    // FIN: ANTONIO XAPU BCN CARRETERA      
       
    // copia valor entrat ( el passa a float segons decimals )
    blocaux.impkm_ss = (float)tx30.var_preu[nbt]/(float)KDEC[tarcom.NDEC];
    // actualitza bits_cuenta (nomes per distancia)
    if(blocaux.impkm_ss)
      blocaux.bits_cuenta = 0x55; // primer i seguents
    else
      blocaux.bits_cuenta = 0;
       
        
    // ***** fraccions salt succesius salts ******************

    modo=0;
    if(blocaux.bits_cuenta & 0x44)  // k_ss  oc pag  
      {
       modo=1;
       tics_d=(KK*(float)blocaux.vsal[0])/(blocaux.impkm_ss*KDEC[tarcom.NDEC]);
      } 
    if(blocaux.bits_cuenta & 0x88)  // h_ss  oc pag  
      {
       modo+=2;  
       tics_t=(3600.*FACTOR_HZ*(float)blocaux.vsal[1])/(blocaux.impho_ss*KDEC[tarcom.NDEC]);
      }
    switch(modo)
      {
       case 0:
         // no conta ni temps ni distancia
         // valors nomes serveixen per cas canvi tarifa
         // per convertir fraccio de salt que ja hi hagues feta
         blocaux.FACTOR_N_ss=10;
         blocaux.FS_K_ss=1000;
         blocaux.KK_ss=1000;
         blocaux.FS_H_ss=1000;
         blocaux.KH_ss=1000;
         break;
       case 1:  // no conta temps
         tics=(unsigned long)tics_d;
         blocaux.FS_H_ss=1000;
         blocaux.KH_ss=1000;
         break;
       case 2:  // no conta distancia
         tics=(unsigned long)tics_t;
         blocaux.FS_K_ss=1000;
         blocaux.KK_ss=1000;
         break;  
       case 3:
         if(tics_d<tics_t)
           tics=(unsigned long)tics_d;
         else  
           tics=(unsigned long)tics_t;
         break;      
      }
    if(modo)
      {  
       if(tics<100)
         blocaux.FACTOR_N_ss=i_sqrt(tics);
       else
         blocaux.FACTOR_N_ss=tics/10;
       if(modo & 0x01)
         {      
          fs=(blocaux.impkm_ss*blocaux.FACTOR_N_ss*KDEC[tarcom.NDEC])/blocaux.vsal[0];
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_K_ss = (unsigned long)(fs*(float)i_factor);
             blocaux.KK_ss = (unsigned long)(KK*(float)i_factor);
            }            
          else
            {
             blocaux.FS_K_ss = (unsigned long)fs;
             blocaux.KK_ss = KK;
            }
         }
       if(modo & 0x02)
         {      
          fs=(blocaux.impho_ss*blocaux.FACTOR_N_ss*KDEC[tarcom.NDEC])/blocaux.vsal[1];
          if(fs<1000.)
            {
             factor=1000./fs;
             i_factor=(int)factor;
             if((float)i_factor<factor)
              i_factor++;
             blocaux.FS_H_ss = (unsigned long)(fs*(float)i_factor);
             blocaux.KH_ss = (unsigned long)(KH*(float)i_factor);
            }            
          else
            {
             blocaux.FS_H_ss = (unsigned long)fs;
             blocaux.KH_ss=KH;
            }
         }   
      }
    
    
    //****** fraccions salt primer salt *********************

    // recalcula metros primer salt (tarifa igual que successius)
    if(blocaux.impkm_ss)
        blocaux.m_ps = ((float)blocaux.vsal[0] * tarcom.yardas)/(blocaux.impkm_ss*(float)KDEC[tarcom.NDEC]);
    else
      blocaux.m_ps = 0;
    blocaux.FACTOR_N_ps = blocaux.FACTOR_N_ss;
    blocaux.FS_K_ps = blocaux.FS_K_ss;
    blocaux.KK_ps   = blocaux.KK_ss;
    blocaux.FS_H_ps = blocaux.FS_H_ss;
    blocaux.KH_ps   = blocaux.KH_ss;
    
         
    memcpy(&bloc_tar[nbt],&blocaux,sizeof blocaux);
	

}  // final loop tarifas 
}



void MOD_TAR_TNUM(void)
{
long l_aux;
  if(tecla_num <4)
  {
   incre_var(tecla_num,&tx30.var_preu[tx30.index_tar]);
   display(1,NF_SUPL_NOBLK,&tx30.var_preu[tx30.index_tar]);
  }
  else if(tecla_num == 4)
  {
    // mira si toca seguent tarifa
    if(tx30.index_tar < (tarcom.ntars -1))
    {
      tx30.index_tar++;
      display(0,NF_LEDEST,(unsigned char*)&tarcom.vars_estat[E_MOD_TAR].ledest);                  
      display(1,NF_SUPL_NOBLK,&tx30.var_preu[tx30.index_tar]);
      l_aux = tx30.index_tar+1;
      display(2,NF_VEL,&l_aux);
    }
    else
    {
      // recalcula primers salts i bits cuenta
      recalcul_params_tar_distancia();
      check_sum = calcul_check();
      // acaba 
      fin_cods();
    }
  }
  
}

// definicions per VeRiFicacio items pre programacio torns ( France )
#define VRF_NULL          0
#define VRF_DIFF          1
#define VRF_ITEM_DINS     2


struct s_taula_preprog{
 char dt[2];
 short* var;
 char ds[4];
  char op;
  char item1;
  char item2;
}const taula_preprog[]={
'\x78','\x06',&bloqueo_torn.preprog_hora_inici_torn[0],'\x06','\x54','\x10','\x00',    VRF_NULL,         0,   0, // 0  t1 ini
'\x78','\x06',&bloqueo_torn.preprog_hora_final_torn[0],'\x71','\x04','\x54','\x00',    VRF_DIFF,         0,   0, // 1  t1 fin
'\x5e','\x06',&bloqueo_torn.preprog_hora_inici_descans[0],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,    0,   1, // 2  d1 ini 
'\x5e','\x06',&bloqueo_torn.preprog_hora_final_descans[0],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,    2,   1, // 3  d1 fin
'\x5e','\x5b',&bloqueo_torn.preprog_hora_inici_descans[1],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,    3,   1, // 4  d2 ini
'\x5e','\x5b',&bloqueo_torn.preprog_hora_final_descans[1],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,    4,   1, // 5  d2 fin
'\x5e','\x4f',&bloqueo_torn.preprog_hora_inici_descans[2],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,    5,   1, // 6  d3 ini 
'\x5e','\x4f',&bloqueo_torn.preprog_hora_final_descans[2],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,    6,   1, // 7  d3 fin

'\x78','\x5b',&bloqueo_torn.preprog_hora_inici_torn[1],'\x06','\x54','\x10','\x00',    VRF_ITEM_DINS,    1,0xff, // 8  t2 ini
'\x78','\x5b',&bloqueo_torn.preprog_hora_final_torn[1],'\x71','\x04','\x54','\x00',    VRF_ITEM_DINS,    8,   0, // 9  t2 fin
'\x5e','\x66',&bloqueo_torn.preprog_hora_inici_descans[3],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,    8,   9, //10  d4 ini
'\x5e','\x66',&bloqueo_torn.preprog_hora_final_descans[3],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,   10,   9, //11  d4 fin
'\x5e','\x6d',&bloqueo_torn.preprog_hora_inici_descans[4],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,   11,   9, //12  d5 ini
'\x5e','\x6d',&bloqueo_torn.preprog_hora_final_descans[4],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,   12,   9, //13  d5 fin
'\x5e','\x7d',&bloqueo_torn.preprog_hora_inici_descans[5],'\x06','\x54','\x10','\x00', VRF_ITEM_DINS,   13,   9, //14  d6 ini
'\x5e','\x7d',&bloqueo_torn.preprog_hora_final_descans[5],'\x71','\x04','\x54','\x00', VRF_ITEM_DINS,   14,   9, //15  d6 fin
};

short  hhmm_2400 = 24*60; 

int verif_item_ok(int item)
{
int err = 0;
short var;
short ini;
short fin;
char item2;


    var = *taula_preprog[item].var;
    switch (taula_preprog[item].op)
    {
    case VRF_NULL:
      break;
    case VRF_DIFF:
      if(var == *taula_preprog[taula_preprog[item].item1].var)
        err = 1;
      break;
    case VRF_ITEM_DINS:
      // verifica item dins interval
      ini = *taula_preprog[taula_preprog[item].item1].var;
      item2 = taula_preprog[item].item2;
      if(item2 == 0xff)
        fin = hhmm_2400;
      else  
        fin = *taula_preprog[item2].var;
      if(!dins_interval(var, ini, fin))
          err = 1;
      break;
    }
  return(err);
}

void preprog_display(void)
{
long laux;
   laux = (long)*taula_preprog[bloqueo_torn.preprog_index].var;
   display(0,NF_LEDEST,(void *)taula_preprog[bloqueo_torn.preprog_index].dt); // d1 ... dn
   display(1,NF_MINUTS_TO_HH_MM,(void *)&laux);
   display(2,NF_TEXT4,(void *)taula_preprog[bloqueo_torn.preprog_index].ds);  
}

void validar_hhmm(short* hhmm)
{
     if((*hhmm < 0) || (*hhmm >= 24*60))
       *hhmm = 0;
}     
       

void PREPROG_TNUM(void)
{
  long laux;
  if(tecla_num <4)
  {
   incre_hhmm(tecla_num,taula_preprog[bloqueo_torn.preprog_index].var);
   laux = (long)*taula_preprog[bloqueo_torn.preprog_index].var;
   display(1,NF_MINUTS_TO_HH_MM,(void *)&laux);
  }
  else if(tecla_num == 4)
  {
    // verifica item entrat
    if(verif_item_ok(bloqueo_torn.preprog_index) != 0)
    {
      // cas item erroni
      OS_Delay(100);
      buzzer_set(500);
      //fin_cods();
      validar_hhmm(taula_preprog[bloqueo_torn.preprog_index].var);
      preprog_display();
      return;
    }
    
    // mira si toca seguent item
    if(bloqueo_torn.preprog_index < (DIM(taula_preprog) -1))
    {
      bloqueo_torn.preprog_index++;
      validar_hhmm(taula_preprog[bloqueo_torn.preprog_index].var);
      preprog_display();
    }
    else
    {
      // guarda a e2prom

      // acaba
      bloqueo_torn.events_preprog = 1;  // indicador hi ha preprogramacio
      fin_cods();
    }
  }
  
else if(tecla_num == 5)
  {
   if(bloqueo_torn.preprog_index)
   {
     bloqueo_torn.preprog_index--;
     validar_hhmm(taula_preprog[bloqueo_torn.preprog_index].var);
     preprog_display();
   }
  }
 
}

extern void enviar_resposta_pin(void);

void INI_TOTS(void)
{
	if((tx30.estat == E_LIBRE) && (tx30.pin_subestat == 1))
	{
		tx30.pin_status_resp = '2';           
		enviar_resposta_pin();
		paso_a_libre(0);
		return;
	}
	if(tx30.estat == E_CERRADO)
	{
		if(tx30.subestat_torn == TORN_ESPERA_TANCAR)
		{
			// inicia PAUSA si autoritzada per configuracio
			if(configs_desde_bg40.hi_ha_pausa_activa) 
			{
				// inicia pausa activa
				timer50 = 0;
				pausa.estat = EPAUSA_temporitza_pre_pausa_activa;
				pausa.segons = PAUSA_SEGONS_PREPAUSA;
				MASK_GEN |= MSK_PAUSA;
				paso_a_libre(0);
			}
			return;
		}
		if(tx30.subestat_torn != TORN_TANCAT)
			return;
	}
	if((tx30.estat==E_OFF) || (tx30.estat == E_CERRADO))
        {
			if(tarcom.OP_VISUTOTSOFF==0)
				return;
			// td-tx PCON=0;    /* GF0 =0    */ 
			REFRESH_ON();
			display(0,NF_LEDEST,(void *)&tarcom.vars_estat[E_LIBRE].ledest);
        }
		else if(tarcom.torn_continu && tx30.acum_off && (tx30.segons_pot_anular_pas_off == 0)) 
			return;
		tx30.estat_retorn_reltots=tx30.estat;
		tx30.subestat=0;
		tx30.new_estat=E_TOTS;
		calcul_tots_aux();
		//memset(RAM_DISP_FLASH,0xff,SIZE_RAM_DISP);
		DISP_FIXE(DISP_LEDEST);
		DISP_FIXE(DISP_IMPORT);
		DISP_FIXE(DISP_EXTRES);
		timer50=1;    /* per saltar inmediatament a primer tot.  */
		APAGAR_DISPLAY(2);
}

void inici_test_vls(char opcio);

void OFF_LIBRE(void)
{
int ret;
unsigned char  bufd[6];
 if(tarcom.hi_ha_td30 && (MASK_1 & MSK_PASOFLI))
   {
    //tx30.new_estat=E_OFF;  2.07
    paso_off();
    return;
   }
 REFRESH_ON();
 if(tx30.pendent_test_displ_pas_a_on == 1)
 {
	 tx30.pendent_test_displ_pas_a_on = 2;
	 tx30.new_estat = E_TEST;
	 inici_test_vls(0);
	 return;
 }
 if(tx30.pendent_password_pas_a_on)
  {
      tx30.estat_retorn_cods=E_OFF;
      DISP_FIXE(DISP_LEDEST);
      DISP_FIXE(DISP_IMPORT);
      DISP_FIXE(DISP_EXTRES);
      timer50 = 0;
      timer100 = 0;
      
     leer_verif_cods();   /* llegir codigs secrets eprom_tax  */    
     num_cods=0;
     if(cods_programat(CODS_OFFxON))
       num_cods = CODS_OFFxON;

     if(num_cods==0 || hay_cargador())  // cas no hi ha password de OFF a LLIURE
       {
        tx30.pendent_password_pas_a_on = 0; 
       }
     else
       {
        APAGAR_DISPLAY(DISP_LEDEST);
        APAGAR_DISPLAY(DISP_EXTRES);
        memset(bufd,0,6);
        bufd[num_cods]=SS_DP;
        display(1,NF_TEXT6,bufd);   // decimal point
        DISP_DIG_FLASH(DISP_IMPORT,0);
      
        tx30.subestat=2;
        timer100=tarcom.t_cods;
        tx30.new_estat=E_CODS;
        tx30.cods_bloc_visu = 0;
        return;
       }
    }
   
  ret = OFF_LIBRE_bloqueig_torn();
  if(ret)
      return;
//  tx30.estat_pet=EPET_LIBRE;
  
// canvia de clau  
  if(multiplex.taula_encrip != NULL)
      RC4_init();

  paso_a_libre(1);  // es desde OFF
  return;
}

void LLUM(void)
{
   if(++tx30.nivell_llum_minim > 7)
      tx30.nivell_llum_minim = tarcom.nivell_llum_minim & 0x07;
}


void APAGAR_LLUMINOS(void)
{
char buf_luz[2];
  if(tarcom.pot_apagar_lluminos)
  {
    if(tx30.lluminos_apagat)
    {
      tx30.lluminos_apagat = 0;
      *DISP[0].POS_RAM &= ~0x80;  // treu punt (.) led estat

    }
    else
    {
      tx30.lluminos_apagat = 1;
      *DISP[0].POS_RAM |= 0x80;  // posa punt (.) led estat
    }
     select_luz(E_LIBRE,buf_luz);
     out_luzex(buf_luz[0],buf_luz[1]);
     display_to_chip(0);  // per refrescar punt (.) led estat   
  }

}


void test_impre(void)
{
char st;
char buf_luz[2];
    if(hay_impresora() && tarcom.OP_error_impre)
    {												
        st = get_status_impre();
        if(st & (IMPRE_SINPAPEL |  IMPRE_FALLO | IMPRE_FALLO_TRANSM))
        {
           if(!error_impre_en_curs_a_display && !lap_on)  
             {
               DISPLAY_ERROR_PLUS_LAP(ERR_IMPRE);
               error_impre_en_curs_a_display=1;
             }
           if(tarcom.bloq_libre_impre && !error_impre_en_curs_a_lumin)		  // !!!E10
                   {
                   error_impre_en_curs_a_lumin=1;
                   select_luz(tx30.estat,buf_luz);
                   out_luzex(buf_luz[0],buf_luz[1]);
                   }
        }
        else
        {
            // restaura visu libre
            if(error_impre_en_curs_a_display)
            {
                error_impre_en_curs_a_display=0;
                FIN_LAPSUS();
            }
            if(error_impre_en_curs_a_lumin)		  // !!!E10
            {
               error_impre_en_curs_a_lumin=0;
               select_luz(tx30.estat,buf_luz);
               out_luzex(buf_luz[0],buf_luz[1]);
            }
        }
        timer_2=12;   // per estat_impre cada 1.2"
    }
    else
      timer_2=0;   // per que ja no ho miri mes
}

void paso_a_ocupado(unsigned char no_ini)
{
unsigned char  tar;
struct s_serv* p_serv;

    p_serv = &serv[0];    // el pas a ocupat es desde lliure i per tant sempre es passatger 0
    tx30.new_estat=E_OCUPADO;   //2.07
    timer50=0;
    if(!no_ini)
       inicialitzar_servei_global();

    switch_tarsup=SW_TAR;
    RESET_LAPSUS();
    LEDS_FRONTAL_FIXE();
    tx30.estat=E_OCUPADO;

    tecla_tarifa = tecla_num;
    tar=tarifa_canvi_autom(tecla_num,AUT_ALL_INICIO, p_serv);
	p_serv->branca_canvi=tarifa[tar].branca;
	if (tecla_num == tarcom.tec_preu_acordat - 1) {
		servei.esPrecioAcordado = 2;
		p_serv->import = p_serv->bb = precio_acordado;
	} else if(tarifa[tar].bb_manual!=1){	//la baixada de bandera es sempre autom�tica o ho es al pasar de lliure a ocupat
		p_serv->import = p_serv->bb = tarifa[tar].bb;
		p_serv->bb_num_tarifa = tar;
		p_serv->bb_ja_aplicada = 1;
	} else {
		p_serv->import = p_serv->bb = 0;
		p_serv->bb_ja_aplicada = 0;
		if(tarifa[tar].prim_salt_man)
			tarifador_noFaPrimerSalt(p_serv);
	}
	hay_precio_acordado = 0;
    // eliminat a versio 1.04  servei.per_tarifa[viaj][tar].import=servei.bb;
    calcul_toptar_import(tar,p_serv->bb, p_serv);
    cambio_tarifa(tar,E_OCUPADO, p_serv);
    tarifador_start(p_serv);   // td-tx  TARIFANDO=1;
    
    if(p_serv->MODO_VF == 3)
      tx30.refresc_leds_ho_km = 1;  // per que ho actualitzi encara que no canvii velocitat
    zapper_activar();   
    
    if(servei.mask_vd)
        display(1,NF_IMPORT,p_import);  /* cas no actiu vd */
    if((tarcom.hay_precio_fijo)&&(p_serv->tarifa==tarcom.tar_precio_fijo))
      {
       servei.es_precio_fijo=1;
       servei.segs_precio_fijo=20;
       display(1,NF_I_T,p_import);  /* cas no actiu vd */ 
       display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
       DISP_FLASH(2);
      }
	
}

#define MODE_PASOCUP_OCUP  1
#define MODE_PASOCUP_ROT   2
#define MODE_PASOCUP_UP    3

unsigned char   save_tecla_num;

#define MASK_BLOC5KM_LIB_OCUP    0x02
#define MASK_BLOC5KM_OCUP_PAGAR  0x01


void libre_ocup_rot_up(char mode)
{
char st;
unsigned char  tar;

	if (hay_precio_acordado) {
		if (tecla_num != (tarcom.tec_preu_acordat - 1)){
			//showError(ERR_PRECIO_ACORDADO);
			return;
		}
	} else {
		if (tecla_num == (tarcom.tec_preu_acordat - 1)){
			//showError(ERR_NO_PRECIO_ACORDADO);
			return;
		}
	}
 if((tarcom.OP_bloc5Km & MASK_BLOC5KM_LIB_OCUP) &&(velocidad>5L))
   return;
 if(!tarifa_ok())
    {
     tx30.estat=E_MEM_MAL;
     paso_a_libre(0);
     return;
    }
 if(luminoso.serie_pendent_test_desde_pas_a_lliure)
   return;
 if(bloq_fallos_hard())
    {
     return;
    }
 
 if(zapper_bloqueja_equip())
 {
  DISPLAY_ERROR_PLUS_LAP(ERR_ZAPPER);
  return;
 }
 if(bloqueo_paso_ocup())
    {
     display(1,NF_TEXT6,&tarcom.text_error[ERR_TORN][0]);
     return;
    }
 
// tx30.estat_pet=EPET_OCUP;

 if(tarcom.hi_ha_td30&&(MASK_1 & MSK_PASLIOC))
   return;
 if(tarcom.bloq_ocup_impre == 1)
 {
	 st = get_status_impre();
         if(st & (IMPRE_SINPAPEL |  IMPRE_FALLO | IMPRE_FALLO_TRANSM))
            return;
 }
 
 if(tx30.lluminos_apagat)   // v 1.11
   APAGAR_LLUMINOS();   // encen lluminos  i apaga punt (.)

  switch(mode)
    {
     case MODE_PASOCUP_OCUP:
       if(tarcom.OP_RELOJ_LIBRE==0)
         {
          tecla_num=save_tecla_num;
          //tx30.new_estat=E_OCUPADO;   //2.07
          paso_a_ocupado(0);
         }
       else
         {
          // caso modo austria
          inicialitzar_servei_global();
          tecla_tarifa=save_tecla_num;
          tar=tarifa_canvi_autom(tecla_tarifa,AUT_ALL_INICIO, &serv[0]);
          serv[0].tarifa=tar;
          LEDS_FRONTAL(tarifa[tar].lf_oc);
          display(0,NF_LEDTAR_INTERM,(void *)tarifa[tar].ledocup);
          out_luzex(tarifa[tar].luzocup[sext_luz],tarifa[tar].luzocup[sext_luz+4]);
          timer50=tarcom.tiup_reloj_libre;
          timer100=0;
          MASK_GEN|=MSK_REL_LIB; // per inhibir tecles up/dwn
          tx30.new_estat=E_UP;
         }   
       break;
     case MODE_PASOCUP_ROT:
       tecla_num=tx30.tarifa_rot;
       //tx30.new_estat=E_OCUPADO;
       paso_a_ocupado(0);
       break;
     case MODE_PASOCUP_UP:
       inicialitzar_servei_global();
       tecla_tarifa=0;
       tar=tarifa_canvi_autom(0,AUT_ALL_INICIO, &serv[0]);
       serv[0].tarifa=tar;
       display(0,NF_LEDTAR_INTERM,tarifa[tar].ledocup);
       APAGAR_DISPLAY(1);
       APAGAR_DISPLAY(2);
       timer50=tarcom.tiup;
       MASK_GEN&= ~MSK_REL_LIB; // per activar tecles up/dwn
       tx30.new_estat=E_UP;
       break;
     default:
       break;
    }
	askCoordenadas(0);
}

const char TEXT_PIN[4] = {0x73,0x04,0x54,0};

void PETICIO_PIN(void)
{
long laux;
  tx30.pin_subestat = 1;
  display(2,NF_TEXT4,(void*)TEXT_PIN);
  memset(tx30.pin_value,0,sizeof(tx30.pin_value));
  laux = 0;
  display(1,NF_TOT4,&laux);
  timer50 = 0;        // per finalitzar si hi havia espera pas OFF  
  MASK_GEN |= MSK_PIN_ENTRAR;   // per bloquejar transicions 
  
}


#define TECLA_CONTROL_CHILE 3

void LIBRE_OCUP(void)
{
char es_control = 0;
unsigned char i;
long laux;
  if(tx30.pin_subestat == 1)
  {
     // cas s'esta entrant pin
     if(tecla_num <4)
       {
        tx30.pin_value[tecla_num]++;
        if(tx30.pin_value[tecla_num]>9)
          tx30.pin_value[tecla_num]=0;
        for(i=0,laux=0L;i<4;i++)
          laux = laux*10 + tx30.pin_value[i];
        // actualitza display pin
        display(1,NF_TOT4,&laux);  
       }
     return;
  }
  if(tarcom.control_chile &&(tecla_num == TECLA_CONTROL_CHILE))
  {
    // sustitueix tecla 4 (opcio control) per tecla 1
    // i posa opcio control
    tecla_num = 0;
    es_control = 1;
  }
 if((tecla_num>=tarcom.ntars)       //!!!!*****
    || !pas_lioc_permes(tecla_num)
    ||(tarcom.OP_MULTIT&&(tecla_num>3))
    ||luminoso.cnt_dec_segon_desde_pas_a_lliure
      // cas Fran�a , si ja han passat tarcom.segs_anular_pas_off segons, 
      // no permet cancelar pas a Off i passar a ocupat
    ||(tarcom.torn_continu && tx30.acum_off && (tx30.segons_pot_anular_pas_off == 0)))
   {
    return;
   }
 save_tecla_num=tecla_num; 
 servcom.es_control = es_control;
 libre_ocup_rot_up(MODE_PASOCUP_OCUP);
}

void LIBRE_ROT(void)
{
 libre_ocup_rot_up(MODE_PASOCUP_ROT);
}

void LIBRE_UP(void)
{
 libre_ocup_rot_up(MODE_PASOCUP_UP);
}

void LIB_ROT_UP(void)
{
unsigned char  tar;
 tar=tx30.tarifa_rot+1;
 if((tar <tarcom.ntars) && pas_lioc_permes(tar))  //!!!!*******
    tx30.tarifa_rot=tar;
 else
    tx30.tarifa_rot=0;
 display(0,NF_LEDTAR_INTERM,tarifa[tx30.tarifa_rot].ledocup);
}

void LIB_ROT_DWN(void)
{
 if(tx30.tarifa_rot)
    --tx30.tarifa_rot;
 else
    {
                               //!!!!*******
     while((tx30.tarifa_rot <tarcom.ntars) && pas_lioc_permes(tx30.tarifa_rot))
       ++tx30.tarifa_rot;
     --tx30.tarifa_rot;
    }
 display(0,NF_LEDTAR_INTERM,tarifa[tx30.tarifa_rot].ledocup);
}

void UP_UP(void)
{
unsigned char  tar;
 tecla_num=tecla_tarifa+1;
             //!!!!*******
 if((tecla_num <tarcom.ntars) && pas_lioc_permes(tecla_num))
   {
    tecla_tarifa=tecla_num;
    tar=tarifa_canvi_autom(tecla_num,AUT_ALL_INICIO, &serv[0]);
    serv[0].tarifa=tar;
    display(0,NF_LEDTAR_INTERM,tarifa[tar].ledocup);
    timer50=tarcom.tiup;
   }
}

void UP_DWN(void)
{
unsigned char  tar;
 if(tecla_tarifa>0)
   {
    tecla_num=tecla_tarifa-1;
                   //!!!!*******
    if((tecla_num <tarcom.ntars) && pas_lioc_permes(tecla_num))
        {
         tecla_tarifa=tecla_num;
         tar=tarifa_canvi_autom(tecla_num,AUT_ALL_INICIO, &serv[0]);
         serv[0].tarifa=tar;
         display(0,NF_LEDTAR_INTERM,tarifa[tar].ledocup);
         timer50=tarcom.tiup;
        }
   }
}



void UP_OCUPADO(void)
{
 tecla_num=tecla_tarifa;
 paso_a_ocupado(1);
}

// pas de lliure a ocupat per metres amb passatger 
void LIBREOFF_OCAUT(void)  
{
 if(!tarifa_ok())
    {
     tx30.estat=E_MEM_MAL;
     paso_a_libre(0);
     return;
    }
 if(zapper_bloqueja_equip())
 {
  DISPLAY_ERROR_PLUS_LAP(ERR_ZAPPER);
  return;
 }
 
  if(luminoso.serie_pendent_test_desde_pas_a_lliure)
   return;      // encara no ha pogut actualitzar test lluminos

 if(bloq_fallos_hard())
  {
   //tx30.new_estat=tx30.estat;  2.07
   return;
  }

   if(bloqueo_paso_ocup())
    {
     display(1,NF_TEXT6,&tarcom.text_error[ERR_TORN][0]);
     //new_estat=estat;  2.07
     return;
    }

 if(tx30.lluminos_apagat)   // v 1.11
   APAGAR_LLUMINOS();   // encen lluminos  i apaga punt (.)
 
  tecla_num=tarcom.tar_liocaut;
     // td-tx if(tx30.estat==E_OFF)
     // td-tx     PCON=0;    /* GF0 =0    */
  
  REFRESH_ON();
  paso_a_ocupado(0);
}

void OCUPADO_0(void)
{
unsigned char  tar;
int ii;

 if(exces_fallo_t)
   {
    fin_servicio(1);   // abort
   }
 else
   {
    for(ii=0; ii<MAX_PSGRS ; ii++)
    {
     if(servcom.viaj_activo[ii])
     {
       tar=tarifa_canvi_autom(serv[ii].tarifa,AUT_ALL, &serv[ii]);
       cambio_tarifa(tar,E_OCUPADO, &serv[ii]);
       tarifador_start(&serv[ii]);   // td-tx  TARIFANDO=1;
     }
    }

     zapper_activar();       
     
     if(tarcom.OP_MULTIT &&(servcom.n_viajs>1))
         timer100=tarcom.tiviaj;
         
     if(servei.es_precio_fijo)
       {
        display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
        if((serv[0].hm_oc>=tarcom.hm_precio_fijo) || (servei.segs_precio_fijo==0))
          {
           DISP_FIXE(2);
          }
        else
          {
           display(1,NF_I_T,(void *)&serv[0].import);
           DISP_FLASH(2);
          }   
       }  
         
   }
}


void gestio_precio_fijo(void)
{
unsigned char i;
 if((tecla_num <4)&&
    (serv[0].hm_oc<tarcom.hm_precio_fijo) &&
    servei.segs_precio_fijo)
   {
    servei.segs_precio_fijo=20;
    servei.buf_precio_fijo[3-tecla_num]++;
    if(servei.buf_precio_fijo[3-tecla_num]>9)
      servei.buf_precio_fijo[3-tecla_num]=0;
    for(i=0,serv[0].import=0L;i<4;i++)
      serv[0].import=(serv[0].import+servei.buf_precio_fijo[3-i])*10L;
    // actualitza display import

    display(1,NF_I_T,(void *)&serv[0].import);
   }
}

void paso_a_pagar(void);

void OCUP_OCUP(void)
{
unsigned char  tar;
unsigned char  aux;
struct s_serv* p_serv;
int ii;

if(switch_tarsup ==SW_TAR)
   {
    if(tarcom.OP_MULTIT &&(tecla_num>3))
      {
       if((tecla_num > (MAX_PSGRS +3)) ||
          servcom.es_control)  // cas control chile nomes 1 passatger
         return;
       viaj=tecla_num-4;
       p_import=&serv[viaj].import;
       p_supl=&serv[viaj].supl;
       if(!servcom.viaj_activo[viaj])
          {
           p_serv = &serv[viaj];
           servcom.viaj_activo[viaj]=1;
           
           servcom.visu_led_viaj=1; /* indicador per led num. viatger */
           servcom.viaj_per_llums = viaj;
           
           inicialitzar_servei(p_serv);
               
          tar=tarifa_canvi_autom(tecla_tarifa,AUT_ALL_INICIO, p_serv);
          p_serv->bb = tarifa[tar].bb;
          p_serv->import = p_serv->bb;
          // eliminat a versio 1.04  servei.per_tarifa[viaj][tar].import=servei.bb;
          calcul_toptar_import(tar,p_serv->bb, p_serv);
          cambio_tarifa(tar,E_OCUPADO, p_serv);
          tarifador_start(p_serv);   // td-tx  TARIFANDO=1;

          }
       if(servcom.visu_led_viaj)
         {
          //*((char*)&ledocpag+1)=SS[viaj+1];
          ledocpag[0] = (ledocpag[0] & 0x00ff)|((short)SS[viaj+1]<<8);
 
          display(0,NF_LEDEST,(void *)ledocpag);
         }
        if(servcom.n_viajs>1)
          timer100=tarcom.tiviaj;  /* per anar canviant de viatger */
        else
          timer100=0;
        if(servei.mask_vd==0)
          {
           aux=servei.vd[gt.mitad_vd];
           DISPLAY_VD1(aux);     /* display 1 */
           DISPLAY_VD2(aux & 0x0f);  /* display 2 */
          }
        else
          {    
           display(1,NF_IMPORT,p_import);
           if((serv[viaj].supl)&& (tarcom.supl_solo_fin_serv!=1))
              display(2,NF_SUPL,(void *)p_supl);
           else
              APAGAR_DISPLAY(2);
          }
      }
    else                                
      {
       if(servei.es_precio_fijo)
         {   
          gestio_precio_fijo();
         }
         
       else if(tarcom.tipus_teclat==0)
         {
           if(servcom.es_control && (tecla_num == TECLA_CONTROL_CHILE))
           {
             // cas final servei per tecla control
              if(tarcom.OP_MULTIT)
                 timer100=0;
              paso_a_pagar();             
           }
           else if((tecla_num <tarcom.ntars) &&pas_ococ_permes(tecla_num,serv[viaj].tarifa))
           {
              tecla_tarifa=tecla_num;
              for(ii=0; ii<MAX_PSGRS ; ii++)
              {
               if(servcom.viaj_activo[ii])
                 {
                    tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[ii]);
                    if(tar != serv[ii].tarifa)
                      {
						if(tarifa[tar].bb_manual==3){		//baixada de bandera sustitutiva
							if(esPrimerSalt(&serv[ii])){
								serv[ii].bb = tarifa[tar].bb;
								serv[ii].import = tarifa[tar].bb;
							}
						} else if((serv[ii].bb_ja_aplicada == 0) && (tarifa[tar].bb_manual==0)){	//baixada de bandera automatica
							serv[ii].bb = tarifa[tar].bb;					//Encara no l'havia fet, la fem ara
							serv[ii].bb_num_tarifa = tar;
							serv[ii].bb_ja_aplicada = 1;
							serv[ii].import += serv[ii].bb;
							if(tarifa[tar].prim_salt_man){					//el primer salt es manual
								tarifador_stop_and_reset(&serv[ii]);		//es fa al baixar bandera
								tarifador_start(&serv[ii]);
							}
						}
                        cambio_tarifa(tar,E_OCUPADO, &serv[ii]);
					    display_1_2();
                      }
                 }
              }
           }
         }
      }
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
    if(servcom.n_viajs>1)
       timer100=tarcom.tiviaj;  // reinicia temps visu de viatger actiu

   }
}

void TARARI(void)
{
 if((tecla_num <tarcom.ntars) && tarcom.newtransm_esp)
    {
     tecla_tarifa=tecla_num;
     var_tarari=1;
     if(tecla_tarifa != serv[0].tarifa)
       {
        cambio_tarifa(tecla_tarifa,tx30.estat, &serv[0]);
       } 
    }
}

const unsigned short  ledprecacord[2]={0x7773, 0x0000};

void PREC_ACORDADO(void){
    hay_precio_acordado = 1;
}

// canvi visualitzacio viatger per temps
void ROT_VIAJ(void)
{
unsigned char  aux;
 if(servcom.n_viajs>1)
   {
    do{if(++viaj>(MAX_PSGRS-1))viaj=0;}while(!servcom.viaj_activo[viaj]);
       p_import=&serv[viaj].import;
       p_supl=&serv[viaj].supl;
          //*((char*)&ledocpag+1)=SS[viaj+1];
          ledocpag[0] = (ledocpag[0] & 0x00ff)|((short)SS[viaj+1]<<8);
          display(0,NF_LEDEST,(void *)ledocpag);
        if(servei.mask_vd==0)
          {
           aux=servei.vd[gt.mitad_vd];
           DISPLAY_VD1(aux);     /* display 1 */
           DISPLAY_VD2(aux & 0x0f);  /* display 2 */
          }
        else
          {
           display(1,NF_IMPORT,p_import);
           if((serv[viaj].supl)&& (tarcom.supl_solo_fin_serv!=1))
              display(2,NF_SUPL,(void *)p_supl);
           else
              APAGAR_DISPLAY(2);
          }
    timer100=tarcom.tiviaj;
   }
 else
    timer100=0;
}




void paso_a_pagar(void)
{
struct s_serv* p_serv;

  p_serv = &serv[viaj];
  
 aplicar_corsa_minima_si_toca(0, p_serv);   // (0) no es pas a fi servei                        
 servei.m_paglib=0;
 servei.m_fslib = 0;
 if(p_serv->fer_pcaut)
     p_serv->fer_pcaut=2;
 cambio_tarifa(p_serv->tarifa, E_PAGAR, p_serv);
 if(tarcom.tip_paglib==1)
     timer100=tarcom.tipaglib;
 if(tarcom.tiposupl==SUP_OCUP || lap_on)
   {
    LEDS_FRONTAL_FIXE();
    switch_tarsup=SW_TAR;
    if(exces_vel)
       {
        exces_vel=0;
        p_serv->CUENTA_K = p_serv->NEW_CUENTA_K;
       }
    FIN_LAPSUS();
   }
 tx30.new_estat=E_PAGAR;

 if(tarcom.suma_autom)
	 p_serv->suma_en_curs = 1;   //  v 2.14 pseudoevent=V_TSUMA;
 if((tarcom.tip_paglib==2) && ((tarcom.paglib_autom) || servcom.es_control))
	 pseudoevent=V_TLIBRE;
 servei.estat_euro=0;
 if(servei.es_precio_fijo)
 {
	 servei.segs_precio_fijo=0;   //per que ja no es pugui modificar
	 // encara que es tornes a ocupat
	 display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
	 DISP_FIXE(2);
 }
 
 // valor inicial tipus iva (despres es pot canviar desde teclat )
 servei.tipus_iva = tarifa[p_serv->tarifa].tipus_iva;
 MASK_GEN &= ~MSK_TIPUS_IVA;
 display_1_2();
 
}

          
void OCUP_PAGAR(void)
{
 if(servcom.es_control)
   return;   // cas control chile nomes acaba amb tecla 4
 if(pas_ocap_permes(serv[viaj].tarifa)&&(!(tarcom.OP_bloc5Km & MASK_BLOC5KM_OCUP_PAGAR) ||(velocidad<=5L)))
   {
    if(tarcom.OP_MULTIT)
       timer100=0;
    paso_a_pagar();
   }
}

void OCUP_UP(void)
{
unsigned char  tar;

if(servei.es_precio_fijo)
  {
   gestio_precio_fijo();
   return;
  }
if(switch_tarsup == SW_TAR)
   {                       
    tecla_num=serv[0].tarifa+1;
                   //!!!!*******
    if((tecla_num <tarcom.ntars) &&pas_ococ_permes(tecla_num,serv[0].tarifa))
      {
       tecla_tarifa=tecla_num;
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
       if(tar != serv[0].tarifa)
         {
          cambio_tarifa(tar,E_OCUPADO, &serv[0]);
         }
      }
    else  // cas pasa a A PAGAR 
      {
       if(!(tarcom.OP_bloc5Km & MASK_BLOC5KM_OCUP_PAGAR) ||(velocidad<=5L))
          paso_a_pagar();
      }
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}


unsigned char next_rot(unsigned char t)
{
unsigned char  tar;
 tar=t;
 do
    {
     tar++;
     if(tar==tarcom.ntars)
        tar=0;
     if(pas_ococ_permes(tar,t))
        return(tar);
    }while(tar!=t);
 return(t);
}

unsigned char ant_rot(unsigned char t)
{
unsigned char  tar;
 tar=t;
 do
    {
     if(tar==0)
            //!!!!*******
        tar=tarcom.ntars;
     --tar;
     if(pas_ococ_permes(tar,t))
        return(tar);
    }while(tar!=t);
 return(t);
}

void OCUP_UPROT(void)
{
unsigned char  tar;
if(switch_tarsup ==SW_TAR)
   {
    tecla_num=next_rot(serv[0].tarifa);
    if(tecla_num != serv[0].tarifa)
      {
       tecla_tarifa=tecla_num;
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
       if(tar != serv[0].tarifa)
         {
          cambio_tarifa(tar,E_OCUPADO, &serv[0]);
         }
      }
    //else  // cas pasa a A PAGAR 
    //   tx30.new_estat=E_OCUPADO;
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}


void OCUP_DWNROT(void)
{
unsigned char  tar;
if(switch_tarsup ==SW_TAR)
   {
    tecla_num=ant_rot(serv[0].tarifa);
    if(tecla_num != serv[0].tarifa)
      {
       tecla_tarifa=tecla_num;
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
       if(tar != serv[0].tarifa)
         {
          cambio_tarifa(tar,E_OCUPADO, &serv[0]);
         }
      }
    //else  // cas pasa a A PAGAR 
    //   tx30.new_estat=E_OCUPADO;
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}

void OCUP_PAGROT(void)
{
if(switch_tarsup ==SW_TAR)
   {
    if(pas_ocap_permes(serv[0].tarifa)&&(!(tarcom.OP_bloc5Km & MASK_BLOC5KM_OCUP_PAGAR) ||(velocidad<=5L)))
      {
       paso_a_pagar();
      }
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}



void OCUP_DWN(void)
{
unsigned char  tar;
if(servei.es_precio_fijo)
  {
   gestio_precio_fijo();
   return;
  }

if(switch_tarsup ==SW_TAR)
   {
    if(serv[0].tarifa==0)
      return;
    tecla_num=serv[0].tarifa-1;
    if(pas_ococ_permes(tecla_num,serv[0].tarifa))
      {
       tecla_tarifa=tecla_num;
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
       if(tar != serv[0].tarifa)
         {
          cambio_tarifa(tar,E_OCUPADO, &serv[0]);
         }
      }
   }
 else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}


void cancelar_visu_euro(void)
{
unsigned char  aux;

 if(servei.estat_euro)
   {   
    servei.estat_euro=0;
    timer100=0;
    servei.mask_vd=servei.mask_vd_sav;
    if(servei.mask_vd==0)
      {
        aux=servei.vd[gt.mitad_vd];
        DISPLAY_VD1(aux);     /* display 1 */
        if(servei.es_precio_fijo)
           {
            display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
            DISP_FIXE(2);
           } 
        else
          DISPLAY_VD2(aux &0x0f);  /* display 2 */
      }
    else  
      display_1_2();
	servcom.save_lf_pag = tarifa[serv[viaj].tarifa].lf_pag;
    LEDS_FRONTAL(servcom.save_lf_pag);  // verificar !!!! es diferent de tx38
    if(tarcom.tip_paglib==1)
        timer100=tarcom.tipaglib;
   }     
}

void PAGAR_OCUP(void)
{
unsigned char  tar;
cancelar_visu_euro();
	
	if(MASK_GEN & MSK_TIPUS_IVA)
	{
		if(tecla_num>2)
			return;
		servei.tipus_iva = tecla_num;
		display_1_2();
		if(tarcom.tipsum==2)
            timer50 = (tarcom.tisum_s == 0) ? tarcom.tisum : tarcom.tisum_s * 20; 
		return;
	}
    if(switch_tarsup ==SW_TAR)
   {
    if(!serv[viaj].suma_en_curs&&(tarcom.tipus_teclat==0) && pas_apoc_permes(tecla_num,serv[viaj].tarifa)) /* no si suma */
      {
       tx30.new_estat=tx30.estat=E_OCUPADO;
       tecla_tarifa=tecla_num;
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[viaj]);
       cambio_tarifa(tar,E_OCUPADO, &serv[viaj]);
         if(servei.es_precio_fijo)
           {
            display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
            DISP_FIXE(2);
           }  
       if(servcom.n_viajs>1)
          timer100=tarcom.tiviaj;
      }
   }
    else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
   }
}

void PAGAR_OCUP_MF(void)
{
unsigned char  tar;

 cancelar_visu_euro();
//
//**************************************
// tecla_num=servei.tarifa;      // versio 2.17
// tecla_tarifa=tecla_num;      // versio 2.17
//
//**************************************
 tecla_num=tecla_tarifa;       // versio 2.17  
 tx30.new_estat=tx30.estat=E_OCUPADO;   
 tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
 cambio_tarifa(tar,E_OCUPADO, &serv[0]);
     if(servei.es_precio_fijo)
       {
        display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
        DISP_FIXE(2);
       }  
}

void PAGAR_DWN(void)
{
unsigned char  tar;

cancelar_visu_euro();

if(switch_tarsup ==SW_TAR)
   {
    //tecla_num=servei.tarifa; // version 2.17
    //if(!servei.suma_en_curs && pas_apoc_permes(tecla_num,servei.tarifa))// version 2.17
    tecla_num=tecla_tarifa;   // version 2.17
    if(!serv[0].suma_en_curs && pas_apoc_permes(serv[0].tarifa,serv[0].tarifa)) // version 2.17
      {
       tx30.estat=tx30.new_estat=E_OCUPADO;   // version 2.17
       //tecla_tarifa=tecla_num;  // version 2.17
       tar=tarifa_canvi_autom(tecla_num,AUT_ALL, &serv[0]);
       cambio_tarifa(tar,E_OCUPADO, &serv[0]);
         if(servei.es_precio_fijo)
           {
            display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
            DISP_FIXE(2);
           }  
      }
    else
       tx30.new_estat=tx30.estat;
   }
else
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
    tx30.new_estat=tx30.estat;
   }
}

void PAGAR_0(void)
{
unsigned char  tar;
int ii;

 timer50=0;   /* per acabar tempor si FEIA SUMA */
 serv[viaj].suma_en_curs=0;
 servei.estat_euro=0;
 if(tarcom.tip_paglib==1)
    timer100=tarcom.tipaglib;

 if(exces_fallo_t)
   {
    fin_servicio(1);   // abort
   }
 else
   {    
// v 2.14    if(!(MASK_GEN & MSK_PAGFS))
// v 2.14       {
        for(ii=0; ii<MAX_PSGRS ; ii++)
        {
         if(servcom.viaj_activo[ii])
         {
            tar = tarifa_canvi_autom(serv[ii].tarifa,AUT_ALL, &serv[ii]);
            if(ii == viaj)
              cambio_tarifa(tar,E_PAGAR, &serv[ii]);
            else
              cambio_tarifa(tar,E_OCUPADO, &serv[ii]);
            tarifador_start(&serv[ii]);   // td-tx  TARIFANDO=1;
         }
        }

        zapper_activar();       
        
// v 2.14       }
// v 2.14    else
// v 2.14       timer100=tarcom.tipaglib;
    display(1,NF_IMPORT,p_import);
    if((serv[viaj].supl)&& (tarcom.supl_solo_fin_serv!=1))
        display(2,NF_SUPL,(void *)p_supl);
        
     if(servei.es_precio_fijo)
       {
        display(2,NF_TEXT4,&tarcom.texto_precio_fijo[0]); 
        DISP_FIXE(2);
       }         
   }
}


void SUPL_TECLA(void)            
{
  
 cancelar_visu_euro();
              
 if(tarcom.sup_p_s)
   {
    // cas suplements secuencial
    MACR_SUPL(serv[viaj].tarifa,TECLA_SECUENCIAL);
    return;
   } 
 if(switch_tarsup==SW_SUP)
   {
    LEDS_FRONTAL_FIXE();
    switch_tarsup=SW_TAR;
    FIN_LAPSUS();
   }
else
   {
    if(servei.es_precio_fijo || servei.esPrecioAcordado)
      return;
    INI_LAPSUS();
    if(lap_on ||
       tarcom.tiposupl==SUP_ALL ||
       (tarcom.tiposupl==SUP_OCUP  && tx30.estat==E_OCUPADO) ||
       (tarcom.tiposupl==SUP_APAG  && tx30.estat==E_PAGAR))
      {
       LEDS_FRONTAL_FLASH(tarcom.lf_FLASH_LAPSO_SUPL);
       switch_tarsup=SW_SUP;
       if((tx30.estat==E_OCUPADO) &&(servcom.n_viajs>1))
          timer100=tarcom.tiviaj;
      }
   }
 //tx30.new_estat=tx30.estat;  2.07
}


void SUMA_SOLTAR(void)
{
	if(tarcom.tipsum == 3)
	{
		serv[viaj].suma_en_curs=0;
		display_1_2();
		timer50=0;   /* per acabar tempor si n'hi havia */
		if(tarcom.tip_paglib==1)
			timer100=tarcom.tipaglib;
	}
}

void SUMA(void)
{	
struct s_serv* p_serv;
	
	p_serv = &serv[viaj];
	cancelar_visu_euro();
	
	if(/*servei.supl[viaj] &&*/ tarcom.tipsum)  //   a v 2.14 comentat servei.supl[viaj]
	{
		if(!p_serv->suma_en_curs)
		{
				p_serv->suma_en_curs=1;
				if(configs_desde_bg40.select_iva_teclat)
				{
 				 MASK_GEN |= MSK_TIPUS_IVA;
				 
				}

			display_1_2();
			if(tarcom.tipsum==2)
                timer50 = (tarcom.tisum_s == 0) ? tarcom.tisum : tarcom.tisum_s * 20;
			if(tarcom.tip_paglib==1)
				timer100=0;           /* es reactivara al desglosar */
		}
		else 
		{
			timer50=0;   /* per acabar tempor si n'hi havia */
			p_serv->suma_en_curs=0;
			MASK_GEN &= ~MSK_TIPUS_IVA;
			LEDS_FRONTAL(servcom.save_lf_pag);  // restaura texte SUPL Euro
			display_1_2();
			if(tarcom.tip_paglib==1)
				timer100=tarcom.tipaglib;
		}
	}
}




void PAGAR_EURO(void)
{
 if(tarcom.visu_euro_pagar)
   {
    serv[viaj].suma_en_curs=0;
    timer50=0;   /* per acabar tempor si n'hi havia */
//    APAGAR_DISPLAY(2);
//    display(1,NF_TEXT6,&tarcom.TEXT_EURO[0]);
    timer100=1;               //per entrar inmediatament
    servei.estat_euro=1;
    servei.mask_vd_sav=servei.mask_vd;
    servei.mask_vd=2;
   }
}   


void ABORT_SUPL(void)
{
unsigned char  i;
struct s_serv* p_serv;

 cancelar_visu_euro();

 if(serv[viaj].supl && !serv[viaj].suma_en_curs && tarcom.abortsupl)
   {
    p_serv = &serv[viaj];
    p_serv->supl = p_serv->supl_aut;
    if(!tarcom.OP_PERCENT_DESC)
       p_serv->fet_pcsup=0;     /* cas descompte no el reseta */
    for(i=0;i<tarcom.ntars;i++)
      {
       p_serv->per_tarifa[i].n_supl=0;
       p_serv->per_tarifa[i].supl=0;
      }
    memset(&p_serv->supl_desgl[0],0,sizeof p_serv->supl_desgl);
    //if(servei.supl[viaj])
    //    display(2,NF_SUPL,p_supl);
    //else
    //    APAGAR_DISPLAY(2);
    display_1_2();
    last_supl=0L;
    //enviar_parlant(5);
   }
}

void pag_lib_tecla(void)
{
struct s_serv* p_serv;
char anul10seg;

 p_serv = &serv[viaj];
 cancelar_visu_euro();
 switch(tarcom.tip_paglib)
    {
     case 0:     // pas inmediat 
        if(!p_serv->suma_en_curs || tarcom.sumlib)
           {
            if(p_serv->suma_en_curs)
              {
               display(1,NF_IMPORT,p_import);
               display(2,NF_SUPL,(void *)p_supl);
               p_serv->suma_en_curs=0;
               //timer50=0;   // v2.18
              }
            timer50=0;    // v2.18
            timer100=0;    // v2.18
            fin_servicio(0);
           }
         else
           tx30.new_estat=E_PAGAR;
         break;
     case 1:      // pas despres de delay 
         if(!p_serv->suma_en_curs && !timer100)
         {
            timer50=0;    // v2.18
            fin_servicio(0);
         }
         else
           tx30.new_estat=E_PAGAR;
         break;
     case 2:     // cas pas automatic despres de temps 
         if(!p_serv->suma_en_curs || tarcom.sumlib || (tarcom.paglib_autom))
            {
             if(p_serv->suma_en_curs)
               {
                display(1,NF_IMPORT,p_import);
                display(2,NF_SUPL,(void *)p_supl);
                p_serv->suma_en_curs=0;
                //timer50=0;       // v2.18// per acabar tempor si n'hi havia 
               }
             timer50=0;  // v2.18 // per acabar tempor si n'hi havia 
// 2.14             aplicar_corsa_minima_si_toca(1);   // (1)  es pas a fi servei
             if(tarcom.display_suma_en_fi_servei)
             {
                p_serv->suma_en_curs = 1;
             }
// 2.14             timer100=tarcom.tipaglib;  // engega timer 
// 2.14             MASK_GEN|=MSK_PAGFS;
// 2.14             DISP_FLASH(0);
             MASK_GEN |= MSK_FSLIB;                       // 2.14
             anul10seg = fin_servicio(0);      // 2.14
             if(anul10seg)
             {
               MASK_GEN &= ~MSK_FSLIB;                      
               return;
             }
             timer100=tarcom.tipaglib;  // engega timer   // 2.14
             DISP_FLASH(0);
             servei.estat_euro=0;
             display_1_2();
// 2.14             LEDS_FRONTAL(tarifa[servei.tarifa].lf_pag);
            }
         else
           tx30.new_estat=E_PAGAR;
         break;
    }
}

void pag_lib_dist(void)
{
  pag_lib_tecla();
}

void pag_lib_temps(void)
{
 uchar mask;
 ulong laux1,laux2;
 if(servei.estat_euro)
   {
    mask=tarcom.visu_euro_pagar>>(servei.estat_euro-1);   
    do
      {
       if(mask&0x01) break;
       servei.estat_euro++;
       mask>>=1;
      }while(mask);
    if(mask==0)
      servei.estat_euro=255;   
    switch(servei.estat_euro)
      {
       case 1:
         APAGAR_DISPLAY(2);
         display(1,NF_TEXT6,&tarcom.TEXT_EURO[0]);
         LEDS_FRONTAL(tarcom.lf_euro1);
         timer100=15;
         servei.estat_euro=2;
         timer100=15;               //per entrar inmediatament
         break;
       case 2:     
         laux1= (ulong)(((float)serv[viaj].import)*tarcom.factor_alt+.5);
         laux2= (ulong)(((float)serv[viaj].supl)*tarcom.factor_alt+.5);
         display(1,NF_IMPORT_ALT,(unsigned char*)&laux1);
         display(2,NF_SUPL_ALT,(unsigned char*)&laux2);
         LEDS_FRONTAL(tarcom.lf_euro2);
         servei.estat_euro=3;
         timer100=50;  
         break;
       case 3: 
         laux1= (ulong)((((float)serv[viaj].import)+(float)serv[viaj].supl)*tarcom.factor_alt+.5); 
         display(1,NF_IMPORT_ALT,(unsigned char*)&laux1);
         display(2,NF_TEXT4,&tarcom.TEXT_EURO[0]);
         LEDS_FRONTAL(tarcom.lf_euro3);
         servei.estat_euro=4;
         timer100=50;  
         break;
       default:
         cancelar_visu_euro();   
         break;    
      }  
   }
 else
   { 
/* 2.14     // passat a FS_LIB_TEMPS
    if(tarcom.tip_paglib==2)
       {
        timer50=0;
        timer100=0;
        fin_servicio();
        MASK_GEN &= ~MSK_PAGFS;
       }
    else
*/
      {
       tx30.new_estat=E_PAGAR;
       timer100=0;
      }
   }   
}

void PAGAR_UP(void)
{
 cancelar_visu_euro();

 if(switch_tarsup==SW_SUP)
   {
    MACR_SUPL(tecla_num,TECLA_PARALELO);
    //tx30.new_estat=E_PAGAR;  2.07
    return;
   }
 tx30.new_estat=E_LIBRE;   //2.07
 pag_lib_tecla();
}

void ERR_GENIMP(void)
{
int ii;
 cancelar_visu_euro();

 if(tarcom.genimp_ocup)
  {
   if(!lap_on)
    {
      DISPLAY_ERROR_PLUS_LAP(ERR_GEN_POLSOS);
      totalitzador_incr(TOT_E3);
    }
   for(ii=0; ii<MAX_PSGRS; ii++)
    {
     if(servcom.viaj_activo[ii])
       {
         serv[ii].CUENTA_K=0;     
         if(tarcom.errgenimp_no_horaria)
           serv[ii].CUENTA_H=0;
       }
    }

  }
 else
  {
   totalitzador_incr(TOT_E3);
   fin_servicio(1);   // abort
  }
}
        
void FIN_ERR_GENIMP(void)
{
int ii;
 if (lap_on)
  {
   for(ii=0; ii<MAX_PSGRS; ii++)
    {
     if(servcom.viaj_activo[ii])
       {
         serv[ii].CUENTA_K = serv[ii].NEW_CUENTA_K;
         serv[ii].CUENTA_H = serv[ii].NEW_CUENTA_H;
       }
    }
   FIN_LAPSUS();
  }
}

// valors rebuts a traves multiplex
void guardar_valors_cobertura(char* buf)
{
  memcpy(tx30.cobertura_buffer, buf, 3);
  tx30.cobertura_rebuda = 1;
  tx30.cobertura_timeout = COBERTURA_SEGONS_TIMEOUT;
}

void VISU_MOD_RELOJ(void)
{
     // cas visu rellotge
     if(tarcom.OP_RELOJ_VISU)
       {
        // cas hi ha visu rellotge
        if( tx30.estat==E_OFF || tx30.estat==E_CERRADO)
           {
            if(tarcom.OP_VISURELOFF==0)
               return;
                 
            REFRESH_ON();
            // td-tx PCON=0;    /* GF0 =0    */
           }
        else if(tarcom.torn_continu && tx30.acum_off && (tx30.segons_pot_anular_pas_off == 0))
          return;
        tx30.estat_retorn_reltots=tx30.estat;
        if(tarcom.OP_RELOJ_PERM_METRES)
           MASK_GEN|=MSK_REL;       
        else
           MASK_GEN &= ~MSK_REL;
//        memset(RAM_DISP_FLASH,0xff,SIZE_RAM_DISP);
        tx30.new_estat=E_RELOJ;
        if(tarcom.visu_torn_en_lliure)
          {
            tx30.subestat = 6;
            display_torn_en_reloj();
          }
        else
          {
            tx30.subestat=0;
            display_hora_en_reloj();
          }
        if(tarcom.t_rel)
           timer100=tarcom.t_rel; /* engega tempor per canvi */
       }
}



void AVANPAP_INCREREL(void)
{    
 uchar aux;
struct s_time  time;

 switch(tx30.subestat)
   {
    case 3:
     // incrementa rellotge
     if(tx30.ajuste_rel_minuts<2)
       {
        get_reloj(RELLOTGE_PLACA,&time);
        if((time.minute==0x59) &&(time.hour==0x23))
          break;  // per no canviar de dia

        tx30.ajuste_rel_last_anydia=tx30.ajuste_rel_last_anydia_temp;    //v2.20b

        tx30.ajuste_rel_minuts++;  
        aux=bcd_bin(time.minute)+1;
        if(aux==60)
          {
           aux=0;
           time.hour=bin_bcd(bcd_bin(time.hour)+1);
          }
        time.minute=bin_bcd(aux);
        set_reloj(RELLOTGE_PLACA,&time);
        timer50=1;  // per refrescar inmediatament   
       }
     break;
    default:  
     if(hay_impresora() && !delay_impre)
        {
          if(tx30.subestat <= 2)
          {
            imprimir_ticket_avance_papel();
            delay_impre=40;   // 2"
          }
          else
          {
            // cas esta visualitzant registre events torn
            imprimir_ticket_events_torn();
            delay_impre=40;   // 2"
          }
        }
     break;
   }     
}

void fin_reltots(void)
{
 tx30.new_estat = tx30.estat_retorn_reltots;
 switch(tx30.estat_retorn_reltots)
   {
    case E_CERRADO:
      tx30.subestat_torn = TORN_TANCAT;
      //sense break;
    case E_OFF:
      timer50=0;
      paso_off();
      break;
    case E_LIBRE:
//    case E_MEM_MAL:    v1.04X_FRANCE
    default:
      paso_a_libre(0);
      break;
  }
}

void MODIF_DECREREL(void)
{     
uchar  aux;
unsigned int anydia;   // v2.20b
struct s_time  time;
 switch(tx30.subestat)
   {
    case 0: 
      // mira si pot pasar a modificacio rellotge
      if(!tarcom.ajuste_rel) 
        return;            
        
      get_reloj(RELLOTGE_PLACA,&time);
      anydia=dia_del_segle_from_bcd(&time.day);              
      if(anydia<(tx30.ajuste_rel_last_anydia+7))  
         return;                                    
      tx30.ajuste_rel_last_anydia_temp=anydia;       
      tx30.ajuste_rel_minuts=0;
      tx30.subestat=3;
      display(1,NF_HORA_FLASH,(unsigned char *)&time.minute);
      timer100=100; // inicia temporitzat 10"  
      break;
    case 3:
     // decrementa rellotge
     if(tx30.ajuste_rel_minuts>-2)
       {
        get_reloj(RELLOTGE_PLACA,&time);
        if((time.minute==0) &&(time.hour==0))
          break;  // per no canviar de dia

        tx30.ajuste_rel_last_anydia=tx30.ajuste_rel_last_anydia_temp;    //v2.20b
 
        tx30.ajuste_rel_minuts--;  
        aux=bcd_bin(time.minute);
        if(aux==0)
          {
           aux=59;
           time.hour=bin_bcd(bcd_bin(time.hour)-1);
          }
        else
         aux--;  
        time.minute=bin_bcd(aux);
        set_reloj(RELLOTGE_PLACA,&time);
        timer50=1;  // per refrescar inmediatament   
       }
      break;
    default:
      return;
   }
}

void RELOJ_FIN(void)
{
 switch(tx30.subestat)
   {
    case 3:
      RELOJ_NEXT();
      break;
    default:
      fin_reltots();
      break;    
   }
}           
 
void display_qt(char nt)   // diplay dia torn
{
char led[2];
long laux;
   // inicia display events torn registrats
   led[0] = 0x67;  // q
   led[1] = SS[bloqueo_torn.index_visu_torn+1]; // 0/1
   display(0,NF_LEDEST,(void *)led);   // q      
   if(tarcom.visu_dia_torn == 1)
     laux = (long)(bloqueo_torn.registre_torn[nt].anydia_torn);// dia torn en dia de l'any
   else // tarcom.visu_dia_torn == 2
     laux = (long)bloqueo_torn.registre_torn[nt].diames_torn;   // dia torn en format mes/dia
   APAGAR_DISPLAY(1);
   display(2,NF_TOT4,(void*)&laux);   
}

void RELOJ_NEXT(void)
{
struct s_time  time;
char led[2];
char fin;
static char nt;
unsigned char hhmm[2];

 get_reloj(RELLOTGE_PLACA,&time);
 switch(tx30.subestat)
   {
   case 6:    // pas a hora minut
        display_hora_en_reloj();
        tx30.subestat=0;
        break;
case 0:  /* pasa a mes-dia */
       //display(0,NF_LEDEST,(void *)&tarcom.lede_rel[tx30.subestat]);
       display(0,NF_LEDEST,(void *)&tarcom.lede_rel[1]);   // v2.18
       display(1,NF_DIA,(unsigned char *)&time.day);
       display(2,NF_TEXT4,&tarcom.text_disem[dia_de_la_semana_from_time(&time)].text[0]);
       tx30.subestat++;
       break;
    case 1:   /* pasa a any */
       DISP_DIG_FIXE(1,2);
       DISP_DIG_FLASH(1,4);
       //display(0,NF_LEDEST,(void *)&tarcom.lede_rel[tx30.subestat]);
       display(0,NF_LEDEST,(void *)&tarcom.lede_rel[2]);   // v2.18
       display(1,NF_ANY,(unsigned char *)&time.year);
       APAGAR_DISPLAY(2);
       tx30.subestat++;
       break;
    case 2:  
       DISP_DIG_FIXE(1,4);
       if(tarcom.veure_events_torn == 0)
          fin_reltots();   // retorna a estat lliure/off/mem_mal/cerrado 
       else
       {
         // inicia contador torn a displaiar
         bloqueo_torn.index_visu_torn = 0;
         nt = (bloqueo_torn.index_visu_torn + bloqueo_torn.index_registre_torn + 1)%NUM_MAX_TORNS;

         display_qt(nt);    // diplay dia torn
         tx30.subestat = 4;
       }
       break;
    case 3:    
      // final modificacio (10")
      // retorna a visualitzacio hhmm
      tx30.subestat=0;
      //memset(RAM_DISP_FLASH,0xff,SIZE_RAM_DISP);
      DISP_FIXE(DISP_LEDEST);
      DISP_FIXE(DISP_IMPORT);
      DISP_FIXE(DISP_EXTRES);
      DISP_DIG_FLASH(1,2);
      break;
    case 4:
         led[0] = 0x78;  // t
         led[1] = SS[bloqueo_torn.index_visu_torn+1];
         nt = (bloqueo_torn.index_visu_torn + bloqueo_torn.index_registre_torn + 1)%NUM_MAX_TORNS;
         display(0,NF_LEDEST,(void *)led); // t
         minuts200_to_hhmm_reversed(bloqueo_torn.registre_torn[nt].hora_inici_torn, hhmm);
         display(1,NF_HHMM_FIXE,(unsigned char *)hhmm);
         if((nt == bloqueo_torn.index_registre_torn) && (bloqueo_torn.estat_torn == 2))
            display(2,NF_TEXT4,"\x40\xc0\x40\x40");
         else
         {
            minuts200_to_hhmm_reversed(bloqueo_torn.registre_torn[nt].hora_final_torn, hhmm);
            display(2,NF_HHMM_FIXE,(unsigned char *)hhmm);
         }
         bloqueo_torn.index_visu_descans = 0;
         tx30.subestat = 5;
      break;
    case 5:
         if((bloqueo_torn.index_visu_descans >= NUM_MAX_DESCANS_REGISTRAR)
            ||(tarcom.num_descans_torn && (bloqueo_torn.index_visu_descans >= tarcom.num_descans_torn)))
         {
           // final visu descans.Mira si hi ha mes torns
           if(++bloqueo_torn.index_visu_torn < NUM_MAX_TORNS)
           { 
             // cas seguent torn
             nt = (bloqueo_torn.index_visu_torn + bloqueo_torn.index_registre_torn +1)%NUM_MAX_TORNS;    
             display_qt(nt);    // diplay dia torn
             tx30.subestat = 4;
           }
           else
            fin_reltots();    // final visualitzacio torns
         }
         else
         { 
           led[0] = 0x5e;
           led[1] = SS[bloqueo_torn.index_visu_descans+1];
           display(0,NF_LEDEST,(void *)led); // d1 ... dn
           fin = 0;
           if(bloqueo_torn.registre_torn[nt].registre_descans[bloqueo_torn.index_visu_descans].hora_inici_descans == 0)
           {
              display(1,NF_TEXT6,"\x00\x00\x40\xc0\x40\x40");
              fin = 1;
           }
           else
           {
              minuts200_to_hhmm_reversed(bloqueo_torn.registre_torn[nt].registre_descans[bloqueo_torn.index_visu_descans].hora_inici_descans, hhmm);
              display(1,NF_HHMM_FIXE,(unsigned char *)hhmm);
           }
           if(bloqueo_torn.registre_torn[nt].registre_descans[bloqueo_torn.index_visu_descans].hora_final_descans == 0)
           {
              display(2,NF_TEXT4,"\x40\xc0\x40\x40");
              fin = 1;
           }
           else
           {
             minuts200_to_hhmm_reversed(bloqueo_torn.registre_torn[nt].registre_descans[bloqueo_torn.index_visu_descans].hora_final_descans, hhmm);
             display(2,NF_HHMM_FIXE,(unsigned char *)hhmm);
           }

           if(fin)
             bloqueo_torn.index_visu_descans = NUM_MAX_DESCANS_REGISTRAR;   // per que propera vegada acabi
           else
             bloqueo_torn.index_visu_descans++;           
         }
      break;
   }
 if(tx30.new_estat==E_RELOJ)
   {
    timer50=10;    /* inicia temporitzat 1/2"  */
    if(tarcom.t_rel)
       timer100=tarcom.t_rel; /* engega tempor per canvi */
   }
 else
   {
    timer50=0;      /* para temporitzat 1/2"  */
       timer100=0; /* para tempor per canvi */
   }
}

const char SS_bGH[3] = {0x00,0x3d,0x76};  // blanc, G, H
void RELOJ_ACTUAL(void)
{
struct s_time  time;
char buf[4];

 get_reloj(RELLOTGE_PLACA,&time);

 switch(tx30.subestat)
   {
    case 0:  /* hora */
        if(tx30.cobertura_rebuda)
        {
          // cas ha de treure cobertura per display extres
          if(tx30.index_nivell < 3)
          {
            display(2, NF_TEXT4, (unsigned char *)nivell_cobertura[tx30.index_nivell++]);  // display guions
          }
          else
          {
            buf[0] = SS[tx30.cobertura_buffer[0]/10];
            buf[1] = SS[tx30.cobertura_buffer[0]%10] | 0x80;
            buf[2] = SS_bGH[tx30.cobertura_buffer[2]];
            buf[3] = SS[tx30.cobertura_buffer[1]];
            display(2, NF_TEXT4, (unsigned char *)buf);
          }
        }
        // sense break
    case 3:  // modif hora
       display(1,NF_HORA,(unsigned char *)&time.minute);
       break;
    case 1:  /*  mes-dia */
       display(1,NF_DIA,(unsigned char *)&time.day);
       break;
    case 2:   /*  any */
       display(1,NF_ANY,(unsigned char *)&time.year);
       break;
   }

 timer50=10;    /* reinicia temporitzat 1/2"  */
}

 
void FI_ITV(void)
{
 switch(bloqueo_torn.estat_retorn_itv)
   {
     case E_OFF:
     tx30.new_estat=E_OFF;
     paso_off();
     break;
     default:
     paso_a_libre(0);
     break;
   }
  bloqueo_torn.esta_passant_itv = 0; 
}


void fin_cods(void)
{
	if(tx30.pendent_test_displ_pas_a_on == 2){
		OFF_LIBRE();
		return;
	}
 switch(tx30.estat_retorn_cods)
   {
    case E_CERRADO:
     tx30.new_estat=E_CERRADO;
     paso_off();
     break;
   case E_OFF:
     tx30.new_estat=E_OFF;
     if(num_cods!=0)    // per cas password correcte
       {
        // cas codi secret incorrecte
        paso_off();
       }
     else
       {
        tx30.pendent_password_pas_a_on = 0; 
        OFF_LIBRE();  // per si ha de fer ESP_ON      
       }
     break;
   default:
    paso_a_libre(0);
    break;
  }
}


unsigned char  mask_test,nDisp_test,nDig_test;

// inicia secuencia test/visu_last_serv segons opcio 0/1
void inici_test_vls(char opcio)
{
 if(opcio)
   {
    // cas visu_last_serv
    test_vls=1;
    timerseg=1;
    timer50=1;   // per saltar a primera vis.
   }
 else
   {
    // cas test
    test_vls=0;
    //memset(RAM_DISP_FLASH,0xff,SIZE_RAM_DISP);
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
    // treu per lluminos configuracio tots el llums encessos.
    // Ha d' estar activada per tarifa
    
    if(tarcom.hi_ha_conf_llums_test_display)
    {
      out_luzex(tarcom.llums_test_display[0],tarcom.llums_test_display[1]);
    }
    if(tarcom.OP_test_old_new==0)
      {
       //memset(RAM_DISP+1,,SIZE_RAM_DISP-1);
       DISP_FILL(DISP_LEDEST,SS[0] | SS_DP);
       DISP_FILL(DISP_IMPORT,SS[0] | SS_DP);
       DISP_FILL(DISP_EXTRES,SS[0] | SS_DP);
       timer50=40;
      }
    else
      {   
       //memset(RAM_DISP,0,SIZE_RAM_DISP);
       APAGAR_DISPLAY(DISP_LEDEST);
       APAGAR_DISPLAY(DISP_IMPORT);
       APAGAR_DISPLAY(DISP_EXTRES);
       
//       mask_test=0xf8;
//       RAM_DISP[0]=0xf8;
       mask_test=0x00;
       nDisp_test = 0;
       nDig_test = 0;
       timer50=20;
      } 
   }
 tx30.subestat=0;
 tx30.new_estat=E_TEST;
}

void TESTIMPR(void)
{  
if( !delay_impre)
    {
     if(test_vls)
       { //cas visu last serv
         if(hay_impresora())
             {
              imprimir_ticket_servei(0);    // sense retorn
              delay_impre=80;   // 4"
             }
       }
     else
       { // cas test
         if(hay_impresora())
            imprimir_ticket_avance_papel();
       }
    }


}

void TEST(void)
{   
 uchar mask;
 ulong laux1,laux2;
 char fin;
 if(test_vls)
   {
    // cas visu_last_serv
    if(--timerseg)
      {
       timer50=20;  // per fer 1 segon
       return;
      }
    if(tx30.subestat==0)
      {
       display(0,NF_LEDEST,(void *)&tarcom.vars_estat[E_LIBRE].ledest);
       if(tarcom.flash_visu_last_serv)
          DISP_FLASH(0);
      }
    mask=tarcom.confi_vls>>tx30.subestat;   
    do
      {
       if(mask&0x01) break;
       tx30.subestat++;
       mask>>=1;
      }while(mask);
    if(mask==0)
      tx30.subestat=5;   
    switch(tx30.subestat)
      {
       case 0:
         display(1,NF_IMPORT,(unsigned char*)&serv[0].import);
         display(2,NF_SUPL,(unsigned char*)&serv[0].supl);
         LEDS_FRONTAL(tarcom.lf_vls0);
         if(tarcom.flash_visu_last_serv)
           {
            DISP_FLASH(1);
            DISP_FLASH(2);
           }
         break;
       case 1:  
         laux1= (ulong)(((float)serv[0].import)*tarcom.factor_alt+.5); 
         laux2= (ulong)(((float)serv[0].supl)*tarcom.factor_alt+.5); 
         display(1,NF_IMPORT_ALT,(unsigned char*)&laux1);
         display(2,NF_SUPL_ALT,(unsigned char*)&laux2);
         LEDS_FRONTAL(tarcom.lf_vls0_alt);
         if(tarcom.flash_visu_last_serv)
           {
            DISP_FLASH(1);
            DISP_FLASH(2);
           }
         break;
       case 2:  
         laux1= (ulong)((((float)serv[0].import)+(float)serv[0].supl)*tarcom.factor_alt+.5); 
         display(1,NF_IMPORT_ALT,(unsigned char*)&laux1);
         display(2,NF_TEXT4,(void *)&tarcom.TEXT_EURO[0]);
         LEDS_FRONTAL(tarcom.lf_vls3);
         if(tarcom.flash_visu_last_serv)
           {
            DISP_FLASH(1);
            DISP_FLASH(2);
           }
         break;
         
       case 3:
         APAGAR_DISPLAY(2);
         laux1 = serv[0].crono/60;
         display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,(unsigned char*)&laux1);    
         LEDS_FRONTAL(tarcom.lf_vls1);
         if(tarcom.flash_visu_last_serv)
           {
            DISP_FLASH(1);
           }
         break;
       case 4:
         APAGAR_DISPLAY(2);
         display(1,NF_D_T,(unsigned char*)&serv[0].hm_oc);
         LEDS_FRONTAL(tarcom.lf_vls2);
         if(tarcom.flash_visu_last_serv)
           {
            DISP_FLASH(1);
           }
         break;
       default:
         break;
      }
    if(tx30.subestat<5)
      {
       tx30.subestat++;
       timerseg=tarcom.segs_visu_last_serv;
       timer50=20; // 1"
      }
    else
      {
       fin_cods();
      }
   }
 else
   {
    // cas test
    if(tarcom.OP_test_old_new==0)
      {
       if(++tx30.subestat<10)
         {
          //memset(RAM_DISP+1,SS[tx30.subestat] | SS_DP,SIZE_RAM_DISP-1);
           DISP_FILL(DISP_LEDEST,SS[tx30.subestat] | SS_DP);
           DISP_FILL(DISP_IMPORT,SS[tx30.subestat] | SS_DP);
           DISP_FILL(DISP_EXTRES,SS[tx30.subestat] | SS_DP);
          timer50=40;
         } 
       else
          fin_cods();
      }
    else
      {
       fin = 0;
       if(mask_test!=0xff)
         {
          // seguent segment
          mask_test=(mask_test<<1)|1;
          //RAM_DISP[tx30.subestat]=mask_test;
         }           
       else
         {
          // seguent digit 
          mask_test=0x01;
          if(++nDig_test >= DISP[nDisp_test].NDIG)
            {
              // seguent display
              if(++nDisp_test >= (sizeof(DISP)/sizeof(DISP[0])))
                  fin = 1;
              else
                nDig_test =0;
            }
           
         }
        if(!fin)
         {
          
          //RAM_DISP[tx30.subestat]=mask_test;
          DISP_DIG_SET(nDisp_test,nDig_test,mask_test);
          timer50=10;
         }           
        else
           fin_cods();
      }
   }
}



void TOTS_TR(void)  // transmissio opcional amb tecla ^
{
 
 if(hay_impresora() && !delay_impre)
	 {
	 calcul_tots_aux();
	 copia_tots_a_reg(tarcom.digtot);
	 get_reloj_fechhor(RELLOTGE_PLACA,&reg_tcks_tx30->fecha_actual);
	 imprimir_ticket_tots();
	 delay_impre=80;   // 4"
	 }

}

void TOTS(void)
{
unsigned char  index,ntot,fin,buf[5];
unsigned long  laux;
int st;
 fin=0;
 index=tx30.subestat>>1;
 if(index==TOT_FIN)
    fin=1;
 else
    {
     ntot=tarcom.disp_tot[index].nt;
     if((tx30.subestat & 0x01)==0)
        {
         if(ntot==255)
            fin=1;
         else
           {
            LEDS_FRONTAL(tarcom.disp_tot[index].lf);
            //
            //  falta incloure cas totalitzador erroni  !!!!!!!!!
            laux = totalitzador_read_for_display(ntot, &st);
            if(tarcom.tot_un_paso)
               {
                display(2,tot[ntot].form,(void *)&laux);
                buf[0]=tarcom.disp_tot[index].sst;
                buf[1]=0;
                display(0,NF_LEDEST,buf);
                memset(buf,0,5);
                display(1,NF_TEXT5,buf);
                switch(tarcom.digtot)
                  {
                   case 5:
                     laux=(laux/10000L)%10;
                     display(1,NF_TOT6,(void *)&laux);
                     break;
                   case 6:
                     laux=(laux/10000L)%100;
                     display(1,NF_TOT2,(void *)&laux);
                     break;
                   case 8:
                   default:
                     laux=(laux/10000L)%10000;
                     display(1,NF_TOT4,(void *)&laux);
                     break;
                  }
               }
            else
               {  // cas tot. en 2 pasos .Primer pas
                buf[0]=tarcom.disp_tot[index].sst;
                memset(buf+1,0,4);
                switch(tarcom.digtot)
                  {
                   case 5:
                     display(1,NF_TEXT5,buf);
                     break;
                   case 6:
                     laux = (laux/100000L)%10;
                     display(1,NF_LEDTOT,buf);
                     display(1,NF_TOT6,(void *)&laux);
                     break;
                   case 8:
                   default:
                     laux = (laux/100000L)%1000;
                     display(1,NF_LEDTOT,buf);
                     display(1,NF_TOT8,(void *)&laux);
                     break;
                  }
               }
           }
        }
     else
        {
         laux = totalitzador_read_for_display(ntot, &st);
            //  falta incloure cas totalitzador erroni  !!!!!!!!!
         display(1,tot[ntot].form,(void *)&laux);
        }
    }
 if(!fin)
   {
    tx30.subestat++;
    if(tarcom.tot_un_paso)
      tx30.subestat++;  // cas un sol pas.
    timer50=tarcom.titot;
   }
 else
   {
    fin_reltots();
   }
}

void TOTS_PASW(void){
	if (tarcom.tots_historics){
		memcpy(reg_tcks_tx30->texte_nom , blqs.texte_nom ,sizeof(reg_tcks_tx30->texte_nom));
		memcpy(reg_tcks_tx30->texte_dni , blqs.texte_dni ,sizeof(reg_tcks_tx30->texte_dni));
		memcpy(reg_tcks_tx30->texte_matricula , blqs.texte_matricula ,sizeof(reg_tcks_tx30->texte_matricula));
		reset_opc_tck(OPCT_GENERICO1);
		reset_opc_tck(OPCT_GENERICO2);
		reset_opc_tck(OPCT_GENERICO3);
		reset_opc_tck(OPCT_GENERICO4);
		get_reloj_fechhor(RELLOTGE_PLACA,&reg_tcks_tx30->fecha_actual);
		switch (tecla_num){
		case 0:
			// Imprimir totalitzadors diaris
			inici_ticket(0);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES DIARIOS", sizeof(reg_tcks_tx30->Texte_Document));
			imprimir(TICKET_TCK_TOTS_HEADER);
			
			set_opc_tck(OPCT_GENERICO1);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES HOY", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_DIA, tarcom.digtot, 1);
			imprimir(TICKET_TCK_TOTS_PERIODICO);
			
			reset_opc_tck(OPCT_GENERICO1);
			set_opc_tck(OPCT_GENERICO2);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES AYER", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_DIA_ANT, tarcom.digtot, 0);
			imprimir(TICKET_TCK_TOTS_PERIODICO);
			imprimir(TICKET_AVANCE_PAPEL);
			
			final_ticket();
			break;
		case 1:
			// Imprimir totalitzadors mensuals
			inici_ticket(0);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES MENSUALES", sizeof(reg_tcks_tx30->Texte_Document));
			imprimir(TICKET_TCK_TOTS_HEADER);
			
			set_opc_tck(OPCT_GENERICO3);
			memcpy(reg_tcks_tx30->Texte_Document, "MES ACTUAL PARCIAL", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_MES, tarcom.digtot, 1);
			imprimir(TICKET_TCK_TOTS_PERIODICO);
			
			reset_opc_tck(OPCT_GENERICO3);
			set_opc_tck(OPCT_GENERICO4);
			memcpy(reg_tcks_tx30->Texte_Document, "MES ANTERIOR", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_MES_ANT, tarcom.digtot, 0);
			imprimir(TICKET_TCK_TOTS_PERIODICO);
			imprimir(TICKET_AVANCE_PAPEL);
			
			final_ticket();
			break;
		case 2:
			// Imprimir totalitzadors semestrals
			copia_tot_sem();
			inici_ticket(0);
			set_opc_tck(OPCT_GENERICO1);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES SEMESTRE", sizeof(reg_tcks_tx30->Texte_Document));
			imprimir(TICKET_TCK_TOTS_HEADER);
			
			reset_opc_tck(OPCT_GENERICO1);
			set_opc_tck(OPCT_GENERICO4);
			
			if(semestre_actiu == 1){
				memcpy(reg_tcks_tx30->Texte_Document, "SEMESTRE ACTUAL PARCIAL", sizeof(reg_tcks_tx30->Texte_Document));
				copia_tots_eq_a_reg(TOT_SEM1, tarcom.digtot, 1);
				imprimir(TICKET_TCK_TOTS_PERIODICO);

				memcpy(reg_tcks_tx30->Texte_Document, "SEMESTRE ANTERIOR", sizeof(reg_tcks_tx30->Texte_Document));
				copia_tots_eq_a_reg(TOT_SEM2_ANT, tarcom.digtot, 0);
				imprimir(TICKET_TCK_TOTS_PERIODICO);
			}
			else if (semestre_actiu == 2){
				memcpy(reg_tcks_tx30->Texte_Document, "SEMESTRE ACTUAL PARCIAL", sizeof(reg_tcks_tx30->Texte_Document));
				copia_tots_eq_a_reg(TOT_SEM2, tarcom.digtot, 1);
				imprimir(TICKET_TCK_TOTS_PERIODICO);

				memcpy(reg_tcks_tx30->Texte_Document, "SEMESTRE ANTERIOR", sizeof(reg_tcks_tx30->Texte_Document));
				copia_tots_eq_a_reg(TOT_SEM1, tarcom.digtot, 0);
				imprimir(TICKET_TCK_TOTS_PERIODICO);
			}

			imprimir(TICKET_AVANCE_PAPEL);
			
			final_ticket();
			break;
		case 3:
			// Imprimir totalitzadors anuals
			inici_ticket(0);
			set_opc_tck(OPCT_GENERICO1);
			memcpy(reg_tcks_tx30->Texte_Document, "TOTALIZADORES ANUALES", sizeof(reg_tcks_tx30->Texte_Document));
			imprimir(TICKET_TCK_TOTS_HEADER);
			
			reset_opc_tck(OPCT_GENERICO1);
			set_opc_tck(OPCT_GENERICO4);
			
			memcpy(reg_tcks_tx30->Texte_Document, "A�O ACTUAL PARCIAL", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_ANY, tarcom.digtot, 1);
			imprimir(TICKET_TCK_TOTS_PERIODICO);

			memcpy(reg_tcks_tx30->Texte_Document, "A�O ANTERIOR", sizeof(reg_tcks_tx30->Texte_Document));
			copia_tots_eq_a_reg(TOT_ANY_ANT, tarcom.digtot, 0);
			imprimir(TICKET_TCK_TOTS_PERIODICO);
			
			imprimir(TICKET_AVANCE_PAPEL);
			
			final_ticket();
			break;
		}
	}
}


float  factork;
unsigned long  acdist;
unsigned char  nv,nt,nv_sub;
unsigned char  timeout_parcial;
unsigned char  timeout_total;
unsigned char es_verif_k; 

void PARMS_0(void)
{
 fin_cods();
}           


void PARMS_K_ABORT(void)
{
 if(es_verif_k)    //(tx30.subestat==1)&&(nv==2)
   {
    acdist++;
    visuk=(long)(((float)acdist)*factork);
    //display(1,visu_parms[nv-1].nform,visu_parms[nv-1].p);
    display(1,visu_parms[1].nform,visu_parms[1].p);
    //tx30.new_estat=E_PARMS;  2.07
    timeout_parcial=15;
   }
 else
    fin_cods();
}

                          
unsigned char  last_tar_enviada;

void PARMS_TR(void)
{
 if(es_verif_k)    //(tx30.subestat==1)&&(nv==2)
   return;   // mentres compta metres per visu K no imprimeix
  if(hay_impresora() &&!delay_impre)
	 {
	 copia_parms_a_reg();
	 get_reloj_fechhor(RELLOTGE_PLACA,&reg_tcks_tx30->fecha_actual);
         imprimir_ticket_params();
	 delay_impre=80;   // 4"
	 }
}

char bdisp[7];

void PARMS(void)
{
unsigned char  exitdo,bld[2],no_cuenta;
unsigned int  despl;
uchar * p_var;
long lvar_aux;
char fecha_aux[3];
unsigned short short_aux;
 exitdo=0;
 if(es_verif_k && event==V_TEMPOR) /* cas verif k */
    {
     --timeout_parcial;
     --timeout_total;
     if(timeout_parcial && timeout_total)
        exitdo=1;
    }


 while(!exitdo)
    {
     exitdo=1;
     switch(tx30.subestat)
        {
         case 0:
            leer_eprom_tabla(GE_BLQS);
            ++tx30.subestat;
            nv=0;
            if(!(tarcom.haybloc & 0x01))
               {
                ++tx30.subestat;
                nv=PARMS_L_INI;
               }
            exitdo=0;
            break;
         case 1:
            switch(nv)
              {
               case 0:
                 break;
               case 1:
                 if(tarcom.haybloc & 0x02)
                   {
                    factork=(tarcom.yardas*(float)tarcom.DIVK)/(float)tarcom.k;
                    acdist=0L;
                    visuk=0L;
                    timeout_parcial=15;  /* 30segs. */
                    timeout_total=60;    /* 2 minuts  */
                   }
                 else
                    {
                     nv++;
                     exitdo=0;
                    }
                 break;
               case PARMS_L_INI:
                 exitdo=0;
                 tx30.subestat=2;
                 break;
               case 12:
                 if(!tarcom.bloqueo_torn)
                   {
                    exitdo=0;
                    tx30.subestat=2;
                    nv=PARMS_L_INI;
                   }
              }
            if(exitdo)
              {
               display(0,NF_LEDEST,(void*)visu_parms[nv].led);
               display(1,visu_parms[nv].nform,visu_parms[nv].p);
               nv++;
              }
            break;
         case 2:     /* bloc 2 */
            if(!(tarcom.haybloc & 0x04)|| (nv==PARMS_r_INI))
                {
                 exitdo=0;
                 nv=0;
                 nt=0;
                 tx30.subestat=3;
                }
            else
               {
                display(0,NF_LEDEST,(void*)visu_parms[nv].led);
                display(1,visu_parms[nv].nform,visu_parms[nv].p);
                nv++;
               }
            break;
         case 3:    /* bloc 3 */
            if(!(tarcom.haybloc & 0x08))
                {
                 tx30.subestat=4;
                 break;
                }
            if(nv==(sizeof visu_parms_tar/sizeof visu_parms_tar[0]))
              {
               nv=0;++nt;
              }
            if(nv==0)
              {
               while(((tarcom.tars_blq3 >>nt) & 0x1L)==0 && nt<tarcom.ntars)
                   nt++;
               if(nt >=tarcom.ntars)
                  {
                   tx30.subestat=4;
                   break;
                  }
              }
            no_cuenta=0;
            switch(visu_parms_tar[nv].tip)
               {
                case 0:  /* depen de no. tarifa */
                    despl=nt*sizeof(struct  s_tarifa);
                    break;
                case 1:
                    despl=tarifa[nt].b_t_ocup*sizeof(struct s_bloc_tar);
                    break;
                case 2:
                    if(!(bloc_tar[tarifa[nt].b_t_pagar].bits_cuenta & 0x40))
                       no_cuenta=1;    /* cas no conta en a pagar*/
                    despl=tarifa[nt].b_t_pagar*sizeof(struct s_tarifa);
                    break;
				case 3:
					despl=0;
					break;
               }
			strcpy(bdisp, (bloc_tar[tarifa[nt].b_t_ocup].tipt_vf==0) ? "\x78\x06\x5e" : "\x78\x40\x5e");
            if(no_cuenta)
              {
               exitdo=0;
              }
            else
              {
               bld[0]=visu_parms_tar[nv].led;
               bld[1]=SS[(nt+1)%16];
               if(nt>15)
                 bld[1]|=0x80;
               display(0,NF_LEDEST,bld);

               p_var=((uchar*)visu_parms_tar[nv].p)+despl;
               display(1,visu_parms_tar[nv].nform,p_var);
              }
            nv++;
            break;

// afegit per TXD
         case 5:     // visualitzacions check i precanvi sense password
            nv=0;
            tx30.subestat=6;
            // sense break  break;
         case 6:
            if(nv>=(sizeof(visu_parms_sin_passw)/sizeof(visu_parms_sin_passw[0])))
            {
               tx30.subestat=7;
               nv = 0;
               nv_sub = 0;
               exitdo = 0; // per continuar dins while i anar a case 7
            }
            else
              {
               display(0,NF_LEDEST,(void*)visu_parms_sin_passw[nv].led);
               display(1,visu_parms_sin_passw[nv].nform,visu_parms_sin_passw[nv].p);
               nv++;
              }
            break;
        case 7:
          if(nv >=blqs.nmodifs_prog)
               tx30.subestat=4;
          else
          {
            switch(nv_sub)
            {
            case 0:
              APAGAR_DISPLAY(0);
              fecha_aux[0] = bin_bcd(blqs.modif_prog[nv].fecha.day);
              fecha_aux[1] = bin_bcd(blqs.modif_prog[nv].fecha.month);
              fecha_aux[2] = bin_bcd(blqs.modif_prog[nv].fecha.year);
              display(1,NF_AMD,(unsigned char*)&fecha_aux);
              lvar_aux = (long)blqs.modif_prog[nv].version;
              display(2,NF_VERSION,&lvar_aux);
              nv_sub = 1;
              break;
            case 1:
              short_aux = reverse_int(blqs.modif_prog[nv].check);
              display(2,NF_CHK,&short_aux);
              nv_sub = 0;
              nv++;
              break;
            }
          }
          break;

        }  // final switch(tx30.subestat)
    }  // final while(!exitdo)
 if(tx30.subestat==4)
    {
     fin_cods();
    }
 else
   {
    if((tx30.subestat==1)&&(nv==2))
       {
        es_verif_k=1; 
        timer50=60;           /* 2" per time-out cas verif k  */
       } 
    else
       {
        es_verif_k=0; 
        timer50=tarcom.titot;
       }
   }     
}


unsigned char  bufd[NUM_CODS_MAX];

void INICIO_CODS(void)
{
unsigned char  i;
int mask;

    if((tx30.estat == E_LIBRE) &&
       (tarcom.torn_continu && tx30.acum_off && (tx30.segons_pot_anular_pas_off == 0)))
      return;
    tx30.estat_retorn_cods=tx30.estat;
    tx30.new_estat=E_CODS;  
    DISP_FIXE(DISP_LEDEST);
    DISP_FIXE(DISP_IMPORT);
    DISP_FIXE(DISP_EXTRES);
    timer50=0;

    leer_verif_cods();   // llegir codigs secrets eprom_tax 

  // prepara mascara amb cods que estan actius
  for(i=0,mask = 0x01,tx30.cods_mask=0;i<NUM_CODS;i++,mask<<=1)
  {
    if(cods_programat(i))
      tx30.cods_mask |= mask;
  }
  tx30.cods_mask &= (~(0x0001 << CODS_OFFxON));  // elimina cods 5 (pas de OFF a Lliure)
  if(tx30.estat != E_CERRADO)
    tx30.cods_mask &= (~(0x0001 << CODS_CERRAR_TURNO));  // elimina cods 7 (torn propietari)
  if(tarcom.torn_continu)
    tx30.cods_mask |= 0x0008; // posa  pasword fp cas Fran�a
  if(tarcom.haypassw & (0x0001 << CODS_PARAMS)) // si esta a haypassw
    tx30.cods_mask |= (0x0001 << CODS_PARAMS);  // inclou params encara que no estigui programat
  
  switch(tx30.cods_mask)
    {
     case 0:          // no hi ha cap codi secret
      i=0;
      if(tarcom.OP_TST)i=1;
      if(tarcom.VISU_LAST_SERV)i|=2; else if(hay_impresora() && (tarcom.ticket_voluntari==1))i=4;
      if(i==0){fin_cods();break;}
      if(i==1){inici_test_vls(0);break;}   // per fer test
      if(i==2){inici_test_vls(1);break;}   //  per fer visu_last_serv
      // cas test i visu no fa break
     default:         /* A triar codi/test/visu last ser.*/
      // prepara display segons cods que hi hagi
      memset(bufd,0,sizeof bufd);
      for(i=0,mask = 0x01;i<NUM_CODS;i++,mask<<=1)
      {
        if(tx30.cods_mask & mask)
          bufd[i] = SS_DP;
      }
      if(tx30.cods_mask & 0x003f)   // 6 primers
      {
        tx30.cods_bloc_visu = 0;
        display(1,NF_TEXT6,bufd);
        APAGAR_DISPLAY(2);
      }
      else if(tx30.cods_mask & 0x03c0)  // 4 segons
      {
        tx30.cods_bloc_visu = 1;
        APAGAR_DISPLAY(1);
        display(2,NF_TEXT4,bufd+6);
      }
      else
      {
        tx30.cods_bloc_visu = 1;  // per que surti amb tecla 0
        APAGAR_DISPLAY(1);
        APAGAR_DISPLAY(2);
      }
      display(0,NF_LEDEST,(unsigned char*)&tarcom.vars_estat[E_CODS].ledest);
      tx30.subestat=0;
      timer100=tarcom.t_cods;
      break;
    }
}


void OFF_TCODS(void)
{
  
//       switch(tx30.subestat_torn)
//       {
//        case TORN_TANCAT:
           // cancela es pugui treure ticket tancar torn
//           timer50=0;
//           tx30.timer_tick_torn=0;
           REFRESH_ON();
           INICIO_CODS();
//           break;
//        }
}


unsigned char  bufsec[LEN_CODS];


void CODS_PARMS_SIN_PASSW(void)
{
 switch(tx30.subestat)
   {
    case 0:
      if((tx30.cods_bloc_visu == 0) && (tx30.cods_mask & 0x03c0))
      {
        // passa a seguent grup de cods ( a display 2)
        tx30.cods_bloc_visu = 1;
        APAGAR_DISPLAY(1);
        display(2,NF_TEXT4,bufd+6);
      }
      else
      {
        // selecciona visu parms sense Codi secret
        tx30.new_estat=E_PARMS;
        tx30.subestat=5;
        DISP_FIXE(1);
        APAGAR_DISPLAY(2);
        es_verif_k=0;
        timer50=1;    /* per saltar inmediatament a display params */
      }
      break;
   }
}


void CODS(void)
{
unsigned char  bufd[NUM_CODS_MAX];
unsigned char  i;
long l_aux;
int i_aux;
 switch(tx30.subestat)
   {
    case 0:   /* selecciona quin codig */
        i=tecla_num;
        if(i==6)
          {
           if(tarcom.VISU_LAST_SERV)
             {
              inici_test_vls(1);   // per fer visu_last serv
             }
           else
             {
              if(hay_impresora() && (tarcom.ticket_voluntari==1))
                 {
                  imprimir_ticket_servei(0);    // sense retorn
                  delay_impre=80;   // 4"
                 }
              fin_cods();
             }
           break;
          }
        if(i==5 && tarcom.OP_TST)
           {
            inici_test_vls(0);   // per fer test
            break;
           }
        if(i>4)
          break;
        if(tx30.cods_bloc_visu == 1)
          i+=6;   // cas segon bloc cods
        if(cods_programat(i))
        {
              memset(bufd,0,sizeof bufd);
              bufd[i]=SS_DP;
              num_cods=i;
              tx30.subestat=2;
              if(tx30.cods_bloc_visu == 0)
              {
              display(1,NF_TEXT6,bufd);
              DISP_FLASH(1);
              }
              else
              {
              display(2,NF_TEXT4,bufd+6);
              DISP_FLASH(2);
              }
              break;
        }
        else if(i == CODS_PARAMS)
          {
            // cas va directament a PARAMS sense entrar password
            tx30.new_estat=E_PARMS;
            tx30.subestat=0;
            DISP_FIXE(1);
            es_verif_k=0;
            timer50=1;    /* per saltar inmediatament a display params */
          }
        break;
        
   case 1:    //   cas Paris entra 1/2/3  itv/bloq sancio/ desbloq sancio
     switch(tecla_num)
     {
     case 0:  // Fran�a anular control horari per passar ITV
       if(bloqueo_torn.esta_passant_itv ||
          (bloqueo_torn.anydia_last_itv == gt.anydia))
         break;   // cas esta dins de proces passar itv o ja l'ha passat avui.
       bloqueo_torn.anydia_last_itv = gt.anydia;   // indicador ja ho ha fet avui.
       bloqueo_torn.esta_passant_itv = 1;
       bloqueo_torn.minuts_passant_itv = 11;
       bloqueo_torn.estat_retorn_itv = tx30.estat_retorn_cods;
       if(tx30.estat_retorn_cods == E_LIBRE)
          paso_a_libre(0);  
       else
          paso_a_libre(1);  // es desde OFF
       return;
       break;
     case 1:
        fecha_paro[0]=0x01;
        fecha_paro[1]=0x01;
        fecha_paro[2]=0x01;  /*  1_1_2001   */
          grabar_fp();
          leer_verif_fp();
          gestio_fecha_paro();
       break;
     case 2:
        fecha_paro[0]=0x31;
        fecha_paro[1]=0x12;
        fecha_paro[2]=0x89;  /*  31_12_2089   */
          grabar_fp();
          leer_verif_fp();
          gestio_fecha_paro();
       break;
     default:
       break;
     }
     fin_cods();
     break;
     
    default:     /* cas entra codig secret */
        i=tx30.subestat-2;
        if(i==0)
           memset(bufsec,0,LEN_CODS);
        if(i<LEN_CODS)
          {
           bufsec[i]=tecla_num+1;
           tx30.subestat++;
           if(tx30.cods_bloc_visu == 0)
           {
             if((num_cods == 3) && (tarcom.torn_continu))
             {
               // cas codi policia.Compara amb valor tarifa
				 int correcto=1;
				 for(i=0; i<LEN_CODS; i++){
					 if(tarcom.password_paris[i]==0) break;
					 if(tarcom.password_paris[i] != (bufsec[i]+'0')){
						 correcto=0;
						 break;
					 }
				 }
				 if(correcto==0) break;
             }
             else if(memcmp(bufsec,cods_tax.cods[num_cods],LEN_CODS)!=0)
                 break;
           }
           else
           {
             if(memcmp(bufsec,cods_tax_ext.cods[num_cods-6],LEN_CODS)!=0)
                 break;
           }
           switch(num_cods) /* cas entrat codi correcte */
               {
                case 0:             /* reset tots parcials */
				   borrar_tots_parcials();
                   // inicialitza fecha borrat totalitzadors (v 1.04)
                   time_to_fechhor(&blqs.fecha_borr_tots , &gt.time);
                   // i ho grava a e2prom
                   blqs.chk_blqs=calcul_chk_blqs();
                   grabar_eprom_tabla(GE_BLQS);
                  
                    pk_tot_tot=tarcom.pmpk0;
                    pk_tot_tot_ccab = tarcom.pmpk0_ccab;
                    pk_oc_tot=tarcom.pmpk0;
                    pk_lpasj_tot=tarcom.pmpk0;
                    servcom.pk_lib_serv=tarcom.pmpk0;
                    servcom.pk_lib_pasj=tarcom.pmpk0;
                    servcom.pk_oc_serv=tarcom.pmpk0;

                  fin_cods();
                  break;
                case 1:
                  tx30.new_estat=E_MODTOT;
                  tx30.subestat=0;
                  DISP_FIXE(1);
                  timer50=1;    /* per saltar inmediatament a primer tot.  */
                  break;
                case 2:     /*  modificar rellotge  */
                  tx30.new_estat=E_MODREL;
                  tx30.subestat=0;
                  DISP_FIXE(1);
                  timer50=1;    /* per saltar inmediatament a modif hora  */
                  break;
                case 3:        /* modif. fecha paro  */
                  if(tarcom.torn_continu == 0)
                  {
                  leer_verif_fp();
                  tx30.new_estat=E_MODFP;
                  tx30.subestat=0;
                  DISP_FIXE(1);
                  timer50=1;    /* per saltar inmediatament a modif fecha paro  */
                  break;
                  }
                  else
                  {
                  // cas password policia Fran�a
                  tx30.subestat=1;
                  DISP_FIXE(1);
                  display(1,NF_TEXT6,"\x06\x40\x5b\x40\x4f");
                  timer100=tarcom.t_cods;
                  break;
                  }
                case CODS_PARAMS:
                  tx30.new_estat=E_PARMS;
                  tx30.subestat=0;
                  DISP_FIXE(1);
                  es_verif_k=0;
                  timer50=1;    /* per saltar inmediatament a display params */
                  break;
                case 5:    // pasword de OFF a LLiure
                  num_cods=0;  //indicador pass. correcte
                  fin_cods();
                  break;
                case 6:
                  tx30.new_estat=E_MOD_TAR;
                  tx30.subestat=0;
                  tx30.index_tar = 0;
                  // passa impkm_ss a integer i el copia a a array auxiliar
                  for(i_aux=0;i_aux<tarcom.ntars;i_aux++)
                      tx30.var_preu[i_aux] = (long)(bloc_tar[i_aux].impkm_ss*KDEC[tarcom.NDEC] +.5);
                  display(0,NF_LEDEST,(unsigned char*)&tarcom.vars_estat[E_MOD_TAR].ledest);                  
                  display(1,NF_SUPL_NOBLK,&tx30.var_preu[tx30.index_tar]);
                  l_aux = tx30.index_tar+1;
                  display(2,NF_VEL,&l_aux);
                  break;
                case CODS_CERRAR_TURNO:      // torn propietari
                  display(2,NF_TEXT4,&tarcom.texte_TORN[0]);
                  tx30.new_estat=E_CERRADO;
                  timer50 =120;  // 6" per confirmar
                  tx30.subestat_torn = TORN_TRIAR_OPCIO;
                  break;
                case 8:      // pre programar events cas control horari Fran�a
                  tx30.new_estat = E_PREPROG;
                  
                  bloqueo_torn.events_preprog = 0;  // elimina preprog. Es reactiva al acabar programacio
                  
                  bloqueo_torn.preprog_index = 0;
                  validar_hhmm(taula_preprog[bloqueo_torn.preprog_index].var);
                  preprog_display();
                  break;
               }
          }
        break;
   }
}

struct
{
 unsigned char ntot;
 unsigned char digit;
 unsigned char bcd[10];
 unsigned char text2[5];
}totmodif;

void MT_TOTS(void)
{
unsigned char  ntot,fin,buf[5];
unsigned long  laux;
int st;

 timer50=0;      /* nomes serveix per entrar primera vegada */
 fin=0;
 display(0,NF_LEDEST,(unsigned char*)&tarcom.vars_estat[E_MODTOT].ledest);
 if((tx30.subestat & 0x01)==1)
    {                     /* cas segona mitat totalitzador */
     laux=totalitzador_read(totmodif.ntot, &st);
     display(1,tot[totmodif.ntot].form,(void *)&laux);
     if(tarcom.totbormodif==2)
         DISP_DIG_FLASH(1,4);
     totmodif.digit=4;
    }
 else
    {
     do
       {
        ntot=tx30.subestat>>1;
        if(ntot==TOT_FIN)
          {
           fin=1;
           break;
          }
        if(!tot[ntot].modif)
           tx30.subestat+=2;
        else
           break;
       }while(1);
     if(!fin)
         {
          totmodif.ntot=ntot;
          buf[0]=SS[tot[ntot].led];
          memset(buf+1,0,4);
          memcpy(totmodif.text2,buf,5);
          switch(tarcom.digtot)
            {
             case 5:
               display(1,NF_TEXT5,buf);
               totmodif.digit=7; /* per evitar modificacio */
               break;
             case 6:
               laux=(totalitzador_read(ntot, &st)/100000L)%10;
               display(1,NF_LEDTOT,buf);
               display(1,NF_TOT6,(void *)&laux);
               if(tarcom.totbormodif==2)
                  DISP_DIG_FLASH(1,0);
               totmodif.digit=5;
               break;
             case 8:
             default:
               laux=(totalitzador_read(ntot, &st)/100000L)%1000;
               display(1,NF_LEDTOT,buf);
               display(1,NF_TOT8,(void *)&laux);
               if(tarcom.totbormodif==2)
                  DISP_DIG_FLASH(1,2);
               totmodif.digit=7;
               break;
            }
         }
    }
 if(!fin)
   {
    tx30.subestat++;
   }
 else
   {
       fin_cods();
   }
}

void MT_INCR(void)
{
unsigned char  nt,nf;
unsigned long  laux;
int st;
 if(tarcom.totbormodif<2 || totmodif.digit>tarcom.digtot)
    return;           /* cas no pot modificar tots */
 nt=totmodif.ntot;
 ltobcd(totalitzador_read(nt, &st),(char *)totmodif.bcd);
 totmodif.bcd[totmodif.digit]=(totmodif.bcd[totmodif.digit]+1)%10;
 totalitzador_write(nt, bcdtol(totmodif.bcd,8));
 nf=tot[nt].form;
 if(totmodif.digit<5)
    {
     laux=totalitzador_read(nt, &st);
     display(1,nf,(unsigned char*)&laux);
    }
 else
    {
     switch(tarcom.digtot)
       {
        case 5:
          break;
        case 6:
          laux=(totalitzador_read(nt, &st)/100000L)%10;
          display(1,NF_LEDTOT,totmodif.text2);
          display(1,NF_TOT6,(unsigned char*)&laux);
          break;
        case 8:
        default:
          laux=(totalitzador_read(nt, &st)/100000L)%1000;
          display(1,NF_LEDTOT,totmodif.text2);
          display(1,NF_TOT8,(unsigned char*)&laux);
          break;
       }
    }
 DISP_FIXE(1);
 DISP_DIG_FLASH(1,totmodif.digit % 5);

}

void MT_CLR(void)
{
unsigned char  nt,nf;
unsigned long  laux;
int st;
 if(tarcom.totbormodif==0 || totmodif.digit>tarcom.digtot)
    return;               /* cas nomes pot visualitzar */
 nt=totmodif.ntot;
 nf=tot[nt].form;
 if(totmodif.digit<5)
    {
     laux = (totalitzador_read(nt, &st)/100000L)*100000L;
     totalitzador_write(nt,laux);
     display(1,nf,(unsigned char*)&laux);
    }
 else
    {
     laux = totalitzador_read(nt, &st) % 100000L;
     totalitzador_write(nt,laux);
     switch(tarcom.digtot)
       {
        case 5:
          break;
        case 6:
          laux=(totalitzador_read(nt, &st)/100000L)%10;
          display(1,NF_LEDTOT,totmodif.text2);
          display(1,NF_TOT6,(unsigned char*)&laux);
          break;
        case 8:
        default:
          laux=(totalitzador_read(nt, &st)/100000L)%1000;
          display(1,NF_LEDTOT,totmodif.text2);
          display(1,NF_TOT8,(unsigned char*)&laux);
          break;
       }
    }
 DISP_FIXE(1);
 if(tarcom.totbormodif==2)
    DISP_DIG_FLASH(1,totmodif.digit % 5);
}

void MT_SKIP(void)
{
 if(tarcom.totbormodif<2 || totmodif.digit>tarcom.digtot)
     return;
 switch(totmodif.digit)
  {
   case 0:
      totmodif.digit=4;
      break;
   case 5:
      totmodif.digit=tarcom.digtot-1;
      break;
   default:
      totmodif.digit--;
      break;
  }
 DISP_FIXE(1);
 DISP_DIG_FLASH(1,totmodif.digit % 5);
}



struct
{
 unsigned char digit;
 unsigned char modif;
 struct s_time time;
}  relmodif;


void MR_REL(void)
{

 timer50=0;      /* nomes serveix per entrar primera vegada */
 display(0,NF_LEDEST,(unsigned char*)&tarcom.vars_estat[E_MODREL].ledest);

 switch(tx30.subestat)
   {
    case 0:   /* any */
       memcpy(&relmodif.time.year,&gt.time.year,1);
       relmodif.modif=0;
       display(1,NF_ANY,(unsigned char *)&relmodif.time.year);
       DISP_DIG_FLASH(1,1);
       relmodif.digit=1;
       tx30.subestat++;
       break;
    case 1:  /*  mes-dia */
       DISP_FIXE(1);
       if(relmodif.modif)
          {
           /* cas any  modificat */
           relmodif.time.day=1;
           relmodif.time.month=1;
          }
       else
          memcpy(&relmodif.time.day,&gt.time.day,2);
       display(1,NF_DIA,(unsigned char *)&relmodif.time.day);
       display(2,NF_TEXT4,&tarcom.text_disem[dia_de_la_semana_from_time(&relmodif.time)].text[0]);
       DISP_DIG_FLASH(1,(ndigdisp_ddmm[3]));
       relmodif.digit=3;
       tx30.subestat++;
       break;
    case 2:  /* hora */
       DISP_FIXE(1);
       memcpy(&relmodif.time.minute,&gt.time.minute,2);
       display(1,NF_HORA,(unsigned char *)&relmodif.time.minute);
       DISP_DIG_FLASH(1,4);
       APAGAR_DISPLAY(2);
       relmodif.digit=3;
       tx30.subestat++;
       break;
    case 3:   /* final mod. rellotge */
       if(relmodif.modif)
          {
           relmodif.time.s100=0;
           relmodif.time.sec=0;
           confi_reloj(RELLOTGE_PLACA,0);
           set_reloj(RELLOTGE_PLACA,&relmodif.time);  /* reescriu fecha hora */
           tx30.ajuste_rel_last_anydia=0;       //v2.20b
           tx30.ajuste_rel_minuts=0;
           gt.time=relmodif.time;
	   	   inici_ajustar_1h();   /* prepara gestio +-1h */
           recalculs_gt();
           gt.canvi_fecha=1;
           
           rearmar_bloqueo_torn(REARMAR_GLOBAL, 0);

           gestio_fecha_paro();
          }
       fin_cods();
       break;
   }
}

void MR_FP(void)
{
 timer50=0;      /* nomes serveix per entrar primera vegada */
 display(0,NF_LEDEST,(void*)&tarcom.vars_estat[E_MODFP].ledest);

 switch(tx30.subestat)
   {
    case 0:   /* any */
       memcpy(&relmodif.time.day,&fecha_paro,3);
       relmodif.modif=0;
       display(1,NF_ANY,(unsigned char *)&relmodif.time.year);
       DISP_DIG_FLASH(1,1);
       relmodif.digit=1;
       tx30.subestat++;
       break;
    case 1:  /*  mes-dia */
       DISP_FIXE(1);
       if(relmodif.modif)
          {
           /* cas any  modificat.Inicia mes dia */
           relmodif.time.day=1;
           relmodif.time.month=1;
          }
       display(1,NF_DIA,(unsigned char *)&relmodif.time.day);
       display(2,NF_TEXT4,&tarcom.text_disem[dia_de_la_semana_from_time(&relmodif.time)].text[0]);
       DISP_DIG_FLASH(1,(ndigdisp_ddmm[3]));
       relmodif.digit=3;
       tx30.subestat++;
       break;
    case 2:   /* final mod. fecha paro */
       if(relmodif.modif)
        {
          memcpy(&fecha_paro,&relmodif.time.day,3);
          grabar_fp();
          leer_verif_fp();
          gestio_fecha_paro();
        }
       fin_cods();
       break;
   }
}

void MR_SKIP(void)
{
unsigned char  nd;
 nd=relmodif.digit;
 switch(tx30.subestat-1)
   {
    case 0:
        DISP_DIG_FIXE(1,nd);
        if(nd==0)
           nd=1;
        else
           nd=0;
        DISP_DIG_FLASH(1,nd);
        relmodif.digit=nd;
        break;
    case 1:
        DISP_DIG_FIXE(1,(ndigdisp_ddmm[nd]));
        if(nd==0)
           relmodif.digit=3;
        else
           --relmodif.digit;
        nd=relmodif.digit;
        DISP_DIG_FLASH(1,(ndigdisp_ddmm[nd]));
        break;
    case 2:
        if(nd>1)nd++;
        DISP_DIG_FIXE(1,nd);
        if(nd==0)
           relmodif.digit=3;
        else
           --relmodif.digit;
        nd=relmodif.digit;
        if(nd>1)nd++;
        DISP_DIG_FLASH(1,nd);
        break;
   }
}

const uchar  mod_nd[]={10,6,10,3};
 
void MR_INCR(void)
{
unsigned char  nd,nib;
unsigned char  nib0;
 relmodif.modif=1;
 nd=relmodif.digit;
 
 
 switch(tx30.subestat-1)
   {
    case 0:  /* entrada any */
      nib=get_nibble(nd,(unsigned char*)&relmodif.time.year);
      nib=(nib+1)%10;
      set_nibble(nib,nd,(unsigned char *)&relmodif.time.year);
      display(1,NF_ANY,(unsigned char *)&relmodif.time.year);
      break;
    case 1:      /* entrada mes dia */
      nib=get_nibble(nd,(unsigned char*)&relmodif.time.day);
      switch(nd)
         {
          case 0:    /* unitats dia */
            nib=(nib+1)%10;
            switch(get_nibble(1,(unsigned char*)&relmodif.time.day))
               {
                case 0:
                   if(nib==0)
                      nib=1;
                   break;
                case 1:
                   break;
                case 2:
                   if((relmodif.time.month==2)&&(bcd_bin(relmodif.time.year) & 0x03))
                       nib%=9;  /* cas febrer no traspas */
                   break;
                case 3:
                   nib%=2;
                   if(dias_mes[relmodif.time.month]==30)
                     nib=0;
                   break;
               }
            break;
          case 1: /* decenes dia */
           ++nib;
           if(relmodif.time.month==2)
              nib%=3;
            else
              nib%=4;
            switch(nib)
              {
               case 0:
                    set_nibble(1,0,(unsigned char*)&relmodif.time.day);
                 break;
               case 1:
               case 2:
               case 3:
                    set_nibble(0,0,(unsigned char*)&relmodif.time.day);
                 break;
              }
            break;
          case 2: /*  unitats mes */
            nib0=get_nibble(1,(unsigned char*)&relmodif.time.month);
            nib=(nib+1)%10;
            if(nib0==0)
              {
               if(nib==0)
                  ++nib;
              }
            else
              nib%=3;
            relmodif.time.day=1;
            break;
          case 3:
            nib=(nib+1)%2;
            if(nib==0)
               set_nibble(1,0,(unsigned char*)&relmodif.time.month);
            else
               set_nibble(0,0,(unsigned char*)&relmodif.time.month);
            relmodif.time.day=1;
            break;
         }
      set_nibble(nib,nd,(unsigned char*)&relmodif.time.day);
      display(1,NF_DIA,(unsigned char *)&relmodif.time.day);
      display(2,NF_TEXT4,&tarcom.text_disem[dia_de_la_semana_from_time(&relmodif.time)].text[0]);
      break;
    case 2:  /* modificacio hora-minut */
      nib=get_nibble(nd,(unsigned char*)&relmodif.time.minute);
      
      nib=(nib+1)%mod_nd[nd];
      
      switch(nd)
         {
          case 0:
          case 1:
            break;
          case 2:
            if(get_nibble(3,(unsigned char*)&relmodif.time.minute)==2)
               nib%=4;
            break;
          case 3:
            if((nib==2) && (get_nibble(2,(unsigned char*)&relmodif.time.minute)>3))
                set_nibble(0,2,(unsigned char*)&relmodif.time.minute);/*hora=20*/
            break;
         }
      set_nibble(nib,nd,(unsigned char*)&relmodif.time.minute);
      display(1,NF_HORA,(unsigned char *)&relmodif.time.minute);
      break;
   }
}
