/***************** tautrans.h ***************/

#ifndef  _INC_TAUTRANS
#define  _INC_TAUTRANS

/* array per call indirecte a semantiques */

typedef void(*SUB0)(void);

extern const SUB0 executar_semantica_TX30[];
extern const unsigned char trans_ini_TX30[19];
extern const int menu_estado_TX30[18];
extern const int luz_frontal[18];


struct s_taula_trans
{ 
	unsigned char event;
	unsigned char estat;
	unsigned char trans;
	unsigned short mask;
};

extern const struct s_taula_trans taula_trans_TX30[];

/* prototips semantiques */

extern void PASO_LIBRE          (void);
extern void ON_OFF_CARG_CERRT   (void);
extern void INICIO_CODS         (void);
extern void INI_TOTS            (void);
extern void LIBRE_OCUP          (void);
extern void LIBRE_UP            (void);
extern void LIBRE_ROT           (void);
extern void LIB_ROT_UP          (void);
extern void LIB_ROT_DWN         (void);
extern void VISU_MOD_RELOJ      (void);
extern void LLUM                (void);
extern void ESP_OFF             (void);
extern void LIBREOFF_OCAUT      (void);
extern void BLOQ_TEC            (void);
extern void reloj_en_libre      (void);
extern void test_impre          (void);
extern void OCUPADO_0           (void);
extern void OCUP_OCUP           (void);
extern void OCUP_UP             (void);
extern void OCUP_DWN            (void);
extern void OCUP_UPROT          (void);
extern void OCUP_DWNROT         (void);
extern void OCUP_PAGROT         (void);
extern void SUPL_TECLA          (void);
extern void OCUP_PAGAR          (void);
extern void flipflop_vd         (void);
extern void ERR_GENIMP          (void);
extern void FIN_ERR_GENIMP      (void);
extern void ROT_VIAJ            (void);
extern void TARARI              (void);
extern void SUMA_OCUP           (void);
extern void PAGAR_0             (void);
extern void PAGAR_OCUP          (void);
extern void SUMA                (void);
extern void PAGAR_UP            (void);
extern void PAGAR_DWN           (void);
extern void pag_lib_tecla       (void);
extern void pag_lib_temps       (void);
extern void pag_lib_dist        (void);
extern void ABORT_SUPL          (void);
extern void PAGAR_EURO          (void);
extern void PAGAR_OCUP_MF       (void);
extern void fin_reltots         (void);
extern void RELOJ_NEXT          (void);
extern void RELOJ_ACTUAL        (void);
extern void RELOJ_FIN           (void);
extern void AVANPAP_INCREREL    (void);
extern void MODIF_DECREREL      (void);
extern void OFF_0               (void);
extern void OFF_LIBRE           (void);
extern void DESBLOQ_TEC         (void);
extern void ESP_ON              (void);
extern void fin_cods            (void);
extern void TEST                (void);
extern void TESTIMPR            (void);
extern void TOTS                (void);
extern void TOTS_TR             (void);
extern void CODS                (void);
extern void CODS_PARMS_SIN_PASSW(void);
extern void MT_TOTS             (void);
extern void MT_SKIP             (void);
extern void MT_INCR             (void);
extern void MT_CLR              (void);
extern void MR_REL              (void);
extern void MR_SKIP             (void);
extern void MR_INCR             (void);
extern void MR_FP               (void);
extern void PARMS_0             (void);
extern void PARMS               (void);
extern void PARMS_K_ABORT       (void);
extern void PARMS_TR            (void);
extern void FS_0                (void);
extern void FS_FI_IMPRE         (void);
extern void FS_LIBRE            (void);
extern void FS_PLUS             (void);
extern void UP_UP               (void);
extern void UP_DWN              (void);
extern void UP_OCUPADO          (void);
extern void OFF_TREURE_TENSIO   (void);
extern void FS_DIST             (void);
extern void CERR_0              (void);
extern void CERR_TLIBRE         (void);
extern void ABRIR_TURNO_COND    (void);
extern void CERR_TREL           (void);
extern void CERR_TIMER          (void);
extern void TORN_REPE_TICKET    (void);
extern void CERR_TCODS          (void);
extern void CERR_TREURE_TENSIO  (void);
extern void MOD_TAR_TNUM        (void);
extern void APAGAR_LLUMINOS     (void);
extern void TANCAR_TORN         (void);
extern void PREPROG_TNUM        (void);
extern void OFF_TCODS           (void);
extern void FI_ITV              (void);
extern void FS_LIB_TEMPS        (void);
extern void PETICIO_PIN         (void);
extern void TORN_ACK_PASSWORD   (void);
extern void TORN_NACK_PASSWORD  (void);
extern void SUMA_SOLTAR  		(void);
extern void ACUM_PAUSA          (void);
extern void FI_ACUM_PAUSA       (void);
extern void TOTS_PASW     		(void);
extern void PREC_ACORDADO       (void);
extern void VAL_PREC_ACORD      (void);
extern void CANC_PREC_AC        (void);


/* LITERALES ESTADOS */
enum eEstatsTx30{E_MEM_MAL=0,E_LIBRE, E_OCUPADO,E_PAGAR,E_RELOJ,E_OFF,E_TEST,E_TOTS,E_CODS,
                  E_MODTOT,E_MODREL,E_MODFP,E_PARMS,E_FISERV,E_UP,E_CERRADO,E_MOD_TAR,E_PREPROG,ETX30_FIN};

/*
#define  E_MEM_MAL        0
#define  E_LIBRE          1
#define  E_OCUPADO        2
#define  E_PAGAR          3
#define  E_RELOJ          4
#define  E_OFF            5
#define  E_TEST           6
#define  E_TOTS           7
#define  E_CODS           8
#define  E_MODTOT         9
#define  E_MODREL        10
#define  E_MODFP         11
#define  E_PARMS         12
#define  E_FISERV        13
#define  E_UP            14
#define  E_CERRADO       15   // a partir de versio 1.03F
#define  E_MOD_TAR       16   // a partir de versio 1.04
#define  E_PREPROG       17   // a partir de versio 2.14
#define  ETX30_FIN       18
*/

/* LITERALES TRANSICIONES */
#define T_PASO_LIBRE            0
#define T_ON_OFF_CARG_CERRT     1
#define T_INICIO_CODS           2
#define T_INI_TOTS              3
#define T_LIBRE_OCUP            4
#define T_LIBRE_UP              5
#define T_LIBRE_ROT             6
#define T_LIB_ROT_UP            7
#define T_LIB_ROT_DWN           8
#define T_VISU_MOD_RELOJ        9
#define T_LLUM                 10
#define T_ESP_OFF              11
#define T_LIBREOFF_OCAUT       12
#define T_BLOQ_TEC             13
#define T_reloj_en_libre       14
#define T_test_impre           15
#define T_OCUPADO_0            16
#define T_OCUP_OCUP            17
#define T_OCUP_UP              18
#define T_OCUP_DWN             19
#define T_OCUP_UPROT           20
#define T_OCUP_DWNROT          21
#define T_OCUP_PAGROT          22
#define T_SUPL_TECLA           23
#define T_OCUP_PAGAR           24
#define T_flipflop_vd          25
#define T_ERR_GENIMP           26
#define T_FIN_ERR_GENIMP       27
#define T_ROT_VIAJ             28
#define T_TARARI               29
#define T_SUMA_OCUP            30
#define T_PAGAR_0              31
#define T_PAGAR_OCUP           32
#define T_SUMA                 33
#define T_PAGAR_UP             34
#define T_PAGAR_DWN            35
#define T_pag_lib_tecla        36
#define T_pag_lib_temps        37
#define T_pag_lib_dist         38
#define T_ABORT_SUPL           39
#define T_PAGAR_EURO           40
#define T_PAGAR_OCUP_MF        41
#define T_fin_reltots          42
#define T_RELOJ_NEXT           43
#define T_RELOJ_ACTUAL         44
#define T_RELOJ_FIN            45
#define T_AVANPAP_INCREREL     46
#define T_MODIF_DECREREL       47
#define T_OFF_0                48
#define T_OFF_LIBRE            49
#define T_DESBLOQ_TEC          50
#define T_ESP_ON               51
#define T_fin_cods             52
#define T_TEST                 53
#define T_TESTIMPR             54
#define T_TOTS                 55
#define T_TOTS_TR              56
#define T_CODS                 57
#define T_CODS_PARMS_SIN_PASSW 58
#define T_MT_TOTS              59
#define T_MT_SKIP              60
#define T_MT_INCR              61
#define T_MT_CLR               62
#define T_MR_REL               63
#define T_MR_SKIP              64
#define T_MR_INCR              65
#define T_MR_FP                66
#define T_PARMS_0              67
#define T_PARMS                68
#define T_PARMS_K_ABORT        69
#define T_PARMS_TR             70
#define T_FS_0                 71
#define T_FS_FI_IMPRE          72
#define T_FS_LIBRE             73
#define T_FS_PLUS              74
#define T_UP_UP                75
#define T_UP_DWN               76
#define T_UP_OCUPADO           77
#define T_OFF_TREURE_TENSIO    78
#define T_FS_DIST              79
#define T_CERR_0               80
#define T_CERR_TLIBRE          81
#define T_ABRIR_TURNO_COND     82
#define T_CERR_TREL            83
#define T_CERR_TIMER           84
#define T_TORN_REPE_TICKET     85
#define T_CERR_TCODS           86
#define T_CERR_TREURE_TENSIO   87
#define T_MOD_TAR_TNUM         88
#define T_APAGAR_LLUMINOS      89
#define T_TANCAR_TORN          90
#define T_PREPROG_TNUM         91
#define T_OFF_TCODS            92
#define T_FI_ITV               93
#define T_FS_LIB_TEMPS         94
#define T_PETICIO_PIN          95
#define T_TORN_ACK_PASSWORD    96
#define T_TORN_NACK_PASSWORD   97
#define T_SUMA_SOLTAR          98
#define T_ACUM_PAUSA		   99
#define T_FI_ACUM_PAUSA		   100
#define T_TOTS_PASW            101
#define T_PREC_ACORDADO		   102
#define T_VAL_PREC_ACORD	   103
#define T_CANC_PREC_AC		   104
#endif