// **************************************************************
// rutines.h
// **************************************************************


#ifndef __RUTINES_H
#define __RUTINES_H

extern void decripta_tx(unsigned char *bufer);


extern void memncpy(char* dest, char* src, int len);
extern void ASCII_EBCDIC(char* pCh, int len, int ascii_to_ebcdic);
extern unsigned int  achtoi(unsigned char* p, int len);
extern void  char_to_hexa(unsigned char ch,unsigned char* high,unsigned char* low);
extern unsigned long  minutos_to_hhmm(unsigned long minutos);
extern int es_str_valido(unsigned char* p, int maxlen);
extern unsigned char hexa_to_uchar(unsigned char* p);
extern unsigned short calcula_crc(unsigned char * buf, int nb);

extern void init_alloc_ifac(void);
extern void* malloc_ifac(unsigned int size);
extern void* calloc_ifac(unsigned int nelem, size_t elsize);

extern void get_string_sep_comas(char * orig, char * dest, int numCampo, int maxlen, int lenOrig);
extern int get_int_sep_comas(char * buffer, int numCampo, int lenOrig);
extern int getNumCampos(char * orig, int len);

extern int diff(int a, int b);
extern int base64(unsigned char * orig, char * dest, int length);
extern void decodB64( unsigned char * dest, char * orig,int nb);

#endif
