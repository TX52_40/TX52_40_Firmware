// *********************************************************************
// impre.h
// *********************************************************************

#ifndef __IMPRE_INC
#define __IMPRE_INC

#include "RTOS.h"

// status (or) respuesta get_status_impre(void)

#define IMPRE_IMPRIMIENDO  0x01
#define IMPRE_SINPAPEL     0x02
#define IMPRE_FALLO        0x04
#define IMPRE_FALLO_TRANSM 0x08

#define IMPREtip_no   0
#define IMPREtip_int  1
#define IMPREtip_ext  2
#define IMPREtip_Opt  3
#define IMPREtip_Kas  4

extern OS_Q ColaImpre;

extern void impre_init(int tipus, int tipus_td30);
extern int hay_impresora(void);
extern void imprimir_buffer(char *buf,int len, int add_ret);

extern void inici_ticket(int ret_event);
extern void final_ticket(void);


extern void impre_event_call_back(char ticket_ret_event, char ticket_ok);


extern char get_status_impre(void);
extern void impre_rcvd_status_mpxd(char ch);
extern char get_impre_imprimint(void);

extern void imprimir(char n_ticket);
extern void imprimir_ticket_no_legal(char *buf,int len);
extern void impre_off(void);
extern void impre_on();
extern void impre_font_size(int font);

#endif

