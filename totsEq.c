
#include "totsEq.h"
#include "reloj.h"
#include "tx30.h"
//#include "hard.h"

extern void calcul_tots_aux(void);

extern int tots_eq_init;
__no_init char semestre_actiu;

const unsigned int eq_tot[]={
	TOT_NSER,
	TOT_IMPT,
	TOT_KMT
};

const unsigned int eq_ant[]={
	TOT_DIA_ANT,	// TOT_DIA
	TOT_MES_ANT,	// TOT_MES
	TOT_PER_FIN,	// TOT_SEM
	TOT_ANY_ANT,	// TOT_ANY
	TOT_SEM1_ANT,	// TOT_SEM1
	TOT_SEM2_ANT,	// TOT_SEM2
	
};

void tot_eq_write(char ntot, char periode, unsigned long value){
	if (ntot >= TOT_EQ_FIN)
		return;
	tots_eq[periode].total_eq[ntot].value = value;
	tots_eq[periode].total_eq[ntot].value_compl = value ^ 0xffffffff;
}

void tot_eq_reset(char ntot, char periode){
	int st;
	if(ntot >= TOT_EQ_FIN)
		return;
	tots_eq[periode].total_eq[ntot].value = totalitzador_read(eq_tot[ntot], &st);
	tots_eq[periode].total_eq[ntot].value_compl = tots_eq[periode].total_eq[ntot].value ^ 0xffffffffL;
}

void tot_eq_zero(char ntot, char periode){

	if(ntot >= TOT_EQ_FIN)
		return;
	tots_eq[periode].total_eq[ntot].value = 0;
	tots_eq[periode].total_eq[ntot].value_compl = 0xffffffffL;
}

void copia_tot_sem(void){
	int i;
	for (i = 0; i < TOT_EQ_FIN; i++){
		if (semestre_actiu == 1)
			tot_eq_write(i, TOT_SEM1, tot_eq_read(i,TOT_SEM));
		else if (semestre_actiu == 2)
			tot_eq_write(i, TOT_SEM2, tot_eq_read(i,TOT_SEM));
	}
}

unsigned long tot_eq_read(char ntot, char periode){
	int st;
	calcul_tots_aux();
	if ((tots_eq[periode].total_eq[ntot].value ^ tots_eq[periode].total_eq[ntot].value_compl) == 0xffffffffL){
		if (periode >= TOT_SEM1)
			return tots_eq[periode].total_eq[ntot].value;
		else{
			int value = totalitzador_read(eq_tot[ntot], &st);
			if (value == 99999999L)
				return 99999999L;
			value -= tots_eq[periode].total_eq[ntot].value;
			return  ((value > 0) ? value : 0);
		}
	}
	else
		return(99999999L);
}

void borra_tot_eq(char periode){
	int i;
	calcul_tots_aux();
	for(i=0;i<TOT_EQ_FIN;i++){
		if (periode <= TOT_ANY)
			tot_eq_reset(i, periode);
		else
			tot_eq_zero(i, periode);
	}
}

unsigned long posar_inici_dia(T_FECHHOR *data_aux){
	time_to_fechhor(data_aux,&gt.time);
	data_aux->hour = 0;
	data_aux->minute = 0;
	data_aux->sec = 0;
	return getSegundosFromFecha(data_aux);
}

unsigned long posar_inici_mes(T_FECHHOR *data_aux){
	posar_inici_dia(data_aux);
	data_aux->day = 1;
	return getSegundosFromFecha(data_aux); 
}

unsigned long posar_inici_any(T_FECHHOR *data_aux){
	posar_inici_mes(data_aux);
	data_aux->month = 1;
	return getSegundosFromFecha(data_aux);
}

void reset_tot_eq(char periode){
	// Inicialitzar dates d'inici i final
	T_FECHHOR data_aux;
	unsigned long segons_ini, segons_ini_aux;
	switch(periode){
	case TOT_DIA:
		// S'inicialitza a l'inici del dia
		segons_ini = posar_inici_dia(&data_aux);
		tots_eq[periode].timeIni = segons_ini;	// Inici, hora del canvi
		tots_eq[periode].timeFi = segons_ini + SEGONS_DIA;
		break;
	case TOT_MES:
		// S'inicialitza a l'inici del mes
		segons_ini = posar_inici_mes(&data_aux);
		tots_eq[periode].timeIni = segons_ini;	// Inici, hora del canvi
		tots_eq[periode].timeFi = segons_ini + segonsDelMes(data_aux.month, data_aux.year);
		break;
	case TOT_SEM:
		// S'inicialitza depenent del semestre actiu
		segons_ini_aux = posar_inici_dia(&data_aux);
		segons_ini = posar_inici_any(&data_aux);
		if (segons_ini_aux < segons_ini + segonsDelSemestre(1, data_aux.year)){
			semestre_actiu = 1;
			tots_eq[periode].timeIni = segons_ini;	// Inici, hora del canvi
			tots_eq[periode].timeFi = segons_ini + segonsDelSemestre(1, data_aux.year);
		}
		else{
			semestre_actiu = 2;
			tots_eq[periode].timeIni = segons_ini + segonsDelSemestre(1, data_aux.year);
			tots_eq[periode].timeFi = segons_ini + segonsDelAny(data_aux.year);
		}
		break;
	case TOT_SEM1:
		// S'inicialitza a l'inici de l'any
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini;	// Inici, hora del canvi
		tots_eq[periode].timeFi = segons_ini + segonsDelSemestre(1, data_aux.year);
		break;
	case TOT_SEM2:
		// S'inicialitza a l'inici de l'any
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini + segonsDelSemestre(1, data_aux.year);
		tots_eq[periode].timeFi = segons_ini + segonsDelAny(data_aux.year);
		break;
	case TOT_ANY:
		// S'inicialitza a l'inici de l'any
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini;	// Inici, hora del canvi
		tots_eq[periode].timeFi = segons_ini + segonsDelAny(data_aux.year);
		break;
	case TOT_DIA_ANT:
		// S'inicialitza quan hi ha hagut un dia d'inactivitat
		segons_ini = posar_inici_dia(&data_aux);
		tots_eq[periode].timeIni = segons_ini - SEGONS_DIA;
		tots_eq[periode].timeFi = segons_ini;
		break;
	case TOT_MES_ANT:
		// S'inicialitza quan hi ha hagut un mes d'inactivitat
		segons_ini = posar_inici_mes(&data_aux);
		if (data_aux.month > 1) tots_eq[periode].timeIni = segons_ini - segonsDelMes(data_aux.month - 1, data_aux.year);
		else tots_eq[periode].timeIni = segons_ini - segonsDelMes(12, data_aux.year - 1);
		tots_eq[periode].timeFi = segons_ini;
		break;
	case TOT_SEM1_ANT:
		// S'inicialitza quan hi ha hagut un any d'inactivitat
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini - segonsDelAny(data_aux.year - 1);
		tots_eq[periode].timeFi = segons_ini - segonsDelSemestre(2, data_aux.year - 1);
		break;
	case TOT_SEM2_ANT:
		// S'inicialitza quan hi ha hagut un any d'inactivitat
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini - segonsDelSemestre(2, data_aux.year - 1);
		tots_eq[periode].timeFi = segons_ini;
		break;
	case TOT_ANY_ANT:
		// S'inicialitza quan hi ha hagut un any d'inactivitat
		segons_ini = posar_inici_any(&data_aux);
		tots_eq[periode].timeIni = segons_ini - segonsDelAny(data_aux.year - 1);
		tots_eq[periode].timeFi = segons_ini;
		break;
	}
	// Borra totalitzadors
	borra_tot_eq(periode);
}

void borra_tots_eq(void){
	int i;
	for (i = 0;i < TOT_PER_FIN; i++){
			reset_tot_eq(i);
		}
		tots_eq_init = 0xABCD;
}

void arxiva_tot_eq(char periode){
	int i;
	if (periode <= TOT_DIA_ANT){
		for (i = 0; i < TOT_EQ_FIN; i++){
			if (periode == TOT_SEM){
				// Guardem el valor del semestre actual al semestre que correspon
				if (semestre_actiu == 1)
					tot_eq_write(i, TOT_SEM2, tot_eq_read(i,periode));
				else if (semestre_actiu == 2)
					tot_eq_write(i, TOT_SEM1, tot_eq_read(i,periode));
			}
			else
				// Altres totalitzadors
				tot_eq_write(i, eq_ant[periode], tot_eq_read(i,periode));
		}
	}
	tots_eq[eq_ant[periode]].timeFi = tots_eq[periode].timeFi;
	tots_eq[eq_ant[periode]].timeIni = tots_eq[periode].timeIni;
	reset_tot_eq(periode);
}

void calcula_semestre_actiu(unsigned long segons){
	if (segons > tots_eq[TOT_SEM1].timeFi)
		semestre_actiu = 1;
	else
		semestre_actiu = 2;
}


void comprova_totalitzadors_eq(void){
	T_FECHHOR data_aux;
	if (!tarcom.tots_historics) return;
	unsigned long segons_ini = posar_inici_dia(&data_aux);
	if (segons_ini < tots_eq[TOT_DIA].timeIni || tots_eq_init != 0xABCD){
		// Totalitzadors no inicialitzats o hem tirat el rellotge enrere
		// Calculem totes les dates d'inici i els posem a 0
		borra_tots_eq();
	}
	if (semestre_actiu != 1 && semestre_actiu != 2) calcula_semestre_actiu(segons_ini);
	if (tots_eq[TOT_DIA].timeFi <= segons_ini){
		// Hem canviat de dia
		if (tots_eq[TOT_DIA].timeFi + SEGONS_DIA <= segons_ini){
			// Ha passat un dia d'inactivitat, borrem totalitzadors del dia i dia anterior, posant les dates d'inici i fi correctes
			reset_tot_eq(TOT_DIA);
			reset_tot_eq(TOT_DIA_ANT);
		}
		else{
			// Hi ha hagut canvi de dia, estem al dia seg�ent
			arxiva_tot_eq(TOT_DIA);
		}
		
		// Mirem si hem canviat de mes
		if (tots_eq[TOT_MES].timeFi <= segons_ini){
			// Hem canviat de mes
			int segons_mes_passat;
			if (data_aux.month > 1) segons_mes_passat = segonsDelMes(data_aux.month - 1, data_aux.year);
			else segons_mes_passat = segonsDelMes(12, data_aux.year - 1);
			if (tots_eq[TOT_MES].timeFi + segons_mes_passat <= segons_ini){
				// Hem passat un mes sencer d'inactivitat, borrem totalitzadors del mes i mes passat, posant les dates d'inici i fi correctes
				reset_tot_eq(TOT_MES);
				reset_tot_eq(TOT_MES_ANT);
			}
			else{
				// Hi ha hagut canvi de mes, estem al mes seg�ent
				arxiva_tot_eq(TOT_MES);
			}
			
			//Mirem si hem canviat de semestre
			// Mirem a quin semestre estem
			if (segons_ini >= tots_eq[TOT_SEM1].timeFi){
				// Arxivem dades del semestre
				if (semestre_actiu == 1){
					semestre_actiu = 2;
					arxiva_tot_eq(TOT_SEM);
				}
			}
			if (segons_ini >= tots_eq[TOT_SEM2].timeFi){
				// Arxivem dades del semestre
				if (semestre_actiu == 2){
					semestre_actiu = 1;
					arxiva_tot_eq(TOT_SEM);
				}
			}
			
			// Mirem si hem canviat d'any
			if (tots_eq[TOT_ANY].timeFi <= segons_ini){
				// Hem canviat d'any
				if (tots_eq[TOT_ANY].timeFi + segonsDelAny(data_aux.year) <= segons_ini){
					// Hem passat un any sencer d'inactivitat, borrem totalitzadors del any i any passat, posant les dates d'inici i fi correctes
					reset_tot_eq(TOT_ANY);
					reset_tot_eq(TOT_ANY_ANT);
					// Borrem tamb� totalitzadors semestrals
					reset_tot_eq(TOT_SEM);
					reset_tot_eq(TOT_SEM1);
					reset_tot_eq(TOT_SEM2);
					reset_tot_eq(TOT_SEM1_ANT);
					reset_tot_eq(TOT_SEM2_ANT);
				}
				else{
					// Estem a l'any seg�ent arxivem tots els totalitzadors anuals
					arxiva_tot_eq(TOT_SEM1);
					arxiva_tot_eq(TOT_SEM2);
					arxiva_tot_eq(TOT_ANY);
				}
			}
			
		}
	}
}
