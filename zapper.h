// ********************************************************
// zapper.h
// ********************************************************

extern unsigned char METRO_ZAPPER; 

extern void zapper_reset_bloqueig(void);
extern void zapper_init(void);    
extern void zapper_interrupt_1ms(void);
extern void zapper_loop(void);
extern void zapper_canvi_segon(void);

extern void zapper_activar(void);
extern void zapper_desactivar(void);

extern int zapper_bloqueja_equip(void);
extern int zapper_error_en_curs(void);
extern int zapper_hi_ha_zapper(void);
extern int zapper_esta_activat(void);


typedef void (*ZAPPER_INT)(void);

extern ZAPPER_INT zapper_1ms[3];
