//*********************************************************
// reloj.c
//*********************************************************

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "reloj.h"
#include "RTOS.h"
#include "i2c.h"


/*
#include "defines.h"              
#include "hard.h"
#include "rutinas.h"
#include "rtos8x.h"
#include "eprom.h"
#include "credcard.h"
*/

#define MAX_SEGONS_ENTRE_LECTURES 60

extern unsigned char bin_bcd(unsigned char);
extern unsigned char bcd_bin(unsigned char);


// variable per gestio error en lectura rellotge
struct s_rellotge rellotge;



OS_RSEMA SemaReloj;

static const char dias_mes[]=
{
 0,31,28,31,30,31,30,31,31,30,31,30,31,
};

void reloj_init(void)
{
  OS_CREATERSEMA(&SemaReloj);
  rellotge.primera_lectura = 1;
}


char get_dias_mes(unsigned char month,unsigned char year)
{
char ndm;
 ndm=dias_mes[month];
 if((month==2)&&((year%4)==0))
   ++ndm;
   return(ndm);
}

char fecha_logica(T_FECHHOR *f)
{
    if(f->year>99) 
        return 0;
    if((f->month<1) || (f->month>12))
        return 0;
    if((f->day<1) || (f->day>get_dias_mes(f->month,f->year)))
        return 0;
    if(f->hour>23)
        return 0;
    return 1;
}

void confi_reloj(int nChip, char mask)
{
char buf;
  switch(DEF_CHIP_I2C[nChip].tipus)
  {
    case RELLOTGE_NOU:
        break;
    case RELLOTGE_VELL:
        buf=mask;
        OS_Use(&SemaReloj);
        write_i2c(nChip, 0, 1, &buf);
        OS_Unuse(&SemaReloj);
        break;
  }
}

char get_confi_reloj(int nChip)
{
char  stat;
  switch(DEF_CHIP_I2C[nChip].tipus)
  {
    case RELLOTGE_NOU:
        stat=0;
        break;
    case RELLOTGE_VELL:
        OS_Use(&SemaReloj);
        wread_i2c(nChip,0,1,&stat);
        OS_Unuse(&SemaReloj);
        break;
  }
  return(stat);
}


void set_reloj(int nChip, struct s_time *time)
{
char status;
unsigned char aux;
char buf[7];

  memset(buf, 0, sizeof(buf));
  OS_Use(&SemaReloj);

  switch(DEF_CHIP_I2C[nChip].tipus)
  {
    case RELLOTGE_NOU:
        buf[0]=time->sec;											
        buf[1]=time->minute;
        buf[2]=time->hour;
        buf[3]=time->day;
        buf[5]=time->month;
        buf[6]=time->year;
        wread_i2c(nChip,0,1,&status);
        status |= 0x20;
        write_i2c(nChip,0,1,&status);   // stop counter 
        write_i2c(nChip,2,7,buf);
        status &= 0xdf;
        write_i2c(nChip,0,1,&status);   // start counter 
        rellotge.primera_lectura = 1;
        break;
    case RELLOTGE_VELL:
        memcpy(buf,(unsigned char *)time,6);
        aux=bcd_bin(time->year);
        buf[4] |=(aux << 6);                    /* inclou any (0-3) */
        /*        buf[5] |=(time->wday << 5);              inclou weak day  */
        wread_i2c(nChip,0,1,&status);
        status |= 0x80;
        write_i2c(nChip,0,1,&status);   /* stop counter */
        write_i2c(nChip,1,6,buf);
        status &= 0x3f;
        write_i2c(nChip,0,1,&status);   /* start counter */
        buf[0]=aux & 0x03;
        buf[1]=aux & 0xfc;
        write_i2c(nChip,0x10,2,buf);   /* guarda any(0-3) i any sense 0-3 (en binari) */
        break;
		
  }
  OS_Unuse(&SemaReloj);
}


void get_reloj(int nChip,struct s_time *buf)
{
struct s_time8563 buf2;
unsigned char  baux[2],aux;
int num_segons,num_segons_aux,i;
volatile int err;

  OS_Use(&SemaReloj);

  switch(DEF_CHIP_I2C[nChip].tipus)
  {
    case RELLOTGE_NOU:
      err = 1;
      for(i=0;i<3;i++)  // loop per repetir lectura fins a tres vegades si detecta error
      {
        wread_i2c(nChip,2,7,(char *)&buf2);
        buf2.sec &= 0x7f;
        buf2.minute &= 0x7f;
        buf2.hour &= 0x3f;
        buf2.day &= 0x3f;
        buf2.month &= 0x1f;
        
        // verifica data correcta
        if(((buf2.sec & 0x0f) > 9) || (buf2.sec > 0x59))
          continue;
        if(((buf2.minute & 0x0f) > 9) || (buf2.minute > 0x59))
          continue;
        if(((buf2.hour & 0x0f) > 9) || (buf2.hour > 0x23))
          continue;
        if(((buf2.day & 0x0f) > 9) || (buf2.day > 0x31) || (buf2.day == 0))
          continue;
        if(((buf2.month & 0x0f) > 9) || (buf2.month > 0x12) || (buf2.month == 0))
          continue;
        num_segons = (bcd_bin(buf2.hour)*60 + bcd_bin(buf2.minute))*60 + bcd_bin(buf2.sec);
        if(!rellotge.primera_lectura)
        {
          num_segons_aux = num_segons;
          if(num_segons_aux < rellotge.segons_lectura_anterior)
            num_segons_aux += 86400;
          if((num_segons_aux - rellotge.segons_lectura_anterior) > MAX_SEGONS_ENTRE_LECTURES)
            continue;
        }
        err = 0;
        break;
      }
      if(err)
      {
        err = err+5;
        err = err * 3;
      }
      buf->sec = buf2.sec;
      buf->minute = buf2.minute;
      buf->hour = buf2.hour;
      buf->day = buf2.day;
      buf->month = buf2.month;
      buf->year = buf2.year;
      buf->s100 = 0;
      rellotge.primera_lectura = 0;
      rellotge.segons_lectura_anterior = num_segons;
      break;
    case RELLOTGE_VELL:
      wread_i2c(nChip,1,6,(char *)buf);
      buf->hour &=0x3f;
      aux=buf->day>>6;
      buf->day &= 0x3f;
      buf->month &= 0x1f;
      wread_i2c(nChip,0x10,2,(char *)baux);
      
      if(aux<baux[0])
      {             /* cas passat volta contador 0-3 */
              aux+=4;
      }
      buf->year=bin_bcd((aux+baux[1])%100);

      break;
  }
        OS_Unuse(&SemaReloj);
}




void get_reloj_fechhor(int nChip,T_FECHHOR *buf)
{
t_time btime;
	get_reloj(nChip,&btime);
	buf->minute=bcd_bin(btime.minute);
	buf->hour  =bcd_bin(btime.hour);
	buf->day   =bcd_bin(btime.day);
	buf->month =bcd_bin(btime.month);
	buf->year  =bcd_bin(btime.year);
	buf->sec   =bcd_bin(btime.sec);
}


void time_to_fechhor(T_FECHHOR* dest, t_time* org)
{
	dest->minute=bcd_bin(org->minute);
	dest->hour  =bcd_bin(org->hour);
	dest->day   =bcd_bin(org->day);
	dest->month =bcd_bin(org->month);
	dest->year  =bcd_bin(org->year);
	dest->sec   =bcd_bin(org->sec);
}

int fecha_vencida(T_FECHHOR* ahora,T_FECHHOR* fecha,unsigned int igual)
{
unsigned char a_1,a_2;
	a_1=ahora->year;
	if(a_1<50)
		a_1+=100;
	a_2=fecha->year;
	if(a_2<50)
		a_2+=100;
	if(a_2>a_1)
		return(0);
	if(a_2<a_1)
		return(1);
	a_1=ahora->month;
	a_2=fecha->month;
	if(a_2>a_1)
		return(0);
	if(a_2<a_1)
		return(1);
	if(igual)
	{
		if(ahora->day<=fecha->day)
			return(0);
	}
	else
	{
		if(ahora->day<fecha->day)
			return(0);
	}
	return(1);
}

int fecha_hora_vencida(T_FECHHOR* ahora,T_FECHHOR* fecha)
{
	if(fecha->year > ahora->year)
		return(0);
	if(fecha->year < ahora->year)
		return(1);
	if(fecha->month > ahora->month)
		return(0);
	if(fecha->month < ahora->month)
		return(1);
	if(fecha->day > ahora->day)
		return(0);
	if(fecha->day < ahora->day)
		return(1);
	if(fecha->hour > ahora->hour)
		return(0);
	if(fecha->hour < ahora->hour)
		return(1);
	if(fecha->minute >= ahora->minute)
		return(0);
	return(1);
}


unsigned long segons_interval(T_FECHHOR* ffin,T_FECHHOR* fini)
{
unsigned long ini,fin;
	fin=ffin->hour*3600L+ffin->minute*60+ffin->sec;
	ini=fini->hour*3600L+fini->minute*60+fini->sec;
	if(fin<ini)
		fin+=(24*3600L);
	return(fin-ini);
}

int minuts_interval(T_FECHHOR* ffin,T_FECHHOR* fini)
{
  return((int)(segons_interval(ffin,fini)/60L));
}


void sumar_dias_a_fecha(T_FECHHOR* fe, int numdias)
{
int diaux;
    diaux=((int)fe->day) + numdias;
    while(diaux>((int)get_dias_mes(fe->month,fe->year)))
    {
      diaux-=get_dias_mes(fe->month,fe->year);
      fe->month++;
      if(fe->month>12)
      {
          fe->month=1;
          fe->year++;
          if(fe->year>=100)
                  fe->year-=100;
      }
    }
    fe->day = diaux;
}


// Variables no externes del m�dul per +/- 1 hora
__no_init t_time gtime;
__no_init t_ajuste_hor next_ajuste_hor;
__no_init unsigned char punter_ajuste_hor;

// Variables externes del m�dul per +/- 1 hora
__no_init t_ajuste_hor ajuste_hor[NUM_AJUSTS_HORA];
/*************************************************************/
/* prepara punters i buffer comparacio per +- 1h             */
/*************************************************************/
void inici_ajustar_1h(void)
{
unsigned char canvi;
unsigned char i;
signed char j;
	get_reloj(RELLOTGE_PLACA,&gtime);
	for(i=0,canvi=0;i<NUM_AJUSTS_HORA;i++)  /* loop 6 fechas canvi  */
	{
		for(j=3;(j>=0) && ajuste_hor[i].tope[j]==*((unsigned char *)&gtime.hour+j);j--);
		if((j<0) || ajuste_hor[i].tope[j] < *((unsigned char *)&gtime.hour+j))
			continue;  /* compara seguent tope */
		canvi=1;
		break; /* ha trobat tope */
	}
	if(canvi)
	{
		next_ajuste_hor=ajuste_hor[i];
		punter_ajuste_hor=i;
	}
	else
	{
		next_ajuste_hor.tope[3]=0xff;  /* per que no canvii mai */
		punter_ajuste_hor=NUM_AJUSTS_HORA-1;
	}
}

unsigned char arrivat_tope(unsigned char nb, unsigned char *b1, unsigned char *b2)
{
unsigned char i;
	i=nb;
	while ((i>1) && ((*b1)==(*b2)))
	{
		b1--;
		b2--;	                
		i--;
	}        
	return (((*b1)>=(*b2)) ? 1:0);
}

/*************************************************************/
/*  mira si cal i ajusta +- 1 hora                           */
/* retorn = 1  si s'ha fet ajust                             */
/*************************************************************/
unsigned char gestio_ajustar_1h(void)
{
unsigned char hora,dia,mes,any;
// if(fallo_tension())
//   return(0);
  get_reloj(RELLOTGE_PLACA,&gtime);
  if(arrivat_tope(4,&gtime.year,&next_ajuste_hor.tope[3]))
    {
     hora=bcd_bin(gtime.hour);
     if(ajuste_hor[punter_ajuste_hor].tipo==0)
     {  
     	if (hora==23)
     	{
     		hora=0;
     		dia=bcd_bin(gtime.day);
     		mes=bcd_bin(gtime.month);
                any=bcd_bin(gtime.year);
                if (dia<((((any%4)==0)&&(mes==2)) ? (29) : dias_mes[mes]))
                  gtime.day=bin_bcd(++dia);
                else
                {    
                    gtime.day=1;
                    if (mes<12)
                      gtime.month=bin_bcd(++mes);
                    else
                    {
                            gtime.month=1;
                            gtime.year=bin_bcd(++any);
                    }
		}
     	}
     	else
     		++hora;
     }
     else
     {
        if (hora==0)
        {
     		hora=23;
     		dia=bcd_bin(gtime.day);
     		mes=bcd_bin(gtime.month);
			any=bcd_bin(gtime.year);
     		if (dia>1)
     			gtime.day=bin_bcd(--dia);
     		else
     		{
                    if (mes>1)
                    {
                      mes--;
                      dia=((((any%4)==0)&&(mes==2)) ? (29) : dias_mes[mes]);
                      gtime.day=bin_bcd(dia);
                      gtime.month=bin_bcd(mes);
                    }
                    else
                    {
                        gtime.day=bin_bcd(dias_mes[12]);
                        gtime.month=bin_bcd(12);
                        gtime.year=bin_bcd(--any);
                    }
     		}	
        }   
        else
     		--hora;
     }
     gtime.hour=bin_bcd(hora);
     set_reloj(RELLOTGE_PLACA,&gtime);

     if(punter_ajuste_hor<(NUM_AJUSTS_HORA-1))
       {
        ++punter_ajuste_hor;
        next_ajuste_hor=ajuste_hor[punter_ajuste_hor];
       }
     else
       {
        next_ajuste_hor.tope[3]=0xff;  /* per que no canvii mai */
        punter_ajuste_hor=NUM_AJUSTS_HORA-1;
       }
     return(1);
    }
 return(0);
}


unsigned int get_dia_del_anyo(T_FECHHOR* fe)
{
unsigned int retorn=0;
	if(fe->month)
	{
		retorn=(unsigned int)fe->day + (unsigned int)get_dias_mes(fe->month, fe->year);
		if((fe->month>2) && (!(fe->year%4)))
			retorn++;
	}
	return retorn;
}


/***************************************************************/
/*   calcula dia del any   (1-1 es dia 1)                      */
/***************************************************************/
const unsigned int  a_d_m[12]={0,31,59,90,120,151,181,212,243,273,304,334};
unsigned int dia_del_any_from_bcd(unsigned char * dmy)
{
unsigned int  aux;
 aux=bcd_bin(dmy[0])+a_d_m[bcd_bin(dmy[1])-1];
 if((dmy[1]>2)&& !(bcd_bin(dmy[2]) % 4) )
    aux++;
 return(aux);
}

unsigned int dia_del_segle_from_bcd(unsigned char * dmy)
{
unsigned int  aux;
 aux=dia_del_any_from_bcd(dmy)+bcd_bin(dmy[2])*365+((bcd_bin(dmy[2])+3) / 4);
 return(aux);
}

int dia_del_any_s(unsigned long segons)
{
    /* Procediment: es divideix el temps en grups de 4 anys. Des de 1970 tenim:
        - No trasp�s, no trasp�s, trasp�s, no trasp�s.  
        - Cada grup s�n 365*4 + 1 dies.
        - Cada dia s�n 24*3600 segons
        - aux1 indica el n�mero de dia dins el grup de 4 anys (comen�ant per 0)
        - aux2 indica el n�mero d'any dins el grup (comen�ant per 0)
        - Per trobar el numero de dia dins l'any actual, s'agafa el n�mero de dia dins el grup i se li resta els dies dels anys que ja han passat completament.
        - Es t� en compte que per al 3r i 4t any es necessita considerar el dia addicional de l'any de trasp�s.
        - Es suma un dia per comen�ar a comptar per 1.
    */
    unsigned long aux1, aux2;
    
    aux1 = (segons/(24*3600))%(365*4 + 1);
    aux2 = aux1/365;
    aux2 -= ((aux1 == 365*3 || aux1 ==  365*4) ? 1 : 0);
    return (aux1 - aux2*365 - aux2/3 + 1);
}


/***************************************************************/
/* calcul dia de la semana  (dilluns=0 diumenge=6)             */
/***************************************************************/
unsigned char dia_de_la_semana_from_time(struct s_time *time)
{
unsigned int  aux;
 aux=bcd_bin(time->year);
 if(aux<90)
   aux+=12;
 else
   aux-=88;
 aux=(aux+3)/4+aux;
 aux=(aux+3+dia_del_any_from_bcd(&time->day)) % 7;
 return ((unsigned char)aux);
}

void suma_minuts_a_hora_minut(unsigned long minuts, struct s_time* time, unsigned char* dst_minute)
{
  dst_minute[1] = bcd_bin(time->hour) + (minuts/60);    // hora
  dst_minute[0] = bcd_bin(time->minute) + (minuts % 60);  // minut
  if(dst_minute[0] >= 60)
  {
    dst_minute[0] -= 60;
    dst_minute[1]++;
  }
  if(dst_minute[1] >= 24)
    dst_minute[1] -= 24;
  
  dst_minute[0] = bin_bcd(dst_minute[0]);
  dst_minute[1] = bin_bcd(dst_minute[1]);
}

// calcula minut del segle a partir time
int minut_del_segle_from_time(t_time* time)
{
int iaux;
  iaux = ((dia_del_segle_from_bcd(&time->day)-1)*1440L)+bcd_bin(time->hour)*60+bcd_bin(time->minute);
  return iaux;  
}

void get_data_from_minuts(long minuts, T_FECHHOR* fecha)
{
long laux;  
  fecha->day = 1; 
  fecha->month = 1; 
  fecha->year = 0; 
  fecha->sec = 0;
  
  laux = 24 * 60 * (365*4 + 1);  // minuts d'un quatrieni
  while(minuts >= laux)
  {
    fecha->year += 4;
    minuts -= laux;
  }
  while(minuts >= (laux = ((fecha->year % 4) == 0) ? 366*24*60 : 365*24*60))
  {
    fecha->year++;
    minuts -= laux;
  }
  while(minuts >= (laux = get_dias_mes(fecha->month,fecha->year)*24*60))
  {
    fecha->month++;
    minuts -= laux;
  }
  
  laux = 24*60;
  fecha->day += (minuts / laux);  
  minuts %= laux;
  
  fecha->hour = minuts / 60;   
  fecha->minute = minuts % 60;   
    
}

void minuts200_to_hhmm(long minuts, unsigned char* buf)
{
T_FECHHOR fecha;
  get_data_from_minuts(minuts, &fecha);
  buf[1] = bin_bcd(fecha.minute);
  buf[0] = bin_bcd(fecha.hour);
}
void minuts200_to_hhmm_reversed(long minuts, unsigned char* buf)
{
T_FECHHOR fecha;
  get_data_from_minuts(minuts, &fecha);
  buf[0] = bin_bcd(fecha.minute);
  buf[1] = bin_bcd(fecha.hour);
}

void fechhor_to_tm(T_FECHHOR *pfecha, struct tm *ts){
	ts->tm_hour = pfecha->hour;
	ts->tm_min = pfecha->minute;
	ts->tm_sec = pfecha->sec;
	ts->tm_year = pfecha->year + 100;
	ts->tm_mon = pfecha->month - 1;
	ts->tm_mday = pfecha->day;
	ts->tm_isdst = -1;
}


// retorna los segundos desde  1/1/1970 00:00:00 hasta pfecha; pfecha ha de ser >= 1/1/2000
unsigned long getSegundosFromFecha(T_FECHHOR *pfecha)
{
	unsigned long lSegundos=0L;
	//unsigned int aux,any,i;
	//aux=(unsigned int)pfecha->year;
	//any=2000+(aux%100);
	
	
	struct tm  ts;
	time_t t;
	
	fechhor_to_tm(pfecha, &ts);
	t = mktime(&ts);
	lSegundos = t;
	
	
	return lSegundos;
}


signed long segonsDelMes(unsigned char cMonth, unsigned char nYear){
	return(get_dias_mes(cMonth, nYear)*3600*24);
}

signed long segonsDelSemestre(int num, unsigned char nYear){
	if (num == 1) return (3600 * 24 * ((nYear%4) ? 181 : 182));
	else return (3600 * 24 * 184);
}

signed long segonsDelAny(unsigned char nYear){
	return(3600 * 24 * ((nYear%4) ? 365 : 366));
}

void tm_to_fechhor(T_FECHHOR *pfecha, struct tm *ts){
	pfecha->hour = ts->tm_hour;
	pfecha->minute = ts->tm_min;
	pfecha->sec = ts->tm_sec;
	pfecha->year = ts->tm_year - 100;
	pfecha->month = ts->tm_mon + 1;
	pfecha->day = ts->tm_mday;
}

void getFechaFromSegundos(unsigned long lSegundos, T_FECHHOR *pfecha){
	// retorna el struct time_t segun lSegundos desde 1/1/1970 00:00:00
	//unsigned long laux;
	
	time_t t = lSegundos;
	
	struct tm ts = *localtime(&t);
	
	tm_to_fechhor(pfecha, &ts);
}
