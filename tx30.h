//******************************************************
// tx30.h    programa gesti� tx30
//******************************************************

#ifndef _INC_TX30
#define _INC_TX30


#define uchar unsigned char
#define uint unsigned int
#define ulong unsigned long

#include "tautrans.h"
#include "reloj.h"

#define  FACTOR_HZ  10    // per test_conector


enum eSubestatsTorn{TORN_TANCAT =0, TORN_ESPERA_OBRIR, TORN_ESPERA_TANCAR, TORN_TRIAR_OPCIO, TORN_TRIAR_TORN};
enum eSubestatsObrirTorn{ENTRADA_CONDUCTOR=0, ENTRADA_PASSWORD, ESPERA_RESPOSTA};


typedef struct s_tx30
{
enum  eEstatsTx30 estat;
enum  eEstatsTx30 new_estat;

 unsigned char subestat;
 enum eSubestatsTorn subestat_torn;
 enum eSubestatsObrirTorn subestat_obrir_torn;
 
 unsigned char tarifa_rot;  // tarifa inicial rotatori 
 unsigned char hay_que_recargar;
 unsigned short led_horaria;    // per guardar estat LED_HORARIA
  int timer_alfanum;
  int event_impre;    // 0/n   no/si hi ha event impre a retornar 
  int event_impre_ok;
  short cnt_sec_treure_tensio;
  char pendent_password_pas_a_on;
  char pendent_test_displ_pas_a_on;
  char nivell_llum_minim;    // entre 0 - 7
  unsigned short acum_off;
  char segons_pot_anular_pas_off;
  long num_conductor;
  long password_conductor;
  char nom_conductor[24];
  int timer_tick_torn; // timer per 1 minut ticket torn
  int cods_bloc_visu;  // 0/1  bloc 6 passw. / bloc seguents 4
  int cods_mask;       // mascara per cods que es poden entrar
  char index_tar;     // num tarifa per entrar preus
  long var_preu[32];  // array per entrar preu de les 32 tarifes
//  T_FECHHOR fecha_borr_tots;    // data borrat totalitzadors     passat a estructura blqs  v 2.00 
  char last_LUZFRONT;
  char lluminos_apagat;
  char testconectorOn;
  unsigned char  estat_retorn_cods;
  char estat_a_final_tempor;
  char descansXtancar;
  enum  eEstatsTx30 estat_retorn_reltots;
  char new_led_ho;
  char new_led_km;
 unsigned short ajuste_rel_last_anydia;     // v 2.20b
 unsigned short ajuste_rel_last_anydia_temp;     // v 2.20b
 signed char ajuste_rel_minuts; 

  unsigned long vel;
 char refresc_leds_ho_km;
 char pin_subestat;
 char pin_status_resp;
 char pin_value[4];
 
  char cobertura_buffer[3];
  char cobertura_rebuda;
  char index_nivell;
  char cobertura_timeout;
  char insika_needed;
  char insika_received;
  char insika_n_lin;
  char insika_line_to_print[10][66];
}t_tx30;



extern void salvar_tx30_a_registros(void);
extern void desconect_tx30(void);

const extern unsigned char  VERSION_TX[2];
const extern unsigned short  VERSION_TARIFA;





extern void motor_tarifacion_preload(uchar ck,uchar ch,uchar tip_vf,uint vvf,uint adj);


//***********************************************************************
//  ext30.h           variables externas tx30
//***********************************************************************



#define LEN_TARIFA_8031 8192




#define BLOQUEO_TORN 0


#define HOMOLOG      0





#if HOMOLOG
extern unsigned char  SAVLUZ;
extern unsigned char  timer_homolog;
#endif



#define LEN_PWD             10


#define EPET_LIBRE  0
#define EPET_OCUP   1
#define EPET_OFF    2



#define GL_MAL    0
#define GL_LIBRE  1
#define GL_OFF    2
#define GL_FISERV 3
#define GL_FIN    4

#define GL_FIN_MAX    8  // per dimensionar tarcom.luzest per si en el futur n'hi hagues mes
// fer els equips vells compatibles amb les tarifes noves

// definicio bits MASK_GEN

#define MSK_FP          0x01     // fecha paro
#define MSK_BLQ         0x02     // off + bloq teclat
//#define MSK_PAGFS   0x04     // cas tempor pas pagar a lliure per temps
#define MSK_FSLIB       0x04     // cas tempor pas  fi servei a lliure per temps
#define MSK_REL         0x08     // cas rellotge fixe
#define MSK_IMPRE       0x10     // espera final impressio (cas obligatoria )
// suprimit a versio 3.05 #define MSK_TRS         0x20     // espera resp transmissio
#define MSK_TIPUS_IVA   0x20     // espera seleccio tipus IVA en A Pagar
#define MSK_REL_LIB     0x40     // espera final temps visu rel per pas a ocupat
#define MSK_PIN_ENTRAR  0x80     // entrada PIN en lliure

// definicio bits MASK_GEN ampliada a 16 bits
#define MSK_PAUSA 0x0100   // per inhibir tecles mentres en pausa

// definicio bits MASK_1

#define MSK_PASLIOC     0x01     // bloqueo paso a ocupado
#define MSK_PASOCFS     0x02     // bloqueo paso a libre
#define MSK_PASLIOF     0x04     // bloqueo paso libre a off
#define MSK_PASOFLI     0x08     // bloqueo paso off a libre
#define MSK_TORN_OBRIR  0x10     // bloqueig obrir torn 
#define MSK_TORN_TANCAR 0x20     // bloqueig tancar torn


// bits de sensors
#define SENSORS_CLAU_CON      0x01       
#define SENSORS_LLUM_POS      0x02      
#define SENSORS_PISON         0x04       
#define SENSORS_PASAJERO      0x08       







#include "struc30.h"



// CODIGOS SECRETOS

#define CS_AC   0
#define CS_PAR  1
#define CS_REL  2
#define CS_FP   3

#define TR_STOP    0
#define TR_EN_CURS 1
#define TR_OK      2
#define TR_MAL     3




extern void DISP_FLASH(unsigned char n);
extern void DISP_DIG_SET(uchar ndisp,uchar ndig, uchar value);
extern void DISP_DIG_FIXE(uchar ndisp,uchar ndig);
extern void DISP_DIG_FLASH(uchar ndisp,uchar ndig);


extern void DISPLAY_VD1(unsigned char num);
extern void DISPLAY_VD2(unsigned char num);



// VARIABLES PER DESENCADENAR TRANSICIONS

#define V_TDUM     0   // TECLA DUMMY

#define V_START     1

#define V_TREL      2
#define V_TLIBRE    3   // LLIURE
#define V_TSUPL     4   // SUPLEMENTOS
#define V_TCANCELSP 5   // CANCELAR SUPLEMENTOS
#define V_TNUM      6   // TECLA NUMERICA
#define V_TPAGAR    7   // tecla a pagar
#define V_TSOLTAR   8   // SOLTADA TECLA
#define V_TSUMA     9   // SUMA
#define V_TTOT     10   // TECLA TOTALITZADORS
#define V_TUP      11
#define V_TDWN     12
#define V_TROT     13
#define V_TROTUP   14
#define V_TROTDWN  15
//#define V_TNEXT    15 
#define V_TABORT   16
#define V_TCODS    17   // seleccion codigo secreto
#define V_TSKIP    18
#define V_TINCR    19
#define V_TCLR     20
#define V_TLLUM    21
//#define V_TCERRAR  22
//#define V_TDECR    22
#define V_TPLUSPLUS  22
#define V_TAPAGAR_LLUM 23
//#define V_CDIA     24
#define V_TEURO    24
#define V_TEMPOR   25   // TEMPOR
#define V_TEMPOR_1 26   // TEMPOR 1
#define V_TR_IN    27   // recepcio port serie
#define V_GENIMP   28   // error generador d'impulsos
#define V_PASAUT   29   // pas automatic per metres
#define V_BLOQ     30   // per pas off+ bloqueo teclat
#define V_SEN2     31   // per off-lliure senyal E2
#define V_APOCMF   32   // pas de A PAGAR a OCUP per MF
#define V_TR_TARI  33
#define V_F_GENIMP 34   // fi error generador d'impulsos
#define V_TANCAR_IMPORT  35   // td30 tanca pantalla imports
#define V_TEMPOR_2 36   // TEMPOR 2
//#define V_OFF_TDTX 37
#define V_MF       37   // impulsos metro (cas desglos suma)
#define V_FI_ITV   38   // final ITV fran�a
#define V_PETICIO_PIN   39
#define V_ACK_PASSWORD  40
#define V_NACK_PASSWORD 41
#define V_PREC_ACORD	42

#define SW_TAR 0
#define SW_SUP 1

// tipus supl

#define SUP_OCUP 1
#define SUP_APAG 2
#define SUP_ALL  3

// tipus ir100

#define IR_NO   0
#define IR_OPC  1
#define IR_OBL  2

// opcions tarifa


//*******************************
#define TIPTOP_IMP   0x08   // topes import per tarifa
#define TIPTOP_DIST  0x10
#define TIPTOP_TEMPS 0x20


// bits de tarifa[].visu_globtar

#define GLOBTAR_DIST   0x01
#define GLOBTAR_CRONO  0x02

// definicio formats

#define F_HMS     0
#define F_BCD     1
#define F_FLOAT   2
#define F_LONG    3
#define F_LONG_NC 4
#define F_SS_JI   5  // SieteSegmentos justif.Izq.
#define F_SS_JD_C 6  // SieteSegmentos justif.Derecha Clear
#define F_HORA    7
#define F_DIA     8
#define F_ANY     9
#define F_HHHH   10
#define F_LONG_60 11  // pasa minuts totals  a hores*100+minuts
#define F_HHMMSS  12
#define F_MODIF_IVA 13


#define FF_DDMM  0
#define FF_MMDD  1


#define NF_LEDEST       0     // led estat
#define NF_IMPORT       1     // import
#define NF_SUPL         2     // suplements
#define NF_K            3     // k
#define NF_TEXT5        4     // texte 5 caracters ss
#define NF_TEXT6_FL     5      // texte 6 caracters ss flashing
#define NF_HORA         6     // hora
#define NF_DIA          7     // dia
#define NF_ANY          8     // any
#define NF_LEDEST_FLASH 9     // led estat intermitent
#define NF_TEXT4       10     // texte 4 caracters ss
#define NF_D_T         11     // distancia per totalits
#define NF_I_T         12     // import per totalits
#define NF_K_T         13     // escalar per totalits
#define NF_TEXT2       14     // texte 3 caracters ss
#define NF_CHK         15     // chek
#define NF_K3D         16     // K co 3 decs.
#define NF_NT          17     // num. tarifa
#define NF_AMD         18     // AA.MM.DD.
#define NF_DIST2       19
#define NF_CRON2       20
#define NF_VEL         21
#define NF_K_FLASH     22     // k  flashing
#define NF_LED_DER     23     // led estat a la dreta
#define NF_BCD5        24
#define NF_BCD4        25
#define NF_BCD2        26
#define NF_FL_1        27
#define NF_FL_D        28
#define NF_FL4_D       29
#define NF_HMS         30
#define NF_HHMMSS_INT  31
#define NF_TEXT_TR     32
#define NF_LIC         33
#define NF_DDDSS       34
#define NF_TOT6        35
#define NF_TOT8        36
#define NF_LEDTOT      37
#define NF_LED_DER_FLASH 38
#define NF_TOT2        39
#define NF_TOT4        40
#define NF_HH_MM       41
#define NF_HORA_FLASH  42
#define NF_AAAA        43
#define NF_HHMM        44
#define NF_TEXT6       45     // texte 6 caracters ss
#define NF_HHHH_DISP1  46
#define NF_HHHH_DISP2  47
#define NF_IMPORT_ALT  48     // import (euros)
#define NF_SUPL_ALT    49     // suplements(euros)
#define NF_LEDTAR_INTERM 50
#define NF_MINUTS_TO_HH_MM 51   // minuts a hh.mm
#define NF_SUPL_NOBLK  52   // Per entrada Imports km/hora
#define NF_VERSION     53   // VV.VV
#define NF_K2D         54   // long amb dos decimals
#define NF_BCD6        55
#define NF_MINUTS_TO_HH_MM_PUNT_INTERM 56   // minuts a hh.mm amb punt intermitent
#define NF_HHMM_FIXE   57     // HH.MM amb punt fixe
#define NF_TEXT6_FLASH_NOMOD  58  // TEXTE 6 mantenint FLASH si n.hi havia
#define NF_MODIF_IVA   59

// constants eeprom taximetre
#define  EEPT_CODS_POS  0x60
#define  EEPT_CODS_LEN  sizeof(struct s_cods_tax)

#define  EEPT_BLQS_POS  EEPT_CODS_POS+EEPT_CODS_LEN
#define  EEPT_BLQS_LEN  sizeof(struct s_blqs)

#define  EEPT_FP_POS    EEPT_BLQS_POS+EEPT_BLQS_LEN
#define  EEPT_FP_LEN    3

#define  EEPT_BLQS_EXT_POS  EEPT_FP_POS + EEPT_FP_LEN
#define  EEPT_BLQS_EXT_LEN  sizeof(struct s_blqs_ext)

// situat despres de BLQS_EXT deixant alguns bytes lliures (desde 4EB a 4ff)
#define  EEPT_CODS_EXT_POS  0x500
#define  EEPT_CODS_EXT_LEN  sizeof(struct s_cods_tax_ext)

// afegit a versio 3.04 despres de CODS_EXT
#define  EEPT_CANVIS_TAR_POS  0x600
#define  EEPT_CANVIS_TAR_LEN  sizeof(struct s_blqs_modifs_tar)

#define  EEPT_BLQS_EXT32_POS	0x1000
#define  EEPT_BLQS_EXT32_LEN  sizeof(struct s_blqs_ext_32)

// taula crides a grabar_eprom
#define GE_BLQS             0
#define GE_n_grabs_carg     1
#define GE_CODS             2
#define GE_FP               3
#define GE_BLQS_nsalts      4
#define GE_BLQS_desc        5
#define GE_param_carg       6
#define GE_BLQS_EXT         7
#define GE_CODS_EXT         8
#define GE_BLQS_MODIFS_TAR  9
#define GE_BLQS_EXT_32		10

// CONSTANTS DEL PROGRAMA



#define H0 1.076408
#define H0M1 1.111131
#define DELTAC (H0M1-H0)

//#define MARCHA_0 5.0       // 5metros/minut per temps marxa en lliure


// Tipus canvis automatics

#define AUT_TEC    0x01
#define AUT_ZONH   0x02
#define AUT_IMP    0x04
#define AUT_DIST   0x08
#define AUT_TEMPS  0x10
#define AUT_EXT    0x20
#define AUT_VEL    0x40
#define AUT_INICIO 0x80
#define AUT_ALL    0x7f
#define AUT_ALL_INICIO    0xff


// opcions visu a display 1
#define VD1_IMP      0
#define VD1_KOC   0x10
#define VD1_IMPK  0x20
#define VD1_IMPH  0x30
#define VD1_CRONO 0x40
#define VD1_VEL   0x50
#define VD1_CRONO_CENT 0x60

// opcions Visu a Display 2
#define VD2_KOC    1
#define VD2_IMPK   2
#define VD2_IMPH   3
#define VD2_CRONO  4
#define VD2_VEL    5
#define VD2_CRONO_CENT  6                    

// Visualitzacio de parametres

#define PARMS_L_INI   15 //13
#define PARMS_r_INI   21 //21
#define PARMS_r_FIN   28 //28

// Visualitzacio TEXT_ERROR

#define ERR_LLUMS      1
#define ERR_GEN_POLSOS 2
#define ERR_RELLOTGE   3
#define ERR_FECHA_PARO 4
#define ERR_EXCES_VEL  5
#define ERR_TORN       6
#define ERR_CHECK_TAR  7
#define ERR_IMPRE      9
#define ERR_AVIS_TORN  10 
#define ERR_ZAPPER     11


// VARIABLES GENERALES
 
extern  t_tx30 tx30;
__no_init extern unsigned short check_programa;

__no_init extern unsigned long  tec_canvi;
__no_init extern unsigned char  amb_canvi;
__no_init extern unsigned int   tdia_canvi;
__no_init extern unsigned int   zonh_canvi;
__no_init extern unsigned char  se_canvi;


extern struct s_tarcom   tarcom;
extern struct s_configs_desde_bg40   configs_desde_bg40;
extern struct s_tarifa   tarifa[32];
extern struct s_bloc_tar bloc_tar[32];
extern struct s_servei   servei;
extern struct s_serv     serv[MAX_PSGRS];
extern struct s_servcom  servcom;

extern char canvi_top;
//extern unsigned int  parcial_dist[32];
//extern unsigned short  parcial_temps[32];

// extern char ntope_dist[32];
//extern char ntope_temps[32];
//extern char ntope_imp[32];

extern unsigned short zonh[17];
extern unsigned long  tope_imp[17];
extern unsigned int   tope_dist[17];
extern t_tope_temps   tope_temps[17];
extern t_canvi_autom canvi_autom[32*8];   // 8 condicions per tarifa


extern unsigned short MASK_GEN;

extern  char ledtar_interm;
extern  char cnt_ledtar_interm;
extern  char buf_ledtar_interm[4];


 
extern __no_init unsigned char  FORMATO_FECHA;
   extern unsigned char ROBO;
   extern unsigned char  nsem;
   
   extern unsigned char tecla_num;
   extern unsigned char tecla_tarifa;

   extern unsigned char pasj;
   extern unsigned char sext;
   extern unsigned char sext_luz;

   // VARIABLES TARIFACION

//   extern  unsigned char TARIFANDO;
   extern unsigned char exces_vel;
//   extern unsigned char CUENTA_K;
//   extern unsigned char NEW_CUENTA_K;
//   extern unsigned char CUENTA_H;
//   extern unsigned char NEW_CUENTA_H;


   extern unsigned long  velocidad;

   extern unsigned long visuk;
   extern unsigned short  visu_chk;
   extern unsigned short  check_tiquet;
   extern unsigned short visu_chk_prec;
   extern  char visu_f_precambio[3];

   
   extern int timer50;
   extern int timer100;
   extern int timer_2;

   extern char exces_fallo_t;
   extern unsigned char sensors;
//   extern unsigned char CNT_MARCHA;
   extern char var_tarari;

   extern struct s_gt gt;
   extern unsigned long last_supl;    // per parlant


// VARIABLES definicion perifericos i2c

extern struct s_teclat teclat;

extern const struct s_tabla_crida_eprom tabla_crida_eprom[11];

extern struct s_canal_serie canal_serie;


extern const struct s_formato  formatos[60];

extern unsigned char event;
extern unsigned char event_tr;
extern unsigned char pseudoevent;

extern unsigned char * ndigdisp_ddmm;

extern const unsigned char dias_mes[19];
extern int pk_tot_tot;
extern int pk_lib_tot;
extern int  pk_tot_tot_ccab;
extern int pk_oc_tot;
extern int pk_off_tot;
extern int pk_lpasj_tot;
/* v 2.02                        
extern unsigned char phora_oc;
extern unsigned char phora_on;
*/
extern unsigned char habia_error_genimp;
extern char contar_salts_inst;

extern struct s_param_carg  param_carg;
extern unsigned int check_sum;

extern struct s_cods_tax      cods_tax;
extern struct s_cods_tax_ext  cods_tax_ext;

extern struct s_blqs blqs;
extern struct s_blqs_ext blqs_ext;
extern struct s_blqs_ext_32 blqs_ext_32;
extern struct s_blqs_modifs_tar blqs_modifs_tar;

extern const struct s_visu       visu_parms[];
extern const struct s_visu  visu_parms_sin_passw[4];
extern const struct s_visu_vd   visu_disp1[];
extern const struct s_visu_vd   visu_disp2[];
extern const struct s_parms_tar  visu_parms_tar[9];

extern unsigned char fecha_paro[3];
extern unsigned long long_fp;



extern const float  FKDEC[4];
extern unsigned char MASK_1;
extern void  * p_import;
extern void  * p_crono;
extern void  * p_hm_oc;

extern unsigned long  *p_supl;

extern char viaj;

extern unsigned short ledocpag[2];


extern char delay_impre;
// varis afegits com externs per taula grabar_eprom

extern struct s_grab aux_bufgrab;
extern struct s_grab new_bufgrab;
extern struct s_grab sav_bufgrab;
extern uchar buf_read8[8];





//***************************************************************
//  prototips i defines rutines
//***************************************************************

#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define min(a,b)    (((a) < (b)) ? (a) : (b))

#define DIM(array)  (sizeof(array)/sizeof(array[0]))
//extern unsigned int i_min(unsigned int a,unsigned int b);
//extern unsigned char c_min(unsigned char a,unsigned char b);

#define offsetof(s,m) (unsigned int)&(((s*)0)->m)
#define calcul_chk_blqs() sum_check((char*)&blqs,offsetof(struct s_blqs,chk_blqs))
#define calcul_chk_blqs_ext() sum_check((char*)&blqs_ext,offsetof(struct s_blqs_ext,chk_blqs_ext))
#define calcul_chk_blqs_ext_32() sum_check((char*)&blqs_ext_32,offsetof(struct s_blqs_ext_32,chk_blqs_ext))
#define calcul_chk_blqs_modifs_tar() sum_check((char*)&blqs_modifs_tar.modif_tar[0], blqs_modifs_tar.nmodifs_tar * sizeof(blqs_modifs_tar.modif_tar[0]))

//#define es_bcd(uchar) ((((uchar)&0xf0)<0xa0) && (((uchar)&0x0f)<0xa))
extern unsigned char es_bcd(uchar c);

#define hay_multi_pasj()  (!status_i2c(NBUS_SENSOR_PASJ,0x48))


extern void VPROM(void);
extern void INIT_VPROM(void);


extern void clear_xdata(struct s_clear_x const* p);

extern void APAGAR_DISPLAY(unsigned char n);
extern void DISP_FIXE(unsigned char n);
extern void DISP_FILL(unsigned char nDisp,unsigned char value);

extern unsigned int sum_check(char * buf,unsigned int len);
extern unsigned int calcul_check(void);
extern void grabar_cods(void);
extern void grabar_cods_ext(void);
extern void leer_verif_cods(void);
extern void copia_blqs_ext_a_blqs_ext_32();
extern unsigned char tarifa_ok(void);

extern unsigned char pas_lioc_permes(unsigned char tar);
extern unsigned char tarifa_canvi_autom(unsigned char tar,unsigned char MASK, struct s_serv* p_serv);
extern void cambio_tarifa(unsigned char tar,unsigned char estat, struct s_serv* p_serv);
extern unsigned char pas_ococ_permes(unsigned char new_tar,unsigned char old_tar);
extern unsigned char pas_ocap_permes(unsigned char tar);
extern unsigned char pas_apoc_permes(unsigned char new_tar,unsigned char old_tar);
extern void SUPL_AUT(unsigned char ns);

extern void ltobcd(unsigned long l,char *);
extern unsigned long bcdtol(unsigned char *buf,unsigned char nd);
extern void bcdtoasc(char * dest, char *orig,unsigned char nd);
extern unsigned char r_bit(unsigned char);
extern unsigned char l_bit(unsigned char);
extern char num_bits_1(int v);
extern void refresh(void);

extern void teclat_carg_init(void);
extern void confi_format_diames(uchar nf);


extern unsigned char llegir_sensors(void);

extern void select_luz(char estat, char* ret);

extern unsigned char reloj_ok(int nChip);
extern unsigned char fecha_paro_ok(unsigned char *fp);
extern unsigned long segon_del_dia(struct s_time *t);

extern unsigned char semana_del_any(struct s_time *time);
extern void anydia_a_dma(unsigned int iaux, char *dma);


extern void  recalculs_gt(void);

extern unsigned int calcul_tdia(void);
extern unsigned char calcul_amb(void);

extern int leer_verif_blqs_ext(void);
extern int leer_verif_blqs_ext_32(void);
extern void leer_verif_blqs_modifs_tar(void);

extern void grabar_fp(void);
extern void leer_verif_fp(void);
extern void gestio_fecha_paro(void);
extern unsigned char arrivat_tope(unsigned char nb,unsigned char *b1,unsigned char *b2);

extern void serial_config(void);
extern void serial_in(unsigned char to);
extern int extraer_cola_env_tx(unsigned char *c);

extern unsigned char inc_bcd(unsigned char);
extern unsigned char sum_bcd(unsigned char,unsigned char);

extern void set_nibble(unsigned char nib,unsigned char nd,unsigned char *buf);
extern unsigned char get_nibble(unsigned char nd,unsigned char *buf);


extern void leer_eprom_tabla(unsigned char n);
extern void grabar_eprom_tabla(unsigned char n);



extern unsigned char llegir_teclat_carg(void);

//extern void definir_tipo_display(int modo);
//extern void display(unsigned char ndisp,unsigned char nf,unsigned char const *adr_var);
extern void display_1_2(void);

extern void display_1_reloj_en_libre(uchar op);

extern  unsigned char led_fos(void);
extern unsigned char hay_transicion_tx30(void);

extern void demanaPermisActualizar(char tipus);
extern void processar_transmissio_tarifa(char* buf_rep, int nc_rep, char* buf_resp);
extern void guardar_valors_cobertura(char* buf); 


extern void reset_interserv(void);

extern void INTT_HANDLER(void);

extern void copia_reg_a_tots(void);
extern void copia_tots_a_reg(unsigned char ndig);
extern void calcul_tots_aux(void);   

extern void calcul_toptar_import(char tar,long import, struct s_serv* p_serv);
extern void calcul_toptar_dist(char tar,unsigned int dist, struct s_serv* p_serv);
extern void calcul_toptar_temps(char tar,unsigned short temps, struct s_serv* p_serv);


extern unsigned int decod_16(unsigned char);
extern unsigned short get_int_odd(unsigned char * adr);
extern void set_int_odd(unsigned char * adr,unsigned short ivar);
extern unsigned long get_long_odd(unsigned char * adr);

extern void copia_parms_a_reg(void);
extern void copia_serie_lic_a_reg(void);

extern void tarifador_stop_and_reset(struct s_serv* p_serv);
extern void tarifador_stop(struct s_serv* p_serv);
extern void tarifador_noFaPrimerSalt(struct s_serv* p_serv);
extern void tarifador_start(struct s_serv* p_serv);
extern int  esPrimerSalt(struct s_serv* p_serv);

extern void setBthConnected(char btc);
extern void loop10ms(void);

#endif
