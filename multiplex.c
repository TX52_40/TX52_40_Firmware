// *********************************************************************
// multiplex.c
// *********************************************************************

#include <stdlib.h>

#include "car30.h"
#include "i2c.h"
#include "rut30.h"
#include "reloj.h"
#include "serial.h"
#include "multiplex.h"
#include "impre.h"
#include "insika.h"
#include "bloqueig_torn.h"
#include "tx30.h"

#define DLE 0x16
#define STX 0x02
#define ETX 0x03

#define REP_STBY 0
#define REP_STX 1
#define REP_BUF 2
#define REP_BUF_DLE 3

#define CLAU_EN_OBERT           0
#define CLAU_TAULA_ERRONIA      1
#define CLAU_ACTUAL             2
#define CLAU_DIFERENT           3

#define MAX_DIES_SENSE_AC  5

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

__no_init t_multiplex multiplex;

unsigned char skey[256];
unsigned char clau[24];
unsigned char msg[120];	

// Constants are the integer part of the sines of integers (in radians) * 2^32.
const unsigned long k[64] = {
0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee ,
0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501 ,
0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be ,
0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821 ,
0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa ,
0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8 ,
0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed ,
0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a ,
0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c ,
0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70 ,
0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05 ,
0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665 ,
0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039 ,
0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1 ,
0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1 ,
0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };
 
// leftrotate function definition
#define LEFTROTATE(x, c) (((x) << (c)) | ((x) >> (32 - (c))))

 
// These vars will contain the hash
unsigned long h0, h1, h2, h3;
    // r specifies the per-round shift amounts
    const unsigned long r[] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                          5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
                          4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                          6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};




void md5(unsigned char *initial_msg, int initial_len,unsigned char* destKey) {
 
    // Message (to prepare)
    //unsigned char *msg = NULL;
int new_len;
unsigned long bits_len;
long offset;
unsigned long *w;
unsigned long a,b,c,d;
unsigned long i,f,g,temp;
	// Note: All variables are unsigned 32 bit and wrap modulo 2^32 when calculating
 
 
    // Initialize variables - simple count in nibbles:
    h0 = 0x67452301;
    h1 = 0xefcdab89;
    h2 = 0x98badcfe;
    h3 = 0x10325476;
 
    // Pre-processing: adding a single 1 bit
    //append "1" bit to message    
    /* Notice: the input bytes are considered as bits strings,
       where the first bit is the most significant bit of the byte.[37] */
 
    // Pre-processing: padding with zeros
    //append "0" bit until message length in bit ? 448 (mod 512)
    //append length mod (2 pow 64) to message
 
    for(new_len = initial_len*8 + 1; new_len%512!=448; new_len++);
    new_len /= 8;
 
//    msg = calloc(new_len + 64, 1); // also appends "0" bits 
	memset(msg,0,sizeof(msg));

                                   // (we alloc also 64 extra bytes...)
    memcpy(msg, initial_msg, initial_len);
    msg[initial_len] = 128; // write the "1" bit
 
    bits_len = 8*initial_len; // note, we append the len
    memcpy(msg + new_len, &bits_len, 4);           // in bits at the end of the buffer
 
    // Process the message in successive 512-bit chunks:
    //for each 512-bit chunk of message:
    for(offset=0; offset<new_len; offset += (512/8)) {
 
        // break chunk into sixteen 32-bit words w[j], 0 ? j ? 15
         w = (unsigned long *) (msg + offset);
 
/*
#ifdef DEBUG
        printf("offset: %d %x\n", offset, offset);
 
        int j;
        for(j =0; j < 64; j++) printf("%x ", ((uint8_t *) w)[j]);
        puts("");
#endif
*/
 
        // Initialize hash value for this chunk:
         a = h0;
         b = h1;
         c = h2;
         d = h3;
 
        // Main loop:
        for(i = 0; i<64; i++) {
 
 
             if (i < 16) {
                f = (b & c) | ((~b) & d);
                g = i;
            } else if (i < 32) {
                f = (d & b) | ((~d) & c);
                g = (5*i + 1) % 16;
            } else if (i < 48) {
                f = b ^ c ^ d;
                g = (3*i + 5) % 16;          
            } else {
                f = c ^ (b | (~d));
                g = (7*i) % 16;
            }
 
            temp = d;
            d = c;
            c = b;
            b = b + LEFTROTATE((a + f + k[i] + w[g]), r[i]);
            a = temp;
 
        }
 
        // Add this chunk's hash to result so far:
        h0 += a;
        h1 += b;
        h2 += c;
        h3 += d;
 
    }
 set_long_to_4chars(destKey,h0);
 set_long_to_4chars(destKey+4,h1);
 set_long_to_4chars(destKey+8,h2);
 set_long_to_4chars(destKey+12,h3);
}


void calcula_clau(char*pos, unsigned char* Key)
{
int i,j,k;
int ptr;
  for(i=0,k=0;i<3;i++)
  {
    ptr = pos[i];
    for(j=0;j<8;j++)
    {
      clau[k++] = multiplex.taula_encrip[ptr++];
      if(ptr >= 256)
        ptr = 0;    // per donar volta a la taula
    }
  }
  // encripta clau ( i la guarda a multiplex.Key )
  md5(clau,24,Key);
  
}

void RC4_init(void)
{
unsigned char pos;
int i ;

t_time  time;

  
  // calcula clau
  get_reloj(RELLOTGE_PLACA,&time);
  srand(OS_Time +time.minute+time.sec*60);
  for(i=0;i<3;i++)
  {
    pos = rand() & 0xff;
    multiplex.pos_clau[i] = pos;
  }
  // encripta clau ( i la guarda a multiplex.Key )
  calcula_clau((char*)multiplex.pos_clau, (unsigned char*)multiplex.Key);
}

//
// encripta/desencripta
//
void RC4(char * Buf, int dwBufLen, char* Key)
{
int i, j;
int dw;
char tmp;

	  for(i = 0; i < 256; i++)
	  {
              skey[i] = i;
	  }
	  for(i = j = 0; i < 256; i++)
	  {
              j = (j + skey[i] + Key[i % MPX_LEN_KEY]) % 256;
              tmp = skey[i];
              skey[i] = skey[j];
              skey[j] = tmp;
	  }

  for(i=j= dw = 0; dw < dwBufLen; dw++)
  {
          i = (i + 1) % 256;
          j = (j + skey[i]) % 256;
          tmp = skey[i];
          skey[i] = skey[j];
          skey[j] = tmp;
          *Buf++ ^= skey[(skey[i] + skey[j]) % 256];
  }
}

void reset_dies_sense_clau(void)
{
  multiplex.last_data_no_ac = 0;  // per que primera vegada canvii de dia
  multiplex.dies_sense_ac = 0;
}

char excedits_dies_sense_clau(void)
{
unsigned short anydia;
      if(multiplex.dies_sense_ac > MAX_DIES_SENSE_AC)
        return 1;
    anydia = get_anydia_actual();
//    anydia = get_minut_del_dia();   // per fer proves amb 5 minuts
    if(multiplex.last_data_no_ac != anydia)
    {
      multiplex.last_data_no_ac = anydia;
      multiplex.dies_sense_ac++;
      if(multiplex.dies_sense_ac > MAX_DIES_SENSE_AC)
        return 1;
    }
    return 0;
}


// es crida al carregar programa nou o al canviar de tarifa
void multiplex_init_0(void)
{
  reset_dies_sense_clau();
}

void multiplex_init(char tipus, char* taula, short id_taula)
{
  multiplex.tipus = tipus;
  switch(tipus)
  {
    case MULTIPLEX_NO:   // no n'hi ha
      break;
    case MULTIPLEX_TL70:
      // la usart ja l'inicialitza lluminos
      multiplex.num_usart = USART_LUMIN;
      break;
    case MULTIPLEX_BLUETOOTH:
      // inicialitza usart
      usart_install(USART_BLUETOOTH,NULL);
      usart_hard(USART_BLUETOOTH, MODO_232, 0, 1, 0);
      usart_init(USART_BLUETOOTH, 115200, 1,0); // 115200 bauds, 8bit  no parity
      
      usart_enviar(USART_BLUETOOTH,"SET BT NAME TX50_11111\x0d",23);
      usart_enviar(USART_BLUETOOTH,"SET BT AUTH * 111111\x0d",21);
      
      multiplex.num_usart = USART_BLUETOOTH;

      break;
    case MULTIPLEX_BT40:
      // inicialitza usart
      usart_install(USART_BT40,NULL);
      usart_hard(USART_BT40, MODO_232, 0, 0, 0);
      usart_init(USART_BT40, 38400, 7,1); // 38400 bauds, 8bit  parity, parity odd 
      multiplex.num_usart = USART_BT40;
      break;
    case MULTIPLEX_BT40_L:
      // es com MULTIPLEX_TL70 pero s'inicialitza la usart aqui
      
      // inicialitza usart
      usart_install(USART_LUMIN,NULL);
      usart_hard(USART_LUMIN, MODO_232, 0, 0, 0);
      usart_init(USART_LUMIN, 38400, 7,1); // 38400 bauds, 8bit  parity, parity odd 
      multiplex.num_usart = USART_LUMIN;
   }

  multiplex.estat_rep = REP_STBY;
  multiplex.missatge_pendent_transferir = (t_msg_mpxd*)NULL;
  multiplex.msg_lum.proces_en_curs = 0;
  multiplex.msg_prn.proces_en_curs = 0;
  multiplex.msg_txm.proces_en_curs = 0;
  
  multiplex.taula_encrip = taula;
  multiplex.id_taula = id_taula;
  if(multiplex.taula_encrip != NULL)
    	RC4_init();
}


void multiplex_transferir_missatge(t_msg_mpxd* msg)
{

    // ja ha acabat de processar missatge previ
    msg->device = multiplex.buf_rep[0];
    msg->nc_rebuts = multiplex.nc_rep-(MPX_LEN_MIN + multiplex.len_encrip);
    msg->ichar_a_processar =  0;
    memcpy(msg->buf,multiplex.buf_rep+(2+multiplex.len_encrip),msg->nc_rebuts);
    msg->proces_en_curs = 1;
    multiplex.missatge_pendent_transferir = (t_msg_mpxd*)NULL;
}

extern char get_tipus_clau_emprada(char* buf);

char get_tipus_clau_emprada(char* buf)
{
unsigned   short id;
  id = (( unsigned short)buf[0])*256 + ( unsigned short)buf[1];
  if(id != multiplex.id_taula)
    return CLAU_TAULA_ERRONIA;
  if(memcmp(buf+2,multiplex.pos_clau,sizeof(multiplex.pos_clau)) == 0)
    return CLAU_ACTUAL;
  return CLAU_DIFERENT;
  
}


#define MAX_CHARS_TO_EXTRACT  30    // caracters que extreu en cada crida
//
// extreu missatge de dins protocol multiplexat
// s'ha de cridar desde loop principal per anar treient caracters de cua
// 
// retorn: 0     no hi ha missatge
//         != 0  num. caracters del missatge
//

extern void posa_estat_a_buffer(char* buf);
extern void recPosicion(char * buf, int nb);

CODE_FAST void multiplex_retrieve_missatge(void)
{
char ch;
int i;
char caux,st;
char perif;
char cal_desencriptar;
t_msg_mpxd*  p_msg_mpxd;
unsigned char* Key_desencriptar;
char  baux[3];
char buf_resp[5];
char device;
int nc;

  if(multiplex.tipus == MULTIPLEX_NO)
      return;
  // MULTIPLEX_TL70:
  // MULTIPLEX_BLUETOOTH: 

  for(i= 0;i < MAX_CHARS_TO_EXTRACT; i++)
  {
      if(multiplex.missatge_pendent_transferir)
      {
        if(multiplex.missatge_pendent_transferir->proces_en_curs == 0)
            multiplex_transferir_missatge(multiplex.missatge_pendent_transferir);  // el transfereix a buffer 
        return;
      }
      if(usart_extreure_char(multiplex.num_usart, &ch))
        return;  // no hi ha caracter a cua
      switch(multiplex.estat_rep)
      {
        case REP_STBY:
          if(ch == DLE)
          {
            multiplex.estat_rep = REP_STX;
            multiplex.nc_rep = 0;
          }
          break;
        case REP_STX:
          if(ch == STX)
            multiplex.estat_rep = REP_BUF;
          else
            multiplex.estat_rep=REP_STBY;
          break;
        case REP_BUF:
          if(ch==DLE)
            multiplex.estat_rep = REP_BUF_DLE;
          else
            multiplex.buf_rep[multiplex.nc_rep++] = ch;
          break;
        case REP_BUF_DLE:
          if(ch==0)
          {
            multiplex.buf_rep[multiplex.nc_rep++] = DLE;
            multiplex.estat_rep=REP_BUF;
          }
          else if(ch==ETX)
          {
            // cas recepcio complerta
            multiplex.estat_rep=REP_STBY;
            if(multiplex.nc_rep < MPX_LEN_MIN)
              return;
            cal_desencriptar = multiplex.buf_rep[1] & 0x80;
            perif = multiplex.buf_rep[1] & 0x7f;  // elimina bit si/no encriptat
            multiplex.buf_rep[1] = perif;
			device = multiplex.buf_rep[0];
            if(cal_desencriptar == 0)
            {
                multiplex.len_encrip = 0;
                switch(perif)
                {
                case PER_TXM_ESTAT:
					if(multiplex.buf_rep[2]!=':')
						return;
					switch(multiplex.buf_rep[3]){
					case 'K':
						//  cas envia estat tx en obert
						posa_estat_a_buffer(baux);  
						multiplex_enviar(device, PER_TXM_ESTAT , baux, 3);
						break;
					case 'C':
						setBthConnected(multiplex.buf_rep[4]);
						break;
					}
                    break;
                case PER_TXM:
                    //  cas envia en obert
                    multiplex_enviar(device, PER_TXM | 0x80, NULL, 0);
                    break;
                case PER_PRN_TX:
                    //  cas envia en obert
                    multiplex_enviar(device, PER_PRN_TX | 0x80, NULL, 0);
                    break;
                case PER_TRANSFER:
                    switch(multiplex.buf_rep[2])
                    {
					case 'P':
					case 'E':
						processar_transmissio_tarifa(multiplex.buf_rep+2, multiplex.nc_rep-MPX_LEN_MIN, buf_resp);
						multiplex_enviar(device, PER_TRANSFER,buf_resp,1);
						break;
					case 'i':
					case 't':
						buf_resp[0] = processar_transmissio_insika(multiplex.buf_rep+2, multiplex.nc_rep-MPX_LEN_MIN);
						multiplex_enviar(device, PER_TRANSFER,buf_resp,1);
						break;
					case 'c':
						buf_resp[0] = 'c'; 
						nc = processar_transmissio_control_torn(buf_resp+1);
						multiplex_enviar(device, PER_TRANSFER,buf_resp,nc+1);
						break;
					case 'Q':
						receiveTextesTiquets(multiplex.buf_rep+2, multiplex.nc_rep-2);
						multiplex_enviar(device, PER_TRANSFER, "Q1", 2);
						break;
					case 'q':
						{
							char buf[1050];
							int nb=sendTextesTiquets(device, buf);
							multiplex_enviar(device, PER_TRANSFER, buf, nb);
						}
						break;
					case 'm':
						{
							char buf[8];
							memcpy(buf,multiplex.buf_rep+3, 8);
							recPosicion(buf, multiplex.nc_rep-2);
						}
						break;
					case 'u':
						demanaPermisActualizar(multiplex.buf_rep[3]);
						break;
					}
					break;
				case PER_STATUS:
                    if(multiplex.buf_rep[2] == 'e')
                    {
                      // cas envia valors cobertura GPS i GSM
                      guardar_valors_cobertura(multiplex.buf_rep+3); 
                    }
                  break;
                }
            }
            else
            {
                multiplex.len_encrip = MPX_LEN_ENCRIP;
                if(multiplex.nc_rep < MPX_LEN_MIN_ENCRIP)
                  return;
                // determina tipus clau amb que s'ha encriptat
                st = get_tipus_clau_emprada(multiplex.buf_rep+2);
                switch(st)
                {
                case CLAU_TAULA_ERRONIA:
                  multiplex_enviar(device, PER_TXM | 0x80, NULL, 0);
                  return;   // no es pot fer res
                case CLAU_ACTUAL:
                  Key_desencriptar = multiplex.Key_encriptar = multiplex.Key;
                  reset_dies_sense_clau();
                  break;
                case CLAU_DIFERENT:
                  if(excedits_dies_sense_clau())                    
                  {
                     // ja nomes espera rebre clau correcte 
                      multiplex_enviar(device, PER_TXM | 0x80, NULL, 0);
                      return;   // no es pot fer res
                  } 
                  calcula_clau(multiplex.buf_rep+4,(unsigned char*)multiplex.Key_alt);
                  Key_desencriptar = multiplex.Key_encriptar = multiplex.Key_alt;
                  break;
                }
                if(Key_desencriptar != NULL)
                  RC4(multiplex.buf_rep+7,multiplex.nc_rep-8, (char *)Key_desencriptar);
            }
            switch(multiplex.buf_rep[1])  // perif
            {
            case PER_TXM:
              p_msg_mpxd = &multiplex.msg_txm;
              break;
            case PER_PRN_PREG_STS:
              st = get_status_impre();
              if(st &= IMPRE_SINPAPEL)
                caux = 'P';
              else if(st &= IMPRE_FALLO)
                caux = 'N';
              else if(get_impre_imprimint())
                caux = 'I';
              else
                caux = 'O';
              multiplex_enviar(device, PER_PRN_RESP_STS,&caux,1);
              p_msg_mpxd = NULL;
              break;
            case PER_PRN_TX:
              p_msg_mpxd = &multiplex.msg_prn;
              break;
            case PER_LUM:
              p_msg_mpxd = &multiplex.msg_lum;
              break;
            case PER_PRN_RESP_STS:
              impre_rcvd_status_mpxd(multiplex.buf_rep[2]);
              p_msg_mpxd = NULL;
              break;
            default:
              p_msg_mpxd = NULL;
              break;
            }  
            if(p_msg_mpxd != NULL)
            {
                if(p_msg_mpxd->proces_en_curs == 0)
                {
                  multiplex_transferir_missatge(p_msg_mpxd);
                }
                else
                  multiplex.missatge_pendent_transferir = p_msg_mpxd;
            }
            return;
          }
          else if(ch==STX)
          {
            // rebut DLE STX inicia nova recepcio
            multiplex.nc_rep = 0;
            multiplex.estat_rep = REP_BUF;
          }
          else
            multiplex.estat_rep=REP_STBY;            
          break;
        default:
          multiplex.estat_rep=REP_STBY;
          break;
      }    
      if(multiplex.nc_rep >= sizeof(multiplex.buf_rep))
        multiplex.estat_rep=REP_STBY;
  }
}


#define guardar_i_stuff(ch) \
{\
baux[p++]=ch;\
if(ch==DLE) baux[p++]=0;\
chk^=ch;\
}

void multiplex_enviar(int device, int perif, char * buf, int nb)
{
  int p;
  char ch;
  unsigned char chk;
  char baux[1024 + MPX_LEN_MIN];
  
  baux[0]=DLE;
  baux[1]=STX;
  
	p = 2;
	chk = 0;
	guardar_i_stuff(device)
	guardar_i_stuff(perif)

	if(perif & 0x80) 
	{
		// cas cal encriptar
		guardar_i_stuff(*(((char*)&multiplex.id_taula)+1))
		guardar_i_stuff(*((char*)&multiplex.id_taula))
		guardar_i_stuff(multiplex.pos_clau[0])
		guardar_i_stuff(multiplex.pos_clau[1])
		guardar_i_stuff(multiplex.pos_clau[2])
		RC4(buf,nb,(char *)multiplex.Key_encriptar);
	}

	while (nb)
	{
		ch=*buf++;
		guardar_i_stuff(ch);
		nb--;
	}

	guardar_i_stuff(chk)

  
  baux[p++]=DLE;
  baux[p++]=ETX;
  usart_enviar(multiplex.num_usart, baux, p);
}
