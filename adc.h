// adc.h


#ifndef __INC_ADC
#define __INC_ADC


void adc_init(void);
int adc_read(int nADC, int* status);
void adc_powerDown(void);


#endif

