// ***************************************************************************
// i2c.c
// ***************************************************************************


#include "i2cifac.h"
#include "iostr710.h"
#include "i2carm.h"
#include "RTOS.h"
#include "i2c.h"
#include "hard.h"

const t_def_chip_i2c DEF_CHIP_I2C[]=
{
  NBUS_CARG,  0,  0x42,	0,      	// TECLAT_CARG      
  NBUS_CARG,  0,  0xA2,	RELLOTGE_VELL,	// RELLOTGE_CARG    
  NBUS_CARG,  1,  0xA0,	EPROM_DEFAULT,	// EPROM_TARIFA_CARG
  NBUS_CARG,  0,  0xAE,	EPROM_256x8,	// EPROM_CONF_CARG
  
  NBUS_PLACA, 0,  0xA2,	RELLOTGE_NOU ,  // RELLOTGE_PLACA
  NBUS_PLACA, 0,  0xA4,	EPROM_DEFAULT,  // EPROM_BASE
  NBUS_PLACA, 0,  0xA6,	EPROM_DEFAULT,  // EPROM_SOPORTE
  NBUS_PLACA, 0,  0x60, 0,              // PIC_FIRMWARE
  NBUS_PLACA, 0,  0x20, 0,              // PIC_BOOTLOADER
  NBUS_CARG,  1,  0xA0, EPROM_ADDR3,    // EPROM_ARXIUS_CARG
  NBUS_CARG,  0,  0x70, 0,              // DISPLAY_CARG
  NBUS_PLACA, 0,  0x40, 0,              // IO_EXPANDER LUCES
  NBUS_CARG,  0,  0xA6, 0,              // CONTROL_HORARI
  NBUS_PLACA, 0,  0xA0,	EPROM_DEFAULT,  // EPROM_PROPIETARIO  
  NBUS_PLACA, 0,  0x70, 0,              //DRIVER_DISPLAY
  NBUS_PLACA, 0,  0x42, 0,              //IO_EXPANDER_TX40
  NBUS_PLACA, 0,  0x50, 0,              //POT_DIGITAL
};

OS_RSEMA SemaI2cPlaca;
char SemaI2cPlacaCreated =0;

void config_i2c(int nbus)
{
  switch(nbus)
  {
  case NBUS_PLACA:
    if(!SemaI2cPlacaCreated)
    {
      SemaI2cPlacaCreated=1;
      OS_CREATERSEMA(&SemaI2cPlaca);
    }
    I2C0_Config();
    break;
  case NBUS_CARG:
    reset_i2c_ifac();
    break;
  }
  
}

int status_i2c(int nChip)
{
int ret;
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    ret = status_i2c_arm(DEF_CHIP_I2C[nChip].slave);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    ret = status_i2c_ifac(DEF_CHIP_I2C[nChip].nclock, DEF_CHIP_I2C[nChip].slave);
    break;
  }
  return(ret);
}

unsigned char input_i2c(int nChip)
{
unsigned char ret;
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    ret = input_i2c_arm(DEF_CHIP_I2C[nChip].slave);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    ret = input_i2c_ifac(DEF_CHIP_I2C[nChip].nclock, DEF_CHIP_I2C[nChip].slave);
    break;
  }
  return(ret);
  
}

void output_i2c(int nChip, unsigned char dato)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    output_i2c_arm(DEF_CHIP_I2C[nChip].slave, dato);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    output_i2c_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dato);
    break;
  }
  
}

void write_i2c(int nChip, unsigned char dir, unsigned char nb, char *buf)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    write_i2c_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    write_i2c_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
}

int wread_i2c(int nChip, unsigned char dir, unsigned char nb, char *buf)
{
	int ret=0;
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    ret=wread_i2c_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    wread_i2c_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
  return ret;
}

void write_i2c_extnd(int nChip,  unsigned int dir, unsigned int nb, char *buf)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    write_i2c_extnd_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    write_i2c_extnd_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
}

void wread_i2c_extnd(int nChip, unsigned int dir, unsigned int nb, char *buf)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    OS_Use(&SemaI2cPlaca);
    wread_i2c_extnd_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    break;
  case NBUS_CARG:
    wread_i2c_extnd_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
}

void write_i2c_adr3(int nChip, unsigned int dir, unsigned int nb, char *buf)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    /*
    OS_Use(&SemaI2cPlaca);
    write_i2c_adr3_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    */
    break;
  case NBUS_CARG:
    write_i2c_adr3_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
}


void wread_i2c_adr3(int nChip, unsigned int dir, unsigned int nb, char *buf)
{
  switch(DEF_CHIP_I2C[nChip].nbus)
  {
  case NBUS_PLACA:
    /*
    OS_Use(&SemaI2cPlaca);
    wread_i2c_adr3_arm(DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
    */
    break;
  case NBUS_CARG:
    wread_i2c_adr3_ifac(DEF_CHIP_I2C[nChip].nclock,DEF_CHIP_I2C[nChip].slave, dir, nb, buf);
    break;
  }
}

void write_i2c_disp(unsigned char nb, char *buf)
{
    OS_Use(&SemaI2cPlaca);
    write_i2c_disp_arm(DEF_CHIP_I2C[DRIVER_DISPLAY].slave, nb, buf);
    OS_Unuse(&SemaI2cPlaca);
}
