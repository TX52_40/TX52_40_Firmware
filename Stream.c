//********************************************************
// stream.c    rutines gestio paquet de ram
//********************************************************

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "stream.h"
#include "eprom.h"
#include "i2c.h"
#include "rutines.h"

#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#define __min(a,b)  (((a) < (b)) ? (a) : (b))

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

extern int hay_cargador(void);


// Identificadors de unitat
#define ID_UNIT_CONF	  0x98
#define ID_UNIT_CONF_INT  0x9800


#define NMODELS_MCARD	4	

typedef struct s_def_pqr
{
	unsigned long last_addr;
	unsigned short last_sect; // ultim sector
	unsigned char size_id;
	unsigned char max_entries; // m�xim n� de fitxers 8 per cada sector de directori
	unsigned short sect_dir;  // sector directori
	unsigned short sect_dat;  // sector dades
}t_def_pqr;

const t_def_pqr def_pqr0[NMODELS_MCARD]={
	0x7fffL, 126 ,0xf0,16,1, 3,			//  32K
	0xffffL, 253 ,0xf1,16,2, 4,			//  64K
	0x1ffffL,507 ,0xf2,16,4, 6,			// 128K
	0x3ffffL,1015,0xf3,16,8,10,			// 256K
//	0x7ffffL,2031,0xf4,32,16,20,			// 512K
};

t_def_pqr pqr;

typedef struct s_dir
{
	short sector_mem;
	short n_entry;
	t_direntry entry;
	char buf[256];
}t_dir;


t_dir dir={-1,}; 


t_direntry direntry[8];
unsigned short FAT[8*128 + 2*128]; 
//unsigned short FAT[16*128 + 4*256]; // si permitidos 512K

typedef struct s_stream
{
int tipus;	
char* buf_stream_ram;    // buffer stream ram
int opened;
int num_bytes_used;
}t_stream;

t_stream stream;



int read_ext_mem(unsigned long adr, unsigned int len, char* dest)
{
int len_parcial;
        if(stream.tipus == STREAM_RAM)
	{
          memcpy(dest,stream.buf_stream_ram+adr,len);
          return 1;
	}
	else 
	{
          while(len )
          {
            // es fa en blocs de 1k per poguer detectar si es treu carregador
            if(!hay_cargador())
                return(0);
            len_parcial = __min(len,1024);
	    leer_eprom(EPROM_ARXIUS_CARG, adr, len_parcial, dest);
            len -= len_parcial;
            adr += len_parcial;
            dest += len_parcial;
          }
          if(!hay_cargador())
              return(0);
          return 1;
	}
}

int write_ext_mem(unsigned long adr, unsigned int len, char* org)
{
        if(stream.tipus == STREAM_RAM)
	{
          memcpy(stream.buf_stream_ram+adr,org,len);
          return 1;
	}
	else if(hay_cargador())
	{
            grabar_eprom(EPROM_ARXIUS_CARG, adr, len, org);
            return 1;
    }
	return 0;
}




void reverse_short(unsigned short* pvar)
{
union 
{
  unsigned short i;
  unsigned char c[2];
} var_o,var_d;
  var_o.i = *pvar;
  var_d.c[0] = var_o.c[1];
  var_d.c[1] = var_o.c[0];
  *pvar = var_d.i;
}


void reverse_long(unsigned long* pvar)
{
union 
{
  unsigned long l;
  unsigned char c[4];
} var_o,var_d;
  var_o.l = *pvar;
  var_d.c[0] = var_o.c[3];
  var_d.c[1] = var_o.c[2];
  var_d.c[2] = var_o.c[1];
  var_d.c[3] = var_o.c[0];
  *pvar = var_d.l;
}

CODE_FAST int llegir_sector(char *buf, short ns, int encriptat)
{
unsigned long addr;//uint adr;
unsigned short chk,i,err;
char b_aux[2];
	err=0;
	do{
		addr=(unsigned long)ns*258L+(ns/127)*2;
		if(!read_ext_mem(addr, 256, buf))
		{
			err=1;
			break;
		}
                if(encriptat)
                {
                  for(i=0;i<256;i+=8)
                      decripta_tx((unsigned char*)buf+i);
                }
		for(i=0,chk=0;i<256;i++)
			chk+=(unsigned short)buf[i];
		//verifica check sum
		if(!read_ext_mem(addr+256, 2, b_aux))
		{
			err=1;
			break;
		}
                reverse_short(&chk);
		if(chk!=*(unsigned short*)b_aux)
		{
			err=ERR_UNIT_BADSECT;
			break;
		}
	}while(0);
	return(err);
}

int escri_sector(char *buf,short nsect, int encriptat)
{
unsigned long addr;
unsigned short chk,i,err;
 err=0;
    do{
        addr=(long)nsect*258L+(nsect/127)*2;
        // calcula check-sum      
        for(i=0,chk=0;i<256;i++)
                chk+=(unsigned short)buf[i];    
/*       
        if(encriptat)
        {
          for(i=0;i<256;i+=8)
              cripta_tx((unsigned char*)buf+i);
        }
*/       
        if(!write_ext_mem(addr, 256, buf))
        {
            err=1;
            break;
        }
        //escriu check sum
        reverse_short(&chk);
        if(!write_ext_mem(addr+256, 2, (char*)&chk))
        {
            err=1;
            break;
        }
      }while(0);
    return(err);
}


/*

int dir_set_entry(t_direntry *entry,short num)
{
int st;
short despl;
char buf[256];
 st=llegir_sector(buf,pqr.sect_dir+(num/8),0);
 if(st==0)
   {
    // cas no error
    despl=(num%8)*sizeof(t_direntry);
    *(t_direntry*)&buf[despl]=*entry;
    st=escri_sector(buf,pqr.sect_dir+(num/8),0);
   }
 return(st);
}


int _flsbuf(int ch,ifacFILE *arx)
{
int err;
short j,iaux;
 err=escri_sector(arx->bufio,arx->transf_sect);
 if(!err)
  {
   //busca sector lliure a la FAT
   for(j=pqr.sect_dat;j<=pqr.last_sect;j++)
     {
      if(FAT[j]==0)
        {
         FAT[j]=0xffff;   // ultim sector de l'arxiu
         iaux=(j/128)*128;
         err=escri_sector((char*)&FAT[iaux],j/128); //actualitza sector FAT
         if(err)break;
         if(arx->transf_sect==arx->chain_sect)
            arx->chain_dest=j; // guarda inici nova cadena
         else
           {
            FAT[arx->transf_sect]=j;   // encadena penultim
            iaux=(arx->transf_sect/128)*128;
            err=escri_sector((char*)&FAT[iaux],arx->transf_sect/128); //actualitza sector FAT
            if(err)break;
           }
         arx->transf_sect=j;
         arx->newsize+=256L;
         arx->_cnt=256;
         arx->_ptr=arx->bufio;
         --arx->_cnt;
         *arx->_ptr++=(char)ch;
         break;
        }
     }
   if(j>pqr.last_sect)
     err=ERR_UNIT_FULL;
  }
 if(err)
   return(err<<8);
 else
   return(ch&0xff);
}
*/

void stream_close(void)
{
  stream.opened=0;
}

int stream_num_bytes_used(void)
{
  return stream.num_bytes_used;
}

//*********************************************
// Apertura  RAM  / e2prom carregador
// Verifica presencia i determina tipus
// Retorna informacio a pqr
//*********************************************
int stream_open(int tipus, char* buf)
{
int st,i,j,k;
char bufaux[2];
t_direntry * p_direntry;
	st=0;
	stream.tipus=tipus;
        stream.buf_stream_ram = buf;
	do
	{
		// Comproba posicio 0
		if(!read_ext_mem(0, 2, bufaux)) 
		{
			st=1;
			break;
		}
		if(bufaux[0]!=ID_UNIT_CONF)
		{
			st=2;
			break;
		}
		// Fi Comproba posicio 0
		if(stream.opened)
			break;
		for(i=0;(i<NMODELS_MCARD);i++)
		{
			if(bufaux[1]==def_pqr0[i].size_id)
			{// trobat tipus paquet
				// comprova ultima posicio
				if(	(stream.tipus!=STREAM_RAM) &&
					(!read_ext_mem(((def_pqr0[i].last_addr)-1L), 2, bufaux)) && 
					(bufaux[1]!=ID_UNIT_CONF) )
					st=2;
				else
					pqr=def_pqr0[i];
				break;
				// Fi comprova ultima posicio
			}
		}
		if(i>=NMODELS_MCARD)
			st=2;
		if(st) 
			break;
		for(i=0;i<pqr.sect_dir;i++)
		{
			st=llegir_sector((char*)&FAT[i*128],i,0);
			if(st)
				break;
		}
                for(j=3;j<(pqr.sect_dir*128);j++)
                  reverse_short(&FAT[j]);
                
                // lectura i conversio sectors directori
		for(;i<pqr.sect_dat;i++)
		{
			st=llegir_sector((char*)&FAT[i*128],i,0);
			if(st)
				break;
                        for(j=0;j<8;j++)
                        {
                          k = i*256 + j*sizeof (t_direntry);
                          p_direntry = (t_direntry *)((char*)FAT+ k);
                          reverse_short(&p_direntry->sector);
                          reverse_long(&p_direntry->fsize);
                        }
		}
	}while(0);
	stream.opened=((st)?0:1);
        if(stream.opened)
        {
         //busca primer sector lliure a la FAT     
         for(j=pqr.sect_dat;j<=pqr.last_sect;j++)
           {
            if(FAT[j]==0)
              {
                stream.num_bytes_used = j *258+(j/127)*2;
                break;
              }
           }
        }
	return(st);
}

// llegeix primera entrada del directori

int dir_get_entry(t_direntry *entry, short num)
{
unsigned char* p;
	p=(unsigned char *)&FAT[((pqr.sect_dir)*128) + ((256*(num/8))/2)];
	*entry=*(t_direntry*)&p[((num%8)*sizeof(t_direntry))];
	return(0);
}


CODE_FAST int _filbuf(ifacFILE *arx)
{
short iaux,err;
 if(arx->newsize <=0)
   return(EOF);
 // busca seguent sector de la file
 iaux=FAT[arx->transf_sect];
 if((iaux==0) || (iaux == (short)0xffff))
   return(EOF);
 arx->transf_sect=iaux;
 arx->_cnt=(short)__min(256L,arx->newsize);
 err=llegir_sector(arx->bufio,arx->transf_sect,arx->entry.crypted);
 arx->newsize-=arx->_cnt;
 arx->_ptr=arx->bufio;
 if(err)
   return(EOF);
 --arx->_cnt;
 return(0xff & *arx->_ptr++);
}



int IFACfclose(ifacFILE* arx)
{
int err;
// !!!!tx50short iaux;
 err=0;
 if((arx->mode=='r') || (arx->_cnt==256))
   free(arx);
/* !!!!tx50 
 else
    {
     err=escri_sector(arx->bufio,arx->transf_sect);
     if(!err)
       {
        if(arx->transf_sect!=arx->chain_sect)
          {
           // enganxa nova cadena (cas 'a')
           FAT[arx->chain_sect]=arx->chain_dest;   // encadena penultim
           iaux=(arx->chain_sect/128)*128;
           err=escri_sector((char*)&FAT[iaux],arx->chain_sect/128); //actualitza sector FAT
          }
        if(!err)
          {
           arx->entry.fsize=arx->newsize - arx->_cnt;
           err=dir_set_entry(&arx->entry,arx->n_entry);
          }
       }
     free(arx);
    }
*/
 return(err);
}

 
ifacFILE* IFACfopen(char* name,char mode, char force_crypted)
{
int err,fin;
short i;
// !!!!tx50  short j,iaux;
ifacFILE* fp;
char auxname[13];
t_direntry auxentry;

 if(!stream.opened)
     return((ifacFILE*)NULL);

 do {
		for(i=0,fin=0;i<pqr.max_entries;i++)
		{
			err=dir_get_entry(&auxentry,i);
			if(err)
				break;
			if(auxentry.name[0]==0)
			{fin=1;break;}
			if(auxentry.name[0]!=0xe5)
			{
				memcpy(auxname,auxentry.name,12);
				auxname[12]=0;
				if(strcmp(auxname,name)==0)
					break;
			}
		}  // seguent entry
		if(err)
			break;  //cas error
		if(i==pqr.max_entries)
			fin=1;
     if(!fin)
       {// cas ha trobat l'arxiu

/* !!!!tx50         
        if(mode=='w') //per reescriure a partir de 0
          {
           auxentry.fsize=0L;
           err=dir_set_entry(&auxentry,i);
           if(err) break;
           //borra la cadena de l'arxiu
           j=FAT[auxentry.sector];
           FAT[auxentry.sector]=0xffff;
           while(j != (short)0xffff)
             {
              iaux=FAT[j];
              FAT[j]=0x0000;
              j=iaux;
             }
           for(j=0;j<pqr.sect_dir;j++)
              escri_sector((char*)&FAT[j*128],j); //actualitza sectors FAT
          }
*/
       }
     else
     if(fin)
       { //cas no ha trobat l'arxiu
        if(mode=='r')
          {err=ERR_UNIT_FNOTFOUND;break;}
/* !!!!tx50
        else
          {// afegir file al directori
           for(i=0,fin=0;i<16;i++)
             {
              err=dir_get_entry(&auxentry,i);
              if(err) break;
              if((auxentry.name[0]==0)||(auxentry.name[0]==0xe5))
                {  // trobat entry buit (o deleted)
                 memcpy(auxentry.name,name,12);
                 auxentry.fsize=0L;
                 //busca primer sector lliure a la FAT
                 for(j=pqr.sect_dat;j<=pqr.last_sect;j++)
                   {
                    if(FAT[j]==0)
                      {
                       auxentry.sector=j;
                       FAT[j]=0xffff;   // ultim sector de l'arxiu
                       iaux=(j/128)*128;
                       escri_sector((char*)&FAT[iaux],j/128); //actualitza sector FAT
                       dir_set_entry(&auxentry,i);
                       break;
                      }
                   }
                 if(j>pqr.last_sect)
                   {err=ERR_UNIT_FULL;break;}
                 break;
                }
             }  // seguent entry
           if(i==16)
             err=ERR_UNIT_DIRFULL;
           if(err) break;  //cas error
          }
*/
       }
     // cas ha trobat arxiu o l'ha creat
     fp=(ifacFILE*)malloc(sizeof(ifacFILE));
     fp->mode=mode;
     fp->entry=auxentry;
     fp->n_entry=i;
     fp->transf_sect=fp->entry.sector;
     fp->chain_sect=fp->entry.sector;
     switch(mode)
       {
        case 'r':
          fp->_cnt=(int)__min(256L,fp->entry.fsize);
          err=llegir_sector(fp->bufio,fp->transf_sect,fp->entry.crypted);
          fp->newsize=fp->entry.fsize - fp->_cnt;
          fp->_ptr=fp->bufio;
          break;
/* !!!!tx50          
        case 'w':
          fp->newsize=256L;
          fp->_cnt=256;
          fp->_ptr=fp->bufio;
          break;
        case 'a':
          // busca final arxiu per afegir
          iaux=fp->entry.sector;
          fp->newsize=fp->entry.fsize;
          while(FAT[iaux]!=0xffff)
             {
              fp->newsize-=256L;
              if(fp->newsize<0L)
                {err=ERR_UNIT_CHAIN;break;}
              iaux=FAT[iaux];
             }
          fp->transf_sect=iaux;
          fp->chain_sect=iaux;
          fp->_cnt=256-(fp->entry.fsize%256);
          if((fp->_cnt==256) && fp->entry.fsize)
            fp->_cnt=0;
          err=llegir_sector(fp->bufio,fp->transf_sect);
          fp->newsize=fp->entry.fsize + fp->_cnt;
          fp->_ptr=fp->bufio+(256-fp->_cnt);
          break;
*/
       }
    }while(0);
 if(err)
   return((ifacFILE*)NULL);
 else
 {
   if(force_crypted)
     fp->entry.crypted = 1;
   return(fp);
 }
}


CODE_FAST char *IFACfgets(char *buf, int n, ifacFILE* fp)
{
int i,c,err;
 for(i=0,err=0;i<(n-1);i++)
   {
    c=getc(fp);
    if(c==EOF){err=1;break;} 
    buf[i]=(char)c;
    if(c=='\n')break;
   }               
 buf[i]=0;
 if(err)
   return((char*)NULL);
 return(buf);    
}

CODE_FAST int IFACfread(char *buf, int n, ifacFILE* fp)
{
int i,c;
	for(i=0; i<n; i++)
	{
		c=getc(fp);
		if(c==EOF)
			return i;
		buf[i]=(char)c;
	}               
	return i;
}


/*

#define PQR_BASEADDR		0x400000L
#define read_mem(long_adr) (*((unsigned char*)long_adr))

int format_paqram(int tipus_device)
{
int st;
unsigned char ch,i,j;
int model;

 st=0;
 if(tipus_device==STREAM_MCARD)
   {
    do
       {
        if(!tension_paq_ram)
           {st=1;break;}
        // Comproba posicio 0
        write_mem(PQR_BASEADDR,ID_UNIT_CONF);
        ch=read_mem(PQR_BASEADDR);
        if(ch!=ID_UNIT_CONF)
           break;
        // determina capacitat paquet
        for(i=0;i<NMODELS_MCARD;i++)
          write_mem(PQR_BASEADDR+def_pqr0[i].last_addr,0x6a);
        ch=read_mem(PQR_BASEADDR);
        if(ch!=ID_UNIT_CONF)
          break;  // error.Ha modificat posicio 0
        write_mem(PQR_BASEADDR+def_pqr0[NMODELS-1].last_addr,ID_UNIT_CONF);
        for(i=0;i<NMODELS_MCARD;i++)
           {
            ch=read_mem(PQR_BASEADDR+def_pqr0[i].last_addr);
            if(ch==ID_UNIT_CONF)
               break;    // l'ha trobat
           }
        if(i>(NMODELS_MCARD-1))
           break;
        model=i;
        pqr=def_pqr0[model];
        // prepara sectors FAT
        for(i=0;i<=pqr.last_sect;i++)
          FAT[i]=0;
        FAT[0]=ID_UNIT_CONF_INT+pqr.size_id;
        for(i=0;i<pqr.sect_dir;i++)
          escri_sector((char*)&FAT[0]+i*256,i);
        for(j=0;j<2;j++)
           {
            for(i=0;i<8;i++)
               direntry[i].name[0]=0;
            escri_sector((char*)&direntry[0],pqr.sect_dir+j);
           }
        // break;  no cal
       }while(0);
   }
 return(st);
}
*/
