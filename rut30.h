//*********************************************************
// rut30.h
//*********************************************************

#ifndef _INC_RUT30
#define _INC_RUT30





extern unsigned char bin_bcd(unsigned char);
extern unsigned char bcd_bin(unsigned char);
extern unsigned short bcd_short(unsigned char const* p);
extern void bcd2btoasc(char * dest, char *orig,unsigned char nd);
extern void bin2btoasc(char * dest, char *orig,unsigned char nd);
extern unsigned short reverse_int(unsigned short i);

extern void set_long_to_4chars(unsigned char * adr,unsigned long val);

extern void out_luzfront(unsigned char mask);
extern int cods_programat(int n_cod);
extern int i_sqrt(int n);

extern void tartrab_a_buf_right(unsigned char *buf,long tartrab);
extern unsigned short  get_anydia_actual(void);
extern unsigned short  get_minut_del_dia(void);
extern char dins_interval(short value, short interval_ini, short interval_fin);

#endif

