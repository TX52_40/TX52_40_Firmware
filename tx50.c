 


#include "rtos.h"
#include <stdlib.h>
#include <stdio.h>
#include "iostr710.h"

#include "hard.h"

#include "adc.h"
#include "i2c.h"
#include "display.h"
#include "teclat.h"
#include "spi.h"
#include "tx30.h"
#include "impre.h"
#include "serial.h"
/*
#include "reloj.h"
#include "eeprom.h"
#include "can.h"
*/



#define RAM_INTERNA _Pragma("location=\"RAM_INTERNA\"")



OS_STACKPTR RAM_INTERNA int Stack_tx50[4096];          /* Task stacks */
OS_TASK TCB_tx50;                  /* Task-control-blocks */




extern void task_tx_interno(void);

static void Task_tx50(void)
{
    
    OS_Delay (20);
    
    // Configura I2C 
    config_i2c(NBUS_PLACA);
    config_i2c(NBUS_CARG);
    OS_Delay(20);
  
    hard_detect();
    hard_init();
  

    spi_init();
    OS_Delay(2);
    
    adc_init();

    buzzer_init();
    display_init();
    teclat_init();
    reloj_init();
    


    OS_Delay(100);    // per que puguin entrar tasques d'inicialitzacio
  
    task_tx_interno();
}


/*********************************************************************
*
*       main
*
*********************************************************************/
/****** Enhanced interrupt controller (EIC) *************************/

#define __EIC_ICR   *(volatile OS_U32*)0xfffff800
#define __EIC_CICR  *(volatile OS_U32*)0xfffff804
#define __EIC_CPIR  *(volatile OS_U32*)0xfffff808
#define __EIC_IVR   *(volatile OS_U32*)0xfffff818
#define __EIC_FIR   *(volatile OS_U32*)0xfffff81C
#define __EIC_IER   *(volatile OS_U32*)0xfffff820
#define __EIC_IPR   *(volatile OS_U32*)0xfffff840
#define __EIC_SIR0  *(volatile OS_U32*)0xfffff860

int main(void) {  
  
  OS_IncDI();                      /* Initially disable interrupts  */
  
  __EIC_ICR = 0x00;   // Disable IRQ  FIQ
  __EIC_IER = 0x00000000;     // Reset pending ints
  __EIC_IPR = 0xffffffff;     // Reset pending ints

  OS_InitKern();                   /* initialize OS                 */
  OS_InitHW();                     /* initialize Hardware for OS    */
  

  PORT3 = 0x40;IOPORT_3 = PORT3;
  PORT4 = 0x80;IOPORT_4 = PORT4;
  PORT6 = 0x0C;IOPORT_6 = PORT6;
   
  /* You need to create at least one task here !                    */
  OS_CREATETASK(&TCB_tx50, " Task_tx50", Task_tx50, 100, Stack_tx50);
  OS_Start();                      /* Start multitasking            */
  return 0;
}




