// ***************************************************************************
// i2c.h
// ***************************************************************************

#ifndef __I2C_H
#define __I2C_H

extern void delay_us(int us);

#define NBUS_PLACA  0
#define NBUS_CARG   1

// ***********************
// llista periferics I2C
// ***********************

#define TECLAT_CARG        0
#define RELLOTGE_CARG      1
#define EPROM_TARIFA_CARG  2
#define EPROM_CONF_CARG    3
#define RELLOTGE_PLACA     4
#define EPROM_BASE         5
#define EPROM_SOPORTE      6
#define PIC_FIRMWARE       7
#define PIC_BOOTLOADER     8
#define EPROM_ARXIUS_CARG  9
#define DISPLAY_CARG      10
#define IO_EXPANDER_LUCES 11
#define CONTROL_HORARI    12
#define EPROM_PROPIETARIO 13
#define DRIVER_DISPLAY    14
#define IO_EXPANDER_TX40  15
#define POT_DIGITAL       16

// tipus periferic I2C rellotge
#define RELLOTGE_VELL   0
#define RELLOTGE_NOU    1

// tipus periferic I2C E2PROM
#define EPROM_256x8   0
#define EPROM_DEFAULT 1
#define EPROM_ADDR3   2


// Codigs dels I2C del pic
#define READ_FIRMWARE 0x00
#define ADR_STORE     0x40 
#define RESET_PIC     0x60
#define BACK_TO_BOOT  0x80
#define ADR_INFORM    0xA0
#define ONOFF         0xC0


typedef struct s_def_chip_i2c{
  char nbus;
  char nclock;
  char slave;
  char tipus;    // tipus del periferic.Depen de si es Eprom, rellotge etc.
}t_def_chip_i2c;

extern const t_def_chip_i2c DEF_CHIP_I2C[];

// ***************************************************************
// prototips
// ***************************************************************

void config_i2c(int nbus);
int status_i2c(int nChip);

unsigned char input_i2c(int nChip);
void output_i2c(int nChip, unsigned char dato);

void write_i2c(int nChip, unsigned char dir, unsigned char nb, char *buf);
int wread_i2c(int nChip, unsigned char dir, unsigned char nb, char *buf);

void write_i2c_extnd(int nChip, unsigned int dir, unsigned int nb, char *buf);
void wread_i2c_extnd(int nChip, unsigned int dir, unsigned int nb, char *buf);

void wread_i2c_adr3(int nChip, unsigned int dir, unsigned int nb, char *buf);
void write_i2c_adr3(int nChip, unsigned int dir, unsigned int nb, char *buf);

void write_i2c_disp(unsigned char nb, char *buf);
#endif

