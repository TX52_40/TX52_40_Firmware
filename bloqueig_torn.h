// *********************************************************
// bloqueig_torn.h
// *********************************************************


#ifndef _INC_BLOQUEIG_TORN
#define _INC_BLOQUEIG_TORN

#include "reloj.h"


#define SEGONS_VISU_ERROR 5  // 5" per  visualitzar error


#define ETORN_NO_INI                0
#define ETORN_FRANQUICIA_DIST       1
#define ETORN_FRANQUICIA_EXHAURIDA  2   // pero no s'ha passat encara per lliure
#define ETORN_INICIAT               3

#define TIPO_DESCANSO_DIASEM  0x01
#define TIPO_DESCANSO_FINSEM  0x02

#define REARMAR_NO          0
#define REARMAR_GLOBAL      1
#define REARMAR_SEGON_TORN  2


#define METRES_MOGUT_EN_OFF   2000

#define VISU_TORN_HORA_FI     1
#define VISU_TORN_TEMPS_QUEDA 2


#define OP_DESCANS  0
#define OP_TANCAR   1


#define NUM_MAX_TORNS 2
#define NUM_MAX_DESCANS_REGISTRAR  5
#define NUM_MAX_DESCANS_PREPROG    6   // 3 per torn

struct s_registre_descans
{
 long hora_inici_descans;
 long hora_final_descans;
};

typedef struct s_registre_torn
{
 unsigned short anydia_torn;
 unsigned short diames_torn;
 long hora_inici_torn;
 long hora_final_torn;
 struct s_registre_descans registre_descans[NUM_MAX_DESCANS_REGISTRAR];
}t_registre_torn;


struct s_bloqueo_torn
{
 unsigned short anydia_torn;
 unsigned long  minuts_queden;
 unsigned short anydia_rearmar;
 unsigned short minut_rearmar;
 unsigned short acum_descans;
 unsigned short acum_sense_tensio;
 unsigned char num_descans_fets;
 unsigned char num_descans_registrar;
 long  dist_off;
 unsigned short  dist_franquicia;
 unsigned char estat_torn;
 char estat_torn_save;
 unsigned char dia_descans;
 unsigned char semana_descans;
 
 char stdesblq;
 short cntdesblq;
 char ve_de_baix_consum;
 char desactivat_x_cap_setmana;
 int torn_en_curs;
 char prescindir_canvi_minut;
 int minut_tancament;
 int minut_segle_inici_descans;
 int minut_segle_final_descans;
 short minuts_descans_queden;
 unsigned short anydia_last_torn;
 unsigned short diames_last_torn;
 unsigned short anydia_last_itv;
 char esta_passant_itv;
 char minuts_passant_itv;
 char estat_retorn_itv;
 char zona_descans;
 char dins_dates_desactivacio_torn;
 // registre torn i descans
 char index_registre_torn;
 t_registre_torn registre_torn[NUM_MAX_TORNS];
 char index_visu_torn;
 char index_visu_descans;
 // pre programacio events
 char events_preprog;
 char preprog_index;
 short preprog_hora_inici_torn[2];
 short preprog_hora_final_torn[2];
 short preprog_hora_inici_descans[NUM_MAX_DESCANS_PREPROG];
 short preprog_hora_final_descans[NUM_MAX_DESCANS_PREPROG];
 int tornDesactivatDiaEspecial;
};                         

extern struct s_bloqueo_torn bloqueo_torn;


extern void rearmar_bloqueo_torn(int mode, int passar_a_off);
extern char bloqueo_paso_ocup(void);
extern char bloqueo_luz_libre(void);
extern unsigned short numero_semana(t_time *time);
extern void gestio_bloqueo_torn_0(struct s_time *time_fallo_tensio, unsigned long segons_off);
extern void bloqueo_turno_minuto(void);
extern void bloqueo_turno_hm(void);
extern void bloqueo_turno_10seg(void);
extern void bloqueig_torn_configura(void);
extern int bloqueig_torn_prou_descansos(void);
extern void recalculs_gt_bloqueig_torn(void);
extern void paso_a_libre_bloqueig_torn(void);
extern int OFF_LIBRE_bloqueig_torn(void);
extern void france_tancar_torn(long minuts2000);
extern void registrar_inici_descans(void);
extern int processar_transmissio_control_torn(char* buf_resp);
extern void descans(void);
extern int inSecondInterval(void);
extern int shiftControlDeactivated(void);

#endif