// ******************************************************
// prima.c    torns i serveis
// ******************************************************


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tx30.h"
#include "tdgp.h"
#include "regs.h"
#include "aplic.h"
#include "prima.h"
#include "hard.h"
#include "rut30.h"

__no_init t_prima prima;

// inicialitza taules torns i serveis
void prima_init(void)
{
  prima.max_regs_torn = getMaxFilas(TABLA_PRIMA_TORNS);
  prima.reg_next_torn = 0;
  prima.num_regs_torn = 0;
  prima.max_regs_servei = getMaxFilas(TABLA_PRIMA_SERVEIS);
  prima.reg_next_servei = 0;
  prima.num_regs_servei = 0;
  
  prima.reg_servei_en_curs = -1;
}

// afegir registre torn

int prima_inici_torn(void)
{
  // mira si encara hi han registres torn lliures
  if(prima.num_regs_torn >= prima.max_regs_torn )
  {
    // Torns ple. Llegeig el mes vell per eliminar-lo
    tabla_move_reg(TABLA_PRIMA_TORNS,prima.reg_next_torn,REG_PRIMA_TORN_AUX,TAULA_LOAD);  // llegeig tor a eliminar
    // llibera registres servei
    if(reg_prima_torn_aux->num_serveis)
    {
      if(prima.num_regs_servei >= reg_prima_torn_aux->num_serveis)
        prima.num_regs_servei -= reg_prima_torn_aux->num_serveis;
      else
        prima.num_regs_servei = 0;
    }
    prima.num_regs_torn = prima.max_regs_torn-1;
  }
  // guarda torn
  reg_prima_torn->nreg_servei_ini = prima.reg_next_servei;
  tabla_move_reg(TABLA_PRIMA_TORNS,prima.reg_next_torn,REG_PRIMA_TORN,TAULA_STORE);  
  prima.num_regs_torn++;
  prima.reg_torn_obert = prima.reg_next_torn;
  if(prima.reg_next_torn < (prima.max_regs_torn-1))
    prima.reg_next_torn++;
  else
    prima.reg_next_torn = 0;
  
  return(0);
}

int prima_final_torn(void)
{
  // guarda dades final torn
  tabla_move_reg(TABLA_PRIMA_TORNS,prima.reg_torn_obert,REG_PRIMA_TORN,TAULA_STORE);
  
   return(0);
}

// nomes es crida si hi ha torns guardats
// (prima.num_regs_torn != 0)
int prima_get_index_last_torn(void)
{
  if(prima.reg_next_torn >0)
    return(prima.reg_next_torn - 1);
  else
    return(prima.max_regs_torn - 1);
}

int prima_get_index_prev_torn(int ireg)
{
   if(ireg >0)
    return(ireg - 1);
  else
    return(prima.max_regs_torn - 1);
 
}

int prima_llegir_torn(int ireg)
{
  tabla_move_reg(TABLA_PRIMA_TORNS,ireg,REG_PRIMA_TORN,TAULA_LOAD);  
  return(0);
}

int prima_afegir_servei()
{
int reg_a_eliminar;
int ret;

  while(fallo_tension()); 
  
  // mira si encara hi han registres servei lliures
  if(prima.num_regs_servei >= prima.max_regs_servei )
  {
    // Tots el registres de servei plens.
    // Mira si pot eliminar un torn    
    while(prima.num_regs_torn > 1)
    {
      // Busca el mes vell per eliminar-lo
      reg_a_eliminar = prima.reg_next_torn - prima.num_regs_torn;
      if(reg_a_eliminar < 0)
        reg_a_eliminar += prima.max_regs_torn;
      tabla_move_reg(TABLA_PRIMA_TORNS,reg_a_eliminar,REG_PRIMA_TORN_AUX,TAULA_LOAD);  // llegeig tor a eliminar
      // elimina torn
      prima.num_regs_torn--;
      // llibera registres servei si el torn en t�
      if(reg_prima_torn_aux->num_serveis)
      {
        if(prima.num_regs_servei >= reg_prima_torn_aux->num_serveis)
          prima.num_regs_servei -= reg_prima_torn_aux->num_serveis;
        else
          prima.num_regs_servei = 0;
        break;  // surt del while
      }
    }
    if(prima.num_regs_servei >= prima.max_regs_servei)
    {
      // no ha pogut alliberar servei
      // !!!!!!! de moment no es far res.
      // al llistat de serveis ja hi ha un limit 
      // &&(i<prima.max_regs_servei)
      // per evitar que imprimis mes del que hi ha
    }
  }
  // guarda servei
  tabla_move_reg(TABLA_PRIMA_SERVEIS,prima.reg_next_servei,REG_PRIMA_SERVEI,TAULA_STORE);
  ret = prima.reg_next_servei;  
  if(prima.reg_next_servei < (prima.max_regs_servei-1))
    prima.reg_next_servei++;
  else
    prima.reg_next_servei = 0;
  if(prima.num_regs_servei < prima.max_regs_servei)
    prima.num_regs_servei++;
  return(ret);
  
}

int prima_actualitzar_servei(int ireg)
{

  while(fallo_tension()); 
  
  tabla_move_reg(TABLA_PRIMA_SERVEIS,ireg,REG_PRIMA_SERVEI,TAULA_STORE);  
  return(0);
}

int prima_llegir_servei(int ireg)
{
  tabla_move_reg(TABLA_PRIMA_SERVEIS,ireg,REG_PRIMA_SERVEI,TAULA_LOAD);  
  return(0);
}

int prima_get_index_next_serv(int ireg)
{
   if((ireg+1) >=prima.max_regs_servei)
    return(0);
  else
    return(ireg + 1);
 
}

void prima_get_dades_servei(void)
{
  int st;
   // prepara dades registre servei prima
   reg_prima_servei->numero_servei = totalitzador_read(TOT_NSER, &st)%10000;
   reg_prima_servei->servei_ini = serv[0].finicio;
   reg_prima_servei->servei_fi = serv[0].ffin;
   reg_prima_servei->servei_import = serv[0].import;
   reg_prima_servei->servei_supl = serv[0].supl_desgl[0] +
                                    serv[0].supl_desgl[1] +
                                    serv[0].supl_desgl[2] +
                                    serv[0].supl_desgl[3] +
                                    serv[0].supl_aut;
   reg_prima_servei->servei_total = serv[0].import + reg_prima_servei->servei_supl;
   reg_prima_servei->servei_kmlib = servei.hm_libre;
   reg_prima_servei->servei_kmoc  = serv[0].hm_oc;
   reg_prima_servei->servei_min_oc = serv[0].minuts_oc;
   reg_prima_servei->servei_min = serv[0].minuts_oc + servei.minuts_lib;
   tartrab_a_buf_right(reg_prima_servei->servei_buf_tartrab, serv[0].tartrab);
   reg_prima_servei->servei_hm_libre_pasj = servei.hm_libre_pasj;
   reg_prima_servei->servei_velmax = servei.velmax;
   reg_prima_servei->servei_velmaxlib = servei.velmaxlib;
   reg_prima_servei->servei_fet_ticket = 0;    
   // guarda a taula serveis
   prima.reg_servei_en_curs = prima_afegir_servei();
   // acumula a torn dades del servei
   // es guardaran al tancar torn
   reg_prima_torn->num_serveis++;
   reg_prima_torn->total_torn += reg_prima_servei->servei_total;
   reg_prima_torn->supl_torn += reg_prima_servei->servei_supl;
   reg_prima_torn->import_torn += serv[0].import;
   reg_prima_torn->km_oc_torn += serv[0].hm_oc;
   reg_prima_torn->km_torn += (servei.hm_libre + serv[0].hm_oc);
   reg_prima_torn->min_oc_torn += serv[0].minuts_oc;
   reg_prima_torn->min_torn += (servei.minuts_lib +serv[0].minuts_oc);
   // iva
   reg_prima_torn->neto_torn += reg_servei->import_net;
   reg_prima_torn->iva_torn += reg_servei->import_iva;
   switch(servei.tipus_iva)
   {
   case 0:
     reg_prima_torn->iva_0 += reg_servei->import_iva;
     break;
   case 1:
     reg_prima_torn->iva_1 += reg_servei->import_iva;
     break;
   case 2:
     reg_prima_torn->iva_2 += reg_servei->import_iva;
     break;
   }
   
}

