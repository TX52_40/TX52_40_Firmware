//************************************************************
// metros.h    generador impulsos
//************************************************************

#ifndef _INC_METROS
#define _INC_METROS

#define DIVISOR_MAX_K 5000L



typedef struct gi
{
 char giTipo;
 char giNivell;
 int  giTrigger;
}t_gi;

#define giTrigger_DEFAULT 1000
#define giTrigger_MAX 2000
#define giTrigger_MIN 100

extern unsigned int COLA_MF;
extern char flanc_pujada;
extern unsigned char CNT_DIVK;

extern unsigned int get_clear_COLA_MF(void);
extern void decr_COLA_MF(void);   // cola COLA_MF para TX30
extern void instala_k(unsigned long K,t_gi* gi);
extern int k_fora_limits(long k);
extern int giTrigger_fora_limits(int trigger);

extern void velocimetro_confi(unsigned long k,unsigned int divk,float delta);
extern unsigned int velocimetro_get(void);
extern void  velocimetro_INTD(void);
extern void  velocimetro_int_1ms(void);


#endif
