/***************************************************************/
/*   rut30.c       rutines auxiliars  tx30                     */
/***************************************************************/
#include <stdlib.h>
#include <string.h>

#include "iostr710.h"

#include "rut30.h"
#include "display.h"
#include "tx30.h"
#include "i2c.h"
#include "tautrans.h"
#include "serial.h"
#include "hard.h"
#include "bloqueig_torn.h"
#include "debug.h"

char dins_interval(short value, short interval_ini, short interval_fin)
{
	char ret = 1;  
	if(interval_fin > interval_ini)
	{
		// interval no creua 24h
		if((value < interval_ini) || (value > interval_fin))
			ret = 0;
	} else {
		// interval creua 24h
		if((value < interval_ini) && (value > interval_fin))
			ret = 0;
	}
	return ret;  
}



void set_long_to_4chars(unsigned char * adr,unsigned long val)
{
union u_var
{
  unsigned long l;
  unsigned char c[4];
} var;
  var.l = val;
  *(adr) = var.c[0];
  *(adr+1) = var.c[1];
  *(adr+2) = var.c[2];
  *(adr+3) = var.c[3];
}

unsigned char bin_bcd(unsigned char v)
{
unsigned char vv;
	vv=(v/10)*16+v%10;
	return (vv);
}                

unsigned char bcd_bin(unsigned char v)
{
	return((v/16)*10 +v%16);
}

unsigned short bcd_short(unsigned char const* p)
{
unsigned short s;

  s = bcd_bin(*(p+1))*100 + bcd_bin(*p);
  return s;
}

unsigned short reverse_int(unsigned short i)
{
  return((i>>8)|(i<<8));
}

//**********************************************************
// posa a 0 variables a x ram segons taula :num bytes,adressa
//**********************************************************
void clear_xdata(struct s_clear_x const* p)
{
 while(p->nb)
  {
   memset(p->adr,0,p->nb);
   p++;
  } 
} 

void set_nibble(unsigned char nib,unsigned char nd,unsigned char *buf)
{
 buf[nd/2]= (nd & 0x1) ? ((buf[nd/2] & 0xf) | (nib<< 4)):((buf[nd/2] & 0xf0) | nib);
}

unsigned char get_nibble(unsigned char nd,unsigned char *buf)
{
   return((nd & 0x1) ? (buf[nd/2] & 0xf0)>>4 : buf[nd/2] & 0xf);
}

unsigned char get_nibble_bin(unsigned char nd,unsigned char *buf)
{
   return((nd & 0x1) ? (buf[nd/2] / 10) : buf[nd/2] % 10);
}


unsigned char es_bcd(uchar c)
{
 return(((c&0xf0)<0xa0) && ((c&0x0f)<0xa));
}                                           


/******************************************************/
/* pasa n bytes de bcd a ascii                        */
/******************************************************/
void bcdtoasc(char * dest, char *orig,unsigned char nd)
{
 unsigned char  i;
 for(i=0;i<nd;i++)
   dest[i]=(orig[i] >9) ?  orig[i]+0x37 : orig[i]|0x30 ;
}

/******************************************************/
/* pasa n digits bcd (2 per byte)  a ascii            */
/******************************************************/
void bcd2btoasc(char * dest,char *orig,unsigned char nd)
{
 unsigned char i;
 for(i=0;i<nd;i++)
   {
    dest[i]=get_nibble(i,(unsigned char*)orig)|0x30;
    if(dest[i] >0x39)
       dest[i]+=7;
   }
}

/******************************************************/
/* pasa n digits bin (2 per byte)  a ascii            */
/******************************************************/
void bin2btoasc(char * dest, char *orig,unsigned char nd)
{
 unsigned char i;
 for(i=0;i<nd;i++)
   {
    dest[i]=get_nibble_bin(i,(unsigned char*)orig)|0x30;
    if(dest[i] >0x39)
       dest[i]+=7;
   }
}

/*******************************************************/
/* pasa de buffer bcd a long                           */
/*   buffer de nd bytes (1 digit per byte)             */
/*            digit menys sig. a lsB                   */
/*******************************************************/
unsigned long bcdtol(unsigned char *buf,unsigned char nd)
{
unsigned char  i;
unsigned long  l;
  i=nd-1;
  while((i>0)&&(!buf[i])) i--;
  l=buf[i];
  while(i>0)
     l=l*10L+buf[--i];
 return(l);
}

int i_sqrt(int n)
{
int i;
 if(n<4)
   return(1);
 for(i=2;(i*i)<=n;i++)
  ;             
 return(i-1); 
}



unsigned int calcul_check(void)
{
unsigned int  suma;
volatile  unsigned int suma1;
 suma=sum_check((char*)&tarcom,sizeof tarcom);
 suma+=sum_check((char*)tarifa,32*sizeof(struct s_tarifa));
 suma+=sum_check((char*)bloc_tar,32*sizeof(struct s_bloc_tar));
 suma += sum_check((char*)zonh,sizeof zonh);
 suma += sum_check((char*)tope_imp,sizeof tope_imp);
 suma += sum_check((char*)tope_dist,sizeof tope_dist);
 suma += sum_check((char*)tope_temps,sizeof tope_temps);
 suma += sum_check((char*)canvi_autom,sizeof canvi_autom);
 
 suma1 = suma*2;
 
 return(suma);
}


void grabar_cods(void)
{
 cods_tax.check=sum_check((char*)&cods_tax.cods[0][0],sizeof cods_tax.cods);
 //grabar_eprom(EPROM_TAX,EEPT_CODS_POS,EEPT_CODS_LEN,(unsigned char *)&cods_tax);
 grabar_eprom_tabla(GE_CODS);
}

void grabar_cods_ext(void)
{
 cods_tax_ext.check=sum_check((char*)&cods_tax_ext.cods[0][0],sizeof cods_tax_ext.cods);
 //grabar_eprom(EPROM_TAX,EEPT_CODS_POS,EEPT_CODS_LEN,(unsigned char *)&cods_tax);
 grabar_eprom_tabla(GE_CODS_EXT);
}


/******************************************************************/
/* llegeix de EPROM_TAX codigs secrets                            */
/* si check sum no es correcte, desactiva les funcions associades */
/******************************************************************/
void leer_verif_cods(void)
{
 //leer_eprom(EPROM_TAX,EEPT_CODS_POS,EEPT_CODS_LEN,(unsigned char *)&cods_tax);
 leer_eprom_tabla(GE_CODS);
 if(cods_tax.check!=sum_check((char*)&cods_tax.cods[0][0],sizeof cods_tax.cods))
    {
     //leer_eprom(EPROM_TAX,EEPT_CODS_POS,EEPT_CODS_LEN,(unsigned char *)&cods_tax);
     leer_eprom_tabla(GE_CODS);
     if(cods_tax.check!=sum_check((char*)&cods_tax.cods[0][0],sizeof cods_tax.cods))
        {
         memset(cods_tax.cods,0,sizeof cods_tax.cods);
         grabar_cods();
        }
    }

 leer_eprom_tabla(GE_CODS_EXT);
 if(cods_tax_ext.check!=sum_check((char*)&cods_tax_ext.cods[0][0],sizeof cods_tax_ext.cods))
    {
     leer_eprom_tabla(GE_CODS_EXT);
     if(cods_tax_ext.check!=sum_check((char*)&cods_tax_ext.cods[0][0],sizeof cods_tax_ext.cods))
        {
         memset(cods_tax_ext.cods,0,sizeof cods_tax_ext.cods);
         grabar_cods_ext();
        }
    }
}

// retorna 1/0 segons el codi secret n_cod estigui programat o no 
int cods_programat(int n_cod)
{
int j;
   if((n_cod == 3) && tarcom.torn_continu)
     return(1);
   if(n_cod < 6)
   {
   for(j=0;j<LEN_CODS;j++)
      if(cods_tax.cods[n_cod][j]!=0)
        {
         return(1);
        }
    return(0);
   }
   else
   {
   for(j=0;j<LEN_CODS;j++)
      if(cods_tax_ext.cods[n_cod-6][j]!=0)
        {
         return(1);
        }
    return(0);
   }
}
/******************************************************************/
/* llegeix de EPROM_TAX fecha paro                                */
/* si  no es correcte, la desactiva                               */
/******************************************************************/



void grabar_fp(void)
{
 //grabar_eprom(EPROM_TAX,EEPT_FP_POS,EEPT_FP_LEN,(unsigned char *)&fecha_paro);
 grabar_eprom_tabla(GE_FP);
}

void leer_verif_fp(void)
{
unsigned char ok;
unsigned char  i;
unsigned char diaux;
 ok=1;
 //leer_eprom(EPROM_TAX,EEPT_FP_POS,EEPT_FP_LEN,(unsigned char *)&fecha_paro);
 leer_eprom_tabla(GE_FP);
 for(i=0;i<3;i++)
     if(!es_bcd(fecha_paro[i]))
        ok=0;
 if((fecha_paro[1]>0x12)|| (fecha_paro[1]==0))
    ok=0;
 if(ok)
   {
    diaux=bin_bcd(dias_mes[fecha_paro[1]]);
    if((diaux==0x28) && ((bcd_bin(fecha_paro[2]) & 0x03)==0 ))
      diaux=0x29;
    if((fecha_paro[0]==0) || (fecha_paro[0]>diaux))
       ok=0;
   }

 if(ok)
   {
    long_fp=fecha_paro[2];
    if(fecha_paro[2]<0x90)
       long_fp+=256;      /* cas >=2000 */
    long_fp*=1000L;
    long_fp+=dia_del_any_from_bcd(&fecha_paro[0]);
   }
 else
   {
    fecha_paro[0]=0x31;
    fecha_paro[1]=0x12;
    fecha_paro[2]=0x89;  /*  31_12_2089   */
    long_fp=99999999L;
   }
}



/*********************************************************************/
/*  Actualitza bit 0 de MASK_GEN  0/1 no/si  rebasada fecha paro     */
/*********************************************************************/
void gestio_fecha_paro(void)
{
unsigned long laux;
 laux=gt.time.year;
 if(gt.time.year<0x90)
    laux+=256;      /* cas >=2000 */
 laux*=1000L;
 laux+=gt.dia_del_any;

 if(laux>=long_fp)  /* cas toca parar. reverifica */
   {
    leer_verif_fp();
    if(laux>=long_fp)
       {
        MASK_GEN|=MSK_FP;   /* fecha paro cumplida */
        return;
       }
   }
 MASK_GEN &= ~MSK_FP;
 return;
}


/***************************************************************/
/* calcula segon del dia                                       */
/***************************************************************/
unsigned long segon_del_dia(struct s_time *t)
{
unsigned long aux;
 aux=bcd_bin(t->sec);
 aux+=(unsigned long)bcd_bin(t->minute)*60L;
 aux+=(unsigned long)bcd_bin(t->hour)*3600L;
 return(aux);
}


void anydia_a_dma(unsigned int iaux,char *dma)
{
char j;
 dma[2]=(char)((iaux>>9)+90)%100; // any 

 iaux&=0x01ff;
 if((dma[2]%4)==0 && iaux>=60)
      {
       if(iaux==60)
         {
          dma[1]=2;
          dma[0]=29;
          return;
         }
       else
         --iaux;
      }
 for(j=1;;j++)
    {
     if(dias_mes[j]>=(int)iaux)
        break;
     iaux-=dias_mes[j];
    }
 dma[1]=j;
 dma[0]=iaux;
 for(j=0;j<3;j++)
   dma[j]=bin_bcd(dma[j]);
 return;
}


//***************************************************************
// calcula elements estructura gt
//***************************************************************
void recalculs_gt(void)
{
unsigned int  iaux;
 gt.minut_del_dia=(unsigned int)bcd_bin(gt.time.hour)*60+(unsigned int)bcd_bin(gt.time.minute);
 gt.dia_del_any=dia_del_any_from_bcd(&gt.time.day);
 iaux=bcd_bin(gt.time.year);
 if(iaux<90)iaux+=100;
 iaux-=90;
 gt.anydia=gt.dia_del_any+iaux*512; 
 recalculs_gt_bloqueig_torn();
}

unsigned short get_anydia_actual(void)
{
  return gt.anydia;
}

unsigned short  get_minut_del_dia(void)
{
  return gt.minut_del_dia;
}

/************************************************************/
/* test tarifa correcta                                     */
/************************************************************/

unsigned char tarifa_ok(void)
{
#if DEBUG_NO_CHECK
  buzzer_set(200);
 return(1);
#endif
 
 return(check_sum==calcul_check());
 
}

/************************************************************/
/* mira si pot pasar a ocupat per tecla                     */
/************************************************************/

unsigned char pas_lioc_permes(unsigned char tar)
{
 return(tarifa[tar].lioc);
}

/************************************************************/
/* mira si pot canviar de tarifa en ocupat per tecla        */
/************************************************************/

unsigned char pas_ococ_permes(unsigned char new_tar,unsigned char old_tar)
{
 return((tarifa[old_tar].ococ &(1L<<new_tar)) != 0L);
}

/************************************************************/
/* mira si pot pasar de ocupat a a pagar per tecla          */
/************************************************************/

unsigned char pas_ocap_permes(unsigned char tar)
{
 return(tarifa[tar].ocap);
}

/************************************************************/
/* mira si pot canviar de a pagar a ocupat per tecla        */
/************************************************************/

unsigned char pas_apoc_permes(unsigned char new_tar,unsigned char old_tar)
{
 //return(tarifa[old_tar].apoc & _lrol_(1L,new_tar));
// if (tarcom.tipus_teclat == 2 && old_tar > 8) return 1;	// Teclat rotatori, per passar a ocupat en tarifes > 8.
 return((tarifa[old_tar].apoc & (1L<<new_tar)) != 0);
}


__no_init unsigned long  tec_canvi;
__no_init unsigned char  amb_canvi;
__no_init unsigned int   tdia_canvi;
__no_init unsigned int   zonh_canvi;
__no_init unsigned char  se_canvi;


extern unsigned char  vel_canvi;




/*********************************************************************/
/*       TRACTAMENT CANVIS AUTOMATICS DE TARIFA                      */
/*********************************************************************/


/**************************************************************/
/* calcula  tipus dia per canvis aut.                 */
/**************************************************************/

unsigned int calcul_tdia(void)
{
int  i;
unsigned char  td;
 td = 0;
 for(i=0;tarcom.diaesp[i].tip && (i<NUM_DIAESP);i++)
   {
    if(tarcom.diaesp[i].anydia == gt.anydia)
    {
       td=tarcom.diaesp[i].tip & 0x0f;
       break;
    }
   }
 if(!td)  /* cas no es dia especial */
    td=tarcom.tipf_disem[dia_de_la_semana_from_time(&gt.time)];
 return (decod_16(td));
}


/*******************************************************************/
/* calcula  tipus temporada segons dia  per canvis autom. */
/*******************************************************************/
#define NUM_AMB (sizeof(tarcom.diaamb)/sizeof(tarcom.diaamb[0]))


unsigned char calcul_amb(void)
{
unsigned char  i;
 for(i=0;(tarcom.diaamb[i].tip>0) && (i<NUM_AMB);i++)
   {
    if((gt.anydia>=tarcom.diaamb[i].anydia_ini)&&(gt.anydia<=tarcom.diaamb[i].anydia_fin))
       break;
   }
 return (((unsigned char )1)<<tarcom.diaamb[i].tip);
}


unsigned int zona_del_dia(void)
{
unsigned char  i;
unsigned char  fin;
 fin=1;
 //for(i=0;i<num_zh;i++)
 for(i=0;;i++)    // sempre ha d'acabar amb 1440
   {
    if(gt.minut_del_dia <zonh[i])
       {
        fin=0;
        break;
       }
   }
 if(fin)
    i=0;   /* cas enllaca amb inici dia o no hi ha zones */
 return(decod_16(i));
}



void calcul_toptar_import(char tar,long import, struct s_serv* p_serv)
{
char  i;
 if(tarifa[tar].tiptop & TIPTOP_IMP)
   {
    for(i=0; import >= tope_imp[i]; i++);
    if(p_serv->per_tarifa[tar].ntope_imp!=i)
      {
       canvi_top=1;
       p_serv->per_tarifa[tar].ntope_imp = i;
      }
   }
}

void calcul_toptar_dist(char tar,unsigned int dist, struct s_serv* p_serv)
{
char  i;
 if(tarifa[tar].tiptop & TIPTOP_DIST)
   {
    for(i=0;((tope_dist[i]!=0xffffffff) && (dist>=tope_dist[i]));i++);
    if(p_serv->per_tarifa[tar].ntope_dist != i)
      {
       canvi_top=1;
       p_serv->per_tarifa[tar].ntope_dist=i;
      }
   }
}

void calcul_toptar_temps(char tar,unsigned short temps, struct s_serv* p_serv)
{
char  i;
 if(tarifa[tar].tiptop & TIPTOP_TEMPS)
   {
    for(i=0; temps >= tope_temps[i];i++);
    if(p_serv->per_tarifa[tar].ntope_temps != i)
      {
       canvi_top=1;
       p_serv->per_tarifa[tar].ntope_temps = i;
      }
   }
}

//********************************************************
// taula per decodificar a 16 bits
// 0-> 0x0001  15->0x8000
const unsigned int DECOD_16[16]={
0x0001,0x0002,0x0004,0x0008,0x0010,0x0020,0x0040,0x0080,
0x0100,0x0200,0x0400,0x0800,0x1000,0x2000,0x4000,0x8000,
};
      
unsigned char gestio_canvis(struct s_serv* p_serv)
{
uchar index,bits,tar;
 for(index=0;;index++)
    {
	 if(!(canvi_autom[index].teclat & tec_canvi))
	   continue;
	 if(p_serv->branca_canvi && canvi_autom[index].branca)
		 if( (canvi_autom[index].branca & p_serv->branca_canvi) == 0)
			 continue;
	 bits=canvi_autom[index].bits;
	 tar=canvi_autom[index].tarifa;   
	 if(bits&0x01)
	   if(!(canvi_autom[index].amb & amb_canvi))
	     continue;
	 if(bits&0x02)
	   if(!(canvi_autom[index].tdia & tdia_canvi))
	     continue;
	 if(bits&0x04)
	   if(!(canvi_autom[index].zonh & zonh_canvi))
	     continue;
	 if(bits&0x08)
	   {
		   if(tarifa[tar].tiptop & TIPTOP_IMP)
		   {
			   // cas tope parcial (per tarifa)
			   if(!(canvi_autom[index].import & DECOD_16[p_serv->per_tarifa[tar].ntope_imp]))
				   continue;
		   }
		   else
		   {
			   // cas tope global
			   if(!(canvi_autom[index].import & p_serv->imp_canvi))
				   continue;
		   }
	   }
	 if(bits&0x10)
	   {
	    if(tarifa[tar].tiptop & TIPTOP_DIST)
		  {
		   // cas tope parcial (per tarifa)
	       if(!(canvi_autom[index].dist & DECOD_16[p_serv->per_tarifa[tar].ntope_dist]))
	         continue;
		  }
		else
		  {
		   // cas tope global
                   if(!(canvi_autom[index].dist & p_serv->dist_canvi))
                         continue;
		  }
	   }
	 if(bits&0x20)
	   {
	    if(tarifa[tar].tiptop & TIPTOP_TEMPS)
		  {
		   // cas tope parcial (per tarifa)
	       if(!(canvi_autom[index].temps & DECOD_16[p_serv->per_tarifa[tar].ntope_temps]))
	         continue;
		  }
		else
		  {
		   // cas tope global
                   if(!(canvi_autom[index].temps & p_serv->temps_canvi))
                         continue;
		  }
	   }
	 if(bits&0x40)	 //senyal exterior
	   {
	   if(!(canvi_autom[index].sext & se_canvi))
	     continue;
	   }
         if(bits&0x80)	   // velocitat
           {
	   if(!(canvi_autom[index].velo & vel_canvi))
	     continue;
	   }
	 // cas trobada tarifa que compleix totes les condicions
	 break;                  
	}  
 return(tar);
}


unsigned char tarifa_canvi_autom(unsigned char tar,unsigned char MASK, struct s_serv* p_serv)
{
unsigned char  caux,tar0;
unsigned short  iaux;
unsigned char i;
unsigned long  laux;
 if(var_tarari)
   return(tar);
 if((tx30.estat==E_OCUPADO) || tarcom.canautpag)
    {
     if(MASK & AUT_TEC)
        {
         laux = 1L << tecla_tarifa;
         if(laux != tec_canvi)
           {
            tec_canvi = laux;
            canvi_top=1;
           }
        }
     if((MASK & AUT_ZONH) && ((tarcom.OP_CANZH_INI==0)||(MASK & AUT_INICIO)))
        {
         iaux=zona_del_dia();
         if(iaux!=zonh_canvi)
            {
             zonh_canvi=iaux;
             canvi_top=1;
            }
         if(gt.amb!=amb_canvi)
           {
            amb_canvi=gt.amb;
            canvi_top=1;
           }
         if(gt.tdia!=tdia_canvi)
           {
            tdia_canvi=gt.tdia;
            canvi_top=1;
           }
        }
     if(MASK & AUT_EXT)
        {
         caux = sext ? 1:2;
         if(caux!=se_canvi)
            {
             se_canvi=caux;
             canvi_top=1;
            }
        }
     if(MASK & AUT_VEL)
        {
         // arriva aqui amb vel_canvi ja actualitzat
         // i nomes si hi ha canvi (i a l'inici)
         canvi_top=1;
        }
     if(MASK & AUT_IMP)
        {
         for(i=0;p_serv->import >= tope_imp[i];i++);
         iaux=decod_16(i);
         if(iaux!=p_serv->imp_canvi)
            {
             p_serv->imp_canvi=iaux;
             canvi_top=1;
            }
        }
     if(MASK & AUT_DIST)
        {
         for(i=0;((tope_dist[i]!=0xffffffff) && (((unsigned int)p_serv->hm_oc)>=tope_dist[i]));i++);
         iaux=decod_16(i);
         if(iaux!=p_serv->dist_canvi)
            {
             p_serv->dist_canvi=iaux;
             canvi_top=1;
            }
        }
     if(MASK & AUT_TEMPS)
        {
         for(i=0;p_serv->minuts_canviaut>=tope_temps[i];i++);
         iaux=decod_16(i);
         if(iaux!=p_serv->temps_canvi)
            {
             p_serv->temps_canvi=iaux;
             canvi_top=1;
            }
        }
     if(canvi_top || (MASK & AUT_INICIO))
        {
         tar0=gestio_canvis(p_serv);   
         canvi_top=0;
         if(tar0==0xff) // cas no troba tarifa desti
              tar0=0;
         tar=tar0;
        }
    }
	if(tarifa[tar].branca)
		p_serv->branca_canvi = tarifa[tar].branca;
 return(tar);
}


void ltobcd(unsigned long lvar,char * buf)
{
char i;

 memset(buf,0,11);
 for(i=0;(i<10)&& lvar;i++)
   {
    buf[i]=lvar%10;
    lvar/=10;
   }
}

unsigned int sum_check(char *buf, unsigned int len)
{
unsigned int i,sum;
 for(i=0,sum=0;i<len;i++)
   sum+=(unsigned char)buf[i];
 return(sum);
}

unsigned char inc_bcd(unsigned char c)
{
 return(bin_bcd(bcd_bin(c)+1));
}


unsigned int decod_16(unsigned char c)
{
 return(0x0001<<c);
}


unsigned short get_int_odd(unsigned char * adr)
{
union u_var
{
  unsigned short i;
  unsigned char c[2];
} var;
  var.c[0]=*adr;
  var.c[1]=*(adr+1);
 return(var.i);
}

void set_int_odd(unsigned char *adr,unsigned short ivar)
{
union u_var
{
  unsigned short i;
  unsigned char c[2];
} var;
  var.i=ivar;
  *adr=var.c[0];
  *(adr+1)=var.c[1];
}


unsigned long get_long_odd(unsigned char * adr)
{
union u_var
{
  unsigned long l;
  unsigned char c[4];
} var;
  var.c[0]=*adr;
  var.c[1]=*(adr+1);
  var.c[2]=*(adr+2);
  var.c[3]=*(adr+3);
 return(var.l);
}


const char tab_1[16]={0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};

char num_bits_1(int v)
{
 return(tab_1[v&0x000f]+tab_1[(v>>4)&0x000f]);
}

unsigned char reloj_ok(int nChip)
{
unsigned char diaux;
unsigned char  stat;
struct s_time time;
 
 switch(DEF_CHIP_I2C[nChip].tipus)
   {
   case RELLOTGE_VELL:
     stat=get_confi_reloj(nChip);
     if ( (stat & 0xfc)!=0) 
        return(0);
     break;
   }
 
     get_reloj(nChip,&time);
     if (!es_bcd(time.s100)) 
        return(0);
     if(!es_bcd(time.sec) || (time.sec >0x59))
        return(0);
     if(!es_bcd(time.minute) || (time.minute>0x59))
        return(0);
     if(!es_bcd(time.hour) || (time.hour>0x23))
        return(0);
    if(!es_bcd(time.year))
        return(0);
     if(!es_bcd(time.month) || (time.month>0x12)|| (time.month==0))
        return(0);
     diaux=bin_bcd(dias_mes[time.month]);
     if((diaux==0x28) && ((bcd_bin(time.year) & 0x03)==0 ))
       diaux=0x29;
     if(!es_bcd(time.day) || (time.day==0) || (time.day>diaux))
        return(0);
		
    return(1);
}

#define FF_DDMM  0
#define FF_MMDD  1

__no_init unsigned char  FORMATO_FECHA;

unsigned char  t_dm[4]={3,4,0,1};
unsigned char  t_md[4]={0,1,3,4};


void confi_format_diames(uchar nf)
{
 FORMATO_FECHA=nf;
 if(nf==FF_DDMM)
    ndigdisp_ddmm=t_dm;
 else
    ndigdisp_ddmm=t_md;
}



unsigned char hay_transicion_tx30(void)
{
unsigned char i;
unsigned char retorn;
 retorn=0;
 for(i=trans_ini_TX30[tx30.estat];i<trans_ini_TX30[tx30.estat+1];i++)
    {
     if(event==taula_trans_TX30[i].event)
       {
        if(!(MASK_GEN & taula_trans_TX30[i].mask)) 
          {
           nsem=taula_trans_TX30[i].trans;
           tx30.new_estat = (enum eEstatsTx30)taula_trans_TX30[i].estat;
           retorn=1;
           break;
          }
        else
          {
           event=0;
           break;
          }
       }
    }
 return(retorn);
}

unsigned char llegir_sensors(void)
{
unsigned char sens;
unsigned short sens1;
  
    if(tipoTx==TX50)
    {
      IOPORT_5 = 0x0c;
    }
    else
    {
      write_i2c(IO_EXPANDER_TX40,0,2,"\x01\x00"); //IODIR
      write_i2c(IO_EXPANDER_TX40,0x13,1,"\x0c"); //GPIOB
    }  
      sens1 = IOPORT2_PD;
      sens =0;
      if((sens1 & 0x1000) == 0) sens |= SENSORS_CLAU_CON;
      if((sens1 & 0x2000) == 0) sens |= SENSORS_LLUM_POS;
      if((sens1 & 0x4000) == 0) sens |= SENSORS_PISON;
      if((sens1 & 0x8000) == 0) sens |= SENSORS_PASAJERO;
 return(sens);
}

void tartrab_a_buf(unsigned char *buf,long tartrab)
{
unsigned long mask_tt;
int no_repe,m,i,j,k;
int ndig = 11;
               
  mask_tt=0x00000001;
  memset(buf,' ',ndig);
  buf[ndig] = 0;
  for(i=0,k=ndig-11,j=0;(i<32) && (j<4);i++,mask_tt<<=1)
      if(mask_tt & tartrab)
      {
          for (no_repe=1,m=0; m<j; m++)
              if (memcmp((buf+ndig-11+(m*3)),tarifa[i].impre_tartrab,2)==0)
                  no_repe=0;
          if (no_repe)
          {
            memcpy(buf+k, tarifa[i].impre_tartrab, 2);
              k+=3;
              j++;
          }
      }  
}

void tartrab_a_buf_right(unsigned char *buf,long tartrab)
{
int ndig = 11;
  int i,j;
  tartrab_a_buf(buf,tartrab);
  for(i=ndig-1;i>=0;i--)
    if(buf[i] != ' ')
      break;
  if(i == (ndig-1))
    return;
  for(j=ndig-1;i>=0;j--,i--)
    buf[j] = buf[i];
  for(;j>=0;j--)
    buf[j] = ' ';
}






