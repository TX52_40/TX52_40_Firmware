#ifndef SHA256_H
#define SHA256_H

/*************************** HEADER FILES ***************************/
#include <stddef.h>

/****************************** MACROS ******************************/
#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest
#define HMAC_SHA256_BLOCK_SIZE 32  /* Same as SHA-256's output size. */


/**************************** DATA TYPES ****************************/
#define uint8_t unsigned char

typedef struct {
	unsigned char data[64];
	unsigned int datalen;
	unsigned long long bitlen;
	unsigned int state[8];
} SHA256_CTX;

/*********************** FUNCTION DECLARATIONS **********************/
//void sha256_init(SHA256_CTX *ctx);
//void sha256_update(SHA256_CTX *ctx, unsigned char data[], size_t len);
//void sha256_final(SHA256_CTX *ctx, unsigned char hash[]);
void internal_SHA256(unsigned char *data, unsigned int len, unsigned char *digest);
void hmac_sha256 (unsigned char out[HMAC_SHA256_BLOCK_SIZE],
             unsigned char *data, size_t data_len,
             unsigned char *key, size_t key_len);

#endif // SHA256_H