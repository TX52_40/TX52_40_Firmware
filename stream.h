// *************************************************************
// stream.h    definicions stream (memoria/carregador)
// *************************************************************

#ifndef _INC_STREAM

// tx50 #define STREAM_MCARD	0
#define STREAM_E2PROM		1
#define STREAM_RAM		2


#define ERR_UNIT_FNOTFOUND    10
#define ERR_UNIT_FULL         11
#define ERR_UNIT_DIRFULL      12
#define ERR_UNIT_CHAIN        13
#define ERR_UNIT_BADSECT      14




typedef struct s_direntry
{
	char name[12];
	char fehor[6];
	unsigned short sector;
	unsigned long fsize;
        char crypted;
        char dum0;
        unsigned short check;
	char dum[4];
}t_direntry;


typedef struct _iobuf
{
	char *_ptr;
	short   _cnt;
	char *_base;
	char  _flag;
	char  _file;
	char mode;
	short n_entry;
	t_direntry entry;
	long newsize;
	short transf_sect;
	short chain_sect;
	short chain_dest;
	char bufio[256];
}ifacFILE;

#define getc(fp)     (--(fp)->_cnt >= 0 \
 ? 0xff & *(fp)->_ptr++ : _filbuf(fp))

//#define putc(_c,fp)  (--(fp)->_cnt >= 0 \
// ? 0xff & (*(fp)->_ptr++=(char)(_c)) : _flsbuf((_c),(fp)))

extern int stream_open(int tipus,char* buf);
extern void stream_close(void);
extern int stream_num_bytes_used(void);

extern ifacFILE* IFACfopen(char* name,char mode, char force_crypted);
extern char *IFACfgets(char *buf,int n,ifacFILE* fp);
extern int IFACfread(char *buf, int n, ifacFILE* fp);
extern int IFACfclose(ifacFILE* fp);
//extern int _flsbuf(int ch,ifacFILE *arx);
//extern int format_paqram(int tipus_device);

extern void detecta_programa(void);
extern int detecta_configuracion(int resetea);
extern char verifica_cierre_pendiente(char avisa);
extern unsigned long detecta_pgmTC50(void);
extern int read_ext_mem(unsigned long adr, unsigned int len, char* dest);

#define _INC_STREAM
#endif
