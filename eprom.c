// ***************************************************************************
// eprom.c
// ***************************************************************************


#include "eprom.h"
#include "i2c.h"
#include "RTOS.h"


void leer_eprom(int nChip,unsigned int adr,unsigned int nb,char * buf)
{
 switch( DEF_CHIP_I2C[nChip].tipus)
   {
    case EPROM_256x8:    /* st2402  (256*8)  */
      wread_i2c(nChip,(unsigned char )adr,(unsigned char)nb,buf);
      break;
    case EPROM_DEFAULT:    
      wread_i2c_extnd(nChip,adr,nb,buf);
      break;
    case EPROM_ADDR3:  // x24c65  (8k*8)  
      wread_i2c_adr3(nChip,adr,nb,buf);
      break;
   }
}


void grabar_eprom(int nChip,unsigned int adr,unsigned int nb, char * buf)
{
 int time_out_eprom;
 unsigned char sub_adr;
 unsigned int nb_parcial;
 switch(DEF_CHIP_I2C[nChip].tipus)
   {
    case EPROM_256x8:    /* st2402  (256*8)  */
      for(sub_adr=(unsigned char)adr;
          nb>0;
          sub_adr++,nb--,buf++ )
        {
         write_i2c(nChip,sub_adr,1,buf);
         time_out_eprom=500;
         do
           {
             if(!status_i2c(nChip))break; /* espera final escritura */
             OS_Delay(1);
                time_out_eprom--;
           } while(time_out_eprom);
        }
      break;
    case EPROM_DEFAULT:   
      
      if (nb<(32-(adr%32))) nb_parcial=nb;
      else nb_parcial=(32-(adr%32));
      for(;nb>0;)
          {
           write_i2c_extnd(nChip,adr,nb_parcial,buf);
           time_out_eprom=500;
           do
             {
               if(!status_i2c(nChip))break; /* espera final escritura */
                OS_Delay(1);
                time_out_eprom--;
             } while(time_out_eprom);
             nb-=nb_parcial;
             buf+=nb_parcial;
             adr+=nb_parcial;
             if (nb<32) nb_parcial=nb;
             else nb_parcial=32;
          }
      break;
    case EPROM_ADDR3:  // x24c65  (8k*8)  
      write_i2c_adr3(nChip,adr,nb,buf);
     time_out_eprom=500;
     do
       {
         if(!status_i2c(nChip))break; /* espera final escritura */
          OS_Delay(1);
          time_out_eprom--;
       } while(time_out_eprom);
      break;
      
   }
}



