// ****************************************************
// 50prn.c
// ****************************************************

#include "RTOS.H"
#include "regs.h"
#include "format.h"
#include "aplic.h"
#include "tx30.h"
#include "tdgp.h"
#include "impre.h"

//*************************************************
// impressio d'un ticket  (sobre buffer)
//*************************************************

unsigned int x_impre(T_TICKET *tck, unsigned int maxlen, unsigned char num_pqt)
{   // if(tck==NULL) -> // Nom�s volem verificar estat impressora
	// num_pqt==0 � 1 -> paquete 1� y �nico
	T_VAR* pvarauxs;
	T_CAMP_TICKET * p_ct;
	int i,iaux1,reg_rep;
	unsigned int k;
	unsigned int k_fin=1;
	unsigned int k_ini=0;
	unsigned int taula=0;
	unsigned int pb=0;
	int local_es0 = 1;
	int global_es0 = 0;
	int antContinua = 0;
	char * bufimp;
	int initick;
	
	pvarauxs=(T_VAR*)OS_malloc(sizeof(T_VAR));
	do
	{
		for(k=k_ini; (pb<maxlen)&&(k!=k_fin); incrementa_taula(&k,taula))  // loop ticket *n
		{
			tabla_move_reg(taula,k,reg_rep,TAULA_LOAD); //load
			pb=0;
			if(tck!=NULL) 
				bufimp=(char*)OS_malloc(tck->nb); 
			if(tck!=NULL) 
			{
				initick=pb;
				for(i=0;(pb<maxlen)&&(i<tck->n_camps_ticket);i++)
				{
					p_ct=&tck->camp_ticket[i];
					if(p_ct->opc == OPCT_IMPR_32_24){
						if(tarcom.tipus_impressora != IMPREtip_Kas && tarcom.tipus_impressora != IMPREtip_Opt)
							continue;
						if(antContinua)
							continue;
					}
					if(global_es0)
					{
						// cas ha de saltarse tots els camos fins a trobar un final de global_es0
						if(p_ct->format == FORM_FIN_G_ES0)
							global_es0 = 0;
						antContinua=1;
						continue;
					}
					iaux1=p_ct->opc;
					if(iaux1==OPCT_ES0)
					{
						if(local_es0){
							antContinua=1;
							continue;
						}
					}
					else if(iaux1==OPCT_NO_ES0)
					{
						if(!local_es0){
							antContinua=1;
							continue;
						}
					}
					else if((iaux1!=OPCT_CERO) && (iaux1!=OPCT_IMPR_32_24) && (get_opc_tck(iaux1)==0) ){
						antContinua=1;
						continue;
					}
					antContinua=0;
					get_camp(&reg[p_ct->n_reg],p_ct->n_item,pvarauxs);
					pb+=formatear(p_ct->format,p_ct->nc_out,pvarauxs,bufimp+pb,&local_es0,&global_es0);
					if(pb>=maxlen)
					{
						if(num_pqt>1)
						{
							pb-=maxlen;
							if(pb)
								memmove(bufimp, bufimp+maxlen, pb);
							num_pqt--;
						}
						else
							pb=maxlen;
					}
					if((pb-initick) >= tck->nb) break;
				} 
			}
			if(pb)
				OS_Q_Put(&ColaImpre,bufimp,pb);
			OS_free(bufimp);
		}   // loop ticket *n
		if((tck==NULL) || (tck->n_reg_continuacio==0) || (pb>=maxlen))
			break;
		//llegeix ticket seguent
		tabla_move_reg(TABLA_CONTINUAR_TICKET,tck->n_reg_continuacio-1,REG_CONTINUAR_TICKET,TAULA_LOAD); //load   
		tck=ticket[reg_continuar_ticket->num_ticket];
		taula=reg_continuar_ticket->taula_repeticio;
		if(taula)
		{
			reg_rep=reg_continuar_ticket->reg_repeticio;
			get_camp(&reg[reg_continuar_ticket->reg_index],reg_continuar_ticket->camp_fin_index,pvarauxs);
			k_fin=pvarauxs->var.i;
			get_camp(&reg[reg_continuar_ticket->reg_index],reg_continuar_ticket->camp_ini_index,pvarauxs);
			k_ini=pvarauxs->var.i;
		}
		else
		{
			k_ini=0;
			k_fin=1;
		}
	}while(1);  
	OS_free(pvarauxs);
	return(pb);
}
