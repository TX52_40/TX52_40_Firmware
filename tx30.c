// ******************************************************
// tx30.c    programa gesti� tx30
// ******************************************************


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "intrinsics.h"

#include "tx30.h"
#include "tautrans.h"
#include "i2c.h"
#include "hard.h"
#include "display.h"
#include "RTOS.h"
#include "iostr710.h"
#include "metros.h"
#include "tdgp.h"
#include "serial.h"
#include "teclat.h"
#include "rut30.h"
#include "lumin.h"
#include "bloqueig_torn.h"
#include "ccab.h"
#include "impre.h"
#include "td30.h"
#include "zapper.h"
#include "multiplex.h"
#include "debug.h"
#include "pausa.h"
#include "car30.h"
#include "totsEq.h"
#include "crc32.h"

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

const unsigned short   VERSION_TARIFA = {0x0008};   //   0x08   
const unsigned char    VERSION_TX[2] = {0x32,0x03};  // versio 3.31

/*



VERSION_TX 1.00    (VERSION_TARIFA 0x01)


Funcio confi_format_diames() que es cridava a final de CARGADOR() s'ha passat
        a final de cargar_tarifa();
instala_k() verifica valors escrits a pic i reintenta fins a 3 vegades.Tamb�
            es crida al donar tensi�. (El pic no guarda valors a flash)
              
Pendent:

 Verificar el sistema de descontar grabaciones.

VERSION_TX 1.02    (VERSION_TARIFA 0x01)

A stream.c arreglat calcul stream.num_bytes_used . Faltaba +(j/127)*2
 tx30.c i tx30.h canviat tope_temps de int a t_tope_temps ( unsigned short)
A lumin.c canviades taules lluminos fetes igual a TC60
A reloj.c arreglat canvi +- 1hora
A car30.c afegit delay a hay_cargador
A car30.c afegit detecti error carregant arxius tickets


VERSION_TX 1.03    (VERSION_TARIFA 0x01)  28/01/2009
Arreglat que no treia be la visualitzacio de la versio taximetre (Parametre L6)
    A TX30.c  canviat VERSION_TX a char[2] en lloc de short.
    A sem30.c assignar VERSION_TX a reg_tcks_tx30->VERSION_TX com a char

VERSION_TX 1.03 D    (VERSION_TARIFA 0x02)  30/04/2009

Afegits camps texte per 6 linies cap�alera i 6 de peu per a tiquets.
Canviada VERSION_TARIFA a 0x02

En el programa de tarifas versiones_valides pasa a 0x03 (El programa nou no 
funcionara amb tarifa vella, pero el programa vell pot funcionar amb tarifa nova)

Afegit poguer entrar per carregador si hi ha impressora o no
Afegit suplements secuencials s acumulen tots a suplement 0


A lumin.c a funcio out_luzex_no_save() afegit treure llum de lliure.
(per activar cable verd)

Afegit totalitzador TOT_NTICK per numero tickets servei impressos
Afegit camp num_tick a registre SERVEI per imprimir num_tick en lloc de num_servei

VERSION_TX 1.03E    (VERSION_TARIFA 0x04)  3/6/2009

A lumin.c a funcio out_luzex_no_save() afegit treure llum de lliure.
(per activar cable verd)

Afegit totalitzador TOT_NTICK per numero tickets servei impressos
Afegit camp num_tick a registre SERVEI per imprimir num_tick en lloc de num_servei

VERSION_TX 1.03F    (VERSION_TARIFA 0x04)  23/7/2009

A lumin.c a funcio out_luzex_no_save() eliminat el segon bloc de sortides paralel
( la primera sortida era la correcta)

VERSION_TX 1.03G    (VERSION_TARIFA 0x04)  21/10/2009

La opcio tarcom.OP_bloc5Km s' ha modificat
      bit 0 = 1  bloqueja pas ocupat a A PAGAR
      bit 1 = 1  bloqueja pas de Lliure a Ocupat
        
Arreglat a envios a TD30 s'enviaba malament les dates d' inici i final de servei, per
que el TX50 les guarda en binari i el TX30 les guardaba en bcd.
Afegit tipus transmissio ITT_BIN2B que espera el valors en binari.

A rut30.c arreglat format ITT_MINS

Arreglat a Car30.c llegir configuracio (registres i tickets) cas E_MEM_MAL 

VERSION_TX 1.04   (VERSION_TARIFA 0x08)  6/11/2009

A sem30.c a funcio paso_a_libre()  recuperat || !tarifa_ok() que havia desaparegut 
com a comentari per DEBUG 

Afegits passwords 7 i 8

Afegida gestio ticket prima

Arreglat tema de sortida llums cas serie.A tarifa afegides 4 combinacions per cas serie 
Arreglat que a les crides per sortida llum frontal ( out_luzfront) calia complementar sext_luz
Arreglat que les 4 combinacions de llums en ocupat/a pagar estaven al reves que les de estats

Sustituit treure tensio per passar CPU a modo STOP. 
  Reiniciar cnt_sec_treure_tensio cas arribin impulsos de metre.

Arreglat a reloj.c que get_reloj mati bit 8 de minute i sec
      
A car30.c al entrar directament la k, si no s'entra res, mante valor actual de la k.


VERSION_TX 1.04A   (VERSION_TARIFA 0x08) 10/12/2009
A sem30.c  funcio MACR_SUPL substituit tarifa[ns].tec_supl  per tarifa[servei.tarifa].tec_supl
(No funcionaba be mascara de tecles supl permeses per tarifa.)

Canviat iva de char a float ( a tarcom.valors_iva a reg_servei->percent_iva i a reg_prima_torn->percent_iva0/1/2

A regs.c afegit tipus camp VAR_FLOAT  (sustitueix a VAR_DUM)


VERSION_TX 1.05   (VERSION_TARIFA 0x08) 20/11/2009
A sem30.c  funcio MACR_SUPL substituit tarifa[ns].tec_supl  per tarifa[servei.tarifa].tec_supl
(No funcionaba be mascara de tecles supl permeses per tarifa.)

Aquesta versio ha quedat integrada a la 1.04A

VERSION_TX 1.06   (VERSION_TARIFA 0x08) 16/07/2010

A partir de versio VERSION_TX 1.04A

A tx30.c canviat #define MS_LOOP_TX30 de 50 a 10. (No donava temps a processar tots els metres)
Guardada com a Odometre ( en realitat nomes es conta metres)



VERSION_TX 1.07   (VERSION_TARIFA 0x08) 15/09/2010

A partir de versio 1.04A

Incorpora la versio 1.06 ( poder funcionar com a contametres)
 
Afegit que es pugui entrar el numero de tiquet per carregador al final de l' entrada
de textes impressora ( Tecla 5 )

Afegit que actualitzi (i visualitzi) el numero de gravacions a la eeprom del carregador.

VERSION_TX 1.08  (1.04B) (VERSION_TARIFA 0x08) 25/11/2010

A impre.c eliminat TAPA_ABIERTA. (Aquest sensor ja no hi es).
A car30.c arreglat tarifa precanvi
  nueva_k s'ha fet __no_init
  reset precambio_pendiente abans de configs_finales
Arreglat fallo temps torn
  A gestio_bloqueo_torn_0() calcul minuts_ini i minuts_fin inclou bcd_bin()

VERSION_TX 1.09  (1.04C) (VERSION_TARIFA 0x08) 10/12/2010

Arreglat fallos a control temps torn:
    Al passar a off es fei reset de la franquicia de distancia.Ara nomes es fa al rearmar torn
    En cas de pas a off amb reset tensio s'ha d'acumular el temps de off + fallo tensio per
    veure si hi ha d'haver penalitzacio.
      
VERSION_TX 1.10  (1.04D) (VERSION_TARIFA 0x08) 22/12/2010

A control temps torn el numero d'impulsos per considerar que s'ha mogut passa a 500      


VERSION_TX 1.11   (1.04E) (VERSION_TARIFA 0x08) 5/05/2011

A impre.c eliminat TAPA_ABIERTA. (Aquest sensor ja no hi es).Tot i que a la versio 1.08
ja es deia que s'havia eliminat, encara hi era.

Afegit que es puguin entrar lineas de texte per els tiquets desde la tarifa. Fins ara nom�s
es podien entrar per carregador.
Aquestes linies inclouen les que ja existien  ( es guardaven a blqs i blqs_ext ) i 10 linies 
m�s que es guarden a blqs_ext.
Per cada una d'aquestes 10 linies hi ha un short que correspon al led d' estat quan s'entren
per carregador ( tecla 5).
Nomes es podran entrar (per carregador) les linies que tinguin led d'estat diferent de 0.

Afegides opcions tarifa hi_ha_conf_llums_test_display i llums_test_display per treure aquesta
configuracio de llums mentre es fa test display.
Tambe s'ha allargat durada del test display (cas OP_test_old_new == 0) per que doni temps a veure
l'estat de les llums exteriors.

Torn de treball:
  Afegida opcio tarifa control_horari_cap_setmana cas =1 no es controla
  temps de treball en el cap de setmana.
  Afegida opcio tarifa sanciona_descans_curt cas = 1 si el temps en off es
  inferior al de descans es desconta tot el temps de descans.
  Afegida opcio tarifa sanciona_fallo_tensio = 1 idem a l'anterior per cas fallo tensio. 
  Modificat que es descompta tot el temps que estigui sense tensi�. (Si esta en
  baix consum es considera com si fos OFF).

Afegit parametre tarifa NUM_supl_max.Valor entre 0 i 3 que indica el numero de
suplement menys 1 sobre el que s'acumulen tots el suplements de numero superior 
(si n'hi ha). Valor per defecte 3.

Afegit parametre tarifa pot_apagar_lluminos. Cas =1 fa que amb la combinacio de
  tecles 7 i 8 es pugui apagar el lluminos en lliure.Per el digit de la dreta del
  led d'estat, apareix el punt decimal mentres esta apagat. Es torna a encendre 
  al passar a Ocupat o per nova pulsacio.
    
VERSION_TX 1.20   (VERSION_TARIFA 0x08) 26/05/2011
   
Afegit control horari amb dos torns
A sem30.c a paso_a_pagar()  eliminat que la diferencia fins a corsa minima s'acumuli 
a l'import per tarifa.
Afegida opcio tarcom.sanciona_moviment per descomptar temps en OFF cas hi hagi moviment 


Modificat que compti el temps que tarda en passar a ON desde OFF com temps que ja esta treballant.
S'ha canviat el nom de tarcom.minuts_penal_torn  per tarcom.minuts_pas_off_a_lliure.Nomes s'ha
mantingut com a texte del XML de tarifa ( per no haver de canviar el programa de tarifes).



VERSION_TX 1.04F   (VERSION_TARIFA 0x08)  6/06/2011

 A partir de versio tx50_110526_1_20
   
 Eliminat del control de torn tot el referent a dos conductors.
   
VERSION_TX 1.04G   (VERSION_TARIFA 0x08)  27/01/2012

 A partir de versio tx50_110606_1_04F
   
 A tx30.c arreglat transmissio tarifes treballades
 A tx30.c funcio cambio_tarifa() arreglat que actualitzi servei.no_es_primer_canvi
   per que recalculi fraccio de salt al canviar de tarifa
 A bloqueig_torn.h METRES_MOGUT_EN_OFF pasat de 500 a 2000
 A car30.c a recalcul_params_tar() afegit que si es velocitat frontera, posi acumuladors junts

VERSION_TX 1.50     ( 1.04X_FRANCE)   (VERSION_TARIFA 0x08)  6/03/2012

 A partir de versio tx50_120127_1_04G


Inclou ZAPPER
A lumin.c funcio test_luzex() modificat que en el for(i=... es faci el test 
  amb llums existents + llum  --->  out_luzex_no_save((mask_luzex[0] | i),.....
A sem30.c funcio fallos_hard afegit 
  display(1,NF_TEXT6,(void*)&TEXT_ERROR[retorn-1][0]);
  cas hi ha error.
A sem30.c funcio FS_0() eliminat if(tarcom.bloq_libre_impre)  (no servia per res)    
A car30.c funcio EJECUTAR_PRECAMBIO() l'opcio d'impressora nomes es copia de la que 
  hi ha a tarcom si es igual a 1.
A struc30.h estructura s_gt eliminat canvi_dia
A tx30.c gt.canvi_fecha, gt.canvi_minut i gt.canvi_minut_ocupat es posan a 0 despres
  de fer-los servir.
A tx30.c a loop principal programa fer que en cas E_MEM_MAL nom�s miri 
  teclat per entrada carregador i deixi de fer resta del loop.
A tautrans.c eliminades transicions totalitzadors i codis secrets en estat E_MEM_MAL.
A tx30.c fet que si s'ha aplicat corsa minima ja no incrementi mes salts.
  (opcio tarcom.corsa_min_deixa_de_comptar).
    
// **************************************************************
// **************************************************************
// **************************************************************    

VERSION_TX 2.00        (VERSION_TARIFA 0x08)  30/04/2012

   A partir versio 1.50

    *************************************************   
    **      Versio que incorpora TX40              **
    *************************************************    
A cua.c passades totes les funcions a memoria interna (FAST)

A sem30.c funcio  TEST() visualitzacio temps ultim servei passada a HH.MM (format NF_HHMM)
A lumin.c arreglat lluminos serie i paral.lel



VERSION_TX 2.02        (VERSION_TARIFA 0x08)  2/05/2012

   A partir versio 2.00
     
A td30.c funcio td30_respondre() arreglat check-sum     
Totalitzadors TOT_TON i TOT_TOC passen a acumular minuts en lloc de hores.
      Ara tots els totalitzadors de temps estan en minuts excepte TOT_TONHHMM
        que es guarda com hores*100+minuts.
      Tots els totalitzadors es visualitzen com HHHHHH.MM
      En les transmissions s'envien en minuts, excepte TOT_TDESC i TOT_TONHHMM
        que s'envien com hores*100+minuts

VERSION_TX 2.04        (VERSION_TARIFA 0x08)  17/05/2012

   A partir versio 2.02
     
Eliminat tarcom.hay_impressora i sustituit per tarcom.tipus_impressora (no esta a tarifa
    i s'entra per teclat carregador.
Eliminat tarcom.newtransm i sustituit per tarcom.hi_ha_td30 (no esta a tarifa
    i s'entra per teclat carregador.


VERSION_TX 2.05        (VERSION_TARIFA 0x08)  1/06/2012

   A partir versio 2.04
     
A lumin.c arreglat lluminos paralel TX40
A display.c arreglat lluminositat display per teclat ( cas TX40)


VERSION_TX 2.10        (VERSION_TARIFA 0x08)  1/06/2012

   A partir versio 2.05

A tx30.c arreglat acumuli salt per tarifa a tots el viatgers actius
    servei.per_tarifa[ii][servei.tarifa].import+=vsal;
                                                             
Incorpora protocol multiplexat Lluminos / TD30 / Impressora / Bluetooth
  A car30.c afegit triar lluminos TL70.
A lumin.c arreglat test lluminos paralel cas Fran�a TX40
    

VERSION_TX 2.xx        (VERSION_TARIFA 0x08)  1/11/2012

   A partir versio 2.10
     
A lumin.c
    A funcio out_luzex() eliminat que es recarregui timer_envio_serie (feia que si es perdia 
    primer enviament al passar a On tardes 10 segons a encendres lluminos)
    A funcio test_luzex() canviat teps loop ( i partit en dos )
      Per lluminos tipus 3 (France) afegit opcio LLUMINOS_TEST_AMB_ALIMENTACIO per tal que al fer el test
      incorpori llum 0x20 (alimentacio) per evitar que el rele canti.
    A funcio luminoso_envio_serie() canviat luminoso.cnt_refr de 0 a CNT_REFR_FAST 
A impre.c
    Afegit tamanys font 'E' 'F' 'G' i 'H'  (mides grans)
A rut30.c
    Funcio pas_ococ_permes() arreglada per cas num. tarifa > 16
      
  10/01/2013
  No compta be distancies per K superiors a 2560
  Factor pmpk0 es un char i per aixo no admet valors superiors a 256 impulsos per hm
  A struc30.h
    tarcom.pmpk0 
          .pmpk0_ccab passats a int
    servei.pk_lib_serv
          .pk_oc_serv
          .pk_lib_pasj  passats a int
  A tx30.c i tx30.h
    pk_tot_tot
    pk_tot_tot_ccab
    pk_oc_tot  
    pk_lpasj_tot   passats a int
  A car30.c
    A funcio recalcul_params_tar() assigna tarcom.pmpk0 i tarcom.pmpk0_ccab com int
      
  22/01/2013
  A multiplex.c 
    A funcio multiplex_retrieve_missatge() fet que tambe detecti seq�encia 0x1c 'S'
    com pregunta status.
      
  18/02/2013
  A lumin.c
    A funcio init_llums_IO_EXPANDER() afegit reset sortides abans configurar port com sortides.

  18/03/2013
  A tx30.c
    A funcio tx30_interrupt_timer1ms() nomes s'actualitza luz front si es TX50
  A hard.c
    A funcio hard_init() canviar mode P1.8 a input pulldown (test fallo tensio)
      
   2/04/2013 
  A sem30.c arreglat que apagui back-light al passar a Off
  A multiplex.c encriptar transmissions PER_TXM
  A multiplex.c afegit PER_TXM_ESTAT per que BT40 pugui preguntar estat tx sense encriptar  

  4/04/2013  
  Recuperat parametre de tarifa  tarcom.newtransm. Ara indica ( si es 1) que per carregador  
  es pot entrar opcio td30. Si no es 1 es desactiva opcio td30.
    
  9/04/2013
  Encripta transmissio impressora

VERSION_TX 2.12        (VERSION_TARIFA 0x08)  3/04/2013

  Arreglat que si es treu tensio mentre visualitza totalitzadors/rellotge en off, al tornar tensio
    es salta temporitzat de OFF a Lliure.(S�han passat variables estat_retorn_reltots i estat_retorn_cods
    a dins l�estructura tx30 per tal que siguin __no_init. Tambe s' ha passat variable num_cods a __no_init) .
  Afegides diverses crides a REFRESH_ON() per evitar que en la intermitencia de OFF a LLiure el 
    display restes apagat.    
  A display.c funcio fotoDiode_interrupt_timer() arreglat calcul temperatura placa.
  A lumin.c arreglat test llums. Afegida nova funcio out_luzex_verif() per que no dupliques llum de
    lliure cas TX40.
  A car30.c afegit que si es tria lluminos TL70 i terminal BT40, utilitza lluminos com a saludes serie.

  A impre.c funcio impre_init() eliminat fer reset impressora externa.(Feia avan� paper al tornar de off) 
    

  11/10/2013    2.12C
  A multiplex.c funcio multiplex_retrieve_missatge()  fet multiplex.nc_rep = 0 al rebre DLE
    (abans ho feia al rebre STX i llavors fallava cas hagues rebassat max caracters)
      
  A multiplex.c canviat MAX_CHARS_TO_EXTRACT de 10 a 30 (per que buidi mes rapid la
    cua mpx)
  A impre.c canviat IMPR_TIMEOUT_RESP_STATUS de 40 a 200ms
  Amb els dos canvis anteriors s'ha arreglat que tx40 amb BT40 dones error E10 quan 
    s'imprimia tiquet desde SmartTd
  A td30.c afegit a pregunta K00 que contesti valor sensors i metres totals.
    (items_preg_estado)
  A td30.c afegit transmissio 'L' per apagar encendre Lluminos
  
  21/10/2013  2.12D
  A car30.c funcio recalcul_params_tar()  afegit factor yardas a calcul fs.

  28/11/2013  2.12E

 - Arregla control torn que el dia 1 de gener deixava treballar sense limit.
	 A bloqtorn.c sustituit ++bloqueo_torn.anydia_rearmar per funcio next_anydia()

       25/06/2014   2.12F
         A partir 2.12E
         A display.c funcio refresh_backlight() passat nomes a 4 nivells
           de lluminositat per evitar pampallugues
           
  07/03/2014  2.14
    
  A bloqueig_torn.c afegit numero de descanssos permesos.
  A bloqueig_torn.c funcio OFF_LIBRE_bloqueig_torn() fet timer50=0  cas bloqueo_torn.stdesblq == 1
    arregla que si s'interrompia el pas de OFF a ON (cas tarcom.minuts_pas_off_a_lliure) es reactivava sol.
  A tautrans. afegit nou estat E_PREPROG per preprogramar events torn cas France
  Afegit un password per activar preprog events.
    Modificat a struc30.h tarcom.haypassw de char a short.
    Modificat a struc30.h NUM_CODS a 9  
    Modificat a car30.c "haypassw" de XML_CHAR a XML_SHORT
      
  A regs.c funcio leer_ticket_paqram() afegit que possi a NULL tots els tiquets abans de llegirlos.
  Recupera gestio 1/2 torns (cas Fran�a sempre 2 torns)
  
  Afegit tiquet no legal a impressio procedent de smarttd. Nomes si tarcom.tck_no_legal = 1
    
  Modificat visualitzacio parametres sota password, que no necessiti password.
    Cal que no s'hagi programat password i que tarcom.haypass tingui el bit "visualitzaci� par�metres sota password" a 1. 

  Modificat que al pasar de A Pagar a Lliure cas tipaglib = 2 mentre estigui en intermitent ja estigui en estat FI servei
    S�ha canviat la mascara MSK_PAGFS per MSK_FSLIB
    s�ha afegit la transicio FS_LIB_TEMPS
      
  Afegit control horari que es pugui desactivar per una temporada (tarcom.dates_desactivacio_torn)    
  2.14A  
  Modificat gestio corsa minima.El parametre tarcom.corsa_min_aplica_pas_fiserv ha canviat de significat:
      = 0  aplica corsa minima al pasar a A PAGAR
      = 1  (cas France) es visualitza corsa minima al aplicar suma, pero no s'aplica fins el pas a fi servei

  2.14B   29/04/2014
    A tdgp.h a registre reg_tcks_tx30 afegit camp minuts_maxim_descans per sortida ticket torns. 
    A sem30.c a funcio imprimir_ticket_events_torn() copiar valor tarcom.minuts_maxim_descans a registre reg_tcks_tx30 
      
  2.14C  11/06/2014
    A tdgp.h i sem30.c eliminades modificacions 2.14B
    A display.c funcio refresh_backlight() passat nomes a 4 nivells de lluminositat per evitar pampallugues
    
      
  2.16   12/05/2014     

    A tdgp.h afegits formats FORM_MON_LZ i FORM_MON_AUX_LZ
    A format.c implementats formats anteriors.( Posen el texte moneda i moneda auxiliar a l' esquerra del import)
    
    A RTOSINIT_STR71x.c a funcio __low_level_init() canviat BCON3_bit.C_LENGTH = 10;
                                              
    La visualitzacio temps que queda / hora final torn s'ha passat de Lliure a a visualitzacio rellotge
    Fran�a:
      El temps maxim de descans es per el total de descanssos, no per cada u
      Si s'acaba el temps de torn estant en descans, passa automaticament a tancar el torn
      Al tiquet torn, el camp no. equip s' imprimeix correctament
      Modificat doble tecla per cas codis secrets
      Al tancat torn quan ja esta en E-7 ara mante la intermitencia fins el moment d' apagar
     
    Afegit configuracio BT40 a traves canal lluminos i impressora IR32 externa.
      A car30.c afegida opcio bt40_L
      A td30.c a funcio td30_init() afegida opcio bt40_L
        
 
        
        
    2.16A   24/07/2014   
      
      A sem30.c a funcio ESP_ON() canviat (a dos llocs) timer50 = 20;  (abans era 17 i feia el temps d'espera un 20% mes curt )

    2.16B   07/10/2014  
      Afegit parametre tarcom.new_leds_ho_km per leds horaria / km cas xile
      A bloqueig_torn.c a funcio rearmar_bloqueo_torn() afegit parametre passar_a_off
        serveix per cas es crida desde tornada de tensio, per que posi estat OFF per tal de obligar a
        fer el temporitzat de pas de Off a lliure (cas treuen tensio en lliure i es produeix rearmar torn
        per haver passat de les 6 del mati.
      A sem30.c arreglat visualitzacio led estat cas visu rellotge
        

    2.16C   20/11/2014  

      A tx36.c i display.c modificat refresc display per evitar pampallugues. El refresc de display es fa amb Output compare B de TIMER1
        Tambe s'ha tingut de modificar el zumbador que ara es fa nomes amb compare A del timer 1 (teclat.c)
      A sem30.c funcio ON_OFF() afegit que en el cas de Fran�a nomes es pugui anular el temporitzat de 
      pas a Off ( o tancar torn) durant els primers segons.
          El numero de segons va al nou parametre tarcom.segs_anular_pas_off. Si per tarifa hi ha el valor 
          0xff ( valor per defecte cas no hi hagi aquest parametre)  aquesta opcio queda desactivada
      El mateix per cas pas a ocupat mentre esta temporitzant per passar a Off ( o tancar torn) sem30.c funcio LIBRE_OCUP()   
        
      A tarifa afegit tarcom.luminTipoFixe per seleccionar tipus lluminos per carregador / tarifa
      A car30.c afegit un tipus mes lluminos cas Argelia
        
      A tx50.icf ( per linker) passat els stacks de  interrupt IRQ i FAST_IRQ a RAM_INTERNA
      A display.c passat el stack Stack_display a RAM_INTERNA  
                                                                        
    2.16D   4/12/2014 
      
      A tx36.c
        A funcio task_tx_interno() modificat que s'ha d'installar interrupcio per buzzer tambe en cas de TX52
        A funcio INT_BUZZER_DISPLAY() fer que la part de refresc display nomes es faci cas TX40
          
    2.16E   13/01/2015 
      
      A sem36.c
        A funcio FS_LIB_TEMPS() modificat que en cas tarcom.hi_ha_td30 no pasi a lliure (amb independencia de 
        de que hi hagi MSK_PASOCFS o no)
      A bloqueig_torn.c a funcio next_anydia() arreglat calcul ( feia que dia 31/12 no descomptes temps torn)    
          

    3.00  1/04/2015
       A partir versio 2.16E
       Eliminat tot el referent a minuts ocupat i lliure en marcha (CNT_MARCHA, tarcom.marcha0, minuts_lib_marcha, ....
       Implementat correctament tot el tema de multitax�metre
       Afegit tipus tarifacio 3  (tarcom.tipt_vf = 3)
         Correspon a velocitat frontera fixa 
           Fins ara el tipus 2 corresponia a vel frontrera fixa pero NO estava implementat malgrat que algunes 
           tarifes l' utilitzaven (Barcelona
           per exemple) per la tarifa de carretera sense retorn. Dins la funcio recalcul_params_tar() cridada
           al carregar tarifa, es sustituia aquest tipus per tipus 1 (velocitat frontera).(Es pot veure amb el 
           comentari ANTONIO XAPU BCN CARRETERA).
           Fins i tot si aquesta sustitucio no es produia, en el moment d'enviar el modus al tarifador (mitjan�ant
           p_serv->NEW_MODO_VF ) el modus 2 (vf fixa) es sustituia per 1 (vf).
          
    3.02  xx/04/2015
      A partir versio 3.00
      Afegir peticio pin desde bt40.
        Verifica hi hagi carregador conectat
        Per display apareix Pin i amb les tecles 1-4 entrar el pin i validar amb tecla rombe
        La tecla 0 cancel.la l'entrada del pin

    3.03 xx/05/2015
      A partir versio 3.02

            Incorpora modificacions versio     2.16F   26/05/2015
            
                Modificacio per intentar solucionar el problema dels equips que perden hores de treball a Madrid
                Detectat que no es reiniciava el torn degut a que la variable bloqueo_torn.anydia_rearmar tenia un valor 
                incorrecte (molts dies mes gran) que impedia que es rearmes el torn.
                Solucio:
                   A rutina get_reloj() es verifica que els valors llegits siguin tots digits 0 a 9
                   Tambe es verifica que desde la lectura anterior no hagin passat mes de 60 segons
                   En cas d'error es repeteix la lectura fins a tres vegades
                     
                   A mes a la funcio hay_que_rearmar() es comprova que el dia de rearmar no estigui mes enlla de anydia +1 
                   i si es aixi es canvia per anydia actual

            Incorpora modificacions versio     2.16G   


                Modificacio prima:
                    Canviat tarcom.hi_ha_prima per tarcom.guardar_dades_prima
                    Afegit parametre tarifa tarcom.hi_ha_torn_prima i tarcom.hi_ha_torn_prima_Fixe  
                Transmissio pregunta d'estat :
                    Els dos primers items de la transmissio 
                        ITT_ESTAT,2,&tx30.estat,
                        ITT_ASC7,1,&tx30.estat_aux,
                    s'han sustituit per un de sol
                        ITT_ESTAT,3,&data_dum,
                    Els dos primers bytes corresponen a l'estat basic (LLiure, ocupat, off etc) de manera que els auxiliars com rellotge,
                    visu totalitzadors etc, s'han substituit per el estat a que es retorna al acabar la visulitzacio ( lliure, off, torn tancat etc)
                    El tercer byte correspon a l'estat real ( amb el bit 7 possat a 1)  
                    S' ha eliminat el camp  tx30.estat_aux i  EAUX_CARG/EAUX_TOTSVD i EAUX_DATTAR que s'hi guardaven
          
                    Tambe s'ha arreglat que l'estat E_CERRADO no s'envi� fins que s'hagi confirmat el tancament
                Transmissio pregunta torn :
                    A items_preg_turno[] s'han afegit els camps seguents
                        ITT_TOTALIZ_LONG,8,(void*)TOT_NSER   ,
                        ITT_TOTALIZ_LONG,8,(void*)TOT_IMPC   ,
                        ITT_TOTALIZ_LONG,8,(void*)TOT_IMPS   ,
                        ITT_BCD2B,10,&gt.time.minute,
                        ITT_LONG,4,&tx30.num_conductor,
        
                Transmissio pregunta servei :
                Transmissio pregunta ultim servei :
                    A items_servicio[] i items_lastserv[] s'han afegit els camps seguents
                        ITT_PERCENT,4,&servei.percent_iva,
                        ITT_LONG,6,&servei.import_iva,

      Arreglat que en cas multiviatger si feia tiquet, despres en els seguents viatgers ja no passava a lliure al cap del temps
          (s'ha passat parametre servcom.envio_copia a serv[].envio_copia)

    3.04 20/01/2016
      A partir versio 3.03

      Incorpora modificacions versio     2.16G   
            Al posar versio 2.16F a Madrid veuen que cas de tenir smarttd al acabar el servei no pasa a lliure despres de
            la intermitencia (cal tornar a polsar tecla lliure).
            Soluci�:
              A sem30.c a funcio FS_LIB_TEMPS() modificat que al acabar temporitzat:
                si no hi ha transmissio passa a lliure
                cas td30 no pasa a lliure
                cas altres transmissions passa a lliure a menys que hi hagi bloqueig de pas a lliure
      
      La versio 2.16G ha passat a ser la 2.16H ( i ja estan incorporades a la 3.03)

      A multiplex afegit periferic PER_TRANSFER

      Enviament tarifa a traves de bt40 ( periferic PER_TRANSFER) amb codis 'P' i 'E'

      A td30.c a taula items_preg_hora[] afegit 2 caracters amb segons
      Afegit que guardi 30 canvis de tarifa a e2prom a posicio EEPT_CANVIS_TAR_POS
        es llegeig sobre estructura blqs_modifs_tar
      
      A multiplex.c afegida transmissio ( periferic PER_TRANSFER) codi 'e' per rebre estatus GPS i GSM
      Els valors rebuts es visualitzen a display extres al visualitzar rellotge.

      Afegides transmissions per validacio tiquet insika.  ( periferic PER_TRANSFER) i codis 'i' i 't'

      Afegida transmissio per validar conductor i password.
      periferic PER_TXM
        enviament :Dccccpppp     conductor/ password
        resposta  :Dsnn...n      s  status '0' / '1'  not ok / ok
                                 n...n      24 bytes amb nom conductor

    3.05 14/04/2016
      A partir versio 3.04

	  Arreglat que cas de multiviatger, si passava a lliure per temps ( cas no s.imprimia tiquet) tancava 
	  tots els passatgers. A sem30.c a transicio FS_LIB_TEMPS() sustituit paso_a_libre() per paso_a_libreXseguent_viaj()

	  Ampliat el numero de fechas de +- una hora de 6 a 20 (10 anys)
	  
	  Canviat nom estructura torn_prima_bg40 per configs_desde_bg40

	  Afegit opcio seleccio del tipus IVA desde estat A PAGAR. Aquesta opcio s.envia desde bg40
	  i es guarda a configs_desde_bg40.select_iva_teclat

	  Afegides opcions hi ha pausa activa i minuts pausa passiva. Aquestes opcions s.envien desde bg40
	  i es guarden a configs_desde_bg40.hi_ha_pausa_activa i configs_desde_bg40.segons_pausa_passiva

  	  A td30.c afegit a pregunta K00 que contesti si esta en Pausa ('0'/'1' no / si en PAUSE).
    	(items_preg_estado)

	  Afegits totalitzadors import baixades de bandera TOT_BB i TOT_BB_P
	  A registre reg_tcks_tx30 ( a traves de base de dades i tdgp.h) s'han afegit els camps cTOT_BB i cTOT_BB_P per poderlos imprimir als tickets
		de totalitzadors.

	  A transmissions per canal multiplex, sustituit camp 'numero sequencia' per 'device' que inicia la transmissio.
		Les transmissions generades desde el TX50/42 corresponen al device DEVICE_TX5240. Les respostes a
		transmissions de pregunta porten el device que ha fet la pregunta.
	  
	  A car30.c arreglat que si es fa alta_taximetre, es guardi a blqs la versio, data i check del programa actual
		( ara es resetejava a zero el numero de canvis de programa )

	  A sem30.c a funcio FS_LIBRE() fet que precindeixi de bloqueig MSK_PASOCFS si hi ha carregador
      Idem a funcio fin_servicio()

	  Modificacio control hores treballades (per Mallorca):
		Afegit parametre tarcom.no_rearmar_automaticament.
		Si esta activat quan arriba a l'hora de reiniciar torn no ho fa sino que queda amb E-10 (encara que li quedessin minuts).
		Les hores que pot treballar nomes es reinicien al passar de OFF a Lliure.

    3.05A 2/11/2016
	  A sem30.c a funcio inicialitzar_servei() canviades les inicialitzacions de ntope_imp ,dist i temps per tarifa de 0xff a 0

    3.06 5/04/2017
	  Eliminada suma en ocupat
	  A multiplex.c a periferic PER_TRANSFER afegida transmissio 'c' per pregunta i resposta temps que queda de torn.
	  Afegir una nova condicio (branca) als canvis automatics
	  A sem30.c a funcio inicialitzar_servei() eliminat el reset que es feia de topes per tarifa de tots el viatgers.
	  Arreglat branca per canvi tarifa
	  Arreglat multitaximetre canvis automatics (el canvi per velocitat el fa per tots els viatgers)
	  A tx30.c a funcio task_tx_interno() a // mira si esta tarifant per canvi minut ocupat
	  afegit que miri estat E_FISERV cas mes viatgers
    
	3.08 11/05/2017
	  Afegits totalitzadors Equador
	  Textes error per tarifa

	3.10 12/05/2017
	  A td30.c afegida transmissio 'T' per borrat totalitzadors parcials
	  Arreglat textes error
	  A calcul_pcsup() afegit que el calcul del supl percentual inclogui la baixada de bandera
	  A pas_apoc_permes si teclat rotatori i tarifa > 8 returna 1. Nomes tenim 8 bits a la mascara
	  Opcions ticket de servei Copia per el conductor i copia per al client
	  Afegir Impresora Kashino
	  Afegir formats 3DEC, 4DEC, 5DEC i 6DEC
	  Textes peu, capsalera i nom del ticket de 32 caracters
	  Afegir opcio per impresora Kashino per posar camps al ticketper ajustar len linies

	  Afegir opcio teset de display automatic al passar a on
	  Afegir nou parametre "no_canvis_primer_salt" par no fer els canvis automatics dins del primer salt.
	  Afegir a les visualitzacions i les impresions de parametres el tipus de velocitat frontera.
	  Coordenades de inici i final de servei al tiquet.
	  Afegir peticions de demanar permis per actualitzar tafifa i programa
	  Format moneda arreglar numero de digits
	  Posar opcions als totalitzadors periodics Ecuador
	  Eliminar el tipus de generador d'impulsos P2P
	  Aumentar en numero de dies festius a 128
	  Comprimir la tarifa
	  Arreglat tarifes comprimides tags vuits
	  Arreglar reset vervei. Posar tota la estructura a 0
	  Si detecta E-8 esborra totes les estructures tarcom, servei, servcom.
	  Rutina askCoordenadas verifica si hi ha transmisi� encriptada avan� d'enviar
	  Arreglar velocitat frontera fixe
	  Afegir opcio suplement percentual de bb
	  Afegir opcio suplement percentual per tecla / tarifa

	3.20 30/08/2018
	  Implementar en el teclat les tecles de tarifa 9 a 12
	  Activar temporizado para anular paso a off aunque no sea control horario de Francia
	  Acumular franquicia de dist�ncia.
	  Periode diari sense control horari.
	  Arregla password de off a on

	3.20B 06/11/2018
	  Numero de ticket de 6 digits
	  Logo per Equador Mastercom
	  Arreglar password policia Paris desde tarifa

	3.22C 29/04/2019
	  Tiempo hasta final de turno en el tichet de control de turno interno no sal�a bien

	3.22D 29/04/2019
	  Actualizar el valor del checksum de la tarifa al gravarla en lugar de al leerla

	3.22E 08/06/2019
	  Arreglar campos tickets ecuador

	3.22F 13/01/2020
	  Anyadir suplementos desglosados 4 a 8 en la transmision
	  Anyadir suplementos borrados en la transmision

	3.23 27/01/2020
	  Compatibilizar las dos impresoras Mastercom
	  Implementar operativa precio acordado
*/

#define MS_LOOP_TX30       10     // ms per task tx30. Canviat de 50 a 10 a versio 1.06 ( Contametres)  


// variable per intercanvi informacio entre boot , loader i programa

   struct s_boot
   {
     long program_loaded;
     char dum[12];
   };
 
#define  __BOOT_EXCHANGE _Pragma("location=\"__BOOT_EXCHANGE\"")
 
__BOOT_EXCHANGE struct s_boot boot;


 
//  VARIABLES GENERALES


__no_init t_tx30 tx30;
__no_init struct s_pausa pausa;

__no_init struct s_tarcom   tarcom;
__no_init struct s_configs_desde_bg40   configs_desde_bg40;
__no_init struct s_tarifa   tarifa[32];
__no_init struct s_bloc_tar bloc_tar[32];
__no_init unsigned short zonh[17];
__no_init unsigned long  tope_imp[17];
__no_init unsigned int   tope_dist[17];
__no_init t_tope_temps   tope_temps[17];
__no_init t_canvi_autom canvi_autom[32*8];   // 8 condicions per tarifa


__no_init struct s_servei   servei;
__no_init struct s_serv     serv[MAX_PSGRS];
__no_init struct s_servcom  servcom;

__no_init char canvi_top;

__no_init unsigned short  MASK_GEN;


__no_init char ledtar_interm;
__no_init char cnt_ledtar_interm;
__no_init char buf_ledtar_interm[4];


   unsigned char  nsem;
   unsigned char ROBO;
   
__no_init    unsigned char  tecla_num;
__no_init    unsigned char  tecla_tarifa;

__no_init    unsigned char pasj;
   unsigned char sext;
   unsigned char sext_luz;

   unsigned char  event;
   unsigned char  event_tr;
   unsigned char  pseudoevent;


                          
//  VARIABLES TARIFACION   


__no_init unsigned char exces_vel;
                                

__no_init struct s_param_carg  param_carg;




const struct s_tabla_crida_eprom  tabla_crida_eprom[11]=
{
EPROM_BASE,       EEPT_BLQS_POS,                                EEPT_BLQS_LEN,                  (char *)&blqs,                    //GE_BLQS
EPROM_CONF_CARG,  offsetof(struct s_param_carg,n_grabs_carg),   sizeof param_carg.n_grabs_carg, (char *)&param_carg.n_grabs_carg, //GE_n_grabs_carg
EPROM_BASE,       EEPT_CODS_POS,                                EEPT_CODS_LEN,                  (char *)&cods_tax,                //GE_CODS
EPROM_BASE,       EEPT_FP_POS,                                  EEPT_FP_LEN,                    (char *)&fecha_paro,              //GE_FP
EPROM_BASE,       EEPT_BLQS_POS+offsetof(struct s_blqs,nsalts), sizeof blqs.nsalts,             (char *)&blqs.nsalts,             //GE_BLQS_nsalts
EPROM_BASE,       EEPT_BLQS_POS+offsetof(struct s_blqs,n_descs),sizeof blqs.n_descs,            (char *)&blqs.n_descs,            //GE_BLQS_desc
EPROM_CONF_CARG,  0,                                            sizeof(struct s_param_carg),    (char *)&param_carg,              //GE_param_carg
EPROM_BASE,       EEPT_BLQS_EXT_POS,                            EEPT_BLQS_EXT_LEN,              (char *)&blqs_ext,                //GE_BLQS_EXT
EPROM_BASE,       EEPT_CODS_EXT_POS,                            EEPT_CODS_EXT_LEN,              (char *)&cods_tax_ext,            //GE_CODS_EXT
EPROM_BASE,       EEPT_CANVIS_TAR_POS,                          EEPT_CANVIS_TAR_LEN,            (char *)&blqs_modifs_tar,         //GE_BLQS_MODIFS_TAR
EPROM_BASE,       EEPT_BLQS_EXT32_POS,                          EEPT_BLQS_EXT32_LEN,            (char *)&blqs_ext_32,             //GE_BLQS_EXT_32
};


extern char bdisp[7];

const struct s_visu visu_parms[]=
{
//  C1-CE 
        0xb9,0x06, (unsigned long*)&blqs.k,           NF_K3D,     // C.1
        0x39,0xdc, (unsigned long*)&visuk,            NF_K_FLASH, // C o.
        0xb9,0x5b, (unsigned char*)&blqs.nt,          NF_NT,      // C.2
        0xb9,0x4f, (unsigned char*)&blqs.f_grab,      NF_AMD,     // C.3
        0xb9,0x66, (unsigned char*)&blqs.CCC,         NF_NT,	  // C.4
        0xb9,0x6d, (unsigned short*)&visu_chk,        NF_CHK,	  // C.5
        0xb9,0x7d, (unsigned short*)&check_tiquet,    NF_CHK,	  // C.6    
        0xb9,0x07, (unsigned char*)&visu_f_precambio, NF_AMD,	  // C.7
        0xb9,0x7f, (unsigned short*)&visu_chk_prec,   NF_CHK,	  // C.8        
        0xb9,0x6f, (unsigned char*)&blqs.nmodifs_k,   NF_K_T,	  // C.9
        0xb9,0x77, (unsigned char*)&blqs.nmodifs_tar, NF_K_T,	  // C.A
        0xb9,0x7c, (unsigned short*)&check_programa,  NF_CHK,	  // C.b  
        // els tres seguents nomes surten si tarcom.bloqueo_torn != 0
        0xb9,0x39, (unsigned long*)&tarcom.visu_minuts_max_torn_1x2,         NF_MINUTS_TO_HH_MM, // C.C
        0xb9,0x5e, (unsigned long*)&tarcom.visu_minuts_max_torn_sabdom_1x2,  NF_MINUTS_TO_HH_MM, // C.d
	0xb9,0xf9, (unsigned long*)&bloqueo_torn.minuts_queden,     NF_MINUTS_TO_HH_MM, // C.E

//  L1-L6  
        0xb8,0x06,(unsigned char*)&blqs.n_lic,     NF_LIC,    // L.1
        0xb8,0x5b,(unsigned char*)&blqs.n_serie,   NF_LIC,    // L.2
        0xb8,0x4f,(unsigned char*)&blqs.f_exp,     NF_AMD,    // L.3
        0xb8,0x66,(unsigned char*)&blqs.f_inst,    NF_AMD,    // L.4
        0xb8,0x6d,(unsigned char*)&blqs.CCC_inst,  NF_NT,     // L.5
        0xb8,0x7d,(unsigned char*)&VERSION_TX,     NF_CRON2,  // L.6
//  r1-r8  
        0xd0,0x06,(unsigned char*)&blqs.n_progt,   NF_NT,     // r.1
        0xd0,0x5b,(unsigned char*)&blqs.n_grabs,   NF_K_T,    // r.2
        0xd0,0x4f,(unsigned char*)&blqs.n_descs,   NF_K_T,    // r.3
        0xd0,0x66,(unsigned char*)&blqs.n_reps,    NF_K_T,    // r.4
        0xd0,0x6d,(unsigned char*)&blqs.r5,        NF_NT,     // r.5
        0xd0,0x7d,(unsigned char*)&blqs.r6,        NF_LIC,    // r.6
        0xd0,0x07,(unsigned char*)&blqs.f_fab,     NF_AMD,    // r.7
        0xd0,0x7f,(unsigned char*)&blqs.SS,        NF_DDDSS   // r.8
};

const struct s_visu  visu_parms_sin_passw[4]=
{

        0x39,0x6d, (unsigned int*)&visu_chk,          NF_CHK,   // C5
        0x39,0x07, (unsigned char*)&visu_f_precambio, NF_AMD,   // C7
        0x39,0x7f, (unsigned short*)&visu_chk_prec,   NF_CHK,   // C8
        0x39,0x7c, (unsigned short*)&check_programa,   NF_CHK,	// Cb	   
};

const struct s_parms_tar visu_parms_tar[9]=
{
 0,0x86,(unsigned char*)tarifa[0].ledocup,NF_LED_DER,           // 1.
 0,0xfc,(unsigned char*)&tarifa[0].bb,NF_IMPORT,                // b.
 1,0xb7,(unsigned long*)&bloc_tar[0].m_ps,NF_FL_1,              // M.
 1,0xed,(unsigned long*)&bloc_tar[0].seg_ps,NF_FL_1,            // S.
 1,0xde,(unsigned long*)&bloc_tar[0].impkm_ss,NF_FL_D,          // d.
 1,0xf6,(unsigned long*)&bloc_tar[0].impho_ss,NF_FL_D,          // H.
 2,0xf3,(unsigned char*)tarifa[0].ledocup,NF_LED_DER,           // P.
 0,0xf9,(unsigned long*)&tarifa[0].supl,NF_IMPORT,              // E.
 3,0xF8,(unsigned char*)&bdisp,		    NF_TEXT6,				// t.
};

const unsigned char   texte_apagat[4]={0,0,0,0};


__no_init void  *p_import;            // assignat dins de servei
	      void  *p_texte_apagat     = (void*) &texte_apagat[0];
__no_init void  *p_hm_oc;             // =&servei.hm_oc; / =&parcial_dist[ntar];
          void  *p_servei_impk_visu = &servcom.impk_visu;
          void  *p_servei_imph_visu = &servcom.imph_visu;
__no_init void  *p_crono;             // =&servei.crono; / =&parcial_crono[ntar];
          void  *p_vel              = &tx30.vel;
          void  *p_texte_moneda     = &tarcom.TEXT_EURO[0];

const struct s_visu_vd visu_disp1[]=
{
 &p_import          ,NF_IMPORT,
 &p_hm_oc           ,NF_D_T,
 &p_servei_impk_visu,NF_FL_D,
 &p_servei_imph_visu,NF_FL_D,
 &p_crono           ,NF_HHMMSS_INT,
 &p_vel             ,NF_K,
 &p_crono           ,NF_HHHH_DISP1,
 &p_texte_moneda    ,NF_TEXT6,
};

const struct s_visu_vd  visu_disp2[]=
{
 &p_texte_apagat    ,NF_TEXT4,
 &p_hm_oc           ,NF_DIST2,
 &p_servei_impk_visu,NF_FL4_D,
 &p_servei_imph_visu,NF_FL4_D,
 &p_crono           ,NF_HMS,
 &p_vel             ,NF_VEL,
 &p_crono           ,NF_HHHH_DISP2,
 &p_texte_moneda    ,NF_TEXT4,
};

// posicio 0 es desembre (gener-1)
// serveix tant per mes 1-12  com 1-0x12
const unsigned char dias_mes[19]={31,31,28,31,30,31,30,31,31,30,31,30,31,00,00,00,31,30,31};



const float FKDEC[4]={1.,10.,100.,1000.};

const struct s_formato formatos[60]={
F_SS_JI  ,   0,0x00 | 0x40 | 2,  NULL,   //   NF_LEDEST  
F_LONG   ,   1,0x80 | 0x40 | 6,  NULL,   //   NF_IMPORT  
F_LONG   ,   1,0x80 | 0x40 | 4,  NULL,   //   NF_SUPL    
F_LONG   ,   0,0x80 | 0x40 | 6,  NULL,   //   NF_K       
F_SS_JD_C,   0,0x00 | 0x40 | 5,  NULL,   //   NF_TEXT5   
F_SS_JD_C,   0,0x00 | 0x20 | 6,  NULL,   //   NF_TEXT6_FL
F_HORA   ,   0,0x00 | 0x00 | 0,  NULL,   //   NF_HORA    
F_DIA    ,   0,0x00 | 0x00 | 0,  NULL,   //   NF_DIA     
F_ANY    ,   0,0x00 | 0x00 | 0,  NULL,   //   NF_ANY     
F_SS_JI  ,   0,0x00 | 0x20 | 2,  NULL,   //   NF_LEDEST_FLASH 
F_SS_JI  ,   0,0x00 | 0x40 | 4,  NULL,   //   NF_TEXT4 
F_LONG   ,   2,0x00 | 0x40 | 5,  NULL,   //   NF_D_T    distancia per totalits   
F_LONG   ,   1,0x00 | 0x40 | 5,  NULL,   //   NF_I_T    import per totalits      
F_LONG   ,   0,0x00 | 0x40 | 5,  NULL,   //   NF_K_T    escalar per totalits     
F_SS_JI  ,   0,0x00 | 0x40 | 2,  NULL,   //   NF_TEXT2 
F_BCD    ,0x8f,0x00 | 0x40 | 4,  NULL,   //   NF_CHK   
F_LONG   ,   4,0x80 | 0x40 | 6,  NULL,   //   NF_K3D     
F_BCD    ,0x81,0x00 | 0x40 | 4,  NULL,   //   NF_NT     
F_BCD    ,0x95,0x00 | 0x40 | 6,  NULL,   //   NF_AMD   
F_LONG   ,   2,0x00 | 0x40 | 4,  NULL,   //   NF_DIST2  
F_BCD    ,   3,0x00 | 0x40 | 4,  NULL,   //   NF_CRON2  
F_LONG   ,   0,0x80 | 0x40 | 4,  NULL,   //   NF_VEL  
F_LONG   ,   0,0x80 | 0x20 | 6,  NULL,   //   NF_K_FLASH     
F_SS_JD_C,   0,0x00 | 0x40 | 2,  NULL,   //   NF_LED_DER   
F_BCD    ,   0,0x00 | 0x40 | 5,  NULL,   //   NF_BCD5  
F_BCD    ,   0,0x00 | 0x40 | 4,  NULL,   //   NF_BCD4  
F_BCD    ,   0,0x00 | 0x40 | 2,  NULL,   //   NF_BCD2  
F_FLOAT  ,   2,0x80 | 0x40 | 6,  NULL,   //   NF_FL_1  float 1 decs. 
F_FLOAT  ,   1,0x80 | 0x40 | 6,  NULL,   //   NF_FL_D  float NDEC decs. 
F_FLOAT  ,   1,0x80 | 0x40 | 4,  NULL,   //   NF_FL4_D  
F_HMS    ,   3,0x00 | 0x20 | 4,  "\xff\x7f\xff\xff",        //   NF_HMS   
F_HHMMSS ,0x94,0x00 | 0x20 | 5,  "\x7f\xff\x7f\xff\xff",    //   NF_HHMMSS_INT   
F_SS_JD_C,   0,0x00 | 0x20 | 6,  "\x50\x50\x50\x50\x50\x50",    //   NF_TEXT_TR   
F_BCD    ,0x81,0x00 | 0x40 | 5,  NULL,   //   NF_LIC    
F_BCD    ,   3,0x00 | 0x40 | 5,  NULL,   //   NF_DDDSS  
F_LONG_NC,   0,0x00 | 0x40 | 1,  NULL,   //   NF_TOT6   
F_LONG_NC,   0,0x00 | 0x40 | 3,  NULL,   //   NF_TOT8   
F_SS_JD_C,   0,0x00 | 0x40 | 5,  NULL,   //   NF_LEDTOT 
F_SS_JD_C,   0,0x00 | 0x20 | 2,  NULL,   //   NF_LED_DER_FLASH   
F_LONG_NC,   0,0x00 | 0x40 | 2,  NULL,   //   NF_TOT2   
F_LONG_NC,   0,0x00 | 0x40 | 4,  NULL,   //   NF_TOT4   
F_LONG,      3,0x80 | 0x40 | 6,  NULL,   //   NF_HH_MM  
F_HORA   ,   0,0x00 | 0x20 | 0,  NULL,   //   NF_HORA_FLASH    
F_BCD    ,0x91,0x00 | 0x20 | 4,  "\x7f\xff\xff\xff\x7f",    //   NF_AAAA   
F_BCD    ,   3,0x00 | 0x20 | 4,  "\xff\x7f\xff\xff",     //   NF_HHMM  
F_SS_JD_C,   0,0x00 | 0x40 | 6,  NULL,                   //   NF_TEXT6   
F_HHHH   ,   3,0x00 | 0x20 | 5,  "\xff\xff\x7f\xff\xff", //   NF_HHHH_DISP1   
F_HHHH   ,   3,0x00 | 0x20 | 4,  "\xff\x7f\xff\xff",     //   NF_HHHH_DISP2   
F_LONG   ,   8,0x80 | 0x40 | 6,  NULL,   //  NF_IMPORT_ALT  (per euros)
F_LONG   ,   8,0x80 | 0x40 | 4,  NULL,   //  NF_SUPL_ALT    (per euros)
F_SS_JI  ,   0,0x00 | 0x40 | 2,  NULL,   //   NF_LEDTAR_INTERM  
F_LONG_60,   3,0x80 | 0x40 | 6,  NULL,   //   NF_MINUTS_TO_HH_MM  
F_LONG   ,   1,0x00 | 0x40 | 4,  NULL,   //   NF_SUPL_NOBLK    
F_LONG   ,   3,0x80 | 0x40 | 4,  NULL,   //   NF_VERSION    
F_LONG   ,   3,0x00 | 0x40 | 6,  NULL,   //   NF_K2D     
F_BCD    ,   0,0x00 | 0x40 | 6,  NULL,   //   NF_BCD6  
F_LONG_60,   3,0x80 | 0x10 | 6,  "\xff\xff\xff\x7f\xff\xff",   //   NF_MINUTS_TO_HH_MM_PUNT_INTERM  
F_BCD    ,   3,0x00 | 0x40 | 4,  NULL,     //   NF_HHMM_FIXE  
F_SS_JD_C,   0,0x00 | 0x00 | 6,  NULL,                   //   NF_TEXT6_FLASH_NOMOD
F_MODIF_IVA, 0,0x00 | 0x40 | 4,  NULL,                   //   NF_MODIF_IVA
};

//  fin  defv30.c      variables y constants tx30                     
// *******************************************************************



unsigned char METRO_TAR;    // per comunicar amb interrup. distancia per tarifador

 

//fin variables Test Conector
 unsigned char mantenersalidaimpulso; //mantener salida de impulsos para cada salto de importe taximetro


#if HOMOLOG
unsigned char  SAVLUZ;
unsigned char  timer_homolog;
#endif


__no_init  unsigned char  vel_canvi;
__no_init  unsigned char  vel_canvi_new;
__no_init  unsigned int  cnt_canvivel;
__no_init  char  iniciat_canvivel;
  
   
   int  timer50;
   int  timer100;
   int  timer_2;
   
   
__no_init   char  exces_fallo_t;
__no_init   unsigned long  visuk;
__no_init   unsigned short  visu_chk;
__no_init   unsigned short  check_tiquet;
__no_init   unsigned short  visu_chk_prec;
__no_init   char visu_f_precambio[3];
__no_init   unsigned short check_programa;
__no_init   unsigned char  sensors;
__no_init   char  var_tarari;

                                   
__no_init   int  pk_tot_tot;
__no_init	int  pk_lib_tot;
__no_init   int  pk_tot_tot_ccab;
__no_init   int  pk_oc_tot;
__no_init	int  pk_off_tot;
__no_init   int  pk_lpasj_tot;
__no_init   unsigned char  habia_error_genimp;
__no_init   char  contar_salts_inst;



__no_init     struct s_gt  gt;
   unsigned long  last_supl;    //  per parlant
   
   struct s_canal_serie  canal_serie;

__no_init unsigned char *  ndigdisp_ddmm;
__no_init unsigned int  check_sum;

struct s_cods_tax       cods_tax;
struct s_cods_tax_ext   cods_tax_ext;

struct s_blqs   blqs;
struct s_blqs_ext   blqs_ext;
struct s_blqs_ext_32	blqs_ext_32;
struct s_blqs_modifs_tar blqs_modifs_tar;


/* VARIABLES TECLADO   */
struct s_teclat teclat;


__no_init unsigned char  fecha_paro[3];   // dia,mes,any 
__no_init unsigned long  long_fp;


__no_init unsigned char  MASK_1;

__no_init unsigned long  * p_supl;

__no_init char  viaj;
__no_init unsigned short  ledocpag[2];   // [2] per cas led_tarifa_flash


__no_init char  delay_impre;
 
 
// *************************************************
// registres
// *************************************************
__no_init T_DREG_CONTINUAR_TICKET*    reg_continuar_ticket;
__no_init T_DREG_TCKS_TX30*	      reg_tcks_tx30;
__no_init T_DREG_MONEDA*              reg_moneda;
__no_init T_DREG_SERVEI*              reg_servei;
__no_init T_DREG_PRIMA_TORN*          reg_prima_torn;
__no_init T_DREG_PRIMA_TORN*          reg_prima_torn_aux;
__no_init T_DREG_PRIMA_SERVEI*        reg_prima_servei;
__no_init T_DREG_PRIMA_SERVEI*        reg_prima_servei_aux;

//**************************************************
//  INTERRUPCION DISTANCIA
//**************************************************

CODE_FAST void INTD_C(void) 
{
	int ii;
	struct s_serv* p_serv;
	if(tx30.estat==E_MEM_MAL) return;
	COLA_MF++;
	velocimetro_INTD();
	METRO_ZAPPER = 1;
	for(ii=0; ii<MAX_PSGRS; ii++)
	{
		if(servcom.viaj_activo[ii])
		{
			p_serv = &serv[ii];
			if(p_serv->TARIFANDO && (!p_serv->CAMBIAR_DE_TARIFA) && p_serv->CUENTA_K &&
			   (((p_serv->VALUE_VF==0) && p_serv->MODO_VF != 3) || (velocidad > p_serv->VALUE_VF)))
			{
				p_serv->acum_dist_control++;
				if(p_serv->PRIMSAL_K)
				{
					p_serv->ACC_K += p_serv->FS_K_ps;
					if(p_serv->ACC_K>=p_serv->KK_ps)
					{
						p_serv->ACC_K-=p_serv->KK_ps;
						if(p_serv->TARIFA_POR_DISTANCIA_ps)
							p_serv->NS_K++;
						if(p_serv->MODO_VF == 1)
						{
							p_serv->ACC_H=0;
							// APAGAR led horaria
							LED_HORARIA_OFF;
						}
						if(p_serv->NS_K>=p_serv->FACTOR_N_ps)
						{
							// cas produit salt per distancia
							p_serv->NS_K=0;
							p_serv->PRIMSAL_K=0;
							p_serv->SALT_PER_DIST=1;
							if(p_serv->MODO_VF == 3)
							{
								p_serv->PRIMSAL_H=0;
								p_serv->ACC_K=0;
								p_serv->ACC_H=0;
								p_serv->NS_H=0;                     
							}
							else if(!p_serv->TIPT_ACS)
							{
								p_serv->PRIMSAL_H=0;
							}
						}
					}
				}
				else
				{
					p_serv->ACC_K+=p_serv->FS_K_ss;
					if(p_serv->ACC_K>=p_serv->KK_ss)
					{
						p_serv->ACC_K-=p_serv->KK_ss;
						if(p_serv->TARIFA_POR_DISTANCIA_ss)
							p_serv->NS_K++;
						if(p_serv->MODO_VF == 1)
						{
							p_serv->ACC_H=0;
							// APAGAR led horaria
							LED_HORARIA_OFF ;
						}
						if(p_serv->NS_K>=p_serv->FACTOR_N_ss)
						{
							// cas produit salt per distancia
							p_serv->NS_K=0;
							p_serv->SALT_PER_DIST=1;
						}
					}
				}
			}
		}
	}
	
}



/***************************************************/
/*    INTERRUPCION DE TIEMPO (10 Hz)               */
/***************************************************/



CODE_FAST static void INT10HZ_C(void)    
{
	int ii;
	struct s_serv* p_serv;
	
	// nomes tarifa temps en OCUPAT
	// aixi en MULTITAX no comtara per cap viatger fins quee acabi proces A PAGAR / FI SERVEI
	if( tx30.estat != E_OCUPADO) 
		return;
	
	for(ii=0; ii<MAX_PSGRS; ii++)
	{
		if(servcom.viaj_activo[ii])
		{
			p_serv = &serv[ii];
			if(p_serv->TARIFANDO && (!p_serv->CAMBIAR_DE_TARIFA) && p_serv->CUENTA_H &&
			   (((p_serv->VALUE_VF==0) && p_serv->MODO_VF != 3) || (velocidad <= p_serv->VALUE_VF)))
			{
				p_serv->acum_temps_control++;
				if(p_serv->PRIMSAL_H)
				{	
					p_serv->ACC_H+=p_serv->FS_H_ps;
					if(p_serv->ACC_H>=p_serv->KH_ps)
					{
						p_serv->ACC_H-=p_serv->KH_ps;
						if(p_serv->TIPT_ACS)
						{
							p_serv->NS_H++;
							if(p_serv->NS_H>=p_serv->FACTOR_N_ps)
							{
								// cas produit salt per temps
								p_serv->NS_H=0;
								p_serv->PRIMSAL_H=0;
								p_serv->SALT_PER_TEMPS=1;
								if(p_serv->MODO_VF == 3)
								{
									// cas Xile
									p_serv->PRIMSAL_K=0;
									p_serv->ACC_K=0;
									p_serv->ACC_H=0;
									p_serv->NS_K=0;
								}
							}
						}
						else
						{
							p_serv->NS_K++;
							if(p_serv->MODO_VF) // el cas == 3 ja esta descartat per que va amb acum. separats
							{
								p_serv->ACC_K=0;
								// encender led horaria
								LED_HORARIA_ON;
							}
							if(p_serv->NS_K>=p_serv->FACTOR_N_ps)
							{
								// cas produit salt (temps i dist. junts)
								p_serv->NS_K-=p_serv->FACTOR_N_ps;
								p_serv->PRIMSAL_H=0;
								p_serv->PRIMSAL_K=0;
								p_serv->SALT_PER_DIST=1;
							}
						}
					}
				} 
				else
				{
					// salts successius 
					p_serv->ACC_H += p_serv->FS_H_ss;
					if(p_serv->ACC_H >= p_serv->KH_ss)
					{
						p_serv->ACC_H-=p_serv->KH_ss;
						if(p_serv->TIPT_ACS)
						{
							p_serv->NS_H++;
							if(p_serv->NS_H>=p_serv->FACTOR_N_ss)
							{
								// cas produit salt per temps
								p_serv->NS_H=0;
								p_serv->SALT_PER_TEMPS=1;
							}
						}
						else
						{
							p_serv->NS_K++;
							if(p_serv->MODO_VF)  // el cas == 3 ja esta descartat per que va amb acum. separats
							{
								p_serv->ACC_K=0;
								// encender led horaria
								LED_HORARIA_ON;
							}
							if(p_serv->NS_K>=p_serv->FACTOR_N_ss)
							{
								// cas produit salt (temps i dist. junts)
								p_serv->NS_K-=p_serv->FACTOR_N_ss;
								p_serv->SALT_PER_DIST=1;
							}
						}
					}
				} 
				
			}
		}
	}
	
}
///////////////////////////////////////////////////////////////////////////

//*******************************************************
// interrupcion per display i buzzer   (cas TX40)
//*******************************************************

extern char buzzerHalf;
#define _OS_TIMER1_PRIO 0x01

CODE_FAST static void INT_BUZZER_DISPLAY(void)
{
 // canvia sortides per buzzer i intensitat display
  
  if(TIM1_SR & 0x4000)
  {
    // cas buzzer  ( OCFA)
    TIM1_SR = 0xb800; // reset OCFA
    if(TIM1_CR2_bit.OCAIE)
    {
        TIM1_OCAR = TIM1_OCAR + 0x0D00;
        if(buzzerHalf)
        {
          buzzerHalf = 0;
          TIM1_CR1_bit.OLVLA = 0;
        }
        else
        {
          buzzerHalf = 1;
          TIM1_CR1_bit.OLVLA = 1;
        } 
    }
  }
  if(tipoTx == TX40)
  {
    if(TIM1_SR & 0x800)
    {
     // cas display (OCFB)
      TIM1_SR = 0xf000; // reset OCFB
      refresh_backlight();
    }  
  }
}

//*******************************************************
// interrupcion comun a tiempo y distancia
//*******************************************************
#define _OS_INT10HZ_D_PRIO 0x01


extern void INT_APAGAR_CAPSALS(void);


CODE_FAST static void INT_TEMPS_DIST(void)    
{
  // cas int temps (10 Hz.)
  // aplica cas TX50
  // aplica cas TX40 amb TestConector activat
  if(XTI_PRL_bit.no2)
  {
    XTI_PRL_bit.no2 = 0;    // treu flag
    if(tx30.testconectorOn)
      IOPORT2_PD_bit.no6 = 1;
	INT10HZ_C();
    if(tx30.testconectorOn)
      IOPORT2_PD_bit.no6 = 0;
  }
  
  
 if(tipoTx == TX50)
 {
      if(XTI_PRL_bit.no3)
      {
        // cas int distancia 
        XTI_PRL_bit.no3 = 0;    // treu flag
		INTD_C();
      }
      if(XTI_PRL_bit.no4)
      {
        // cas int exces temps impressora
        XTI_PRL_bit.no4 = 0;    // treu flag
        INT_APAGAR_CAPSALS();
      }
 }
 else
 {
   // cas TX40
   if(tx30.testconectorOn == 0)
   {
       if(tarcom.gi.giTipo == 0)
       {
         // cas analogic
          if(XTI_PRL_bit.no4)
          {
            // cas int DIST1 (P2.10)
            XTI_PRL_bit.no4 = 0;    // treu flag
            flanc_pujada = 1;
            IOPORT2_PD_bit.no7 = 0;            
          }
          if(XTI_PRL_bit.no5)
          {
            // cas int DIST2 (P2.11)
            XTI_PRL_bit.no5 = 0;    // treu flag
            if(flanc_pujada)
            {
              if(--CNT_DIVK == 0)
              {
                 CNT_DIVK = tarcom.DIVK;
                 INTD_C();
                 IOPORT2_PD_bit.no7 = 1;
              }
              flanc_pujada = 0;
            }
          }
       }
       else
        {
          // cas digital
          
        }
   }
   else
   {
     // cas test conector activat
      if(XTI_PRL_bit.no3)
      {
        // cas int EDIST (P2.9)
        IOPORT2_PD_bit.no7 = 1;  // treu pols per P2.7
        XTI_PRL_bit.no3 = 0;    // treu flag
        if(--CNT_DIVK == 0)
        {
           CNT_DIVK = tarcom.DIVK;
           INTD_C();
        }
        IOPORT2_PD_bit.no7 = 0;
      }
    }
     
   }
#if DEBUG_TEMPS_INTERRUPCIO
         IOPORT0_PD_bit.no13 = 0;
#endif  
}

 

void sortida_impuls_salt(int on_off)
{

#if DEBUG_TEMPS_INTERRUPCIO || DEBUG_TEMPS_OS_TIMER
  return;
#endif
  
  // salida salto a test conector
  if(tipoTx==TX50)
  {
      if(on_off)
      {
              OS_IncDI();
              PORT4 |= 0x40;          // Inclou SALTO 
              IOPORT_4 = PORT4;
              OS_DecRI();
      }
      else 
      {
              OS_IncDI();
              PORT4 &= 0xbf;          // Treu SALTO 
              IOPORT_4 = PORT4;
              OS_DecRI();
      }
  }
     else
     {
       IOPORT0_PD_bit.no13 = on_off;
     }
}

// *********************************************************************************
//   tarifaci�n 
// *********************************************************************************

void tarifador_stop_and_reset(struct s_serv* p_serv)
{
 p_serv->TARIFANDO=0;
 p_serv->ACFS_K=0;
 p_serv->ACFS_H=0;
 p_serv->PRIMSAL_K = p_serv->SAV_PRIMSAL_K = 1;
 p_serv->PRIMSAL_H=1;

  p_serv->ACC_K=0;
  p_serv->ACC_H=0;
  p_serv->NS_K=0;
  p_serv->NS_H=0;
  p_serv->SALT_PER_DIST=0;
  p_serv->SALT_PER_TEMPS=0;

}

void tarifador_stop(struct s_serv* p_serv)
{
 p_serv->TARIFANDO=0;
}

void tarifador_start(struct s_serv* p_serv)
{
	p_serv->TARIFANDO=1;
	sortida_impuls_salt(1);
	mantenersalidaimpulso=1;
}

int esPrimerSalt(struct s_serv* p_serv){
	return p_serv->PRIMSAL_H || p_serv->PRIMSAL_K;
}

void tarifador_noFaPrimerSalt(struct s_serv* p_serv)
{
	p_serv->PRIMSAL_K = p_serv->SAV_PRIMSAL_K = 0;
	p_serv->PRIMSAL_H=0;
}

// ***************************************************************
//     producido salto                                           *
// ***************************************************************
unsigned char tarifador_producido_salto(struct s_serv* p_serv)
{
unsigned char salt;

 salt=0;
 
  if(p_serv->SALT_PER_TEMPS)
    {
      p_serv->SALT_PER_TEMPS=0;
      salt=2;
    }
  else if(p_serv->SALT_PER_DIST)
    {
      p_serv->SALT_PER_DIST=0;
      salt=1;
    }
 
 return(salt);
}


void desconect_tx30(void)
{
    //tx30.estat=E_MEM_MAL;
	// indicador per que recarregui tarifa interna
	tx30.hay_que_recargar=1;
}



void recuperar_registros_a_tx30(void) // es crida al fer recarga
{
 copia_reg_a_tots();
}


// *******************************************************
//    PROGRAMA  PRINCIPAL                                 
// *******************************************************
//  INICIALIZACIONES 
unsigned char  sensors_ant;
unsigned long   velocidad;
unsigned long  vsal;
struct s_time  timeaux;


/***************************************************************/
/*  prepara nous parametres per interrupcio temps              */
/*  actualitza tarifas treballades                             */
/*  posa punter a nova tarifa                                  */
/*  actualitza llums exteriors  i led estat                    */
/*  actualitza leds frontal                                    */
/*  tracta suplements automatics                               */
/*  actualitza punters p_hm_oc                                 */
/*                    p_crono  segons global/tarifa            */
/***************************************************************/



void cambio_tarifa(unsigned char tar,unsigned char estat, struct s_serv* p_serv)
{
struct s_bloc_tar * pbt;    // punter a bloc_tar 
struct s_tarifa * pt;
unsigned char  mask_k_ps,mask_k_ss,mask_h_ps,mask_h_ss;
unsigned char  aux;
float  a;
unsigned long  NEW_NS_K,NEW_NS_H,NEW_ACC_K,NEW_ACC_H;

   
 p_serv->tarifa = tar;

 pt=&tarifa[tar];
 if(estat==E_OCUPADO)
   {
    pbt=&bloc_tar[pt->b_t_ocup];
    mask_k_ps=0x01;
    mask_k_ss=0x04;
    mask_h_ps=0x02;
    mask_h_ss=0x08;
    
    p_serv->tartrab |= (0x00000001L<< tar);
   }
 else
   {
    pbt=&bloc_tar[pt->b_t_pagar];
    mask_k_ps=0x10;
    mask_k_ss=0x40;
    mask_h_ps=0x20;
    mask_h_ss=0x80;
   }
 
      p_serv->CAMBIAR_DE_TARIFA=0;
      if(pbt->tipt_vf == 2)
	p_serv->NEW_MODO_VF = 1;
      else
        p_serv->NEW_MODO_VF = pbt->tipt_vf;
      p_serv->NEW_VALUE_VF = (int) pbt->vf_fija;
      p_serv->NEW_TIPT_ACS=pbt->tipt_acs;
      p_serv->NEW_FS_H_ps=pbt->FS_H_ps;
      p_serv->NEW_FS_K_ps=pbt->FS_K_ps;
      p_serv->NEW_FS_H_ss=pbt->FS_H_ss;
      p_serv->NEW_FS_K_ss=pbt->FS_K_ss;
      p_serv->NEW_FACTOR_N_ps=pbt->FACTOR_N_ps;
      p_serv->NEW_FACTOR_N_ss=pbt->FACTOR_N_ss;
      p_serv->NEW_KK_ps=pbt->KK_ps;
      p_serv->NEW_KH_ps=pbt->KH_ps;
      p_serv->NEW_KK_ss=pbt->KK_ss;
      p_serv->NEW_KH_ss=pbt->KH_ss;

      if(p_serv->PRIMSAL_K)
         p_serv->NEW_CUENTA_K =pbt->bits_cuenta & mask_k_ps;
      else
         p_serv->NEW_CUENTA_K =pbt->bits_cuenta & mask_k_ss;

      if(p_serv->PRIMSAL_H)
         p_serv->NEW_CUENTA_H =pbt->bits_cuenta & mask_h_ps;
      else
         p_serv->NEW_CUENTA_H =pbt->bits_cuenta & mask_h_ss;

      memcpy(&p_serv->vsal[0],&pbt->vsal[0], sizeof(p_serv->vsal));
      
      p_serv->CAMBIAR_DE_TARIFA=1;    // per evitar que la interrupcio modifiqui res
                              // o agafi valors a mig modificar
             
      // copia acumuladors a nous valors (per cas no es recalculin)
      NEW_ACC_H = p_serv->ACC_H;
      NEW_NS_H = p_serv->NS_H;
      NEW_ACC_K = p_serv->ACC_K;
      NEW_NS_K = p_serv->NS_K;
      
      if(p_serv->no_es_primer_canvi)
        {
         // cas cambia de tarifa (ja no es la primera)
         // converteix fraccio salt a nova tarifa
         if(p_serv->PRIMSAL_K)
               {
                a=((float)(p_serv->NS_K*p_serv->KK_ps+p_serv->ACC_K)*(float)p_serv->NEW_KK_ps*(float)p_serv->NEW_FACTOR_N_ps)/(float)(p_serv->KK_ps*p_serv->FACTOR_N_ps);
                NEW_NS_K=(unsigned long)a/p_serv->NEW_KK_ps;
                if(NEW_NS_K>=p_serv->NEW_FACTOR_N_ps)
                      {
                       NEW_NS_K=p_serv->NEW_FACTOR_N_ps-1;
                       if(p_serv->NEW_KK_ps>p_serv->NEW_FS_K_ps)
                         NEW_ACC_K=p_serv->NEW_KK_ps-p_serv->NEW_FS_K_ps;
                       else
                         NEW_ACC_K=0;
                      }
                else
                      NEW_ACC_K=(unsigned long)a-NEW_NS_K*p_serv->NEW_KK_ps;
               }
         else
               {
                a=((float)(p_serv->NS_K*p_serv->KK_ss+p_serv->ACC_K)*(float)p_serv->NEW_KK_ss*(float)p_serv->NEW_FACTOR_N_ss)/(float)(p_serv->KK_ss*p_serv->FACTOR_N_ss);
                NEW_NS_K=(unsigned long)a/p_serv->NEW_KK_ss;
                if(NEW_NS_K>=p_serv->NEW_FACTOR_N_ss)
                      {
                       NEW_NS_K=p_serv->NEW_FACTOR_N_ss-1;
                       if(p_serv->NEW_KK_ss>p_serv->NEW_FS_K_ss)
                         NEW_ACC_K=p_serv->NEW_KK_ss-p_serv->NEW_FS_K_ss;
                       else
                         NEW_ACC_K=0;
                      }
                else
                      NEW_ACC_K=(unsigned long)a-NEW_NS_K*p_serv->NEW_KK_ss;
               }
         if(p_serv->NEW_TIPT_ACS)
               {
                // cas temps i distancia separats
                if(p_serv->PRIMSAL_H)
                      {
                       a=((float)(p_serv->NS_H*p_serv->KH_ps+p_serv->ACC_H)*(float)p_serv->NEW_KH_ps*(float)p_serv->NEW_FACTOR_N_ps)/(float)(p_serv->KH_ps*p_serv->FACTOR_N_ps);
                       NEW_NS_H=(unsigned long)a/p_serv->NEW_KH_ps;
                       if(NEW_NS_H>=p_serv->NEW_FACTOR_N_ps)
                         {
                              NEW_NS_H=p_serv->NEW_FACTOR_N_ps-1;
                              if(p_serv->NEW_KH_ps>p_serv->NEW_FS_H_ps)
                                NEW_ACC_H=p_serv->NEW_KH_ps-p_serv->NEW_FS_H_ps;
                              else
                                NEW_ACC_H=0;
                         }
                       else
                         NEW_ACC_H=(unsigned long)a-NEW_NS_H*p_serv->NEW_KH_ps;
                      }
                else
                      {
                       a=((float)(p_serv->NS_H*p_serv->KH_ss+p_serv->ACC_H)*(float)p_serv->NEW_KH_ss*(float)p_serv->NEW_FACTOR_N_ss)/(float)(p_serv->KH_ss*p_serv->FACTOR_N_ss);
                       NEW_NS_H=(unsigned long)a/p_serv->NEW_KH_ss;
                       if(NEW_NS_H>=p_serv->NEW_FACTOR_N_ss)
                         {
                              NEW_NS_H=p_serv->NEW_FACTOR_N_ss-1;
                              if(p_serv->NEW_KH_ss>p_serv->NEW_FS_H_ss)
                                NEW_ACC_H=p_serv->NEW_KH_ss-p_serv->NEW_FS_H_ss;
                              else
                                NEW_ACC_H=0;
                         }
                       else
                         NEW_ACC_H=(unsigned long)a-NEW_NS_H*p_serv->NEW_KH_ss;
                      }
               }
        }
      
      p_serv->no_es_primer_canvi = 1;  // versio 1_04G
      
      p_serv->TIPT_ACS = p_serv->NEW_TIPT_ACS;
      p_serv->MODO_VF = p_serv->NEW_MODO_VF;
      p_serv->VALUE_VF = p_serv->NEW_VALUE_VF;
      p_serv->CUENTA_K = p_serv->NEW_CUENTA_K;
      p_serv->CUENTA_H = p_serv->NEW_CUENTA_H;
      p_serv->FS_H_ps = p_serv->NEW_FS_H_ps;
      p_serv->FS_K_ps = p_serv->NEW_FS_K_ps;
      p_serv->FS_H_ss = p_serv->NEW_FS_H_ss;
      p_serv->FS_K_ss = p_serv->NEW_FS_K_ss;
      p_serv->FACTOR_N_ps = p_serv->NEW_FACTOR_N_ps;
      p_serv->FACTOR_N_ss = p_serv->NEW_FACTOR_N_ss;
      p_serv->KK_ps = p_serv->NEW_KK_ps;	
      p_serv->KH_ps = p_serv->NEW_KH_ps;
      p_serv->KK_ss = p_serv->NEW_KK_ss;
      p_serv->KH_ss = p_serv->NEW_KH_ss;
      p_serv->NS_K = NEW_NS_K;
      p_serv->NS_H = NEW_NS_H;
      p_serv->ACC_K = NEW_ACC_K;
      p_serv->ACC_H = NEW_ACC_H;
      p_serv->TARIFA_POR_DISTANCIA_ps = pbt->tarifarDistancia_ps;
      p_serv->TARIFA_POR_DISTANCIA_ss = pbt->tarifarDistancia_ss;
      LED_HORARIA_OFF;
       if(tarcom.new_leds_ho_km)
        {
          tx30.refresc_leds_ho_km = 1;    // per actualitzar
        }

      if(zapper_error_en_curs())
      {
           p_serv->CUENTA_K=0;
           if(tarcom.exces_vel_no_horaria)
                   p_serv->CUENTA_H=0;        
      }
      p_serv->CAMBIAR_DE_TARIFA=0;

 
 if(estat==E_OCUPADO)
    {
     ledocpag[0] = pt->ledocup[0];
     if(servcom.es_control)
       ledocpag[1] = 0x39;  // C
     else
      ledocpag[1] = pt->ledocup[1];
     out_luzex(pt->luzocup[sext_luz],pt->luzocup[sext_luz+4]);
// v 1.04      out_luzfront(pt->luzocup[sext_luz]);
     LEDS_FRONTAL(pt->lf_oc);
    }
 else
    {
     ledocpag[0] = pt->ledapagar[0];
     ledocpag[1] = pt->ledapagar[1];
     out_luzex(pt->luzpagar[sext_luz],pt->luzpagar[sext_luz+4]);
// v 1.04      out_luzfront(pt->luzpagar[sext_luz]);
	 servcom.save_lf_pag = pt->lf_pag;
     LEDS_FRONTAL(servcom.save_lf_pag);
    }
 if(servcom.visu_led_viaj)
 {
    ledocpag[0] = (ledocpag[0] & 0x00ff) | ((short)SS[viaj+1]<<8);
    display(0,NF_LEDEST,(void*)ledocpag); // no hi ha flash
 }
 else
    display(0,NF_LEDTAR_INTERM,(void*)ledocpag);

// gestio punters per display dist i crono Global o de Tarifa 
 if(pt->visu_globtar & GLOBTAR_DIST)
   { 
//    p_hm_oc=&serv[0].hm_oc_tarifa;   // per que sigui long (parcial_dist[tar]);
//    serv[0].hm_oc_tarifa=(unsigned long)p_serv->per_tarifa[tar].parcial_dist;
     p_hm_oc = &serv[0].per_tarifa[tar].parcial_dist;
   }    
 else
    p_hm_oc=&serv[0].hm_oc;
 
 if(pt->visu_globtar & GLOBTAR_CRONO)
    p_crono=&serv[0].per_tarifa[tar].parcial_crono;
 else
    p_crono=&serv[0].crono;

// GESTIO VISU DISP1/2 
 if(servei.mask_vd<2)
   {
     servei.vd[0]=0;
     servei.vd[1]=0;

     servei.op_tec0=pt->op_tec0;

     if(estat==E_PAGAR && servei.mask_vd==1 && (pt->op_tec0 & 0x04))
         servei.mask_vd=0;
     if(servei.mask_vd==0 || (servei.op_tec0 & 0x02))   /* visu actiu o recuperable */
       {
       
        aux=(estat==E_OCUPADO)? 0:2;
        memcpy(&servei.vd[0],&pt->op_vd[aux],2);        
          
        aux=servei.vd[gt.mitad_vd];

        servcom.impk_visu=pbt->impkm_ss;    /* per visu imports hora/km a disp2 */
        servcom.imph_visu=pbt->impho_ss;

        if(servei.mask_vd==0)
          {
           DISPLAY_VD1(aux);     /* display 1 */
           DISPLAY_VD2(aux & 0x0f);  /* display 2 */
          }
       }
   }
 if(tarcom.supl_solo_fin_serv!=1) // si es ==1 nomes es fa al pasar a fi servei
	 SUPL_AUT(tar);

 if(tarifa[tar].pctip==2)
   p_serv->fer_pcaut|=1;

 if(p_serv->d_t_sep&&((pbt->tipt_vf == 1) || (pbt->tipt_acs==0)))
   {
    // nomes per ticket holanda
    p_serv->d_t_sep=0;
    //servei.import_dist_temps[0]=0L;
    //servei.import_dist_temps[1]=0L;
    memset(&p_serv->import_dist_temps[0],0,sizeof p_serv->import_dist_temps);
   }

}

char iniciar_servicio=0;
unsigned int vel,vel_ant;



//**************************************************
// interrupcio timer1  (cada 1ms.)                 
//**************************************************
short tick_counter_100ms = 0;   

OS_TIMER tx30Timer1ms ;
#define TIME_tx30Timer1ms 1   // 10ms

char loop_40ms = 40;

CODE_FAST void tx30_interrupt_timer1ms(void)
{
static   int bit100 = 0;
static    unsigned char  cnt10ms = 10;
static    unsigned char  cnt50ms = 50;
static char half_10hz;

// refresca luzfront segons lluminositat   
    if(tipoTx == TX50)
      refresh_luzfront();
// genera clock 100ms per ticks temps tarifacio
    if(tipoTx == TX40)
    {
      if(tx30.testconectorOn == 0)
        {
          // genera clock 100ms.
          if(++tick_counter_100ms >= 50)
          {
            tick_counter_100ms = 0;
            if((half_10hz ^= 0x01)& 0x01)
            {
              IOPORT2_PD_bit.no6 = 0;
              INT10HZ_C();
            }
            else
              IOPORT2_PD_bit.no6 = 1;
          }         
        }
    }
    if(--cnt10ms == 0)
    {
      cnt10ms =10;
      teclat_tic10ms();   // decrementa timers teclat
    }
    
    if(loop_40ms)
      --loop_40ms;    
//***************************************************
 
// 
// tractament metro_zapper
//
    
    if(tx30.estat!=E_MEM_MAL)
    	zapper_1ms[zapper_hi_ha_zapper()]();    
    
    if(--cnt50ms == 0)
    {
       cnt50ms = 50;
       if(delay_impre)
          --delay_impre; // delay per evitar mogollon impressora
       
       if(luminoso.timer_envio_serie>1)
               luminoso.timer_envio_serie--;
       if(luminoso.timeout_resp_serie>1)
               luminoso.timeout_resp_serie--;
    
       if(timer50>1)
         timer50--;
       bit100=!bit100;  // per contar cada 100ms  
       if(bit100)
          {
           if(timer100>1)
              timer100--;
           if(timer_2>1)
              timer_2--;
           if(luminoso.timer_apagat_per_error>1)
                luminoso.timer_apagat_per_error--;
           if(tx30.timer_alfanum >1)
             tx30.timer_alfanum--;
           if(luminoso.cnt_dec_segon_desde_pas_a_lliure)
             luminoso.cnt_dec_segon_desde_pas_a_lliure--;
          }
    }
    
// gestio veloc�metre  

  	if(tx30.estat!=E_MEM_MAL)
		velocimetro_int_1ms();    
  
  
      OS_RetriggerTimer(&tx30Timer1ms);
}


// **********************************************************
// rutina que es crida desde Task Impre
// al acabar un tiquet
// **********************************************************

void impre_event_call_back(char ticket_ret_event, char ticket_ok)
{
  tx30.event_impre_ok = ticket_ok;  // indicador si ha anat b�
  tx30.event_impre = ticket_ret_event;
}



// *****************************************************************
// llegeix blqs_ext de EPROM_TAX                            
// si check sum no es correcte, borra textes Header i Foot 
// *****************************************************************
int leer_verif_blqs_ext(void)
{
 leer_eprom_tabla(GE_BLQS_EXT);    //leer_eprom(EPROM_TAX,EEPT_BLQS_EXT_POS,EEPT_BLQS_EXT_LEN,(unsigned char *)&blqs_ext);
 if(blqs_ext.chk_blqs_ext == 0 || blqs_ext.chk_blqs_ext != calcul_chk_blqs_ext())
    {
     // repeteix lectura per si de cas
     leer_eprom_tabla(GE_BLQS_EXT);    
     if(blqs_ext.chk_blqs_ext != calcul_chk_blqs_ext())
        {
          // borra i regraba
         memset((char*)&blqs_ext,0,sizeof blqs_ext);
         grabar_eprom_tabla(GE_BLQS_EXT);
		 return 0;
        }
    }
 return 1;
}

void copia_blqs_ext_a_blqs_ext_32(){
	memset((char*)&blqs_ext_32,0,sizeof blqs_ext_32);
	memcpy(blqs_ext_32.texte_H1, blqs_ext.texte_H1, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_H2, blqs_ext.texte_H2, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_H3, blqs_ext.texte_H4, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_H4, blqs_ext.texte_H4, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_H5, blqs_ext.texte_H5, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_H6, blqs_ext.texte_H6, LEN_TEXTE_H);
	memcpy(blqs_ext_32.texte_F1, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_F2, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_F3, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_F4, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_F5, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_F6, blqs_ext.texte_F1, LEN_TEXTE_F);
	memcpy(blqs_ext_32.texte_nom, blqs.texte_nom, LEN_TEXTE_NOM);
	blqs_ext_32.chk_blqs_ext = calcul_chk_blqs_ext_32();
	grabar_eprom_tabla(GE_BLQS_EXT_32);
}

int leer_verif_blqs_ext_32(void)
{
 leer_eprom_tabla(GE_BLQS_EXT_32);    //leer_eprom(EPROM_TAX,EEPT_BLQS_EXT_POS,EEPT_BLQS_EXT_LEN,(unsigned char *)&blqs_ext);
 if(blqs_ext_32.chk_blqs_ext == 0 || blqs_ext_32.chk_blqs_ext != calcul_chk_blqs_ext_32())
    {
     // repeteix lectura per si de cas
     leer_eprom_tabla(GE_BLQS_EXT_32);    
     if(blqs_ext_32.chk_blqs_ext == 0 || blqs_ext_32.chk_blqs_ext != calcul_chk_blqs_ext_32())
        {
			 // borra i regraba
			memset((char*)&blqs_ext_32,0,sizeof blqs_ext_32);
			grabar_eprom_tabla(GE_BLQS_EXT_32);
			return 0;
        }
    }
 return 1;
}

// *****************************************************************
// passa a memoria dades canvis tarifa 
// llegeix blqs_modifs_tar de EPROM_TAX                            
// si check sum no es correcte, posa a 0 numero canvis fets
// *****************************************************************      
void leer_verif_blqs_modifs_tar(void)
{
     leer_eprom_tabla(GE_BLQS_MODIFS_TAR);
     if((blqs_modifs_tar.nmodifs_tar > MAX_MODIFS_TAR) ||
        (blqs_modifs_tar.chk_blqs_modifs_tar != calcul_chk_blqs_modifs_tar()))
     {
       // repeteix lectura per si de cas
       leer_eprom_tabla(GE_BLQS_MODIFS_TAR);
       if((blqs_modifs_tar.nmodifs_tar > MAX_MODIFS_TAR) ||
          (blqs_modifs_tar.chk_blqs_modifs_tar != calcul_chk_blqs_modifs_tar()))
       {
         // borra i regraba
         blqs_modifs_tar.nmodifs_tar = 0;
         grabar_eprom_tabla(GE_BLQS_MODIFS_TAR);
       }
     }
}

void testConector_change_mode(int forced, char k_ja_instalada)
{
char mode_aux;  
      mode_aux = (IOPORT1_PD_bit.no3 == 0);
      if((mode_aux != tx30.testconectorOn) || forced)
      {
        // cas ha canviat mode o change forced
        if(mode_aux == 1)
        {
          // cas s'ha activat Test Conector
          
          // desactiva interrupcio metres 
          XTI_MRL &= ~0x10;   // P2.10     int DIST_1
          XTI_MRL &= ~0x20;   // P2.11     int DIST_2
          __no_operation();
          __no_operation();
          //
          tx30.testconectorOn = 1;
          //
          // activa interrupcio metres de Test Conector
          XTI_MRL |= 0x08;    // P2.9     int EDIST
          // activa interrupcio temps Test Conector
          XTI_MRL |= 0x04;    // P2.8     int ETMP

        }
        else
        {
            // desactivat
            
            // desactiva interrupcio metres de Test Conector
            XTI_MRL &= ~0x08;    // P2.9     int EDIST
            // desactiva interrupcio temps Test Conector
            XTI_MRL &= ~0x04;    // P2.8     int ETMP
            __no_operation();
            __no_operation();
            //
            tx30.testconectorOn = 0;
            //
              // activa interrupcio metres
            if(k_ja_instalada)
            {
                XTI_MRL |= 0x10;   // P2.10     int DIST_1
                XTI_MRL |= 0x20;   // P2.11     int DIST_2
            }
         }
      }
 }


#define PROGRAM_NUM_BYTES   0x60010020

extern void paso_MEM_MAL(void);
extern void display_1_en_libre(int err);

void task_tx_interno(void)
{
unsigned long  segs_fallo_t;
unsigned char  mira_reloj;
unsigned long  taux;
unsigned char  ii,tipsal;
unsigned char  aux;
unsigned char  canvi_mitad,fer_vd1,fer_vd2;
unsigned long   lindex_fin;
char buf_luz[2];
char new_luz;
int st, st1;
struct s_serv* p_serv;
int segonsTramaGps=0;

 RESET_LAPSUS();

 vel_ant=9999;

 confi_format_diames(tarcom.formddmm);
 leer_verif_cods(); //  llegir i verificar codigs secrets 

 leer_eprom_tabla(GE_BLQS);    //leer_eprom(EPROM_TAX,EEPT_BLQS_POS,EEPT_BLQS_LEN,(unsigned char *)&blqs);
 ++blqs.n_descs;
 
 blqs.chk_blqs=calcul_chk_blqs();  //grabar_eprom(EPROM_TAX,EEPT_BLQS_POS+offsetof(struct s_blqs,n_descs),sizeof blqs.n_descs,(unsigned char *)&blqs.n_descs);
 grabar_eprom_tabla(GE_BLQS_desc);

  // llegeig blqs_ext  ( textes capssalera i peu per tiquets)
  st1 = leer_verif_blqs_ext();
  st = leer_verif_blqs_ext_32();
  if(st == 0 && st1 == 1)
	 copia_blqs_ext_a_blqs_ext_32();

 sensors_ant=0xff;

 delay_impre=40; // 2"

  for(ii=0; ii<MAX_PSGRS ; ii++)
  {
    serv[ii].CAMBIAR_DE_TARIFA=0; 
    tarifador_stop(&serv[ii]);
  }
 

 timer50=0;
 timer100=0;
 timer_2=0;
 
 velocidad=0;  // afegit a versio  2.08

 exces_vel=0;
 gt.mitad_vd=0;                          
 gt.segs_interm=tarcom.segs_interm;

 lindex_fin = *(int*)PROGRAM_NUM_BYTES;
 check_programa = crc32(0x234adc1a, 0, 0x24);
 check_programa = (crc32(check_programa, (unsigned char *)0x28, lindex_fin - 0x28) & 0xffff);
//   check_programa = 0;
//   // es fa en dos for per saltar els 4 bytes check
//   for(lindex=0L;lindex<0x24;lindex++)
//        check_programa+=(unsigned int)(*(unsigned char*)(lindex+0x60010000L));
//   lindex_fin = *(int*)PROGRAM_NUM_BYTES;
//   for(lindex=0x28;lindex < lindex_fin;lindex++)
//        check_programa += (unsigned int)(*(unsigned char*)(lindex+0x60010000L));

// mira si s'ha carregat nou programa
   
   if(boot.program_loaded != 0xaa5555aa)
   {
	   memset((void*)&servcom, 0, sizeof servcom);
	   memset((void*)&tx30, 0, sizeof tx30);
	   memset((void*)&tarcom, 0, sizeof tarcom);
	   tx30.estat = E_MEM_MAL;   // per que hagi de recarregar tarifa
	   // en aquest cas no cal conservar l�hora per calcular temps en off
	   get_reloj(RELLOTGE_PLACA,(struct s_time*)&gt.time);
	   
	   if(blqs.nmodifs_prog < MAX_MODIFS_PROG)
	   {
		   if((blqs.nmodifs_prog == 0) || (blqs.modif_prog[blqs.nmodifs_prog-1].version != bcd_short(VERSION_TX))
			  ||(blqs.modif_prog[blqs.nmodifs_prog-1].check != reverse_int(check_programa)) )
		   {
			   guarda_versio_prog_a_blqs();
		   }
	   }
	   // inicialitza fecha borrat totalitzadors (v 1.04)
	   // eliminat a versio 2.00  
	   //time_to_fechhor(&tx30.fecha_borr_tots , &gt.time);
	   
	   zapper_init();    
	   multiplex_init_0();    // per reiniciar contador dies sense clau correcte
	   
	   bloqueo_torn.events_preprog = 0;   // caldra re-preprogramar events  
	   boot.program_loaded = 0xaa5555aa;
	   
   }
   
   
 if((tx30.estat>=ETX30_FIN)||(tx30.estat==E_MEM_MAL)||(!tarifa_ok())) //  !!!!afegir reloj_ok()  
   {
    paso_MEM_MAL(); 
   }
 else
   {
    segs_fallo_t=segon_del_dia(&gt.time);
    velocimetro_confi(tarcom.k,tarcom.DIVK,1); 
    instala_k(tarcom.k, &tarcom.gi);
   }

 OS_CreateTimer(&tx30Timer1ms, tx30_interrupt_timer1ms,TIME_tx30Timer1ms);
 OS_StartTimer(&tx30Timer1ms);
// Instala interrupcio 10Hz 
  OS_ARM_InstallISRHandler(XTI_VECT_ID, &INT_TEMPS_DIST);   // Timer impre interrupt vector.
  OS_ARM_ISRSetPrio(XTI_VECT_ID, _OS_INT10HZ_D_PRIO);       // Timer/counter interrupt priority.
  OS_ARM_EnableISR(XTI_VECT_ID);                            // Enable timer3 interrupt.
  
  if(tipoTx == TX50)
  {
      XTI_MRL |= 0x04;   // P2.8      int temps (10 Hz.) 
      XTI_MRL |= 0x08;   // P2.9      int distancia
      XTI_MRL |= 0x10;   // P2.10     int exces temps impressora
  }
  else
  {
    testConector_change_mode(1, 0);   // (forced i NOT k_ja_instalada)
  }
  // Instala interrupcio per buzzer 
  // i cas TX40 per display 
  OS_ARM_InstallISRHandler(T1_GI_VECT_ID, &INT_BUZZER_DISPLAY);   // Timer 1 interrupt vector.
  OS_ARM_ISRSetPrio(T1_GI_VECT_ID, _OS_TIMER1_PRIO);       // Timer 1 interrupt priority.
  OS_ARM_EnableISR(T1_GI_VECT_ID);                            // Enable timer1 interrupt.

  
  XTI_CTRL_bit.WKUP_INT = 0;   // disable wake-up mode 
  XTI_CTRL_bit.ID1S = 1;
  

 if(tx30.estat!=E_MEM_MAL){
  copia_serie_lic_a_reg();  
  td30_init(tarcom.hi_ha_td30);  
  impre_init(tarcom.tipus_impressora, tarcom.hi_ha_td30);   //  ja s�ha actualitzat tarcom.tipus_impressora cas MEM_MAL 
  luminoso_init();
  ccab_init(tarcom.tr_ccab);
 }
 
 memcpy(&timeaux,&gt.time,sizeof(struct s_time));

 get_reloj(RELLOTGE_PLACA,(struct s_time*)&gt.time);
 if(memcmp(&timeaux.minute,&gt.time.minute,offsetof(struct s_time,year)
                                             -offsetof(struct s_time,minute)
                                             +sizeof gt.time.minute))
   {
    gt.min_ant=0xff;
    gt.dia_ant=0xff;
    bloqueo_torn.prescindir_canvi_minut = 1;  // 
   }
 else
    bloqueo_torn.prescindir_canvi_minut = 0;

 taux=segon_del_dia(&gt.time);
 if(taux<segs_fallo_t)
    taux+=86400L;
 taux-=segs_fallo_t;
 exces_fallo_t = taux>tarcom.fallot;

 if(tx30.estat!=E_MEM_MAL){
	recalculs_gt();
 	comprova_totalitzadors_eq();
	gestio_bloqueo_torn_0(&timeaux, taux);
 }

 sensors = llegir_sensors();
 sext_luz = sensors & (SENSORS_CLAU_CON | SENSORS_LLUM_POS);  // no es fa xor amb 3 per que despres ho trobi canviat
 sext=sensors & SENSORS_PISON;
 
 tx30.last_LUZFRONT = 0xff;  // per obligar a actualitzar

 pasj =(sensors & SENSORS_PASAJERO) != 0;

 if(exces_fallo_t)
    {
     totalitzador_reset(TOT_TDESC);    //  temps desde ultima conexio  
     totalitzador_incr(TOT_NDESC);
    }
 
  
 if(blqs.nsalts && blqs.nsalts<300)
     contar_salts_inst=1;
 else
     contar_salts_inst=0;


 MASK_GEN &= ~(MSK_FP);

 if(tx30.estat==E_MEM_MAL || !tarcom.OP_LOCKT || tx30.estat!=E_OFF)
    MASK_GEN &= ~MSK_BLQ;  //  per evitar es pengi 

 mira_reloj=3;

 //ENABLE;

 tx30.event_impre = 0;
 MASK_GEN &= ~MSK_IMPRE;
 
  if(tarcom.pot_apagar_lluminos == 0)
    tx30.lluminos_apagat = 0;

// cancela possible entrada pin en Lliure
  tx30.pin_subestat = 0;
  MASK_GEN &= ~MSK_PIN_ENTRAR;

// reset indicador s'han rebut dades cobertura
  tx30.cobertura_rebuda = 0;
  tx30.cobertura_timeout = 0;
  
 event=V_START;
 while(1)
    {

// **********************************************************
// pasa usart 0 a 2
// per proves bluetooth
// **********************************************************
// 
      
     while(fallo_tension()); 
      
     if(((++mira_reloj)&0x03)==0)
        get_reloj(RELLOTGE_PLACA,(struct s_time*)&gt.time);

     if(event && hay_transicion_tx30())
       {
        pseudoevent=0;
        executar_semantica_TX30[nsem]();
        event=pseudoevent;    // per cas suma automatica o paglib_autom
       if(tx30.estat!=tx30.new_estat)
          {  
		   
	    tx30.estat=tx30.new_estat;
            if((tx30.estat!=E_OCUPADO)&&(tx30.estat!=E_PAGAR)&&(tx30.estat!=E_TOTS))
                LEDS_FRONTAL(tarcom.vars_estat[tx30.estat].lf);
          }
        new_luz = luz_frontal[tx30.estat];
        if((tx30.estat == E_CERRADO) && (tx30.subestat_torn != TORN_TANCAT))
          new_luz = 1;
        if(tx30.last_LUZFRONT != new_luz)
        {
          out_luzfront(new_luz);
          tx30.last_LUZFRONT = new_luz;
        }
        continue;
       }
     else
        event=0;

// ******************************************************
// cas E_MEM_MAL nomes espera tecla per carregar tarifa     
// ******************************************************
    if(tx30.estat == E_MEM_MAL)
    {
     event = get_event_teclat(0,&tecla_num);
     if(event)
       continue;    // per executar semantica
     else
     {
       OS_Delay(MS_LOOP_TX30);
       continue;
     }
    }
	
	 loop10ms();

     if(tarcom.tr_ccab)
       {
         gestio_ccab();
       }
     
//  gestio errors 1 i 2  de zapper                                              *    
    zapper_loop();
	int estaContant;
	if(tx30.estat==E_OCUPADO || tx30.estat==E_PAGAR || ((tx30.estat==E_FISERV) &&(servcom.n_viajs > 1)))
		estaContant=1;
	else
		estaContant=0;
      
// **********************************************************************
// canvi de segon,minut,dia                                             *
//                                                                      *
     if(gt.time.sec!=gt.seg_ant)
        {
			if((tarcom.hi_ha_td30==4 || tarcom.hi_ha_td30==5) && tx30.estat!=E_OFF && segonsTramaGps++==10){
				segonsTramaGps=0;
				solicitaGps();
			}
         gt.seg_ant=gt.time.sec;
         canvi_mitad=0;
	 	 luminoso.tempor_test++;
         if(--gt.segs_interm==0)
            {
             gt.segs_interm=tarcom.segs_interm;
             canvi_mitad=1;
             gt.mitad_vd=gt.mitad_vd==0 ? 1:0;
            }
        
         // gestio time-out cobertura
         if(tx30.cobertura_timeout)
         {
           tx30.cobertura_timeout--;
           if(tx30.cobertura_timeout == 0)
             tx30.cobertura_rebuda = 0;
         }
        zapper_canvi_segon();       // gestio zapper cada segon 
		//
		// gestio pausa cada segon
		//
		if(tx30.estat == E_LIBRE) 
		{
			switch(pausa.estat)
			{
			case EPAUSA_temporitza_inici_pausa_passiva:
				// per gestionar pas automatic a pausa passiva
				if(configs_desde_bg40.segons_pausa_passiva)
				{
					if((velocidad >= 5) || pausa.tecla_polsada)
					{
						pausa.tecla_polsada = 0;
						pausa.segons = 0;
					}
					else
					{
						pausa.segons++;
						if(pausa.segons >= configs_desde_bg40.segons_pausa_passiva)
						{
							pausa.estat = EPAUSA_temporitza_pre_pausa_passiva;
							MASK_GEN |= MSK_PAUSA;
							pausa.segons = PAUSA_SEGONS_PREPAUSA_PASIVA;
							pausa.visu_acumulat = 0;
							display_1_en_libre(0);
						}
					}
					
				}
				break;
			case EPAUSA_temporitza_pre_pausa_activa:
			case EPAUSA_temporitza_pre_pausa_passiva:
				if(velocidad >= 5) 
				{
					pausa.estat = EPAUSA_temporitza_inici_pausa_passiva;  // finalitza pre pausa
					MASK_GEN &= ~MSK_PAUSA;
					pausa.segons = 0;
					display_1_en_libre(0);
				}
				else
				{
					if(pausa.segons)
						--pausa.segons;
					if(pausa.segons == 0)
					{
						if(pausa.estat == EPAUSA_temporitza_pre_pausa_activa)							
							pausa.estat = EPAUSA_pausa_activa;
						else
							pausa.estat = EPAUSA_pausa_passiva;
						pausa.visu_acumulat = 0;
						display_1_en_libre(0);
					}
				}
				break;
			case EPAUSA_pausa_activa:
			case EPAUSA_pausa_passiva:
				if(velocidad >= 5) 
				{
					pausa.estat = EPAUSA_temporitza_inici_pausa_passiva;
					pausa.segons = 0;
					MASK_GEN &= ~MSK_PAUSA;
					display_1_en_libre(0);
					APAGAR_DISPLAY(2);
				}
				else
				{
					pausa.segons++;
					pausa.segons_acumulat_pausa++;
					if((pausa.segons %60) == 0)
						display_1_en_libre(0);
				}
				break;
			}
			
		}
		
        if(tipoTx == TX40)
          testConector_change_mode(0, 1);  // (forced i k_ja_instalada)
        
         if(cnt_ledtar_interm)
           {
            if(--cnt_ledtar_interm==0)
              {
               if(ledtar_interm==1)
                 {
                  ledtar_interm=2;
                  cnt_ledtar_interm=2;
                  DISPLAY_RESTORE(DISP_LEDEST,buf_ledtar_interm+2);
                 }
               else
                 {
                  ledtar_interm=1;
                  cnt_ledtar_interm=2;
                  DISPLAY_RESTORE(DISP_LEDEST,buf_ledtar_interm);
                 }
              }
           }
         
         
//
// ****** gestio crono i visu ***************************
//         
         fer_vd2=0;
         fer_vd1=0;
         if(tx30.estat==E_OCUPADO) //  el crono no conta en A PAGAR 
            {
//             incre_crono(&servei.crono[0]);
//             incre_crono(&parcial_crono[servei.tarifa][0]);
              for(ii=0; ii<MAX_PSGRS; ii++)
                {
                 if(servcom.viaj_activo[ii])
                   {
                    serv[ii].crono++;
                    serv[ii].per_tarifa[serv[ii].tarifa].parcial_crono++;  // versio 2.10
                   }
                }
             if(servei.mask_vd==0)
               {
                if(((servei.vd[gt.mitad_vd]&0x0f)==VD2_CRONO)
                   ||((servei.vd[gt.mitad_vd]&0x0f)==VD2_CRONO_CENT))
                   fer_vd2=1;
                if(((servei.vd[gt.mitad_vd] & 0xf0)==VD1_CRONO)
                  ||((servei.vd[gt.mitad_vd] & 0xf0)==VD1_CRONO_CENT))
                   fer_vd1=1;
               }
             if(servei.es_precio_fijo &&servei.segs_precio_fijo)
               {
                if((--servei.segs_precio_fijo)==0)
                  {
                   // para intermitencia display extres i posa blanking a import
                   DISP_FIXE(2);
                   display(1,NF_IMPORT,(unsigned char*)&serv[0].import);
                  } 
               }
            }
         if(servei.mask_vd==0)
           {
            if(fer_vd2 ||(canvi_mitad && (servei.vd[0]&0x0f)!=(servei.vd[1]&0x0f)))
               DISPLAY_VD2(servei.vd[gt.mitad_vd] & 0x0f);
            if(fer_vd1 ||(canvi_mitad && (servei.vd[0]&0xf0)!=(servei.vd[1]&0xf0)))
               DISPLAY_VD1(servei.vd[gt.mitad_vd] );
           }            
//
// ****** final gestio crono i visu ***************************
         
		 
	 //GESTION de error E-3 Generador de impulsos 
         if(tipoTx == TX50)
         {
            if(IOPORT2_PD_bit.no11 == 1)
            {
              // hay error de generador de impulsos del PIC
             if(tarcom.genimp_ocup)
               {
                habia_error_genimp=5;
                event=V_GENIMP;
                continue;   //  per transicio cas lliure,ocupat,pagar 
               }
             if(!habia_error_genimp)
               {
                habia_error_genimp=2;
                event=V_GENIMP;
                continue;   //  per transicio cas lliure,ocupat,pagar 
               }
            }
            else
            {// no hay error de generador de impulsos
             if(tarcom.genimp_ocup && habia_error_genimp)
               {
                if (--habia_error_genimp==0)
                  {
                   event=V_F_GENIMP;
                   continue;   //  per transicio cas lliure,ocupat,pagar 
                  }
               }
            }
         }
         else
         {
           // cas TX40
           
           // falta !!!!!!!!
         }
             
         if(tarcom.OP_CANVIVEL)  //  cada segon 
            {
             switch(tx30.estat)   //  per acumular temps Ocupat i libre 
               {
                case E_OCUPADO:
                case E_PAGAR:
                   if(tarcom.OP_CANVIVEL==1)
                      {
                       if(velocidad>=tarcom.veltop_canvivel)
                         vel_canvi_new=0x01;  //zona 1
                       else
                         vel_canvi_new=0x02;  //zona 2
                      }
                   else
                      {
                       if(velocidad>=tarcom.veltop_canvivel)
                         vel_canvi_new=0x02;  //zona 1
                       else
                         vel_canvi_new=0x01;  //zona 2
                      }
                   if(vel_canvi==vel_canvi_new)
                      iniciat_canvivel=0;
                   else
                      {
                       if(!iniciat_canvivel)
                         {
                          iniciat_canvivel=1;
                          if(vel_canvi_new==1)
                            cnt_canvivel=tarcom.segssortir_canvivel;
                          else
                            cnt_canvivel=tarcom.segsentrar_canvivel;
                         }
                       if((cnt_canvivel==0) || (--cnt_canvivel==0))
                         {
                          vel_canvi=vel_canvi_new;
                          iniciat_canvivel=0;
						  for(ii=0; ii<MAX_PSGRS ; ii++)
						  {
							  if(servcom.viaj_activo[ii])
							  {
								  if(!serv[ii].PRIMSAL_H || tarcom.no_canvis_primer_salt==0)
									  aux = tarifa_canvi_autom(serv[ii].tarifa,AUT_VEL, &serv[ii]);
								  if(aux != serv[ii].tarifa){
									  if((tx30.estat == E_PAGAR) && (ii == viaj))
										  cambio_tarifa(aux,E_PAGAR, &serv[ii]);
									  else
										  cambio_tarifa(aux,E_OCUPADO, &serv[ii]);
								  }
							  }
						  }
						 }
                      }
                    break;  //final case E_OCUP E_PAGAR
                default:
                    break;
               }
            }  // final if OP_CANVIVEL

         if((gt.time.minute !=gt.min_ant) || gt.canvi_fecha)
            {
             gestio_ajustar_1h();
             recalculs_gt();
             gt.min_ant=gt.time.minute;
             gt.canvi_minut=1;
             if((gt.time.day != gt.dia_ant)|| gt.canvi_fecha)
                {
                 gt.dia_ant=gt.time.day;
                 gt.tdia=calcul_tdia();   // calcula tdia per canvis aut.
                 gt.amb=calcul_amb();     // calcula amb
				 comprova_totalitzadors_eq();
                }
             gt.canvi_fecha=0;  

             totalitzador_incr(TOT_TDESC);    //  temps desde ultima conexio  
             switch(tx30.estat)   //  per acumular temps Ocupat i libre 
               {
                case E_OCUPADO:
                case E_PAGAR:
                    // el proces de minut es fa en base al contador
                    // de segons en ocupat
                    break;
                case E_LIBRE:
                  if(tarcom.OP_RELOJ_LIBRE) 
                    {
                     display_1_reloj_en_libre(0);  // nomes per refrescar
                     display(2,NF_HHMM,&gt.time.minute);
                    }
                  // sense break   
                default:

                    ++servei.minuts_lib;
                    if(pasj)
                       ++servei.minuts_lib_pasj;
                    break;
               }

             //if(!(PCON & 0x04))     //  temps en ON 
	    if(tx30.estat!=E_OFF && tx30.estat!=E_CERRADO)	  //  temps en ON  td-tx
               {
                totalitzador_incr(TOT_TON_P);
                totalitzador_incr(TOT_TON);     // v 2.02
                taux = totalitzador_read(TOT_TON, &st);
                if(st == 0)
                  taux = (taux/60)*100 + (taux%60);
                totalitzador_write(TOT_TONHHMM, taux);
               }
             bloqueo_turno_minuto();
             if((tx30.estat==E_LIBRE) && bloqueo_torn.esta_passant_itv && (bloqueo_torn.minuts_passant_itv == 0))
             {
               event = V_FI_ITV;    // per executar FI_ITV();
               continue;
             }

            } //final canvi minut
             
          bloqueo_turno_10seg();
             

         // mira si esta tarifant per canvi minut ocupat
         if(estaContant)
            {
             if(servcom.segs_ocupat<59)
                servcom.segs_ocupat++;
             else
                { // canvi minut ocupat
                 servcom.segs_ocupat=0;
                 totalitzador_incr(TOT_TOC_P);
                 totalitzador_incr(TOT_TOC);   // v 2.02
                }
              for(ii=0; ii<MAX_PSGRS; ii++)
                {
                 if(servcom.viaj_activo[ii] && ((tx30.estat==E_OCUPADO) || (tx30.estat==E_PAGAR)))
                   {
                     if(serv[ii].segs_ocupat<59)
                        serv[ii].segs_ocupat++;
                     else
                        { // canvi minut ocupat
                         serv[ii].segs_ocupat=0;
                         serv[ii].canvi_minut_ocupat=1;
                         switch(tx30.estat)   //  per acumular temps  
                           {
                            case E_OCUPADO:
                              ++serv[ii].minuts_oc;
                              //  sense break per que continui 
                            case E_PAGAR:
                              ++serv[ii].minuts_canviaut;
                              serv[ii].per_tarifa[serv[ii].tarifa].parcial_temps++;
                              calcul_toptar_temps(serv[ii].tarifa,serv[ii].per_tarifa[serv[ii].tarifa].parcial_temps, &serv[ii]);
                              break;
                           }
                        }
                   }
                }
            }
        }// final if canvi segon
//                                                                      *
// final canvi de segon,minut,dia                                       *
//***********************************************************************

//
// gestio lluminos cas canal serie
//

        luminoso_serie_main_loop();

             
// *****************************************************************
// proces salt                                                     *
//                                                                 *
	 if (mantenersalidaimpulso==1)
	 {
		 mantenersalidaimpulso=0;
	 }
	 else
	 {
		 sortida_impuls_salt(0);
	 }

	 if(estaContant){
        for(ii=0; ii<MAX_PSGRS ; ii++)
        {
         if(servcom.viaj_activo[ii])
         {
           p_serv = &serv[ii];
           tipsal = tarifador_producido_salto(p_serv);
           if(tipsal && ( (p_serv->aplicat_corsa_min == 0) || (tarcom.corsa_min_deixa_de_comptar== 0)))
               {
#if HOMOLOG
                SAVLUZ=(SAVLUZ^0x20)|0x10;
                out_luzex(SAVLUZ);
                timer_homolog=250;
#endif
                sortida_impuls_salt(1);
                if(tarcom.salto_pita)
                  buzzer_set(BUZZER_TIME_SALT);    // pita a cada salt
                if(p_serv->SAV_PRIMSAL_K != p_serv->PRIMSAL_K)
                {
                  // just es el primer salt
                  p_serv->SAV_PRIMSAL_K = 0;
                  cambio_tarifa(p_serv->tarifa,tx30.estat, p_serv);
                }
                vsal=p_serv->vsal[tipsal-1];
                if(p_serv->d_t_sep)
                   p_serv->import_dist_temps[tipsal-1]+=vsal;

                p_serv->import+=vsal;
                p_serv->per_tarifa[p_serv->tarifa].import+=vsal;  // versio 2.10
                p_serv->num_salts++;
                p_serv->num_salts_dist_temps[tipsal-1]++;   // per Xile
                
                calcul_toptar_import(p_serv->tarifa,p_serv->per_tarifa[p_serv->tarifa].import, p_serv);
                totalitzador_incr(TOT_SALTS);
                if(servei.mask_vd || (servei.vd[gt.mitad_vd]&0xf0)==VD1_IMP)
                    display_1_2();
                aux=tarifa_canvi_autom(p_serv->tarifa,AUT_IMP, p_serv);
                if(aux != p_serv->tarifa)
                   {
                    cambio_tarifa(aux,tx30.estat, p_serv);
                   }
               }
         }
        }
	 }
//                                                                 *
// final proces salt                                               *
//******************************************************************


//******************************************************************
//  calcul velocitat instantanea i maxima                          *
//                                                                 *

     velocidad=velocimetro_get(); 
     if(velocidad !=tx30.vel || tx30.refresc_leds_ho_km)
        {
         tx30.refresc_leds_ho_km = 0; 
         tx30.vel=velocidad;
         if(velocidad>servei.velmax)
            servei.velmax=velocidad;
         if(!servei.mask_vd && (servei.vd[gt.mitad_vd]&0x0f)==VD2_VEL)
             DISPLAY_VD2(VD2_VEL);
         switch(tx30.estat)
           {
            case E_LIBRE:               
                if(tarcom.visvel_imp_extr==1)
                  {
                   DISPLAY_VD1(VD1_VEL);
                  }
                else if(tarcom.visvel_imp_extr==2)
                  {
                   DISPLAY_VD2(VD2_VEL);
                  }
                break;
            case E_RELOJ:
                if(tx30.subestat==0 &&(tarcom.visvel_imp_extr==2))
                  {
                   DISPLAY_VD2(VD2_VEL);
                  }
                break;
           case E_OCUPADO:
             if(tarcom.new_leds_ho_km && (serv[viaj].MODO_VF == 3))
             {
                if(tx30.vel > serv[viaj].VALUE_VF)
                    {
                      NEW_LED_HORARIA_OFF;
                      NEW_LED_KM_ON;
                    }
                else
                    {
                      NEW_LED_HORARIA_ON;
                      NEW_LED_KM_OFF;
                    }
             }
             break;
           case E_PAGAR:
             if(tarcom.new_leds_ho_km && (serv[viaj].MODO_VF == 3))
             {
                NEW_LED_HORARIA_OFF;
                if(tx30.vel > serv[viaj].VALUE_VF)
                    {
                      NEW_LED_KM_ON;
                    }
                else
                    {
                      NEW_LED_KM_OFF;
                    }
             }
             break;
               
            default:
                if(!servei.mask_vd && (servei.vd[gt.mitad_vd]&0xf0)==VD1_VEL)
                    DISPLAY_VD1(VD1_VEL);
                break;
           }

       
         if(tarcom.veltop)  
            {
             if(velocidad > tarcom.veltop)
                {
			      if(!exces_vel && !lap_on)
				  {
				    DISPLAY_ERROR_PLUS_LAP(ERR_EXCES_VEL);
				    totalitzador_incr(TOT_E6);
				    exces_vel=1;
				  }
				  if(estaContant){
					  for(ii=0; ii<MAX_PSGRS ; ii++)
						{
						   if(servcom.viaj_activo[ii] && serv[ii].TARIFANDO)
						   {
							   serv[ii].CUENTA_K=0;
							   if(tarcom.exces_vel_no_horaria)
									   serv[ii].CUENTA_H=0;
						   }
						 }
				  }
                }
             else if(exces_vel && (velocidad <= tarcom.veltop_fin_err))
                {
                  exces_vel=0;
				  if(estaContant){
					  for(ii=0; ii<MAX_PSGRS ; ii++)
						{
						   if(servcom.viaj_activo[ii] && serv[ii].TARIFANDO)
						   {
							 serv[ii].CUENTA_K=serv[ii].NEW_CUENTA_K;
							 serv[ii].CUENTA_H=serv[ii].NEW_CUENTA_H;
							 FIN_LAPSUS();
						   }
						 }
				  }
                }
            }
        }
//                                                                 *
//  final gestio velocitat instantanea i maxima                    *
//******************************************************************

// *************************************************************
// gestion cola metros fisicos                                 *
//                                                             *

     while(COLA_MF && !event)
        {
         decr_COLA_MF();
         tx30.cnt_sec_treure_tensio = tarcom.sec_treure_tensio;   // per treure tensio (versio 1.03G)
         if(tarcom.torn_continu == 0)
         {
             if( tarcom.sanciona_moviment &&
                ((bloqueo_torn.estat_torn == ETORN_INICIAT) 
                 || (bloqueo_torn.estat_torn == ETORN_NO_INI)) // cas  no hi ha franquicia distancia
               &&(tx30.estat==E_OFF )
                && (bloqueo_torn.dist_off < tarcom.impulsos_mogut_en_off))
               bloqueo_torn.dist_off++;  //per veure si esta completament parat
         }
         if(!(--pk_tot_tot_ccab))
           {
            pk_tot_tot_ccab=tarcom.pmpk0_ccab;
            totalitzador_incr(TOT_CCAB);
           }
         
         if(!(--pk_tot_tot))
           {
            // cas produit hm total

            bloqueo_turno_hm();
          
            pk_tot_tot=tarcom.pmpk0;
            totalitzador_incr(TOT_KMT);
            totalitzador_incr(TOT_KMT_P);

            if( habia_error_genimp && !tarcom.genimp_ocup )   
               {
                 //  per recuperar fallo genimp 
                  if(tipoTx == TX50)
                  {
                     if(IOPORT2_PD_bit.no11 == 0)
                     {
                      if((--habia_error_genimp)==0)
                        {
                         event=V_GENIMP;
                         continue;   //  per transicio cas lliure recuperar error 
                        }
                     }
                  }
                  else
                  {
                    // TX40
                    
                    // falta !!!!!!!!
                  }
               }
           }
         switch(tx30.estat)   //  per totalitzadors distancies 
           {
            unsigned char  naux;
			case E_OFF:
		    case E_CERRADO:
				if(!(--pk_off_tot)){
					pk_off_tot=tarcom.pmpk0;
					totalitzador_incr(TOT_KMOFF);
					totalitzador_incr(TOT_KMOFF_P);
					if(tx30.estat == E_OFF)
						servei.hm_fins_fi_torn++;
				}
				break;
            case E_OCUPADO:
            case E_PAGAR:
                if(!(--pk_oc_tot))
                  {
                   pk_oc_tot=tarcom.pmpk0;
                   totalitzador_incr(TOT_KMO);
                   totalitzador_incr(TOT_KMO_P);
                  }
                if(!(--servcom.pk_oc_serv))
                  {
                   servcom.pk_oc_serv=tarcom.pmpk0;
                    for(ii=0; ii<MAX_PSGRS; ii++)
                      {
                       if(servcom.viaj_activo[ii])
                         {
                           serv[ii].hm_oc++;
                           serv[ii].per_tarifa[serv[ii].tarifa].hm_oc++; 
                           serv[ii].per_tarifa[serv[ii].tarifa].parcial_dist++;
                           //servei.hm_oc_tarifa=(unsigned long)parcial_dist[servei.tarifa]; // long per display
                           calcul_toptar_dist(serv[ii].tarifa,serv[ii].per_tarifa[serv[ii].tarifa].parcial_dist,&serv[ii]);
                           if(pasj)
                              serv[ii].ninot=max(serv[ii].ninot,pasj);  //  posa ninot 
                           aux=tarifa_canvi_autom(serv[ii].tarifa,AUT_DIST, &serv[ii]);
                           if(aux != serv[ii].tarifa)
                              cambio_tarifa(aux,tx30.estat, &serv[ii]);
                           else  //  si hi ha canvi ja s'ha fet 
                             {
                              naux=servei.vd[gt.mitad_vd];
                              if(!servei.mask_vd)
                                {
                              if((naux& 0x0f)==VD2_KOC)
                                  DISPLAY_VD2(VD2_KOC);
                              if((naux&0xf0)==VD1_KOC)
                                  DISPLAY_VD1(VD1_KOC);
                                }
                             }
                         }
                      }
                   if(servei.es_precio_fijo &&(serv[0].hm_oc==tarcom.hm_precio_fijo))
                     {
                      // para intermitencia display extres i posa blanking a import
                      DISP_FIXE(2);
                      display(1,NF_IMPORT,(unsigned char*)&serv[0].import);
                     }
                  }
                break;
            default:
				if(!(--pk_lib_tot))
				{
					pk_lib_tot=tarcom.pmpk0;
					totalitzador_incr(TOT_KML);
					totalitzador_incr(TOT_KML_P);
				}
                if(!(--servcom.pk_lib_serv))
                  {
                   servcom.pk_lib_serv=tarcom.pmpk0;
                   servei.hm_libre++;
				   servei.hm_fins_fi_torn++;
                   if(!pasj)
                      servei.pasj_ant=0;
                  }
                if(pasj)
                  {
                   if(!(--servcom.pk_lib_pasj))
                     {
                      servcom.pk_lib_pasj=tarcom.pmpk0;
                      servei.hm_libre_pasj++;
                      if(!servei.pasj_ant)
                         servei.nsent++;
                      servei.pasj_ant=1;
                     }
                   if(!(--pk_lpasj_tot))
                     {
                      pk_lpasj_tot=tarcom.pmpk0;
                      totalitzador_incr(TOT_KMLP);
                      totalitzador_incr(TOT_KMLP_P);
                     }
                  }
                break;
           }
         switch(tx30.estat)   //  per transicions per distancia 
           {
            case E_LIBRE:
            case E_OFF:
                if(timer50 && (velocidad >= 5))        //   si  espera pas a off 
                   {
                    event=V_MF;
                    continue;      //  per executar semantica 
                   }

                if(tarcom.hay_liocaut && pasj)
                   {
                    if(++servei.m_libpas>tarcom.m_liocaut_fk)
                       {
                        event=V_PASAUT;
                        continue;      //  per executar pas a Ocup per metres 
                       }
                   }
                break;
            case E_PAGAR:
                if(serv[viaj].suma_en_curs || servei.estat_euro)    //  si hi ha suma en curs 
                  {
                   event=V_MF;
                   continue;      //  per executar semantica 
                  }
                if(tarcom.OP_APOCMF)
                  {
                   event=V_APOCMF;
                   continue;      //  per executar semantica 
                  }
                if(tarcom.hay_paglibaut==2 ||(tarcom.hay_paglibaut==1&& pasj))
                   {
                    if(++servei.m_paglib>tarcom.m_paglibaut_fk)
                       {
                        event=V_PASAUT;
                        continue;      //  per executar pas a Lliure per metres 
                       }
                   }
                break;
           case E_FISERV:
             if(tarcom.hay_fslibdist)
             {
               if(++servei.m_fslib > tarcom.m_fslibdist_fk)
                   {
                    event=V_PASAUT;
                    continue;      //  per executar pas a Lliure per metres 
                   }
             }
             break;
            case E_RELOJ:
            case E_TEST:
            case E_TOTS:
            case E_CODS:
            case E_MODREL:
            case E_MODFP:
            case E_PARMS:
                event=V_MF;
                continue;      //  per executar semantica 
            default:
                break;
           }
        }    // final while(COLA_MF && !event)
     if(event)
       continue;
//                                                             *
// final gestion cola metros fisicos                           *
// *************************************************************

     if (timer50==1)
        {
         timer50=0;
         event=V_TEMPOR;  //  tecla temps  
         continue;
        }
     if (timer100==1)
        {
         timer100=0;
         event=V_TEMPOR_1;  //  tecla temps
         continue;
        }
     if (timer_2==1)
        {
         timer_2=0;
         event=V_TEMPOR_2;  //  tecla temps  
         continue;
        }

     event = get_event_teclat(0,&tecla_num);
     if(event)
     {
		 pausa.tecla_polsada = 1;
       continue;    // per executar semantica
     }
     
     
     if(loop_40ms == 0)   // per nomes fer aquesta part cada 40ms.
     {
       loop_40ms = 40;
         sensors = llegir_sensors();
         
         if(tarcom.robo)
            {
             if(ROBO && !(sensors & SENSORS_PISON))
               {
                // cas s'ha acabat el robo restaura llums 
                sext_luz = sensors & (SENSORS_CLAU_CON | SENSORS_LLUM_POS); // no es fa xor amb 3 per que despres ho trobi canviat
               }
             ROBO = (sensors & SENSORS_PISON);
            }
         else
            ROBO=0;

         pasj =(sensors & SENSORS_PASAJERO) != 0;

         lluminos_45ms_loop();

         if((((sensors & (SENSORS_CLAU_CON | SENSORS_LLUM_POS)) ^(SENSORS_CLAU_CON | SENSORS_LLUM_POS)) != sext_luz))
            {
             sext_luz = (sensors & (SENSORS_CLAU_CON | SENSORS_LLUM_POS)) ^ (SENSORS_CLAU_CON | SENSORS_LLUM_POS);
             switch(tx30.estat)
                {
                 case E_OCUPADO:
                 case E_PAGAR:
                    if(tx30.estat==E_OCUPADO)
                      {
                       out_luzex(tarifa[serv[servcom.viaj_per_llums].tarifa].luzocup[sext_luz],tarifa[serv[servcom.viaj_per_llums].tarifa].luzocup[sext_luz+4]);
                      }
                    else
                      {
                       out_luzex(tarifa[serv[servcom.viaj_per_llums].tarifa].luzpagar[sext_luz], tarifa[serv[servcom.viaj_per_llums].tarifa].luzpagar[sext_luz+4]);
                      }
                    break;
                 default:
                    select_luz(tx30.estat,buf_luz);
                    out_luzex(buf_luz[0],buf_luz[1]);
                    break;
                }
            }

// **************************************************************
// canvi tarifa per temps
// **************************************************************
          for(ii=0; ii<MAX_PSGRS ; ii++)
            {
               if(servcom.viaj_activo[ii] )
               {
                p_serv = &serv[ii];
                if(p_serv->TARIFANDO)
                  {
                    if(gt.canvi_minut || p_serv->canvi_minut_ocupat)
                      {
						if(!p_serv->PRIMSAL_H || tarcom.no_canvis_primer_salt==0)
						  {
							aux=tarifa_canvi_autom(p_serv->tarifa,AUT_ZONH | AUT_TEMPS, p_serv);
                       		if(aux != p_serv->tarifa)
                           		cambio_tarifa(aux,tx30.estat, p_serv);
                       		p_serv->canvi_minut_ocupat = 0;
						  }
                      }
                  }
               }
            }

         gt.canvi_minut=0;

         if((sensors & SENSORS_PISON) != sext)
           {
            sext=sensors & SENSORS_PISON; /* l'actualitza encara que no estigui en
                                    tarifacio per propera vegada(estat
                                    inicial per canvi AUT_ALL_INICIO al pasar a
                                    ocupat */

            switch(tx30.estat)
                {
                 case E_OCUPADO:
                 case E_PAGAR:
                    if(viaj == 0)
                    {
                    aux=tarifa_canvi_autom(serv[0].tarifa,AUT_EXT, &serv[0]);
                    if(aux != serv[0].tarifa)
                       cambio_tarifa(aux,tx30.estat, &serv[0]);
                    }
                    break;
                 default:
                    break;
                }
           }        
        }   // final part que es fa cada 40ms.


     // per processar canal multiplexat
     multiplex_retrieve_missatge();
     


    // recupera transmissio per impre
     if(multiplex.msg_prn.proces_en_curs)
     {
       if(tarcom.tck_no_legal)
          imprimir_ticket_no_legal(multiplex.msg_prn.buf,multiplex.msg_prn.nc_rebuts);
       else
           if(multiplex.msg_prn.nc_rebuts>0 && multiplex.msg_prn.nc_rebuts<1024)
                OS_Q_Put(&ColaImpre,multiplex.msg_prn.buf,multiplex.msg_prn.nc_rebuts);
       multiplex.msg_prn.proces_en_curs = 0;
     }

    
     // cas transmissio amb terminal ( si n'hi ha)
     event = td30_get_transm(); 
     if(event)
        continue;  // a executar transicio             


     if(tx30.estat==E_LIBRE && tarcom.OP_LOCKT)
       {
        if(!(sensors & SENSORS_CLAU_CON))
           {
            event=V_BLOQ;
            continue;
           }
       }
     if(tx30.estat==E_OFF && (MASK_GEN & MSK_BLQ))
       {
        if(sensors & SENSORS_CLAU_CON)
           {
            event=V_BLOQ;
            continue;
           }
       }
     
//  opcion grecia apagado disp
     if((sensors & SENSORS_CLAU_CON) != sensors_ant)
        {
         sensors_ant = sensors & SENSORS_CLAU_CON;
         if(tarcom.OP_LLAVEOFF)      
             {
              if(sensors_ant)
                {
                 REFRESH_ON();
                }
              else
                {
                 REFRESH_OFF();
                }
             }
        }
 
     if((tx30.estat==E_OFF) && tarcom.OP_LIB_E2)
       {
        if((sensors & SENSORS_CLAU_CON)
        && (bloqueo_torn.stdesblq == 0))  // si esta temporitzant, ja no repeteix
           {
            event=V_SEN2;
            continue;
           }
       }
     
//     
// deteccio final impressio ticket
//     

     if(tx30.event_impre)
     {
      event = V_TR_IN;
      if(tx30.event_impre_ok)
          event_tr = TR_OK;
      else
          event_tr = TR_MAL;
      tx30.event_impre = 0;
      continue;     // a executar transicio
     }

     OS_Delay(MS_LOOP_TX30);
 
  }   //while(1);
}








#if 0
volatile unsigned int i_loop;
unsigned int num_segs;


void task_background(void)
{
unsigned long l_seg;
	br_ini();

 
    for(num_segs=0;;num_segs++)
      {
       no_operation();
       for(l_seg=0;l_seg<91700L;l_seg++)
         i_loop++;
       no_operation();

      }
}						   

#endif


 



