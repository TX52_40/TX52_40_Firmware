//*******************************************************************
// hard.h
//*******************************************************************
#ifndef __hard_H
#define __hard_H

// aquestes definicions provenen de RTOSINIT_STR71x.c
/****** Enhanced interrupt controller interrupt sources *************/

#define T0_EFTI_VECT_ID    0  // IRQ0  T0.EFTI Timer 0 global interrupt 5
#define FLASH_VECT_ID      1  // IRQ1  FLASH FLASH global interrupt
#define PRCCU_VECT_ID      2  // IRQ2  PRCCU PRCCU global interrupt
#define RTC_VECT_ID        3  // IRQ3  RTC Real Time Clock global interrupt 2
#define WDG_VECT_ID        4  // IRQ4  WDG.IRQ Watchdog timer interrupt 1
#define XTI_VECT_ID        5  // IRQ5  XTI.IRQ XTI external interrupt 16
#define USB_VECT_ID        6  // IRQ6  USB.HPIRQ USB high priority event interrupt 0-7
#define I2C0_ERR_VECT_ID   7  // IRQ7  I2C0.ITERR I2C 0 error interrupt
#define I2C1_ERR_VECT_ID   8  // IRQ8  I2C1.ITERR I2C 1 error interrupt
#define UART0_VECT_ID      9  // IRQ9  UART0.IRQ UART 0 global interrupt 9
#define UART1_VECT_ID     10  // IRQ10 UART1.IRQ UART 1 global interrupt 9
#define UART2_VECT_ID     11  // IRQ11 UART2.IRQ UART 2 global interrupt 9
#define UART3_VECT_ID     12  // IRQ12 UART3.IRQ UART 3 global interrupt 9
#define SPI0_VECT_ID      13  // IRQ13 SPI0.IRQ BSPI 0 global interrupt 5
#define SPI1_VECT_ID      14  // IRQ14 SPI1.IRQ BSPI 1 global interrupt 5
#define I2C0_VECT_ID      15  // IRQ15 I2C0.gobal interrupt
#define I2C1_VECT_ID      16  // IRQ16 I2C1.gobal interrupt
#define CAN_VECT_ID       17  // IRQ17 CAN.gobal interrupt
#define ADC_VECT_ID       18  // IRQ18 ADC.IRQ ADC sample ready interrupt 1
#define T1_GI_VECT_ID     19  // IRQ19 T1.GI Timer 1 global interrupt 5
#define T2_GI_VECT_ID     20  // IRQ20 T2.GI Timer 2 global interrupt 5
#define T3_GI_VECT_ID     21  // IRQ21 T3.GI Timer 3 global interrupt 5
#define Reserved1_VECT_ID 22  // IRQ22 Reserved
#define Reserved2_VECT_ID 23  // IRQ23 Reserved
#define Reserved3_VECT_ID 24  // IRQ24 Reserved
#define HDLC_IR_VECT_ID   25  // IRQ25 HDLC.IRQ HDLC global interrupt
#define USB_LPI_VECT_ID   26  // IRQ26 USB.LPIRQ USB low priority event interrupt 7-15
#define Reserved4_VECT_ID 27  // IRQ27 Reserved
#define Reserved5_VECT_ID 28  // IRQ28 Reserved
#define T0_TOI_VECT_ID    29  // IRQ29 T0.TOI Timer 0 Overflow interrupt 1
#define T0_OC1_VECT_ID    30  // IRQ30 T0.OC1 Timer 0 Output Compare 1 interrupt 1
#define T0_OC2_VECT_ID    31  // IRQ31 T0.OC2 Timer 0 Output Compare 2 interrupt 1
#ifndef NUM_INT_SOURCES
  #define NUM_INT_SOURCES  32
#endif



// exemple accedir a port 1 bit 12 IOPORT1_PD_bit.no12


#define IOPORT_3 *(unsigned char*)0x66200000
#define IOPORT_4 *(unsigned char*)0x66200001
#define IOPORT_5 *(unsigned char*)0x66200002
#define IOPORT_6 *(unsigned char*)0x66200003

extern unsigned char PORT3;
extern unsigned char PORT4;
extern unsigned char PORT5;
extern unsigned char PORT6;



#define TX50 1
#define TX40 2

extern int tipoTx;

extern void hard_detect(void);
extern void hard_init(void);
extern int fallo_tension(void);

extern void salta_a_reset(void);

#endif
