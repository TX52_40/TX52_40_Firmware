// **************************************************************
// rutines.c
// **************************************************************

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "rutines.h"

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

const unsigned char kdef[8]={35,68,243,44,60,213,212,141};
unsigned char xk[17][8];

union uk
{
  unsigned char ck[8];
  unsigned long lk[2];
} sk;

unsigned char de[4], dd[8], xr[8], sde[4], iz[4], niz[4];

const unsigned char tsk[]={1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1};

const unsigned char table[512]={
      14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7,
       0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8,
       4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0,
      15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13,

      15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10,
       3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5,
       0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15,
      13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9,

      10, 0, 9,14, 6, 3,15, 5, 1,13,12, 7,11, 4, 2, 8,
      13, 7, 0, 9, 3, 4, 6,10, 2, 8, 5,14,12,11,15, 1,
      13, 6, 4, 9, 8,15, 3, 0,11, 1, 2,12, 5,10,14, 7,
       1,10,13, 0, 6, 9, 8, 7, 4,15,14, 3,11, 5, 2,12,

       7,13,14, 3, 0, 6, 9,10, 1, 2, 8, 5,11,12, 4,15,
      13, 8,11, 5, 6,15, 0, 3, 4, 7, 2,12, 1,10,14, 9,
      10, 6, 9, 0,12,11, 7,13,15, 1, 3,14, 5, 2, 8, 4,
       3,15, 0, 6,10, 1,13, 8, 9, 4, 5,11,12, 7, 2,14,

       2,12, 4, 1, 7,10,11, 6, 8, 5, 3,15,13, 0,14, 9,
      14,11, 2,12, 4, 7,13, 1, 5, 0,15,10, 3, 9, 8, 6,
       4, 2, 1,11,10,13, 7, 8,15, 9,12, 5, 6, 3, 0,14,
      11, 8,12, 7, 1,14, 2,13, 6,15, 0, 9,10, 4, 5, 3,

      12, 1,10,15, 9, 2, 6, 8, 0,13, 3, 4,14, 7, 5,11,
      10,15, 4, 2, 7,12, 9, 5, 6, 1,13,14, 0,11, 3, 8,
       9,14,15, 5, 2, 8,12, 3, 7, 0, 4,10, 1,13,11, 6,
       4, 3, 2,12, 9, 5,15,10,11,14, 1, 7, 6, 0, 8,13,

       4,11, 2,14,15, 0, 8,13, 3,12, 9, 7, 5,10, 6, 1,
      13, 0,11, 7, 4, 9, 1,10,14, 3, 5,12, 2,15, 8, 6,
       1, 4,11,13,12, 3, 7,14,10,15, 6, 8, 0, 5, 9, 2,
       6,11,13, 8, 1, 4,10, 7, 9, 5, 0,15,14, 2, 3,12,

      13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7,
       1,15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2,
       7,11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8,
       2, 1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11
   };

// tablas para permutación incial, directa e inversa, del dato
const unsigned char ip[64]={57,49,41,33,25,17, 9, 1,
							59,51,43,35,27,19,11, 3,
							61,53,45,37,29,21,13, 5,
							63,55,47,39,31,23,15, 7,
							56,48,40,32,24,16, 8, 0,
							58,50,42,34,26,18,10, 2,
							60,52,44,36,28,20,12, 4,
							62,54,46,38,30,22,14, 6}; 

const unsigned char ip_inv[64]={39, 7,47,15,55,23,63,31,
								38, 6,46,14,54,22,62,30,
								37, 5,45,13,53,21,61,29,
								36, 4,44,12,52,20,60,28,
								35, 3,43,11,51,19,59,27,
								34, 2,42,10,50,18,58,26,
								33, 1,41, 9,49,17,57,25,
								32, 0,40, 8,48,16,56,24}; 

// tablas para permuted choice 1

const unsigned char pch1_C[28]={56,48,40,32,24,16, 8,
								 0,57,49,41,33,25,17,
								 9, 1,58,50,42,34,26,
								18,10, 2,59,51,43,35};
const unsigned char pch1_D[28]={62,54,46,38,30,22,14,
								 6,61,53,45,37,29,21,
								13, 5,60,52,44,36,28,
								20,12, 4,27,19,11, 3};

// tablas para permuted choice 2
const unsigned char pch2[48]=  {13,16,10,23, 0, 4,
								 2,27,14, 5,20, 9,
								22,18,11, 3,25, 7,
								15, 6,26,19,12, 1,
								40,51,30,36,46,54,
								29,39,50,44,32,47,
								43,48,38,55,33,52,
								45,41,49,35,28,31}; 

// tablas para Expansion del dato de 32 bits a 48
const unsigned char Expan[48]= {31, 0, 1, 2, 3, 4,
								 3, 4, 5, 6, 7, 8,
								 7, 8, 9,10,11,12,
								11,12,13,14,15,16,
								15,16,17,18,19,20,
								19,20,21,22,23,24,
								23,24,25,26,27,28,
								27,28,29,30,31, 0}; 

// tablas para Permutacion del dato de 32 bits a 32
const unsigned char tPerm[32]= {15, 6,19,20, 
								28,11,27,16, 
								 0,14,22,25,
								 4,17,30, 9,
								 1, 7,23,13,
								31,26, 2, 8,
								18,12,29, 5,
								21,10, 3,24}; 



/******************************************************
*	Funciones de la libreria
******************************************************/
static unsigned short des_completo;
static unsigned short des_ifac;

#define  copiar_bit(org,ib_org,dest,ib_dest) {if((org[ib_org/8])&(0x80>>(ib_org%8)))dest[ib_dest/8]|=(0x80>>(ib_dest%8));}


CODE_FAST void rotate_right_28bits(unsigned char* orig)
{
short i;
unsigned char buftemp[4];
	memcpy(buftemp, orig, 4);
	for(i=0; (i<4); i++)
	{
		orig[i]>>=1;
		orig[i]&=0x7f;
	}
	if(buftemp[0] & 0x01)
		orig[1]|=0x80;
	if(buftemp[1] & 0x01)
		orig[2]|=0x80;
	if(buftemp[2] & 0x01)
		orig[3]|=0x80;
	if(buftemp[3] & 0x10)
		orig[0]|=0x80;
	orig[3]&=0xf0;
}

CODE_FAST void rotate_left_28bits(unsigned char* orig)
{
short i;
unsigned char buftemp[4];
	memcpy(buftemp, orig, 4);
	for(i=0; (i<4); i++)
	{
		orig[i]<<=1;
		orig[i]&=0xfe;
	}
	if(buftemp[0] & 0x80)
		orig[3]|=0x10;
	if(buftemp[1] & 0x80)
		orig[0]|=0x01;
	if(buftemp[2] & 0x80)
		orig[1]|=0x01;
	if(buftemp[3] & 0x80)
		orig[2]|=0x01;
	orig[3]&=0xf0;
}

CODE_FAST void cal_k(short vez, char id)
{
char n;
unsigned char clave56[8];
unsigned char i;
unsigned char* pxk;
	if(id=='d')
		n=tsk[(des_completo?15:5)-vez];
	else
		n=tsk[vez];
	do
	{
		n--;
		switch (id)
		{
			case 'd':
				if(!des_ifac)
				{
					rotate_right_28bits(&sk.ck[0]);
					rotate_right_28bits(&sk.ck[4]);
				}
				else
				{
					if (sk.lk[0] & 0x1) sk.lk[0] |= 0x10000000;
					if (sk.lk[1] & 0x1) sk.lk[1] |= 0x10000000;
					sk.lk[0]=sk.lk[0]>>1;  sk.lk[1]=sk.lk[1]>>1;
				}
				break;
			case 'i':
				if(!des_ifac)
				{
					rotate_left_28bits(&sk.ck[0]);
					rotate_left_28bits(&sk.ck[4]);
				}
				else
				{
					sk.lk[0]=sk.lk[0]<<1;  sk.lk[1]=sk.lk[1]<<1;
					if (sk.lk[0] & 0x10000000) sk.lk[0] |= 1;
					if (sk.lk[1] & 0x10000000) sk.lk[1] |= 1;
				}
				break;
			default:
				break;
		}
	}while(n>0);
	if(id=='d')
		vez++;
	pxk=&(xk[vez][0]);
	memset(pxk,0,8);
	if(!des_ifac)
	{// permuted choice 2
		memcpy(clave56, &sk.ck[0], 8);
		rotate_left_28bits(&clave56[4]);
		rotate_left_28bits(&clave56[4]);
		rotate_left_28bits(&clave56[4]);
		rotate_left_28bits(&clave56[4]);
		clave56[3]|=(clave56[7]>>4);
		for(i=0;i<48;i++)
			copiar_bit(clave56,pch2[i],pxk,i);
	}
	else
	{
		if (sk.ck[0] & 0x01) xk[vez][0] |= 0x10;  /*  1  5 */
		if (sk.ck[0] & 0x02) xk[vez][3] |= 0x20;  /*  2 24 */
		if (sk.ck[0] & 0x04) xk[vez][1] |= 0x01;  /*  3  7 */
		if (sk.ck[0] & 0x08) xk[vez][2] |= 0x08;  /*  4 16 */
		if (sk.ck[0] & 0x10) xk[vez][0] |= 0x20;  /*  5  6 */
		if (sk.ck[0] & 0x20) xk[vez][1] |= 0x08;  /*  6 10 */
		if (sk.ck[0] & 0x40) xk[vez][3] |= 0x02;  /*  7 20 */
		if (sk.ck[0] & 0x80) xk[vez][2] |= 0x20;  /*  8 18 */
		if (sk.ck[1] & 0x02) xk[vez][1] |= 0x20;  /* 10 12 */
		if (sk.ck[1] & 0x04) xk[vez][0] |= 0x04;  /* 11  3 */
		if (sk.ck[1] & 0x08) xk[vez][2] |= 0x04;  /* 12 15 */
		if (sk.ck[1] & 0x10) xk[vez][3] |= 0x10;  /* 13 23 */
		if (sk.ck[1] & 0x20) xk[vez][0] |= 0x01;  /* 14  1 */
		if (sk.ck[1] & 0x40) xk[vez][1] |= 0x04;  /* 15  9 */
		if (sk.ck[1] & 0x80) xk[vez][3] |= 0x01;  /* 16 19 */
		if (sk.ck[2] & 0x01) xk[vez][0] |= 0x02;  /* 17  2 */
		if (sk.ck[2] & 0x04) xk[vez][2] |= 0x02;  /* 19 14 */
		if (sk.ck[2] & 0x08) xk[vez][3] |= 0x08;  /* 20 22 */
		if (sk.ck[2] & 0x10) xk[vez][1] |= 0x10;  /* 21 11 */
		if (sk.ck[2] & 0x40) xk[vez][2] |= 0x01;  /* 23 13 */
		if (sk.ck[2] & 0x80) xk[vez][0] |= 0x08;  /* 24  4 */
		if (sk.ck[3] & 0x02) xk[vez][2] |= 0x10;  /* 26 17 */
		if (sk.ck[3] & 0x04) xk[vez][3] |= 0x04;  /* 27 21 */
		if (sk.ck[3] & 0x08) xk[vez][1] |= 0x02;  /* 28  8 */
		if (sk.ck[4] & 0x01) xk[vez][7] |= 0x10;  /* 29 47 */
		if (sk.ck[4] & 0x02) xk[vez][5] |= 0x01;  /* 30 31 */
		if (sk.ck[4] & 0x04) xk[vez][4] |= 0x04;  /* 31 27 */
		if (sk.ck[4] & 0x08) xk[vez][7] |= 0x20;  /* 32 48 */
		if (sk.ck[4] & 0x10) xk[vez][5] |= 0x10;  /* 33 35 */
		if (sk.ck[4] & 0x20) xk[vez][6] |= 0x10;  /* 34 41 */
		if (sk.ck[4] & 0x80) xk[vez][7] |= 0x08;  /* 36 46 */
		if (sk.ck[5] & 0x01) xk[vez][4] |= 0x08;  /* 37 28 */
		if (sk.ck[5] & 0x04) xk[vez][6] |= 0x04;  /* 39 39 */
		if (sk.ck[5] & 0x08) xk[vez][5] |= 0x02;  /* 40 32 */
		if (sk.ck[5] & 0x10) xk[vez][4] |= 0x01;  /* 41 25 */
		if (sk.ck[5] & 0x20) xk[vez][7] |= 0x02;  /* 42 44 */
		if (sk.ck[5] & 0x80) xk[vez][6] |= 0x01;  /* 44 37 */
		if (sk.ck[6] & 0x01) xk[vez][5] |= 0x08;  /* 45 34 */
		if (sk.ck[6] & 0x02) xk[vez][7] |= 0x01;  /* 46 43 */
		if (sk.ck[6] & 0x04) xk[vez][4] |= 0x10;  /* 47 29 */
		if (sk.ck[6] & 0x08) xk[vez][5] |= 0x20;  /* 48 36 */
		if (sk.ck[6] & 0x10) xk[vez][6] |= 0x02;  /* 49 38 */
		if (sk.ck[6] & 0x20) xk[vez][7] |= 0x04;  /* 50 45 */
		if (sk.ck[6] & 0x40) xk[vez][5] |= 0x04;  /* 51 33 */
		if (sk.ck[6] & 0x80) xk[vez][4] |= 0x02;  /* 52 26 */
		if (sk.ck[7] & 0x01) xk[vez][6] |= 0x20;  /* 53 42 */
		if (sk.ck[7] & 0x04) xk[vez][4] |= 0x20;  /* 55 30 */
		if (sk.ck[7] & 0x08) xk[vez][6] |= 0x08;  /* 56 40 */
	}
}        

CODE_FAST void Jexp(void)
{
unsigned char i;
	memset(dd,0,8);
	if(!des_ifac)
	{
		for(i=0;i<48;i++)
			copiar_bit(de,Expan[i],dd,i);
	}
	else
	{
   if (de[0] & 0x01) {dd[0] |= 0x02; dd[7] |= 0x20;}  /*  1 */
   if (de[0] & 0x02) dd[0] |= 0x04;                   /*  2 */
   if (de[0] & 0x04) dd[0] |= 0x08;                   /*  3 */
   if (de[0] & 0x08) {dd[0] |= 0x10; dd[1] |= 0x01;}  /*  4 */
   if (de[0] & 0x10) {dd[0] |= 0x20; dd[1] |= 0x02;}  /*  5 */
   if (de[0] & 0x20) dd[1] |= 0x04;                   /*  6 */
   if (de[0] & 0x40) dd[1] |= 0x08;                   /*  7 */
   if (de[0] & 0x80) {dd[1] |= 0x10; dd[2] |= 0x01;}  /*  8 */
   if (de[1] & 0x01) {dd[1] |= 0x20; dd[2] |= 0x02;}  /*  9 */
   if (de[1] & 0x02) dd[2] |= 0x04;                   /*  0 */
   if (de[1] & 0x04) dd[2] |= 0x08;                   /* 11 */
   if (de[1] & 0x08) {dd[2] |= 0x10; dd[3] |= 0x01;}  /* 12 */
   if (de[1] & 0x10) {dd[2] |= 0x20; dd[3] |= 0x02;}  /* 13 */
   if (de[1] & 0x20) dd[3] |= 0x04;                   /* 14 */
   if (de[1] & 0x40) dd[3] |= 0x08;                   /* 15 */
   if (de[1] & 0x80) {dd[3] |= 0x10; dd[4] |= 0x01;}  /* 16 */
   if (de[2] & 0x01) {dd[3] |= 0x20; dd[4] |= 0x02;}  /* 17 */
   if (de[2] & 0x02) dd[4] |= 0x04;                   /* 18 */
   if (de[2] & 0x04) dd[4] |= 0x08;                   /* 19 */
   if (de[2] & 0x08) {dd[4] |= 0x10; dd[5] |= 0x01;}  /* 20 */
   if (de[2] & 0x10) {dd[4] |= 0x20; dd[5] |= 0x02;}  /* 21 */
   if (de[2] & 0x20) dd[5] |= 0x04;                   /* 22 */
   if (de[2] & 0x40) dd[5] |= 0x08;                   /* 23 */
   if (de[2] & 0x80) {dd[5] |= 0x10; dd[6] |= 0x01;}  /* 24 */
   if (de[3] & 0x01) {dd[5] |= 0x20; dd[6] |= 0x02;}  /* 25 */
   if (de[3] & 0x02) dd[6] |= 0x04;                   /* 26 */
   if (de[3] & 0x04) dd[6] |= 0x08;                   /* 27 */
   if (de[3] & 0x08) {dd[6] |= 0x10; dd[7] |= 0x01;}  /* 28 */
   if (de[3] & 0x10) {dd[6] |= 0x20; dd[7] |= 0x02;}  /* 29 */
   if (de[3] & 0x20) dd[7] |= 0x04;                   /* 30 */
   if (de[3] & 0x40) dd[7] |= 0x08;                   /* 31 */
   if (de[3] & 0x80) {dd[7] |= 0x10; dd[0] |= 0x01;}  /* 32 */
	}
}

CODE_FAST void perm(void)
{
unsigned char i;
	memset(de,0,4);
	if(!des_ifac)
	{
		for(i=0;i<32;i++)
			copiar_bit(sde,tPerm[i],de,i);
	}
	else
	{
   if (sde[0] & 0x01) de[1] |= 0x80;  /*  1 16 */
   if (sde[0] & 0x02) de[0] |= 0x40;  /*  2  7 */
   if (sde[0] & 0x04) de[2] |= 0x08;  /*  3 20 */
   if (sde[0] & 0x08) de[2] |= 0x10;  /*  4 21 */
   if (sde[0] & 0x10) de[3] |= 0x10;  /*  5 29 */
   if (sde[0] & 0x20) de[1] |= 0x08;  /*  6 12 */
   if (sde[0] & 0x40) de[3] |= 0x08;  /*  7 28 */
   if (sde[0] & 0x80) de[2] |= 0x01;  /*  8 17 */
   if (sde[1] & 0x01) de[0] |= 0x01;  /*  9  1 */
   if (sde[1] & 0x02) de[1] |= 0x40;  /* 10 15 */
   if (sde[1] & 0x04) de[2] |= 0x40;  /*  1 23 */
   if (sde[1] & 0x08) de[3] |= 0x02;  /*  2 26 */
   if (sde[1] & 0x10) de[0] |= 0x10;  /*  3  5 */
   if (sde[1] & 0x20) de[2] |= 0x02;  /*  4 18 */
   if (sde[1] & 0x40) de[3] |= 0x40;  /*  5 31 */
   if (sde[1] & 0x80) de[1] |= 0x02;  /*  6 10 */
   if (sde[2] & 0x01) de[0] |= 0x02;  /*  7  2 */
   if (sde[2] & 0x02) de[0] |= 0x80;  /*  8  8 */
   if (sde[2] & 0x04) de[2] |= 0x80;  /*  9 24 */
   if (sde[2] & 0x08) de[1] |= 0x20;  /* 20 14 */
   if (sde[2] & 0x10) de[3] |= 0x80;  /*  1 32 */
   if (sde[2] & 0x20) de[3] |= 0x04;  /*  2 27 */
   if (sde[2] & 0x40) de[0] |= 0x04;  /*  3  3 */
   if (sde[2] & 0x80) de[1] |= 0x01;  /*  4  9 */
   if (sde[3] & 0x01) de[2] |= 0x04;  /*  5 19 */
   if (sde[3] & 0x02) de[1] |= 0x10;  /*  6 13 */
   if (sde[3] & 0x04) de[3] |= 0x20;  /*  7 30 */
   if (sde[3] & 0x08) de[0] |= 0x20;  /*  8  6 */
   if (sde[3] & 0x10) de[2] |= 0x20;  /*  9 22 */
   if (sde[3] & 0x20) de[1] |= 0x04;  /* 30 11 */
   if (sde[3] & 0x40) de[0] |= 0x08;  /*  1  4 */
   if (sde[3] & 0x80) de[3] |= 0x01;  /*  2 25 */
	}
}

CODE_FAST unsigned char get_6bits(unsigned char* orig, unsigned char index)
{
unsigned char ch;
	switch(index)
	{
		case 0:
			ch=orig[0];
			break;
		case 1:
			ch=(orig[0]<<6) | ((orig[1]>>2) & 0x3c);
			break;
		case 2:
			ch=(orig[1]<<4) | ((orig[2]>>4) & 0x0c);
			break;
		case 3:
			ch=orig[2]<<2;
			break;
		case 4:
			ch=orig[3];
			break;
		case 5:
			ch=(orig[3]<<6) | ((orig[4]>>2) & 0x3c);
			break;
		case 6:
			ch=(orig[4]<<4) | ((orig[5]>>4) & 0x0c);
			break;
		case 7:
			ch=orig[5]<<2;
			break;
	}
	return (ch & 0xfc);
}

CODE_FAST void cajas(void)
{
unsigned char i,py,px,out,ch;
short ptabla;
	out=0;
	memset(sde,0,4);  
	if(!des_ifac)
	{
		for(i=0; i<8; i++)
		{
			py=0;
			ch=get_6bits(xr,i);
			if(ch & 0x80)
				py = py | 2;
			if(ch & 0x04)
				py = py | 1;
			px = (ch & 0x78)>>3;
			px&=0x0f;
			ptabla = px + (py * 16) + (i * 64);
			sde[out]=(table[ptabla]<<4);
			i++; 
			py=0;
			ch=get_6bits(xr,i);
			if(ch & 0x80)
				py = py | 2;
			if(ch & 0x04)
				py = py | 1;
			px = (ch & 0x78)>>3;
			px&=0x0f;
			ptabla = px + (py * 16) + (i * 64);
			sde[out]|=table[ptabla]; 
			out++;
		}
	}
	else
	{
		for (i=0; i<8; i++)
		{
			py=0;
			if (xr[i] & 0x01) py = py | 1;
			if (xr[i] & 0x20) py = py | 2;
			px = (xr[i] & 0x1e)>>1;
			ptabla = px + (py * 16) + (i * 64);
			sde[out]=sde[out] | table[ptabla];
			i++; 
			py=0;
			if (xr[i] & 0x01) py = py | 1;
			if (xr[i] & 0x20) py = py | 2;
			px = (xr[i] & 0x1e)>>1;
			ptabla = px + (py * 16) + (i * 64);
			sde[out]=sde[out] | (table[ptabla]<<4); out++;
		}
	}
}

CODE_FAST void clave_64_to_56(unsigned char* origen)
{
unsigned char i;				
	if(des_ifac)
	{// se eliminan los MSB de cada byte
		sk.lk[0]=sk.lk[1]=0L;
		sk.lk[0]=origen[0] & 0x7f;
		sk.lk[0] |= ( ((unsigned long) (origen[1] & 0x7f)) << 7);
		sk.lk[0] |= ( ((unsigned long) (origen[2] & 0x7f)) << 14);
		sk.lk[0] |= ( ((unsigned long) (origen[3] & 0x7f)) << 21);
		sk.lk[1]=origen[4] & 0x7f;
		sk.lk[1] |= ( ((unsigned long) (origen[5] & 0x7f)) << 7);
		sk.lk[1] |= ( ((unsigned long) (origen[6] & 0x7f)) << 14);
		sk.lk[1] |= ( ((unsigned long) (origen[7] & 0x7f)) << 21);
	}
	else
	{// permuted choice 1
		memset(&sk.ck[0],0,8);
		for(i=0;i<28;i++)
			copiar_bit(origen,pch1_C[i],(&sk.ck[0]),i);
		for(i=0;i<28;i++)
			copiar_bit(origen,pch1_D[i],(&sk.ck[4]),i);
	}
}

CODE_FAST void data_initial_permutation(unsigned char* bufer, short invers)
{
unsigned char i;				
unsigned char buftemp[8];
	if(des_ifac)
		return;
	memcpy(buftemp, bufer, 8);
	memset(bufer,0,8);
	if(invers)
	{
		for(i=0;i<64;i++)
			copiar_bit(buftemp,ip_inv[i],bufer,i);
	}
	else
	{
		for(i=0;i<64;i++)
			copiar_bit(buftemp,ip[i],bufer,i);
	}
}

unsigned char hay_clave_anterior_dec;
unsigned char hay_clave_anterior_enc;
unsigned char clave_anterior_decripta[8];
unsigned char clave_anterior_encripta[8];
unsigned short memo_des_completo;
unsigned short memo_des_ifac;


CODE_FAST void decripta_generico(unsigned char *bufer, unsigned char* clave)
{
unsigned short cas, i;
unsigned short clave_repetida=0;
	if(hay_clave_anterior_dec==69)
	{
		if(	(memcmp(clave_anterior_decripta, clave, 8)==0) &&
			(memo_des_completo==des_completo) && 
			(memo_des_ifac==des_ifac) )
			clave_repetida=1;
	}
	if(!clave_repetida)
	{
		clave_64_to_56(clave);// de clave[64] obtiene sk[56]
		cal_k(0,'n');
		memcpy(clave_anterior_decripta, clave, 8);
		hay_clave_anterior_dec=69;
		hay_clave_anterior_enc=0;
		memo_des_completo=des_completo;
		memo_des_ifac=des_ifac;
	}
	data_initial_permutation(bufer,0);
	if(!des_ifac)
	{
		memcpy(iz,bufer,4);
		memcpy(de,bufer+4,4);
	}
	else
	{
		memcpy(de,bufer,4);
		memcpy(iz,bufer+4,4);
	}
	for(cas=0; cas<(unsigned short)(des_completo?16:6); cas++)
	{
		memcpy(niz,de,4);
		Jexp();
		for(i=0;i<(unsigned short)(des_ifac?8:6);i++) 
			xr[i]=xk[cas][i]^dd[i];
		cajas();
		if(!clave_repetida)
			cal_k(cas,'d');
		perm();
		for(i=0;i<4;i++)
			de[i]=de[i]^iz[i];
		memcpy(iz,niz,4);
	}
    memcpy(bufer,de,4);  memcpy(bufer+4,iz,4);
	data_initial_permutation(bufer,1);
}

void decripta_tx(unsigned char *bufer)
{
static const unsigned char kd_tx[8]={225,58,16,189,117,80,162,37};
unsigned char ke[8];
	des_ifac=1;
	des_completo=0;
	memcpy(ke, kd_tx, 8);
	decripta_generico(bufer,ke);
}


void memncpy(char* dest, char* src, int len)
{
	memcpy(dest, src, len);
	dest[len]='\0';
}

static const  char _EBCDIC_ASCII[]=
{
0xF0,0x30, // '0'
0xF1,0x31, // '1'
0xF2,0x32, // '2'
0xF3,0x33, // '3'
0xF4,0x34, // '4'
0xF5,0x35, // '5'
0xF6,0x36, // '6'
0xF7,0x37, // '7'
0xF8,0x38, // '8'
0xF9,0x39, // '9'

0x40,0x20, // ' '
0x4B,0x2E, // '.'
0x4C,0x3C, // '<'
0x4D,0x28, // '('
0x4E,0x2B, // '+'
0x4F,0x7C, // '|'
0x50,0x26, // '&'
0x5A,0x21, // '!'
0x5B,0x24, // '$'
0x5C,0x2A, // '*'
0x5D,0x29, // ')'
0x5E,0x3B, // ';'
0x60,0x2D, // '-'
0x61,0x2F, // '/'
0x6B,0x2C, // ','
0x6C,0x25, // '%'
0x6D,0x5F, // '_'
0x6E,0x3E, // '>'
0x6F,0x3F, // '?'

0x7A,0x3A, // ':'
0x7B,0x23, // '#'
0x7C,0x40, // '@'
0x7D,0x27, // '''
0x7E,0x3D, // '='
0x7F,0x22, // '"'

0xC1,0x41, // 'A'
0xC2,0x42, // 'B'
0xC3,0x43, // 'C'
0xC4,0x44, // 'D'
0xC5,0x45, // 'E'
0xC6,0x46, // 'F'
0xC7,0x47, // 'G'
0xC8,0x48, // 'H'
0xC9,0x49, // 'I'

0xD1,0x4A, // 'J'
0xD2,0x4B, // 'K'
0xD3,0x4C, // 'L'
0xD4,0x4D, // 'M'
0xD5,0x4E, // 'N'
0xD6,0x4F, // 'O'
0xD7,0x50, // 'P'
0xD8,0x51, // 'Q'
0xD9,0x52, // 'R'

0xE2,0x53, // 'S'
0xE3,0x54, // 'T'
0xE4,0x55, // 'U'
0xE5,0x56, // 'V'
0xE6,0x57, // 'W'
0xE7,0x58, // 'X'
0xE8,0x59, // 'Y'
0xE9,0x5A, // 'Z'

0x81,0x61, // 'a'
0x82,0x62, // 'b'
0x83,0x63, // 'c'
0x84,0x64, // 'd'
0x85,0x65, // 'e'
0x86,0x66, // 'f'
0x87,0x67, // 'g'
0x88,0x68, // 'h'
0x89,0x69, // 'i'
          
0x91,0x6a, // 'j'
0x92,0x6b, // 'k'
0x93,0x6c, // 'l'
0x94,0x6d, // 'm'
0x95,0x6e, // 'n'
0x96,0x6f, // 'o'
0x97,0x70, // 'p'
0x98,0x71, // 'q'
0x99,0x72, // 'r'

0xA2,0x73, // 's'
0xA3,0x74, // 't'
0xA4,0x75, // 'u'
0xA5,0x76, // 'v'
0xA6,0x77, // 'w'
0xA7,0x78, // 'x'
0xA8,0x79, // 'y'
0xA9,0x7A, // 'z'

0x00,0x00 
};

const unsigned short mtab[256]=
{
 0x0000, 0xc1c0, 0x81c1, 0x4001, 0x01c3, 0xc003, 0x8002, 0x41c2,
 0x01c6, 0xc006, 0x8007, 0x41c7, 0x0005, 0xc1c5, 0x81c4, 0x4004,
 0x01cc, 0xc00c, 0x800d, 0x41cd, 0x000f, 0xc1cf, 0x81ce, 0x400e,
 0x000a, 0xc1ca, 0x81cb, 0x400b, 0x01c9, 0xc009, 0x8008, 0x41c8,
 0x01d8, 0xc018, 0x8019, 0x41d9, 0x001b, 0xc1db, 0x81da, 0x401a,
 0x001e, 0xc1de, 0x81df, 0x401f, 0x01dd, 0xc01d, 0x801c, 0x41dc,
 0x0014, 0xc1d4, 0x81d5, 0x4015, 0x01d7, 0xc017, 0x8016, 0x41d6,
 0x01d2, 0xc012, 0x8013, 0x41d3, 0x0011, 0xc1d1, 0x81d0, 0x4010,
 0x01f0, 0xc030, 0x8031, 0x41f1, 0x0033, 0xc1f3, 0x81f2, 0x4032,
 0x0036, 0xc1f6, 0x81f7, 0x4037, 0x01f5, 0xc035, 0x8034, 0x41f4,
 0x003c, 0xc1fc, 0x81fd, 0x403d, 0x01ff, 0xc03f, 0x803e, 0x41fe,
 0x01fa, 0xc03a, 0x803b, 0x41fb, 0x0039, 0xc1f9, 0x81f8, 0x4038,
 0x0028, 0xc1e8, 0x81e9, 0x4029, 0x01eb, 0xc02b, 0x802a, 0x41ea,
 0x01ee, 0xc02e, 0x802f, 0x41ef, 0x002d, 0xc1ed, 0x81ec, 0x402c,
 0x01e4, 0xc024, 0x8025, 0x41e5, 0x0027, 0xc1e7, 0x81e6, 0x4026,
 0x0022, 0xc1e2, 0x81e3, 0x4023, 0x01e1, 0xc021, 0x8020, 0x41e0,
 0x01a0, 0xc060, 0x8061, 0x41a1, 0x0063, 0xc1a3, 0x81a2, 0x4062,
 0x0066, 0xc1a6, 0x81a7, 0x4067, 0x01a5, 0xc065, 0x8064, 0x41a4,
 0x006c, 0xc1ac, 0x81ad, 0x406d, 0x01af, 0xc06f, 0x806e, 0x41ae,
 0x01aa, 0xc06a, 0x806b, 0x41ab, 0x0069, 0xc1a9, 0x81a8, 0x4068,
 0x0078, 0xc1b8, 0x81b9, 0x4079, 0x01bb, 0xc07b, 0x807a, 0x41ba,
 0x01be, 0xc07e, 0x807f, 0x41bf, 0x007d, 0xc1bd, 0x81bc, 0x407c,
 0x01b4, 0xc074, 0x8075, 0x41b5, 0x0077, 0xc1b7, 0x81b6, 0x4076,
 0x0072, 0xc1b2, 0x81b3, 0x4073, 0x01b1, 0xc071, 0x8070, 0x41b0,
 0x0050, 0xc190, 0x8191, 0x4051, 0x0193, 0xc053, 0x8052, 0x4192,
 0x0196, 0xc056, 0x8057, 0x4197, 0x0055, 0xc195, 0x8194, 0x4054,
 0x019c, 0xc05c, 0x805d, 0x419d, 0x005f, 0xc19f, 0x819e, 0x405e,
 0x005a, 0xc19a, 0x819b, 0x405b, 0x0199, 0xc059, 0x8058, 0x4198,
 0x0188, 0xc048, 0x8049, 0x4189, 0x004b, 0xc18b, 0x818a, 0x404a,
 0x004e, 0xc18e, 0x818f, 0x404f, 0x018d, 0xc04d, 0x804c, 0x418c,
 0x0044, 0xc184, 0x8185, 0x4045, 0x0187, 0xc047, 0x8046, 0x4186,
 0x0182, 0xc042, 0x8043, 0x4183, 0x0041, 0xc181, 0x8180, 0x4040,
};

CODE_FAST unsigned short calcula_crc(unsigned char * buf, int nb)
{
int i; 
unsigned short rCRC16;
unsigned short  q;
unsigned char b;
	for(i=0,rCRC16=0xffff;i<nb;i++)
	{
		b=buf[i];
		q = (*(mtab+(b ^ (rCRC16>>8)))) & 0xffff;
		rCRC16 = ((q&0xFF00) ^ ((rCRC16<<8) & 0xffff)) | (q&0x00ff);
	}
	return (rCRC16);
}


void ASCII_EBCDIC( char* pCh, int len, int ascii_to_ebcdic)
{
int i,j;
	for(j=0; j<len ;j++)
	{
		i=ascii_to_ebcdic;
		do
		{
			if(_EBCDIC_ASCII[i]==pCh[j])
			{
				pCh[j]=_EBCDIC_ASCII[(ascii_to_ebcdic?(i-1):(i+1))];
				break;
			}
			i+=2;
		}while(_EBCDIC_ASCII[i]);
	}
}


unsigned int  achtoi(unsigned char* p, int len)
{       
char bufaux[12];
unsigned int uiaux=0;
	memcpy(bufaux,p,len);
	bufaux[len]=0;
	sscanf(bufaux,"%x",&uiaux);
	return uiaux;
/*
	switch(len)
	{
		case 1: 
			return( (unsigned int) ( ((p[0]>'9') ? (p[0]-7):p[0])-'0' ) );
		case 2:
			return((unsigned int)hexa_to_uchar(p));
		case 3:
			return((unsigned int)(((((*p)>'9') ? ((*p)-7) : (*p)) -'0')*16*16 + 
			 ((((*(p+1))>'9') ? ((*(p+1))-7) : (*(p+1)))-'0')*16 +
			 ((((*(p+2))>'9') ? ((*(p+2))-7) : (*(p+2)))-'0')));
		case 4:
			return(hexa_to_uint(p));
	}
	return 0;
*/
}

void  char_to_hexa(unsigned char ch,unsigned char* high,unsigned char* low)
{
	*high=(ch & 0xf0)>>4;
	if ((*high)>0x09)
		(*high)+=7;
	*high+='0';
	
	*low=(ch & 0x0f);
	if ((*low)>0x09)
		(*low)+=7;
	*low+='0';
}

unsigned long minutos_to_hhmm(unsigned long minutos)
{
unsigned long hores;
	hores=minutos/60L;
	return((hores*100L)+(minutos-(hores*60L)));
}

int es_str_valido(unsigned char* p, int maxlen)
{
int i;
	if(p!=NULL)
	{
		for(i=0; i<maxlen; i++)
		{
			if(p[i]=='\0')
				return 0;
			if((p[i]!=' ') && (p[i]!='*'))
				return 1;
			if(p[i]=='*')
				if((i<(maxlen-1)) && (p[i+1]!='\0'))
					return 1;
		}
	}
	return 0;
}

unsigned char hexa_to_uchar(unsigned char* p)
{       
	return((unsigned char)achtoi(p,2));
	//return ( ((((*p)>'9') ? ((*p)-7) : (*p)) -'0')*16 + 
	//		 ((((*(p+1))>'9') ? ((*(p+1))-7) : (*(p+1)))-'0') );
}


// *******************************************************
//  rutines per alloc permanent
// *******************************************************

////////////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////////////
#define MALLOC_BUFSIZE_TXT  (1024*64 )

__no_init char bulk_storage_txt[MALLOC_BUFSIZE_TXT];

// punters amb els que treballen les rutines: init_alloc_ifac,free_ifac,malloc_ifac
__no_init char * heap_of_memory;
__no_init char * last_heap_object;
__no_init char * top_of_heap;
// OS_Events
//OS_EVENT *semafor_alloc_ifac;

////////////////////////////////////////////////////////////////////////////////////////
// Aquesta funcio s'ha de cridar a l'arrancar el programa
////////////////////////////////////////////////////////////////////////////////////////

void init_alloc_ifac(void)
{
	//semafor_alloc_ifac=OSSemCreate(1);
	heap_of_memory = bulk_storage_txt;
	last_heap_object = bulk_storage_txt;
	top_of_heap = bulk_storage_txt + MALLOC_BUFSIZE_TXT - 1;
	memset(bulk_storage_txt,0x00,sizeof(bulk_storage_txt));
  
}

void* malloc_ifac(unsigned int size)
{
unsigned int size_adj;
	size_adj=(size+3)& (~0x3);   // ajusta a multiple de 4
	if((((unsigned long)top_of_heap)-(unsigned long)last_heap_object)>= size_adj)
		{
		last_heap_object += size_adj;
		return(last_heap_object - size_adj);
		}
	// cas no hi ha prous bytes lliures 
	return(NULL);
}

////////////////////////////////////////////////////////////////////////////////////////
void* calloc_ifac(unsigned int nelem, size_t elsize)
{
 char* p,*p_aux;
 unsigned int nc;
 nc=nelem*elsize;
 p=(char*)malloc_ifac(nc);
 if(p!=NULL)
 	{
 	 p_aux=p;
 	 while(nc)
 	 	{
 	    *p_aux++=0;
 	    nc--;
 	    }
 	 }
 return(p);
}

void get_string_sep_comas(char * orig, char * dest, int numCampo, int maxlen, int lenOrig)
{
	char ch;
	int inumCampo=0;
	int cnt=0;
	while(1)
	{
		if(numCampo==inumCampo) break;
		if(lenOrig==0) break;
		lenOrig--;
		ch=*orig;
		if(ch==0) break;
		if(ch==',') inumCampo++;
		orig++;
	}
  	if(inumCampo<numCampo) return;
	while(1)
	{
		if(lenOrig==0) break;
		lenOrig--;
		ch=*orig;
		if(ch==0) break;
		if(ch==',') break;
		*dest++=ch;
		orig++;
		if((cnt++)>=maxlen) break;
	}
	*dest=0;
}

int get_int_sep_comas(char * buffer, int numCampo, int lenOrig)
{
	char ch;
	int inumCampo=0;
	char strnum[12];
	int pnum=0;

	while(1)
	{
		if(numCampo==inumCampo) break;
		if(lenOrig==0) return 0;
		ch=*buffer;
		if(ch==0) return 0;
		if(ch==',') inumCampo++;
		buffer++;
		lenOrig--;
	}
	if(inumCampo<numCampo) return 0;
	strnum[0]=0;
	while(1)
	{
		if(lenOrig==0) break;
		ch=*buffer;
		if(ch==0) break;
		if(ch==',') break;
		if(ch=='.') break;
		if((ch>='0' && ch<='9') || ch=='-' || ch == 'x' || ch == 'X')
		  strnum[pnum++]=ch;
		if(pnum>(sizeof(strnum)-2)) break;
		buffer++;
		lenOrig--;
	}
	if(pnum==0) return -1;
	strnum[pnum]=0;
	return atoi(strnum);
}

int getNumCampos(char * orig, int len){
	int nc;
	if(len>0) nc=1;
	while(len){
		if(*orig++==',')
			nc++;
		len--;
	}
	return nc;
}

int diff(int a, int b){
	if(a>b)
		return a-b;
	else
		return b-a;
}

const char tabchar[65]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int base64(unsigned char * orig, char * dest, int length){
	int paso=0;
	int p=0;
	unsigned char ch;
	while(1){
		switch (paso){
		case 0:
			ch=*orig/4;
			paso++;
			break;
		case 1:
			ch=((*orig++)&0x03)*16;
			ch+=(*orig)/16;
			length--;
			paso++;
			break;
		case 2:
			ch=((*orig++)&0x0f)*4;
			ch+=*orig/64;
			length--;
			paso++;
			break;
		case 3:
			ch=(*orig++)&0x3f;
			length--;
			paso=0;
			break;
		}
		*dest++=tabchar[ch];
		p++;
		if(length==0)
			break;
	}
	while (p%4){
		*dest++='=';
		p++;
	}
	return p;
}

int posi(char * tab, char ch){
	char c;
	int pos=0;
	while(1){
		c=*tab++;
		if(c==0)
			break;
		if(c==ch)
			return pos;
		pos++;
	}
	return -1;
}

void decodB64( unsigned char * dest, char * orig,int nb){
	int paso=0;
//	int p=0;
	unsigned char ch;
	unsigned char tch;
	int pos;
	while(1){
		tch=*orig++;
		if(tch=='=')
			break;
		pos=posi((char *)tabchar, tch);
		switch (paso){
		case 0:
			ch=(pos*4)&0xff;
			paso++;
			break;
		case 1:
			ch+=(pos/16);
			*dest++=ch;
			ch=(pos&0x0f)*16;
			paso++;
			break;
		case 2:
			ch+=(pos/4);
			*dest++=ch;
			ch=(pos&3)*64;
			paso++;
			break;
		case 3:
			ch+=pos;
			*dest++=ch;
			paso=0;
			break;
		}
		if(--nb==0)
			break;
	}
}
