// ******************************************************
// prima.h    torns i serveis
// ******************************************************

#ifndef __PRIMA_H
#define __PRIMA_H


#endif


typedef struct
{
int max_regs_torn;
int reg_next_torn;
int num_regs_torn;
int reg_torn_obert;

int max_regs_servei;
int reg_next_servei;
int num_regs_servei;

int reg_servei_en_curs; // per actualitzar dades servei
T_FECHHOR data_tancar_torn;

int itorn_visu;  // index torn per triar impressio
int ntorns_visu;  // num torns a visualitzar

int torn_obert;
}t_prima;


extern t_prima prima;


extern void prima_init(void);
extern int prima_inici_torn(void);
extern int prima_final_torn(void);
extern int prima_get_index_last_torn(void);
extern int prima_get_index_prev_torn(int ireg);
extern int prima_llegir_torn(int ireg);

extern int prima_afegir_servei(void);
extern int prima_actualitzar_servei(int ireg);
extern int prima_llegir_servei(int ireg);
extern int prima_get_index_next_serv(int ireg);

extern void prima_get_dades_servei(void);


  