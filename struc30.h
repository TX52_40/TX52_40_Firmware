/***********************************************************************/
/*  struc30.h           estructures tx30                               */
/***********************************************************************/

#ifndef _INC_STRUC30
#define _INC_STRUC30

#include "reloj.h"
#include "metros.h"
#include "tx30.h"
#include "tots.h"

#define NMAX_SUPL_DESGL   8   // suplements desglossats
#define MAX_PSGRS 6   // maxim passatgers cas multitaximetre

#define NUM_DIAESP 128   // per dimensionar i processar tarcom.diaesp

struct s_tabla_crida_eprom
{
	unsigned char nChip;
	unsigned int  adr;
	unsigned int  nb;
	char * buf;
};

struct s_grab
{
	unsigned short n_grabs;
	unsigned char chk_tar;
	unsigned char keycrip[2];
	unsigned char dum[3];
};


struct s_canal_serie
{
	unsigned char status_env;
	unsigned char sav_len_env;
	unsigned short  chk_env;
	//  !!!!tx50 per que faltan ram  unsigned char sav_buf_env[256];
	unsigned char status_rep;
	// unsigned char len_rep;
	unsigned short  chk_rep;
	//unsigned char cnt_to;
	unsigned char to_env;
	unsigned char to_rep;
	unsigned char op_env;
	unsigned char cola_rep[32]; /*!! punters fan &0x1f per tornar a 0 */
	unsigned char  cola_env[1024];  //!!!! punters fan &0x3ff per tornar a 0
};


typedef struct s_diaesp
{
	unsigned short anydia;
	unsigned char tip;
}t_diaesp;

typedef struct s_diaamb
{
	unsigned short anydia_ini;
	unsigned short anydia_fin;
	unsigned char tip;
}t_diaamb;

typedef struct s_interval_dies
{
	unsigned short anydia_ini;
	unsigned short anydia_fin;
}t_interval_dies;


struct s_param_carg
{
	unsigned long restrin[2];
	unsigned long n_grabs_carg;
	unsigned long restrin_2;
};

#define LEN_CODS 9        // numero digits de cada codi secret
#define NUM_CODS 9        // numero de passwords
#define NUM_CODS_MAX  10  // numero maxim que pot haver-hi

#define CODS_PARAMS       4   // visu params
#define CODS_OFFxON       5   // numero cods pas de OFF a ON
#define CODS_CERRAR_TURNO 7   // numero cods obrir/tancar torn


struct s_cods_tax
{
	unsigned char cods[6][LEN_CODS];
	unsigned short check;
};

struct s_cods_tax_ext
{
	unsigned char cods[4][LEN_CODS];
	unsigned short check;
};


#define LEN_TEXTE_NOM         24
#define LEN_TEXTE_NOM_32	  32
#define LEN_TEXTE_DNI         16
#define LEN_TEXTE_LLICENCIA   12
#define LEN_TEXTE_MATRICULA   12

#define LEN_TEXTE_H           24
#define LEN_TEXTE_F           24
#define LEN_TEXTE_H_32        32
#define LEN_TEXTE_F_32        32
#define LEN_TEXTE_GENERIC     24

#define MAXNUM_TEXTE_GENERIC   10

#define MAX_MODIFS_PROG  30

struct s_modif_prog
{
	unsigned short version;
	unsigned short check;
	T_FECHHOR fecha;
};

#define MAX_MODIFS_TAR  30

struct s_modif_tar
{
	char num_serie[2];
	unsigned short check;
	T_FECHHOR fecha;
};

struct s_blqs
{
	unsigned long k;
	t_gi gi;
	char luminosoTipo;
	char nt[2];
	char f_grab[3];
	char CCC[2];
	char n_lic[3];
	char n_serie[3];
	char f_exp[3];
	char f_inst[3];
	char CCC_inst[2];
	char n_progt[2];
	char f_fab[3];
	char chk_ff;
	char SS;
	char DDD[2];
	unsigned long n_grabs;
	unsigned long n_reps;
	char r5[2];
	char r6[3];
	unsigned long n_descs;
	unsigned long  nsalts;
	unsigned long nmodifs_k;
	unsigned long nmodifs_tar;
	char texte_nom[LEN_TEXTE_NOM+1];
	char texte_dni[LEN_TEXTE_DNI+1];
	char texte_llicencia[LEN_TEXTE_LLICENCIA+1];
	char texte_matricula[LEN_TEXTE_MATRICULA+1];
	unsigned long nmodifs_prog;
	struct s_modif_prog modif_prog[MAX_MODIFS_PROG];
	
	unsigned char   pswd_zapper[LEN_PWD];// zapper
	T_FECHHOR fecha_borr_tots;    // data borrat totalitzadors afegit a v 2.00
	char tipus_impressora;
	char hi_ha_td30;
	char dum[32];  
	unsigned short  chk_blqs; 
};

struct s_blqs_ext
{
	char texte_H1[24+1];
	char texte_H2[24+1];
	char texte_H3[24+1];
	char texte_H4[24+1];
	char texte_H5[24+1];
	char texte_H6[24+1];
	char texte_F1[24+1];
	char texte_F2[24+1];
	char texte_F3[24+1];
	char texte_F4[24+1];
	char texte_F5[24+1];
	char texte_F6[24+1];
	//char dum[300];      versio 1.11
	struct
	{
		unsigned short nom;
		char texte[25];
		char dum[3];
	}array_text[10];
	unsigned int  chk_blqs_ext; 
};

struct s_blqs_ext_32
{
	char texte_H1[32+1];
	char texte_H2[32+1];
	char texte_H3[32+1];
	char texte_H4[32+1];
	char texte_H5[32+1];
	char texte_H6[32+1];
	char texte_F1[32+1];
	char texte_F2[32+1];
	char texte_F3[32+1];
	char texte_F4[32+1];
	char texte_F5[32+1];
	char texte_F6[32+1];
	char texte_nom[32+1];
	unsigned int  chk_blqs_ext; 
};

struct s_blqs_modifs_tar
{
	unsigned short chk_blqs_modifs_tar; 
	unsigned long nmodifs_tar;
	struct s_modif_tar modif_tar[MAX_MODIFS_TAR];
};

struct s_visu      
{
	char led[2];
	void *p;
	char nform;
};

struct s_visu_vd
{
	void  **p;
	char nform;
};

struct s_parms_tar
{
	char tip;
	char led;
	void *p;
	char nform;
};

typedef struct s_disp_tot
{
	char nt;   /* num. tot a visualizar (255 = fin) */
	char sst;   /* codi ss identif. visu totalizador */
	unsigned short lf;  // leds frontal
}t_disp_tot;



typedef struct 
{
	char luz[8];
}t_luz;

typedef struct s_text_disem
{
	char text[4];
}t_text_disem;

#define E_MAX     25


typedef struct s_vars_estat
{
	unsigned short ledest;  // leds estat (display 0)
	unsigned short lf;      // textes frontal
}t_vars_estat;

typedef struct s_bloc_tar_eprom
{
	unsigned char tipt_vf;       //  0/1/2/3  rent/vf/vffija??/vffixe 
	unsigned char tipt_acs;      /*  0/1  ACFS_H ACFS_K  junts/separats  */
	unsigned long vsal[2];          /* valor salto */
	float vf_fija;               /* velo frontera caso isr (km/hora)*/
	unsigned char bits_cuenta;   /*    76543210      0/1  no/si cuenta */
	/*    hkhkhkhk   */
	/*    sspsssps    */
	/*    pagaocup   */
	/* primer salto  */
	float m_ps;                  /* metros salto */
	float seg_ps;                /* segundos salto */
	/* siguientes saltos */
	float impkm_ss;              /* importe km. */
	float impho_ss;              /* importe hora */
}t_bloc_tar_eprom;



struct s_bloc_tar
{
	struct s_bloc_tar_eprom;
	
	/*  
	//  primer salto  
	unsigned short  vvf_ps;        //  num. tics vvf 
	unsigned short adjust_ps;             //  num. tics adjust 
	float impkm_ps;              //  importe km. 
	float impho_ps;              //  importe hora 
	unsigned long fsk_ps;        //  fraccion salto km 
	unsigned long fsh_ps;        //  fraccion salto horaria 
	//  siguientes saltos 
	unsigned short  vvf_ss;        //  num. tics vvf 
	unsigned short adjust_ss;             //  num. tics adjust 
	float m_ss;                  //  metros salto 
	float seg_ss;                //  segundos salto 
	unsigned long fsk_ss;        //  fraccion salto km 
	unsigned long fsh_ss;        //  fraccion salto horaria 
	*/
	//Variables para calculo test conector
	/* primer salto  */
	unsigned long  FS_K_ps;
	unsigned long  FS_H_ps;
	unsigned long  FACTOR_N_ps;
	unsigned long  KK_ps;
	unsigned long  KH_ps;
	// siguientes  saltos 
	unsigned long  FS_K_ss;
	unsigned long  FS_H_ss;
	unsigned long  FACTOR_N_ss;
	unsigned long  KK_ss;
	unsigned long  KH_ss;
	unsigned char	 tarifarDistancia_ps;
	unsigned char  tarifarDistancia_ss;
	//fin Variables para calculo test conector
};



typedef struct s_tarifa
{
	unsigned long bb;         /* bajada bandera */
	unsigned long corsa_min;
	unsigned char bb_manual;
	unsigned char prim_salt_man;
	unsigned char branca;
	unsigned char lioc;
	unsigned long ococ;
	unsigned char ocap;
	unsigned long apoc;
	unsigned char b_t_ocup;   /* bloque tarifa en ocupado */
	unsigned char b_t_pagar;  /* bloque tarifa en a pagar */
	unsigned short  ledocup[2];    // [2] per cas led_tarifa_flash
	unsigned short  ledapagar[2];  // [2] per cas led_tarifa_flash
	unsigned char luzocup[8];
	unsigned char luzpagar[8];
	unsigned long supl;
	unsigned char nmaxs;        /* num. max. supl */
	float pcsup;                /* % supl.  */
	unsigned char pctip;        /* 0/1/2  no desencadena/manual/automatico */
	unsigned long supl_aut;
	unsigned char tip_s_aut;    /* 0 adicion  1 sustit. */
	unsigned char op_vd[4];
	unsigned char op_tec0;
	unsigned char visu_globtar; // 0/1 global/tarifa  bit 0 dist. bit 1 crono
	char tiptop;
	unsigned char tec_supl;   // tecles (bit 0-7) valides per fer supl. 
	unsigned short lf_oc;
	unsigned short lf_pag;
	unsigned char impre_tartrab[2];      // texte per imprimir tarifes treballades
	char tipus_iva;   // 0-2
}t_tarifa;


struct s_servcom
{
	char  n_viajs;
	char  visu_led_viaj;
	int pk_lib_serv;
	int pk_lib_pasj;
	char pet_tick_opc;
	char ja_fet_reset_interserv;
	char  viaj_activo[MAX_PSGRS];
	
	char num_ticket_incrementat;
	char segs_ocupat;
	char viaj_per_llums;
	int pk_oc_serv;
	float imph_visu;
	float impk_visu;
	char es_control;
	unsigned short save_lf_pag;
	
};

struct s_per_tarifa
{
	unsigned long import;
	unsigned long supl;
	unsigned long hm_oc;
	unsigned char n_supl;
	unsigned char fet_s_aut;
	long parcial_crono;
	unsigned int  parcial_dist;
	unsigned short  parcial_temps;
	char ntope_dist;
	char ntope_temps;
	char ntope_imp;
};


struct s_serv
{
	unsigned char tarifa;      
	unsigned long bb;      // baixada bandera 
	char bb_ja_aplicada;
	char bb_num_tarifa;    // tarifa de la baixada de bandera 
	unsigned long import;
	unsigned long supl;
	unsigned long supl_aut;
	unsigned long supl_desgl[NMAX_SUPL_DESGL];
	unsigned long supl_max;
	T_FECHHOR finicio;
	T_FECHHOR ffin;
	unsigned long hm_oc;
	struct s_per_tarifa per_tarifa[32];   //desgloses per cada tarifa treballada 
	unsigned long tartrab;      // tarifes treballades 
	unsigned long num_salts;
	char segs_ocupat;
	char canvi_minut_ocupat;
	unsigned short minuts_oc;
	// unsigned short minuts_ocup_marcha;
	long crono;
	char aplicat_corsa_min;   
	long corsa_min;           
	long import_tarifat;
	long import_abans_corsa_min;
	
	unsigned char TARIFANDO;
	unsigned char PRIMSAL_K;
	unsigned char SAV_PRIMSAL_K;
	unsigned char PRIMSAL_H;
	unsigned long ACFS_K;
	unsigned long ACFS_H;
	unsigned long  ACC_K;
	unsigned long  ACC_H;
	unsigned long  NS_K;
	unsigned long  NS_H;
	unsigned char SALT_PER_DIST;
	unsigned char SALT_PER_TEMPS;
	unsigned char CUENTA_K;
	unsigned char CUENTA_H;
	unsigned char NEW_CUENTA_K;
	unsigned char NEW_CUENTA_H;
	unsigned char CAMBIAR_DE_TARIFA;
	unsigned long   FACTOR_N_ps;
	unsigned long   FACTOR_N_ss;
	
	unsigned long   KK_ps;
	unsigned long   KH_ps;
	unsigned long   KK_ss;
	
	unsigned long   KH_ss;
	unsigned long   FS_H_ps;
	unsigned long   FS_K_ps;
	unsigned long   FS_H_ss;
	unsigned long   FS_K_ss;
	
	unsigned long   NEW_FACTOR_N_ps;
	unsigned long   NEW_FACTOR_N_ss;
	unsigned long   NEW_KK_ps;
	unsigned long   NEW_KH_ps;
	unsigned long   NEW_KK_ss;
	unsigned long   NEW_KH_ss;
	unsigned long   NEW_FS_H_ps;
	unsigned long   NEW_FS_K_ps;
	unsigned long   NEW_FS_H_ss;
	unsigned long   NEW_FS_K_ss;
	char NEW_TIPT_ACS;
	unsigned char NEW_MODO_VF;
	unsigned char NEW_VALUE_VF;
	unsigned char TARIFA_POR_DISTANCIA_ps;
	unsigned char TARIFA_POR_DISTANCIA_ss;
	unsigned char TIPT_ACS;
	unsigned char MODO_VF;
	unsigned char VALUE_VF;
	unsigned long vsal[2];          
	
	
	char no_es_primer_canvi;   // per recalcular fraccio salt
	unsigned int   imp_canvi;
	unsigned int   dist_canvi;
	unsigned int   temps_canvi;
	unsigned short minuts_canviaut;
	char ninot;
	unsigned long import_dist_temps[2];
	char     d_t_sep;           // tot el servei amb dist/temps separats
	char fet_pcsup;
	char fer_pcaut;
	char fet_pcaut;
	char suma_en_curs;
	char envio_copia;
	// per Xile
	unsigned long num_salts_dist_temps[2];
	long acum_dist_control;
	long acum_temps_control;
	char branca_canvi;
	unsigned long longitud[2];
	char ns[2];
	unsigned long latitud[2];
	char eo[2];
};


struct s_servei
{
	// unsigned long bb;      /* bajada bandera */
	// unsigned long import_dist_temps[2];
	// char     d_t_sep;           // tot el servei amb dist/temps separats
	//unsigned long import[MAX_PSGRS];
	// char fet_pcsup;
	// char fer_pcaut;
	// char fet_pcaut;
	// char suma_en_curs;
	// unsigned long hm_oc_tarifa;
	// char ninot;
	unsigned long hm_libre;
	unsigned long hm_libre_pasj;
	unsigned short  m_libpas;       /* metres lliure amb pasj. per pas a ocupat */
	unsigned short  m_paglib;
	unsigned long nsent;         /* veces sentado pasajero en libre */
	unsigned char pasj_ant;
	// int pk_oc_serv;
	unsigned short minuts_lib;
	unsigned short minuts_lib_pasj;
	unsigned short hm_fins_fi_torn;
	// unsigned short minuts_canviaut;
	unsigned char mask_vd;
	// float imph_visu;
	// float impk_visu;
	unsigned long velmax;
	unsigned long velmaxlib;
	unsigned char vd[2];
	unsigned char op_tec0;
	unsigned char visu_globtar; // 0/1 global/tarifa  bit 0 dist. bit 1 crono
	// unsigned char tarifa;      /* tarifa en curs */
	unsigned short sav_minuts_lib;
	unsigned long sav_hm_libre;
	unsigned long sav_velmaxlib;
	unsigned short sav_minuts_lib_pasj;
	unsigned long sav_hm_libre_pasj;
	unsigned long sav_nsent;
	unsigned long sav_velmax;
	// unsigned short sav_minuts_lib_marcha; 
	unsigned char aun_reloj_en_libre; 
	unsigned char estat_euro;
	unsigned long total_euros;
	char buf_precio_fijo[4];
	char es_precio_fijo;
	unsigned char segs_precio_fijo;
	unsigned char mask_vd_sav;
	unsigned char mitad_reloj_en_libre;
	unsigned short m_fslib;
	float percent_iva;
	long import_iva;
	char tipus_iva;   // 0-2
	int esPrecioAcordado;
};



struct s_clear_x
{
	unsigned int nb;
	char * adr;
};

struct s_gt
{
	struct s_time time;
	char seg_ant;
	char min_ant;
	char dia_ant;
	char canvi_fecha;
	unsigned short minut_del_dia;
	unsigned short dia_del_any;
	char canvi_minut;
	char mitad_vd;
	char segs_interm;
	char amb;
	unsigned short  tdia;
	unsigned short  anydia;
};


typedef struct s_tarcom_eprom
{
	unsigned char serie[3];
	unsigned char CCC[2];
	unsigned char DDD[2];
	unsigned char SS;
	
	unsigned char TTT[2];
	unsigned char n_progt[2];
	unsigned char robo;
	unsigned short  versions_valides;
	unsigned char tiup;   // temps (base 50ms. per up teclat secuencial  
	char OP_CANVIVEL;  /* cambio tarifa por velocidad */
	unsigned char veltop_canvivel;
	short segssortir_canvivel;
	short segsentrar_canvivel;
	char OP_MULTIT;      /* Multitaximetro    */
	unsigned char tiviaj;  /* 1/10" para cambio multi viajero*/
	unsigned char veltop;
	unsigned char veltop_fin_err;    // v 1.04X per sortir d'error exces vel
	float yardas;
	char OP_VISUTOTSOFF;   // visu totalit. en OFF
	char OP_PERCENT_DESC;      /* porcentual es descuento */
	char OP_ANUL10SEG;         /* anula servei si temps en OCUPAT < 10 segs. */
	char OP_ANUL_100M;      /* anulacion servicios<100metros */
	char OP_RELOJ_VISU;         /* rellotge */
	char OP_RELOJ_PERM_METRES;       // rellotge  no imterr. per metres 
	char OP_RELOJ_LIBRE;  // 0/1 no/si
	char tiup_reloj_libre; // temps (base 50ms.) per pas ocupat cas rellotge lliure
	char OP_VISURELOFF;    // visu rellotge en OFF
	char OP_LAP;       /* opcion lapsus */
	char OP_LIB_E2;    /* Libre por E2  */
	short sec_treure_tensio;    //  segons per apagar equip desde off
	char OP_E5;        /* salida E-6 caso fecha paro */
	char hay_impresora;
	char ticket_obligatori;
	char ticket_voluntari;
	char OP_APOCMF;    /* paso de A PAGAR a OCUP por metro fisico */
	char OP_ETT;         /* entrada valors tarifa per teclat carg */
	char OP_TST;         /* opcio test  */
	char OP_LOCKT;       /* bloqueig teclat */
	char OP_BLOQ_ERR;    /* E1 i E2 bloquean  */
	char OP_LLAVEOFF;    /* Llave contacto Apaga display */
	char  visvel_imp_extr;   /* 0/1/2 velo en lliure no/display import/disp. extres  */
	//unsigned char haypassw;
	unsigned short haypassw;   // v 2.14
	unsigned char haybloc;
	unsigned long tars_blq3;
	unsigned char totbormodif;  /* 0/1/2  visu/bor/modif */
	unsigned char disp_errrel;
	unsigned char hay_zapper;
	unsigned char luces_ver;   // luces que se verifican
	unsigned char hay_bloq_luz_lib;	  // hay bloqueo luz libre por fallo luces              td-tx
	char luz_bloq_lib[2];           // luces a sacar caso bloqueo luz libre por fallo               td-tx          
	unsigned char hay_liocaut;
	unsigned char tar_liocaut;
	unsigned short  m_liocaut;
	char tipus_teclat;     /* 0/1/2  par sec rot */
	unsigned char formddmm;
	unsigned char NDEC;
	unsigned char ntars;
	unsigned char nblocs_tar;
	unsigned char canautpag;
	unsigned char t_rel;     /*  0/ numero 100ms per avansar display rellotge */
	unsigned char titot;     /* num. 50ms per avancar tots */
	unsigned char tipsum;    /* 0/1/2/3/4  no suma/fija/tempor/tecla/en ocupado */
	unsigned char tisum;    // temporitzat per desglossar la suma en 1/20 de segon
    unsigned char tisum_s;  // temporitzat per desglossar la suma en segons
	unsigned char sumlib;  /* 0/1 no/si paso de suma a libre */
	unsigned char tip_paglib;      // 0/1/2  libre a apagar tecla/delay/tempor
	unsigned char tipaglib;        /* tiempo de a pagar a libre  */
	unsigned char hay_paglibaut;   /* 0/1/2  no/con pasj /consin pasj */
	unsigned short  m_paglibaut;
	unsigned char t_cods;  /* num. 50ms. per entrar codi secret */
	unsigned long fallot;
	unsigned short  tioff;
	unsigned char sup_p_s;
	unsigned char abortsupl;    /* 0/1 no/si anular supl  */
	unsigned char tiposupl;
	unsigned long maxsup;
	char maxsup_excluye_aut;	 // 0/1 no/si excluye supl automaticos
	unsigned char pcimpsup;    /*  0/1  supl.porcent. sobre imp/imp+supl */
	unsigned char supl_solo_fin_serv;   
	unsigned char digtot;
	unsigned char tot_un_paso;
	unsigned char segs_interm;
	t_disp_tot disp_tot[TOT_FIN_MAX];
	t_luz luzest[GL_FIN_MAX];
	unsigned short  lede_rel[3];
	unsigned char text_lib[10];
	t_text_disem text_disem[7];
	t_ajuste_hor ajuste_hor[NUM_AJUSTS_HORA];
	char con_precambio;
	short  anydia_precambio;
	char hora_precambio;
	char test_disp_auto;
	char newtransm;       // noves transmissions per terminal
	char newtransm_esp;   // autoritza transm 'M'
	char OP_CANZH_INI;    // 0/1 cambios por zh siempre/solo al pasar a ocupado
	char genimp_ocup;     // fallo gi 0-pasa a libre 1-se queda en ocupado
	// unsigned long k_min;
	// unsigned long k_max;
	char OP_error_impre;
	char OP_test_old_new;
	char OP_bloc5Km;
	char VISU_LAST_SERV;
	char flash_visu_last_serv;         // 0/1 no/si visu_last_serv intermitent
	unsigned char segs_visu_last_serv; // segons visu_last_serv intermitent
	unsigned char confi_vls;           // mascara bits per visu last serv
	unsigned char visu_euro_pagar;
	char TEXT_EURO[6];
	unsigned char NDEC_ALT;
	float factor_alt;             // per convertir a euros
	unsigned char suma_autom;
	unsigned char paglib_autom;
	unsigned char hay_precio_fijo;     // 0/1  no/si 
	unsigned char tar_precio_fijo;     // num.tarifa  
	unsigned char hm_precio_fijo;      // hm. m�ximo introduccion precio fijo
	unsigned char texto_precio_fijo[4];// texto en display supl.
	
	char ajuste_rel;      // 0/1 no/si ajustar reloj    (correspon a new_ajuste_rel de tx30 2.20)
	
	char exces_vel_no_horaria;   //  0/1 no/si deja de tarifar por horaria	 caso exces velo	  td-tx
	char errgenimp_no_horaria;   //  0/1 no/si deja de tarifar por horaria	 caso error genimp	  td-tx
	char bloq_libre_impre;       // 0/1  no/si  fallo impresora apaga luz libre    td-tx
	char bloq_ocup_impre;        // 0/1  no/si  fallo impresora bloquea paso a ocupado           td-tx
	char llums_combi;            // 0/1 taula normal / alternativa (portugal)
	char corsa_min_supl;         // corsa minima incluye suplementos
	char salto_pita;             // pito a cada salt
	char ledtar_interm;
	unsigned short lf_HORARIA;
	char new_leds_ho_km; 
	unsigned short lf_FECHA_PARO;
	unsigned short lf_FLASH_LAPSO_SUPL; // textes a posar en FLASH cas lapsus e extres
	unsigned short lf_euro1;
	unsigned short lf_euro2;
	unsigned short lf_euro3;
	unsigned short lf_vls0; 
	unsigned short lf_vls0_alt; 
	unsigned short lf_vls1;
	unsigned short lf_vls2;
	unsigned short lf_vls3; 
	t_vars_estat vars_estat[E_MAX];
	unsigned char tipf_disem[7];  /* tipo de fecha per cada dia de la semana*/
	t_diaesp diaesp[NUM_DIAESP];
	t_diaamb diaamb[17];
	
	char control_turno;   // nomes per pc30 visu pantalla torn
	char bloqueo_disem;           // 0/1/2   no/si SE==0/siempre
	short  minut_inici_dia;
	char dias_librar[5][2];
	char sabdom;              // 0/1/2  treballa si paritat igual/contraria/sempre
	char tck_no_legal;
	char bloqueo_torn;           // 0/1/2   no/si SE==0/siempre
	short  minut_inici_torn;
	short  minut_final_torn;
	long  minuts_max_torn;
	short  minuts_pas_off_a_lliure;       // abans es deia minuts_penal_torn 
	short  minuts_descans;  // minuts descans torn
	short  hm_off_torn;     // distancia franquicia en OFF
	long  minuts_max_torn_sabdom;
	char bloq_ocup_turno; // 0/1  no/si bloqueo paso a ocupado 
	unsigned char tipo_descanso;
	char bloq_luz_libre;
	char nivell_llum_minim;    // entre 0 i 7
	char hay_fslibdist;       // fi servei a lliure per distancia
	short m_fslibdist;
	unsigned char tr_ccab;
	unsigned char miles_km_ccab;
	char texte_COND[4];  // texte a display extres
	char texte_PASSW[4];  
	char texte_TANCAR_TORN[6]; // texte a display import
	char texte_TORN[4];        // texte a display extres
	char ticket_tancar_torn;
	float valors_iva[3];
	char hi_ha_conf_llums_test_display;
	char llums_test_display[2];
	char NUM_supl_max;   // valor entre 0-3  v1.11  numero supl m�xim (per acumular-hi la resta)
	char pot_apagar_lluminos;  // permet apagar lluminos en lliure
	char sanciona_descans_curt; // si temps descans curt,  sanciona .
	char sanciona_fallo_tensio;   // si fallo tensio penalitza amb temps descans
	char control_horari_cap_setmana;  // 0/1  no/si hi ha control
	long  minuts_max_torn_2;
	long  minuts_max_torn_sabdom_2;
	char sanciona_moviment;    // si mogut en OFF descompta temps descans
	unsigned char max_acc1;
	unsigned char max_acc2;
	unsigned char vel_zap_change;
	unsigned char num_errors1;
	unsigned short zapper_segons_error_definitiu;
	char corsa_min_deixa_de_comptar;
	char corsa_min_aplica_pas_fiserv;
	char display_suma_en_fi_servei;
	char taula_claus[256];
	char clau_preu_ac[64];
        short Id_clau_preu_ac;
	short id_claus;
	t_interval_dies intervals_control_horari[17];
	char visu_torn_en_lliure;
	char visu_dia_torn;
	char torn_continu;
	char segs_anular_pas_off;  // nomes per Fran�a
	short interval_entre_torns;
	short preavis_fi_torn;
	short minuts_maxim_descans;
	char veure_events_torn;
	char preprog_events_torn;
	short hora_minim_ini_torn;
	short hora_maxim_ini_torn;
	char num_descans_torn;
	char max_descans_fets;
	char texte_max_descans_fets[6];
	char password_paris[LEN_CODS];
	char dates_desactivacio_torn[4];
	char luminTipoFixe;
	char control_chile;
	char no_rearmar_automaticament;  // torn nomes es rearma al tornar de OFF
	char tots_historics;
	char text_error[12][6];
	char no_canvis_primer_salt;
	unsigned long Tiq_obl_imp;
	char tarifa_remota;
	char progr_TX_remoto;
	char pcsup_bb;
	char pcsup_pt;
 char acumula_franq;
 short hora_ini_fr2;
 short hora_fin_fr2;
 short min_max_fr2;
 char tec_preu_acordat;
}t_tarcom_eprom;

struct s_tarcom
{ 
	struct s_tarcom_eprom;
	// a partir d' aqui ja no estan a eprom tarifa
	
	unsigned long k;             /* constant k             */
	unsigned char DIVK;            /* divisor impulsos metro */
	
	int pmpk0;
	int pmpk0_ccab;
	float fvelo;                 /* factor velocitat */
	unsigned short ledtar[32];     /* leds tarifa per envio a ir100 */
	// unsigned char marcha0;       /* metros/minut amb correccio k */
	unsigned short m_liocaut_fk;    // m_liocaut amb correccio k
	unsigned short m_paglibaut_fk;  // m_paglibaut amb correccio k
	unsigned short m_fslibdist_fk;  // m_fslibdist amb correccio k
	t_gi gi;
	char luminosoTipo;   // 0/1/2  Saludes / Otros / Saludes Serial
	unsigned short lf_HORARIA_OFF;
	char hi_ha_torn_prima_intern;
	char num_torns_control_horari;
	// valors que depenen hi hagi 1/2 torns
	long  minuts_max_torn_1x2;         
	long  minuts_max_torn_sabdom_1x2;  
	long  visu_minuts_max_torn_1x2;         // visualitza el temps d'un torn o el total del dos torns
	long  visu_minuts_max_torn_sabdom_1x2;
	long impulsos_mogut_en_off;
	
	unsigned short max_acc1_fk;   // fb_zapper amb correccio k
	unsigned short max_acc2_fk;   // fb_zapper amb correccio k
	unsigned short min_pulses_per_sec; // fb_zapper 
	char tipus_impressora;
	char hi_ha_td30;
};

struct s_configs_desde_bg40
{
	char hi_ha_torn_prima_bg40;
	char hi_ha_password_cond;
	char hi_ha_pausa_activa;
	char select_iva_teclat;
	int segons_pausa_passiva;	// si es 0 no hi ha pausa passiva
	
};

typedef unsigned short t_zonh;
typedef unsigned long t_tope_imp;
typedef unsigned long t_tope_dist;
typedef unsigned short t_tope_temps;
typedef struct s_canvi_autom{
	unsigned char tarifa;
	unsigned char bits;
	unsigned long teclat;
	unsigned char amb;
	unsigned char velo;
	unsigned short tdia;
	unsigned short zonh;
	unsigned short import;
	unsigned short dist;
	unsigned short temps;
	unsigned char sext;
	unsigned char branca;
}t_canvi_autom;


typedef struct
{
	unsigned short nom;
	char texte[LEN_TEXTE_GENERIC];
}t_texte_generic;

typedef struct
{
	char hi_ha_textes;
	char texte_nom[LEN_TEXTE_NOM];
	char texte_dni[LEN_TEXTE_DNI];
	char texte_llicencia[LEN_TEXTE_LLICENCIA];
	char texte_matricula[LEN_TEXTE_MATRICULA];
	char texte_H1[LEN_TEXTE_H];
	char texte_H2[LEN_TEXTE_H];
	char texte_H3[LEN_TEXTE_H];
	char texte_H4[LEN_TEXTE_H];
	char texte_H5[LEN_TEXTE_H];
	char texte_H6[LEN_TEXTE_H];
	char texte_F1[LEN_TEXTE_F];
	char texte_F2[LEN_TEXTE_F];
	char texte_F3[LEN_TEXTE_F];
	char texte_F4[LEN_TEXTE_F];
	char texte_F5[LEN_TEXTE_F];
	char texte_F6[LEN_TEXTE_F];
	t_texte_generic texte_generic[MAXNUM_TEXTE_GENERIC];
}t_textes_tiquet;


typedef struct
{
	unsigned short check;
	unsigned short visu_chk;
	t_tarcom_eprom tarcom;
	t_bloc_tar_eprom bloc_tar[32];
	t_tarifa tarifa[32];
	t_zonh zonh[17];
	t_tope_imp tope_imp[17];
	t_tope_dist tope_dist[17];
	t_tope_temps tope_temps[17];
	t_canvi_autom canvi_autom[32*8];   // 8 condicions per tarifa
	t_textes_tiquet textes_tiquet;
}t_tarifa_tx50;


#endif

