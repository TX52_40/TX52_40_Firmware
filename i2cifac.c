//***************************************************************
// i2cifac.c
//***************************************************************

#include "i2cifac.h"
#include "iostr710.h"
#include "hard.h"
#include "intrinsics.h"

 
void delay_us(int us)
{
  volatile int i,j;
//  j=j;  j=j;
  for(i = 0;i < (us-1);i++)
  {
//    j=j;    j=j;    j=j;
    j=j;   // j=j;    j=j;
  }
  return;
}

char numClock=0;    

void D_IN (void)
{
// if (numBus>0x01) PD3.b.SDA0=0;
// else PD3.b.SDA1=0;
  IOPORT0_PC0 &= 0xfffb;
  IOPORT0_PC1 |= 0x0004;
  IOPORT0_PC2 &= 0xfffb;
    
}

void D_OUT (void)
{
// if (numBus>0x01) PD3.b.SDA0=1;
// else PD3.b.SDA1=1;
  IOPORT0_PC0 |= 0x0004;
  IOPORT0_PC1 &= 0xfffb;
  IOPORT0_PC2 |= 0x0004;
}

void D_UP (void)
{
// if (numBus>1) P3.b.SDA0=0x01;
// else P3.b.SDA1=0x01;
  IOPORT0_PD |= 0x04;
}

void D_DW (void)
{
// if (numBus>1) P3.b.SDA0=0x00;
// else P3.b.SDA1=0x00;
    IOPORT0_PD &= 0xfffb;
    __no_operation();
    __no_operation();
    __no_operation();
    __no_operation();
}

int DATO(void)
{
// if (numBus>1) return P3.b.SDA0;
// return P3.b.SDA1;
  if(IOPORT0_PD & 0x04)
    return(1);
  else
    return(0);
}


void C_UP (void)
{
// if (numBus==0) {P3.b.SCL1O=0; return;}
// if (numBus==1) {P3.b.SCL2O=0; return;}
  if(numClock == 0)
    IOPORT0_PD |= 0x01;
  else
    IOPORT0_PD |= 0x02;
}

void C_DW (void)
{
// if (numBus==0) {P3.b.SCL1O=1; return;}
// if (numBus==1) {P3.b.SCL2O=1; return;}
  if(numClock == 0)
    IOPORT0_PD &= 0xfffe;
  else
    IOPORT0_PD &= 0xfffd;
  __no_operation();
  __no_operation();
  __no_operation();
  __no_operation();
}


void on_i2c(void)
{
   D_UP(); delay_us(2); C_UP(); delay_us(2); D_DW(); delay_us(2); C_DW();
}

void off_i2c(void)
{
   D_DW(); delay_us(1); C_UP(); delay_us(1); D_UP();
}

void reset_i2c_ifac(void){ 

   char i;

   for (numClock=0; numClock<2; numClock++)
    {
     D_IN(); C_DW();      //Dato com a entrada (no_ack)
     for (i=0; i<20; i++) //20 clocks per asegurarse de que ha acabat
      {
       C_UP(); delay_us(5); C_DW(); delay_us(5); 
      }
     D_OUT(); D_DW(); delay_us(5);      //dato com a sortida i a 0
     C_UP();  delay_us(5); D_UP();     //off_i2c;
//   D_UP(); C_UP();                     aix� era abans cuan fallaba
//   if (DATO()){off_i2c(); continue;}
//   on_i2c();
//   while (!DATO())
//    {
//     C_UP(); C_DW(); D_UP();
//    }
//   D_DW();
//   off_i2c();
    }
   numClock=0;
}


char retorn;

void saca_i2c(unsigned char dato){

   unsigned char gmask;
   gmask=0x80;
   do{
     if (dato & gmask) D_UP(); else D_DW(); delay_us(2);
     C_UP(); gmask>>=1; delay_us(5);  C_DW();delay_us(1);
   }while (gmask);
   D_IN(); delay_us(1); C_UP(); delay_us(4); retorn=DATO();
   delay_us(1); C_DW(); D_OUT(); delay_us(1); D_DW();
}

unsigned char lee_i2c(unsigned char ak)
{

   unsigned char gmask;
   unsigned char gdato;
   gdato=0;  gmask=0x80; D_IN();
   delay_us(1);
   do{
      C_UP(); delay_us(1); if(DATO()) gdato|=gmask; C_DW();delay_us(1);
      gmask>>=1;
   }while (gmask);
   D_OUT();
   delay_us(1);
   if (ak==1){
     D_DW(); delay_us(1); 
     C_UP(); delay_us(1);  C_DW(); delay_us(1);  D_IN();
   }
   else{
     D_UP(); delay_us(1); 
     C_UP(); delay_us(2);  C_DW();
     delay_us(1); D_DW();
   }
   return (gdato);
}


void output_i2c_ifac(char nClock, char nSlave, unsigned char dato)
{
  numClock = nClock;
      on_i2c();  saca_i2c(nSlave);  saca_i2c(dato);  off_i2c();
}

unsigned char input_i2c_ifac(char nClock, char nSlave)
{
unsigned char st;
  numClock = nClock;
      on_i2c();  saca_i2c(nSlave|1);  st=lee_i2c(0);    off_i2c();
  return st;
}

void write_i2c_ifac(char nClock, unsigned char sl, unsigned char dir, unsigned char nb, char *buf)
{
numClock = nClock;
   on_i2c();   saca_i2c(sl);   saca_i2c(dir);
   do{saca_i2c(*buf);   buf++; nb--;}while (nb>0);
   off_i2c();
}

void wread_i2c_ifac(char nClock, unsigned char sl, unsigned char dir, unsigned char nb, char *buf)
{
numClock = nClock;
   on_i2c();  saca_i2c(sl);  saca_i2c(dir);
   on_i2c();  saca_i2c(sl | 1);
   do{delay_us(5);  *buf=lee_i2c(1);   buf++;  nb--;}while (nb>1);
   delay_us(5);
   *buf=lee_i2c(0);
   off_i2c();
}

void write_i2c_extnd_ifac(char nClock, unsigned char sl, unsigned int dir, unsigned int nb, char *buf)
{
numClock = nClock;
   on_i2c();saca_i2c(sl);
   saca_i2c((unsigned char)(dir>>8));
   saca_i2c((unsigned char)(dir&0x00ff));
   do{saca_i2c(*buf);   buf++; nb--;}while (nb>0);
   off_i2c();
}

void wread_i2c_extnd_ifac(char nClock, unsigned char sl,unsigned int dir,unsigned int nb, char *buf)
{
numClock = nClock;
   on_i2c();
   saca_i2c(sl);
   saca_i2c((unsigned char)(dir>>8));
   saca_i2c((unsigned char)(dir&0x00ff));
   on_i2c();  saca_i2c(sl | 0x01);
   while ((nb--)>1)
     {
      delay_us(5); *(buf++)=lee_i2c(1); 
     }
   delay_us(5);
   *buf=lee_i2c(0);
   off_i2c();
}

void write_i2c_adr3_ifac(char nClock, unsigned char sl, unsigned int dir, unsigned int nb, char *buf)
{
numClock = nClock;
   on_i2c();saca_i2c(sl);
   saca_i2c((unsigned char)((dir>>16)|0x80));
   saca_i2c((unsigned char)((dir>>8)&0x00ff));
   saca_i2c((unsigned char)(dir&0x00ff));
   do{saca_i2c(*buf);   buf++; nb--;}while (nb>0);
   off_i2c();
}

void wread_i2c_adr3_ifac(char nClock, unsigned char sl,unsigned int dir,unsigned int nb, char *buf)
{
numClock = nClock;
   on_i2c();
   saca_i2c(sl);
   saca_i2c((unsigned char)((dir>>16)|0x80));
   saca_i2c((unsigned char)((dir>>8)&0x00ff));
   saca_i2c((unsigned char)(dir&0x00ff));
   delay_us(5); 
   on_i2c();  saca_i2c(sl | 0x01);
   while ((nb--)>1)
     {
      delay_us(5); *(buf++)=lee_i2c(1); 
     }
   delay_us(5);
   *buf=lee_i2c(0);
   off_i2c();
}




unsigned char status_i2c_ifac(char nClock, char nSlave)
{
  numClock = nClock;
   on_i2c();  saca_i2c(nSlave);  off_i2c();
   return (retorn);
}


/*
// nomes es pot cridar des de una funcio que ja ha possat nClock
// no el modifica
int private_status_i2c_ifac(unsigned char sl)
{
   on_i2c();  saca_i2c(sl);  off_i2c();
   if (retorn) return(0);
   else return(1);
}
*/
