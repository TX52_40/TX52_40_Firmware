// *********************************************************
// bloqueig_torn.c
// *********************************************************

#include <string.h>
#include "tx30.h"
#include "rut30.h"
#include "display.h"
#include "lumin.h"
#include "i2c.h"
#include "bloqueig_torn.h"
#include "debug.h"

#define SEGONS_MINIM_EN_OFF  30     // si menys temps en OFF ho considera un microtall i passa 



#if DEBUG_MODIF_RELOJ

char var_debug[3] = {0,0,'R'};    // R/S  reempla�a / suma

void debug_reloj(void)
{
unsigned char aux;  
struct s_time  time;
  if(var_debug[0] || var_debug[1])
  {
      get_reloj(RELLOTGE_PLACA,&time);
      aux = var_debug[0];
      if(var_debug[2] == 'S')
          aux += bcd_bin(time.hour);
      time.hour=bin_bcd(aux);
      aux = var_debug[1];
      if(var_debug[2] == 'S')
          aux += bcd_bin(time.minute);
      time.minute=bin_bcd(aux);
      
      set_reloj(RELLOTGE_PLACA,&time);
      var_debug[0] = 0;
      var_debug[1] = 0;
  }
}
#endif


__no_init struct s_bloqueo_torn bloqueo_torn;

unsigned char calcul_tdia_torn(void)
{
int  i;
unsigned char  td;
 td = 0;
 for(i=0;tarcom.diaesp[i].tip && (i<NUM_DIAESP);i++)
   {
    if(tarcom.diaesp[i].anydia == bloqueo_torn.anydia_torn)
    {
      td=(tarcom.diaesp[i].tip & 0xf0)>>4;
      break;
    }
   }
 return (td);
}

//****************************************************
// calcul numero de semana
//****************************************************

unsigned short numero_semana(struct s_time *time)
{
unsigned short  aux;
 aux=bcd_bin(time->year);
 if(aux<96)
   aux+=4;
 else
   aux-=96;
 // 1-1-96 es lunes (semana 1)
 aux=(aux*365 + (aux+3)/4 + dia_del_any_from_bcd(&time->day) - 1)/7+1;
 return(aux);
}

long next_dia_any(void)
{
  if((gt.time.day == 0x31) && (gt.time.month == 0x12))
    return(1);     
  else
    return(gt.dia_del_any + 1);
}

unsigned short next_anydia(void)
{
unsigned short  iaux;
  
  if((gt.time.day == 0x31) && (gt.time.month == 0x12))
  {
   iaux=bcd_bin(gt.time.year)+1;
   if(iaux<90)iaux+=100;
   iaux-=90;
    return(iaux*512 + 1); 
  }    
  else
    return(gt.anydia + 1);
}
  
long next_dia_mes(void)
{
char aux;  
  if((gt.time.day == 0x31) && (gt.time.month == 0x12))
    return(101);  // 0101
  aux = dias_mes[gt.time.month];
  if((gt.time.month == 2) && ((bcd_bin(gt.time.year) % 4)==0))
    aux++;  // cas febrer any traspas
  if(bcd_bin(gt.time.day) >= aux)
    return(100 + bcd_bin(gt.time.month) + 1);
  return((bcd_bin(gt.time.day)+1)*100 + bcd_bin(gt.time.month));
  
}


void asigna_proper_rearme(void){
	  if(tarcom.hora_fin_fr2 != 0 || tarcom.hora_ini_fr2 != 0){
		if(inSecondInterval())
		   bloqueo_torn.minut_rearmar=tarcom.minut_inici_torn;
		else
			bloqueo_torn.minut_rearmar=tarcom.hora_ini_fr2;
	  } else {
		  bloqueo_torn.minut_rearmar=tarcom.minut_inici_torn;
	  }
	  bloqueo_torn.anydia_rearmar = (bloqueo_torn.minut_rearmar<gt.minut_del_dia) ? next_anydia() : gt.anydia;
}

// determina si esta en OFF ( o en visualitzacions desde OFF)

int esta_en_off(void)
{
  return((tx30.estat==E_OFF)||    
         (((tx30.estat==E_TOTS)||(tx30.estat==E_RELOJ))&&(tx30.estat_retorn_reltots==E_OFF )));
}

// nomes es crida si esta en OFF
// distingeix entre descans o torn tancat  
int esta_en_descans(void)
{
   return((bloqueo_torn.estat_torn == 2) || 
          ((bloqueo_torn.estat_torn == 1) && (bloqueo_torn.estat_torn_save == 2)));
 
}

// determina si esta en un servei
int esta_fent_un_servei(void)
{
  if((tx30.estat == E_OCUPADO) 
     || (tx30.estat == E_PAGAR) 
     || (tx30.estat == E_FISERV)
     || (tx30.estat == E_UP))
    return(1);
  return(0);
}

//
// crida:
//      mode = REARMAR_GLOBAL  mira si s'ha passat la data de canvi torn
//      mode = REARMAR_SEGON_TORN  mira s'ha passat la data de canvi torn
//                                 o si encara queda un conductor per fer el torn (cas hi hagi dos torns)
//  retorn:
//          REARMAR_NO          no s'ha de rearmar
//          REARMAR_GLOBAL      rearmar per canvi dia
//          REARMAR_SEGON_TORN  nomes activar segon torn
//
int hay_que_rearmar(int mode, int rearmar_sempre)
{
int ret;

  ret = REARMAR_NO;
  // afegit per si anydia rearmar s'havia tocat per un punter erroni
  if(bloqueo_torn.anydia_rearmar > next_anydia())
    bloqueo_torn.anydia_rearmar = next_anydia();
  
  switch(mode)
  {
    case REARMAR_GLOBAL:
      if((gt.anydia>bloqueo_torn.anydia_rearmar)
             ||( (gt.anydia==bloqueo_torn.anydia_rearmar) &&
                 (gt.minut_del_dia>=bloqueo_torn.minut_rearmar)))
	  {
		  
  		if(tarcom.no_rearmar_automaticament && !rearmar_sempre)
		{
			// cas no rearma automaticament al passar per hora inici
			// posa a 0 temps que queda pero encara no rearma torn
			bloqueo_torn.minuts_queden = 0;
		}
		else  
          ret = REARMAR_GLOBAL;
	  }
      break;  
    case REARMAR_SEGON_TORN:
      if((gt.anydia>bloqueo_torn.anydia_rearmar)
             ||( (gt.anydia==bloqueo_torn.anydia_rearmar) &&
                 (gt.minut_del_dia>=bloqueo_torn.minut_rearmar)))
	  {
  		if(tarcom.no_rearmar_automaticament && !rearmar_sempre)
		{
			// cas no rearma automaticament al passar per hora inici
			// posa a 0 temps que queda pero encara no rearma torn
			bloqueo_torn.minuts_queden = 0;
		}
		else  
          ret = REARMAR_GLOBAL;
	  }
      else
      {
        //aqui falta veure si hi ha dos torns, s'ha fet el primer i encara no s'ha fet el segon
        if(tarcom.num_torns_control_horari != 2)
          break;
        if((bloqueo_torn.torn_en_curs == 1) && (bloqueo_torn.minuts_queden == 0))
        {
          // ha acabat primer torn
          ret = REARMAR_SEGON_TORN;
        }
      }
      break;
  }
  return(ret);  
}

//********************************************************
// reinicia contador temps torn
// recalcula data nou canvi per bloqueig torn
//********************************************************
//
//   crida
//          
//          REARMAR_GLOBAL      reinicia contador temps del primer torn
//                              recalcula data proper rearmar torn
//          REARMAR_SEGON_TORN  nomes reinicia contador temps del segon torn
//
void rearmar_bloqueo_torn(int mode, int passar_a_off)
{
unsigned char  diasem;

  if(tarcom.torn_continu == 0)
  {

	  if(bloqueo_torn.tornDesactivatDiaEspecial || bloqueo_torn.dins_dates_desactivacio_torn){
		  bloqueo_torn.minuts_queden=1440;
	  } else if(inSecondInterval()){
		  bloqueo_torn.minuts_queden=tarcom.min_max_fr2;
	  } else {
		  // assigna durada torn
		  diasem=dia_de_la_semana_from_time(&gt.time);
		  if(gt.minut_del_dia < tarcom.minut_inici_dia)
			diasem=(diasem+6)%7;    //cas compta com dia anterior
		  if(diasem<=4)
		  {
		   bloqueo_torn.minuts_queden = tarcom.minuts_max_torn_1x2;
		   bloqueo_torn.desactivat_x_cap_setmana = 0;
		  }
		  else
		  {
		   // cas cap de setmana
		   bloqueo_torn.minuts_queden = tarcom.minuts_max_torn_sabdom_1x2;
		   if(tarcom.control_horari_cap_setmana ==0)
			 bloqueo_torn.desactivat_x_cap_setmana = 1;
		   else
			 bloqueo_torn.desactivat_x_cap_setmana = 0;
		  }
	  }
      
      bloqueo_torn.dist_off = 0;
      bloqueo_torn.dist_franquicia = 0;
      bloqueo_torn.num_descans_fets = 0;
      bloqueo_torn.num_descans_registrar = 0;
      bloqueo_torn.acum_descans = 0;
      bloqueo_torn.acum_sense_tensio = 0;
      
      switch(mode)
      {
        case REARMAR_GLOBAL:
           bloqueo_torn.torn_en_curs = 1; // indicador es el primer dels dos torns possibles
           if(passar_a_off)
           {
             tx30.estat = E_OFF;
           }
           if(esta_en_off())
             {
              // cas en OFF ( o en visualitzacions desde OFF)
              if(tarcom.hm_off_torn)
                    bloqueo_torn.estat_torn = ETORN_FRANQUICIA_DIST;  // cas hi ha franquicia distancia
              else
                    bloqueo_torn.estat_torn = ETORN_NO_INI;   // sense franquicia
             }
           else
             {
              // cas no esta en OFF  
              bloqueo_torn.estat_torn=ETORN_INICIAT;
             }
          // calcula nova data reset torn
		  asigna_proper_rearme();
          break;
        case REARMAR_SEGON_TORN:
          bloqueo_torn.torn_en_curs = 2; // indicador es el segon torn
          break;
      }
        
       bloqueo_torn.ve_de_baix_consum = 0;
  }
}
 
//
// test queden minuts torn per pas a ocupat
//
char bloqueo_paso_ocup(void)
{
 if((tarcom.torn_continu && bloqueo_torn.esta_passant_itv && bloqueo_torn.minuts_passant_itv)
    || (tarcom.bloq_ocup_turno==0) || bloqueo_torn.minuts_queden)
   return(0);
 return(1);
}


//
// test condicions fi torn
//                                           
// retorn:
//       actualitza dia/hora rearmar torn
//           si es dia de descans:
//                 hora inici dia (del mateix o seguent dia)
//           si fora d'interval torn treball o masses hores dins del torn
//                 hora inici torn (del mateix o seguent dia)
void test_bloqueo_turno(void)
{
unsigned char  diasem,dia,nlic,tdia;
char  fin;
 fin=0;
 if((tarcom.bloqueo_disem==2)
    ||((tarcom.bloqueo_disem==1) && !sext))
   {
    diasem=dia_de_la_semana_from_time(&gt.time);
    dia=bcd_bin(gt.time.day);
    if(gt.minut_del_dia < tarcom.minut_inici_dia)
      {
       //cas compta com dia anterior
       diasem=(diasem+6)%7;
       if(diasem>=5)
         {
          // dia del mes
          if(dia>1)
            --dia;
          else
            {
             dia=dias_mes[bcd_bin(gt.time.month)-1];
             if((dia==28) && ((bcd_bin(gt.time.year) & 0x03)==0 ))
                dia=29;
            }
         }
      }
    nlic=blqs.n_lic[0]&0x0f;
    tdia=calcul_tdia_torn();
    if(tdia!=3) do
      {
       if(diasem<5)
         {
          if(tarcom.tipo_descanso & TIPO_DESCANSO_DIASEM)
            {
             // cas per tarifa
             if(nlic==tarcom.dias_librar[diasem][0]
               || nlic==tarcom.dias_librar[diasem][1])
               fin=1;
            }
          else
            {
             // cas per carregador
             if(diasem==bloqueo_torn.dia_descans)
               fin=1;
            }
         }
       else
         {
          if(tarcom.tipo_descanso & TIPO_DESCANSO_FINSEM)
            {
             // cas per tarifa

             switch(tarcom.sabdom)
               {
                case 0:    // treballa si paritat igual
                  if((nlic^dia)&0x01)
                     fin=1;
                  break;
                case 1:    // treballa si paritat contraria
                  if(((nlic^dia)&0x01)==0)
                      fin=1;
                  break;
                default:      // 2 treballa sab i dom
                  break;
               }
            }
          else
            {
             // cas per carregador
             if((numero_semana(&gt.time)%2)==bloqueo_torn.semana_descans)
               fin=1;
            }
         }
       if(fin || !tdia)
         break;
       if(( (tdia==1) && ((nlic%2)==1) )  || ((tdia==2) && ((nlic%2)==0)))
         fin=1; 
      }while(0);
    if(fin)
      {
       bloqueo_torn.minuts_queden=0;  // per visu i bloqueig
	   asigna_proper_rearme();
       return;
      }
   }
 if(((tarcom.bloqueo_torn==2)
      ||((tarcom.bloqueo_torn==1) && !sext))
    && !bloqueo_torn.desactivat_x_cap_setmana && !bloqueo_torn.dins_dates_desactivacio_torn && !bloqueo_torn.tornDesactivatDiaEspecial)
   {
	   if(!dins_interval(gt.minut_del_dia, tarcom.minut_inici_torn, tarcom.minut_final_torn) && !dins_interval(gt.minut_del_dia, tarcom.hora_ini_fr2, tarcom.hora_fin_fr2))
		   fin=1;
//    if(tarcom.minut_inici_torn<tarcom.minut_final_torn)
//      {
//       if((gt.minut_del_dia<tarcom.minut_inici_torn)||(gt.minut_del_dia>tarcom.minut_final_torn))
//         fin=1;
//      }
//    else
//       if((gt.minut_del_dia<tarcom.minut_inici_torn)&&(gt.minut_del_dia>tarcom.minut_final_torn))
//         fin=1;
    if(!fin && bloqueo_torn.minuts_queden==0) // aqui determina si masses hores
        fin=1;  // esgotat temps torn
    if(fin && !tarcom.no_rearmar_automaticament)
      {
       bloqueo_torn.minuts_queden=0;  // per visu i bloqueig
   	   asigna_proper_rearme();
       return;
      }
   }
 return;
}

 
void recalculs_gt_bloqueig_torn(void)
{
int ini,fin,avui;
 bloqueo_torn.anydia_torn=gt.anydia;
 if(gt.minut_del_dia < tarcom.minut_inici_dia)
   {
    if(gt.dia_del_any>1)
      --bloqueo_torn.anydia_torn;
    else
     {
      bloqueo_torn.anydia_torn-=148;  // -512 + 364
      if((gt.time.year%4)==1)
        bloqueo_torn.anydia_torn++;  // cas any de traspas  (dia 366)
     }
   }
 
 // determina si esta dins dates desactivacio torn
 bloqueo_torn.dins_dates_desactivacio_torn = 0;
 ini = tarcom.dates_desactivacio_torn[1]*100 + tarcom.dates_desactivacio_torn[0];
 if(ini != 0)
 {
   fin = tarcom.dates_desactivacio_torn[3]*100 + tarcom.dates_desactivacio_torn[2];
   avui = bcd_bin(gt.time.month)*100 + bcd_bin(gt.time.day);
   if(gt.minut_del_dia < tarcom.minut_inici_dia)
     {
       // cas ha de comptar com dia anterior
       if(bcd_bin(gt.time.day) != 1) 
         avui--;
       else if(avui == 101)
         avui = 1231;
       else
         avui = (bcd_bin(gt.time.month)-1)*100 + dias_mes[bcd_bin(gt.time.month)-1];
     }
   if(fin >= ini)
   {
     if((avui >= ini) && (avui <= fin))
       bloqueo_torn.dins_dates_desactivacio_torn = 1;
   }
   else if((avui >= ini) ||(avui <= fin))
       bloqueo_torn.dins_dates_desactivacio_torn = 1;
 }
 	unsigned char nlic=blqs.n_lic[0]&0x0f;
	unsigned char tdia=calcul_tdia_torn();
	bloqueo_torn.tornDesactivatDiaEspecial=0;
	if((tdia==3) || ((tdia==1) && ((nlic%2)==0))  || ((tdia==2) && ((nlic%2)==1)))
		bloqueo_torn.tornDesactivatDiaEspecial=1;
 }


char bloqueo_luz_libre(void)
{
 if((tarcom.bloq_luz_libre==0) || bloqueo_torn.minuts_queden)
   return(0);
 return(1);
}


void bloqueig_torn_configura(void)
{
int i,j;
float raux;

// actualitza polsos per moviment d'acord amb la k
  raux=(METRES_MOGUT_EN_OFF*((float)tarcom.k))/(1000.*(float)tarcom.DIVK) +0.5;
  tarcom.impulsos_mogut_en_off = (long)raux;
  
//
// configura 1/2 torns  recuperat a versio 2.14  
//
      if(tarcom.minuts_max_torn_2 == 0)
        tarcom.num_torns_control_horari = 1;
      else if((tarcom.num_torns_control_horari == 0) || (tarcom.num_torns_control_horari >2))
        tarcom.num_torns_control_horari = 1;
      if(tarcom.num_torns_control_horari == 1)
      {
        // cas 1 torn
        tarcom.visu_minuts_max_torn_1x2 = tarcom.minuts_max_torn_1x2 = tarcom.minuts_max_torn;  
        tarcom.visu_minuts_max_torn_sabdom_1x2 = tarcom.minuts_max_torn_sabdom_1x2 = tarcom.minuts_max_torn_sabdom;  
      }
      else
      {
        // cas 2 torns
        tarcom.visu_minuts_max_torn_1x2 = tarcom.minuts_max_torn_2;  
        tarcom.visu_minuts_max_torn_sabdom_1x2 = tarcom.minuts_max_torn_sabdom_2;  
        tarcom.minuts_max_torn_1x2 = tarcom.minuts_max_torn_2 / 2;  
        tarcom.minuts_max_torn_sabdom_1x2 = tarcom.minuts_max_torn_sabdom_2 / 2;  
      }
      
    // inicialitza registre torns i descans
    bloqueo_torn.index_registre_torn = 0;
    for(i=0;i<NUM_MAX_TORNS;i++)
    {
      bloqueo_torn.registre_torn[i].anydia_torn = 0;
      bloqueo_torn.registre_torn[i].diames_torn = 0;
      bloqueo_torn.registre_torn[i].hora_inici_torn = 0;
      bloqueo_torn.registre_torn[i].hora_final_torn = 0;
      for(j=0;j<NUM_MAX_DESCANS_REGISTRAR;j++)
      {
        bloqueo_torn.registre_torn[i].registre_descans[j].hora_inici_descans = 0;
        bloqueo_torn.registre_torn[i].registre_descans[j].hora_final_descans = 0;
      }
    }
      
      
      
  if(tarcom.torn_continu)
  {
    // cas Fran�a
    
    bloqueo_torn.estat_torn = 0;  // inicialitza torn a situacio tancat
    bloqueo_torn.minuts_queden = 0;
    bloqueo_torn.torn_en_curs = 0;
    bloqueo_torn.minut_tancament = 0;   // per que obri sense verificar que ha passat prou temps
    bloqueo_torn.anydia_last_torn = 0;  // per que obri sense verificar ha canviat de dia
    bloqueo_torn.anydia_last_itv =0;    // per poguer verificar itv primer dia
    bloqueo_torn.esta_passant_itv = 0;
        
  }
}



void france_tancar_torn(long minuts2000)
{
  if(bloqueo_torn.torn_en_curs == 1)
    bloqueo_torn.minut_tancament = minuts2000;  //minut_del_segle_from_time(&gt.time);
  // registre dades final torn
  bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].hora_final_torn = minuts2000;
  
  bloqueo_torn.estat_torn = 0;  // torn tancat
  if(bloqueo_torn.torn_en_curs >= tarcom.num_torns_control_horari)
    bloqueo_torn.torn_en_curs = 0;
}


void registrar_final_descans(long minuts2000)
{
  if(tarcom.torn_continu == 0)
  {
  }
  else
  {
    if(bloqueo_torn.num_descans_registrar < NUM_MAX_DESCANS_REGISTRAR)
    {
       bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].registre_descans[bloqueo_torn.num_descans_registrar].hora_final_descans = minuts2000;
       bloqueo_torn.num_descans_registrar++;
    }
  }
}

void registrar_inici_descans(void)
{
  if(tarcom.torn_continu == 0)
  {
  }
  else
  {
    bloqueo_torn.minut_segle_inici_descans = minut_del_segle_from_time(&gt.time);  
    if(bloqueo_torn.num_descans_registrar < NUM_MAX_DESCANS_REGISTRAR)
      bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].registre_descans[bloqueo_torn.num_descans_registrar].hora_inici_descans = bloqueo_torn.minut_segle_inici_descans;
  }
}

//
// cas passa a lliure desde OFF
//
void paso_a_libre_bloqueig_torn(void)
{
int mode_rearmar;
char mogut_en_off_i_fi_franquicia;
unsigned short temps_a_descomptar;
unsigned short temps_pas_a_lliure;
char pot_fer_descans;


 if(tarcom.torn_continu == 0)
 {
   // versio clasica
   // ****** cas torn reinicia al canviar de dia **********
    // mira si cal reiniciar temps torn
    mode_rearmar = hay_que_rearmar(REARMAR_SEGON_TORN, 1);
    if(mode_rearmar)
       {
        rearmar_bloqueo_torn(mode_rearmar, 0);
       }
    bloqueo_torn.estat_torn = ETORN_INICIAT;
    mogut_en_off_i_fi_franquicia = (bloqueo_torn.dist_off >= tarcom.impulsos_mogut_en_off);
    bloqueo_torn.dist_off = 0;
    
    temps_a_descomptar = 0;
    if((tarcom.num_descans_torn == 0) || (bloqueo_torn.num_descans_fets < tarcom.num_descans_torn)) 
            pot_fer_descans = 1;
    else
            pot_fer_descans = 0;
    if(tarcom.minuts_pas_off_a_lliure)
    {
      // cas ha tardat un temps per passar a lliure
      if(bloqueo_torn.acum_descans >= tarcom.minuts_pas_off_a_lliure)
        bloqueo_torn.acum_descans -= tarcom.minuts_pas_off_a_lliure;
      else
        bloqueo_torn.acum_descans  = 0;	// nomes hauria de passar cas ha rearmat torn
    }
    temps_pas_a_lliure = tarcom.minuts_pas_off_a_lliure;
    if((bloqueo_torn.acum_descans + bloqueo_torn.acum_sense_tensio)==0)
    {
      ; 
    }
    else
    {
      if(((bloqueo_torn.acum_descans + bloqueo_torn.acum_sense_tensio) >= tarcom.minuts_descans)
         && (!mogut_en_off_i_fi_franquicia) && pot_fer_descans)
      {       
        // compta temps en OFF com descans
        // pero si dins d'aquest temps hi ha hagut temps sense tensio el descompta
        if(bloqueo_torn.acum_sense_tensio)
          temps_a_descomptar = bloqueo_torn.acum_sense_tensio;
        // actualitza numero descans fets
        if(tarcom.num_descans_torn && (bloqueo_torn.num_descans_fets < tarcom.num_descans_torn))
              bloqueo_torn.num_descans_fets++;			
      }
      else
        {
         // descompta temps descans (off + sense tensio)
         temps_a_descomptar = bloqueo_torn.acum_descans + bloqueo_torn.acum_sense_tensio;
         if(tarcom.sanciona_descans_curt && pot_fer_descans)
           {
             if(temps_a_descomptar < tarcom.minuts_descans)
               temps_a_descomptar=tarcom.minuts_descans;
           }
        }
    }
    // registra final descans ( compti o no compti com a descans fet)
    registrar_final_descans(minut_del_segle_from_time(&gt.time));
    // actualitza temps torn 
	if(!inSecondInterval())
	{
		temps_a_descomptar += temps_pas_a_lliure;
		if(bloqueo_torn.minuts_queden > temps_a_descomptar)
		  bloqueo_torn.minuts_queden -= temps_a_descomptar;
		else
		  bloqueo_torn.minuts_queden=0;
	}
    bloqueo_torn.acum_descans = 0;
    bloqueo_torn.acum_sense_tensio = 0;
    bloqueo_torn.dist_off=0;
 }
}
  
extern void paso_off(void);
extern int hay_cargador(void);



#define ERR_MATEIX_DIA                  1
#define ERR_POC_TEMPS_DESDE_TANCAMENT   2
#define ERR_POC_TEMPS_DESCANS           3
#define ERR_FORA_INTERVAL               4
#define ERR_FORA_TORN_PREPROG           5
#define ERR_DESCANS_PREPROG             6


//
// mira si esta fora d'interval apertura torn
//
int apertura_torn_fora_interval(void)
{
  if((tarcom.hora_minim_ini_torn == 0)&&(tarcom.hora_maxim_ini_torn == 0))
    return 0; // no hi ha limitacio interval
  if(!dins_interval(gt.minut_del_dia, tarcom.hora_minim_ini_torn, tarcom.hora_maxim_ini_torn))
      return 1;
  return 0;  
}


int poc_temps_desde_tancament(int* minuts_falten)
{
int iaux;
  if(bloqueo_torn.minut_tancament == 0)
    return(0);    // es primer torn despres de carregar tarifa i no es verifica
  // calcula minut actual desde inici segle
  iaux = minut_del_segle_from_time(&gt.time)- bloqueo_torn.minut_tancament;
  if(iaux  < tarcom.interval_entre_torns)
  {
    *minuts_falten = tarcom.interval_entre_torns - iaux;
    return(1);
  }
  return(0);  
}

// 
// nomes es crida cas fran�a
//
int descans_massa_curt(int* minuts_falten)
{
int td;  
  if(tarcom.sanciona_descans_curt != 2)
    return(0);
  td =  minut_del_segle_from_time(&gt.time) - bloqueo_torn.minut_segle_inici_descans;
  if( td < tarcom.minuts_descans)
  {
    *minuts_falten = tarcom.minuts_descans - td;
    return(1);
  }
  return(0);
}

int bloqueig_torn_prou_descansos(void)
{
  if((tarcom.num_descans_torn) &&
     (bloqueo_torn.num_descans_fets >= tarcom.num_descans_torn) &&
     (tarcom.max_descans_fets == 1)) 
     return(1);
  
  // cas fran�a
  // verifica que cas no surt de descans fins haver fet temps minim, quedi prou temps de descans
  // tambe verifica que no hagi acabat temps del torn
  //
  if(tarcom.torn_continu == 1)
  {
    if(((tarcom.sanciona_descans_curt == 2) &&
        (bloqueo_torn.minuts_descans_queden < tarcom.minuts_descans))
       || (bloqueo_torn.minuts_queden == 0))
    return(1);
  }
  return(0);
  
}

//
// determina hora inici proper torn preprogramat
// nomes es crida aquesta funcio desde fora de torn
//  
short hora_inici_next_torn_preprog(void)
{
short i,nt;
  nt = 0;
  for(i=0;i<NUM_MAX_TORNS;i++)
    if(gt.minut_del_dia <= bloqueo_torn.preprog_hora_inici_torn[i])
    {
      nt = i;
      break;
    }
  return(bloqueo_torn.preprog_hora_inici_torn[nt]);
}


void visu_err_i_seguir_en_off(int err, int minuts_falten)
{
long laux;  
  bloqueo_torn.estat_torn = 1;      // temporitzat  visu error i retorn a OFF
  switch(err)
  {
    case ERR_MATEIX_DIA:
       if(tarcom.visu_dia_torn == 1)
       {
         laux = next_dia_any();
         display(2,NF_TOT4,(void*)&laux);   // dia torn en format dia de l'any
       }
       else //if(tarcom.visu_dia_torn == 2)
       {
         laux = next_dia_mes();
         display(2,NF_TOT4,(void*)&laux);   // dia torn en format mes/dia
       }
       break;
    case ERR_POC_TEMPS_DESDE_TANCAMENT:
    case ERR_POC_TEMPS_DESCANS:
       laux = (bcd_bin(gt.time.hour)*60L + bcd_bin(gt.time.minute) + minuts_falten)% 1440;
       display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
       break;
    case ERR_FORA_INTERVAL:
       laux = tarcom.hora_minim_ini_torn;
       display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
       break;
    case ERR_FORA_TORN_PREPROG:
       laux = hora_inici_next_torn_preprog();
       display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
       break;
    case ERR_DESCANS_PREPROG:
       laux = bloqueo_torn.preprog_hora_final_descans[bloqueo_torn.zona_descans]+1;
       if(laux >= 24*60)
         laux = 0;
       display(1,NF_MINUTS_TO_HH_MM_PUNT_INTERM,&laux);
       break;
  }
  
// display(2,NF_TEXT4,&tarcom.text_lib[6]);
  if(tarcom.tipus_teclat==2)
     display(0,NF_LEDEST,&tarifa[tx30.tarifa_rot].ledocup);
  else
     display(0,NF_LEDEST,&tarcom.vars_estat[E_LIBRE].ledest);
  DISP_FLASH(0);DISP_FLASH(1);DISP_FLASH(2);          
  bloqueo_torn.stdesblq=1;
  timer50=20;
  bloqueo_torn.cntdesblq = SEGONS_VISU_ERROR;
}


//
// determina si esta dins o fora de zona de torn (cas torn predeterminat)
// retorn:
//         0  fora de torn
//         != 0  numero de torn ( 1,2,...)
int get_zona_torn(void)
{
int i;
int dins = 0;

  for(i=0;i<NUM_MAX_TORNS;i++)
  {
    if(dins_interval(gt.minut_del_dia, bloqueo_torn.preprog_hora_inici_torn[i],bloqueo_torn.preprog_hora_final_torn[i]))
    {
      dins = i+1;
      break;
    }
  }
  return dins;
}
//
// determina si esta dins o fora de zona de descans (cas torn predeterminat)
// retorn:
//         0  fora de descans
//         != 0  numero de descans ( 1,2,...)
int get_zona_descans(void)
{
int i;
int dins = 0;

  for(i=0;i<DIM(bloqueo_torn.preprog_hora_inici_descans);i++)
  {
    
    if(dins_interval(gt.minut_del_dia, bloqueo_torn.preprog_hora_inici_descans[i],bloqueo_torn.preprog_hora_final_descans[i]))
    {
      dins = i+1;
      break;
    }
  }
  return dins;
}

int OFF_LIBRE_bloqueig_torn(void)
{
int err;
int minuts_falten;
int i;
int zona_descans;
int exit;
int temps_en_descans;
int minut_segle_ara;

 if(hay_cargador())
   return(0);
 
    if(tarcom.torn_continu == 0)
    {
        // cas torn classic
        if( tarcom.minuts_pas_off_a_lliure &&
        (tarcom.bloqueo_torn==2 ||((tarcom.bloqueo_torn==1) && !sext))
          && !bloqueo_torn.dins_dates_desactivacio_torn)
        {
         if(bloqueo_torn.stdesblq == 0)
           {
            display(1,NF_TEXT6,&tarcom.text_lib[0]);
            display(2,NF_TEXT4,&tarcom.text_lib[6]);
            if(tarcom.tipus_teclat==2)
               display(0,NF_LEDEST,&tarifa[tx30.tarifa_rot].ledocup);
            else
               display(0,NF_LEDEST,&tarcom.vars_estat[E_LIBRE].ledest);
            DISP_FLASH(0);
            DISP_FLASH(1);
            DISP_FLASH(2);
            bloqueo_torn.stdesblq=1;
            timer50=20;
            bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*60;
#if DEBUG_NO_CHECK            
                bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*5;
#endif
        //new_estat=E_OFF;  2.07
            return(1);
           }
         if(bloqueo_torn.stdesblq==1)
           {
            // new_estat=E_OFF;  2.07
            timer50=0;    // versio 2.12
            paso_off();
            return(1);
           }
         // cas 2
            //final intermitencia i pas a LIBRE
        }
        return(0);
    }
    
    else
    {
      // cas torn Fran�a              
      do{
          exit = 1;          
          switch(bloqueo_torn.estat_torn)
          {
          case 0:   // torn tancat                        
             if(tarcom.minuts_pas_off_a_lliure == 0)
                bloqueo_torn.stdesblq = 0;
             switch(bloqueo_torn.stdesblq)
             {
             case 0:           
                // verifica es compleixen condicions per obrir torn
                err = 0;
                if(bloqueo_torn.events_preprog)
                {
                    if(get_zona_torn() == 0)
                    {
                      err = ERR_FORA_TORN_PREPROG;
                    }
                    else 
                    {
                      zona_descans = get_zona_descans();
                      if(zona_descans)
                      {
                        bloqueo_torn.zona_descans = zona_descans - 1;
                        err = ERR_DESCANS_PREPROG;
                      }
                    }        
                }
                else if(apertura_torn_fora_interval())
                  err = ERR_FORA_INTERVAL;
                else if((bloqueo_torn.torn_en_curs == 0) || (bloqueo_torn.torn_en_curs >= tarcom.num_torns_control_horari))
                {
                  // cas es primer torn del dia. Verifica dia apertura anterior i temps desde tancament
                    if(gt.anydia == bloqueo_torn.anydia_last_torn)
                      err = ERR_MATEIX_DIA; // encara es el mateix dia.No pot obrir
                    else if(poc_temps_desde_tancament(&minuts_falten))
                      err = ERR_POC_TEMPS_DESDE_TANCAMENT; // encara no ha passat prou temps.No pot obrir
                } 
                if(err)
                {
                  bloqueo_torn.estat_torn_save = 0;
                  visu_err_i_seguir_en_off(err,minuts_falten);
                  return(1);          
                }          
                // cas pot iniciar apertura torn
                if(tarcom.minuts_pas_off_a_lliure)
                {
                    display(1,NF_TEXT6,&tarcom.text_lib[0]);
                    display(2,NF_TEXT4,&tarcom.text_lib[6]);
                    if(tarcom.tipus_teclat==2)
                       display(0,NF_LEDEST,&tarifa[tx30.tarifa_rot].ledocup);
                    else
                       display(0,NF_LEDEST,&tarcom.vars_estat[E_LIBRE].ledest);
                    DISP_FLASH(0);
                    DISP_FLASH(1);
                    DISP_FLASH(2);
                    bloqueo_torn.stdesblq=1;
                    timer50=20;
                    bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*60;
    #if DEBUG_NO_CHECK            
                    bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*5;
    #endif
                    //new_estat=E_OFF;  2.07
                    return(1); 
                }
                break;
             case 1:
                // tecla 0 mentres temporitzava pas a Lliure
                // new_estat=E_OFF;  2.07
                timer50=0;    // versio 2.12
                paso_off();
                return(1);
             case 2:
                // final temporitzat  pas a lliure
                break;
             }
                                
            // arriba aqui cas no hi ha temporitzat o ja s'ha acabat
            // apertura de torn (cas France)        
            bloqueo_torn.estat_torn = 2;
            if(bloqueo_torn.torn_en_curs < tarcom.num_torns_control_horari)
              bloqueo_torn.torn_en_curs++;
            else
              bloqueo_torn.torn_en_curs = 1;
            if(bloqueo_torn.torn_en_curs == 1)
            {
              // cas primer torn
              bloqueo_torn.anydia_last_torn = gt.anydia;
              bloqueo_torn.diames_last_torn = bcd_bin(gt.time.day)*100 + bcd_bin(gt.time.month);        
            }
            bloqueo_torn.num_descans_fets = 0;
            bloqueo_torn.num_descans_registrar = 0;
            bloqueo_torn.minuts_descans_queden = tarcom.minuts_maxim_descans;
            bloqueo_torn.minuts_queden = tarcom.minuts_max_torn_1x2;
            
            // actualitza index registre torn
            bloqueo_torn.index_registre_torn++;
            if(bloqueo_torn.index_registre_torn >= NUM_MAX_TORNS)
              bloqueo_torn.index_registre_torn = 0;
            
            // registre inici torn
            bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].anydia_torn = gt.anydia & 0x1ff; // dia del any
            bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].diames_torn = bcd_bin(gt.time.day)*100 + bcd_bin(gt.time.month);
            bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].hora_inici_torn = minut_del_segle_from_time(&gt.time);
            for(i=0;i<NUM_MAX_DESCANS_REGISTRAR;i++)
            {
              bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].registre_descans[i].hora_inici_descans = 0;
              bloqueo_torn.registre_torn[bloqueo_torn.index_registre_torn].registre_descans[i].hora_final_descans = 0;
            }
            return(0);
          case 1:
             // arriba aqui ja sigui per tecla Lliure o per haver acabat temps visu error 
            timer50=0;    // versio 2.12
            bloqueo_torn.estat_torn = bloqueo_torn.estat_torn_save; // 0 o 2
            paso_off();
            return(1);
          case 2:
            // torn esta obert i esta en  descans
            
             if(tarcom.minuts_pas_off_a_lliure == 0)
                bloqueo_torn.stdesblq = 0;
             switch(bloqueo_torn.stdesblq)
             {
             case 0:           
                // verifica es compleixen condicions per sortir de descans
            
                if(bloqueo_torn.events_preprog)
                {
                   zona_descans = get_zona_descans();
                   if(zona_descans)
                   {
                    bloqueo_torn.estat_torn_save = 2;
                    bloqueo_torn.zona_descans = zona_descans - 1;
                    visu_err_i_seguir_en_off(ERR_DESCANS_PREPROG, minuts_falten);
                    return(1);
                   }
                }    
                // mira si temps de descans arriba a temps m�nim
                else if(descans_massa_curt(&minuts_falten))
                {
                  // cas no pot sortir de OFF
                  bloqueo_torn.estat_torn_save = 2;
                  visu_err_i_seguir_en_off(ERR_POC_TEMPS_DESCANS, minuts_falten);
                  return(1);
                }
                // cas pot finalitzar descans (pas a Lliure)
                if(tarcom.minuts_pas_off_a_lliure)
                {
                    display(1,NF_TEXT6,&tarcom.text_lib[0]);
                    display(2,NF_TEXT4,&tarcom.text_lib[6]);
                    if(tarcom.tipus_teclat==2)
                       display(0,NF_LEDEST,&tarifa[tx30.tarifa_rot].ledocup);
                    else
                       display(0,NF_LEDEST,&tarcom.vars_estat[E_LIBRE].ledest);
                    DISP_FLASH(0);
                    DISP_FLASH(1);
                    DISP_FLASH(2);
                    bloqueo_torn.stdesblq=1;
                    timer50=20;
                    bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*60;
    #if DEBUG_NO_CHECK            
                    bloqueo_torn.cntdesblq=tarcom.minuts_pas_off_a_lliure*5;
    #endif
                    //new_estat=E_OFF;  2.07
                    return(1);          
                 }
                break;
             case 1:
                // tecla 0 mentres temporitzava sortida de descans
                // new_estat=E_OFF;  2.07
                timer50=0;    // versio 2.12
                paso_off();
                return(1);
             case 2:
               //final intermitencia i pas a LIBRE
               break;
             }
             
            // arriba aqui cas no hi ha temporitzat o ja ha acabat
             
            // determina si ha rebassat temps que quedava de descans i
            // s'ha acabat el temps del torn
            minut_segle_ara =  minut_del_segle_from_time(&gt.time);
            temps_en_descans = minut_segle_ara - bloqueo_torn.minut_segle_inici_descans;
            if(temps_en_descans <= bloqueo_torn.minuts_descans_queden)
            {
              bloqueo_torn.minut_segle_final_descans = minut_segle_ara;
              bloqueo_torn.minuts_descans_queden -= temps_en_descans;
            }
            else
            {
              // cas exhaurit temps de descans
              bloqueo_torn.minut_segle_final_descans = bloqueo_torn.minut_segle_inici_descans + bloqueo_torn.minuts_descans_queden;
              temps_en_descans -= bloqueo_torn.minuts_descans_queden;
              bloqueo_torn.minuts_descans_queden = 0;
              if(temps_en_descans <= bloqueo_torn.minuts_queden)
              {
                // descompta temps del torn que ja estava fora de descans
                bloqueo_torn.minuts_queden -= temps_en_descans;
              }
              else
              {
                // desde final descans s'ha acabat temps que quedava del torn
                // posa indicador per obrir torn
                exit = 0;  
              }
            }
              
            // primer acaba descans
            registrar_final_descans(bloqueo_torn.minut_segle_final_descans);    // registra final descans
            bloqueo_torn.num_descans_fets++;
            
            if(exit == 0)  // i si cal
            {
              //  tanca torn
              bloqueo_torn.minut_tancament = minut_segle_ara - (temps_en_descans - bloqueo_torn.minuts_queden);
              france_tancar_torn(bloqueo_torn.minut_tancament);
              bloqueo_torn.stdesblq=0;  // per que no passa per off
            }
            if(exit)
               return(0);
            //  repetir switch i procedir a obrir torn
          }   //switch(bloqueo_torn.estat_torn)
          
      }while(!exit);  // per repetir proces i obrir torn  (cas pasa de descans a torn tancat ) 
    }
    return 0;
}


//
// gestion bloqueo turno al dar tension
// o al sortir de baix consum  (es fa a traves de salta_a_reset())
//
void gestio_bloqueo_torn_0(struct s_time *time_fallo_tensio, unsigned long segons_off)
{
int  minuts_ini,minuts_fin;
unsigned short minuts_off, temps_a_descomptar;
int rearmar;

 if(tarcom.bloqueo_torn)
   {
#if DEBUG_NO_CHECK
  OS_Delay(50); // per parar buzzer
#endif
    if(tarcom.torn_continu == 0)
    {
        rearmar = 0; 
        if((tarcom.bloqueo_torn==1 && sext) 
           || bloqueo_torn.desactivat_x_cap_setmana
           || bloqueo_torn.dins_dates_desactivacio_torn
		   || bloqueo_torn.tornDesactivatDiaEspecial)
		
               rearmar = 1;  // cas desactivat per senyal ext o cap de setmana
        else
          rearmar = hay_que_rearmar(REARMAR_GLOBAL, 0);  // mira si cal reiniciar temps torn
        if(rearmar)
              {
               rearmar_bloqueo_torn(REARMAR_GLOBAL, 1);
               return;
              }
        if(segons_off < SEGONS_MINIM_EN_OFF)
            return;// si menys temps en OFF ho considera un microtall i passa
        
        //calcul temps sense tensio
        minuts_ini = (unsigned short)(bcd_bin(time_fallo_tensio->hour))*60 + (unsigned short)bcd_bin(time_fallo_tensio->minute);
        minuts_fin = (unsigned short)(bcd_bin(gt.time.hour))*60 + (unsigned short)bcd_bin(gt.time.minute);
        if(minuts_fin<minuts_ini)minuts_fin+=1440;
        minuts_off = minuts_fin - minuts_ini;   // temps apagat/baix consum
        
        if(esta_en_off()) 
         {
           if(bloqueo_torn.ve_de_baix_consum)
           {
             switch(bloqueo_torn.estat_torn)
               {
                 case ETORN_NO_INI:
                 case ETORN_FRANQUICIA_DIST:
                   break;
                 case ETORN_FRANQUICIA_EXHAURIDA:
                   // decrementa contador temps
                   if(bloqueo_torn.minuts_queden >= minuts_off)
                      bloqueo_torn.minuts_queden -= minuts_off;
                   else
                      bloqueo_torn.minuts_queden = 0;
                   test_bloqueo_turno();
                   break;
                 case ETORN_INICIAT:
                    bloqueo_torn.acum_descans += minuts_off;
                    break;
               }
           }
           else
           {
             switch(bloqueo_torn.estat_torn)
               {
                 case ETORN_NO_INI:
                 case ETORN_FRANQUICIA_DIST:
                   break;
                 case ETORN_FRANQUICIA_EXHAURIDA:
                 case ETORN_INICIAT:
                   // incrementa contador temps sense tensio
                    bloqueo_torn.acum_sense_tensio += minuts_off;
                    break;
               }
           } 
         } 
        else 
         {
           // cas no esta en OFF
			 if(!inSecondInterval()){
			   temps_a_descomptar = minuts_off;
			   if(tarcom.sanciona_fallo_tensio)
			   {
				   if(temps_a_descomptar < tarcom.minuts_descans)
					 temps_a_descomptar=tarcom.minuts_descans;
			   }
			   if(bloqueo_torn.minuts_queden > temps_a_descomptar)
				  bloqueo_torn.minuts_queden -= temps_a_descomptar;
			   else
				  bloqueo_torn.minuts_queden=0;
			 }
         }
        bloqueo_torn.ve_de_baix_consum = 0;
       }
    else
    {
      // cas torn continu (Fran�a)
      if(segons_off < SEGONS_MINIM_EN_OFF)
        return;// si menys temps en OFF ho considera un microtall i passa
      
      //calcul temps sense tensio
      minuts_ini = minut_del_segle_from_time(time_fallo_tensio);
      minuts_fin = minut_del_segle_from_time(&gt.time);
      minuts_off = minuts_fin - minuts_ini;   // temps apagat/baix consum
        if(esta_en_off()) 
         { 
/*           
          if(esta_en_descans())
            {
              bloqueo_torn.acum_descans += minuts_off;            

                   if(tarcom.minuts_maxim_descans && (bloqueo_torn.minuts_descans_queden == 0))
                   {
                     // cas ha superat temps maxim descans.Descompta temps torn
                     if(bloqueo_torn.minuts_queden)
                       bloqueo_torn.minuts_queden--;
                   }
                   else
                   {
                     bloqueo_torn.acum_descans++;
                     if(bloqueo_torn.minuts_descans_queden)
                          bloqueo_torn.minuts_descans_queden--;
                   }            
            }
*/           
         }
        else
        {
           // cas no esta en OFF
           if(bloqueo_torn.minuts_queden > minuts_off)
              bloqueo_torn.minuts_queden -= minuts_off;
           else
              bloqueo_torn.minuts_queden=0;
        }
    }
   } 
}

int inSecondInterval(void) {
	return ((tarcom.hora_ini_fr2 != 0 || tarcom.hora_fin_fr2 != 0)) && dins_interval(gt.minut_del_dia,tarcom.hora_ini_fr2,tarcom.hora_fin_fr2);
}

int shiftControlDeactivated(void) {
	return (bloqueo_torn.dins_dates_desactivacio_torn || bloqueo_torn.tornDesactivatDiaEspecial || bloqueo_torn.desactivat_x_cap_setmana || inSecondInterval());
}

extern void display_1_en_libre(int err);

//
// gestio bloqueig torn per temps
//
void bloqueo_turno_minuto(void)
{
char buf_luz[2];
int mode_rearmar;
int zona_torn,zona_descans;

#if DEBUG_MODIF_RELOJ
    debug_reloj();  // per debug (modifica rellotge a traves variable var_debug modificada desde l 'ICE)
#endif

     if(tarcom.bloqueo_torn == 0)
        return;
     if(bloqueo_torn.prescindir_canvi_minut)
     {
       // cas ja ha fet el canvi de minut per tornada de tensio
       bloqueo_torn.prescindir_canvi_minut = 0;
       return;
     }
     if(tarcom.torn_continu == 0)
     {    
         if(bloqueo_torn.desactivat_x_cap_setmana)
         {
           rearmar_bloqueo_torn(REARMAR_GLOBAL, 0);
           return;
         }
       
         if(esta_en_off())   
           {
            // gestion bloqueo turno cas OFF
               mode_rearmar = hay_que_rearmar(REARMAR_GLOBAL, 0);
               if(mode_rearmar || bloqueo_torn.dins_dates_desactivacio_torn || bloqueo_torn.tornDesactivatDiaEspecial)
                  {
                   rearmar_bloqueo_torn(mode_rearmar, 0);
                  }
               switch(bloqueo_torn.estat_torn)
                 {
                   case ETORN_NO_INI:
                   case ETORN_FRANQUICIA_DIST:
                     break;
                   case ETORN_FRANQUICIA_EXHAURIDA:
                     // decrementa contador temps
                     if(bloqueo_torn.minuts_queden)
                      {
                       --bloqueo_torn.minuts_queden;
                       test_bloqueo_turno();
                      }
                     break;
                   case ETORN_INICIAT:
                      bloqueo_torn.acum_descans++;
                      break;
                 }
           }    
         else
           {
            if((tarcom.bloqueo_torn==1 && sext) || bloqueo_torn.dins_dates_desactivacio_torn || bloqueo_torn.tornDesactivatDiaEspecial)
              {
               // cas desactivat per senyal ext o per fora temporada torn
               rearmar_bloqueo_torn(REARMAR_GLOBAL, 0);
              }
            else
              {
               // mira si cal reiniciar temps torn
               mode_rearmar = hay_que_rearmar(REARMAR_GLOBAL, 0);
               if(mode_rearmar)
                  {                       
                   rearmar_bloqueo_torn(mode_rearmar, 0);
                  }
               else
                  {
                   // decrementa contador temps
                   if(bloqueo_torn.minuts_queden)
                    {
                     --bloqueo_torn.minuts_queden;
                    }
                  } 
               test_bloqueo_turno();
              }
            if(tx30.estat==E_LIBRE)
              {
               select_luz(E_LIBRE,buf_luz);
               out_luzex(buf_luz[0],buf_luz[1]);
                     
               display_1_en_libre(0);
                      
               if(tx30.acum_off)
                {
                  // cas espera per passar a OFF
                  DISP_FLASH(0);
                  DISP_FLASH(1);
                  DISP_FLASH(2);
                }
             }
           }
     }
     else
     {
         // cas Fran�a
         if(bloqueo_torn.esta_passant_itv)
         {
           if(bloqueo_torn.minuts_passant_itv)
             bloqueo_torn.minuts_passant_itv--;
         }
         if(bloqueo_torn.events_preprog)
         {
           // cas hi ha events preprogramats. Mira si ha d'activar/desactivar torn/ descans
           if((!esta_en_off()) && (!esta_fent_un_servei()))
           {             
            // si esta en OFF (descans o torn tancat) o en mig d'un servei no fa res
             zona_torn = get_zona_torn();
             if(zona_torn == 0)
             {
               // cas ha sortit de l'interval de torn
                 france_tancar_torn(minut_del_segle_from_time(&gt.time));
                 tx30.new_estat=E_OFF;  // 2.07
                 paso_off();
                 tx30.estat=E_OFF;  // ja que el canvi d'estat no es fa a traves d'una transicio
             }
             else
             {
               // mira si esta en zona descans
               zona_descans = get_zona_descans();
               if(zona_descans)
               {
                 // inicia descans
                 registrar_inici_descans();
                 tx30.new_estat=E_OFF;  // 2.07
                 paso_off(); 
                 tx30.estat=E_OFF;  // ja que el canvi d'estat no es fa a traves d'una transicio
               }
             }
           }
           
                      
         }
         else
         {
           // cas no hi ha preprogramacio events
             if(!esta_en_off())
               {
                  if(bloqueo_torn.minuts_queden)
                    {
                     --bloqueo_torn.minuts_queden;
                    }
               }
         }
      if(tx30.estat == E_LIBRE)   
          display_1_en_libre(0);  // per cas minuts queden == 0 o <11
     }    // final cas Fran�a
}

void bloqueo_turno_hm(void)
{
  if(tarcom.torn_continu == 0)
  {
     if(tx30.estat == E_OFF )
     {
       switch(bloqueo_torn.estat_torn)
       {
        case ETORN_FRANQUICIA_DIST:
          bloqueo_torn.dist_franquicia++;  //per veure si rebasa franquicia
          if(bloqueo_torn.dist_franquicia >= tarcom.hm_off_torn)
            {
             bloqueo_torn.dist_off = 0;
             bloqueo_torn.estat_torn = ETORN_FRANQUICIA_EXHAURIDA;
            }
          break;
       }   
     }
  }
}

void bloqueo_turno_10seg(void)
{
 unsigned char on_off;
 unsigned char minuts_visu_rep[2];

 if(tarcom.bloqueo_torn && !(gt.time.sec&0x0f) )
   {
    // cada 10"
    if(esta_en_off())
      on_off=0;    // apagat
    else
     {
      if((bloqueo_torn.minuts_queden==0) &&((tx30.estat!=E_OCUPADO)&&(tx30.estat!=E_PAGAR)))
        on_off=0x01; // intermitent
      else  
        on_off=0x03; // fixe
     } 

    minuts_visu_rep[0] = bin_bcd((char)(bloqueo_torn.minuts_queden%60));
    minuts_visu_rep[1] = bin_bcd((char)(bloqueo_torn.minuts_queden/60));
    write_i2c(CONTROL_HORARI,on_off,2,(char *)&minuts_visu_rep[0]);// per engegar/apagar
   }
}   


int processar_transmissio_control_torn(char* buf_resp){
	unsigned long laux;
	if(tarcom.bloqueo_torn)
		laux = bloqueo_torn.minuts_queden;							
	else
		laux = 0;
	memcpy(buf_resp,(char*)&laux,4);
	return 4;
}

void descans(void){
	if(tarcom.acumula_franq && bloqueo_torn.dist_franquicia < tarcom.hm_off_torn)
		bloqueo_torn.estat_torn = ETORN_FRANQUICIA_DIST;
}