//*********************************************************
// reloj.h
//*********************************************************

#ifndef _INC_RELOJ
#define _INC_RELOJ

#define SEGONS_DIA 	86400	// 3600*24
#define MINUTS_DIA  1440    // 60*24


typedef struct s_time
{
 unsigned char s100;
 unsigned char sec;
 unsigned char minute;
 unsigned char hour;
 unsigned char day;
 unsigned char month;
 unsigned char year;
 unsigned char year100;
}t_time;

typedef struct s_time8563
{
 unsigned char sec;
 unsigned char minute;
 unsigned char hour;
 unsigned char day;
 unsigned char weeks;
 unsigned char month;
 unsigned char year;
 unsigned char year100;
}s_time8563;



typedef struct s_ajuste_hor
{
    unsigned char tope[5];  /* hora dia mes any 100any (en bcd ) */
    unsigned char tipo;     /* 0,1  avan/retras */
}t_ajuste_hor;

typedef struct s_fechhor
{
 unsigned char minute;
 unsigned char hour;
 unsigned char day;
 unsigned char month;
 unsigned char year;
 unsigned char sec;    // per ajustar a int
}T_FECHHOR;

struct s_rellotge
{
int primera_lectura;
int segons_lectura_anterior;
};

#define NUM_AJUSTS_HORA_OLD 6   // n�mero d'ajusts hora que es guardaven  a tarifa (abans de versio 3.05)
#define NUM_AJUSTS_HORA 20      // n�mero total d'ajusts hora que es guarden a tarifa

extern struct s_rellotge rellotge;
extern t_ajuste_hor ajuste_hor[NUM_AJUSTS_HORA];


void reloj_init(void);    // inicialitza semafor rellotge

extern char get_confi_reloj(int nChip);
extern void confi_reloj(int nChip, char mask);

extern void set_reloj(int nChip,struct s_time *time);
extern void get_reloj(int nChip,struct s_time *buf);
extern void get_reloj_fechhor(int nChip,T_FECHHOR *buf);

extern void time_to_fechhor(T_FECHHOR* dest, t_time* org);

extern char get_dias_mes(unsigned char month,unsigned char year);
extern char fecha_logica(T_FECHHOR *f);

extern void tancar_rellotge(void);
extern int minuts_interval(T_FECHHOR* ffin,T_FECHHOR* fini);
extern unsigned long segons_interval(T_FECHHOR* ffin,T_FECHHOR* fini);

extern unsigned char gestio_ajustar_1h(void);
extern void inici_ajustar_1h(void);
extern int check_data_caducitat(char *buf_cc, unsigned char* pmargen);
extern int check_caducitat(char *buf_cc);
extern int check_efectivitat(char *buf_cc);
extern int caducidad_FE(unsigned char* p, unsigned char* pmargen);
extern unsigned int get_dia_del_anyo(T_FECHHOR* fe);
extern void sumar_dias_a_fecha(T_FECHHOR* fe, int numdias);
extern int fecha_hora_vencida(T_FECHHOR* ahora,T_FECHHOR* fecha);



extern unsigned char dia_de_la_semana_from_time(struct s_time *time);
extern unsigned int dia_del_any_from_bcd(unsigned char* dmy);
extern int minut_del_segle_from_time(t_time* time);
extern unsigned int dia_del_segle_from_bcd(unsigned char * dmy);
extern void get_data_from_minuts(long minuts, T_FECHHOR* fecha);

extern void suma_minuts_a_hora_minut(unsigned long minuts,struct s_time* time, unsigned char* dst_minute);
extern void minuts200_to_hhmm(long minuts, unsigned char* buf);
extern void minuts200_to_hhmm_reversed(long minuts, unsigned char* buf);

extern unsigned long getSegundosFromFecha(T_FECHHOR *pfecha);
extern signed long segonsDelMes(unsigned char cMonth, unsigned char nYear);
extern signed long segonsDelSemestre(int num, unsigned char nYear);
extern signed long segonsDelAny(unsigned char nYear);
extern void getFechaFromSegundos(unsigned long lSegundos, T_FECHHOR *pfecha);

#endif
