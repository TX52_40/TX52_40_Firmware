//*********************************************************************
// impre.c
//*********************************************************************

#include <stdio.h>

#include "iostr710.h"

#include "hard.h"
#include "RTOS.H"
#include "adc.h"
#include "regs.h"
#include "impre.h"
#include "spi.h"
#include "serial.h"
#include "multiplex.h"
#include "debug.h"
#include "qrcodegen.h"
#include "rutines.h"

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")




#define IMPR_TIMEOUT_RESP_STATUS   200   // 200 ms

#define IMPRE_TIME_PREG_STATUS_FAST  2   // 2 ms
#define IMPRE_TIME_PREG_STATUS_SLOW 40   // 40 ms


#define DIM(array)  (sizeof(array)/sizeof(array[0]))

#define IMPRE_OFF   {OS_IncDI(); PORT4 |= 0x80;IOPORT_4 = PORT4;OS_DecRI();}    // MOTOR OFF
#define IMPRE_ON    {OS_IncDI(); PORT4 &= 0x7f;IOPORT_4 = PORT4;OS_DecRI();}    // MOTOR ON

#define LED_ON      {OS_IncDI();PORT3 |= 0x80;IOPORT_3 = PORT3;OS_DecRI();}
#define LED_OFF     {OS_IncDI();PORT3 &= 0x7f;IOPORT_3 = PORT3;OS_DecRI();}

#define STROBES_OFF {OS_IncDI();PORT3 &= 0xc0;IOPORT_3 = PORT3;OS_DecRI();}


#define PAPEL           (IOPORT1_PD_bit.no3)

#define EVENTIMPRE_PREGUNTAR_STATUS   0x01
#define EVENTIMPRE_STATUS_RCVD        0x02
#define EVENTIMPRE_TIMEOUT_STATUS     0x04

#define MAX_LEN_BUFFER_IR32  256
#define MAX_LEN_ENVIAMENT     65    // cas linia de 64 caracters

                 
// preparacion linea impresora ( imprime_low_level )

#define ANA 2
#define AVR 3
#define FIN 4
#define END 5
#define ASC 6
#define QR 8
#define FS 9    // FontSize
#define LF 10
#define GR 11
#define FF 12
#define CR 13
#define FI 14 // Fi tiquet
#define IN 15 // Ini tiquet

#define RES 17
#define RAVR 25
#define AVL 27
#define AV_1   28



#define TIK1LF  0       // tickets interns a impre.c
#define TIK1FF  1
#define TIK1ANA 2
#define TIKRAM  3
#define TIK1_QRRAM 4
#define N_TIK   5

#define PRINTER_RESOLUTION_MAX 384
#define PRINTER_DOT_SIZE 0.125
#define QR_SIZE_MIN 30
#define PAPER_BODER_MIN 20 // eperezr: 0.25 cm de margen
#define MODULE_DOT 2       // eperezr: 2 puntos por modulo

extern unsigned int x_impre(T_TICKET *tck, unsigned int maxlen, unsigned char num_pqt);

unsigned char tikram[32]={ASC,64,0,0,FIN,AVL,END};   //bufer para confeccionar tickets en ram
unsigned char tik_qrram[2048];       // bufer para confeccionar tickets QR en ram

const unsigned char tik1lf[]={AVR,AVR,AVR,END};
const unsigned char tik1ff[]={AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,AVR,END};
const unsigned char tik1ana[]={ANA,48,0,255,END};

const unsigned char  * const TTIK[N_TIK]=
{
   tik1lf,		//  #define TIK1LF  0 	 
   tik1ff,		//  #define TIK1FF  1 	 
   tik1ana,	    //  #define TIK1ANA 2 	 
   tikram,		//  #define TIKRAM  3 	 
   tik_qrram,   //  #define TIK1_QRRAM  4
};

#define RAM_INTERNA _Pragma("location=\"RAM_INTERNA\"")

OS_STACKPTR  RAM_INTERNA int Stack_drvImpre[4096];   /* Task stacks */
OS_TASK TCB_drvImpre;                  /* Task-control-blocks */


extern const unsigned char tab24c_wo[12288];
extern const unsigned char tab24c_wh[12288];
extern const unsigned char tab24c_wg[12288];

extern const unsigned char tab24c_wo_old[12288];
extern const unsigned char tab24c_wh_old[12288];
extern const unsigned char tab24c_wg_old[12288];

extern const unsigned char tab24c_wo_numbits[6144];
extern const unsigned char tab24c_wh_numbits[6144];
extern const unsigned char tab24c_wg_numbits[6144];


extern const unsigned char tab32c_wo[6144];
extern const unsigned char tab32c_wh[6144];
extern const unsigned char tab32c_wg[6144];

extern const unsigned char tab32c_wo_old[6144];
extern const unsigned char tab32c_wh_old[6144];
extern const unsigned char tab32c_wg_old[6144];

extern const unsigned char tabwo[1536];
extern const unsigned char tabwh[1536];
extern const unsigned char tabwg[1536];

#define uint8_t unsigned char
uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];  // eperezr: buffer QR
uint8_t bmpbitQR[bmpQR_BUFFER_LEN_BIT_MAX];  // eperezr: buffer BMP QR en bits

void envia_texte(char* buf, int len, int font_reset);

typedef struct
{
   unsigned char preparar_linea;	//Linea pendiente de preparar
   char imprimint;		        
   unsigned char strobe;		//�ltimo strobe que se ha enviado
   unsigned char fila;			//Fila de puntos que se est� escribiendo de la linea actual
   unsigned char filas;			//N�mero de filas de la matriz de caracteres
   unsigned char cas;			//Tipo de linea escribiendose
   unsigned char st_int;		//Estado del driver de interrupci�n
   unsigned char send;			//Indicador que deja la interrupci�n para saber que tiene que enviar la siguiente lines;
   int paso;
   int pasos;
   unsigned int step;
   int FontSize;
   int pendent_reset_font;
   unsigned char first_step_fila;
   unsigned int  temps_strobes;
   unsigned char num_sortides_strobe;
   unsigned char const* cpunter;
   int hay_impresora;
   char ticket_ret_event;
   char ticket_ok;
   char status;
   int falla_ntc;
   int fallava_ntc;
   int falta_paper;
   int faltava_paper;
   int externa;   // 0/1  impressora integrada a l'equip/ impre externa
   char pendent_preg_status;
   int timer_preg_status;
   int timer_resp_status;
   int timeout_resp_status;
   char ch_rcvd;
   short timer_ticket_no_legal;
   struct s_bitmap
   {
    int en_curs;
    int numcar_linea;
    int numcar_ceros;
    int carWidth;
    char *BMP;
    int y;
   }bitmap;
   int reconfig;
   int parada;
}s_impre;

// variables de la impresora 
s_impre impre;


// Impresora
int motor;
int subtabla;
unsigned int tmo_motor;           // time out per apagar motor
unsigned int tmo_startup_motor;   // time out para considerar si el motor arranca ahora mismo o ya estaba arrancado
                                  // =0-->significa que el motor lleva mucho rato parado y ha de arancar dando unas pasos extras de motor
unsigned char warmuploop;         // Repeticiones de pasos de motor antes de empezar a imprimir despues de m�s de un segundo sin hacerlo
volatile unsigned char timLed;
volatile unsigned char timTemperatura;
int tipoImpresora;
unsigned char ptabla;

RAM_INTERNA unsigned char pxn[6];	// Nombre de pixels de cada strobe per la nova linea.
RAM_INTERNA unsigned char linea[64];    // linea preparada de la impresora */
RAM_INTERNA char linea_grafica[54];    // linea grafica per enviar a impressora externa


unsigned char const* tab7x5;
unsigned char const* tab14x10;
unsigned char const* tab14x10_old;
unsigned char const* tab21x15_old;
unsigned char const* tab21x15;
unsigned char const* tab21x15_numbits;


const unsigned char * const tabpun75[]={tabwo, tabwh, tabwg};
const unsigned char * const tabpun1410_old[]={tab32c_wo_old, tab32c_wh_old, tab32c_wg_old};
const unsigned char * const tabpun1410[]={tab32c_wo, tab32c_wh, tab32c_wg};
const unsigned char * const tabpun2115_old[]={tab24c_wo_old, tab24c_wh_old, tab24c_wg_old};
const unsigned char * const tabpun2115[]={tab24c_wo, tab24c_wh, tab24c_wg};
const unsigned char * const tabpun2115_numbits[]={tab24c_wo_numbits, tab24c_wh_numbits, tab24c_wg_numbits};

enum qr_error_code {
    
    qr_error_code_NoError = 0,
    qr_error_code_BufferCorrupted = 1,
    qr_error_code_InputBufferSize = 2,
    qr_error_code_InputBufferCheckSum = 3,
    qr_error_code_QRencoder = 4,
	qr_error_code_QRsize = 5,  
	qr_error_code_QRscale = 6,  
	qr_error_code_QRborder = 7,
};

//Estados del driver de impresora en la interrupci�n
#define STWAIT 0
#define STSTROBE 1
#define STMOTOR 2

#define _OS_TIMER_IMPRE_ID  T3_GI_VECT_ID   // Assign to TC3 global interrupt

#define _OS_TIMER_IMPRE_PRIO  0x01 
#define _OS_TIMER_IMPRE_INTERVAL  22118


#define T7X5	0   
#define T11X7	1
#define T14X10	2
#define T21X15	3
#define T14X20  4
#define T28X20  5
#define T21X30  6
#define T42X30  7


#define FONTSIZE_DEFAULT  T21X15

struct s_tipo_letra
{
 int pasos ;
 int filas;
 int doble_fila;
 char tipo_externa;
};

const struct s_tipo_letra tipos_letra[] =
{

 7,  7, 0,'D',    // T7X5	  0   
11, 11, 0,'C',    // T11X7	  1
15, 14, 0,'B',    // T14X10	  2
23, 21, 0,'A',    // T21X15	  3
15, 14, 0,'E',    // T14X20   4
30, 28, 1,'F',    // T28X20   5
23, 21, 0,'G',    // T21X30   6
45, 42, 1,'H',    // T42X30   7

};


//Constantes de impresora
//#define TEMPS_MOTOR 2200    //4200   !!!! TV60
#define TEMPS_MOTOR 16219    

const char tabpaso[8]={0x1a,0x08,0x0a,0x22,0x2a,0x38,0x3a,0x12};   // 4 passos fb

//Tabla de tiempo de cabezales en funci�n de la temperatura entre -20 y 70 �C
//Por encima de 70 �C no imprime para que no se estropee.

const unsigned short t_stb[51]=
{
0,0,0,0,0,0,0,61,61,62,
63,65,65,66,68,69,70,71,72,73,
75,77,78,80,81,83,85,86,89,92,
94,96,98,100,102,105,108,112,114,116,
118,121,124,127,131,139,144,148,158,158,
158
};


// TV60  const unsigned char mskb[7]={0,0x80,0x40,0x20,0x10,0x8,0x4};
const unsigned char mskb[6]={0x20,0x10,0x08,0x04,0x02,0x01};


const unsigned char taula_bits[16] = {0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};  

void testTiposLetra(void){
	imprimir_buffer("\x1b\x37\x01\x1b\x38\x02", 6, 0);
	imprimir_buffer("32 caracteres por linea\r\n", 25, 0);
	imprimir_buffer("\x1b\x37\x02\x1b\x38\x02", 6, 0);
	imprimir_buffer("24 caracteres por linea\r\n", 25, 0);
	imprimir_buffer("\x1b\x37\x03\x1b\x38\x03", 6, 0);
	imprimir_buffer("16 caracteres po\r\n", 18, 0);
	imprimir_buffer("\x1b\x37\x02\x1b\x38\x02", 6, 0);
}

void set_timerB1_motor_avance(void)
{
	TIM3_OCAR += TEMPS_MOTOR;
	TIM3_CR1_bit.EN  = 1 ;                 // Start timer
}

void set_timerB1_motor_dibu(void)
{
unsigned int temps;

	if(TEMPS_MOTOR > (impre.temps_strobes + 1000))
		temps =	TEMPS_MOTOR - impre.temps_strobes;
	else 
		temps=1000;
	TIM3_OCAR += temps;
	TIM3_CR1_bit.EN  = 1 ;                 // Start timer
}


// **********************************************************************
// Deteccio temperatura
// **********************************************************************

#define TEMPERATURA_TICKS     5  // per preguntar cada 5ms.
#define LED_TICKS           200  // led paper impressora
OS_TIMER Impre_Timer;
int temperaturaImpre;

#define ADC_TEMPERATURA_IMPRE   0

#if DEBUG_IMPRE_T
char debug_array_status[4000];
int num_pregs_status = 0;
int num_resps_status = 0;
int num_enviaments = 0;
int index_array = 0;
int num_timeouts = 0;
int num_loops_impre = 0;
int num_loops_impre_timed = 0;
int num_status_rcvd = 0;
#endif

#if DEBUG_IMPRE

int num_pregs_status = 0;
int num_resps_status = 0;
int num_ms_impre = 0;
int num_linies_enviades = 0;
int num_enviaments = 0;
int num_on_off = 0;
int num_ms_impre0;


char debug_array[4000][16];
int index = -1;
int prev_var;
void debug_mem_set(int var)
{
int aux_var;

  if(var < 0)
  {
    index = 0;    // inicia array
    memset(debug_array,' ',sizeof debug_array);
    prev_var = -var;
  }
  else if(index >= 0)
  {
    aux_var = var - prev_var;
    prev_var = var;
    debug_array[index][2]= aux_var%10+0x30;
    aux_var /= 10;
    debug_array[index][1]= aux_var%10+0x30;
    aux_var /= 10;
    debug_array[index][0]= aux_var%10+0x30;
    if(index < (4000-1))
      index++;
  }
}

#endif

void ImpreExterna_interrupt_timer(void)    // cada 1ms.
{
  
#if DEBUG_IMPRE  
volatile int dum;  
num_ms_impre++;
#endif

      if(impre.timer_preg_status)
      {
        if(--impre.timer_preg_status == 0)
          OS_SignalEvent(EVENTIMPRE_PREGUNTAR_STATUS,&TCB_drvImpre);
      }
      if(impre.timer_resp_status)
      {
        if(--impre.timer_resp_status == 0)
          OS_SignalEvent(EVENTIMPRE_TIMEOUT_STATUS,&TCB_drvImpre);
      }
      if(impre.timer_ticket_no_legal)
        impre.timer_ticket_no_legal--;
      OS_RetriggerTimer(&Impre_Timer);
}


CODE_FAST void ImpreInterna_interrupt_timer(void)    // cada 1ms.
{
  int st;
#if DEBUG_IMPRE
num_ms_impre++;
#endif
   
        if(--timTemperatura == 0)
        {
          timTemperatura = TEMPERATURA_TICKS;
          temperaturaImpre = adc_read(ADC_TEMPERATURA_IMPRE,&st)/80;  // llegeig conversor
//          if(temperaturaImpre<7)
//          { 
//                  IMPRE_OFF     // IMPRE OFF   
//                  STROBES_OFF
//                  impre.falla_ntc = 1;
//                  impre.fallava_ntc = 1;
//          }
//          else
                  impre.falla_ntc = 0;
        }
        if(timLed > 3)
          timLed--;
        else if((timLed > 0) && (timLed < 3))
          timLed--;
    
        if(tmo_motor)
            {
                    tmo_motor--;
                    if(tmo_motor<10) IMPRE_OFF
                    if(tmo_motor==0) IMPRE_OFF   
            }
        if(tmo_startup_motor>0)
        {
            tmo_startup_motor--;
        }
        
      if(impre.timer_ticket_no_legal)
        impre.timer_ticket_no_legal--;
       
      OS_RetriggerTimer(&Impre_Timer);
}


int set_timerB1_strobes(unsigned char stb)
{
unsigned short factor;
unsigned short value;

	if(impre.first_step_fila ) 
		{
		factor= 230;   // TV60 28;
		//factor= 204;   // TV60 28;
		}
	else if(impre.num_sortides_strobe<2)
		{
		factor= 169;    //TV60 23;
		}
	else if(impre.num_sortides_strobe<4)
		{
		factor= 184;   //TV60 25
		}
	else  
		{
		factor= 230;   // TV60 28;
		//factor= 206;   // TV60 28;
		}

	value=(unsigned short)t_stb[temperaturaImpre]*factor;	//Cabezales se tiene que ajustar en funci�n de la temperatura
	//TB1=value;
	//TB1_START;	//Arranca el timer
        TIM3_OCAR += value;
         TIM3_CR1_bit.EN  = 1 ;                 // Start timer
	return(value);
}

CODE_FAST void send_strobe(void)
{
	unsigned char numbits;
	unsigned char i;
	unsigned char stb;

	numbits=0;
	stb=0x00;

	for (i=impre.strobe; i<6; i++)
	{
		numbits+=pxn[i];
		if(numbits<65)		//La fuente es de 3A. 64*31mA = 2000 mA + 700 mA del motor = 2,7A aprox.
		{					//Dejamos un margen de seguridad de 0,3 A.
			if(pxn[i])
				{
				stb |= mskb[i];
				}
			impre.strobe=i+1;
		}
		else
			break;	//ya no puede enviar m�s bits porque pasar�a demasiada corriente
	}
        impre.st_int = STSTROBE;
	impre.temps_strobes += set_timerB1_strobes(stb);	//inicia el timer de cabezales
        OS_IncDI();
        PORT3 &= 0xc0;
        PORT3 |= stb;     // strobes
        IOPORT_3 = PORT3;
        OS_DecRI();
}

// Casos seg�n el tipo de linea
#define CAS_LINEA 0
#define CAS_AVANCE 1
#define CAS_ANA 2
#define CAS_AVANCE_RV 3
#define CAS_QR 4

// arranque de motor
#define TIMEOUT_1S        1000
#define VUELTAS_ARRANQUE  30


// rutina interrupcio timer3
//Interrupci�n del driver de impresora.
static void _OS_ISR_TickImpre(void)
{
  TIM3_SR_bit.OCFA = 0;
 
  
  //TIM3_OCAR += _OS_TIMER_IMPRE_INTERVAL;
  //TIM3_CR1_bit.EN  = 1 ;                 // Start timer
  
  
	//TB1_STOP;
        TIM3_CR1_bit.EN  = 0 ;                 // Stop timer
        IMPRE_ON                 // Fuente impresora en marcha
        STROBES_OFF              // strobes parados
/*  !!!!       
	if(INT_PIC==0)
	{
            //TB1=TEMPS_MOTOR;		//Reconfigura el timer para dar tiempo a que el pic suba la interrupci�n
            //TB1_START;
            TIM3_OCAR += TEMPS_MOTOR;
            TIM3_CR1_bit.EN  = 1 ;                 // Start timer
            return;
	}
*/
    switch (impre.cas)
    {
    case CAS_LINEA:
        switch (impre.st_int)
        {
          case STSTROBE:                  
            if(tmo_startup_motor == 0)
            {//es un arranque-->doy unos pasos extras al motor (VUELTAS_ARRANQUE vueltas)                    
                if(warmuploop++ > VUELTAS_ARRANQUE)
                {
                  tmo_startup_motor = TIMEOUT_1S;
                  warmuploop=0;
                }
                else
                {
                  OS_IncDI();
                  PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
                  PORT4 |= tabpaso[((++impre.step) % 8)]; // pas 
                  IOPORT_4 = PORT4;
                  OS_DecRI();
                }
                set_timerB1_motor_avance();
            }
            else
            {// no es un arranque
                if(impre.strobe==6)
                {
                  //Ya ha terminado los strobes de la linea actual
                  // passat a  prepara_linea//impre.fila++;
                  // passat a  prepara_linea//impre.paso=0;
                  // passat a  prepara_linea//impre.pasos=3;
                  impre.first_step_fila=0;
                  OS_IncDI();
                  PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
                  PORT4 |= tabpaso[((++impre.step) % 8)];   // pas 
                  IOPORT_4 = PORT4;
                  OS_DecRI();
                  set_timerB1_motor_dibu();             
                  impre.st_int=STMOTOR;
                }
                else
                  send_strobe();

            }
            break;
          case STMOTOR:
            if(impre.paso==impre.pasos)
            {
                //Ya ha terminado los pasos del motor
                //P1=0;	//para el motor
                impre.st_int=STWAIT;	//estado de espera
                if(++impre.fila == impre.filas)
                {
                        //Ya ha terminado de escribir todas las filas de la linea
                        impre.preparar_linea=1;	//Pendiente preparar la siguiente linea
                        tmo_startup_motor=TIMEOUT_1S;
                }
                else
                {
                        impre.send=1;	//Inicia el envio de datos de la siguiente linea
                        OS_WakeTask(&TCB_drvImpre);
                }
            }
            else 
            {
                impre.paso++;
                //*****
                //P1=tabpaso[(++impre.step) % 8];
                //TB1_START;
                //******
                impre.st_int=STSTROBE;
                impre.strobe=0;
                impre.temps_strobes=0;
                send_strobe();
            }
            break;
         }
         break;

    case CAS_AVANCE:					  
        if(impre.paso==impre.pasos)
        {
                //Ya ha terminado los pasos del motor
                //P1=0;				//para el motor
                impre.st_int=STWAIT;	//estado de espera
                impre.preparar_linea=1;			//Pendiente preparar la siguiente linea
        }
        else 
        {
                impre.paso++;
                OS_IncDI();
                PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
                PORT4 |= tabpaso[(++impre.step) % 8];   // pas 
                IOPORT_4 = PORT4;
                OS_DecRI();
                //TB1_START;
                
                
                
                TIM3_OCAR += TEMPS_MOTOR;//!!!!
                TIM3_CR1_bit.EN  = 1 ;                 // Start timer
        }
        break;
    case CAS_AVANCE_RV:
        if(impre.paso==impre.pasos)
        {
                //Ya ha terminado los pasos del motor
                //P1=0;				//para el motor
                impre.st_int = STWAIT;	//estado de espera
                impre.preparar_linea=1;			//Pendiente preparar la siguiente linea
        }
        else 
        {
                impre.paso++;
                OS_IncDI();
                PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
                PORT4 |= tabpaso[(--impre.step) % 8];   // pas 
                IOPORT_4 = PORT4;
                OS_DecRI();
                //TB1_START;
                TIM3_CR1_bit.EN  = 1 ;                 // Start timer
        }
        break;

    case CAS_ANA:
    case CAS_QR:
        switch (impre.st_int)
        {
          case STSTROBE:
              if(impre.strobe==6)
              {
              // Ya ha terminado los strobes de la linea actual
              // passat a  prepara_linea	    impre.paso=0;
              // passat a  prepara_linea		impre.pasos=1;
                      impre.first_step_fila=0;

                     OS_IncDI();
                     PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
                     PORT4 |= tabpaso[((++impre.step) % 8)];       // pas 
                     IOPORT_4 = PORT4;
                     OS_DecRI();
                      set_timerB1_motor_dibu(); 

                      impre.st_int=STMOTOR;
              }
              else
                      send_strobe();

              break;
          case STMOTOR:
              if(impre.paso==impre.pasos)
              {
                      //Ya ha terminado los pasos del motor
                      //P1=0;				//para el motor
                      impre.st_int=STWAIT;	//estado de espera
                      impre.preparar_linea=1;			//Pendiente preparar la siguiente linea
                      OS_WakeTask(&TCB_drvImpre);
              }
              else 
              {	   
                      impre.paso++;
                      //*****
                      //P1=tabpaso[(++impre.step) % 8];
                      //TB1_START;
                      //******
                      impre.st_int=STSTROBE;
                      impre.strobe=0;
                      impre.temps_strobes=0;
                      send_strobe();
              }
              break;
        }
        break;
    }
  
}

// tria per cada tamany de lletra la subtabla corresponent  

void set_tabla_impre(int nt)
{
int nt_aux;
   if(nt >= DIM(tabpun75) )
      nt_aux = 0;
   else
      nt_aux = nt;
   tab7x5=tabpun75[nt_aux];
   
   if(nt >= DIM(tabpun1410_old))
      nt_aux = 0;
   else
      nt_aux = nt;
   tab14x10_old = tabpun1410_old[nt_aux];
   
   if(nt >= DIM(tabpun1410))
      nt_aux = 0;
   else
      nt_aux = nt;
   tab14x10 = tabpun1410[nt_aux];

   if(nt >= DIM(tabpun2115_old) )
      nt_aux = 0;
   else
      nt_aux = nt;
   tab21x15_old = tabpun2115_old[nt_aux];
   
   if(nt >= DIM(tabpun2115) )
      nt_aux = 0;
   else
      nt_aux = nt;
   tab21x15=tabpun2115[nt_aux];// Marc_V
   tab21x15_numbits=tabpun2115_numbits[nt_aux];// Marc_V
}


// inicializacions del driver de interrupcio 

void reset_impresora_low_level(void)
{
	impre.imprimint = 0;
        OS_IncDI();
        PORT4 |= 0x80;
	IOPORT_4 = PORT4;     // MOTOR OFF
        OS_DecRI();
	impre.st_int = STWAIT;
	impre.strobe=0;
	impre.fila=0;
	impre.cpunter=0;
	impre.preparar_linea=0;
}

unsigned char baux[64+1];


int imprime_low_level(int n_tik, int forced)
{
  if(impre.falta_paper)
  {
   impre.ticket_ok = 0;  // per saltarse resta ticket encara que s' arregli paper
    return 0;
  }
  if(impre.imprimint)
    return 0;
  if(!forced && !impre.ticket_ok)
    return 0;
  reset_impresora_low_level();
  impre.cpunter = TTIK[n_tik];
  impre.imprimint = 1;
  tmo_motor = 0;
  OS_IncDI();
  PORT4 &= 0x40;                    // Mante SALTO i fa IMPRE_ON
  PORT4 |= tabpaso[impre.step % 8];   // Paso  
  IOPORT_4 = PORT4;
  OS_DecRI();
  impre.preparar_linea = 1;
  return 1;
}


unsigned char p1char(void)
{
   return(*impre.cpunter++);
}

void p1char_prev(void)
{
   impre.cpunter--;
}

const char tabMaskBits[8] = {128, 64, 32, 16, 8, 4, 2, 1};

// eperezr: imprime QR desde mapa de bits optimizado en memoria
CODE_FAST void print_bmpbit_qr(unsigned char escala, unsigned char fila_size, unsigned char nfila){

    int index_filasize;
    int index_linea, index_bit, index_escala;
    char mask_linea, mask_bmpbit;
    
    
    int border = (PRINTER_RESOLUTION_MAX - (fila_size*escala))/2;
   
    //centrado del codigo QR
    index_linea = border/8;
    index_bit = 0;

    //centrado del codigo QR fino a nivel de bits
    int bits_ajustes = border - (index_linea*8); // trunca la variable
    index_bit = 7-bits_ajustes;
    if(bits_ajustes<7){   
        for(int index_bits_ajustes=index_bit;index_bits_ajustes<8;index_bits_ajustes++){
            mask_linea = tabMaskBits[index_bit];
            linea[index_linea] = linea[index_linea] & ~mask_linea;
            index_bit++;
            if (index_bit > 7){
                index_bit = 0;
                index_linea++;
            }
        }
    }else{
        index_linea++;
    }
    //fin de centrado
    
    //imprime codifgo QR en linea
    for(index_filasize=0;index_filasize<fila_size;index_filasize++){
        for(index_escala=0;index_escala<escala;index_escala++){
                    
            mask_linea = tabMaskBits[index_bit];
            mask_bmpbit = tabMaskBits[((fila_size*nfila)+index_filasize)%8];
            
            if ((bmpbitQR[((fila_size*nfila)+index_filasize)/8] & mask_bmpbit) == mask_bmpbit){
                linea[index_linea] = linea[index_linea] | mask_linea;
            }
            else{
                linea[index_linea] = linea[index_linea] & ~mask_linea;
            }
            index_bit++;
                    
            if (index_bit > 7){
                index_bit = 0;
                index_linea++;
            }
        }
    
    }
}

CODE_FAST void preparar_linea(void)
{
unsigned char st,nb,pos,ch,ncam;
int x;
   impre.preparar_linea=0;
   memset(linea,' ',64);
   while (1)
   {
      st=p1char();

      switch (st)
	  {
          case ASC:
              nb =p1char();
              pos=p1char();
              p1char();     // per extreure caracter que no s'utilitza
              memcpy(&linea[pos], baux, nb);
             break;
          case QR:
                nb = p1char();
                pos = p1char();
                ncam = p1char();
                memset(linea, 0, 48);
                print_bmpbit_qr(nb, pos, ncam);
                impre.paso = 0;
                impre.pasos = 3;
                impre.cas = CAS_QR;
                return;
          case ANA:
             p1char_prev(); // reposiciona a ANA per seguir treient linies.
                            // Quan s'acabi el bitmap ja fara  END
             
             if(impre.bitmap.y == 0)
             {
               // ja s'han acabat les linies 
//fbp               impre.bitmap.en_curs = 0;
               // fa com si hagues llegit un END
               impre.imprimint = 0;
               tmo_motor=10000;
               return;
             }
             memset(linea,0,48);
             for(x=0; x < impre.bitmap.numcar_linea; x++)
                {
                ch = impre.bitmap.BMP[((impre.bitmap.y-1)*impre.bitmap.carWidth)+x] ;
                ch ^= 0xFF; //invert blanco y negro
                linea[x+impre.bitmap.numcar_ceros] = ch;
                }
             impre.bitmap.y--;
             impre.paso=0;
             impre.pasos=3;
             impre.cas=CAS_ANA;
             return;
          case AVR:
             impre.paso=0;
             impre.pasos=35;
             impre.cas=CAS_AVANCE;
             return;
          case AV_1:
             impre.paso=0;
             impre.pasos=p1char();
             impre.cas=CAS_AVANCE;
             return;
          case RAVR:
             impre.paso=0;
             impre.pasos=p1char();
             impre.cas=CAS_AVANCE_RV;
             return;
           case FIN:
             impre.cas=CAS_LINEA;
             return;
          case END:
             impre.FontSize = FONTSIZE_DEFAULT;    // default per seguent linia
             impre.imprimint = 0;
             tmo_motor=10000;
             return;
          case RES:
             ptabla=0;
             break;
          case AVL:
             impre.paso = 0;
             impre.pasos = tipos_letra[impre.FontSize].pasos;
             impre.cas=CAS_AVANCE;
             return;
      }
   }
}

const unsigned char maskf21[21]={0x10,8,4,2,1,0x80,0x40,0x20,0x10,8,4,2,1,0x80,0x40,0x20,0x10,8,4,2,1};
const unsigned char maskf14[14]={0x20,0x10,8,4,2,1,0x80,0x40,0x20,0x10,8,4,2,1};
const unsigned char maskf7[7]={0x40,0x20,0x10,8,4,2,1};

#define SERIAL_SET1 {byteLatch |= maskLatch;numpunts++;}
#define SERIAL_NEXT {\
                      maskLatch >>= 1;\
                      if(maskLatch == 0)\
                          {maskLatch = 0x80;latch_buf[nByteLatch++]=byteLatch;byteLatch=0;}\
                      np++;\
                      if(np==64)\
                      {\
                              np=0;\
                              pxn[col]=numpunts;\
                              numpunts=0;\
                              col++;\
                      }\
                     }

RAM_INTERNA unsigned char latch_buf[48];
RAM_INTERNA  unsigned char maskLatch;
RAM_INTERNA  unsigned char byteLatch;
RAM_INTERNA  int nByteLatch;
RAM_INTERNA  unsigned char MaskFila;
RAM_INTERNA  unsigned char caracter;
RAM_INTERNA  unsigned char numpunts;
RAM_INTERNA  unsigned char i;
RAM_INTERNA  unsigned char col;
RAM_INTERNA  unsigned char columna;
RAM_INTERNA  static unsigned int np;
RAM_INTERNA  int aux_fila;

//#pragma location="MEM_RAPIDA"
CODE_FAST void send_bits(char fila) 
{
    unsigned int ptabla;
    maskLatch = 0x80;
    byteLatch = 0;
    nByteLatch=0;
    col=0;
    np=0;
    impre.send=0;
    char c_aux,c_aux1;
    switch(impre.cas)
    {
    case CAS_LINEA:
         impre.paso=0;
         impre.pasos=3; 
         numpunts=0;
         switch (impre.FontSize)
                {
                case T7X5:
                        MaskFila=maskf7[impre.fila];
                        for (i=0; i<64; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<6;columna++)
                                {
                                        if(tab7x5[(caracter * 6) + columna] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }	
                        }
                        break;
                case T14X10:
/*                  
                        MaskFila=maskf14[impre.fila];
                        for (i=0; i<32; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<12;columna++)
                                {
                                        ptabla=(caracter * 24) + (columna * 2);
                                        if(impre.fila<6) ptabla++;
                                        if(tab14x10[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }
                        }
*/
                        for (i=0; i<32; )   // agafa caracters de dos en dos (24 bits)
                        {
                                caracter=linea[i++];
                                ptabla = (caracter * 32)+impre.fila*2;
                                latch_buf[nByteLatch++]=tab14x10[ptabla++];
                                c_aux = tab14x10[ptabla];
                                caracter=linea[i++];
                                ptabla = (caracter * 32)+impre.fila*2;
                                c_aux1 = tab14x10[ptabla++];
                                latch_buf[nByteLatch++]= c_aux | (c_aux1>>4);
                                c_aux = tab14x10[ptabla] >>4;
                                latch_buf[nByteLatch++]= (c_aux1<<4) | c_aux;
                        }
                        for(i=0,col=0,numpunts=0; i<48; )
                        {
                                c_aux = latch_buf[i++];
                                numpunts+=(taula_bits[c_aux & 0x0f] + taula_bits[c_aux>>4]);
                                if((i%8) == 0)
                                {
                                  pxn[col++]=numpunts;
                                  numpunts=0;
                                }
                        }        
                        break;
                case T11X7:
                        //Utiliza la tabla de 24 caracteres por linea cogiendo las filas y columnas de 2 en dos
                        //para imprimir 48 caracteres por linea
                        MaskFila=maskf21[impre.fila*2];
                        for (i=0; i<48; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<48;columna+=6)
                                {
                                        ptabla=(caracter * 48) + (columna);
                                        if(impre.fila<3) ptabla++;
                                        if(impre.fila<7) ptabla++;
                                        if(tab21x15_old[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }
                        }
                        break;
                case T21X15:
/*                  
                        MaskFila=maskf21[impre.fila];
                        for (i=0; i<24; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<48;columna+=3)
                                {
                                        ptabla=(caracter * 48) + (columna);
                                        if(impre.fila<5) ptabla++;
                                        if(impre.fila<13) ptabla++;
                                        if(tab21x15[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }
                        }
*/
                                
                        for (i=0,col=0,numpunts=0; i<24; i++)
                        {
                                caracter=linea[i];
                                ptabla = (caracter * 48)+impre.fila*2;
                                latch_buf[nByteLatch++]=tab21x15[ptabla++];
                                latch_buf[nByteLatch++]=tab21x15[ptabla];
                                numpunts+=tab21x15_numbits[caracter *24 +impre.fila];
                                if((i%4) == 3)
                                {
                                  pxn[col++]=numpunts;
                                  numpunts=0;
                                }
                        } 
                       break;
                case T14X20:
                case T28X20:  
                        // si ha de doblar linea de punts repeteix fila dos vegades
                        if(tipos_letra[impre.FontSize].doble_fila == 0)
                          aux_fila = impre.fila;
                        else
                          aux_fila = impre.fila/2;
                        MaskFila=maskf14[aux_fila];
                        for (i=0; i<16; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<12;columna++)
                                {
                                        ptabla=(caracter * 24) + (columna * 2);
                                        if(aux_fila<6) ptabla++;
                                        if(tab14x10_old[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                        if(tab14x10_old[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }
                        }
                        break;
                case T21X30:
                case T42X30:
                        // si ha de doblar linea de punts repeteix fila dos vegades
                        if(tipos_letra[impre.FontSize].doble_fila == 0)
                          aux_fila = impre.fila;
                        else
                          aux_fila = impre.fila/2;
                        MaskFila=maskf21[aux_fila];
                        for (i=0; i<12; i++)
                        {
                                caracter=linea[i];
                                for (columna=0;columna<48;columna+=3)
                                {
                                        ptabla=(caracter * 48) + (columna);
                                        if(aux_fila<5) ptabla++;
                                        if(aux_fila<13) ptabla++;
                                        if(tab21x15_old[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                        if(tab21x15_old[ptabla] & MaskFila)
                                                SERIAL_SET1
                                        SERIAL_NEXT
                                }
                        }
                        break;
                }
         break;
    case CAS_QR:
    case  CAS_ANA:
        numpunts=0;
        for (i=0; i<48; i++)
        {
                MaskFila=0x80;
                while (MaskFila)
                {
                        if(linea[i] & MaskFila)
                                SERIAL_SET1
                        MaskFila>>=1;
                        maskLatch >>= 1;
                        if(maskLatch == 0)
                            {maskLatch = 0x80;latch_buf[nByteLatch++]=byteLatch;byteLatch=0;}
                 }
                if((i%8)==7)
                {
                        pxn[i/8]=numpunts;
                        numpunts=0;
                }
        }
        break;
    }
    
    // envia i latcha 
    spi_send(2,latch_buf,48);

    for(i=0,numpunts=0,np=0;i<6;i++)
            {
            numpunts+=pxn[i];
            if(numpunts<65)
                    continue;
            np++;
            numpunts=pxn[i];
            }
    if(numpunts)
            np++;
    impre.num_sortides_strobe=np;
    impre.strobe=0;
    impre.first_step_fila=1;
    impre.temps_strobes=0;
    send_strobe();
}

#define COLA_IMPRE_SIZE 16*1024    
OS_Q ColaImpre;

static char ColaImpreBuffer[COLA_IMPRE_SIZE];


void inici_ticket(int ret_event)
{
char c[2];
   c[0] = IN;
   c[1] = ret_event;
   OS_Q_Put(&ColaImpre,&c,2);
     
}

void final_ticket(void)
{
char c = FI;  
   OS_Q_Put(&ColaImpre,&c,1);     
}

// **********************************************************
// gestio bit map ( anagrama)
// **********************************************************
// Passar bitmap a linies impressora

extern __no_init char* memory_bmp;
extern __no_init unsigned int mem_bmp_size;


int bitmap_init(char* buf, int nc) 
{
unsigned char inf;
int i,j;
unsigned int columna;
unsigned char nombre_BMP[10];
unsigned int sig_BMP,ant_BMP;
unsigned int biWidth,biHeight,biBitCount,carWidth;
char *BMPn;

    //0x0b+"xxx,12345678" - con xxx=posici�n y "," y 12345678=nombre ANAGRAMA		 
    //buscar columna xxx en la linea - xxx,"nombre"
    columna=0;
    i = 0;
    inf=buf[i];
    while((inf>=0x30) && (inf<=0x39))
            {
            columna=(columna*10)+(inf & 0x0f);
            i++;
            inf=buf[i];
            }
    if(columna>383) //IR32 tiene columnas de 0_383
            columna=383;
    if(inf==0x2c) //saltar ","
            i++;
    //nombre de 8 car�cteres...
    for(j=0;j<8;j++)
            {
            nombre_BMP[j]=buf[i+j];
            }
    nombre_BMP[8]=0;
    while((buf[i]!=0x0d) && (i < nc)) //buscar �ltimo car�cter de este linea 0x0d.
            i++;
    //	-buscar BITMAP "nombre" en memoria, desde unsigned char* memory_bmp y pasar a impresora IR32.
    //	-imprimir desde posici�n columna
  if(memory_bmp!=NULL) //Existen BMPs en memoria
    {
      BMPn = memory_bmp;
      sig_BMP=0;
      while(memcmp(nombre_BMP, BMPn, 8)!=0)
        {
        //Buscar siguiente BMP si existe...
        ant_BMP=sig_BMP;
        sig_BMP=(256*BMPn[14])+BMPn[15]; //offset cuenta desde primera posici�n memory_bmp
        if(sig_BMP==0)
                break; //No hay mas BITMAPs para repasar.
        if(sig_BMP<=ant_BMP)
                break; //No puede pasar nunca que el siguiente BMP est� en una posici�n por debajo del anterior.
        BMPn = memory_bmp + sig_BMP;
        }
      if(memcmp(nombre_BMP, BMPn, 8)==0)
        {
          //BITMAP encontrado - controlar contenido correcto
          //BMPn[0] apunta nombre de BITMAPn dentro del fichero
          //El BITMAP "BM---" empieza 16 bytes mas adelante...
          biBitCount=BMPn[44]+(256*BMPn[45]);	//biBitCount specifies the number of bits per pixel.
          if(	(BMPn[16]==0x42) && (BMPn[17]==0x4D) && //"BM"
              (biBitCount==1) ) //1 bit per pixel
            {
            biWidth=BMPn[34]+(256*BMPn[35]);	//biWidth specifies the width of the image, in pixels
            biHeight=BMPn[38]+(256*BMPn[39]);	//biHeight height of the image, in pixels
  
            carWidth=biWidth/8;
            while((carWidth%4)!=0)	carWidth++; //multiple de 4.
            impre.bitmap.carWidth = carWidth;
            impre.bitmap.numcar_ceros=(unsigned char)(columna/8);	//000_383 => 00_47.
            impre.bitmap.numcar_linea=(unsigned char)carWidth;
            if((impre.bitmap.numcar_ceros+impre.bitmap.numcar_linea) > 48)
                    impre.bitmap.numcar_linea=(48-impre.bitmap.numcar_ceros);
            impre.bitmap.y=biHeight;
            impre.bitmap.BMP = BMPn + 78;
//fbp            impre.bitmap.en_curs = 1;
            
            }
        }
    }
  return(i+1);
}

void reset_bitmap(void)
{
//fbp  impre.bitmap.en_curs = 0;
  impre.bitmap.y = 0;
}

char bufAuxQr[2048];

int resendQrCode(char * buf, int nc){
    unsigned int nb = 0;
    unsigned char cksm=0;
    unsigned chk_in, ch;
    int i;
    
	memset(bufAuxQr, 0, 2048);
    bufAuxQr[0] = 0x1b;             //Secuencia QR para IR80 = ESC, '#'
    bufAuxQr[1] = '#';

    for (i = 0; i<3; i++){          //Calcula el numero de bytes
        ch = *buf++;
        nb = (nb * 10)  + (ch - 48);
        cksm ^= ch;
        bufAuxQr[i+2] = ch;
    }
    for(i=0; i<nb; i++){            //Copia el buffer del codigo QR
        ch = *buf++;
        cksm ^= ch;
        bufAuxQr[i+5] = ch;
    }
    chk_in = hexa_to_uchar((unsigned char *)buf);    //lee el checksum
    bufAuxQr[i+5] = *buf++;         //Guarda el ckecksum en el buffer
    bufAuxQr[i+6] = *buf;
    
    
	if (nb > 999 || nb > nc - 5 || cksm != chk_in){
        nb = sprintf((char *)baux, "QR_Er: %d, %d, %02x %02x\r\n\n\n\n", nb, nc, cksm, chk_in);
        envia_texte((char *)baux, nb, 1);
        return nc;
    }
    envia_texte(bufAuxQr, nb + 7, 0);
    return nb + 5;
}

int printQrCode(char * buf, int nc){
	unsigned int nb = 0;
    unsigned chk_in, ch;
    char *text;                            // User-supplied text
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_MEDIUM;   // Error correction level
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];
    int qr_size, escala, paper_border;
    enum qr_error_code qr_error = qr_error_code_NoError;
    bool result_encoder_qr = 0;
    bool buffer_corrupto = 0;
    
    unsigned char cksm=0;
	memset(bufAuxQr, 0, 2048);

    for (i = 0; i<3; i++){          //Calcula el numero de bytes
        ch = *buf++;
        nb = (nb * 10)  + (ch - 48);
        cksm ^= ch;
//        bufAuxQr[i] = ch;
    }
    for(i=0; i<nb; i++){            //Copia el buffer del codigo QR
        ch = *buf++;
        cksm ^= ch;
        bufAuxQr[i] = ch;
    }
    chk_in = hexa_to_uchar((unsigned char *)buf);    //lee el checksum
    //eperezr: Genera QR estatico
    //imprime(TIK1QR);  // Modo pruebas. Requiere comentar el resto de la funcion
	
	// eperezr: Genera mapa de bits de codigo QR
	text = (char*) bufAuxQr;                            // User-supplied text
	errCorLvl = qrcodegen_Ecc_MEDIUM;   // Error correction level
	qr_error = qr_error_code_NoError;
		   
	if (buffer_corrupto){
		result_encoder_qr = 0;
		qr_error = qr_error_code_BufferCorrupted;
        buffer_corrupto = 0;
    }else if (nb>999){
        result_encoder_qr = 0;
		qr_error = qr_error_code_InputBufferSize;
	}else if (chk_in != cksm){
        result_encoder_qr = 0;
		qr_error = qr_error_code_InputBufferCheckSum;
    }else{
		result_encoder_qr = qrcodegen_encodeText(text, tempBuffer, qrcode, errCorLvl, qrcodegen_VERSION_9, qrcodegen_VERSION_MAX, qrcodegen_Mask_0, true);
		qr_size = qrcodegen_getSize(qrcode);
		escala = (int)(QR_SIZE_MIN/(PRINTER_DOT_SIZE*qr_size))+1;
		paper_border = (PRINTER_RESOLUTION_MAX - (qr_size*escala))/2;
		if (result_encoder_qr){
			//Generacion de codigos de error
			if (qr_size>(PRINTER_RESOLUTION_MAX-(2*PAPER_BODER_MIN))/2){
				result_encoder_qr = 0;
				qr_error = qr_error_code_QRsize;
			}else if (escala<MODULE_DOT){
				result_encoder_qr = 0;
				qr_error = qr_error_code_QRscale;
			}else if (paper_border<PAPER_BODER_MIN){
				result_encoder_qr = 0;
				qr_error = qr_error_code_QRborder;   
			}
		}else{
			result_encoder_qr = 0;
			qr_error = qr_error_code_QRencoder;   
		}
        
	}
				   
	if (!result_encoder_qr){
		
		memset(bufAuxQr, 0, 48);
		memcpy(bufAuxQr, "QR_encoder_error: ", 18);
		bufAuxQr[18] = qr_error+48;
		
		tikram[0] = ASC; 
		tikram[1] = 32; 
		tikram[2] = 0; 
		tikram[3] = 0; 
		tikram[4] = FIN;
		tikram[5] = END;
		tikram[6] = END;
		
		imprime_low_level(TIKRAM, 0);
		
	}else{
	
		int index_qr;
        
        // eperezr: BMP code generator a nivel de bits
        int index_qr_bits = 0;
        memset(bmpbitQR,0,bmpQR_BUFFER_LEN_BIT_MAX);
        index_qr = 0;
        
        for (int index_degenQR_i = 0; index_degenQR_i < qr_size ; index_degenQR_i++) {
			for (int index_degenQR_j = 0; index_degenQR_j < qr_size ; index_degenQR_j++) {
                
                if (qrcodegen_getModule(qrcode, index_degenQR_j, index_degenQR_i)){
                    bmpbitQR[index_qr]=bmpbitQR[index_qr] | tabMaskBits[index_qr_bits];                           
                }else{
                    bmpbitQR[index_qr]=bmpbitQR[index_qr] & ~tabMaskBits[index_qr_bits]; 
                }  
         
                index_qr_bits++;       
                if (index_qr_bits > 7){
                    index_qr_bits = 0;
                    index_qr++;
                }
			}
		}
	
		// eperezr: TIKQR code generator
	
		index_qr = 0;
        memset(tik_qrram,0,tik1qr_BUFFER_LEN_MAX);
        
		for (int index_size=0 ; index_size<qr_size ; index_size++){
			for (int index_escala=1 ; index_escala<=escala ; index_escala++){
				tik_qrram[index_qr]=QR;
				index_qr++;
				tik_qrram[index_qr]=escala;
				index_qr++;
				tik_qrram[index_qr]=qr_size;
				index_qr++;
				tik_qrram[index_qr]=index_size;   
				index_qr++;                 
			}
		}    
	
		tik_qrram[index_qr]=END;
		//fin to_do QR
	
        imprime_low_level(TIK1_QRRAM, 0);
	}
    return nb + 5;
}




//
// ****************************************************************
// ****************************************************************
// funcions per enviar a impre externa
//

// cas no hi ha impre externa
void impre_enviar_null(char *buf_env, int nb)
{
  return;
}

void impre_preg_status_null(void)
{
  return;
}

// cas impre per canal serie 1
void impre_enviar_usart(char *buf_env, int nb)
{
    usart_enviar(USART_IMPRE,buf_env,nb);
    return;
}
void impre_preg_status_usart(void)
{
	switch(tipoImpresora){
	case IMPREtip_int:
	case IMPREtip_ext:
    	usart_enviar(USART_IMPRE, "\x1b\x0c", 2);
		break;
	case IMPREtip_Opt:
		usart_enviar(USART_IMPRE, "\x1c\x76", 2);
		break;
	case IMPREtip_Kas:
		usart_enviar(USART_IMPRE, "\x1b\x76\x00",3);
		break;
	}
}


//  cas impre per multiplex BT40
void impre_enviar_multiplex(char *buf_env, int nb)
{
    multiplex_enviar(DEVICE_TX5240, PER_PRN_TX, buf_env, nb);
    return;
}

void impre_preg_status_multiplex(void)
{
    multiplex_enviar(DEVICE_TX5240, PER_PRN_PREG_STS, NULL, 0);
    return;
}

//
// funcio virtual per enviar a impre externa
// funcio virtual per enviar pregunta status a impre externa
// se'ls hi assigna una funcio segons el tipus de conexio
//
void (*impre_enviar)(char *buf_env, int nb) = impre_enviar_null;
void (*impre_preg_status)(void) = impre_preg_status_null;

//
// ****************************************************************
// ****************************************************************

void preguntar_status(void)
{
   impre_preg_status();
   impre.timer_resp_status = IMPR_TIMEOUT_RESP_STATUS;   // per si es perd resposta status
   impre.timeout_resp_status = 0;
#if DEBUG_IMPRE_T
   num_pregs_status++;
   if(index_array < sizeof(debug_array_status))
      debug_array_status[index_array++] = 'S';   
#endif
}
//
// envia linia grafica
//
// retorn:
//  
void envia_linia_grafica(void)
{
unsigned char ch;
int x;
#if DEBUG_IMPRE
volatile int dum;
   num_linies_enviades++;
#endif
    
   memset(linea_grafica+4,0,48);
   linea_grafica[0] = 0x1b;   // ESC
   linea_grafica[1] = 0x2a;   // *
   linea_grafica[2] = impre.bitmap.numcar_linea;
   linea_grafica[3] = impre.bitmap.numcar_ceros;
   for(x=0; x < impre.bitmap.numcar_linea; x++)
      {
      ch = impre.bitmap.BMP[((impre.bitmap.y-1)*impre.bitmap.carWidth)+x];
      ch ^= 0xFF; //invert blanco y negro
      linea_grafica[x+4] = ch;
      }
   impre.bitmap.y--;
   impre_enviar(linea_grafica,impre.bitmap.numcar_linea+4);
   impre.pendent_preg_status = 0;
   impre.imprimint = 1;
   preguntar_status();
#if DEBUG_IMPRE
   if(impre.bitmap.y == 0)
   {
     dum = 3*dum;
     dum -= 3;
   }
#endif
   return;
   
}

void impre_off(){
	impre.parada = 1;
	switch(tipoImpresora){
	case IMPREtip_int:
		break;
	case IMPREtip_ext:
		break;
	case IMPREtip_Kas:
		impre_enviar("\x1b\x38\x14\x00", 4);	//Set sleep mode after 500 ms
		break;
	case IMPREtip_Opt:
		break;
	}
}

void impre_on(void){
	impre.parada = 0;
}

void impre_font_size(int font){
	impre.FontSize = font;
}

void envia_texte(char* buf, int len, int font_reset)
{
  if(len)
  {
     impre.imprimint = 1;
     impre_enviar(buf,len);
     if(font_reset && (impre.FontSize != FONTSIZE_DEFAULT))
     {
       impre.pendent_reset_font = 1;
     }
     impre.pendent_preg_status = 0;
     impre.timeout_resp_status = 0;
     preguntar_status();
  }
}

static void Task_drvImpreInterna(void)
{
  int chars_to_process = 0;
  char msg_to_purge =0;
  char coplin = 0;
  char *buf_rec; 
  int st;
  unsigned char ch,n1;
  
  
  reset_impresora_low_level();
  reset_bitmap();
  
  while(1)
  {
        OS_Delay(10);
        if(!impre.falta_paper && impre.faltava_paper)
        {
           impre.faltava_paper = 0;
           impre.status  &= ~IMPRE_SINPAPEL;
           imprime_low_level(TIK1FF,1);    // per avan�ar paper sempre 
        }
        if(!impre.falla_ntc && impre.fallava_ntc)
        {
           impre.fallava_ntc = 0;
           impre.status  &= ~IMPRE_FALLO;
           //imprime_low_level(TIK1FF,1);    // per avan�ar paper sempre 
        }
        
        if(timLed == 3)
        {
          LED_ON
          timLed = 2;
        }
        else if(timLed == 0)
        {
         if(!PAPEL )
             {
              impre.falta_paper=1;
              impre.faltava_paper=1;
              impre.status |= IMPRE_SINPAPEL;
              impre.ticket_ok = 0;  // per saltarse resta ticket encara que s' arregli paper
             }
         else 
            impre.falta_paper=0;
         timLed = LED_TICKS;
         LED_OFF
        }
        if(impre.fallava_ntc)
        {
           impre.status  |= IMPRE_FALLO;
           impre.ticket_ok = 0;  // per saltarse resta ticket encara que s' arregli paper
        }
    
        if(impre.preparar_linea==1)	//Pendiente preparar linea
        {
              preparar_linea();
              if(impre.imprimint)
              {
                switch(impre.cas)
                {
                    case CAS_AVANCE:
                        OS_IncDI();
                        PORT4 &= 0x40;                        // Mante SALTO i fa IMPRE_ON
                        PORT4 |= tabpaso[(++impre.step) % 8];   //Paso 
                        IOPORT_4 = PORT4;
                        OS_DecRI();
                        set_timerB1_motor_avance();		//inicia el timer del motor
                        break;
                    case CAS_AVANCE_RV:
                        OS_IncDI();
                        PORT4 &= 0x40;                        // Mante SALTO i fa IMPRE_ON
                        PORT4 |= tabpaso[(--impre.step) % 8];   // Paso 
                        IOPORT_4 = PORT4;
                        OS_DecRI();
                        set_timerB1_motor_avance();		//inicia el timer del motor
                        break;
                    case CAS_LINEA:
                        impre.fila = 0;
                        impre.filas = tipos_letra[impre.FontSize].filas;
                        impre.send=1;	// Linea de texto o anagrama inicia el env�o de bits
                        break;
                    case CAS_ANA:
                    case CAS_QR:
                        impre.send=1;	// Linea de texto o anagrama inicia el env�o de bits
                        break;
                }
              }
        }
        if(impre.send==1)
            send_bits(impre.fila);	//Envia una fila de puntos, env�a los strobes y arranca el timer
    
    if(!impre.imprimint)
    {
           if(chars_to_process == 0)
           {
             if(msg_to_purge)
             {
               OS_Q_Purge(&ColaImpre);
               msg_to_purge=0;
             }
             chars_to_process = OS_Q_GetPtrCond(&ColaImpre,(void**)&buf_rec);
             if(chars_to_process)
               msg_to_purge=1;
           }
           while (chars_to_process)
           {
              ch = *buf_rec++;
              chars_to_process--;
              st = -1;
              switch(ch)
              {
              case IN:  // Inici tiquet
                impre.ticket_ok = 1;
                impre.ticket_ret_event = *buf_rec++;  // ho treu de seguent caracter
                chars_to_process--;
                break;
              case FI:  // Final tiquet
                if(impre.ticket_ret_event)
                {
                  impre_event_call_back(impre.ticket_ret_event, impre.ticket_ok);
                  impre.ticket_ret_event = 0;
                }
                break;
              case CR:
                 if(coplin==0) {
                      st = imprime_low_level(TIK1LF,0);
                 } else {
                      tikram[0]=ASC; tikram[1]=64; tikram[2]=0; tikram[3]=59; tikram[4]=FIN;tikram[5]=AVL;tikram[6]=END;
                      st = imprime_low_level(TIKRAM,0);
                 }
                 coplin=0;
                 break;
              case LF:
                  st = imprime_low_level(TIK1LF,0);
                break;
              case FF:
                   st = imprime_low_level(TIK1FF,0);
                break;
              case GR:    // imprimir bitmap
                n1 = bitmap_init(buf_rec,chars_to_process);
                chars_to_process -= n1;
                if(chars_to_process <0)
                  chars_to_process = 0;   // no hauria de passar mai
                buf_rec += n1;
                if(impre.bitmap.y)
                {
                      st = imprime_low_level(TIK1ANA,0);
                }
                break;
              case QR:
                n1 = printQrCode(buf_rec,chars_to_process);
                chars_to_process -= n1;
                buf_rec += n1;
                st = 1;
                break;
              case FS:
                n1 = *buf_rec++;
                chars_to_process--;
                if(n1 >= DIM(tipos_letra))
                    n1 = FONTSIZE_DEFAULT;
                impre.FontSize = n1;
                break;

              default :
                if(coplin==0) memset(baux,' ',64);
                if(coplin<64) baux[coplin++]=ch;
                break;
              } // final switch
             if(st == -1)
               continue;
              if(st ==0)
              {
                // cas error en imprime_low_level
              }
              else
              {
                // cas a esperar acabi imprime_low_level
                break;
              }
           }  //  while (chars_to_process )
    }     //  if(!impre.imprimint)

  } // while(1)
}

extern char tabFontsOptronicW[]; 
extern char tabFontsOptronicH[]; 
extern char tabFontsKashino[];

void reconfig_impre(){
	if(tipoImpresora==IMPREtip_int || tipoImpresora==IMPREtip_ext){		//interna / IR32
        usart_hard(USART_IMPRE, MODO_232, 0, 1, 0);
        usart_init(USART_IMPRE, 9600, 1,0);     // 9600, 8bits no parity, dum
      	impre_enviar("\x1b\x41", 2); // 24 caracters per linia
      	impre_enviar("\x1b\x30", 2); // fuente windows occidental
	  } else if(tipoImpresora==IMPREtip_Opt){			//Optronic
        usart_hard(USART_IMPRE, MODO_232, 0, 1, 0);
        usart_init(USART_IMPRE, 9600, 1,0);     // 9600, 8bits no parity, dum
      	impre_enviar("\x1c\x76", 2); // demana el status del paper per canal serie
      	impre_enviar("\x1b\x31\x01", 3); // espai entre linies
		impre_enviar("\x1b\x36\x01", 3);	//character set
		impre_enviar("\x1b\x63\x01", 3);	//Reverse print (left to right)
	  } else if(tipoImpresora==IMPREtip_Kas){			//Kashino
        usart_hard(USART_IMPRE, MODO_232, 0, 1, 0);
        usart_init(USART_IMPRE, 9600, 1,0);     // 9600, 8bits no parity, dum
      	impre_enviar("\x1b\x33\x20", 3); // espai entre linies = 1,2 mm
		impre_enviar("\x1d\x61\x04", 3);	//Enable Automatic Status Back
	  }
	  impre.reconfig = 0;
}

int tabDelay[8]={80,80,80,80,300,300,300,300};

static void Task_drvImpreExterna(void)
{
	int chars_to_process = 0;
	char msg_to_purge =0;
	char coplin = 0;
	char *buf_rec; 
	int st;
	unsigned char ch,n1;
	char events_signaled;
	char buf_aux[6];  
	static char last_event=0;
	
	reset_impresora_low_level();
	reset_bitmap();
	
	preguntar_status();
	
	while(1)
	{
		events_signaled = OS_WaitEventTimed(EVENTIMPRE_STATUS_RCVD | EVENTIMPRE_PREGUNTAR_STATUS | EVENTIMPRE_TIMEOUT_STATUS,60);
		if(events_signaled & EVENTIMPRE_STATUS_RCVD)
		{
			
			impre.timer_resp_status = 0;  // reset timer time-out
			impre.status &= ~IMPRE_FALLO_TRANSM;
			
			switch(tipoImpresora){
			case IMPREtip_int:
			case IMPREtip_ext:
				switch(impre.ch_rcvd)
				{
				case 'R':
				case 'O':
					impre.imprimint = 0;
					impre.status &= ~(IMPRE_SINPAPEL | IMPRE_FALLO);
					impre.timer_preg_status = IMPRE_TIME_PREG_STATUS_SLOW;    // cada 50 ms
					break;
				case 'I':      
					impre.imprimint = 1;
					impre.status &= ~(IMPRE_SINPAPEL | IMPRE_FALLO);
					impre.timer_preg_status = IMPRE_TIME_PREG_STATUS_FAST;   // per que entri cada 2ms
					break;
				case 'P':
					impre.status |= IMPRE_SINPAPEL;
					break;
				case 'N':
					impre.status |= IMPRE_FALLO;
					break;
				}
				break;
			case IMPREtip_Opt:
				if(impre.ch_rcvd == 0x04){
					OS_Delay(tabDelay[impre.FontSize]);
   					impre.imprimint=0;
					impre.status &= ~(IMPRE_SINPAPEL | IMPRE_FALLO);
					impre.timer_preg_status = IMPRE_TIME_PREG_STATUS_SLOW;    // cada 50 ms
				} else {
					impre.status |= IMPRE_SINPAPEL;
				}
				break;
			case IMPREtip_Kas:
				if((impre.ch_rcvd & 0x04) == 0){
					impre.imprimint = 0;
					impre.status &= ~(IMPRE_SINPAPEL | IMPRE_FALLO);
					impre.timer_preg_status = IMPRE_TIME_PREG_STATUS_SLOW;    // cada 50 ms
				} else {
					impre.status |= IMPRE_SINPAPEL;
				}
				break;
			}
			if (last_event & EVENTIMPRE_TIMEOUT_STATUS)
				impre.reconfig=1;
		}
		if(events_signaled & EVENTIMPRE_PREGUNTAR_STATUS)
		{
			impre.pendent_preg_status = 1;
		}
		
		if(events_signaled & EVENTIMPRE_TIMEOUT_STATUS)          
		{
			impre.timeout_resp_status = 1;
			impre.status |= IMPRE_FALLO_TRANSM;
//			if(tipoImpresora == IMPREtip_Kas)
//				impre_enviar("\xff", 1);
		}
		last_event = events_signaled;
		if(!impre.imprimint && (impre.pendent_preg_status || impre.timeout_resp_status))
		{
			if(chars_to_process == 0)
			{
				if(msg_to_purge)
				{
					OS_Q_Purge(&ColaImpre);
					msg_to_purge=0;
				}
				chars_to_process = OS_Q_GetPtrCond(&ColaImpre,(void**)&buf_rec);
				if(chars_to_process)
					msg_to_purge=1;
			}
			
			while (chars_to_process || impre.bitmap.y)
			{
				if(impre.pendent_reset_font)
				{
					switch(tipoImpresora){
					case 2:			//IR32
						buf_aux[0] = 0x1b;
						buf_aux[1] = tipos_letra[FONTSIZE_DEFAULT].tipo_externa;
						impre_enviar(buf_aux,2);
						break;
					case 3:			//Optronic
						buf_aux[0] = 0x1b;
						buf_aux[1] = 0x37;
						buf_aux[2] = tabFontsOptronicW[FONTSIZE_DEFAULT];
						buf_aux[3] = 0x1b;
						buf_aux[4] = 0x38;
						buf_aux[5] = tabFontsOptronicH[FONTSIZE_DEFAULT];
						impre_enviar(buf_aux,6);
						break;
					case 4:			//Kashino
						buf_aux[0] = 0x1b;
						buf_aux[1] = 0x21;
						buf_aux[2] = tabFontsKashino[FONTSIZE_DEFAULT];
						impre_enviar(buf_aux,3);
						break;
					}
					
					impre.FontSize = FONTSIZE_DEFAULT;
					impre.pendent_reset_font = 0;
				}
				if(impre.bitmap.y)
				{
					envia_linia_grafica();
					st = 0;   // a esperar                    
					break;
				}
				ch = *buf_rec++;
				chars_to_process--;
				st = -1;
				switch(ch)
				{
				case IN:  // Inici tiquet
					impre.ticket_ok = 1;
					impre.ticket_ret_event = *buf_rec++;  // ho treu de seguent caracter
					chars_to_process--;
					break;
				case FI:  // Final tiquet
					if(impre.ticket_ret_event)
					{
						impre_event_call_back(impre.ticket_ret_event, impre.ticket_ok);
						impre.ticket_ret_event = 0;
					}
					break;
				case CR:
					if(coplin==0)
					{
						envia_texte("\x0d",1,1);
						st = 1;
					}
					else 
					{
						baux[coplin] = 0x0d;
						envia_texte((char *)baux,coplin+1,1);
						st = 1;
					}
					coplin=0;
					break;
				case LF:
					envia_texte("\x0d",1,1);
					st = 1;
					break;
				case FF:
					envia_texte("\x0c",1,1);
					st = 1;
					break;
				case GR:    // imprimir bitmap
					n1 = bitmap_init(buf_rec,chars_to_process);
					chars_to_process -= n1;
					if(chars_to_process <0)
						chars_to_process = 0;   // no hauria de passar mai
					buf_rec += n1;
					if(impre.bitmap.y)
					{
						envia_linia_grafica();
						st = 1;   // a esperar
						break;
					}
					break;
                case QR:
                    n1 = resendQrCode(buf_rec,chars_to_process);
                    chars_to_process -= n1;
                    buf_rec += n1;
                    break;
				case FS:
					n1 = *buf_rec++;
					chars_to_process--;
					if(n1 >= DIM(tipos_letra))
						n1 = FONTSIZE_DEFAULT;
					impre.FontSize = n1;

					switch(tipoImpresora){
					case 2:			//IR32
						buf_aux[0] = 0x1b;
						buf_aux[1] = tipos_letra[n1].tipo_externa;
						impre_enviar(buf_aux,2);
						break;
					case 3:			//Optronic
						buf_aux[0] = 0x1b;
						buf_aux[1] = 0x37;
						buf_aux[2] = tabFontsOptronicW[n1];
						buf_aux[3] = 0x1b;
						buf_aux[4] = 0x38;
						buf_aux[5] = tabFontsOptronicH[n1];
						impre_enviar(buf_aux,6);
						break;
					case 4:			//Kashino
						buf_aux[0] = 0x1b;
						buf_aux[1] = 0x21;
						buf_aux[2] = tabFontsKashino[n1];
						impre_enviar(buf_aux,3);
						break;
					}
					st = 1;
					break;
					
				default :
					if(coplin==0) memset(baux,' ',64);
					if(coplin<64) baux[coplin++]=ch;
					break;
				} // final switch
				if(st == -1)
					continue;
				if(st ==0)
				{
					// cas error en imprime_low_level
				}
				else
				{
					// cas a esperar acabi imprime_low_level
					break;
				}
			}  //  while (chars_to_process || impre.bitmap.y)
		}     //  if(!impre.imprimint)
		if(!impre.parada || impre.imprimint){
			if(!impre.timer_resp_status && !impre.timer_preg_status)
			{
				preguntar_status();
			}      
			if(impre.reconfig)
				reconfig_impre();
		}
	} // while(1)
}

CODE_FAST void INT_APAGAR_CAPSALS(void)
{
  OS_IncDI();
  PORT3 &= 0xC0;
  IOPORT_3 = PORT3;
  OS_DecRI();
}

// caracter rebut a traves canal multiplexat bt40
void impre_rcvd_status_mpxd(char ch)
{
          impre.ch_rcvd = ch;
          OS_SignalEvent(EVENTIMPRE_STATUS_RCVD,&TCB_drvImpre);
}

extern int OS_OnTx_UART1(void);
CODE_FAST void UART1_IRQHandler_Impre(void) {
volatile unsigned short _Dummy;
  do {
      if (UART1_SR_bit.RxBufNotEmpty)                // Data received?
      {
        if (UART1_SR_bit.ParityError || UART1_SR_bit.FrameError ||UART1_SR_bit.OverrunError)      // Any error ?
        {  
          _Dummy = UART1_RXBUFR_bit.RX;                  // => Discard data
        }
        else
        {
          // Process actual byte
          impre.ch_rcvd = UART1_RXBUFR;
          OS_SignalEvent(EVENTIMPRE_STATUS_RCVD,&TCB_drvImpre);
        }
      }
    
      if (UART1_SR_bit.TxEmpty&& (UART1_IER_bit.TxEmptyIE == 1))                 // Check Tx status => Send next character
      {
        // !!! no es pot escriure  UART1_SR_bit.TxHalfEmpty = 0;                 // Clear Tx Int
        if (OS_OnTx_UART1())            // No more characters to send ?
        {
          UART1_IER_bit.TxEmptyIE = 0; // Disable further tx interrupts
          usart[1].flag_tx_activa = 0;  // desactiva transmissio
        }
      }
     } while ((UART1_IER_bit.TxEmptyIE && UART1_SR_bit.TxEmpty) || (UART1_IER_bit.RxBufNotEmptyIE && UART1_SR_bit.RxBufNotEmpty));
}

// ********************************************************
// inicialitza impressora
// ********************************************************
// crida:
//   tipus 
//          0   no hi ha impre
//          1   interna
//          2   externa IR32
//			3	externa Optronic
//
void impre_init(int tipus, int tipus_td30)
{
  impre.hay_impresora = (tipus == 0)? 0:1;
  tipoImpresora = tipus;
  if(hay_impresora())
  {
    // determina si impressora interna o externa    
    impre.externa = (tipus == 1)? 0:1;
    if(impre.externa)
    {
      // cas transmissio amb impressora externa 
      impre.pendent_preg_status = 0;
      impre.timer_preg_status = 0;
      impre.timer_resp_status = 0;
      impre.timeout_resp_status = 0;
      
      // determina tipus conexio externa
      if(tipus_td30 == 4)
      {
        // cas conectada a traves de BT40 (multiplex)
        // multiplex init ja el fara td30_init
        impre_enviar = impre_enviar_multiplex;
        impre_preg_status = impre_preg_status_multiplex;
      }
      else
      {
        // conectada a Usart 1
        usart_install(USART_IMPRE,UART1_IRQHandler_Impre);
        usart_hard(USART_IMPRE, MODO_232, 0, 1, 0);
        usart_init(USART_IMPRE, 9600, 1,0);     // 9600, 8bits no parity, dum
        impre_enviar = impre_enviar_usart;
        impre_preg_status = impre_preg_status_usart;
      }
	  reconfig_impre();
	  impre.pendent_reset_font = 1;
    }
    else
    {
      // cas impressora interna
 
      // Initialize timer3 
      TIM3_CR1 = 0;                     // Stop timer, reset mode
      TIM3_CR2 = 0;                     // Disable all interrupts, clear prescaler
      TIM3_OCAR = _OS_TIMER_IMPRE_INTERVAL;  // Setup compare register, initially use 1000 ticks per second
      TIM3_CR1_bit.EN  = 1 ;                 // Start timer
      TIM3_CR2_bit.OCAIE = 1 ;               // Enable output capture interrupt A

      // instala interrupcio temps INT3     
      OS_ARM_InstallISRHandler(_OS_TIMER_IMPRE_ID, &_OS_ISR_TickImpre);   // Timer impre interrupt vector.
      OS_ARM_ISRSetPrio(_OS_TIMER_IMPRE_ID, _OS_TIMER_IMPRE_PRIO);              // Timer/counter interrupt priority.
      OS_ARM_EnableISR(_OS_TIMER_IMPRE_ID);                               // Enable timer3 interrupt.
                  
      impre.step = 0;
      timTemperatura = TEMPERATURA_TICKS;
      timLed = LED_TICKS;
      tmo_startup_motor = 0;
      tmo_motor = 0;
      warmuploop = 0;    
    }
    
    impre.FontSize = FONTSIZE_DEFAULT;
    impre.falta_paper = 0;
    impre.faltava_paper = 0;
    impre.ticket_ok = 1;
    impre.ticket_ret_event = 0;    
    impre.timer_ticket_no_legal = 0;
    impre.status = 0;
	impre.parada = 0;
    subtabla = 0;
    set_tabla_impre(subtabla);   // inicialitza amb subtabla 0
    
    OS_Q_Create(&ColaImpre, &ColaImpreBuffer, sizeof(ColaImpreBuffer));

    if(impre.externa)
    {
        OS_CreateTimer(&Impre_Timer, ImpreExterna_interrupt_timer,1); // interrupcio cada 1ms.
        OS_StartTimer(&Impre_Timer);
    
        OS_CREATETASK(&TCB_drvImpre, " Task_drvImpre", Task_drvImpreExterna, 250, Stack_drvImpre);
    }
    else
    {
        OS_CreateTimer (&Impre_Timer, ImpreInterna_interrupt_timer,1); // interrupcio cada 1ms.
        OS_StartTimer(&Impre_Timer);
    
        OS_CREATETASK(&TCB_drvImpre, " Task_drvImpre", Task_drvImpreInterna, 250, Stack_drvImpre);
    }
  }
  
}


//********************************************************
// impressio d�un buffer, linea por linea hasta final.
//********************************************************
#define LINE_SIZE_IR30      64
void imprimir_buffer(char *buf,int len, int add_ret)
{
char *bufimp;
char ch;
int i,pb,nc_lin,fin;
if(hay_impresora() && len)
{
 int lentot=len+(len+LINE_SIZE_IR30)/LINE_SIZE_IR30+3;
 bufimp=(char*)OS_malloc(lentot);
 pb=0;

 i=0,fin=0;     
 while(!fin)
   {
    nc_lin=0;
	ch=0;
    do
       {
        // Llenar/preparar bufimp[] con car�cteres entrantes.
        ch=buf[i++];			//sacar car�cter actual del b�fer entrante
        if(/*(ch==0)||*/(i>len))
          {
           if(nc_lin)
             if(add_ret)
               bufimp[pb++]=0x0d;
           bufimp[pb]=0;  
           fin=1;
           break;
          }
        if(ch==0x0a)
           {
            if(nc_lin)
              bufimp[pb++]=0x0d;
            break;
           } 
        if(ch==0x0d)
           {
            //if(nc_lin)
                bufimp[pb++]=0x0d;
            break;
           } 
        if(nc_lin>=LINE_SIZE_IR30)
           {
            bufimp[pb++]=0x0d;
            i--;   // per que torni a agafar el caracter
            break;
           } 
        bufimp[pb++]=ch;
        nc_lin++;   
       }while(pb<lentot);
   }
    OS_Q_Put(&ColaImpre,bufimp,pb);    //Imprimir linea por linea hasta final.
    OS_free(bufimp);
}
}

#define MS_TIMER_TICKET_NO_LEGAL  1500

void imprimir_ticket_no_legal(char *buf,int len)
{
char *bufimp;
char ch;
int i,pb,nc_lin,fin,fi_ticket;

  if(hay_impresora() && len)
  {
     if(impre.timer_ticket_no_legal == 0)
        imprimir(TICKET_CABE_NO_LEGAL);
     impre.timer_ticket_no_legal = MS_TIMER_TICKET_NO_LEGAL;
     bufimp=(char*)OS_malloc(len+300); // maxim de 100 linies
     pb=0;
    
     i=0,fin=0,fi_ticket=0;     
     while(!fin)
       {
        // inici linia
        bufimp[pb++] = 0x1B;  // ESC
        bufimp[pb++] = 'B';   // 32 chars per linia
        bufimp[pb++] = '/';
        bufimp[pb++] = ' ';
        
        nc_lin=0;
        ch=0;
        do
           {
            // transferir car�cteres a   bufimp[]
            ch=buf[i++];		// treure car�cter actual del b�fer
            
            if(ch == 0x0f)  // prescindeix inici  ticket
              continue;
            if((ch==0) || (i>len) || (ch == 0x0e))
              {
                // buffer d'entrada ja acabat o final ticket
               if(nc_lin)
               {
                 if(bufimp[pb] != 0x0d)
                    bufimp[pb++] = 0x0d;
               }
               else
                 pb -= 4;  // elimina inici de l�nia
               bufimp[pb]=0;  
               fin=1;
               if(ch == 0x0e)
                 fi_ticket = 1;
               break;
              }
            if((ch==0x0a) || (ch==0x0d))
               {
                bufimp[pb++]=0x0d;
                break;
               }
            if(nc_lin>=LINE_SIZE_IR30)
               {
                bufimp[pb++]=0x0d;
                i--;   // per que torni a agafar el caracter
                break;
               } 
            bufimp[pb++]=ch;
            nc_lin++;   
           }while(1);
       }   // while(!fin)
        if(pb)
          OS_Q_Put(&ColaImpre,bufimp,pb);    //Imprimir linea por linea hasta final.
        OS_free(bufimp);
        if(fi_ticket)
        {
          imprimir(TICKET_PEU_NO_LEGAL);
          impre.timer_ticket_no_legal = 0;
        }
  }
}



char get_status_impre(void)
{
   return impre.status;
}

int hay_impresora(void)
{
  return(impre.hay_impresora != 0);
}

char get_impre_imprimint(void)
{
   return impre.imprimint;
}


//*************************************************
// impressio d'un ticket  (a impressora)
//*************************************************

void imprimir(char n_ticket)
{
    if(hay_impresora() && (ticket[n_ticket] != NULL))
        x_impre(ticket[n_ticket],64000,0);
    // es posa a 0 per que si es cola un tiquet legal al mig
    // torni a posar la cap�alera de no legal quan continui el no legal
    impre.timer_ticket_no_legal = 0;
}



