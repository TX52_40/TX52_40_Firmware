// *************************************************
// ccab.h
// *************************************************


#ifndef _INC_CCAB
#define _INC_CCAB

struct s_ccab
{
 char logon;  //  0/1 no si logged on
 char control;  // si diferent 0 control rebut
 char resp_en_curs;
 char nc_env;
 char len_env;
 char buf_env[12];
};

extern struct s_ccab ccab;

extern void ccab_init(char hi_ha_ccab);
extern void gestio_ccab(void);

#endif



