/******************** (C) COPYRIGHT 2003 STMicroelectronics ********************
* File Name          : i2c.c
* Author             : MCD Application Team
* Date First Issued  : 09/05/2003
* Description        : This file provides Code sources I2C functions
********************************************************************************
* History:
*  13/01/2006 : V3.1
*  24/05/2005 : V3.0
*  30/11/2004 : V2.0
*  14/07/2004 : V1.3
*  01/01/2004 : V1.2
*******************************************************************************
 THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS WITH
 CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
 AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT
 OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/


#include "i2carm.h"
#include "rccu.h"
#include <stdarg.h>
#include "io_macros.h"
#include "RTOS.h"
#include "hard.h"

/*******************************************************************************
* Function Name  : I2C_Init
* Description    : Initializes I2C peripheral control and registers to their
*                  default reset values.
* Input          : I2Cx ( I2C0 or I2C1 )
* Return         : None.
*******************************************************************************/
void I2C_Init (void)
{
  /* Initialize all the register of the specified I2C passed as parameter */
  unsigned char aux;
  
  I2C0.CR=0x0;
  I2C0.CCR=0x0;
  I2C0.ECCR=0x0;
  I2C0.OAR1=0x0;
  I2C0.OAR2=0x0;
  aux=I2C0.SR1;
  aux=I2C0.SR2;
  aux++;
  I2C0.DR=0x0;
}

/*******************************************************************************
* Function Name  : I2C_OnOffConfig
* Description    : Enables or disables I2C peripheral.
* Input          : I2Cx ( I2C0 or I2C1 )
*                  condition(ENABLE or DISABLE).
* Return         : None.
*******************************************************************************/
void I2C_OnOffConfig ( FunctionalState NewState)
{
  if (NewState == ENABLE)
    {
      /* Enable the I2C selected by setting twice the PE bit on the CR register */
      I2C0.CR |= I2C_PESET_Mask;
      I2C0.CR |= I2C_PESET_Mask;
    }
  else
      /* Disable the I2C selected */
      I2C0.CR &= ~I2C_PESET_Mask;
}

/*******************************************************************************
* Function Name  : I2C_FlagStatus
* Description    : Checks whether any I2C Flag is set or not.
* Input          : I2Cx ( I2C0 or I2C1 )
*                  Access(DIRECT or INDIRECT)
*                  Flag : the flag to be read
*                  input 4: an (u8) variable needed in the case
*                                     of the INDIRECT access
* Return         : the NewState of the Flag (SET or RESET).
*******************************************************************************/
FlagStatus I2C_FlagStatus ( RegisterAccess Access, I2C_Flags Flag, ...)
{
  u32 Tmp=0;
  
  if (Access == DIRECT)
    /* Store in Tmp variable the register where is located the flag */
    Tmp = I2C_GetStatus()&Flag;
  else
  { 
    va_list list;  
    /* Get the fourth register */
    va_start(list,Flag);
    Tmp = va_arg(list,u32);
    Tmp&=Flag;
  }
  /* Return the Flag NewState */
  return Tmp != 0 ? SET : RESET;
}

/*******************************************************************************
* Function Name  :  I2C_FlagClear
* Description    : Clears the I2C Flag passed as a parameter
* Input          : I2Cx ( I2C0 or I2C1 )
*                  Flag : the flag to be read
*                  input3: an (u8) parameter needed in the case that the flag
*                         to be cleared need a write in one register
* Return         : None.
*******************************************************************************/
void I2C_FlagClear ( I2C_Flags Flag, ...)
{
  u8 Tmp = (u8)*((u32 *)&Flag + sizeof(Flag));

  if (Flag==I2C_ADD10 || Flag==I2C_EVF || Flag==I2C_BERR || Flag==I2C_ARLO ||
      Flag==I2C_STOPF || Flag==I2C_AF  || Flag==I2C_ENDAD)

  /* flags that need a read of the SR2 register to be cleared */
  {
    /* Read the SR2 register */
    (void)I2C0.SR2;
    /* Two flags need a second step to be cleared */
    switch (Flag)
    {
      case  I2C_ADD10: I2C0.DR = Tmp; break;
      case  I2C_ENDAD: I2C0.CR|=0x20; break;
    }
  }
  else if (Flag==I2C_SB || Flag==I2C_ADSL || Flag==I2C_BTF || Flag==I2C_TRA)
  /* flags that need a read of the SR1 register to be cleared */
  {
  	/* Read the SR1 register */
    (void)I2C0.SR1;
    if (Flag==I2C_SB) I2C0.DR=Tmp;
    else if (Flag==I2C_BTF || Flag==I2C_TRA) (void)I2C0.DR;
  }
  else if ( Flag==I2C_M_SL || Flag==I2C_GCAL)
  /*flags that need the PE bit to be cleared */
  {
    I2C_OnOffConfig ( DISABLE);
    I2C_OnOffConfig ( ENABLE);
  }
}

/*******************************************************************************
* Function Name  : I2C_SpeedConfig
* Description    : Selects I2C clock speed and configures its corresponding mode.
* Input          : I2Cx ( I2C0 or I2C1 )
*                  Clock: I2C expected clock in Hertz.
* Return         : None.
*******************************************************************************/
void I2C_SpeedConfig ( u32 Clock)
{
  u32 FCLK=0;
  u16 result=0;
  /* Get the FCLK frequency using the RCCU library */
  FCLK = RCCU_FrequencyValue ( RCCU_FCLK );
  /* Test on speed mode */
  /* Update the CCR and ECCR are updated */
  if (Clock <=100000)
  /* Standard mode selected */
  {
    result = ((FCLK / Clock) - 7)/2;
    /* Clear FM/SM bit */
    I2C0.CCR=result &0x7f;
  }
  else if (Clock <=400000)
  {
    /* Fast mode selected */
    result = ((FCLK/Clock)-9)/3;
    /* set FM/SM bit */
    I2C0.CCR=result |0x80;
  }
  I2C0.ECCR= result >>7;
}
/*******************************************************************************
* Function Name  : I2C_AddressConfig
* Description    : Defines the I2C bus address of the interface.
* Input          : I2Cx ( I2C0 or I2C1 ).
*                  Address: an u16 parameter indicating the address
*                           of the interface.
*                  Mode (I2C_Mode10,I2C_Mode7).
* Return         : None.
*******************************************************************************/
void I2C_AddressConfig ( u16 Address, I2C_Addressing Mode)
{
  /* Update OAR1 bit[7:1] by the lowest byte of address */
  I2C0.OAR1 = (u8)Address;

  if (Mode == I2C_Mode10)
    /* Update Add8 and add9 bits in OAR2 */
    I2C0.OAR2 |= (Address & 0x0300)>>7;
}

/*******************************************************************************
* Function Name  : I2C_FCLKConfig
* Description    : Configures frequency bits according to RCLK frequency.
*                  the selected I2C  must be disabled
* Input          : I2Cx ( I2C0 or I2C1 )
* Return         : None.
*******************************************************************************/
void I2C_FCLKConfig (void)
{
  u32 FCLK=0;
     /* Get the FCLK frequency using the RCCU library */
  FCLK = RCCU_FrequencyValue ( RCCU_FCLK );
     /* Test the value of the FCLK and affect FR0,FR1&FR2 of the OAR2 register */
  if (FCLK > 5000000)
    {
    if (FCLK <10000000)
      I2C0.OAR2 |= 0x00;
    else if (FCLK <16670000)
      I2C0.OAR2 |= 0x20;
    else if (FCLK < 26670000)
      I2C0.OAR2 |= 0x40;
    else if (FCLK <40000000)
      I2C0.OAR2 |= 0x60;
    else if (FCLK < 53330000)
      I2C0.OAR2 |= 0x80;
    else if (FCLK < 66000000)
      I2C0.OAR2 |= 0xA0;
    else if (FCLK <80000000)
      I2C0.OAR2 |= 0xC0;
    else if (FCLK <100000000)
      I2C0.OAR2 |= 0xE0;
   }
}

/*******************************************************************************
* Function Name  : I2C_AddressSend
* Description    : Transmits the address byte to select the slave device.
* Input          : I2Cx ( I2C0 or I2C1 )
*                  Address: an u16 parameter indicating the slave address
*                  Mode (I2C_Mode10,I2C_Mode7).
*                  Direction (I2C_RX,I2C_TX).
* Return         : None.
********************************************************************************/
void I2C_AddressSend ( u16 Address, I2C_Addressing Mode, I2C_Direction Direction)
{
  if (Mode == I2C_Mode10 )
  /*10 bit addressing mode */
  {
    /* Update the DR register by generated header */
    I2C0.DR = ((Address>>7)|0xf0)&0xfe;
    /* Wait till I2C_ADD10 flag is set */
    while ((I2C0.SR1&0x40)==0);
    /* clear I2C_ADD10 flag */
    (void)I2C0.SR2;
    delay_us(25);
    I2C0.DR=(u8)Address;
    /* Test on the direction to define the read/write bit */
    if (Direction == I2C_RX)
    {
      /* Wait till I2C_ENDAD flag is set */
      while ((I2C0.SR2&0x20)==0);
      I2C0.CR|=0x20;
      /* Repeated START Generate */
      delay_us(25);
      I2C_STARTGenerate ( ENABLE);
      /* Test on SB flag status */
      while ((I2C0.SR1&0x01)==0);
      I2C0.DR = ((Address>>7)|0xf1);
    }
  }
  else
  /* 7 bit addressing mode */
  {
    if (Direction == I2C_RX) Address|=0x01; else Address&=~0x01;
    I2C0.DR=(u8)Address;
  }
}

/*******************************************************************************
* Function Name  : I2C_ByteSend
* Description    : Send a single byte of data.
* Input          : I2Cx ( I2C0 or I2C1 )
*                  Data : the byte to be sent to the slave
* Return         : None.
*******************************************************************************/
void I2C_ByteSend ( u8 Data)
{
  /* Write in the DR register the byte to be sent */
  I2C0.DR = Data;
}


/*******************************************************************************
* Function Name  : I2C_TransmissionStatus
* Description    : Report the NewState of the transmission
* Input          : I2Cx ( I2C0 or I2C1 )
* Return         : I2C_Tx_Status :transmission status (I2C_TX_NO, I2C_TX_SB,
*                   I2C_TX_AF, I2C_TX_ARLO, I2C_TX_BERR,I2C_TX_ADD_OK,
*                   I2C_TX_DATA_OK,I2C_TX_ONGOING)
*******************************************************************************/
I2C_Tx_Status I2C_TransmissionStatus (void)
{
  u8 SR1value=0;
  u8 SR2value=0;
  I2C_Tx_Status NewState = I2C_TX_NO;

  SR1value = I2C0.SR1;
  SR2value = I2C0.SR2;
  if ((I2C0.SR1&0x10)==0)
    NewState=I2C_TX_NO;
  else if (I2C0.SR1&0x01)
    /* I2C_SB bit is set */
    NewState=I2C_TX_SB;
  else if ((SR2value & 0x10)&&(I2C0.CR&0x04))
    /* I2C_ACK &I2C_AF are both set */
    NewState=I2C_TX_AF;
  else if (SR2value & 0x04)
    /* I2C_ARLO is set in multimaster mode */
    NewState=I2C_TX_ARLO;
  else if (SR2value & 0x02)
    /* I2C_BERR bit is set */
    NewState=I2C_TX_BERR;
  else if ((SR1value & 0x80)&& (I2C0.SR2&0x20))
    /* I2C_EVF and I2C_ENDAD are both set */
    NewState=I2C_TX_ADD_OK;
  else if ((I2C0.SR1&0x20)&& (I2C0.SR1&0x08))
    /* I2C_TRA and I2C_BTF are both set */
    NewState=I2C_TX_DATA_OK;
  else
    NewState=I2C_TX_ONGOING;

  return NewState;
}

/*******************************************************************************
* Function Name  : I2C_ByteReceive
* Description    : Returns the received byte.
* Input          : I2Cx ( I2C0 or I2C1 )
* Return         : the byte received
*******************************************************************************/
u8 I2C_ByteReceive (void)
{
   /* Wait till I2C_BTF bit is set */
  while ((I2C0.SR1 & 0x08)==0);
  delay_us(25);
  return I2C0.DR;
}


/*******************************************************************************
* Function Name  :I2C_ReceptionStatus
* Description    : Report the reception NewState.
* Input          : I2Cx ( I2C0 or I2C1 )
* Return         : I2C_Rx_Status:the NewState of the reception ( I2C_RX_NO,
*                  I2C_RX_SB,I2C_RX_AF,I2C_RX_ARLO,I2C_RX_BERR,I2C_RX_ADD_OK,
*                  I2C_RX_DATA_OK, I2C_RX_ONGOING)
*******************************************************************************/
I2C_Rx_Status I2C_ReceptionStatus (void)
{
  u8 SR1value=0;
  u8 SR2value=0;
  I2C_Rx_Status NewState = I2C_RX_NO;
  SR1value= I2C0.SR1;
  SR2value= I2C0.SR2;

  if ((I2C0.SR1&0x10) == 0)
    NewState=I2C_RX_NO;
  else if (I2C0.SR1&0x01)
    /* I2C_SB bit is set */
    NewState=I2C_RX_SB;
  else if ((SR2value & 0x10) && (I2C0.CR&0x04))
    /* I2C_ACK &I2C_AF are both set */
    NewState=I2C_RX_AF;
  else if (SR2value & 0x04)
    /* I2C_ARLO is set */
    NewState=I2C_RX_ARLO;
  else if (SR2value & 0x02)
    /* I2C_BERR bit is set */
    NewState=I2C_RX_BERR;
  else if ((SR1value & 0x80) && (I2C0.SR1&0x08)==0)
    /* I2C_EVF is set & I2C_BTF is not set */
    NewState=I2C_RX_ADD_OK;
  else if ((I2C0.SR1&0x20)==0 && (I2C0.SR1&0x08))
    /* I2C_TRA is cleared & I2C_BTF is set */
    NewState=I2C_RX_DATA_OK;
  else
  NewState=I2C_RX_ONGOING;
  return NewState;
}

/******************* (C) COPYRIGHT 2003 STMicroelectronics *****END OF FILE****/









/*******************************************************************************
* Function Name  : I2C0_Config
* Description    : Configure the I2C0 interface
* Input          : None
*******************************************************************************/

void I2C0_Config(void)
{
  
  
//  /* Configure the SDA and the SCL lines to alternate functions Open Drain */
//  GPIO_Config (GPIO1, I2C_SCL|I2C_SDA, GPIO_AF_OD);
  
  
  /* Initialize the I2C0 peripheral */
  I2C_Init ();
  /* Configure frequency bits */
  I2C_FCLKConfig ();
  /* Enable I2C0 peripheral */
  I2C_OnOffConfig ( ENABLE);
  /* Configure I2C0 clock speed */
  I2C_SpeedConfig ( tipoTx==TX40 ? 100000 : 20000);
  /* Enable Acknowledge */
  I2C_AcknowledgeConfig ( ENABLE);
}

/*******************************************************************************
* Function Name  : i2c_WaitForLastTask
* Description    : Wait for I2C EEPROM to terminate the last task.
* Input          : None.
* Return         : None.
*******************************************************************************/
void i2c_WaitForLastTask(unsigned char slaveAddr)
{
u32 I2CStatus;
volatile int cnt =0;
volatile int cnt1 =0;
  do
  {
    cnt1++;
    I2C_STARTGenerate(  ENABLE );
    while( I2C_FlagStatus(  DIRECT, I2C_SB) == RESET )cnt++;
    I2C_AddressSend(  slaveAddr, I2C_Mode7, I2C_TX );
    while(!((I2CStatus = I2C_GetStatus(  ))& I2C_EVF))cnt++;
    while( I2C_FlagStatus(  DIRECT, I2C_ENDAD ) ==RESET )cnt++;
    I2C_FlagClear(  I2C_ENDAD );
  }while( I2C_FlagStatus(  INDIRECT, I2C_AF, I2CStatus ) == SET );
}


/*******************************************************************************
* Function Name  : wread_i2c
* Description    : Read a buffer from the I2C slave.
* Input          : I2Cx (I2C0 or I2C1).
*                  slaveAddr
*                  internal address from which data will be read.
*                  Buffer size.
*                  Buffer where read data are put.
* Return         : None.
*******************************************************************************/

int wread_i2c_arm(unsigned char slaveAddr,u8 InternalAddress,
              u8 NbOfBytes,
              char *PtrToBuffer
                     )
{
  if (NbOfBytes==0) return 0;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB )== RESET);
  /* Send the slave address with LSB bit reset */
  delay_us(25);
  I2C_AddressSend (slaveAddr,I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  int count=10000;
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD )== RESET){
	  count--;
	  if(count==0){
		  return 1;
	  }
  }
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  /* Send the  internal address to read from */
  delay_us(25);
  I2C_ByteSend ( InternalAddress);
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);
  /* Generate the RE-START condition */
  delay_us(25);
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB )==RESET);

  if(NbOfBytes==1)
  { /* Disable the ACK generation */
    I2C_AcknowledgeConfig ( DISABLE);
  }

  /* Send the slave address with LSB bit set */
  delay_us(25);
  I2C_AddressSend ( slaveAddr, I2C_Mode7, I2C_RX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  delay_us(25);
  I2C_FlagClear ( I2C_ENDAD);

/* Read 'NbOfBytes' bytes from the EEPROM starting from address 'InternalAddress'
of Block3 and place them in PtrToBuffer[] array */

while(NbOfBytes)
{
   /* Wait until the byte is received */
   while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);

  if(NbOfBytes==2)
  { /* Disable the ACK generation */
    I2C_AcknowledgeConfig ( DISABLE);
  }

  if (NbOfBytes==1)
    {
  /* Generate STOP condition to close the communication after the
     next byte reception */
      delay_us(25);
      I2C_STOPGenerate ( ENABLE);
    }

  *PtrToBuffer=I2C_ByteReceive ();
   PtrToBuffer++;
   NbOfBytes--;
}

  /* Enable the ACK generation */
  I2C_AcknowledgeConfig ( ENABLE);
  return 0;
}

/*******************************************************************************
* Function Name  : write_i2c
* Description    : Send data to the I2C slave.
* Input          : I2Cx (I2C0 or I2C1).
*                  slaveAddr 
*                  internal address from which data will be read.
*                  Buffer size.
*                  Buffer containing data to be written .
* Return         : None.
*******************************************************************************/
void write_i2c_arm(unsigned char slaveAddr,u8 InternalAddress,
                 u8 NbOfBytes,char *PtrToBuffer )
{

u8 SentBytes =0;

  if (NbOfBytes==0) return;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB)==RESET);
  /* Send the slave's address with LSB bit reset */
  delay_us(25);
  I2C_AddressSend ( slaveAddr, I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  /* Send the  internal address to write to */
  delay_us(25);
  I2C_ByteSend (InternalAddress);
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);
  /* Write 'PtrToBuffer' buffer contents  starting from address 'InternalAddress' of
  Block3 */

   while (SentBytes<NbOfBytes)
  {
    delay_us(25);
    I2C0.DR= *(PtrToBuffer+SentBytes);
    /* Wait till I2C_BTF bit is set */
    while (!(I2C0.SR1 & 0x08));
    SentBytes++;
   }
   /* Generate the stop condition */
  delay_us(25);
  I2C_STOPGenerate ( ENABLE);

 }

void write_i2c_disp_arm(unsigned char slaveAddr, u8 NbOfBytes,char *PtrToBuffer )
{

u8 SentBytes =0;

  if (NbOfBytes==0) return;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB)==RESET);
  /* Send the slave's address with LSB bit reset */
  delay_us(25);
  I2C_AddressSend ( slaveAddr, I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);

  /* Write 'PtrToBuffer' buffer contents  starting from address 'InternalAddress' of
  Block3 */
   while (SentBytes<NbOfBytes)
  {
    delay_us(25);
    I2C0.DR= *(PtrToBuffer+SentBytes);
    /* Wait till I2C_BTF bit is set */
    while (!(I2C0.SR1 & 0x08));
    SentBytes++;
   }
   /* Generate the stop condition */
  delay_us(25);
  I2C_STOPGenerate ( ENABLE);

 }

/*******************************************************************************
* Function Name  : wread_i2c_extnd
* Description    : Read a buffer from the I2C slave.
* Input          : I2Cx (I2C0 or I2C1).
*                  slaveAddr
*                  internal address ( u16) from which data will be read.
*                  Buffer size   (u16).
*                  Buffer where read data are put.
* Return         : None.
*******************************************************************************/

void wread_i2c_extnd_arm(unsigned char slaveAddr,u16 InternalAddress,
              u16 NbOfBytes,
              char *PtrToBuffer)
{

  if (NbOfBytes==0) return;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB )== RESET);
  /* Send the slave address with LSB bit reset */
  I2C_AddressSend (slaveAddr,I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD )== RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  /* Send the  internal address to read from */
  // upper 8 bits
  I2C_ByteSend ( (unsigned char)(InternalAddress>> 8));
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);
 
  /* Send the  internal address to read from */
  // lower 8 bits
  I2C_ByteSend ( (unsigned char)(InternalAddress & 0xff));
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);
  
  
/* Generate the RE-START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB )==RESET);
  /* Send the slave address with LSB bit set */

  if(NbOfBytes==1)
  { /* Disable the ACK generation */
    I2C_AcknowledgeConfig ( DISABLE);
  }

  I2C_AddressSend ( slaveAddr, I2C_Mode7, I2C_RX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);

/* Read 'NbOfBytes' bytes from the EEPROM starting from address 'InternalAddress'
of Block3 and place them in PtrToBuffer[] array */

while(NbOfBytes)
{
   /* Wait until the byte is received */
   while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);

  if(NbOfBytes==2)
  { /* Disable the ACK generation */
    I2C_AcknowledgeConfig ( DISABLE);
  }

  if (NbOfBytes==1)
    {
  /* Generate STOP condition to close the communication after the
     next byte reception */
      I2C_STOPGenerate ( ENABLE);
    }

  *PtrToBuffer=I2C_ByteReceive ();
   PtrToBuffer++;
   NbOfBytes--;
}

  /* Enable the ACK generation */
  I2C_AcknowledgeConfig ( ENABLE);
}

/*******************************************************************************
* Function Name  : write_i2c_extnd
* Description    : Send data to the I2C slave.
* Input          : I2Cx (I2C0 or I2C1).
*                  slaveAddr 
*                  internal address (u16)from which data will be read.
*                  Buffer size (u16).
*                  Buffer containing data to be written .
* Return         : None.
*******************************************************************************/
void write_i2c_extnd_arm(unsigned char slaveAddr,u16 InternalAddress,
                 u16 NbOfBytes,char *PtrToBuffer )
{

u16 SentBytes =0;

  if (NbOfBytes==0) return;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB)==RESET);
  /* Send the slave's address with LSB bit reset */
  I2C_AddressSend ( slaveAddr, I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  /* Send the  internal address to write to */
  // upper 8 bits
  I2C_ByteSend ((unsigned char)(InternalAddress>>8));
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);
  
  /* Send the  internal address to write to */
  // lower 8 bits
  I2C_ByteSend ((unsigned char)(InternalAddress & 0xff));
  /* Wait until BTF bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);

  /* Write 'PtrToBuffer' buffer contents  starting from address 'InternalAddress' of
  Block3 */

   while (SentBytes<NbOfBytes)
  {
    I2C0.DR= *(PtrToBuffer+SentBytes);
    /* Wait till I2C_BTF bit is set */
    while (!(I2C0.SR1 & 0x08));
    SentBytes++;
   }
   /* Generate the stop condition */
  I2C_STOPGenerate ( ENABLE);

 }


void output_i2c_arm(unsigned char sla, unsigned char dato)
{
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB)==RESET);
  /* Send the slave's address with LSB bit reset */
  I2C_AddressSend ( sla, I2C_Mode7, I2C_TX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  
  /* Write dato */
    I2C0.DR = dato;
    /* Wait till I2C_BTF bit is set */
    while (!(I2C0.SR1 & 0x08));
   /* Generate the stop condition */
  I2C_STOPGenerate ( ENABLE);
}

unsigned char input_i2c_arm(unsigned char sla)
{
unsigned char ret;
  /* Generate the START condition */
  I2C_STARTGenerate ( ENABLE);
  /* Wait until SB bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_SB)==RESET);
  /* Send the slave's address with LSB bit set */

   /* Disable the ACK generation */
    I2C_AcknowledgeConfig ( DISABLE);

  I2C_AddressSend ( sla , I2C_Mode7, I2C_RX);
  /* Wait until ENDAD bit is set */
  while (I2C_FlagStatus ( DIRECT, I2C_ENDAD)==RESET);
  /* Clear ENDAD bit */
  I2C_FlagClear ( I2C_ENDAD);
  
   /* Wait until the byte is received */
   while (I2C_FlagStatus ( DIRECT, I2C_BTF )==RESET);

  /* Generate STOP condition to close the communication after the
     next byte reception */
      I2C_STOPGenerate ( ENABLE);

  ret = I2C_ByteReceive ();
  
      
   /* Enable the ACK generation */
  I2C_AcknowledgeConfig ( ENABLE);

  return(ret & 0x80);
}


char status_i2c_arm(unsigned char sla)
{
  OS_Delay(5);
  return(0);  // nomes es crida al gravar e2prom
              // !!!! s'hauria de fer de veritat 
}

volatile __no_init __READ_WRITE I2C_TypeDef I2C0 @ I2C0_BASE;
//I2C_TypeDef *I2C0 = (I2C_TypeDef *)I2C0_BASE;

PCU_TypeDef * PCU =  ((PCU_TypeDef *)PCU_BASE);
RCCU_TypeDef * RCCU = ((RCCU_TypeDef *)RCCU_BASE);
