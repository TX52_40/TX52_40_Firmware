//***********************************************************
// cua.h
//***********************************************************

#ifndef __CUA_H
#define __CUA_H

typedef struct 
{
  int p_in;
  int p_out;
  int maxLen;
  char* buf;
}t_cua;

extern void cua_init(t_cua* pCua,char* buf, int maxLen);
extern int cua_bytes_lliures(t_cua* pCua);
extern int cua_extreure(t_cua* pCua, char* c);
extern int cua_guardar(t_cua* pCua,char* buf, int nc);
extern int cua_guardar_c(t_cua* pCua,char c);

#endif


