// display.c


#include "rtos.h"
#include "iostr710.h"
#include <math.h>

#include "spi.h"
#include "display.h"
#include "adc.h"
#include "rut30.h"
#include "tx30.h"
#include "i2c.h"
#include "hard.h"

#define CODE_FAST _Pragma("location=\"CODE_RAM_INTERNA\"")

void ledDriver_init(int numIC);
void ledDriver_dataWrite(int numIC, int pos, int num_c);
void ledDriver_dataRead(int numIc, unsigned char *buf, int num_c);
void ledDriver_control(int numIc,int on, char level);



unsigned char ram_leds_tx40[16];

unsigned short textes_frontal;
unsigned short textes_frontal_flash = 0xffff;

#define RAM_INTERNA _Pragma("location=\"RAM_INTERNA\"")

OS_STACKPTR RAM_INTERNA int Stack_display[150];     /* Task stack */
OS_TASK TCB_display;                    /* Task-control-block */


const unsigned char  SS[16]=
{
 0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7F,0x6f,
 0x77,0x7c,0x39,0x5e,0x79,0x71
};


// ***************************************************************************
// led Driver  PT6961
// ***************************************************************************


// copia a memoria ARM ram del chip PT6961
struct s_ledDriver_Ram ledDriver_Ram[2];

void ledDriver_init(int numIc)
{
  if(tipoTx==TX50)
  {
  spi_send(numIc, "\x02", 1); // set mode 6 * 12
  memset(ledDriver_Ram[numIc].buf,0x00,sizeof ledDriver_Ram[0].buf);
  ledDriver_dataWrite(numIc,0,MAX_DIGITS_DRIVER*2);
  ledDriver_control(numIc, 1, 7);
  }
  else
  {
  }
}

void ledDriver_dataWrite(int numIc, int pos, int num_c)
{
  unsigned char baux[(MAX_DIGITS_DRIVER)*2];

  if(num_c > (MAX_DIGITS_DRIVER*2)) num_c = MAX_DIGITS_DRIVER*2;
  
  spi_send(numIc, "\x40", 1); // data write
  baux[0] = 0xc0 | (unsigned char)pos;  // posicio inicial
  memcpy(baux+1,ledDriver_Ram[numIc].buf+pos,num_c);
  spi_send(numIc, baux, num_c+1);
  
}

void ledDriver_dataRead(int numIc, unsigned char *buf, int num_c)
{
  if(num_c > (MAX_RAWS_DRIVER)) num_c = MAX_RAWS_DRIVER;
  
  spi_recv(numIc,buf, num_c);
}

void ledDriver_control(int numIc,int on, char level)
{
  unsigned char mask;
  
  if (level>7 ) mask = 7; else mask = level;
  if(on)
    mask |= 0x88;
  else
    mask |= 0x80;
  spi_send(numIc, &mask, 1);
}



// **********************************************************************
// Deteccio llum ambient
// **********************************************************************

#define FOTO_DIODE_TICKS_TIMER  97 // per preguntar cada 100ms.
#define fotoDiode_NUMLECTURES 10

OS_TIMER fotoDiode_Timer;
int fotoDiode_arrayLectures[fotoDiode_NUMLECTURES];
int fotoDiode_indexArray;
int fotoDiode_sigmaArray;
int fotoDiode_valor;
int temperaturaPlaca;

#define ADC_FOTODIODE_TX50  1
#define ADC_FOTODIODE_TX40  0
#define ADC_TEMPERATURA_PLACA 1

#define FOTODIODE_GOT   0x01    // wait event

int fotoDiode_getValor(void)
{
  return(fotoDiode_valor);
}

int interrupt_nivell_llum = 0;

void fotoDiode_interrupt_timer(void)
{
int st;
int value;
int valor_AD;
double ad;
float temp;
int ttx;
int nivell_llum;
int itemp,max;

      ttx=(tipoTx==TX40) ? ADC_FOTODIODE_TX40 : ADC_FOTODIODE_TX50;
      value = adc_read(ttx,&st);  // llegeig conversor
      // acumula a array lliscant
      fotoDiode_sigmaArray += value;
      fotoDiode_sigmaArray -= fotoDiode_arrayLectures[fotoDiode_indexArray];
      fotoDiode_arrayLectures[fotoDiode_indexArray++] = value;
      if(fotoDiode_indexArray >= fotoDiode_NUMLECTURES)
      {
        fotoDiode_indexArray = 0;
        OS_SignalEvent(FOTODIODE_GOT, &TCB_display);
      }
      fotoDiode_valor = fotoDiode_sigmaArray/fotoDiode_NUMLECTURES;
      OS_RetriggerTimer(&fotoDiode_Timer);

      if(tipoTx==TX40)
        {
          valor_AD=adc_read(ADC_TEMPERATURA_PLACA,&st);  // llegeig conversor
          ad=valor_AD;
          temp = ad / (5406 - ad);  // 5406 = 2*V0 = 2*2048*3.3/2.5
          temp = log(temp);
          temp /= 4175;
          temp+=(1.0/(273.0+25.0));
          temp=(1.0 / temp) - 273.0;
          temperaturaPlaca = (int)temp;
          
          // prepara valors per Timer 1 ( Compare B)
          itemp = temperaturaPlaca;  // limita temps on si temperatura > 60
          if( itemp > 60)
          {
            max = 7-((itemp -60)/5);
            if(max < 0) max = 0;
          }
          else max = 7;
          
          nivell_llum = (fotoDiode_getValor() >> 9)+1;
          if(nivell_llum < tx30.nivell_llum_minim)
             nivell_llum = tx30.nivell_llum_minim;
          if(nivell_llum > max)
            nivell_llum = max;
          // ho traspassa per poder-ho agafar desde interrupcio Timer 1
          interrupt_nivell_llum = nivell_llum;
          
        }
}

void fotoDiode_init(void)
{
    memset(fotoDiode_arrayLectures,0,fotoDiode_NUMLECTURES);
    fotoDiode_indexArray= 0;
    fotoDiode_sigmaArray = 0;
    fotoDiode_valor = 0;
    OS_CreateTimer (&fotoDiode_Timer, fotoDiode_interrupt_timer,FOTO_DIODE_TICKS_TIMER);
    OS_StartTimer(&fotoDiode_Timer);
}




// ****************************************************************************
// rutines display
// ****************************************************************************


#define NDIG_LEDEST  2
#define NDIG_IMPORT  6
#define NDIG_EXTRES  4
#define NDIG_CARREGADOR 8

unsigned char RAM_DISP_LEDEST[NDIG_LEDEST];
unsigned char RAMFLASH_DISP_LEDEST[NDIG_LEDEST];
unsigned char RAMPUNTS_DISP_LEDEST[NDIG_LEDEST];
unsigned char RAMPUNTSFLASH_DISP_LEDEST[NDIG_LEDEST]={0xff,0xff};

unsigned char RAM_DISP_IMPORT[NDIG_IMPORT];
unsigned char RAMFLASH_DISP_IMPORT[NDIG_IMPORT];
unsigned char RAMPUNTS_DISP_IMPORT[NDIG_IMPORT];
unsigned char RAMPUNTSFLASH_DISP_IMPORT[NDIG_IMPORT]={0xff,0xff,0xff,0xff,0xff,0xff};

unsigned char RAM_DISP_EXTRES[NDIG_EXTRES];
unsigned char RAMFLASH_DISP_EXTRES[NDIG_EXTRES];
unsigned char RAMPUNTS_DISP_EXTRES[NDIG_EXTRES];
unsigned char RAMPUNTSFLASH_DISP_EXTRES[NDIG_EXTRES]={0xff,0xff,0xff,0xff};

unsigned char RAM_DISP_CARREGADOR[NDIG_CARREGADOR];
unsigned char RAMFLASH_DISP_CARREGADOR[NDIG_CARREGADOR];
//unsigned char RAMPUNTS_DISP_CARREGADOR[NDIG_CARREGADOR];
//unsigned char RAMPUNTSFLASH_DISP_CARREGADOR[NDIG_CARREGADOR]={0xff,0xff};

const struct s_disp DISP[NUM_DISPLAYS]=  // amb l'ordre amb que es defineixin DISP's
{
  {NDIG_LEDEST,RAM_DISP_LEDEST+(NDIG_LEDEST-1),RAMFLASH_DISP_LEDEST+(NDIG_LEDEST-1),
               RAMPUNTS_DISP_LEDEST+(NDIG_LEDEST-1),RAMPUNTSFLASH_DISP_LEDEST+(NDIG_LEDEST-1),DISP_CHIP_RAM,1,1,8},    // DISP_LEDEST
  {NDIG_IMPORT,RAM_DISP_IMPORT+(NDIG_IMPORT-1),RAMFLASH_DISP_IMPORT+(NDIG_IMPORT-1),
               RAMPUNTS_DISP_IMPORT+(NDIG_IMPORT-1),RAMPUNTSFLASH_DISP_IMPORT+(NDIG_IMPORT-1),DISP_CHIP_RAM,1,0,0},    // DISP_IMPORT
  {NDIG_EXTRES,RAM_DISP_EXTRES+(NDIG_EXTRES-1),RAMFLASH_DISP_EXTRES+(NDIG_EXTRES-1),
               RAMPUNTS_DISP_EXTRES+(NDIG_EXTRES-1),RAMPUNTSFLASH_DISP_EXTRES+(NDIG_EXTRES-1),DISP_CHIP_RAM,1,1,0},    // DISP_EXTRES
  {NDIG_CARREGADOR,RAM_DISP_CARREGADOR+(NDIG_CARREGADOR-1),RAMFLASH_DISP_CARREGADOR+(NDIG_CARREGADOR-1),
               (unsigned char*)NULL,(unsigned char*)NULL,                                     DISP_I2C,0,0,0},         // DISP_CARREGADOR
};


char get_nivell_llum(int adc_value)
{
  // pasa de 0 - 0xfff a  0-7
  return((adc_value >> 9) & 0x07);
}



int half_intermitent = 0;

char tab_low[16] = {0x00,0x10,0x01,0x11,0x04,0x14,0x05,0x15,
0x80,0x90,0x81,0x91,0x84,0x94,0x85,0x95};

char tab_high[16] = {0x00,0x40,0x20,0x60,0x02,0x42,0x22,0x62,
0x08,0x48,0x28,0x68,0x0a,0x4a,0x2a,0x6a};

char trans_40to50(char value)
{
  return(tab_low[value&0x0f]|tab_high[value >> 4]);
}


void copy40to50(char* dest,int nDisp,int nb)
{
int i;
unsigned char* org;
unsigned char* org_flash;

  org = (DISP[nDisp].POS_RAM-DISP[nDisp].NDIG)+1;
  org_flash = (DISP[nDisp].POS_RAM_FLASH-DISP[nDisp].NDIG)+1;
  if(half_intermitent)
  {
      for(i=0;i<nb;i++)
         {
           *dest++ = trans_40to50((*org++)& (*org_flash++));
         }
  }
  else
  {
      for(i=0;i<nb;i++)
         {
           *dest++ = trans_40to50(*org++);
         }
  }
}

void textes_to_chip(void)
{
char bufI2c[16];
unsigned short s_aux;
unsigned char c_aux,c_aux1;
  if(tipoTx==TX50)
  {
      display_to_chip(0);
      display_to_chip(1);
  }
  else
  {
      bufI2c[0]=0xC8;
      bufI2c[1]=4;
      s_aux = textes_frontal;
      if(half_intermitent)
         s_aux &= textes_frontal_flash;
//      bufI2c[2]=ram_leds_tx40[12]+(ram_leds_tx40[2] & 0x0f);
      c_aux = *((DISP[1].POS_RAM-DISP[1].NDIG)+1);
      if(half_intermitent)
        c_aux &= *((DISP[1].POS_RAM_FLASH-DISP[1].NDIG)+1);
      c_aux = (trans_40to50(c_aux)>> 4) & 0x0f;
      c_aux1 =  (unsigned char)((s_aux &0x07)<<4);  // T1 T2 T3
      if(s_aux &0x10)c_aux1 |= 0x80;                // T4
      bufI2c[2]=c_aux|c_aux1;
      write_i2c_disp(3, bufI2c);
      
      bufI2c[0]=0xC8;
      bufI2c[1]=24;
      c_aux = *(DISP[2].POS_RAM);
      if(half_intermitent)
        c_aux &= *(DISP[2].POS_RAM_FLASH);
      c_aux = (trans_40to50(c_aux)<<4)  & 0xf0;
      if(s_aux &0x100)c_aux |= 0x02;   // T9
      if(s_aux &0x80)c_aux |= 0x01;    // T10      
      bufI2c[2] = c_aux;
      c_aux = 0;
      if(s_aux &0x40)c_aux |= 0x80;    // T8      
      if(s_aux &0x20)c_aux |= 0x40;    // T7     
      if(s_aux &0x200)c_aux |= 0x08;   // T5     
      if(s_aux &0x08)c_aux |= 0x04;    // T6     
      bufI2c[3] = c_aux;
      write_i2c_disp(4, bufI2c);
  }
}



// per girar bits del display
const unsigned char taula_reverse[16]={0,8,4,0x0c,2,0x0a,6,0x0e,1,9,5,0x0d,3,0x0b,7,0x0f};

// envia ram display a chip display
void display_to_chip(int nDisp)
{
  unsigned char* pos_ram;
  unsigned char c,c_mem,c_punts;
  int nIc,nDig,i,j;
  char bufI2c[16];
  
  nDig = DISP[nDisp].NDIG;
  
  switch(DISP[nDisp].tipus_disp)
  {
  case DISP_CHIP_RAM:
    
  if(tipoTx==TX50)
  {
    nIc = DISP[nDisp].numIc;
    pos_ram = ledDriver_Ram[nIc].buf+DISP[nDisp].posRamIc;
    for(i=0,j=nDig-1; i<nDig; i++,j--)
    {
      c_mem = (unsigned char)*(DISP[nDisp].POS_RAM-j);
      c_punts = (unsigned char)*(DISP[nDisp].POS_RAMPUNTS-j);
      if(half_intermitent)
      {
        c_mem &= (unsigned char)*(DISP[nDisp].POS_RAM_FLASH-j);
        c_punts &= (unsigned char)*(DISP[nDisp].POS_RAMPUNTS_FLASH-j);
      }
      c = c_punts | (taula_reverse[(c_mem >> 4) & 0x0f] <<4);
      
      *pos_ram = c;
      *(++pos_ram) =  taula_reverse[c_mem  & 0x0f];
      pos_ram += 1;
    }
    //ledDriver_dataWrite(nIc,DISP[nDisp].posRamIc-(nDig*2)+2,nDig*2);
    ledDriver_dataWrite(nIc,DISP[nDisp].posRamIc,nDig*2);
    //ledDriver_control(nIc,1,7);  // on, nivell 7
  }
  else
  {
    switch(nDisp)
    {
    case 0: //LedEstado
      bufI2c[0]=0xC8;
      bufI2c[1]=0x00;
//      memcpy(bufI2c+2,ram_leds_tx40,2);
      copy40to50(bufI2c+2,0,2);
      write_i2c_disp(4, bufI2c);
      break;
    case 1:  //Total
      bufI2c[0]=0xC8;
      bufI2c[1]=5;
//      memcpy(bufI2c+2,ram_leds_tx40+2,6);
//      memcpy(bufI2c+2,(DISP[1].POS_RAM-DISP[1].NDIG)+1,6);
      copy40to50(bufI2c+2,1,6);

      write_i2c_disp(8, bufI2c);
      break;
    case 2:  //extras
      bufI2c[0]=0xC8;
      bufI2c[1]=17;
//      memcpy(bufI2c+2,ram_leds_tx40+8,4);
      copy40to50(bufI2c+2,2,4);
      write_i2c_disp(6, bufI2c);
      break;
    }
  }
    break;
  case DISP_I2C:
    write_i2c(DISPLAY_CARG,0,nDig,(char *) DISP[nDisp].POS_RAM-(DISP[nDisp].NDIG - 1));
    break;
  }
}



void memcpy_reversed(char* dest, char* org, int nb)
{
  int i;
  char *p;
  if(nb == 0)return;
  p = org + (nb-1);
  for(i=0;i<nb;i++)
    dest[i] = *p--;
}


// actualitza leds textes frontal
// te en compte les variables 
//      tx30.led_horaria
//      tarcom.lf_HORARIA
// per incloure-hi els leds de horaria
// Si no hi hagues led horaria cal fer tarcom.lf_HORARIA = 0
// i es pot utilitzar el bit com un altre texte


void LEDS_FRONTAL(unsigned short val)
{
  val = (val & tarcom.lf_HORARIA_OFF);
  if( tx30.led_horaria)
    val |= tarcom.lf_HORARIA;
  
  if(tipoTx==TX50)
  {
      if(val & 0x0001)  // led  libre
        *(DISP[1].POS_RAMPUNTS-5) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-5) = 0;
      if(val & 0x0002)  // led  ocupado
        *(DISP[1].POS_RAMPUNTS-4) = 0x0f;  
      else
        *(DISP[1].POS_RAMPUNTS-4) = 0;  
      if(val & 0x0004)  // led  pagar
        *(DISP[1].POS_RAMPUNTS-3) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-3) = 0;
      if(val & 0x0008)  // led horaria
        *(DISP[2].POS_RAMPUNTS-3) = 0x0f;  
      else
        *(DISP[2].POS_RAMPUNTS-3) = 0;  
      if(val & 0x0010)  // texte tarifa
        *(DISP[2].POS_RAMPUNTS-2) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS-2) = 0;
      if(val & 0x0020)  // texte import esquerra
        *(DISP[1].POS_RAMPUNTS-2) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-2) = 0;
     if(val & 0x0040)  // texte import dreta
        *(DISP[1].POS_RAMPUNTS-1) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-1) = 0;
      if(val & 0x0080)  // texte extres dalt
        *(DISP[2].POS_RAMPUNTS-1) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS-1) = 0;
      if(val & 0x0100)  // texte extres baix
        *(DISP[2].POS_RAMPUNTS  ) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS  ) = 0;
      
      // gestio leds horaria i kilometrica Chile
      // 
      if(tarcom.new_leds_ho_km)
      {
        if(serv[viaj].TARIFANDO && tx30.new_led_ho)
          *(DISP[1].POS_RAMPUNTS  ) = 0x08;
        else
          *(DISP[1].POS_RAMPUNTS  ) = 0;
        if(serv[viaj].TARIFANDO && tx30.new_led_km)
          *(DISP[0].POS_RAMPUNTS  ) = 0x08;
        else
          *(DISP[0].POS_RAMPUNTS  ) = 0;
      }
  }
  else
  {
    textes_frontal = val;
  }
 
  textes_to_chip();
}


void LEDS_FRONTAL_FIXE(void)
{
  if(tipoTx==TX50)
  {
    *(DISP[1].POS_RAMPUNTS_FLASH-5) = 
    *(DISP[1].POS_RAMPUNTS_FLASH-4) = 
    *(DISP[1].POS_RAMPUNTS_FLASH-3) = 
    *(DISP[1].POS_RAMPUNTS_FLASH-2) = 
    *(DISP[1].POS_RAMPUNTS_FLASH-1) = 
    *(DISP[2].POS_RAMPUNTS_FLASH-3) = 
    *(DISP[2].POS_RAMPUNTS_FLASH-2) = 
    *(DISP[2].POS_RAMPUNTS_FLASH-1) = 
    *(DISP[2].POS_RAMPUNTS_FLASH  ) = 0xff;
    // posa intermitent per cas hi hagi leds chile
    *(DISP[1].POS_RAMPUNTS_FLASH  ) = 
    *(DISP[0].POS_RAMPUNTS_FLASH  ) = 0;
  }
  else
  {
    textes_frontal_flash = 0xffff;
  }
}

// posa flash els textes amb bit = 1
// posa fixes els textes amb bit = 0
void LEDS_FRONTAL_FLASH(unsigned short val)
{  
  if(tipoTx==TX50)
  {
      if(val & 0x0001)  // led  libre
        *(DISP[1].POS_RAMPUNTS_FLASH-5) = 0;
      else
        *(DISP[1].POS_RAMPUNTS_FLASH-5) = 0x0f;
      if(val & 0x0002)  // led  ocupado
        *(DISP[1].POS_RAMPUNTS_FLASH-4) = 0;  
      else
        *(DISP[1].POS_RAMPUNTS_FLASH-4) = 0x0f;  
      if(val & 0x0004)  // led  pagar
        *(DISP[1].POS_RAMPUNTS_FLASH-3) = 0;
      else
        *(DISP[1].POS_RAMPUNTS_FLASH-3) = 0x0f;
      if(val & 0x0008)  // led horaria
        *(DISP[2].POS_RAMPUNTS_FLASH-3) = 0;  
      else
        *(DISP[2].POS_RAMPUNTS_FLASH-3) = 0x0f;  
      if(val & 0x0010)  // texte tarifa
        *(DISP[2].POS_RAMPUNTS_FLASH-2) = 0;
      else
        *(DISP[2].POS_RAMPUNTS_FLASH-2) = 0x0f;
      if(val & 0x0020)  // texte import esquerra
        *(DISP[1].POS_RAMPUNTS_FLASH-2) = 0;
      else
        *(DISP[1].POS_RAMPUNTS_FLASH-2) = 0x0f;
     if(val & 0x0040)  // texte import dreta
        *(DISP[1].POS_RAMPUNTS_FLASH-1) = 0;
      else
        *(DISP[1].POS_RAMPUNTS_FLASH-1) = 0x0f;
      if(val & 0x0080)  // texte extres dalt
        *(DISP[2].POS_RAMPUNTS_FLASH-1) = 0;
      else
        *(DISP[2].POS_RAMPUNTS_FLASH-1) = 0x0f;
      if(val & 0x0100)  // texte extres baix
        *(DISP[2].POS_RAMPUNTS_FLASH  ) = 0;
      else
        *(DISP[2].POS_RAMPUNTS_FLASH  ) = 0x0f;
  }
  else
  {
    textes_frontal_flash &= ~val;
  }
 
  textes_to_chip();
}


// ojo cas tx40 !!!!!! const int tabinipos[]={0,2,8};
// ojo cas tx40 !!!!!! const int tabnumdig[]={2,6,4};


void display(int nDisp, int nFormat,void *adr_var)
{
unsigned long lvar;
float fvar;
unsigned char  nd,nib;
unsigned char  ndig;
unsigned char  aux[11];
char  auxlong[11];
unsigned char  *  p;
unsigned char  *adr_var_uchar;
struct s_formato  formato_nf;
unsigned char formato_nf_ndig;
int i;
char blk_tipus_rellotge;  // per eliminar blank de decenes minut


struct s_disp     DISP_ndisp;
unsigned char  aux_hora[3];      // per format F_HHHH i F_HHMMSS
unsigned long minuts_to_hh_mm;   // per format F_LONG_60


 if(nDisp==1 && lap_on)
    {
     lap_nf=nFormat;
     memcpy(lap_var,(char*)adr_var,10);  // guarda variable sense formatar
     return;
    }

  memcpy(&formato_nf,&formatos[nFormat],sizeof(struct s_formato));
  formato_nf_ndig=formato_nf.blk_flsh_ndig & 0x07;
  blk_tipus_rellotge = 0;
  
  memcpy(&DISP_ndisp,&DISP[nDisp],sizeof(struct s_disp));
  
  if(nDisp==0)
    {
     if((nFormat==NF_LEDTAR_INTERM) && (tarcom.ledtar_interm ||((tx30.estat == E_OCUPADO) && servcom.es_control)))
       {
           ledtar_interm=1;
           cnt_ledtar_interm=2;
           memcpy(buf_ledtar_interm,(char*)adr_var,4);
       }   
     else     
       {
        ledtar_interm=0;
        cnt_ledtar_interm=0;
       }
    }
  
 switch(formato_nf.tipo)
    {
     case F_HHHH:          // format hora i centessimes d�hora
        lvar = *(unsigned long*)adr_var;
        aux_hora[0] = bin_bcd((lvar%3600)/36);        
        aux_hora[1]= bin_bcd(lvar/3600);
        adr_var=&aux_hora;
        formato_nf.tipo=F_BCD;
        break;
    case F_LONG_60:
        minuts_to_hh_mm = *(unsigned long*)adr_var;
        minuts_to_hh_mm = (minuts_to_hh_mm/60)*100 + (minuts_to_hh_mm % 60);
        formato_nf.tipo=F_LONG;
        adr_var=&minuts_to_hh_mm;
        blk_tipus_rellotge = 1;
        break;
     case F_HMS:          // selecciona HH-MM o MM-SS  a partir de long (segons)
        lvar = *(unsigned long*)adr_var;
        if(lvar >= 3600)
          lvar /= 60;
        aux_hora[0] = bin_bcd(lvar %60);
        aux_hora[1] = bin_bcd(lvar/60);
        adr_var=&aux_hora;
        formato_nf.tipo=F_BCD;
        break;      
     case F_HHMMSS:          // HH.MM.SS  a partir de long (segons)
        lvar = *(unsigned long*)adr_var;
        aux_hora[0] = bin_bcd(lvar %60);
        aux_hora[1] = bin_bcd((lvar/60)%60);
        aux_hora[2] = bin_bcd(lvar/3600);
        adr_var=&aux_hora;
        formato_nf.tipo=F_BCD;
        break;      
     default:
        break;
    }

 adr_var_uchar=(unsigned char*)adr_var;  // per formats F_HMS y F_BCD td-tx       
 switch(formato_nf.tipo)
    {

     case F_BCD:
        for(nd=formato_nf_ndig;nd<DISP_ndisp.NDIG;nd++)
           aux[nd]=0;
        for(nd=0;nd<formato_nf_ndig;nd++)
           {
            nib=*adr_var_uchar;       // td-tx
            if(nd & 0x01)
               {
                nib>>=4;
                adr_var_uchar++;      // td-tx
               }
            aux[nd]=SS[nib & 0x0f];
           }
        nib=formato_nf.dec;
        if(nib)  /* cas decimals */
          {
           if((nib & 0x80)==0)
             {
               if(nib==1)
              nib=tarcom.NDEC;
                else
                  nib -= 1;
              aux[nib] |= SS_DP;
             }
           else
             {
              for(nib&=0x7f,nd=0;nib;nib>>=1,nd++)
                 if(nib&0x01)
                     aux[nd] |= SS_DP;
             }
          }
        if(formato_nf.blk_flsh_ndig & 0x80)  /* cas blanking */
           for(nd=formato_nf_ndig-1;(nd>0) && (aux[nd]==SS_0);nd--)
              aux[nd]=0;
        p=DISP_ndisp.POS_RAM;
        for(nd=0,p=DISP_ndisp.POS_RAM;nd<DISP_ndisp.NDIG;nd++,p--)
           *p=aux[nd];
        break;
     case F_FLOAT:
     case F_LONG:
     case F_LONG_NC:
        nib=formato_nf.dec;        
        if(nib)  /* cas decimals */
          {                        
           if(nib==8)
             nib=tarcom.NDEC_ALT;   // alternate decs per tarifa
           else
             if(nib == 1)
             nib=tarcom.NDEC;
           else
             nib -=1;
          }
        if(formato_nf.tipo==F_FLOAT)
			{
            //lvar=(unsigned long)(*(float*)adr_var*FKDEC[nib]+.5);
			memcpy((char*)&fvar,(char*)adr_var,sizeof(fvar));		 // td-tx  per address impar
			lvar=(unsigned long)(fvar*FKDEC[nib]+.5);
			}
        else
			{
            //lvar=*(long*)adr_var;
			memcpy((char*)&lvar,(char*)adr_var,sizeof(lvar));		 // td-tx  per address impar
			}
        ltobcd(lvar,auxlong);
        for(nd=0;nd<formato_nf_ndig;nd++)
           {
            switch(DISP_ndisp.tipus_disp)
            {
            case DISP_CHIP_RAM:
              aux[nd]=SS[auxlong[nd]];  // set segments
              break;
            case DISP_I2C:
              aux[nd]=auxlong[nd]|0x30; // ho passa a ASCII
              break;
            }             
           }
        if(nib)                /* decimals */
           {
            switch(DISP_ndisp.tipus_disp)
            {
            case DISP_CHIP_RAM:
              aux[nib] |= SS_DP;
              break;
            case DISP_I2C:
                // no fa res
              break;
            }             
           }
        if(formato_nf.blk_flsh_ndig & 0x80)  /* cas blanking */
        {
            switch(DISP_ndisp.tipus_disp)
            {
            case DISP_CHIP_RAM:
              for(nd=formato_nf_ndig-1;(nd>0) && (aux[nd]==SS_0);nd--)
                  aux[nd]=0;
              if(blk_tipus_rellotge &&(aux[3] == 0))
                 aux[3] = SS_0; // elimina blank de decenes de minut
              break;
            case DISP_I2C:
              for(nd=formato_nf_ndig-1;(nd>0) && (aux[nd]== '0');nd--)
                  aux[nd] = ' ';
              if(blk_tipus_rellotge &&(aux[3] == ' '))
                 aux[3] = '0'; // elimina blank de decenes de minut
              break;
            }             
        }
        p=DISP_ndisp.POS_RAM;
        ndig=(unsigned char)min(DISP_ndisp.NDIG,formato_nf_ndig);
        for(nd=0,p=DISP_ndisp.POS_RAM;nd<ndig;nd++,p--)
           *p=aux[nd];
        switch(formato_nf.tipo)
           {
            case F_LONG_NC:
              break;
            default:
              for(;nd<DISP_ndisp.NDIG;nd++)
              {
                switch(DISP_ndisp.tipus_disp)
                {
                case DISP_CHIP_RAM:
                     *p--=0;
                  break;
                case DISP_I2C:
                     *p--= ' ';
                  break;
                }             
              }
            break;
           }
        break;
     case F_SS_JI:
        nd=(unsigned char)min(DISP_ndisp.NDIG,formato_nf_ndig);
        memcpy((DISP_ndisp.POS_RAM-DISP_ndisp.NDIG)+1,(unsigned char *)adr_var,nd);
        break;
     case F_SS_JD_C:
        nd=(unsigned char)min(DISP_ndisp.NDIG,formato_nf_ndig);
        if(DISP_ndisp.NDIG>nd)
          memset((DISP_ndisp.POS_RAM-DISP_ndisp.NDIG)+1,0,DISP_ndisp.NDIG-nd);
        memcpy((DISP_ndisp.POS_RAM-nd)+1,(unsigned char *)adr_var+(formato_nf_ndig-nd),nd);
        break;
     case F_HORA:
        p=(unsigned char *)adr_var;
        nib=*p;nib &= 0xf;
        *DISP_ndisp.POS_RAM=SS[nib];
        nib=*p;nib &= 0xf0;nib>>=4;p++;
        *(DISP_ndisp.POS_RAM-1)=SS[nib];
        nib=*p;nib &= 0xf;
        *(DISP_ndisp.POS_RAM-3)=SS[nib];
        nib=*p;nib &= 0xf0;nib>>=4;
        *(DISP_ndisp.POS_RAM-4)=SS[nib];
        *(DISP_ndisp.POS_RAM-2)=SS_GUION;
        for(nd=5;nd<DISP_ndisp.NDIG;nd++)
           *(DISP_ndisp.POS_RAM-nd)=0;
        break;
     case F_DIA:
        p=(unsigned char *)adr_var;
        if(FORMATO_FECHA!=FF_DDMM)
          p++;
        nib=*p;nib &= 0xf;
        *(DISP_ndisp.POS_RAM-3)=SS[nib];
        nib=*p;nib &= 0xf0;nib>>=4;
        *(DISP_ndisp.POS_RAM-4)=SS[nib];
        if(FORMATO_FECHA!=FF_DDMM)
          p--; 
        else  
          p++;
        nib=*p;nib &= 0xf;
        *(DISP_ndisp.POS_RAM)=SS[nib];
        nib=*p;nib &= 0xf0;nib>>=4;
        *(DISP_ndisp.POS_RAM-1)=SS[nib];
#if 0        
        if(FORMATO_FECHA==FF_DDMM)
          {
           nib=*p;nib &= 0xf;
           *(DISP_ndisp.POS_RAM-3)=SS[nib];
           nib=*p;nib &= 0xf0;nib>>=4;p++;
           *(DISP_ndisp.POS_RAM-4)=SS[nib];
           nib=*p;nib &= 0xf;
           *(DISP_ndisp.POS_RAM)=SS[nib];
           nib=*p;nib &= 0xf0;nib>>=4;
           *(DISP_ndisp.POS_RAM-1)=SS[nib];
          }
        else
          {
           nib=*p;nib &= 0xf;
           *DISP_ndisp.POS_RAM=SS[nib];
           nib=*p;nib &= 0xf0;nib>>=4;p++;
           *(DISP_ndisp.POS_RAM-1)=SS[nib];
           nib=*p;nib &= 0xf;
           *(DISP_ndisp.POS_RAM-3)=SS[nib];
           nib=*p;nib &= 0xf0;nib>>=4;
           *(DISP_ndisp.POS_RAM-4)=SS[nib];
          }
#endif          
        *(DISP_ndisp.POS_RAM-2)=SS_GUION;
        for(nd=5;nd<DISP_ndisp.NDIG;nd++)
           *(DISP_ndisp.POS_RAM-nd)=0;
        break;
     case F_ANY:
        p=(unsigned char *)adr_var;
        nib=*p;nib &= 0xf;
        *DISP_ndisp.POS_RAM=SS[nib];
        nib=*p;nib &= 0xf0;nib>>=4;
        *(DISP_ndisp.POS_RAM-1)=SS[nib];
        if(nib==9)
          {
           *(DISP_ndisp.POS_RAM-2)=SS[9];
           *(DISP_ndisp.POS_RAM-3)=SS[1];
          }
        else
          {
           *(DISP_ndisp.POS_RAM-2)=SS[0];
           *(DISP_ndisp.POS_RAM-3)=SS[2];
          }
        *(DISP_ndisp.POS_RAM-4)=SS_GUION;
        for(nd=5;nd<DISP_ndisp.NDIG;nd++)
           *(DISP_ndisp.POS_RAM-nd)=0;
        break;
	case F_MODIF_IVA:
        *(DISP_ndisp.POS_RAM-3)=SS[((*(unsigned long *)adr_var)/10)%10];
        *(DISP_ndisp.POS_RAM-2)=SS[(*(unsigned long *)adr_var)%10];
		*(DISP_ndisp.POS_RAM-1)= 0x63; // o
		*(DISP_ndisp.POS_RAM)= 0x5c;   // o
		break;
     default:
        break;
    }
 if(formato_nf.blk_flsh_ndig & 0x40)
   {
    // posar  fixe  
    memset((DISP_ndisp.POS_RAM_FLASH-DISP_ndisp.NDIG) +1,0xff,DISP_ndisp.NDIG);
   }     
 if(formato_nf.blk_flsh_ndig & 0x20)
   {
    // posar flash */
    if(formato_nf.maskflsh==NULL)
       memset((DISP_ndisp.POS_RAM_FLASH-DISP_ndisp.NDIG) +1,0x00,DISP_ndisp.NDIG);
    else
      {
       memcpy((DISP_ndisp.POS_RAM_FLASH-formato_nf_ndig) +1,formato_nf.maskflsh,formato_nf_ndig);
      }
   }
 if(formato_nf.blk_flsh_ndig & 0x10)
   {
    // posar flash */
    if(formato_nf.maskflsh==NULL)
       memset((DISP_ndisp.POS_RAM_FLASH-DISP_ndisp.NDIG) +1,0x00,DISP_ndisp.NDIG);
    else
      {
        for(i=0;i<formato_nf_ndig;i++)
          *((DISP_ndisp.POS_RAM_FLASH-formato_nf_ndig) +1 +i) &= *(formato_nf.maskflsh+i);
      }
   }

 
 if((nDisp == 0) && tx30.lluminos_apagat)
   *DISP_ndisp.POS_RAM |= 0x80;
 
  display_to_chip(nDisp);
}


#define DISPLAY_INTERMITENT_TICKS_TIMER 500 // 500ms


OS_TIMER displayIntermitent_Timer;
#define TICK_INTERM_DISPLAY   0x02     // event

void displayIntermitent_interrupt_timer(void)
{
   OS_SignalEvent(TICK_INTERM_DISPLAY, &TCB_display);
   OS_RetriggerTimer(&displayIntermitent_Timer);

}

// es crida nomes desde task display
// per actualitzar led_horaria
// actualitza els bits de mask horaria segons led on/off
void led_horaria_a_ramdisp(unsigned short mask, char on)
{
  if(tipoTx==TX50)
  {
    if(mask & 0x0001)   // led  libre
      if(on )  
        *(DISP[1].POS_RAMPUNTS-5) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-5) = 0;
    if(mask & 0x0002)  // led  ocupado
      if(on )  
        *(DISP[1].POS_RAMPUNTS-4) = 0x0f;  
      else
        *(DISP[1].POS_RAMPUNTS-4) = 0;  
    if(mask & 0x0004)  // led  pagar
      if(on )  
        *(DISP[1].POS_RAMPUNTS-3) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-3) = 0;
    if(mask & 0x0008)  // led horaria
      if(on )  
        *(DISP[2].POS_RAMPUNTS-3) = 0x0f;  
      else
        *(DISP[2].POS_RAMPUNTS-3) = 0;  
    if(mask & 0x0010)  // texte tarifa
      if(on )  
        *(DISP[2].POS_RAMPUNTS-2) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS-2) = 0;
    if(mask & 0x0020)  // texte import esquerra
      if(on )  
        *(DISP[1].POS_RAMPUNTS-2) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-2) = 0;
    if(mask & 0x0040)  // texte import dreta
      if(on )  
        *(DISP[1].POS_RAMPUNTS-1) = 0x0f;
      else
        *(DISP[1].POS_RAMPUNTS-1) = 0;
    if(mask & 0x0080)  // texte extres dalt
      if(on )  
        *(DISP[2].POS_RAMPUNTS-1) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS-1) = 0;
    if(mask & 0x0100)  // texte extres baix
      if(on )  
        *(DISP[2].POS_RAMPUNTS  ) = 0x0f;
      else
        *(DISP[2].POS_RAMPUNTS  ) = 0;
      // gestio leds horaria i kilometrica Chile
      // 
      if(tarcom.new_leds_ho_km)
      {
        if(serv[viaj].TARIFANDO && tx30.new_led_ho)
          *(DISP[1].POS_RAMPUNTS  ) = 0x08;
        else
          *(DISP[1].POS_RAMPUNTS  ) = 0;
        if(serv[viaj].TARIFANDO && tx30.new_led_km)
          *(DISP[0].POS_RAMPUNTS  ) = 0x08;
        else
          *(DISP[0].POS_RAMPUNTS  ) = 0;
      }
  }
  else
  {
    if(on)
      textes_frontal |= mask;
    else
      textes_frontal &= ~mask;
  }
 
}

char nivell_llum;    // valor de 0 a 7 segons foto diode
int LUZFRONT;

const char taula_refr_luz[] =
{
  0x01,    // nivell 0
  0x11,
  0x45,
  0x55,
  0xd5,
  0x77,
  0x7f,
  0xff,     // nivell 7
};


int REFRESH = 1;    // per opcio Grecia 

// actualitza lluminositat llum pulsadors frontal
// es crida cada ms. desde timer 1ms
void refresh_luzfront(void)
{
static char mask = 0x01;
  if(REFRESH)
  {
      if(LUZFRONT == 1)
      {
        if(mask == 0)
          mask = 0x01;
        if(taula_refr_luz[nivell_llum] & mask)
            IOPORT1_PD |= 0x100;
        else
            IOPORT1_PD &= 0xFeff;  // apaga llum
        mask <<=1;
      }
  }
}

/****************************************************/
/* possa var. luzfront                              */
/****************************************************/
void out_luzfront(unsigned char val)
{
      if(tipoTx == TX50)
      {    
         if(val )  // versio 1.04
           {
             LUZFRONT = 1;
             // ja refresca llum refresh_luzfront();
           }
         else
           {
             LUZFRONT = 0;
             IOPORT1_PD &= 0xFeff;  // apaga llum
           }
      }
      else
      {
         if(val)  // versio 1.04
           {
              LUZFRONT = 1;
              PORT6 |= 0x02;            
           }
         else
           {
             LUZFRONT = 0;
             PORT6 &= 0xfd;
           }
         write_i2c(IO_EXPANDER_TX40,0x12,1,(char*)&PORT6);      
      }
}


void REFRESH_ON(void)
{
  REFRESH = 1;
}

void REFRESH_OFF(void)
{
  REFRESH = 0;
  if(tipoTx == TX50)
  {    
      // apaga els tres displays i luzfront
      ledDriver_control(0, 0, 0);
      ledDriver_control(1, 0, 0);
      IOPORT1_PD &= 0xFeff;  // apaga llum
  }
  else
  {
   // apaga backlight 
   //IOPORT1_PD_bit.no6 = 0;    
    TIM1_CR1_bit.OLVLB = 0;
    TIM1_CR1_bit.FOLVB = 1;
    TIM1_CR1_bit.FOLVB = 0;
  }
}

//
// inicialitza refresh backlight ( cas TX40)
//
void ini_refresh_backlight(void)
{
  if(tipoTx==TX40)
  {
    TIM1_CR1_bit.EN = 1;
    TIM1_CR2_bit.OCBIE = 1;   // activa interrupcio    
  }
}
//
// gestio backlight segons fotodiode  
// es crida desde interrupcio Timer 1
//

#define FACTOR_REFR_DISPLAY 0x1000
//
// es crida desde interrupcio Timer 1
// 
CODE_FAST void refresh_backlight(void)
{
static char displayHalf = 0;
static int nivell_llum;

  if(REFRESH && (tipoTx == TX40))
    {      
      if(displayHalf == 0)
      {
        nivell_llum = interrupt_nivell_llum;        
        TIM1_OCBR = TIM1_CNTR + (FACTOR_REFR_DISPLAY * nivell_llum);  // per comen�ar inmediatament
        TIM1_CR1_bit.OLVLB = 0;
        TIM1_CR1_bit.OCBE = 1; 
        displayHalf = 1;
      }
      else
      {
        TIM1_OCBR = TIM1_CNTR + (FACTOR_REFR_DISPLAY * (7-nivell_llum)) + 0x100;  // el 0x100 per que no sigui 0 cas llum ==7
        TIM1_CR1_bit.OLVLB = 1;
        TIM1_CR1_bit.OCBE = 1;
        displayHalf = 0;
      }
    }
}

static void Task_display(void)
{
char events;
int nDisp;

    fotoDiode_init();
    ledDriver_init(0);
    ledDriver_init(1);
    ini_refresh_backlight();
    OS_CreateTimer (&displayIntermitent_Timer, displayIntermitent_interrupt_timer,DISPLAY_INTERMITENT_TICKS_TIMER);
    OS_StartTimer(&displayIntermitent_Timer);
    
    while(1)
    {
    
       events = OS_WaitEvent(FOTODIODE_GOT | TICK_INTERM_DISPLAY);
       
       
       if(REFRESH)
       {
           if(events & FOTODIODE_GOT)
           {
           // llegit fotodiode
           nivell_llum = get_nivell_llum(fotoDiode_getValor());
           if(nivell_llum < tx30.nivell_llum_minim)
              nivell_llum = tx30.nivell_llum_minim;
           
           if(tipoTx==TX50)
           {
               ledDriver_control(0,1,nivell_llum);  // on, nivell 
               ledDriver_control(1,1,nivell_llum);  // on, nivell 
           }
           else
           {
             // es fa dins del tick 1ms.
           }
           }
           
           
           
           if(events & TICK_INTERM_DISPLAY)
           {
             led_horaria_a_ramdisp(tarcom.lf_HORARIA,tx30.led_horaria);
             for(nDisp = 0; nDisp< NUM_DISPLAYS; nDisp++)
             {
              if(DISP[nDisp].gestiona_flash) 
                display_to_chip(nDisp);    
             }
             
              if(tipoTx==TX40)
              {
                // cas TX50 display_to_chip ja gestiona textes
                textes_to_chip();
              }
             
            if(half_intermitent)
             half_intermitent = 0; // apaga
            else
             half_intermitent = 1; // encen
           }
       } 
    }
}


void display_init(void)
{
  OS_CREATETASK(&TCB_display, " Task_display", Task_display, 150, Stack_display);
}





//***************************************************************************
// barra progres
//***************************************************************************

static int barra_progres_cnt;
static int barra_progres_compare;
static int barra_progres_cols_filled;

static unsigned char barra_progres_buf_disp[6];

void barra_progres_init(int max_value)
{
  barra_progres_compare = max_value/12;
  barra_progres_cnt = 1;
  barra_progres_cols_filled = 0;
  memset(barra_progres_buf_disp,0,sizeof barra_progres_buf_disp);
}

void barra_progres(void)
{
  int nd;
  if(--barra_progres_cnt)
    return;
  barra_progres_cnt = barra_progres_compare;  // reinicia contador
  nd = barra_progres_cols_filled/2;
  barra_progres_buf_disp[nd] = (barra_progres_cols_filled % 2) ? 0x36 : 0x6;
  display(DISP_IMPORT,0,barra_progres_buf_disp);
  if(barra_progres_cols_filled < 11)
    barra_progres_cols_filled++;
  
}

// !!!! tx50

void APAGAR_DISPLAY(unsigned char nDisp)
{
  DISP_FILL(nDisp,0);
}

void DISP_FILL(unsigned char nDisp,unsigned char value)
{
unsigned char i;
unsigned char * j;
 i=DISP[nDisp].NDIG; j=DISP[nDisp].POS_RAM;
 memset((j-i)+1,value,i);
 // ojo cas tx40 !!!!!! memset(ram_leds_tx40+tabinipos[nDisp],value,tabnumdig[nDisp]);
 display_to_chip(nDisp);
}

void DISP_DIG_SET(unsigned char ndisp,unsigned char ndig, unsigned char value)
{
 *(DISP[ndisp].POS_RAM -ndig)= value;
// ojo cas tx40 !!!!!! ram_leds_tx40[tabinipos[ndisp]+ndig]=value;
 display_to_chip(ndisp);
}

void DISP_FIXE(unsigned char nDisp)
{
unsigned char i;
unsigned char * j;
 i=DISP[nDisp].NDIG; j=DISP[nDisp].POS_RAM_FLASH;
 memset((j-i)+1,0xff,i);
}

void DISP_FLASH(unsigned char nDisp)
{
 memset((DISP[nDisp].POS_RAM_FLASH-DISP[nDisp].NDIG)+1,0x00,DISP[nDisp].NDIG);
}

void DISP_DIG_FIXE(unsigned char ndisp,unsigned char ndig)
{
 *(DISP[ndisp].POS_RAM_FLASH -ndig)=0xff;
}

void DISP_DIG_FLASH(unsigned char ndisp,unsigned char ndig)
{
 *(DISP[ndisp].POS_RAM_FLASH -ndig)=0x00;
}

void DISPLAY_VD1(unsigned char num)
{
 display(1,visu_disp1[num>>4].nform,*(visu_disp1[num>>4].p));
}

void DISPLAY_VD2(unsigned char num)
{
 display(2,visu_disp2[num].nform,*(visu_disp2[num].p));
}


void DISPLAY_SAVE(unsigned char ndisp,char * buf)
{
  memcpy(buf,DISP[ndisp].POS_RAM - DISP[ndisp].NDIG +1,DISP[ndisp].NDIG);
}

void DISPLAY_RESTORE(unsigned char ndisp,char * buf)
{
  memcpy(DISP[ndisp].POS_RAM - DISP[ndisp].NDIG +1,buf,DISP[ndisp].NDIG);
 display_to_chip(ndisp);
}

void DISPLAY_SAVE_FLASH(unsigned char ndisp,char * buf)
{
  memcpy(buf,DISP[ndisp].POS_RAM_FLASH - DISP[ndisp].NDIG +1,DISP[ndisp].NDIG);
}

void DISPLAY_RESTORE_FLASH(unsigned char ndisp,char * buf)
{
  memcpy(DISP[ndisp].POS_RAM_FLASH - DISP[ndisp].NDIG +1,buf,DISP[ndisp].NDIG);
 display_to_chip(ndisp);
}

// *************************************************************************
//  lap
// *************************************************************************

__no_init unsigned char  lap_on;
__no_init unsigned char  lap_nf;        //  guarda crida a disp cas lap_on 
__no_init char  lap_var[10];

void RESET_LAPSUS(void)   // Per que es pugui escriure a Display
{
  lap_on=0;
}


void INI_LAPSUS(void)
{
  if(tarcom.OP_LAP)
  {
    lap_on=1;
    lap_nf=255;
  }
}

void FIN_LAPSUS(void)
{
 if(lap_on==1)
  {
   lap_on=0;
   if(lap_nf!=255)
      display(1,lap_nf,lap_var);
  }
}


void DISPLAY_ERROR_PLUS_LAP(int error)
{
    if(lap_on == 0)
    {
      DISPLAY_SAVE(1,lap_var);
      lap_nf=NF_TEXT6; 
    }
    lap_on = 0;
    display(1,NF_TEXT6,&tarcom.text_error[error][0]);
    lap_on = 1;
}

