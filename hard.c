//*******************************************************************
// hard.c
//*******************************************************************
#include "iostr710.h"

#include "rtos.h"
#include "i2c.h"
#include "i2carm.h"
#include "hard.h"


int tipoTx;


void hard_detect(void)
{
  char st;
  //IOPORT1_PD |= 0x0200; //Conecta las fuentes auxiliares
  while(wread_i2c(IO_EXPANDER_TX40, 2, 1, (char*)&st)){
	I2C_Init();
	I2C0_Config();
  }
  tipoTx=(st==0xff) ? TX50 : TX40;
  
  //tipoTx=(status_i2c(DRIVER_DISPLAY)==0) ? TX40 : TX50;

}


void hard_init(void)
{
  if(tipoTx==TX50)
  {
    PORT3 = 0x40;IOPORT_3 = PORT3;
    PORT4 = 0x80;IOPORT_4 = PORT4;
    IOPORT_5 = 0x0c;
    PORT6 = 0x0C;IOPORT_6 = PORT6;
  }
  else
  {
    IOPORT0_PC0 = 0x3a3f;   //init_port0
    IOPORT0_PC1 = 0xcfc0;
    IOPORT0_PC2 = 0x3a3f; 
    IOPORT0_PD = 0x40ff;

    IOPORT1_PC0 = 0x97f4;   //init_port1
//fbp    IOPORT1_PC1 = 0x7988;
    IOPORT1_PC1 = 0x79c8;
    IOPORT1_PC2 = 0xf6f4;
    IOPORT1_PD = 0x0240;    

    IOPORT2_PC0 = 0x00ff;   //init_port2
    IOPORT2_PC1 = 0xff33;
    IOPORT2_PC2 = 0x00ff;
    IOPORT2_PD = 0x00c0;    

    write_i2c(IO_EXPANDER_TX40,0,2,"\x01\x00"); //IODIR
    write_i2c(IO_EXPANDER_TX40,0x12,2,"\x0a\x00"); //IODIR
  }
}

int fallo_tension(void)
{
  if(tipoTx == TX50)
    return(IOPORT1_PD_bit.no6 == 1);
  else
  {
    IOPORT1_PD_bit.no8 = 0;
    return(IOPORT1_PD_bit.no8 == 1);
  }
}

void salta_a_reset(void)
{
        // treu interrupcions
        OS_DI();            // Disable interrupts  
        OS_DI();            // Disable interrupts

  EIC_ICR = 0x00;   // Disable IRQ  FIQ
  EIC_IER0 = 0x00000000;     // Reset pending ints
  EIC_IPR0 = 0xffffffff;     // Reset pending ints
        
      XTI_MRH = 0;     // desactiva External Ints              
      XTI_MRL = 0;                   
      XTI_PRH = 0;     // clear pending interrupts
      XTI_PRL = 0;

 
         // salta a BOOTstrap a traves de vector 0
         __asm ("  ldr     PC,JMP_RESET      \n"
               "JMP_RESET:     DCD   0x60000000");
}
