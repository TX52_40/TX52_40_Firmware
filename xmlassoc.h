// xmlassoc.h

#ifndef  __XML_ASSOC
#define  __XML_ASSOC

//#include "xmlparse.h"
typedef void *XML_Parser;

// **********************************************
// **********************************************

#define XML_MAX_LEN_NOM 25// llargada maxima nom camp  25 caracters
#define XML_MAX_LEVELS  15// maxim nivells xml  15

enum element_type
{
  TXML_NONE = 0,
  TXML_STRUCT,
  TXML_VECTOR,
  TXML_CHAR,
  TXML_SHORT,
//  TXML_INT,
  TXML_LONG,
  TXML_FLOAT,
  TXML_STRING,
};


/*
typedef void(*T_funcio_start)(void *);
typedef void(*T_funcio_end)(void);
typedef void(*T_funcio_char)(void);
*/
typedef struct
{
  char *nomCurt;
  char *nom;
  int nivell;
  char assignable;
  enum element_type tipus;
  int repe;
  int element_offset;
  int element_size;
}XML_DefElement;


typedef void *XML_Assoc;

extern int XML_Associar(char* buff_arxiu, int len_arxiu, XML_DefElement const* taula, char* pdesti);

#endif

