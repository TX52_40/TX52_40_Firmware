//********************************************************
// format.c     formatos y conversiones
//********************************************************


#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "regs.h"
#include "aplic.h"
#include "format.h"
#include "rutines.h"
#include "rut30.h"

#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#define __min(a,b)  (((a) < (b)) ? (a) : (b))

int GLOBAL_ES0;


const unsigned long KDEC[7]={1L,10L,100L,1000L,10000L,100000L,1000000L};

#define SEP_P2	'='   //0x3d

//*********************************
// format enter amb decimals
//*********************************
void itoa_dp(unsigned int ival,char* buf,int nc,int ndec,char sep)
{
char buf_tmp[15];
char buf_form[15];
int i1,i2,len;
    if(ndec>6)
        ndec=0;
	if(ndec)
	{                                              
		i1=(int)(((unsigned long)(ival))/KDEC[ndec]);
		i2=(int)(((unsigned long)(ival))%KDEC[ndec]);
		sprintf(buf_form,"%%%uu%c%%0%uu",nc-ndec-1,sep,ndec);
		sprintf(buf_tmp,buf_form,i1,i2);
	}
	else
	{
		sprintf(buf_form,"%%%uu",nc);
		sprintf(buf_tmp,buf_form,ival);
	}  
	len=strlen(buf_tmp);
	if(len>nc)
		strcpy(buf, buf_tmp+len-nc); 
	else 
	{
		if(len<nc)
		{
			memmove(buf_tmp+nc-len, buf_tmp, len+1);
			memset(buf_tmp, ' ', nc-len);
		}
		strcpy(buf, buf_tmp); 
	}
}                 

void itoa_dp_blk0(unsigned int ival,char* buf,int nc,int ndec,char sep)
{
char buf_tmp[15];
char buf_form[15];
int i1,i2;
    if(ndec>6)
        ndec=0;
    if(ndec)
    {                                              
        i1=(int)(((unsigned long)(ival))/KDEC[ndec]);
        i2=(int)(((unsigned long)(ival))%KDEC[ndec]);
        sprintf(buf_form,"%%0%uu%c%%0%uu",nc-ndec-1,sep,ndec);
        sprintf(buf_tmp,buf_form,i1,i2);
    }
    else
    {
        sprintf(buf_form,"%%0%uu",nc);
        sprintf(buf_tmp,buf_form,ival);
    }  
	if(strlen(buf_tmp)>nc)
		strcpy(buf, buf_tmp+strlen(buf_tmp)-nc); 
	else
		strcpy(buf, buf_tmp); 
}                 
/*
void itoach_dp_blk0(int ival,char* buf,int nc)
{
char buf_tmp[15];
char buf_form[15];
    sprintf(buf_form,"%%0%dd",nc);
    sprintf(buf_tmp,buf_form,ival);
    strcpy(buf, buf_tmp+strlen(buf_tmp)-nc); 
} 
*/                
//*****************************************
// format bcd a asc amb decimals
//*****************************************
void bcdtoa_dp(char* org,char* buf,int nc,int ndec,char sep,int nb)
{
int i;
int i1;
 i1=nb*2;
 if(ndec)i1++;
 if(nc>i1)
   {
    nc-=i1;
    memset(buf,' ',nc);
    buf+=nc;
   }
 for(i=0;i<nb*2-ndec;i++)
   {
    if((i%2)==0)
       *buf++=((*org >>4)&0x0f)|'0';
    else
       *buf++=((*org++)&0x0f)|'0';
   }
 if(ndec)
   {
    *buf++=sep;
    for(;i<nb*2;i++)
      {
       if((i%2)==0)
          *buf++=((*org >>4)&0x0f)|'0';
       else
          *buf++=((*org++)&0x0f)|'0';
      }
   }
 *buf=0;
}                 

//*********************************
// format long amb decimals
//*********************************
void ltoa_dp(unsigned long lval,char* buf,int nc,int ndec,char sep)
{
char buf_form[15];
char buf_tmp[15];
unsigned long l1,l2;
int len;
    if(ndec>6)
        ndec=0;
    if(ndec)
    {                                              
        l1=lval/KDEC[ndec];
        l2=lval%KDEC[ndec];
        sprintf(buf_form,"%%%ulu%c%%0%ulu",nc-ndec-1,sep,ndec);
        sprintf(buf_tmp,buf_form,l1,l2);
    }
    else
    {
        sprintf(buf_form,"%%%ulu",nc);
        sprintf(buf_tmp,buf_form,lval);
    }  
	len=strlen(buf_tmp);
	if(len>nc)
		strcpy(buf, buf_tmp+len-nc); 
	else 
	{
		if(len<nc)
		{
			memmove(buf_tmp+nc-len, buf_tmp, len+1);
			memset(buf_tmp, ' ', nc-len);
		}
		strcpy(buf, buf_tmp); 
	}
} 

void ltoa_dp_blk0(unsigned long lval,char* buf,int nc,int ndec,char sep)
{
char buf_form[15];
char buf_tmp[15];
unsigned long l1,l2;
    if(ndec>6)
        ndec=0;
    if(ndec)
    {                                               
        l1=lval/KDEC[ndec];
        l2=lval%KDEC[ndec];
        sprintf(buf_form,"%%0%ulu%c%%0%ulu",nc-ndec-1,sep,ndec);
        sprintf(buf_tmp, buf_form, l1, l2);
    }
    else
    {
        sprintf(buf_form,"%%0%ulu",nc);
        sprintf(buf_tmp, buf_form, lval);
    }  
	if(strlen(buf_tmp)>nc)
		strcpy(buf, buf_tmp+strlen(buf_tmp)-nc); 
	else
		strcpy(buf, buf_tmp); 
}                 
/*
void ltoach_dp_blk0(unsigned long lval,char* buf,int nc)
{
char buf_form[15];
char buf_tmp[15];
    sprintf(buf_form,"%%0%dlX",nc);
    sprintf(buf_tmp, buf_form, lval);
    strcpy(buf, buf_tmp+strlen(buf_tmp)-nc); 
}                 
*/

int assignar_cursors(char* buf, char* buftsys, char prefix)
{
int nc;
	nc=strlen(buf);
        if(strlen(buftsys))
        {
          if(prefix)
                memcpy(buf,buftsys, strlen(buftsys));  // texte moneda/km abans del valor
		  else{ 
                strcpy(buf+nc,buftsys);  // texte despres del valor variable
          		nc+=strlen(buftsys);
		  }
        }
	return(nc);
} 

__no_init static int diames_mesdia;

void format_configurar(int diames, int tipoImpresora)
{
  diames_mesdia = !diames;
}

#define LEN_ABRV	5

char tabFontsOptronicW[8]={0,0,1,1,2,2,3,3};
char tabFontsOptronicH[8]={1,1,1,1,2,2,3,3};
char tabFontsKashino[8]={1,1,0,0,0x21,0x20,0x31,0x30};
char tabFontsIr32[8]={'D','C','B','A','E','F','G','H'};
float tabPot[7]={1.,10.,100.,1000.,10000.,100000.,1000000.};
int formatear(int format,int ndig,T_VAR* var,char *buf, int *plocal_es0,int *pglobal_es0)
{
char baux[41];
char buftsys[24+1];
int iaux1,iaux2;
int nc;
int i,i1,i2;
int finlin;
unsigned char ch;
unsigned long ulaux;
unsigned short kk;
int dec=0;
char prefix;
int x_es0;  // per actualitzar plocal_es0/ pglobal_es0 segons format
	finlin=format & FINLIN;
	format=format & (~FINLIN);
	switch(format)
	{// assignar valor a buftsys
		case FORM_KM:       // 1 decimal i texte KM
			get_texte_sys(T_KM,buftsys);
			break;
		case FORM_MON:  //ndec i texte moneda(PTA,DM..)
		case FORM_MON_AST: 
		case FORM_CONTRAVALOR:
        case FORM_MON_LZ:
			memncpy(buftsys,(char*)reg_moneda->abrev_B,LEN_ABRV);
			break;
		case FORM_MON_AUX:  //ndec i texte contramoneda(PTA,DM..)
		case FORM_MON_AUX_AST:  
                case FORM_MON_AUX_LZ:
			memncpy(buftsys,(char*)reg_moneda->abrev_A,LEN_ABRV);
			break;
		default:
			buftsys[0]=0; 
			break;
	}// fi assignar valor a buftsys
        
        prefix = 0;
        
	switch(format)
	{// assignar lengths
		case FORM_MON:  //ndec i texte moneda(PTA,DM..)
		case FORM_MON_AST: 
		case FORM_CONTRAVALOR:       
		case FORM_MON_AUX:  //ndec i texte contramoneda(PTA,DM..)
		case FORM_MON_AUX_AST:  
		case FORM_0DEC:       
		case FORM_0DEC_BLK:       
		//case FORM_ACH_BLK:       
		case FORM_SECRET: // format  secret (VAR_ASC)
		case FORM_RZ_VAR:
		case FORM_RZ_ALFANUM: 
		case FORM_RZ_NUM: 
			iaux1=0;
			iaux2=ndig-strlen(buftsys);
			break;
        case FORM_MON_LZ:
        case FORM_MON_AUX_LZ:
			iaux1=strlen(buftsys);
			iaux2=ndig-strlen(buftsys);
			prefix = 1;
			break;                            
	}// fi assignar lengths
	switch(format)
          {
		case FORM_LZ_EBCDIC:
			switch(var->tipus)
			{
				case VAR_UCHAR:
					itoa_dp_blk0((unsigned short)var->var.c,baux,ndig,0,'.');
					break;
				case VAR_UINT:
					itoa_dp_blk0(var->var.i,baux,ndig,0,'.');
					break;
				case VAR_ULONG:
					ltoa_dp_blk0(var->var.l,baux,ndig,0,'.');
					break;
				case VAR_ASC:
					if(strlen(var->var.s)<ndig)
					{
						memset(baux,' ',ndig);
						memcpy(baux, var->var.s, strlen(var->var.s));
						break;
					}
					// sense break;
				default:
					memcpy(baux, var->var.s, ndig);
					//baux[ndig]=0;
					break;
			}
			memcpy(buf, baux, ndig);
			ASCII_EBCDIC(buf, ndig, 1);
/*
			for(iaux1=0; baux[iaux1] && (iaux1<ndig); iaux1++)
			{
				buf[iaux1]=baux[iaux1];
				if((baux[iaux1]>='0') && (baux[iaux1]<='9'))
					buf[iaux1]+=0xc0;
				else if(((baux[iaux1]>='a') && (baux[iaux1]<='i')) || 
						(baux[iaux1]==' ') )
					buf[iaux1]+=0x20;
				else if((baux[iaux1]>='j') && (baux[iaux1]<='r'))
					buf[iaux1]+=0x27;
				else if((baux[iaux1]>='s') && (baux[iaux1]<='z'))
					buf[iaux1]+=0x2F;
				else if((baux[iaux1]>='A') && (baux[iaux1]<='I'))
					buf[iaux1]+=0x80;
				else if((baux[iaux1]>='J') && (baux[iaux1]<='R'))
					buf[iaux1]+=0x87;
				else if((baux[iaux1]>='S') && (baux[iaux1]<='Z'))
					buf[iaux1]+=0x8F;
			}
*/
			nc=ndig;
			break;
		case FORM_HMSMD:
			nc=ndig/2;
			buf[0]=bin_bcd(var->var.fh.hour);
			if(nc>1)
				buf[1]=bin_bcd(var->var.fh.minute);
			if(nc>2)
				buf[2]=bin_bcd(var->var.fh.sec);
			if(nc>3)
				buf[3]=bin_bcd(var->var.fh.month);
			if(nc>4)
				buf[4]=bin_bcd(var->var.fh.day);
			break;
		case FORM_LLVAR:
			iaux1=(strlen(var->var.s)+1)/2;
			if(iaux1>99)
				iaux1=99;
			buf[0]=0xf0+iaux1/10;
			buf[1]=0xf0+iaux1%10;
			nc=2+iaux1;
			for(iaux2=0; iaux2<iaux1; iaux2++)
			{
				if(var->var.s[iaux2*2]!=SEP_P2)
					buf[iaux2+2]=((unsigned char)achtoi((unsigned char *) &var->var.s[iaux2*2],1))<<4;
				else
					buf[iaux2+2]=0xd0;
				if(var->var.s[iaux2*2+1]==SEP_P2)
					buf[iaux2+2]|=0x0d;
				else if(var->var.s[iaux2*2+1]=='\0')
					buf[iaux2+2]|=0x0f;
				else
					buf[iaux2+2]|=(unsigned char)achtoi((unsigned char *) &var->var.s[iaux2*2+1],1);
			}
			break;
		case FORM_N_BCD:// PASAR ASCII(N�S 0..9,A..F) TORNES BCD. "12F4",4->0x12,0xF4
			switch(var->tipus)
			{
				case VAR_UCHAR:
					itoa_dp_blk0((unsigned short)var->var.c,baux,ndig,0,reg_moneda->punto_coma_B[0]);
					break;
				case VAR_UINT:
					itoa_dp_blk0(var->var.i,baux,ndig,0,reg_moneda->punto_coma_B[0]);
					break;
				case VAR_ULONG:
					ltoa_dp_blk0(var->var.l,baux,ndig,0,reg_moneda->punto_coma_B[0]);
					break;
				case VAR_ASC:
				default:
					memcpy(baux, var->var.s, ndig);
					baux[ndig]=0;
					break;
			}
			nc=(ndig+1)/2;
			iaux1=0;
			for(iaux2=nc; iaux2>0; iaux2--)
			{
				iaux1++;
				buf[iaux2-1]=(unsigned char)achtoi((unsigned char *) &baux[ndig-iaux1],1);			
				if(ndig-iaux1)
				{
					iaux1++;
					buf[iaux2-1]|=( (((unsigned char)achtoi((unsigned char *) &baux[ndig-iaux1],1))<<4) & 0xf0);
				}
			}
			break;  
     case FORM_BIN:
			switch(var->tipus)
			{
				case VAR_UCHAR:
					nc=1;
					baux[0]=var->var.c;
					break;
				case VAR_UINT:
					nc=2;
					memcpy(baux, (char*)(&var->var.i), 2);
					break;
				case VAR_ULONG:
					nc=4;
					memcpy(baux, (char*)(&var->var.l), 4);
					break;
				case VAR_ASC:
				default:
					nc=ndig;
					memcpy(baux, var->var.s, nc);
					break;
			}
			memset(buf,0,ndig);
			if(ndig>=nc)
				memcpy(&buf[ndig-nc],baux,nc);
			else // nc>ndig
				memcpy(buf,&baux[nc-ndig],ndig);
			nc=ndig;
			break;       
     case FORM_LZ_VAR:
		switch(var->tipus)
		{
			case VAR_UINT:
				sprintf(var->var.s,"%d",var->var.i);
				break;
			case VAR_ULONG:
				sprintf(var->var.s,"%ld",var->var.l);
				break;
		}
     case FORM_LZ:	 
	   iaux2=strlen(var->var.s);
       iaux1= __min(ndig,iaux2);
	   nc=0;
	   for(i=0; i<iaux1; i++,nc++)
	   {
			buf[nc]=var->var.s[i];
			if(buf[nc]==0x1b)
				buf[++nc]=0x1b;
	   }
	   //   memcpy(buf,var->var.s,iaux1);
       //nc=iaux1;
       if((format==FORM_LZ) && (iaux1<ndig))
          {
           memset(buf+nc,' ',ndig-iaux1);
           nc+=(ndig-iaux1);
          }
       break;       
     case FORM_MON:  //ndec i texte moneda(PTA,DM..)
     case FORM_MON_AST:
     case FORM_MON_LZ:
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf+iaux1,iaux2,reg_moneda->num_dec_B,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf+iaux1,iaux2,reg_moneda->num_dec_B,reg_moneda->punto_coma_B[0]);
            break;
          default:
            memset(buf+iaux1,'?',iaux2);
            buf[iaux1+iaux2]=0;
            break;
         }
       nc=assignar_cursors(buf,buftsys, prefix);  
       break;
     case FORM_CONTRAVALOR:       
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf+iaux1,iaux2,reg_moneda->decimales_contravalor,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf+iaux1,iaux2,reg_moneda->decimales_contravalor,reg_moneda->punto_coma_B[0]);
            break;
          default:
            memset(buf+iaux1,'?',iaux2);
            buf[iaux1+iaux2]=0;
            break;
         }         
       nc=assignar_cursors(buf,buftsys, prefix);
       break;
     case FORM_MON_AUX:  //ndec i texte contramoneda(PTA,DM..)
     case FORM_MON_AUX_AST:
     case FORM_MON_AUX_LZ:
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf+iaux1,iaux2,reg_moneda->num_dec_A,reg_moneda->punto_coma_A[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf+iaux1,iaux2,reg_moneda->num_dec_A,reg_moneda->punto_coma_A[0]);
            break;
          default:
            memset(buf+iaux1,'?',iaux2);
            buf[iaux1+iaux2]=0;
            break;
         }
       nc=assignar_cursors(buf,buftsys, prefix);
       break;
     case FORM_DDMMAA:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
            if(diames_mesdia)
             sprintf(baux,"%02d/%02d/%02d",
                           (int)var->var.fh.day,
                           (int)var->var.fh.month,
                           (int)var->var.fh.year);
             else
             sprintf(baux,"%02d/%02d/%02d",
                           (int)var->var.fh.month,
                           (int)var->var.fh.day,
                           (int)var->var.fh.year);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          case VAR_AMDHM:
          case VAR_AMD:
            if(diames_mesdia)
             sprintf(baux,"%02d/%02d/%02d",
                           (int)bcd_bin(var->var.s[2]),
                           (int)bcd_bin(var->var.s[1]),
                           (int)bcd_bin(var->var.s[0]));
             else
             sprintf(baux,"%02d/%02d/%02d",
                           (int)bcd_bin(var->var.s[1]),
                           (int)bcd_bin(var->var.s[2]),
                           (int)bcd_bin(var->var.s[0]));
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
    case FORM_ACH:
        for(iaux2=0; iaux2<ndig; iaux2++)
            //char_to_hexa((unsigned char)var->var.s[iaux2], &baux[iaux2*2], &baux[(iaux2*2)+1]);
            baux[iaux2]=(unsigned char)var->var.s[iaux2];
        baux[iaux2]=0;
        memcpy(buf,baux,ndig);
        nc=ndig;
        break;  
    case FORM_AMD:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
             sprintf(baux,"%02d%02d%02d",
                           (int)var->var.fh.year,
                           (int)var->var.fh.month,
                           (int)var->var.fh.day);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_HHMM:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
             sprintf(baux,"%02d:%02d",
                           (int)var->var.fh.hour,
                           (int)var->var.fh.minute);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          case VAR_AMDHM:
             sprintf(baux,"%02d:%02d",
                           (int)bcd_bin(var->var.s[3]),
                           (int)bcd_bin(var->var.s[4]));
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          case VAR_HM:
             sprintf(baux,"%02d:%02d",
                           (int)bcd_bin(var->var.s[0]),
                           (int)bcd_bin(var->var.s[1]));
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          case VAR_BCD:
            bcdtoa_dp(var->var.s,buf,iaux1,2,':',var->nb);
            nc=strlen(buf);
            break;
          case VAR_UINT:
          case VAR_ULONG:
			ltoa_dp_blk0(minutos_to_hhmm((var->tipus==VAR_UINT)?var->var.i:var->var.l),buf,iaux1,2,':');
            nc=strlen(buf);
            break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_TOT_MINUTS:   // versio 2.00 per veure totalitzadors temps (en minuts) amb format H...H:MM
     case FORM_TOT_HORES:   // versio 2.02 fa el mateix que FORM_TOT_MINUTS 
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_UINT:
          case VAR_ULONG:
            ulaux =(var->tipus==VAR_UINT)?var->var.i:var->var.l;
            if(ulaux == 99999999L)
            {
              memset(buf,'9',ndig);
              buf[ndig]=0;
            }
            else
	      ltoa_dp(minutos_to_hhmm(ulaux),buf,iaux1,2,':');
            nc=strlen(buf);
            break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_HM:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
             sprintf(baux,"%02d%02d",
                           (int)var->var.fh.hour,
                           (int)var->var.fh.minute);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_KM:       // 1 decimal i texte KM
       iaux1=ndig-strlen(buftsys);
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf,iaux1,1,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf,iaux1,1,reg_moneda->punto_coma_B[0]);
            break;
          default:
            memset(buf,'?',iaux1);
            buf[iaux1]=0;
            break;
         }
       nc=strlen(buf);
       strcpy(buf+nc,buftsys);
       nc+=strlen(buftsys);
       break;
     case FORM_DEC:       // num_dec decimals
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf,ndig,reg_moneda->num_dec_B,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf,ndig,reg_moneda->num_dec_B,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_BCD:
            bcdtoa_dp(var->var.s,buf,ndig,reg_moneda->num_dec_B,reg_moneda->punto_coma_B[0],var->nb);
            break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            break;
         }
       nc=strlen(buf);
       break;
     case FORM_DEC_AUX:       // num_dec decimals
       switch(var->tipus)
         {
          case VAR_UINT:
            itoa_dp(var->var.i,buf,ndig,reg_moneda->num_dec_A,reg_moneda->punto_coma_A[0]);
            break;
          case VAR_ULONG:
            ltoa_dp(var->var.l,buf,ndig,reg_moneda->num_dec_A,reg_moneda->punto_coma_A[0]);
            break;
          case VAR_BCD:
            bcdtoa_dp(var->var.s,buf,ndig,reg_moneda->num_dec_A,reg_moneda->punto_coma_A[0],var->nb);
            break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            break;
         }
       nc=strlen(buf);
       break;
	case FORM_6DEC:
		dec++;
	case FORM_5DEC:
		dec++;
	case FORM_4DEC:		  // 4 decimel
		dec++;
	case FORM_3DEC:
		dec++;
	case FORM_2DEC:       // 2 decimal
		dec++;
	case FORM_1DEC:       // 1 decimal
		dec++;
	case FORM_0DEC:       // sense decimals (char,int,long,float)
		switch(var->tipus)
		{
		case VAR_UINT:
			itoa_dp(var->var.i,buf,ndig,dec,reg_moneda->punto_coma_B[0]);
			break;
		case VAR_ULONG:
			ltoa_dp(var->var.l,buf,ndig,dec,reg_moneda->punto_coma_B[0]);
			break;
		case VAR_FLOAT:
			ulaux = (unsigned long)(var->var.f *tabPot[dec] + .5);
			ltoa_dp(ulaux,buf,ndig,dec,reg_moneda->punto_coma_B[0]);
			break;
		case VAR_BCD:
			bcdtoa_dp(var->var.s,buf,ndig,dec,reg_moneda->punto_coma_B[0],var->nb);
			break;
		default:
			memset(buf,'?',ndig);
			buf[ndig]=0;
			break;
		}
		if(format==FORM_0DEC)
			nc=assignar_cursors(buf,buftsys, prefix);
		else
			nc=strlen(buf);
		break;
     case FORM_0DEC_BLK:       // sense decimals (char,int,long)
       switch(var->tipus)
         {
         case VAR_UCHAR:
                itoa_dp_blk0((unsigned short)var->var.c,buf+iaux1,iaux2,0,reg_moneda->punto_coma_B[0]);
			 break;
         case VAR_UINT:
                itoa_dp_blk0(var->var.i,buf+iaux1,iaux2,0,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_ULONG:
                ltoa_dp_blk0(var->var.l,buf+iaux1,iaux2,0,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_FLOAT:
                ulaux = (unsigned long)(var->var.f  + .5);
                ltoa_dp_blk0(ulaux,buf+iaux1,iaux2,0,reg_moneda->punto_coma_B[0]);
            break;
          case VAR_BCD:
            bcdtoa_dp(var->var.s,buf+iaux1,iaux2,0,reg_moneda->punto_coma_B[0],var->nb);
            break;
          default:
            memset(buf+iaux1,'?',iaux2);
            buf[iaux1+iaux2]=0;
            break;
         }
       nc=assignar_cursors(buf,buftsys, prefix);
       break;
     case FORM_ES0:
     case FORM_G_ES0:
       nc=0;
       x_es0=0;
       switch(var->tipus)
         {
          case VAR_FEHOR:
            if(!fecha_logica(&(var->var.fh)))
              x_es0=1;
            break;
          case VAR_UCHAR:
            if(var->var.c==0)
              x_es0=1;
            break;
          case VAR_UINT:
            if(var->var.i==0)
              x_es0=1;
            break;
          case VAR_ULONG:
            if(var->var.l==0)
              x_es0=1;
            break;
          case VAR_FLOAT:
            if(var->var.f==0.0)
              x_es0=1;
            break;
          case VAR_BCD:
            x_es0=1;
            for(i=0;i<var->nb;i++)
              if(var->var.s[i])
                {x_es0=0;break;}
            break;
          case VAR_ASC:
            if(es_str_valido((unsigned char *) var->var.s,var->nb))
              x_es0=0;
            else
            {
              x_es0=1;
              for(kk=0; (kk<var->nb) && (var->var.s[kk]); kk++)
              {
                if(var->var.s[kk]==' ')
                {
                  if(var->var.s[kk+1]==0)
                  {
                    x_es0=0;
                    break;
                  }
                  continue;
                }
                else
                  break;
              }
            }
            break;
         }
       switch(format)
       {
        case FORM_ES0:
          *plocal_es0 = x_es0;
          break;
        case FORM_G_ES0:
          *pglobal_es0 = x_es0;
          break;
       }
       break;
     case FORM_4BITS:
       switch(var->tipus)
         {
          case VAR_UCHAR:
            ulaux=(unsigned long)var->var.c;
            break;
          case VAR_UINT:
            ulaux=(unsigned long)var->var.i;
            break;
          case VAR_ULONG:
            ulaux=(unsigned long)var->var.l;
            break;
         }
       for(i=1;i<=ndig;i++,ulaux/=16)
         {
          ch=(char)(ulaux%16);
          if(ch<10)
            buf[ndig-i]=ch|0x30;
          else  
            buf[ndig-i]=ch+55;
         }   
       buf[ndig]=0;
       nc=ndig;
       break;
     case FORM_SECRET_CC: 
       iaux2=strlen(var->var.s);
	   if(iaux2>12)
			memset(var->var.s,'*',12);
       if((iaux2)<ndig)
          {       
           iaux1=ndig-iaux2;
           memset(buf,' ',iaux1);
           memcpy(buf+iaux1,var->var.s,iaux2);
          }
       else
         {
          memcpy(buf,var->var.s+iaux2-ndig,ndig);
         } 
       buf[ndig]=0;
       nc=ndig;
       break;
     case FORM_SECRET: // format  secret (VAR_ASC)
       switch(var->tipus)
         {
          case VAR_ASC:
            memset(buf+iaux1,'-',iaux2);
            memset(buf+iaux1,'*',strlen(var->var.s));
            buf[iaux1+iaux2]=0;
            break;
          default:
            memset(buf+iaux1,'?',iaux2);
            buf[iaux1+iaux2]=0;
            break;
         }
       nc=assignar_cursors(buf,buftsys, prefix);
       break;
     case FORM_DMA:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
             sprintf(baux,"%02d%02d%02d",
                           (int)var->var.fh.day,
                           (int)var->var.fh.month,
                           (int)var->var.fh.year);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_DM:
       iaux1=ndig;
       switch(var->tipus)
         {
          case VAR_FEHOR:
             sprintf(baux,"%02d%02d",
                           (int)var->var.fh.day,
                           (int)var->var.fh.month);
             iaux2=strlen(baux);
             nc=0;
             if(iaux2<iaux1)
                {
                 memset(buf,' ',iaux1-iaux2);
                 nc=(iaux1-iaux2);
                }
             strcpy(buf+nc,baux);
             nc+=iaux2;
             break;
          default:
            memset(buf,'?',ndig);
            buf[ndig]=0;
            nc=ndig;
            break;
         }
       break;
     case FORM_RZ: 
       iaux2=strlen(var->var.s);
       if((iaux2)<ndig)
          {       
           iaux1=ndig-iaux2;
           memset(buf,' ',iaux1);
           memcpy(buf+iaux1,var->var.s,iaux2);
          }
       else
         {
          memcpy(buf,var->var.s+iaux2-ndig,ndig);
         } 
       buf[ndig]=0;
       nc=ndig;
       break;
     case FORM_RZ_ACH: 
       for(i=0; i<ndig; i+=2)
            char_to_hexa(var->var.s[i/2], (unsigned char *) &buf[i], (unsigned char *) &buf[i+1]);
       nc=ndig;
       buf[nc]=0;
       break;
     case FORM_RZ_VAR:
     case FORM_RZ_ALFANUM: 
     case FORM_RZ_NUM: 
      buf[0]=' ';
       i2=strlen(var->var.s);
       if(i2<iaux2)
          {       
           i1=iaux2-i2;
           memset(buf+iaux1,' ',i1);
           memcpy(buf+iaux1+i1,var->var.s,i2);
          }
       else
          {
           memcpy(buf+iaux1,var->var.s+i2-iaux2,iaux2);
          }
       buf[iaux1+iaux2]=0; 
       nc=assignar_cursors(buf,buftsys, prefix);   
       buf[nc]=0;
       break;        
     case FORM_ANAG:
        memset(baux,0x20,14);
        baux[0] = GR; //car�cter especial , que indica a drv_lpt que toca imprimir un BITMAP.
        nc = 13; //GR+"xxx,12345678" - con xxx=posici�n y "," y 12345678=nombre ANAGRAMA		 
        if(ndig>=12)
           memcpy(&baux[1], var->var.s, 12);
        else
           memcpy(&baux[1], var->var.s, ndig);
        memcpy(buf,baux,nc);
	break;

    case FORM_FONT_SIZE:
		buf[0] = FS;
		buf[1] = ndig;
		nc = 2;
		break;
	 case FORM_LOGO:
		buf[0]=0x1c;
		buf[1]=0x70;
		buf[2]=1;
		buf[3]=0;
//		buf[4]=0x0d;
		nc=4;
		break;
     default:
       memset(buf,'?',ndig);
       buf[ndig]=0;
       nc=ndig;
       break;
    }
	if((format==FORM_MON_AST) || (format==FORM_MON_AUX_AST))
	{
		for(i=0; (i<nc); i++)
		{
			if(buf[i]==' ')
				buf[i]='*';
		}
	}
	if(finlin)
		buf[nc++]=0x0d;
	return(nc);
}

