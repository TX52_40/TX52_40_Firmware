#ifndef _CRC32_H
#define _CRC32_H

extern unsigned long crc32(unsigned long sum, unsigned char *p, unsigned long len);

#endif